#ifndef TRUTHZPTNTUPLES_H
#define TRUTHZPTNTUPLES_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <SampleHandler/MetaData.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <TH1D.h>
#include <TTree.h>
#include <string>
#include <vector>

class TruthZPtNtuples : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  TruthZPtNtuples (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  enum Mode {Signal=0, Ztautau=1, Zll=2};

private:
  // Configuration, and any other types of variables go here.
  Mode m_mode;
  std::string m_submitted_at;
  std::string m_submitted_by;
  std::string m_submitted_revision;
  std::string m_submitted_type;

  TH1D *m_metadata;  //!
  TTree *m_tree;  //!
  int m_runNumber;  //!
  double m_mcWeight;  //!
  double m_truth_Z_pT;  //!

  const xAOD::EventInfo* m_eventInfo;  //!
  const xAOD::TruthParticleContainer* m_truthParticles;  //!
};

#endif
