#include <TruthZPtNtuples/TruthZPtNtuples.h>
#include <AsgTools/MessageCheck.h>
#include <EventLoop/Worker.h>



TruthZPtNtuples :: TruthZPtNtuples (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
  declareProperty("mode", m_mode=Mode::Signal, "Run mode ('Signal=0, Ztautau=1, Zll=2)");
  declareProperty("submitted_at", m_submitted_at="UNKNOWN", "Machine that submitted the production job");
  declareProperty("submitted_by", m_submitted_by="UNKNOWN", "Person that submitted the production job");
  declareProperty("submitted_revision", m_submitted_revision="UNKNOWN", "Code revision");
  declareProperty("submitted_type", m_submitted_type="UNKNOWN", "Submitted job type");
}



StatusCode TruthZPtNtuples :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  TFile *output_file = wk()->getOutputFile("truthZPt");

  auto current_dir = gDirectory;
  output_file->cd();
  SH::MetaData<std::string>("submitted_at", m_submitted_at).Write();
  SH::MetaData<std::string>("submitted_by", m_submitted_by).Write();
  SH::MetaData<std::string>("submitted_type", m_submitted_type).Write();
  SH::MetaData<std::string>("submitted_revision", m_submitted_revision).Write();
  current_dir->cd();

  m_metadata = new TH1D("metadata", "Metada information", 15, -0.5, 14.5);  // Consistent with LFV_Z package
  m_metadata->SetDirectory(output_file);
  m_metadata->GetXaxis()->SetBinLabel(7, "#Sigma AOD events");
  m_metadata->GetXaxis()->SetBinLabel(8, "#Sigma AOD weighted events");

  m_tree = new TTree("_NOMINAL", "_NOMINAL");
  m_tree->SetDirectory(output_file);
  m_tree->Branch("runNumber", &m_runNumber);
  m_tree->Branch("mcWeight", &m_mcWeight);
  m_tree->Branch("truth_Z_pT", &m_truth_Z_pT);

  return StatusCode::SUCCESS;
}



StatusCode TruthZPtNtuples :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  m_runNumber = -999;
  m_mcWeight = -999.;
  m_truth_Z_pT = -999.;

  if (!evtStore()->retrieve(m_eventInfo, "EventInfo").isSuccess()) {
    throw std::runtime_error("Could not retrieve EventInfo!");
  }
  m_runNumber = m_eventInfo->mcChannelNumber();
  const std::vector<float> weights = m_eventInfo->mcEventWeights();
  m_mcWeight = weights.empty() ? 1. : weights[0];
  m_metadata->Fill(6., 1.);  // number of events
  m_metadata->Fill(7., m_mcWeight);  // sum of weights

  if (!evtStore()->retrieve(m_truthParticles, "TruthParticles").isSuccess()) {
    throw std::runtime_error("Could not retrieve TruthParticles!");
  }

  switch(m_mode) {
    case Mode::Signal : {
      bool lep_found, tau_found;
      const xAOD::TruthParticle *Z_child;
      for(const auto &Z : *m_truthParticles) {
        if(Z->absPdgId() != 23 || Z->nChildren() != 2 || fabs(Z->m() - 91000.) > 25000.) continue;
        lep_found = false;
        tau_found = false;
        for(int i=0; i<2; ++i) {
          Z_child = Z->child(i);
          if(!Z_child || Z_child->pt() < 27000. || fabs(Z_child->eta()) > 2.5) continue;
          lep_found |= Z_child->isMuon() || Z_child->isElectron();
          tau_found |= Z_child->isTau();
        }
        if(lep_found && tau_found) {
          m_truth_Z_pT = Z->pt() * 0.001;
          break;
        }
      }
      break;
    }

    // There is no truth Z in Sherpa Z->tautau samples
    // There are, however, always exactly two truth isolated/status=2 taus from the Z decay
    // Use them to reconstruct the Z
    case Mode::Ztautau : {
      const xAOD::TruthParticle *tau_1 = nullptr;
      const xAOD::TruthParticle *tau_2 = nullptr;
      for(const auto &tau : *m_truthParticles) {
        if(!tau->isTau() || tau->status() != 2 || tau->pt() < 27000. || fabs(tau->eta()) > 2.5) continue;
        if(!tau_1) {
          tau_1 = tau;
        } else {
          tau_2 = tau;
          break;
        }
      }
      if(tau_1 && tau_2) {
        auto Z = tau_1->p4() + tau_2->p4();
        if(fabs(Z.M() - 91000.) > 25000.) break;
        m_truth_Z_pT = Z.Pt() * 0.001;
      }
      break;
    }

    // Powheg Z->ll/tautau only has one truth Z that decays into >=2 daughter particles
    // The first 2 daughters are always the l+l-/tau+tau- pair (the rest are photons)
    case Mode::Zll : {
      const xAOD::TruthParticle *lep_1, *lep_2;
      for(const auto &Z : *m_truthParticles) {
        if(Z->absPdgId() != 23 || Z->nChildren() < 2 || fabs(Z->m() - 91000.) > 25000.) continue;
        lep_1 = Z->child(0);
        lep_2 = Z->child(1);
        if(!lep_1 || lep_1->pt() < 27000. || fabs(lep_1->eta()) > 2.5) continue;
        if(!lep_2 || lep_2->pt() < 27000. || fabs(lep_2->eta()) > 2.5) continue;
        if(!lep_1->isChLepton()) continue;
        if(lep_1->pdgId() + lep_2->pdgId()) continue;  // Not l+l- pair
        m_truth_Z_pT = Z->pt() * 0.001;
        break;
      }
      break;
    }
  }

  if(m_truth_Z_pT < 0.) return StatusCode::SUCCESS;
  m_tree->Fill();

  return StatusCode::SUCCESS;
}



StatusCode TruthZPtNtuples :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}
