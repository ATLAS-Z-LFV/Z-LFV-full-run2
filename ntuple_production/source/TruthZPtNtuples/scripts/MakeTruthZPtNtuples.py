#!/usr/bin/env python

import sys, os, argparse
import time, uuid, getpass, socket
import subprocess

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.xAOD.Init().ignore()

from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig

#=================================================
default_user_name = None
for var_name in ['CERN_USER', 'CERNUSER', 'LOGNAME', 'USER']:
    if var_name in os.environ:
        default_user_name = os.getenv(var_name)
        break
if default_user_name is None:
    default_user_name = getpass.getuser()

current_datetime = time.strftime("%y%m%d_%H%M")

#=================================================
parser = argparse.ArgumentParser()
parser.add_argument("--mode", required=True, type=str, choices=['Sig','Ztt','Zll'], help="Output directory.")
parser.add_argument("--submitDir", default=None, type=str, help="Output directory.")
parser.add_argument("--maxEvents", default=0, type=str, help="Maximum number of events to process. Process all if zero.")
parser.add_argument("--samplePattern", default="*.root*", type=str, help="Sample pattern (default *.root*).")

parser.add_argument("--localPath", default=None, type=str, help="Local input directory.")

parser.add_argument("--useGrid", action='store_true', help="Use grid or else run locally.")
parser.add_argument("--inputDS", default=None, type=str, help="Input dataset(s).")
parser.add_argument("--inputDSFile", dest="inputDSFiles", action="append", type=str, help="File(s) to read input DS from.")
parser.add_argument("--outputSuffix", default=current_datetime, type=str, help="Suffix for output DS (default the current date and time: {0})".format(current_datetime))
parser.add_argument("--shortName", default=None, type=str, help="Short generator+process name.")
parser.add_argument("--destSE", default=None, type=str, help="Destination site for Grid output (e.g. CERN-PROD_SCRATCHDISK).")
parser.add_argument("--excludedSite", default=None, type=str, help="Grid sites to exclude.")
parser.add_argument("--site", default=None, type=str, help="Force to run at selected Grid site.")
parser.add_argument("--resubmission", default=False, action='store_true', help="It is a resubmission of an existing Grid job.")
parser.add_argument("--userName", default=default_user_name, type=str, help="Grid user name (default {0}).".format(default_user_name))

args = parser.parse_args()

#=================================================
if not args.submitDir:
    args.submitDir = 'run_{0}_{1}'.format(current_datetime, uuid.uuid4().hex)
if os.path.exists(args.submitDir):
    raise Exception("submitDir {0} already exists!".format(args.submitDir))

job = ROOT.EL.Job()

output = ROOT.EL.OutputStream("truthZPt")
job.outputAdd(output)

alg = AnaAlgorithmConfig('TruthZPtNtuples/TruthZPtNtuples')

if args.mode == "Sig":
    alg.mode = 0
elif args.mode == "Ztt":
    alg.mode = 1
elif args.mode == "Zll":
    alg.mode = 2

revision = subprocess.check_output(['git', 'describe', '--always', '--tags']).strip()
user_host = "{0}@{1}".format(getpass.getuser(), socket.getfqdn())
date = time.strftime("%d/%m/%Y %H:%M:%S", time.localtime())

print "Will write the following metadata to the output files:"
print "  User: {0}".format(user_host)
print "  Date: {0}".format(date)
print "  Rev.:  {0}".format(revision)

alg.submitted_at = date
alg.submitted_by = user_host
alg.submitted_revision = revision
alg.submitted_type = "local" if not args.useGrid else "grid"

job.algsAdd(alg)

if args.maxEvents > 0:
    job.options().setDouble(ROOT.EL.Job.optMaxEvents, float(args.maxEvents))

if not args.useGrid:
    if args.localPath is None:
        raise Exception("Running in local mode but no --localPath given.")

    sh = ROOT.SH.SampleHandler()
    sh.setMetaString('nc_tree', 'CollectionTree')
    ROOT.SH.ScanDir().filePattern(args.samplePattern).scan(sh, args.localPath)
    job.sampleHandler(sh)

    driver = ROOT.EL.DirectDriver()
    driver.submit(job, args.submitDir)

elif args.useGrid:
    input_DSs = []
    if args.inputDSFiles:
        inputs = set()
        for file_name in args.inputDSFiles:
            if not os.path.exists(file_name) or not os.path.isfile(file_name):
                raise Exception("Cannot find/access '{}'".format(file_name))
            with open(file_name) as f:
                lines = f.readlines()
            for L in lines:
                L = L.strip().strip('/')
                if L and not L.startswith('#'):
                    inputs.add(L)
        inputs = list(inputs)
        print("Loaded {0} input DS names from {1}".format(len(inputs), file_name))
        input_DSs += inputs
    if args.inputDS:
        input_DSs += args.inputDS.split(",")

    sh = ROOT.SH.SampleHandler()
    for x in input_DSs:
        ROOT.SH.addGrid(sh, x)
    sh.setMetaString("nc_grid_filter", args.samplePattern)
    job.sampleHandler(sh)

    driver = ROOT.EL.PrunDriver()

    # Set output DS name
    # prun counts name[] indices from 1 and not 0!
    process_name = args.shortName if args.shortName else "%in:name[3]%"
    output_DS_name = "user.{0}.truthZPt.%in:name[2]%.{1}.%in:name[6]%".format(args.userName, process_name)
    if args.outputSuffix:
        output_DS_name += ".{0}".format(args.outputSuffix)
    driver.options().setString("nc_outputSampleName", output_DS_name)

    # Set destination RSE
    if args.destSE:
        print "Setting destSE to {0}".format(args.destSE)
        driver.options().setString(ROOT.EL.Job.optGridDestSE, args.destSE)

    # Set selected/excluded Grid sites
    if args.excludedSite:
        job.options().setString(ROOT.EL.Job.optGridExcludedSite, args.excludedSite)

    if args.site:
        job.options().setString(ROOT.EL.Job.optGridSite, args.site)

    extraOpts = ["--excludeFile=*/.svn/*,.git,LFV_Z/*,Extrapolate/*,SUSYTools/*,TauSpinnerTool/*"]
    extraOpts += ["--useContElementBoundary --addNthFieldOfInDSToLFN=2,3"] # add DSID and name to sample name
    if args.maxEvents > 0:
        extraOpts += ["--nEvents={0} ".format(args.maxEvents)]

    if args.resubmission:
        extraOpts += ["--useNewCode"]

    if extraOpts != []:
        job.options().setString(ROOT.EL.Job.optSubmitFlags, " ".join(extraOpts))

    driver.options().setDouble(ROOT.EL.Job.optGridMergeOutput, False)

    driver.submitOnly(job, args.submitDir)
