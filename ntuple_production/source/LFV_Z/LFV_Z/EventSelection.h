// vim: ts=4 sw=4
#ifndef HAVE_EVENTSELECTION_H
#define HAVE_EVENTSELECTION_H

#include <string>
#include <vector>

#include "AsgTools/ToolHandle.h"
#include "TFile.h"

#include <EventLoop/Algorithm.h>

#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "GoodRunsLists/TGRLCollection.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "TauSpinnerTool/xAODEventReaderTool.h"
#include "TauSpinnerTool/StandardModelTool.h"

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODRootAccess/TActiveStore.h"

#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthVertex.h"

#include <LFV_Z/Config.h>
#include <LFV_Z/Cutflow.h>
#include "LFV_Z/Enums.h"
#include "LFV_Z/Region.h"
#include "LFV_Z/SUSYTools.h"

#include "DiTauMassTools/MissingMassTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "PileupReweighting/PileupReweightingTool.h"

#include "AssociationUtils/DeltaROverlapTool.h"

#include "PMGTools/PMGSherpa22VJetsWeightTool.h"

class EventSelection : public EL::Algorithm {
    // put your configuration variables here as public variables.
    // that way they can be set directly from CINT and python.
  public:
    // variables that don't get filled at submission time should be
    // protected from being send from the submission node to the worker
    // node (done by the //!)

    bool m_isSignal;
    bool m_isData;
    bool m_is25ns;
    bool m_isAtlfast;
    bool m_useSmearedJets;
    bool m_doSystematics;
    bool m_getPhotons;
    bool m_getTaus;
    bool m_photonInOR;
    int m_JESNuisanceParameterSet;

    std::string m_electronContainerKey;
    std::string m_photonContainerKey;
    std::string m_jetContainerKey;
    std::string m_bTaggingTimeStamp;
    std::string m_tauContainerKey;
    std::string m_trackParticleContainerKey;

    std::string m_eventNumbersFilename = "";

    std::string m_submitted_at = "";
    std::string m_submitted_by = "";
    std::string m_submitted_revision = "";
    std::string m_submitted_type = "local";

  public:
    // Tree *myTree; //!
    // TH1 *myHist; //!

    // this is a standard constructor
    EventSelection();

    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob(EL::Job& job);
    virtual EL::StatusCode fileExecute();
    virtual EL::StatusCode histInitialize();
    virtual EL::StatusCode changeInput(bool firstFile);
    virtual EL::StatusCode initialize();
    virtual EL::StatusCode execute();
    virtual EL::StatusCode postExecute();
    virtual EL::StatusCode finalize();
    virtual EL::StatusCode histFinalize();

    void setVerbose() {
        m_verbose = true;
    }

    void setConfigFile(const std::string& s) {
        m_configFile = s;
    }

  private:
    bool m_initialized = false;
    std::string m_configFile = "config.ini";
    std::shared_ptr<Config> m_config;  //!
    std::string m_containerSuffix;     //!
    std::string m_systematicSuffix;    //!

    xAOD::TEvent* m_event;  //!

    EL::StatusCode initializeSUSYTools();
    EL::StatusCode configurePRWTool();
    void initializeSystematics();
    void initializeNoSystematics();

    void addPreviousFileMetaData();
    EL::StatusCode processMetaData(bool firstFile);
    void processEvent(const ST::SystInfo& sys);
    void processWeightSystematics(bool onlyNominal = false);
    EL::StatusCode processPileup(const ST::SystInfo& sys);
    void fillTriggerInfo(xAOD::TEvent*& event) const;

    bool setPreselectionVariables();

    // stuff to get objects
    void getJets(bool storeVariation);
    void getTaus(bool storeVariation);
    void getElectrons(bool storeVariation);
    void getMuons(bool storeVariation);
    void getPhotons(bool storeVariation);
    void getScaleFactors(bool isNominal);
    char getIsolationWP(const xAOD::Muon& p);
    char getIsolationWP(const xAOD::Electron& p);
    void removeOverlaps();
    void setViewContainers();
    void rebuildMET(bool storeVariation);
    void rebuildTrackMET(bool storeVariation);
    bool checkBadJets(bool storeVariation);

    // Private event-based functions
    bool isSimulation();
    bool isData();
    bool primaryVertexCheck();
    void findPrimaryVertex();
    bool eventCheck();
    bool hasBadDetectorQuality();
    bool isNonCollisionBackgroundEvent();
    bool lArErrorCheck();
    bool SCTErrorCheck();
    bool tileErrorCheck();
    bool coreFlagCheck();
    bool tileTripCheck();
    bool passGRL();

    // Used tools
    std::shared_ptr<custom_SUSYObjDef_xAOD> m_SUSYTools;  //!
    std::unique_ptr<GoodRunsListSelectionTool> m_GRL;  //!
    // std::unique_ptr<CP::PileupReweightingTool> m_PRWTool;                            //!
    asg::AnaToolHandle<CP::IPileupReweightingTool> m_PRWTool;                        //!
    asg::AnaToolHandle<TauAnalysisTools::ITauSelectionTool> m_tauSelectionTool;      //!
    asg::AnaToolHandle<TauAnalysisTools::ITauSmearingTool> m_tauSmearingTool;        //!
    std::shared_ptr<TauAnalysisTools::TauTruthMatchingTool> m_tauTruthMatchingTool;  //!
    TauSpinnerTools::xAODEventReaderTool* m_TST_xAODEventReaderTool;                 //!
    ToolHandle<TauSpinnerTools::IEventReaderToolBase> m_TST_readerHandle;            //!
    std::shared_ptr<TauSpinnerTools::StandardModelTool> m_TST_StandardModelTool;     //!
    std::unique_ptr<MissingMassTool> m_MMC;                                          //!
    std::unique_ptr<ORUtils::DeltaROverlapTool> m_DeltaROverlapTool;                 //!
    std::unique_ptr<PMGTools::PMGSherpa22VJetsWeightTool> m_PMGSherpaTool;           //!

    //[> Input File Metadata <]
    // TTree* m_metadata;                                    //!
    // double m_evtSample;                                   //!
    // double m_evtWeightSample;                             //!
    // double m_tmpEvt;                                      //!
    // double m_tmpEvtWeight;                                //!
    // bool m_isDerivation;                                  //!
    // const xAOD::CutBookkeeperContainer* m_incompleteCBC;  //!
    // const xAOD::CutBookkeeperContainer* m_completeCBC;    //!
    // const xAOD::CutBookkeeper* m_allEvtsCBK;              //!

    bool m_usePileupWeight = true;

    // meta data
    bool m_isMC;                               //!
    bool m_isDerivation;                       //!
    std::string m_streamType;                  //!
    
    double m_xAOD_initialSumOfEvents;        //!
    double m_xAOD_initialSumOfWeights;         //!
    double m_xAOD_initialSumOfWeightsSquared;  //!

    double m_DxAOD_initialSumOfEvents;        //!
    double m_DxAOD_initialSumOfWeights;         //!
    double m_DxAOD_initialSumOfWeightsSquared;  //!
    
    double m_xAOD_initialSumOfEvents_thisFile;        //!
    double m_xAOD_initialSumOfWeights_thisFile;         //!
    double m_xAOD_initialSumOfWeightsSquared_thisFile;  //!

    double m_DxAOD_initialSumOfEvents_thisFile;        //!
    double m_DxAOD_initialSumOfWeights_thisFile;         //!
    double m_DxAOD_initialSumOfWeightsSquared_thisFile;  //!

    // counters
    //unsigned long m_nFilesProcessed;   //!
    double m_nFilesProcessed;   //!
    unsigned long m_nEventsProcessed;  //!
    unsigned int m_nAcceptedEvents;    //!

    // weighted events
    double m_nMCWeightedEvents;     //!
    double m_nPUWeightedEvents;     //!
    double m_nTotalWeightedEvents;  //!
    
    double m_nMCWeightedEvents_thisFile;  //!

    // event - channel - run
    int m_eventNumber;      //!
    int m_runNumber;        //!
    int m_mcChannelNumber;  //!

    // Event info
    const xAOD::EventInfo* m_eventInfo;  //!

    // Truth containers
    const xAOD::TruthEventContainer* m_truthEventContainer;        //!
    const xAOD::TruthParticleContainer* m_truthParticleContainer;  //!
    const xAOD::TruthParticleContainer* m_truthElectronContainer;  //!
    const xAOD::TruthParticleContainer* m_truthMuonContainer;      //!
    const xAOD::TruthParticleContainer* m_truthTauContainer;       //!
    const xAOD::JetContainer* m_truthJetContainer;                 //!
    const xAOD::MissingETContainer* m_truthMETContainer;           //!
    const xAOD::MissingET* m_truthMET;                             //!

    // Sherpa weights
    std::vector<float> m_LHE3weights;                 //!
    std::map<std::string, int> m_sherpa_theory_syst;  //!

    // Objects from event
    const xAOD::VertexContainer* m_vertices;                       //!
    const xAOD::Vertex* m_primaryVertex;                           //!
    bool m_primaryVertexFound;                                     //!
    const xAOD::TrackParticleContainer* m_trackParticleContainer;  //!

    // Containers we get back from SUSYTools
    xAOD::PhotonContainer* m_photons;                    //!
    xAOD::ShallowAuxContainer* m_photons_aux;            //!
    xAOD::ElectronContainer* m_electrons;                //!
    xAOD::ShallowAuxContainer* m_electrons_aux;          //!
    xAOD::MuonContainer* m_muons;                        //!
    xAOD::ShallowAuxContainer* m_muons_aux;              //!
    xAOD::TauJetContainer* m_taus;                       //!
    xAOD::ShallowAuxContainer* m_taus_aux;               //!
    xAOD::JetContainer* m_jets;                          //!
    xAOD::ShallowAuxContainer* m_jets_aux;               //!
    xAOD::MissingETContainer* m_rebuiltMET;              //!
    xAOD::MissingETAuxContainer* m_rebuiltMET_aux;       //!
    xAOD::MissingETContainer* m_rebuiltTrackMET;         //!
    xAOD::MissingETAuxContainer* m_rebuiltTrackMET_aux;  //!

    // Dummy containers
    xAOD::TauJetContainer* m_empty_taus_base;          //!
    xAOD::PhotonContainer* m_empty_photons_base;       //!
    //xAOD::AuxContainerBase* m_empty_taus_base_aux;     //!
    //xAOD::AuxContainerBase* m_empty_photons_base_aux;  //!
    xAOD::ShallowAuxContainer* m_empty_taus_shallow_aux;     //!
    xAOD::ShallowAuxContainer* m_empty_photons_shallow_aux;  //!

    // View containers for good objects
    std::unique_ptr<xAOD::MuonContainer> m_viewElemMuonContainer;          //!
    std::unique_ptr<xAOD::ElectronContainer> m_viewElemElectronContainer;  //!
    std::unique_ptr<xAOD::PhotonContainer> m_viewElemPhotonContainer;      //!
    std::unique_ptr<xAOD::TauJetContainer> m_viewElemTauContainer;         //!
    std::unique_ptr<xAOD::JetContainer> m_viewElemJetContainer;            //!

    // Private members
    std::vector<ST::SystInfo> m_systInfoList;            //!
    std::vector<ST::SystInfo> m_systInfoList_kinematic;  //!
    std::vector<ST::SystInfo> m_systInfoList_weights;    //!
    std::vector<std::string> m_systMatch;                //!
    std::vector<std::string> m_trigger_items;            //!
    std::map<std::string, bool> m_trigger_decisions;     //!

    // Event picker
    std::vector<unsigned int> m_eventNumbersToUse;  //!

    std::map<EleLH, std::unique_ptr<AsgElectronLikelihoodTool>> m_AsgElectronLikelihoodTool_map;  //!
    std::map<IsoSel, std::unique_ptr<CP::IsolationSelectionTool>> m_IsolationSelectionTool_map;   //!

    std::vector<float>* m_event_weights;                          //!
    std::vector<std::string>* m_event_weights_names;              //!
    std::vector<CP::SystematicSet>* m_sys_variations_kinematics;  //!

    xAOD::TStore* m_store;  //!

    std::string m_outputLabel = "foo";
    TFile* m_outputFile;               //!
    std::unique_ptr<Region> m_region;  //!

    bool m_verbose;
    double m_eventWeight;    //!
    double m_puWeight;       //!
    double m_puWeight_up;    //!
    double m_puWeight_down;  //!
    double m_mcWeight;       //!

    double m_initialSumOfWeights;         //!
    double m_initialSumOfWeightsSquared;  //!
    double m_finalSumOfWeights;           //!

    double m_initialSumOfEvents;  //!
    double m_finalSumOfEvents;    //!

    bool m_doTauTruthMatching;  //!

    // Cached booleans for SFs
    bool m_doTauSF;           //!
    bool m_doTauTrigSF;       //!
    bool m_doElectronSF;      //!
    bool m_doElectronTrigSF;  //!
    bool m_doMuonSF;          //!
    bool m_doMuonTrigSF;      //!
    bool m_doPhotonSF;        //!

    std::string m_SF_muonTrig2015 = "HLT_mu20_iloose_L1MU15";
    std::string m_SF_muonTrig2016 = "HLT_mu26_ivarmedium";

    // this is needed to distribute the algorithm to the workers
    ClassDef(EventSelection, 1);
};

// Accessors etc
const static SG::AuxElement::Accessor<char> is_baseline("baseline");
const static SG::AuxElement::Accessor<char> is_signal("signal");
const static SG::AuxElement::Accessor<char> is_isol("isol");
const static SG::AuxElement::Accessor<char> is_passOR("passOR");
const static SG::AuxElement::Accessor<char> is_passBaseID("passBaseID");
const static SG::AuxElement::Accessor<char> is_bad("bad");
const static SG::AuxElement::ConstAccessor<char> is_bjet("bjet");
const static SG::AuxElement::ConstAccessor<char> is_bjet_loose("bjet_loose");
const static SG::AuxElement::ConstAccessor<char> prioAcc("selected");

const static SG::AuxElement::ConstAccessor<char> acc_passJvt("passJvt");

const static SG::AuxElement::Accessor<double> acc_effscalefact("effscalefact");
const static SG::AuxElement::Accessor<float> acc_jvtscalefact("jvtscalefact");

const static SG::AuxElement::Decorator<char> dec_passOR("passOR");
const static SG::AuxElement::Decorator<char> dec_baseline("baseline");
const static SG::AuxElement::Decorator<char> dec_signal("signal");
const static SG::AuxElement::Decorator<char> dec_isol("isol");

#endif
