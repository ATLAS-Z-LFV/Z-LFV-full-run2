#ifndef LFV_Z_DummyAlg_H
#define LFV_Z_DummyAlg_H

#include <EventLoop/Algorithm.h>
#include "xAODRootAccess/TActiveStore.h"
#include "LFV_Z/Region.h"

class DummyAlg : public EL::Algorithm {
    // put your configuration variables here as public variables.
    // that way they can be set directly from CINT and python.
  public:
    // float cutValue;

    // variables that don't get filled at submission time should be
    // protected from being send from the submission node to the worker
    // node (done by the //!)
  public:
    // Tree *myTree; //!
    // TH1 *myHist; //!

    std::unique_ptr<Region> m_region;  //!
    unsigned int m_nEventsProcessed;   //!

    xAOD::TStore* m_store;  //!
    xAOD::TEvent* m_event;  //!

    // this is a standard constructor
    DummyAlg();

    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob(EL::Job& job);
    virtual EL::StatusCode fileExecute();
    virtual EL::StatusCode histInitialize();
    virtual EL::StatusCode changeInput(bool firstFile);
    virtual EL::StatusCode initialize();
    virtual EL::StatusCode execute();
    virtual EL::StatusCode postExecute();
    virtual EL::StatusCode finalize();
    virtual EL::StatusCode histFinalize();

    // this is needed to distribute the algorithm to the workers
    ClassDef(DummyAlg, 1);
};

#endif
