/**
 * @file Region.h
 * @short Region base class for regions
 *
 * Forked from xTauFW's xChannel.
 *
 * Defines the structure of the output root files for a given region.
 * Builds TTrees based on list of systematics
 * Links variables to tree branches.
 * Fills the booked tree branches with content assigned in derived regions
 * Communicates with the EventLoop Workers to store the tree output.
 *
 * Original author Z. Zinonos - zenon@cern.ch
 * @author Geert-Jan Besjes geert-jan.besjes@cern.ch
 *
 * @date Feb 2016
 */

#ifndef Region_Region_H
#define Region_Region_H

#include <algorithm>
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <string>
#include <vector>

//** implementation for C++11 */
#include "CxxUtils/make_unique.h"

/**
 * Root includes
 */
#include "TBranch.h"
#include "TFile.h"
#include "TTree.h"

/**
 * ASG includes
 */
#include <EventLoop/OutputStream.h>
#include <EventLoop/Worker.h>
#include <xAODRootAccess/TEvent.h>
#include <xAODRootAccess/TStore.h>
#include "PATInterfaces/SystematicSet.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"

//#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "LFV_Z/SUSYTools.h"

#include "TauSpinnerTool/xAODEventReaderTool.h"
#include "TauSpinnerTool/StandardModelTool.h"

/**
 * xTau includes
 */
//#include <xDefinitions/xDefinitions.h>
//#include <xEDMIncludes/xEDMIncludes.h>
//#include <Region/RegionDefs.h>
//#include <xJobOptions/xJobOptions.h>
//#include <xHistograms/xHistograms.h>
//#include <xCPTools/xCPTools.h>
//#include <xTools/xTools.h>
//#include <xHCP/xHCP.h>
//#include <xVariables/xVarList.h>
//#include <xVariables/xVar.h>
#include "LFV_Z/Config.h"
#include "LFV_Z/RegionTreeList.h"
#include "LFV_Z/VarList.h"
//#include "xSystematics/xSystematics.h"
//#include "xTrigger/xTriggerItemLists.h"

/**
 * Forward declarations
 */
// class xCPTools;
// class RegionTreeList;

/**
 * @short struct functions holding pointers to different objects
 * Keeps multiple input arguments to be used insided a derived region
 * @sa FillBranches()
 * @{
 */

struct MetArg {
    const xAOD::MissingET *reco;                          //!< container: reco   MET
    const xAOD::MissingET *reco_track;                    //!< container: reco track MET
    const xAOD::MissingET *truth;                         //!< container: truth  MET
    const xAOD::MissingET *softTerm;                      //!< container: soft   MET
    const xAOD::MissingET *eleTerm;                       //!< container: ele    MET
    const xAOD::MissingET *phoTerm;                       //!< container: photon MET
    const xAOD::MissingET *tauTerm;                       //!< container: tau    MET
    const xAOD::MissingET *muonTerm;                      //!< container: muon   MET
    const xAOD::MissingET *jetTerm;                       //!< container: jet    MET
    ConstDataVector<xAOD::ElectronContainer> *electrons;  // !< container: electrons used for met
    ConstDataVector<xAOD::PhotonContainer> *photons;      // !< container: photons   used for met
    ConstDataVector<xAOD::TauJetContainer> *taus;         // !< container: taus      used for met
    ConstDataVector<xAOD::MuonContainer> *muons;          // !< container: muons     used for met
    ConstDataVector<xAOD::JetContainer> *jets;            // !< container: jets      used for met
};

struct VtxArg {
    const xAOD::Vertex *primary;             //!< primary vertex
    const xAOD::VertexContainer *container;  //!< container: vertices
};

struct TruthArg {
    const xAOD::TruthEventContainer *event_info;    //!< truth event information
    const xAOD::TruthParticleContainer *particles;  //!< general truth particle container
    const xAOD::TruthParticleContainer *electrons;  //!< truth electron container
    const xAOD::TruthParticleContainer *muons;      //!< truth muon container
    const xAOD::TruthParticleContainer *taus;       //!< truth tau container
    const xAOD::JetContainer *jets;                 //!< truth jet container
};

struct TriggerArg {
    // const xTriggerDecision *decision; coming soon
    const std::map<std::string, bool> decision;  //!< map of trigger items
    // const xTriggerMuonList *muon_sf; //!< list of muon triggers and scale factors
};

struct FillBranchesArgs {
    // const xDiTauMass *ditauMass;				//!< di-tau mass wrapper class
    const xAOD::EventInfo *eventInfo;            //!< event info
    const xAOD::MuonContainer *muons;            //!< container: muons
    const xAOD::ElectronContainer *electrons;    //!< container: electrons
    const xAOD::PhotonContainer *photons;        //!< container: photons
    const xAOD::TauJetContainer *taus;           //!< container: taus
    const xAOD::JetContainer *jets;              //!< container: jets
    const xAOD::TrackParticleContainer *tracks;  //!< container tracks
    const VtxArg vertex;       //!< Vertex struct holding different vertex definitions and also the entire vtx container
    const MetArg met;          //!< MET struct holding truth and reco definitions
    const TruthArg truth;      //!< truth struct holding truth particles and truth electron, muon and tau containers
    const TriggerArg trigger;  //!< trigger struct bundling trigger information
    TauSpinnerTools::xAODEventReaderTool *TST_xAODEventReaderTool;  //!< TauSpinner reader tool
    TauSpinnerTools::StandardModelTool *TST_StandardModelTool;  //!< TauSpinner SM tool
};
/** @} */

struct RegionArgs {
    TFile *output_file;
    xAOD::TStore *store;
    xAOD::TEvent *event;
    // xTools *tools;
    // xCPTools *cp_tools;
    // xSystematics *systematics;
    std::vector<ST::SystInfo> systInfoList;
    // xTriggerMuonList *muon_trigger_sf;
    EL::IWorker *wk;
    std::map<std::string, int> theory_syst;
    std::shared_ptr<custom_SUSYObjDef_xAOD> SUSYTools;
};

/**
 * @short Region base class
 */
class Region {
  private:
    Region();  //!< default constructor hide it

  public:
    /**
     * @brief explicit constructor
     * @param args reference to the RegionArgs structure
     */
    // explicit Region( const RegionArgs & args);
    explicit Region(const std::string &prefix, TFile *outputFile, xAOD::TStore *store, xAOD::TEvent *event, EL::IWorker *wk,
                    const std::vector<ST::SystInfo> &systInfoList, const std::map<std::string, int> &theory_systs,
                    std::shared_ptr<custom_SUSYObjDef_xAOD> SUSYTools);

    /**
     * Default copy constructor
     */
    Region(const Region &) = default;

    /**
     * @short Default copy assignment constructor
     */
    Region &operator=(const Region &) = default;

    /**
     * @short Default move constructor
     */
    Region(Region && /*other*/) = default;

    /**
     * @short Default destructor
     */
    virtual ~Region() = default;

    /**
     * @short Setter for the event
     */

    virtual void setEvent(xAOD::TEvent *e) final {
        m_event = e;
    }
    
    size_t systematicListSize() { return m_syst_set_vec.size(); }

  private:
    std::string m_treePrefix;

    RegionTreeList m_tree_list;                     //<! list of RegionTree items
    std::vector<CP::SystematicSet> m_syst_set_vec;  //<! set of systematic variations
    std::vector<std::string> m_syst_name_vec;       //<! list of systematic variation  names

    // Used to be RegionArgs
    TFile *m_outputFile;
    xAOD::TStore *m_store;
    xAOD::TEvent *m_event;
    // xTools *tools;
    // xCPTools *cp_tools;
    // xSystematics *systematics;
    // xTriggerMuonList *muon_trigger_sf;
    EL::IWorker *m_wk;

    /**
     * @short Function to the list of systematic sets
     */
    void DefineSystematics();

    /**
     * @short Function to adjust names of systematics
     */
    void AdjustSystematicNames();

    /**
     * @short Function to initialize tree list with 'new' trees
     */
    void InitializeTrees();

    /**
     * @short Function to add 'new' trees to EL Worker output
     * DEPRECATED
     */
    //void AddTreesWorkerOutput();

    /**
     * @short Function to link branches to trees
     */
    void LinkVariables();

    /**
     * @short Function to associate trees to output root file
     */
    void AssociateOutputFile();

  protected:
    /**
     * @short Virtual final function to setup trees
     * @see AdjustSystematicNames()
     * @see InitializeTrees()
     * @see AddTreesWorkerOutput()
     * @see LinkVariables()
     * @see AssociateOutputFile()
     */

    virtual void SetupTrees() final;

    VarList vlist;                                             //!< variable list
    std::shared_ptr<Config> m_config = Config::getInstance();  //<! configuration
    std::shared_ptr<custom_SUSYObjDef_xAOD> m_SUSYTools;
    std::map<std::string, int> m_theory_systs;
    std::vector<ST::SystInfo> m_systInfoList;
    // xHistograms&      m_hist = xHistograms::getInstance(); //<! histograms

  public:
    /**
     * @short Pure virtual public function which returns the list of trigger items.
     * Body is expected to be defined in derived regions.
     * Can be used in main as  m_region()->GetTriggersFinal()
     */
    virtual auto GetTriggersFinal() const -> std::vector<std::string> = 0;

    /**
     * @short Pure virtual function which returns the list of all variables
     * Body is expected to be defined in derived regions
     * Can be used in main as  m_region_svc->region()->GetVariablesFinal()
     */
    virtual auto GetVariablesFinal() const -> std::vector<std::string> = 0;

    /**
     * @short Final virtual function which resets tree variables
     * Used in main at the end of every event scan
     */
    virtual void ResetVariables() final;

    /**
     * @short Final virtual function which fills tree with a given name
     * Used in main at the end of every event scan
     */
    virtual void FillTree(const std::string &) final;

    /**
     * @short Write trees to file and delete them
     * Used at the end of the job
     */
    virtual void WriteTrees(TDirectory* file) final;

    /**
     * @short Pure virtual function which passes event information to derived regions
     * Body is defined in derived regions
     * Used in main at the end of every event scan
     * @return true if the event should be stored
     */
    virtual bool FillBranches(const FillBranchesArgs &) = 0;
    virtual void FillDeferredBranches(const FillBranchesArgs &) = 0;

    /**
     * @brief: Finalize executed at the end of the run
     */
    virtual void Finalize();

    /**
     * @brief Region input arguments, @sa <code>struct RegionArgs</code> above.
     * Public access
     */

    // RegionArgs region;
};

#endif
