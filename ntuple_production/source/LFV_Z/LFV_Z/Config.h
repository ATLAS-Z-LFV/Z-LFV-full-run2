#ifndef HAVE_CONFIG_H
#define HAVE_CONFIG_H

#include <iostream>
#include <string>
#include <set>
#include <thread>
#include <mutex>
#include <map>
#include <vector>
#include <boost/lexical_cast.hpp>

class Config {
  private:
    static std::mutex _mutex;

    std::map<std::string, std::string> m_settings;

    Config() {}
    Config(const Config& rs) {
        m_settings = rs.m_settings;
    }
    Config& operator=(const Config& rs) {
        this->m_settings = rs.m_settings;
        return *this;
    }

  public:
    ~Config() {}

    void readConfigFile(std::string filename);

    static std::shared_ptr<Config> getInstance() {
        static std::shared_ptr<Config> instance = nullptr;

        if (!instance) {
            std::lock_guard<std::mutex> lock(_mutex);

            if (!instance) {
                instance.reset(new Config());
            }
        }

        return instance;
    }

    template <class T>
    T get(const std::string& name) {
        return boost::lexical_cast<T>(this->get(name));
    }

    bool has(const std::string& name) {
        if (m_settings.find(name) == m_settings.end()) {
            return false;
        }
        return true;
    }

    const std::string get(const std::string& name) {
        return m_settings[name];
    }

    template <class T>
    void set(const std::string& name, T value) {
        m_settings[name] = boost::lexical_cast<std::string>(value);
    }

    void set(const std::string& name, std::string value) {
        m_settings[name] = value;
    }

    // several methods because PyROOT is ridiculous
    void setInt(const std::string& name, int value) {
        this->set<int>(name, value);
    }

    void setFloat(const std::string& name, float value) {
        this->set<float>(name, value);
    }

    void setDouble(const std::string& name, double value) {
        this->set<double>(name, value);
    }

    void setBool(const std::string& name, bool value) {
        this->set<bool>(name, value);
    }

    void show() {
        print();  // for python
    }

    void print() {
        std::cout << "Dumping string-converted config" << std::endl;
        for (const auto& kv : m_settings) {
            std::cout << kv.first << " -> " << kv.second << std::endl;
        }
    }

    void demo() {
        std::cout << "smart pointers # next - your code ..." << std::endl;
        print();
    }
};

template <>
std::vector<std::string> Config::get<std::vector<std::string>>(const std::string& name);
#endif
