/**
 * @file RegionBuilder.h
 *
 * @brief Derived class to wrap around Region resources
 *
 * Manages the storage of all derived Region classes.
 *
 * @author Geert-Jan Besjes - geert-jan.besjes@cern.ch
 *
 * @date Feb 2016
 */

#ifndef HAVE_REGIONBUILDER_H
#define HAVE_REGIONBUILDER_H

#include <memory>
#include <string>
#include "CxxUtils/make_unique.h"

#include <LFV_Z/Region.h>
#include <LFV_Z/SystematicFunctions.h>

template <class T>
class RegionBuilder {
  public:
    RegionBuilder() : m_prefix("") {}

    RegionBuilder &setPrefix(const std::string &s) {
        // strip out accidentally passed " characters
        this->m_prefix = StringUtils::ReplaceSubstr(s, "\"", "");
        return *this;
    }

    RegionBuilder &setOutputFile(TFile *f) {
        this->m_outputFile = f;
        return *this;
    }

    RegionBuilder &setStore(xAOD::TStore *s) {
        this->m_store = s;
        return *this;
    }

    RegionBuilder &setEvent(xAOD::TEvent *e) {
        this->m_event = e;
        return *this;
    }

    RegionBuilder &setWorker(EL::IWorker *w) {
        this->m_wk = w;
        return *this;
    }

    RegionBuilder &setSystematics(std::vector<ST::SystInfo> &systInfoList) {
        this->m_systInfoList = systInfoList;
        return *this;
    }

    RegionBuilder &setTheorySystematics(std::map<std::string, int> &theory_systs) {
        this->m_theory_systs = theory_systs;
        return *this;
    }

    RegionBuilder &setSUSYTools(std::shared_ptr<custom_SUSYObjDef_xAOD> SUSYTools) {
        this->m_SUSYTools = SUSYTools;
        return *this;
    }

    std::unique_ptr<T> build() {
        return std::unique_ptr<T>(new T(this->m_prefix, this->m_outputFile, this->m_store, this->m_event, this->m_wk,
                                        this->m_systInfoList, this->m_theory_systs, this->m_SUSYTools));
    }

    /**
     * @short private member variables
     */
  private:
    std::string m_prefix;
    TFile *m_outputFile;
    xAOD::TStore *m_store;
    xAOD::TEvent *m_event;
    // xTools *tools;
    // xCPTools *cp_tools;
    // xSystematics *systematics;
    std::vector<ST::SystInfo> m_systInfoList;
    // xTriggerMuonList *muon_trigger_sf;
    EL::IWorker *m_wk;
    std::map<std::string, int> m_theory_systs;
    std::shared_ptr<custom_SUSYObjDef_xAOD> m_SUSYTools;
};

#endif
