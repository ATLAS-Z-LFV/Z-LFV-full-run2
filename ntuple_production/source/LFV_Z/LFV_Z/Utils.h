// vim: ts=4 sw=4
#ifndef HAVE_UTILS_H
#define HAVE_UTILS_H

#include <string>
#include <vector>

#include "TVector2.h"
#include "xAODJet/JetContainerInfo.h"
#include "xAODTracking/VertexFwd.h"

#include "PATInterfaces/SystematicSet.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/TActiveStore.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODTracking/Vertex.h"

#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODJet/Jet.h"
#include "xAODMissingET/MissingET.h"
#include "xAODMuon/Muon.h"
#include "xAODTau/TauJet.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "Extrapolate/Track.h"
#include "Extrapolate/Vertex.h"

// assert with message
#define ASSERT_WITH_MESSAGE(condition, message) \
    do {                                        \
        if (!(condition)) {                     \
            printf((message));                  \
        }                                       \
        assert((condition));                    \
    } while (false)

//--------------------------------------------------------------------------
// Utility functions that may require access to the event store
//--------------------------------------------------------------------------

enum class RunPeriod { p7tev, p8tev, p13tev, INVALID };
enum class DerivationTag { NotADerivation, p1872, p2353, p2363, p2372, p2375, p2377, p2384, p2419, p2425, INVALID_Derivation };

namespace Utils {
    typedef struct {
        uint32_t RunNumber;
        unsigned long long EventNumber;
        uint32_t LumiBlockNumber;
        uint32_t mc_channel_number;
    } EventInfo;

    RunPeriod periodFromString(const std::string &period);
    DerivationTag derivationTagFromString(const std::string &tag);
    xAOD::JetInput::Type JetTypeFromString(const std::string &algname);

    const xAOD::EventInfo *GetEventInfo(xAOD::TEvent &event);
    const xAOD::Vertex *GetPrimaryVertex(xAOD::TEvent &event);
    const EventInfo ExtractEventInfo(const xAOD::EventInfo *info, bool isData = true);

    const std::vector<std::string> splitString(const std::string &s, const std::string &delim = ",");

    const std::string getSystematicSuffix(const CP::SystematicSet &systSet);
    CP::SystematicSet *GetCurrentSystematicSet();

}  // namespace Utils

namespace ParticleUtils {
    bool InCrackRegion(const xAOD::Electron *el);
    float GetCharge(const xAOD::IParticle *p);
    int IsHadronicTau(const xAOD::TruthParticle *p);
    std::vector<const xAOD::TruthParticle *> GetTruthTaus(const xAOD::TruthParticleContainer *pc,
                                                          const std::string &type = "Had");
    int FindTrueElectron(const xAOD::TruthParticleContainer *c, const xAOD::IParticle *e);
    int FindTrueMuon(const xAOD::TruthParticleContainer *c, const xAOD::IParticle *m);
    int FindTrueTau(const xAOD::TruthParticleContainer *c, const xAOD::TauJet *tau);
    int FindTrueParticle(const xAOD::TruthParticleContainer *c, const xAOD::IParticle *p, const int absPdgId);

    double d0sig(const xAOD::IParticle *p);
    double d0sig(const xAOD::Electron *el);
    double d0sig(const xAOD::Muon *mu);
    double d0sig(const xAOD::TauJet *tau);

    double z0sinTheta(const xAOD::IParticle *p);
    double z0sinTheta(const xAOD::Electron *el);
    double z0sinTheta(const xAOD::Muon *mu);
    double z0sinTheta(const xAOD::TauJet *tau);

    // distance of closest approach (a0) of a track w.r.t. a vertex
    double doca(const xAOD::TrackParticle *trk, const xAOD::Vertex *vtx, TVector3& beamPos);
    double doca(Vertex *trk, const xAOD::Vertex *vtx, TVector3& beamPos);
}  // namespace ParticeUtils

namespace VertexUtils {
    unsigned short CountVertices(const xAOD::VertexContainer *VtxCont, xAOD::VxType::VertexType vxType, unsigned short minNtrks,
                                 double maxAbsZ);
    unsigned short CountVertices(const xAOD::VertexContainer *VtxCont, unsigned short minNtrks, double maxAbsZ);
}  // namespace VertexUtils

// Counting functions for xAOD containers
namespace ContainerUtils {
    // template <typename T = int> int Count(const xAOD::IParticleContainer *c, const std::string &s = "");
    // template <typename T = int> int Count(const xAOD::IParticleContainer *c, const std::vector<std::string> &v, bool passAll =
    // true);
    // template <typename T = int> int Count(const std::vector<const xAOD::IParticle *> &particles, const std::vector<std::string>
    // &v, bool passAll = true);
    // object counter based on a single criterion
    template <typename T = int>
    int Count(const xAOD::IParticleContainer *c, const std::string &s) {
        if (s == "") return c->size();
        return std::count_if(c->begin(), c->end(),
                             [&](decltype(*c->begin()) p) { return (p->isAvailable<T>(s.c_str()) && p->auxdata<T>(s.c_str())); });
    }

    // object counter to be used with selection
    template <typename T = int>
    int Count(const xAOD::IParticleContainer *c, const std::vector<std::string> &v) {
        int count = 0;
        for (xAOD::IParticleContainer::size_type i = 0; i < c->size(); ++i) {
            bool meetsCriteria = true;
            for (auto itr = begin(v); itr != end(v); ++itr) {
                if (not c->at(i)->isAvailable<T>(itr->c_str()) or not c->at(i)->auxdata<T>(itr->c_str())) {
                    meetsCriteria = false;
                }
            }
            if (meetsCriteria) {
                ++count;
            }
        }

        return count;
    }

    // object counter to be used with selection
    template <typename T = int>
    int Count(const std::vector<const xAOD::IParticle *> &particles, const std::vector<std::string> &v) {
        int count = 0;
        for (auto particle : particles) {
            bool meetsCriteria = true;
            for (auto itr = begin(v); itr != end(v); ++itr) {
                if (not particle->isAvailable<T>(itr->c_str()) or not particle->auxdata<T>(itr->c_str())) {
                    meetsCriteria = false;
                }
            }
            if (meetsCriteria) {
                ++count;
            }
        }

        return count;
    }

    // Will return object matching selection with given pT index (default is leading object).
    template <typename T = int>
    const xAOD::IParticle *GetSelectedParticle(const xAOD::IParticleContainer *c, const std::vector<std::string> &v, int pTidx) {
        // integer overloaded with number of particles meeting criteria, used for matching to specific pT-indexed particle
        int nGood(0);
        for (xAOD::IParticleContainer::size_type i = 0; i < c->size(); ++i) {
            bool meetsCriteria = true;
            for (auto itr = begin(v); itr != end(v); ++itr) {
                if (not c->at(i)->isAvailable<T>(itr->c_str()) or !c->at(i)->auxdata<T>(itr->c_str())) {
                    meetsCriteria = false;
                }
            }
            if (!meetsCriteria) continue;

            if (nGood == pTidx) {
                return c->at(i);
            } else {
                nGood++;
            }
        }

        return 0;  // shouldn't reach here if your selection is not tighter than it should be
    }

    unsigned int Count(const xAOD::IParticleContainer *c, const double pT);

    void PtSort(xAOD::IParticleContainer *c);
    unsigned short Size(const xAOD::IParticleContainer *c);
    bool HasAtLeast(const xAOD::IParticleContainer *c, unsigned int n);
    bool IsEmpty(const xAOD::IParticleContainer *c);
    bool IsNonEmpty(const xAOD::IParticleContainer *c);

}  // namespace ContainerUtils

namespace KinematicUtils {
    double DeltaPhi(const TLorentzVector &v, const TLorentzVector &u);
    double DeltaPhi(const xAOD::IParticle *p1, const xAOD::IParticle *p2);
    double DeltaPhi(const xAOD::IParticleContainer *c1, const xAOD::IParticleContainer *c2);
    double DeltaPhi(const xAOD::IParticleContainer *c1, unsigned int i, const xAOD::IParticleContainer *c2, unsigned int j);
    double DeltaPhi(const xAOD::IParticleContainer *c);
    double DeltaPhi(const xAOD::IParticle *p, const xAOD::MissingET *met);
    double DeltaPhi(const xAOD::IParticleContainer *c, unsigned int i, const xAOD::MissingET *met);
    double DeltaPhi(const xAOD::IParticleContainer *c, const xAOD::MissingET *met);
    double DeltaPhi(double phi1, double phi2);
    double CosDeltaPhi(double phi1, double phi2);
    double CosDeltaPhi(const xAOD::IParticleContainer *c1, const xAOD::IParticleContainer *c2);

    double DeltaEta(const TLorentzVector &v, const TLorentzVector &u);
    double DeltaEta(const xAOD::IParticle *p1, const xAOD::IParticle *p2);
    double DeltaEta(const xAOD::IParticleContainer *c, unsigned int i, unsigned int j);
    double DeltaEta(const xAOD::IParticleContainer *c);
    double DeltaEta(const xAOD::IParticleContainer *c1, const xAOD::IParticleContainer *c2);
    double DeltaEta(const xAOD::IParticleContainer *c1, unsigned int i, const xAOD::IParticleContainer *c2, unsigned int j);
    double DeltaEta(double eta1, double eta2);

    double DeltaR(const TLorentzVector &v, const TLorentzVector &u);
    double DeltaR(const xAOD::IParticleContainer *c, unsigned int i, unsigned int j);
    double DeltaR(const xAOD::IParticleContainer *c);
    double DeltaR(const xAOD::IParticle *p1, const xAOD::IParticle *p2);
    double DeltaR(const xAOD::IParticleContainer *c1, const xAOD::IParticleContainer *c2);
    double DeltaR(const xAOD::IParticleContainer *c1, unsigned int i, const xAOD::IParticleContainer *c2, unsigned int j);

    double mT(const xAOD::IParticle *p, const xAOD::MissingET *met);
    double mT(const xAOD::IParticle *p1, const xAOD::IParticle *p2, const xAOD::MissingET *met);
    double mT(const xAOD::IParticleContainer *c, const xAOD::MissingET *met);
    double mT(const xAOD::IParticleContainer *c, unsigned int i, const xAOD::MissingET *met);
    double mT(const TLorentzVector &p, const xAOD::MissingET *met);

    double VisibleMass(const xAOD::IParticle *p1, const xAOD::IParticle *p2);
    double VisibleMass(const xAOD::IParticle *p1, const xAOD::IParticle *p2, const xAOD::IParticle *p3);
    double VisibleMass(const xAOD::IParticleContainer *c1, const xAOD::IParticleContainer *c2);
    double VisibleMass(const xAOD::IParticleContainer *c1, unsigned int, const xAOD::IParticleContainer *c2, unsigned int);
    double VisibleMass(const xAOD::IParticleContainer *c);

    double CollinearMass(const xAOD::TauJet *tau, const xAOD::IParticle *lep, const xAOD::MissingET *met);
    double ColinearMass(const xAOD::TauJet *tau, const xAOD::IParticle *lep, const xAOD::MissingET *met);

    double CollinearMass(const xAOD::TauJet *tau, const xAOD::IParticle *lep, const xAOD::Photon *photon,
                         const xAOD::MissingET *met);
    double CollinearMass(const xAOD::IParticle *lep_0, const xAOD::IParticle *lep_1, const xAOD::MissingET *met);
    double CollinearMass(const xAOD::IParticle *lep_0, const xAOD::IParticle *lep_1, const xAOD::Photon *photon,
                         const xAOD::MissingET *met);

    double InvariantMass(std::initializer_list<const xAOD::IParticle *> l);

    double Mass(std::initializer_list<TLorentzVector> l);
    double InvariantMass(std::initializer_list<TLorentzVector> l);

    double Alpha1(const xAOD::TauJet *tau, const xAOD::IParticle *p);
    double Alpha2(const xAOD::TauJet *tau, const xAOD::IParticle *p);
    double DeltaAlpha(const xAOD::TauJet *tau, const xAOD::IParticle *p);

    double Alpha1(const xAOD::IParticle *lep1, const xAOD::IParticle *lep2);
    double Alpha2(const xAOD::IParticle *lep1, const xAOD::IParticle *lep2);
    double DeltaAlpha(const xAOD::IParticle *lep1, const xAOD::IParticle *lep2);
    
    TLorentzVector TruthVisibleP4(const xAOD::TruthParticle* p);
    const xAOD::TruthParticle* RadiatedTruthParticle(const xAOD::TruthParticle* p);

}  // namespace KinematicUtils

#endif
