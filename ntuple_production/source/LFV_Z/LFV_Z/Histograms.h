// vim: ts=4 sw=4
/** Histogram holder
 *
 *  Forked from xTauFW's xHistogram class.
 */

#ifndef HAVE_HISTOGRAMS_H
#define HAVE_HISTOGRAMS_H

#include <TH1D.h>
#include <TH2D.h>
#include "Output.h"

class Histograms {
  public:
    Output<TH1D> metadata;              //!< histogram of events and weights for (D)xAOD datasets
    Output<TH1D> monHist_muon_pt;       //!< monitoring histogram: muon pT
    Output<TH1D> monHist_ele_pt;        //!< monitoring histogram: electron pT
    Output<TH1D> monHist_tau_pt;        //!< monitoring histogram: tau pT
    Output<TH1D> monHist_jet_pt;        //!< monitoring histogram: jet pT
    Output<TH1D> monHist_ETmiss;        //!< monitoring histogram: calibrated ETmiss
    Output<TH1D> monHist_track_ETmiss;  //!< monitoring histogram: calibrated track ETmiss

    Output<TH2D> monHist_muon_pt_eta;  //!< monitoring histogram: muon pT vs. eta
    Output<TH2D> monHist_ele_pt_eta;   //!< monitoring histogram: electron pT vs. eta
    Output<TH2D> monHist_tau_pt_eta;   //!< monitoring histogram: tau pT vs. eta
    Output<TH2D> monHist_jet_pt_eta;   //!< monitoring histogram: jet pT vs. eta

    Output<TH1D> monHist_ETmiss_phi;        //!< monitoring histogram: calibrated ETmiss
    Output<TH1D> monHist_track_ETmiss_phi;  //!< monitoring histogram: calibrated track ETmiss
    Output<TH2D> monHist_muon_eta_phi;      //!< monitoring histogram: muon phi vs. eta
    Output<TH2D> monHist_ele_eta_phi;       //!< monitoring histogram: electron phi vs. eta
    Output<TH2D> monHist_tau_eta_phi;       //!< monitoring histogram: tau phi vs. eta
    Output<TH2D> monHist_jet_eta_phi;       //!< monitoring histogram: jet phi vs. eta

    Output<TH1D> monHist_muon_no_OLR_pt;  //!< monitoring histogram: muon pT without overlap removal
    Output<TH1D> monHist_ele_no_OLR_pt;   //!< monitoring histogram: electron pT without overlap removal
    Output<TH1D> monHist_tau_no_OLR_pt;   //!< monitoring histogram: tau pT without overlap removal
    Output<TH1D> monHist_jet_no_OLR_pt;   //!< monitoring histogram: jet pT without overlap removal

    Output<TH1D> cutflow_common;  //!< event selection cutflow histogram - preselection
    Output<TH1D> cutflow_LFVLep;  //!< event selection cutflow histogram - HNL
    Output<TH1D> cutflow_LFV;     //!< event selection cutflow histogram - LFV

    /** @brief: getInstance method.
     *  Returns the only existing instance of this class
     */
    static Histograms& getInstance() {
        static Histograms instance;
        return instance;
    }

    /** @brief: set directory of all histograms (objects)
     *  This method should only be called once in the initializaton of your event loop
     */
    EL::StatusCode set_directory(TDirectory* file) {
        // this loops over all the pointers stored by the Output<T> classes and registers them in the output stream
        ObjList::getInstance().set_directory(file);

        // all OK.
        return EL::StatusCode::SUCCESS;
    }

    /** @brief: write all histograms (objects) and delete them
     *  This method should only be called once in the finalization of your event loop
     */
    EL::StatusCode write(TDirectory* file) {
        // this loops over all the pointers stored by the Output<T> classes and write and delete them
        ObjList::getInstance().write(file);

        // all OK.
        return EL::StatusCode::SUCCESS;
    }

    // const std::vector<CP::SystematicSet>::const_iterator& currentSystematic() { return m_sysListItr; }
    // void setCurrentSystematic(const std::vector<CP::SystematicSet>::const_iterator& itr) { m_sysListItr = itr; }

    const ST::SystInfo& currentSystematic() {
        return m_systematic;
    }
    void setCurrentSystematic(const ST::SystInfo& s) {
        m_systematic = s;
    }

  private:
    /** Disabled constructors
     */
    Histograms(){};
    Histograms(Histograms const&) = delete;
    void operator=(Histograms const&) = delete;

    // EL::Worker* m_worker;
    ST::SystInfo m_systematic;
    // std::vector<CP::SystematicSet>::const_iterator m_sysListItr;
};

#endif
