#ifndef HAVE_VARGROUPS_H
#define HAVE_VARGROUPS_H

#include "LFV_Z/Enums.h"
#include "LFV_Z/Var.h"
#include "LFV_Z/VarGroup.h"
#include "xAODEventInfo/EventInfo.h"

#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "DiTauMassTools/MissingMassCalculator.h"

//#include "SUSYTools/ISUSYObjDef_xAODTool.h"
//#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "LFV_Z/SUSYTools.h"

/**
 * @brief: The FillEventInfo struct
 * Fills the basic event information
 */
struct FillEventInfo : public VarGroup {
    FillEventInfo(VarList& varList);  //!< ctor

    /** @brief: output variables */
    // common
    VarUL eventNumber{vlist, "eventNumber"};  //!< event number
    VarUI runNumber{vlist, "runNumber"};      //!< run number

    // data
    VarUI lbNumber{vlist, "lbNumber"};                //!< lumi number
    VarUI grl_pass_run_lb{vlist, "grl_pass_run_lb"};  //!< GRL decision
    // std::unique_ptr<VarUI> lbNumber;         //!< lumi number
    // std::unique_ptr<VarUI> grl_pass_run_lb;  //!< GRL decision

    //// MC
    VarUI treatAsYear{vlist, "treatAsYear"};            //<! treat as what year?
    VarUI random_runNumber{vlist, "random_runNumber"};  //!< random run number
    VarUI random_lbNumber{vlist, "random_lbNumber"};    //!< ransom lumi number
    VarUL PRWHash{vlist, "PRWhash"};                    //!< PRWHash branch for offline pileup reweighting
    // std::unique_ptr<VarUI> random_runNumber;  //!< random run number
    // std::unique_ptr<VarUI> random_lbNumber;   //!< ransom lumi number
    // std::unique_ptr<VarUL> PRWHash;           //!< PRWHash branch for offline pileup reweighting

    /** @brief: MAIN fill method */
    void fill(const xAOD::EventInfo* eventInfo);
};

//-------------------------------------------------------------------
/**
 * @brief: The FillEventWeights struct
 * Fills the MC event weights
 */
struct FillEventWeights : public VarGroup {
    FillEventWeights(VarList& varList);  //!< ctor

    /** @brief: output variables */
    // VarD pileupWeight{vlist, "pileupWeight"};  //!< pileup weight, not filled at the moment
    VarD mcWeight{vlist, "mcWeight"};          //!< MC event weight, 1 for data
    VarD sherpaWeight{vlist, "sherpaWeight"};  //!< sherpa 2.2.0 weights
    // VarD totalWeight{vlist, "totalWeight"};    //!< mc weight * pileup weight

    // VarD normWeight{vlist, "normWeight"};      //!< normWeight, default is 1

    // VarD scaleFactor{vlist, "SF"};  //!< total scale factors, 1 for data
    // VarD btagSF{vlist, "btagSF"};   //!< total b-tag scale factors, 1 for data
    // VarD elSF{vlist, "elSF"};       //!< total el scale factors, 1 for data
    // VarD muSF{vlist, "muSF"};       //!< total mu scale factors, 1 for data
    // VarD tauSF{vlist, "tauSF"};     //!< total tau scale factors, 1 for data

    /** @brief: MAIN fill method */
    void fill(const xAOD::EventInfo* eventInfo);
};
//-------------------------------------------------------------------
/**
 * @brief: The FillMultiplicities struct
 * Fills the selected object multiplicities
 */
struct FillMultiplicities : public VarGroup {
    FillMultiplicities(VarList& varList) : VarGroup(varList, "") {}

    /** @brief: output variables */
    VarUS nMuons{vlist, "nMuons"};                             //!< number of selected muons
    VarUS nElectrons{vlist, "nElectrons"};                     //!< number of selected electrons
    VarUS nPhotons{vlist, "nPhotons"};                         //!< number of selected photons
    VarUS nTaus{vlist, "nTaus"};                               //!< number of selected taus
    VarUS nTaus_loose{vlist, "nTaus_loose"};                   //!< number of selected loose taus
    VarUS nTaus_medium{vlist, "nTaus_medium"};                 //!< number of selected medium taus
    VarUS nTaus_tight{vlist, "nTaus_tight"};                   //!< number of selected tight taus
    VarUS nJets{vlist, "nJets"};                               //!< number of selected jets
    VarUS nJets_bad{vlist, "nJets_bad"};                       //!< number of selected bad jets
    VarUS nBjets{vlist, "nbJets"};                             //!< number of selected b-jets
    VarUS nBjetsLoose{vlist, "nbJets_loose"};                  //!< number of selected loose b-jets
    VarUS nVertices{vlist, "nVertices"};                       //!< number of vertices
    VarUS nPrimaryVertices{vlist, "nPrimaryVertices"};         //!< number of primary vertices
    VarF nActualInteractions{vlist, "nActualInteractions"};    //!< actual number of pp interactions
    VarF nAverageInteractions{vlist, "nAverageInteractions"};  //!< average number of pp interactions
    VarF nAverageInteractionsCorrected{
        vlist,
        "nAverageInteractionsCorrected"};  //!< average number of pp interactions, which is corrected by pileup reweighting tool.

    VarUS nSignalMuons{vlist, "nSignalMuons"};          //!< number of signal muons
    VarUS nSignalElectrons{vlist, "nSignalElectrons"};  //!< number of signal electrons
    VarUS nSignalPhotons{vlist, "nSignalPhotons"};      //!< number of signal photons
    VarUS nSignalTaus{vlist, "nSignalTaus"};            //!< number of signal taus

    VarUS nNonIsolatedSignalMuons{vlist, "nNonIsolatedSignalMuons"};          //!< number of QCD muons
    VarUS nNonIsolatedSignalElectrons{vlist, "nNonIsolatedSignalElectrons"};  //!< number of QCD electrons

    /** @brief: MAIN fill method */
    void fill(const xAOD::EventInfo* eventInfo, const xAOD::MuonContainer* muons, const xAOD::ElectronContainer* electrons,
              const xAOD::PhotonContainer* photons, const xAOD::TauJetContainer* taus, const xAOD::JetContainer* jets,
              const xAOD::VertexContainer* vertices);
};

//-------------------------------------------------------------------
/**
 * @brief: The FillParticle struct
 * Fills the basic variables for objects derifed from the IParticle
 */
struct FillVertex : public VarGroup {
    FillVertex(VarList& varList, std::string gName = "vertex") : VarGroup(varList, gName) {}

    /** @brief: output variables */
    VarI vertex{vlist, group};

    VarF x{vlist, "x", group};  //!< x position
    VarF y{vlist, "y", group};  //!< y position
    VarF z{vlist, "z", group};  //!< z position

    /** @brief: MAIN fill method */
    void fill(const TVector3& v, const TVector3& beamPos);
    void fill(const xAOD::Vertex* v, const TVector3& beamPos);
    void fill(const xAOD::TruthVertex* v, const TVector3& beamPos);
};

//-------------------------------------------------------------------
/**
 * @brief: The FillParticle struct
 * Fills the basic variables for objects derifed from the IParticle
 */
struct FillParticle : public VarGroup {
    FillParticle(VarList& varList, std::string gName = "particle") : VarGroup(varList, gName) {}
    virtual ~FillParticle() {}

    /** @brief: output variables */
    VarUS particle{vlist, group};  //!< 0 = not filled, 1 = is muon, 2 = is electron, 3 = is tau, 4 = is jet, 5 = is track
    //! particle, 6 = is truth particle, 7 = unkown
    VarF eta{vlist, "eta", group};  //!< pseudorapidity
    VarF phi{vlist, "phi", group};  //!< azimuthal angle
    VarF pt{vlist, "pt", group};    //!< transverse momentum
    VarF et{vlist, "et", group};    //!< transverse energy
    VarF m{vlist, "m", group};      //!< mass
    VarF q{vlist, "q", group};      //!< electric charge

    VarS origin{vlist, "origin", group};
    VarS type{vlist, "type", group};
    // VarI test{vlist, "test", group};

    /** @brief: MAIN fill method */
    virtual void fill(const xAOD::IParticle* p);

    /** @brief: OVERLOADED fill methods */
    void fill(const xAOD::IParticleContainer* c, size_t i);
    void fill(const xAOD::IParticleContainer* c);
};
//-------------------------------------------------------------------
/**
 * @brief: The FillFourVector struct
 * Fills information on a TLorenzVector
 */
struct FillFourVector : public VarGroup {
    FillFourVector(VarList& varList, std::string gName = "vector") : VarGroup(varList, gName) {}
    virtual ~FillFourVector() {}

    /** @brief: output variables */
    VarF eta{vlist, "eta", group};  //!< pseudorapidity
    VarF phi{vlist, "phi", group};  //!< azimuthal angle
    VarF pt{vlist, "pt", group};    //!< transverse momentum
    VarF e{vlist, "e", group};      //!< energy

    /** @brief: MAIN fill method */
    virtual void fill(const TLorentzVector& v);
};
//-------------------------------------------------------------------
//-------------------------------------------------------------------
/**
 * @brief: The FillBasicPhoton struct
 */
struct FillBasicPhoton : public VarGroup {
    FillBasicPhoton(VarList& varList, std::string gName = "photon") : VarGroup(varList, gName) {}
    virtual ~FillBasicPhoton() {}

    /** @brief: output variables */
    VarF eta{vlist, "eta", group};        //!< pseudorapidity
    VarF phi{vlist, "phi", group};        //!< azimuthal angle
    VarF et{vlist, "et", group};          //!< transverse energy
    VarB signal{vlist, "signal", group};  //!< signal flag

    /** @brief: MAIN fill method */
    virtual void fill(const xAOD::IParticle* p);

    /** @brief: OVERLOADED fill methods */
    void fill(const xAOD::IParticleContainer* c, size_t i);
    void fill(const xAOD::IParticleContainer* c);
};

//-------------------------------------------------------------------
struct FillMatchedParticle : public FillParticle {
    FillMatchedParticle(VarList& varList, std::string gName = "part_matched") : FillParticle(varList, gName) {}
    virtual ~FillMatchedParticle() {}

    VarI pdgId{vlist, "pdgId", group};                //!< pdg ID code of the matched particle
    VarI mother_pdgId{vlist, "mother_pdgId", group};  //!< pdg ID code of its mother
    VarB isHad{vlist, "isHad", group};                //!< is hadronic tau?
    VarUI origin{vlist, "origin", group};             //!< MCTruthClassifier origin (see
    //! https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/MCTruthClassifier/trunk/MCTruthClassifier/MCTruthClassifierDefs.h)
    VarI type{vlist, "type", group};  //!< MCTruthClassifier type (see
    //! https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/MCTruthClassifier/trunk/MCTruthClassifier/MCTruthClassifierDefs.h)

    virtual void fill(const xAOD::IParticle* p, const xAOD::IParticleContainer* pc, int truthIdx);
    /** @brief: OVERLOADED fill methods */
    virtual void fill(const xAOD::IParticle* p, const xAOD::IParticleContainer* pc);

  protected:
    // hide the fill methods from FillParticle
    using FillParticle::fill;
};
//-------------------------------------------------------------------
struct FillTruthJet : public VarGroup {
    FillTruthJet(VarList& varList, std::string gName = "antikt4truthjet") : VarGroup(varList, gName) {}

    VarF pt{vlist, "pt", group};
    VarF eta{vlist, "eta", group};
    VarF phi{vlist, "phi", group};
    VarF m{vlist, "m", group};

    /** @brief: MAIN fill method */
    void fill(const xAOD::Jet* p);
};
//-------------------------------------------------------------------
struct FillTruthParticle : public FillParticle {
    FillTruthParticle(VarList& varList, std::string gName = "part_truth") : FillParticle(varList, gName) {}
    virtual ~FillTruthParticle() {}

    VarI pdgId{vlist, "pdgId", group};
    VarI mother_pdgId{vlist, "mother_pdgId", group};
    VarI origin{vlist, "origin", group};
    VarI type{vlist, "type", group};

    /** @brief: MAIN fill method */
    virtual void fill(const xAOD::TruthParticle* p);

  protected:
    // hide the fill methods from FillParticle
    using FillParticle::fill;
};
//-------------------------------------------------------------------
struct FillTruthLepton : public FillTruthParticle {
    FillTruthLepton(VarList& varList, std::string gName = "lepton_truth") : FillTruthParticle(varList, gName) {}
    virtual ~FillTruthLepton() {}

    VarF pt_dressed{vlist, "pt_dressed", group};
    VarF e_dressed{vlist, "e_dressed", group};
    VarF eta_dressed{vlist, "eta_dressed", group};
    VarF phi_dressed{vlist, "phi_dressed", group};
    VarI classifierParticleOrigin{vlist, "classifierParticleOrigin", group};
    VarI classifierParticleType{vlist, "classifierParticleType", group};

    // just give the reco particle
    // optionally provide your own truth particle in case no link is available
    // this then reverts to xTruthParticle::fill(ctp);
    virtual void fill(const xAOD::IParticle* p, const xAOD::TruthParticle* ctp = 0);

    // if you do your own matching
    // probably will only fill FillTruthParticle info
    virtual void fill(const xAOD::TruthParticle* p);

  protected:
    // hide the fill methods from FillTruthParticle
    using FillTruthParticle::fill;
};
//-------------------------------------------------------------------
struct FillTruthMuon : public FillTruthLepton {
    FillTruthMuon(VarList& varList, std::string gName = "muon_truth") : FillTruthLepton(varList, gName) {}
    virtual ~FillTruthMuon() {}
    VarB isIsoMuon{vlist, "isIsoMuon", group};

    // just give the reco particle
    // optionally provide your own truth particle in case no link is available
    // this then reverts to xTruthParticle::fill(ctp);
    virtual void fill(const xAOD::IParticle* p, const xAOD::TruthParticle* ctp = 0);

    // if you do your own matching
    // probably will only fill FillTruthParticle info
    virtual void fill(const xAOD::TruthParticle* p);

  protected:
    // hide the fill methods from FillTruthParticle
    using FillTruthParticle::fill;
};
//-------------------------------------------------------------------
struct FillTruthElectron : FillTruthLepton {
    FillTruthElectron(VarList& varList, std::string gName = "electron_truth") : FillTruthLepton(varList, gName) {}
    virtual ~FillTruthElectron() {}
    VarB isIsoEle{vlist, "isIsoEle", group};

    // just give the reco particle
    // optionally provide your own truth particle in case no link is available
    // this then reverts to xTruthParticle::fill(ctp);
    virtual void fill(const xAOD::IParticle* p, const xAOD::TruthParticle* ctp = 0);

    // if you do your own matching
    // probably will only fill FillTruthParticle info
    virtual void fill(const xAOD::TruthParticle* p);

  protected:
    // hide the fill methods from FillTruthParticle
    using FillTruthParticle::fill;
};
//-------------------------------------------------------------------
struct FillTruthTau : public FillTruthParticle {
    FillTruthTau(VarList& varList, std::string gName = "tau_truth") : FillTruthParticle(varList, gName) {}
    virtual ~FillTruthTau() {}

    VarB isTau{vlist, "isTau", group};
    VarB isHadTau{vlist, "isHadTau", group};
    VarB isEle{vlist, "isEle", group};
    VarB isMuon{vlist, "isMuon", group};
    VarB isJet{vlist, "isJet", group};
    VarUI classifierParticleType{vlist, "classifierParticleType", group};
    VarUI classifierParticleOrigin{vlist, "classifierParticleOrigin", group};
    VarF pt_vis{vlist, "pt_vis", group};
    VarF eta_vis{vlist, "eta_vis", group};
    VarF phi_vis{vlist, "phi_vis", group};
    VarF m_vis{vlist, "m_vis", group};
    VarF pt_invis{vlist, "pt_invis", group};
    VarF eta_invis{vlist, "eta_invis", group};
    VarF phi_invis{vlist, "phi_invis", group};
    VarF m_invis{vlist, "m_invis", group};
    VarC decay_mode{vlist, "decay_mode", group, xAOD::TauJetParameters::DecayMode::Mode_Error};
    VarUS n_charged{vlist, "n_charged", group};
    VarUS n_charged_pion{vlist, "n_charged_pion", group};
    VarUS n_neutral{vlist, "n_neutral", group};
    VarUS n_neutral_pion{vlist, "n_neutral_pion", group};

    // FIXME: cgrefe: these decorations from the TauTruthMatchingTool don't work at the moment
    //  VarD  pt_vis_charged   { vlist, "pt_vis_charged"   , group } ;
    //  VarD  eta_vis_charged  { vlist, "eta_vis_charged"  , group } ;
    //  VarD  phi_vis_charged  { vlist, "phi_vis_charged"  , group } ;
    //  VarD  m_vis_charged    { vlist, "m_vis_charged"    , group } ;
    //  VarD  pt_vis_neutral   { vlist, "pt_vis_neutral"   , group } ;
    //  VarD  eta_vis_neutral  { vlist, "eta_vis_neutral"  , group } ;
    //  VarD  phi_vis_neutral  { vlist, "phi_vis_neutral"  , group } ;
    //  VarD  m_vis_neutral    { vlist, "m_vis_neutral"    , group } ;

    /** @brief main fill method
     *  @input p pointer to @c TruthParticle
     */
    virtual void fill(const xAOD::TruthParticle* p);

    /** @brief other fill method
     *  @input p pointer to @c IParticle
     */
    virtual void fill(const xAOD::IParticle* p);

    /** @brief other fill method
     *  @input p pointer to @c Jet
     */
    virtual void fill(const xAOD::Jet* p);

  protected:
    // hide the fill methods from FillTruthParticle
    using FillTruthParticle::fill;
};

//-------------------------------------------------------------------

/**
 * @brief: The FillPhoton struct
 * Fills additional muon or electron related variables
 * Inherits from FillParticle
 */
struct FillPhoton : public FillParticle {
    FillPhoton(VarList& varList, std::string gName = "photon") : FillParticle(varList, gName) {}
    virtual ~FillPhoton() {}

    /** @brief: common isolation  variables */
    VarI iso_wp{vlist, "iso_wp", group};  //!< WPs from IsolationSelectionTool. 0=passes nothing, 1=ISO_LOOSETRACKONLY,
    //! 10=ISO_LOOSE, 100=ISO_TIGHT, 1000=ISO_GRADIENTLOOSE, 10000=ISO_GRADIENT
    VarD etcone20{vlist, "iso_etcone20", group};          //!< isolation etcone20
    VarD etcone30{vlist, "iso_etcone30", group};          //!< isolation etcone30
    VarD etcone40{vlist, "iso_etcone40", group};          //!< isolation etcone40
    VarD topoetcone20{vlist, "iso_topoetcone20", group};  //!< isolation topoetcone20
    VarD topoetcone30{vlist, "iso_topoetcone30", group};  //!< isolation topoetcone30
    VarD topoetcone40{vlist, "iso_topoetcone40", group};  //!< isolation topoetcone40
    VarD ptcone20{vlist, "iso_ptcone20", group};          //!< isolation ptcone20
    VarD ptcone30{vlist, "iso_ptcone30", group};          //!< isolation ptcone30
    VarD ptcone40{vlist, "iso_ptcone40", group};          //!< isolation ptcone40
    VarD ptvarcone20{vlist, "iso_ptvarcone20", group};    //!< isolation ptvarcone20
    VarD ptvarcone30{vlist, "iso_ptvarcone30", group};    //!< isolation ptvarcone30
    VarD ptvarcone40{vlist, "iso_ptvarcone40", group};    //!< isolation ptvarcone40

    /** @short common identification flags
     * @note -1 = N/A, 0 = didn't pass, 1 = passed
     */
    VarB id_loose{vlist, "id_loose", group};    //!< passed loose identification
    VarB id_medium{vlist, "id_medium", group};  //!< passed medium identification
    VarB id_tight{vlist, "id_tight", group};    //!< passed tight identification

    /** @short photon cluster eta */
    VarD clusterEta{vlist, "cluster_eta", group};         //!< photon cluster eta
    VarD clusterEtaBE2{vlist, "cluster_eta_be2", group};  //!< photon cluster eta at BE(2) sampling

    VarB signal{vlist, "signal", group};        //!< signal flag
    VarB isolation{vlist, "isolation", group};  //!< isolation flag
    VarB passORL{vlist, "passORL", group};      //!< ORL flag

    /** @brief: MAIN fill method */
    using FillParticle::fill;
    virtual void fill(const xAOD::Photon* p);
};

//-------------------------------------------------------------------
/**
 * @brief: The FillLepton struct
 * Fills additional muon or electron related variables
 * Inherits from FillParticle
 */
struct FillLepton : public FillParticle {
    FillLepton(VarList& varList, std::string gName = "lep") : FillParticle(varList, gName) {}
    virtual ~FillLepton() {}

    /** @brief: common tracking  variables */

    VarD trk_d0{vlist, "trk_d0", group};                    //!< transverse impact parameter of the track
    VarD trk_d0_sig{vlist, "trk_d0_sig", group};            //!< significance of the transverse impact parameter of the track
    VarD trk_z0{vlist, "trk_z0", group};                    //!< longitudinal impact parameter of the track
    VarD trk_z0_sintheta{vlist, "trk_z0_sintheta", group};  //!< longitudinal impact parameter of the track * sinTheta
    VarD trk_z0_sig{vlist, "trk_z0_sig", group};            //!< significance longitudinal impact parameter of the track
    VarD trk_pvx_z0{vlist, "trk_pvx_z0", group};  //!< longitudinal impact parameter of the track relative to a primary vertex
    VarD trk_pvx_z0_sintheta{vlist, "trk_pvx_z0_sintheta",
                             group};  //!< longitudinal impact parameter of the track relative to a primary vertex * sinTheta
    VarD trk_pvx_z0_sig{vlist, "trk_pvx_z0_sig",
                        group};  //!< significance longitudinal impact parameter of the track  relative to a primary vertex
    VarD trk_pt_error{vlist, "trk_pt_error", group};  //!< track pT uncertainty

    /** @brief: common isolation  variables */
    VarI iso_wp{vlist, "iso_wp", group};  //!< WPs from IsolationSelectionTool. 0=passes nothing, 1=ISO_LOOSETRACKONLY,

    //! 10=ISO_LOOSE, 100=ISO_TIGHT, 1000=ISO_GRADIENTLOOSE, 10000=ISO_GRADIENT
    VarD etcone20{vlist, "iso_etcone20", group};          //!< isolation etcone20
    VarD etcone30{vlist, "iso_etcone30", group};          //!< isolation etcone30
    VarD etcone40{vlist, "iso_etcone40", group};          //!< isolation etcone40
    VarD topoetcone20{vlist, "iso_topoetcone20", group};  //!< isolation topoetcone20
    VarD topoetcone30{vlist, "iso_topoetcone30", group};  //!< isolation topoetcone30
    VarD topoetcone40{vlist, "iso_topoetcone40", group};  //!< isolation topoetcone40
    VarD ptcone20{vlist, "iso_ptcone20", group};          //!< isolation ptcone20
    VarD ptcone30{vlist, "iso_ptcone30", group};          //!< isolation ptcone30
    VarD ptcone40{vlist, "iso_ptcone40", group};          //!< isolation ptcone40
    VarD ptvarcone20{vlist, "iso_ptvarcone20", group};    //!< isolation ptvarcone20
    VarD ptvarcone30{vlist, "iso_ptvarcone30", group};    //!< isolation ptvarcone30
    VarD ptvarcone40{vlist, "iso_ptvarcone40", group};    //!< isolation ptvarcone40

    /** @short common identification flags
     * @note -1 = N/A, 0 = didn't pass, 1 = passed
     */
    VarB id_veryloose{vlist, "id_veryloose", group};  //!< passed very loose identification: muons only
    VarB id_loose{vlist, "id_loose", group};          //!< passed loose identification: muons, electrons
    VarB id_medium{vlist, "id_medium", group};        //!< passed medium identification: muons, electrons
    VarB id_tight{vlist, "id_tight", group};          //!< passed tight identification: muons, electrons
    VarB id_bad{vlist, "id_bad", group};              //!< passed bad identification: muons only

    /** @short muon type */
    VarI muonType{vlist, "muonType", group};  //!< muonType

    /** @short electron cluster eta */
    VarD clusterEta{vlist, "cluster_eta", group};         //!< electron cluster eta
    VarD clusterEtaBE2{vlist, "cluster_eta_be2", group};  //!< electron cluster eta at BE(2) sampling

    VarB signal{vlist, "signal", group};        //!< signal flag
    VarB isolation{vlist, "isolation", group};  //!< isolation flag
    VarB passORL{vlist, "passORL", group};      //!< ORL flag

    /** @brief: MAIN fill method */
    using FillParticle::fill;
    virtual void fill(const xAOD::IParticle* p);
};

//-------------------------------------------------------------------
struct FillJet : public FillParticle {
    FillJet(VarList& varList, std::string gName = "jet") : FillParticle(varList, gName) {}
    virtual ~FillJet() {}

    /** @brief: additional output variables
     * @note JVT: http://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2014-018/
     * http://acode-browser.usatlas.bnl.gov/lxr/source/atlas/Reconstruction/Jet/JetMomentTools/JetMomentTools/JetVertexTaggerTool.h
     * JVF:
     * http://acode-browser.usatlas.bnl.gov/lxr/source/atlas/Reconstruction/Jet/JetMomentTools/JetMomentTools/JetVertexFractionTool.h
     */

    VarD jvf{vlist, "jvf", group};  //!< jet vertex fraction
    // VarD jvfloose         { vlist , "jvfloose"         , group};  //!<
    // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/Run2JetMoments
    VarF jvt{vlist, "jvt", group};                  //!< https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JetVertexTagger
    VarD mvx{vlist, "mvx", group};                  //!< b-tagger?
    VarI mvx_tagged{vlist, "mvx_tagged", group};    //!< b-tagged
    VarI flavorlabel{vlist, "flavorlabel", group};  //!< heavy flavor label
    VarI flavorlabel_part{vlist, "flavorlabel_part", group};  //!< auxiliary flavor label
    VarI flavorlabel_cone{vlist, "flavorlabel_cone", group};  //!< auxiliary flavor label

    /** @brief: MAIN fill method */
    virtual void fill(const xAOD::Jet* p);

    /** @brief: OVERLOADED fill methods */
    virtual void fill(const xAOD::JetContainer* pc, size_t i);
    virtual void fill(const xAOD::JetContainer* pc);

  protected:
    // hide the fill methods from FillParticle
    using FillParticle::fill;
};
//-------------------------------------------------------------------
/**
 * @brief: The FillBasicTau struct
 * Fills additional tau related variables
 * Inherits from FillParticle
 */

struct FillBasicTau : public FillParticle {
    FillBasicTau(VarList& varList, std::string gName = "tau") : FillParticle(varList, gName) {}
    virtual ~FillBasicTau() {}

    VarUI n_tracks{vlist, "n_tracks", group};            //!< number of core tracks (prongs)
    VarUI n_wide_tracks{vlist, "n_wide_tracks", group};  //!< number of tracks in a wider cone

    VarB jet_bdt_veryloose{vlist, "jet_bdt_veryloose", group};             //!< jet BDT ID flag - very loose working point
    VarB jet_bdt_loose{vlist, "jet_bdt_loose", group};             //!< jet BDT ID flag - loose working point
    VarB jet_bdt_medium{vlist, "jet_bdt_medium", group};           //!< jet BDT ID flag - medium working point
    VarB jet_bdt_tight{vlist, "jet_bdt_tight", group};             //!< jet BDT ID flag - tight working point
    VarF jet_bdt_score{vlist, "jet_bdt_score", group};              //!< jet BDT ID score
    VarF jet_bdt_score_trans{vlist, "jet_bdt_score_trans", group};  //!< jet BDT ID score (transformed)

    VarB jet_rnn_veryloose{vlist, "jet_rnn_veryloose", group};             //!< jet RNN ID flag - very loose working point
    VarB jet_rnn_loose{vlist, "jet_rnn_loose", group};             //!< jet RNN ID flag - loose working point
    VarB jet_rnn_medium{vlist, "jet_rnn_medium", group};           //!< jet RNN ID flag - medium working point
    VarB jet_rnn_tight{vlist, "jet_rnn_tight", group};             //!< jet RNN ID flag - tight working point
    VarF jet_rnn_score{vlist, "jet_rnn_score", group};              //!< jet RNN ID score
    VarF jet_rnn_score_trans{vlist, "jet_rnn_score_trans", group};  //!< jet RNN ID score (transformed)

    VarB ele_bdt_loose{vlist, "ele_bdt_loose", group};    //!< ele BDT ID flag - loose working point
    VarB ele_bdt_medium{vlist, "ele_bdt_medium", group};  //!< ele BDT ID flag - medium working point
    VarB ele_bdt_tight{vlist, "ele_bdt_tight", group};    //!< ele BDT ID flag - tight working point
    VarF ele_bdt_score{vlist, "ele_bdt_score", group};            //!< ele BDT ID score
    VarF ele_bdt_score_trans{vlist, "ele_bdt_score_trans", group};  //!< ele BDT ID score (transformed)
    
    VarC decay_mode{vlist, "decay_mode", group,
                    xAOD::TauJetParameters::DecayMode::Mode_Error};  //!< tau decay classification / decay mode

    VarF d0{vlist, "d0", group};                    //!< transverse impact parameter of the track
    VarF delta_z0{vlist, "delta_z0", group};        //!< longitudinal impact parameter of the track relative to a primary vertex
    VarF d0_sig{vlist, "d0_sig", group};            //!< significance of the transverse impact parameter of the track
    VarF z0_sintheta{vlist, "z0_sintheta", group};  //!< longitudinal impact parameter of the track * sinTheta
    VarF pvx_z0_sintheta{vlist, "pvx_z0_sintheta",
                         group};      //!< longitudinal impact parameter of the track * sinTheta relative to a primary vertex
    VarF a0PV{vlist, "a0PV", group};  //!< distance of closest approach of the track relative to a primary vertex

    VarB signal{vlist, "signal", group};  //!< signal flag

    /** @brief: MAIN fill method */
    using FillParticle::fill;
    virtual void fill(const xAOD::TauJet* tau);
};

//-------------------------------------------------------------------
/**
 * @brief: The FillTau struct
 * Fills even more additional tau related variables
 * Inherits from FillBasicTau
 */
struct FillTau : public FillBasicTau {
    FillTau(VarList& varList, std::string gName = "tau") : FillBasicTau(varList, gName) {}
    virtual ~FillTau() {}

    /**
     * @brief additional output variables for tau objects
     * @sa RootCoreBin/include/xAODTau/TauDefs.h
     */

    // VarI isTruthLepMathched         { vlist, "isTruthLepMatched",      group }; //!< if a match to a truth lepton was found
    // ignoring possible matches to a truth jet
    //VarD jet_bdt_score{vlist, "jet_bdt_score", group};          //!< Jet BDT ID score
    //VarB ele_bdt_loose{vlist, "ele_bdt_loose", group};         //!< Electron BDT ID flag - loose working point
    //VarB ele_bdt_medium{vlist, "ele_bdt_medium", group};       //!< Electron BDT ID flag - medium working point
    //VarB ele_bdt_tight{vlist, "ele_bdt_tight", group};         //!< Electron BDT ID flag - tight working point
    //VarD ele_bdt_score{vlist, "ele_bdt_score", group};          //!< Electron BDT ID score
    VarD ele_bdt_eff_sf{vlist, "ele_bdt_eff_sf", group};        //!< Electron BDT ID efficiency scale factor
    VarD ele_match_lhscore{vlist, "ele_match_lhscore", group};  //!< Electron BDT ID efficiency scale factor
    VarB ele_olr_pass{vlist, "ele_olr_pass", group};            //!< Electron OLR flag
    //VarC decay_mode{vlist, "decay_mode", group,
    //                xAOD::TauJetParameters::DecayMode::Mode_Error};  //!< tau decay classification / decay mode
    VarD leadTrk_pt{vlist, "leadTrk_pt", group};                      //!< tau lead track pt
    VarD leadTrk_eta{vlist, "leadTrk_eta", group};                    //!< tau lead track eta
    VarD leadTrk_phi{vlist, "leadTrk_phi", group};                    //!< tau lead track phi
    VarD mvx{vlist, "mvx", group};                                    //!< b-tagger?
    VarI mvx_tagged{vlist, "mvx_tagged", group};                      //!< b-tagged

    VarB signal{vlist, "signal", group};    //!< signal flag
    VarB passORL{vlist, "passORL", group};  //!< ORL flag

    /** @brief: MAIN fill method */
    using FillBasicTau::fill;
    virtual void fill(const xAOD::TauJet* tau);
};
//-------------------------------------------------------------------
/**
 * @brief: leptonic tau decay truth match
 * Match leptonically decayed taus to the reco leptons (e or mu). The method uses tau_truth container produced by the
 * TauTruthMatchingTool.
 * The actual matching is done using dR<0.2 requirement between the true lepton from tau and the reco. lepton.
 */
struct FillMatchedTruthLeptonicTauDecay : public FillTruthParticle {
    FillMatchedTruthLeptonicTauDecay(VarList& varList, std::string gName = "part_matched") : FillTruthParticle(varList, gName) {}
    virtual ~FillMatchedTruthLeptonicTauDecay() {}

    /** @brief: output variables */
    VarF pt_vis{vlist, "pt_vis", group};    //!< visible [e or mu (+gamma)] pT
    VarF eta_vis{vlist, "eta_vis", group};  //!< visible [e or mu (+gamma)] eta
    VarF phi_vis{vlist, "phi_vis", group};  //!< visible [e or mu (+gamma)] phi
    VarF m_vis{vlist, "m_vis", group};      //!< visible [e or mu (+gamma)] mass

    VarF pt_invis{vlist, "pt_invis", group};    //!< pT   of the neutrinos
    VarF eta_invis{vlist, "eta_invis", group};  //!< eta  of the neutrinos
    VarF phi_invis{vlist, "phi_invis", group};  //!< phi  of the neutrinos
    VarF m_invis{vlist, "m_invis", group};      //!< invariant mass of the neutrinos

    /** @brief: fill methods */
    using FillTruthParticle::fill;
    virtual void fill(const xAOD::IParticle* p, const xAOD::TruthParticleContainer* truth_taus);
};

//-------------------------------------------------------------------
/**
 * @brief The FillTriggerMatch struct
 * Fills the trigger matches to a reconstructed object based on a list of trigger objects
 * This is a general class which takes list of trigger signatures as a parameter of its constructor
 * Matching information is retrieved from decorated attributes
 */
struct FillTriggerMatch : public VarGroup {
    /** @brief: constructor */
    FillTriggerMatch(VarList& varList, std::string gName = "trigger_match", const std::vector<std::string>& triggerItems = {});

    /** @brief: destructor */
    ~FillTriggerMatch() = default;

  public:
    /** @brief: MAIN fill method */
    void fill(const xAOD::IParticle* p);

  private:
    std::map<std::string, std::unique_ptr<VarUI>> m_trigger_match_map;
};
//-------------------------------------------------------------------
/**
 * @brief: The FillTriggers struct
 * Fills the trigger information
 * This is a general class which takes list of trigger signatures as a parameter of its constructor
 */
struct FillTriggers : public VarGroup {
    /** @brief: constructor */
    FillTriggers(VarList& varList, std::string trigger_type, std::string gName = "");

    /** @brief: destructor */
    ~FillTriggers();

    /** @brief: output variables (trigger decisions)*/
    std::map<std::string, VarI*> variables;

    /** @brief: MAIN fill methods */
    void fill(const std::map<std::string, bool>& trig_map);
};
/**
 * @brief: The FillMuonTriggers struct
 * Fill dafault muon triggers
 */
struct FillMuonTriggers : public FillTriggers {
    FillMuonTriggers(VarList& varList, std::string gName = "") : FillTriggers(varList, "muon", gName) {}
};
/**
 * @brief: The FillElectronTriggers struct
 * Fill dafault electron triggers
 */
struct FillElectronTriggers : public FillTriggers {
    FillElectronTriggers(VarList& varList, std::string gName = "") : FillTriggers(varList, "electron", gName) {}
};
/**
 * @brief: The FillTauTriggers struct
 * Fill dafault tau triggers
 */
struct FillTauTriggers : public FillTriggers {
    FillTauTriggers(VarList& varList, std::string gName = "") : FillTriggers(varList, "tau", gName) {}
};
/**
 * @brief: The FillJetTriggers struct
 * Fill default jet triggers
 */
struct FillJetTriggers : public FillTriggers {
    FillJetTriggers(VarList& varList, std::string gName = "") : FillTriggers(varList, "jet", gName) {}
};
/**
 * @brief: The FillMETTriggers struct
 * Fill default met triggers
 */
struct FillMETTriggers : public FillTriggers {
    FillMETTriggers(VarList& varList, std::string gName = "") : FillTriggers(varList, "met", gName) {}
};
//-------------------------------------------------------------------
struct FillMET : public VarGroup {
    FillMET(VarList& varList, std::string gName = "met") : VarGroup(varList, gName) {}

    /** @brief: output variables */
    VarD et{vlist, "et", group};                  //!< missing ET
    VarD etx{vlist, "etx", group};                //!< x-component
    VarD ety{vlist, "ety", group};                //!< y-component
    VarD phi{vlist, "phi", group};                //!< azimuthal angle
    VarD sumet{vlist, "sumet", group};            //!< scalar sum ET
    VarD sig{vlist, "sig", group};                //!< significance
    VarD sig_tracks{vlist, "sig_tracks", group};  //!< significance not filled with main fill method

    /** @brief: MAIN fill method */
    void fill(const xAOD::MissingET* pmet);
    /** @brief: overloaded fill method will calculate met_sig_tracks using tracks associated to pmv */
    void fill(const xAOD::MissingET* pmet, const xAOD::Vertex* pmv);
    /** @brief: overloaded fill method will calculate met_sig_tracks using tracks associated to vertex i */
    void fill(const xAOD::MissingET* pmet, const xAOD::VertexContainer*, size_t i = 0);
};
//-------------------------------------------------------------------
struct FillTruthMET : public VarGroup {
    FillTruthMET(VarList& varList, std::string gName = "truth_met") : VarGroup(varList, gName) {}

    /** @brief: output variables */
    VarF et{vlist, "et", group};        //!< missing ET
    VarF etx{vlist, "etx", group};      //!< x-component
    VarF ety{vlist, "ety", group};      //!< y-component
    VarF phi{vlist, "phi", group};      //!< azimuthal angle
    VarF sumet{vlist, "sumet", group};  //!< scalar sum ET
    VarF sig{vlist, "sig", group};      //!< significance

    /** @brief: MAIN fill method */
    void fill(const xAOD::MissingET* m);
};

//-------------------------------------------------------------------
struct FillBasicMMC : public VarGroup {
    FillBasicMMC(VarList& varList, std::string gName) : VarGroup(varList, gName) {}

    // only write out the mass of _one_ method
    VarD mmc_resonance_m{vlist, "resonance_m", group};  //!< mass

    /** @brief: MAIN fill method */
    void fill(const xAOD::EventInfo* peventinfo, const xAOD::IParticle* p0, const xAOD::IParticle* p1,
              const xAOD::JetContainer* pjets, const xAOD::MissingET* pmet);
};

//-------------------------------------------------------------------
struct FillMMC : public VarGroup {
    FillMMC(VarList& varList, std::string gName) : VarGroup(varList, gName) {
        // create three sets of MMC variables
        for (uint i = 0; i < MMCFitMethod::MAX; ++i) {
            std::string methname = MMCFitMethod::shortName[i];

            // convert to lower case
            std::transform(methname.begin(), methname.end(), methname.begin(), ::tolower);
            mmc.push_back(new VarI(vlist, "mmc_" + methname + "", group));
            mmc_fit_status.push_back(new VarI(vlist, "mmc_" + methname + "_fit_status", group));
            mmc_resonance_m.push_back(new VarF(vlist, "mmc_" + methname + "_m", group));

            if (i == MMCFitMethod::MLM) continue;
            // the following is meaningless for MLM method (but then be careful to handle vector of different length
            mmc_resonance_pt.push_back(new VarF(vlist, "mmc_" + methname + "_pt", group));
            mmc_resonance_eta.push_back(new VarF(vlist, "mmc_" + methname + "_eta", group));
            mmc_resonance_phi.push_back(new VarF(vlist, "mmc_" + methname + "_phi", group));
            mmc_met_et.push_back(new VarF(vlist, "mmc_" + methname + "_met_et", group));
            mmc_met_phi.push_back(new VarF(vlist, "mmc_" + methname + "_met_phi", group));
            mmc_x0.push_back(new VarF(vlist, "mmc_" + methname + "_x0", group));
            mmc_x1.push_back(new VarF(vlist, "mmc_" + methname + "_x1", group));
        }
    }

    ~FillMMC();

    /** @brief: output variables (this time defined as vectors) */
    std::vector<VarI*> mmc;
    std::vector<VarI*> mmc_fit_status;
    std::vector<VarF*> mmc_resonance_m;
    std::vector<VarF*> mmc_resonance_pt;
    std::vector<VarF*> mmc_resonance_eta;
    std::vector<VarF*> mmc_resonance_phi;
    std::vector<VarF*> mmc_met_et;
    std::vector<VarF*> mmc_met_phi;
    std::vector<VarF*> mmc_x0;
    std::vector<VarF*> mmc_x1;

    /** @brief: MAIN fill method */
    void fill(const xAOD::EventInfo* peventinfo, const xAOD::IParticle* p0, const xAOD::IParticle* p1,
              const xAOD::JetContainer* pjets, const xAOD::MissingET* pmet);

    /** @brief: OVERLOADED fill methods */
    void fill(const xAOD::EventInfo* peventinfo, const xAOD::IParticleContainer* pc0, size_t i,
              const xAOD::IParticleContainer* pc1, size_t j, const xAOD::JetContainer* pjets,
              const xAOD::MissingET* pmet);  //!< Uses i-th and j-th element from two different collections
    void fill(const xAOD::EventInfo* peventinfo, const xAOD::IParticleContainer* pc0, const xAOD::IParticleContainer* pc1,
              const xAOD::JetContainer* pjets,
              const xAOD::MissingET* pmet);  //!< Uses first elements from the two different collections
    void fill(const xAOD::EventInfo* peventinfo, const xAOD::IParticleContainer* pc, size_t i, size_t j,
              const xAOD::JetContainer* pjets,
              const xAOD::MissingET* pmet);  //!< Uses the i-th and j-th element from the same collection
    void fill(const xAOD::EventInfo* peventinfo, const xAOD::IParticleContainer* pc, const xAOD::JetContainer* pjets,
              const xAOD::MissingET* pmet);  //!< Uses the first and second element from the same collection

    /**
     * @brief: CountMMCJets
     * Internal helper function. Counts jets with pT above threshold defined by m_jo.mmc_njet_min_pt()
     */
    static size_t countMMCJets(const xAOD::JetContainer* p);
};
//-------------------------------------------------------------------
struct FillScaleFactors : public VarGroup {
    /** @brief: constructors
     * Using an @c std::initializer_list for the @c ObjectType to allow cases, for example,  when the input
     * @c IParticle can be either an @c xAOD::Electron or @c xAOD::Muon
     *@{
     */
    FillScaleFactors(VarList& varList, std::string gName, std::initializer_list<SF::Type> SF_types,
                     const std::vector<ST::SystInfo>& systInfoList);
    FillScaleFactors(VarList& varList, std::string gName, std::initializer_list<SF::Type> SF_types,
                     const std::vector<ST::SystInfo>& systInfoList, std::shared_ptr<custom_SUSYObjDef_xAOD> SUSYTools);
    FillScaleFactors(VarList& varList, std::string gName, std::initializer_list<SF::Type> SF_types,
                     const std::vector<ST::SystInfo>& systInfoList, std::shared_ptr<custom_SUSYObjDef_xAOD> SUSYTools,
                     // xTriggerMuonList *mlist);
                     void* mlist);
    /** @} */

    /** @brief: destructor */
    ~FillScaleFactors();

    /** fill method */
    void fill(const xAOD::IParticle* p);
    void fill(const xAOD::JetContainer* c);
    void fill(const xAOD::EventInfo* e);

  private:
    /** SF enum */
    typedef enum SFtype { GENERAL, MUON_TRIG_SF } SFtype_t;

    /** add entry method */
    void add(const std::string& label);
    void add(const std::string& label, const SFtype_t& sf_type);

    /** @brief: mapped output variables (scale factors)*/
    // can't be std::shared_ptr - because ROOT.
    std::map<std::string, VarF*> m_scale_factors;
    std::map<std::string, VarF*> m_mu_trig_scale_factors;

    /** list of systematics */
    const std::vector<ST::SystInfo> m_systInfoList;

    /** instance to SUSYtools */
    std::shared_ptr<custom_SUSYObjDef_xAOD> m_SUSYTools;

    //[>* instance to muon trigger SFs and systematics  <]
    // xTriggerMuonList *m_muon_trigger_sf_list;

    /** booking methods */
    void book_branches_for_global_muon_trigger();
    void book_branches_for_muons();
    void book_branches_for_electrons();
    void book_branches_for_taus();
    void book_branches_for_photons();
    void book_branches_for_jets();
    void book_branches_for_pileup();
    void book_branches_for_global_btag();
    void book_branches_for_global_jvt();

    /** helper boolean flags*/
    bool m_fill_global_muon_trigger = false;
    bool m_fill_global_btag = false;
    bool m_fill_global_jvt = false;
};

struct FillTheoryWeights : public VarGroup {
    FillTheoryWeights(VarList& varList, std::string gName = "theory_syst", const std::map<std::string, int> weight_map = {});

  public:
    void fill(const xAOD::TruthEventContainer* truth_event_cont, const std::map<std::string, int> map, int dsid);
    bool checkVarName(const std::string var_name);

  private:
    std::vector<VarF*> theory_weights;
    std::unique_ptr<VarD> PDF_central_value;
    std::unique_ptr<VarD> PDF_error_up;
    std::unique_ptr<VarD> PDF_error_down;
};
#endif
