#ifndef HAVE_LFVSELECTION_H
#define HAVE_LFVSELECTION_H

#include <EventLoop/Algorithm.h>
#include <LFV_Z/Config.h>
#include <LFV_Z/Enums.h>
#include <LFV_Z/Region.h>
#include <LFV_Z/VecVarGroups.h>

#include <string>
#include <vector>

class LFVSelection : public Region {
  private:
    SG::AuxElement::ConstAccessor<char> is_signal;
    FillEventInfo m_eventInfo{vlist};            //!< basic event information
    FillEventWeights m_eventWeights{vlist};      //!< MC event weights
    FillMultiplicities m_multiplicities{vlist};  //!< selected object multiplicities

    // Triggers
    FillElectronTriggers m_trigger_el{vlist};  //!< electron trigger signatures
    FillMuonTriggers m_trigger_mu{vlist};      //!< muon trigger signatures
    // FillTauTriggers m_trigger_tau{vlist};        //!< tau trigger signatures
    FillTriggerMatch m_mu_0_trigmatch{vlist, "mu_0_trigmatch",
                                      Config::getInstance()->get<std::vector<std::string>>("trigger_list_muon")};
    FillTriggerMatch m_el_0_trigmatch{vlist, "el_0_trigmatch",
                                      Config::getInstance()->get<std::vector<std::string>>("trigger_list_electron")};

    //// MET
    FillMET m_met{vlist, "met"};
    FillMET m_trackmet{vlist, "trackmet"};

    // Particles
    FillBasicTaus m_taus{vlist, "taus"};
    FillMuonsBasics m_muons{vlist, "muons"};
    FillLeptonsBasics m_electrons{vlist, "electrons"};
    // FillPhotons m_photons{vlist, "photons"};
    FillJets m_jets{vlist, "jets"};

    FillFourVector m_tau_0_lead_track{vlist, "tau_0_lead_track"};

    FillBasicPhoton m_ph_0{vlist, "ph_0"};
    FillBasicPhoton m_ph_1{vlist, "ph_1"};

    // Truth info
    // FillTruthMET m_truth_met{vlist, "truth_met"};
    // FillTruthTau m_truth_tau_0{vlist, "truth_tau_0"};                 //!< truth tau
    // FillTruthParticle m_truth_muon_0{vlist, "truth_muon_0"};          //!< truth muon
    // FillTruthParticle m_truth_electron_0{vlist, "truth_electron_0"};  //!< truth electron

    // Truth matching info
    FillTruthTau m_truth_tau_0{vlist, "tau_0_truth"};                   //!< truth tau
    FillMatchedParticle m_truth_muon_0{vlist, "muon_0_truth"};          //!< truth muon
    FillMatchedParticle m_truth_electron_0{vlist, "electron_0_truth"};  //!< truth electron

    /** Sherpa reweighting*/
    // FillTheoryWeights m_theory_weights;  //!< Theory reweighting
    std::unique_ptr<FillTheoryWeights> m_theory_weights;

    // Vertexing info
    FillVertex m_primaryVertex{vlist};
    VarUI m_primaryVertex_nTracks{vlist, "vertex_nTracks"};

    // Kinematic quantities
    VarB m_tau_0_overlaps_with_calo_muon{vlist, "tau_0_overlaps_with_calo_muon"};
    VarD m_tau_0_mT{vlist, "tau_0_mT"};
    VarD m_el_0_mT{vlist, "el_0_mT"};
    VarD m_mu_0_mT{vlist, "mu_0_mT"};
    VarD m_ph_0_mT{vlist, "ph_0_mT"};
    VarD m_mu_ph_0_mT{vlist, "mu_ph_0_mT"};
    VarD m_el_ph_0_mT{vlist, "el_ph_0_mT"};

    // Tau+el
    VarD m_tau_el_delta_alpha{vlist, "tau_el_delta_alpha"};
    VarD m_tau_el_mvis{vlist, "tau_el_mvis"};
    VarD m_tau_el_mcoll{vlist, "tau_el_mcoll"};
    VarD m_tautrack_el_invmass{vlist, "tautrack_el_invmass"};
    FillBasicMMC m_tau_el_mmc{vlist, "tau_el_mmc"};

    // Tau+mu
    VarD m_tau_mu_delta_alpha{vlist, "tau_mu_delta_alpha"};
    VarD m_tau_mu_mvis{vlist, "tau_mu_mvis"};
    VarD m_tau_mu_mcoll{vlist, "tau_mu_mcoll"};
    VarD m_tautrack_mu_invmass{vlist, "tautrack_mu_invmass"};
    FillBasicMMC m_tau_mu_mmc{vlist, "tau_mu_mmc"};

    // VarD m_tau_track_lep_invmass{vlist, "tau_track__invmass"};

    // Tau+lep+photon
    VarD m_tau_el_ph_mcoll{vlist, "tau_el_ph_mcoll"};
    VarD m_tau_mu_ph_mcoll{vlist, "tau_mu_ph_mcoll"};
    VarD m_tau_el_ph_mvis{vlist, "tau_el_ph_mvis"};
    VarD m_tau_mu_ph_mvis{vlist, "tau_mu_ph_mvis"};

    // Lep+lep invariant mass - for Z peak
    VarD m_leplep_invmass{vlist, "m_ll"};

    // Truth Z pT for Ztautau and signal
    VarD m_truth_Z_pT{vlist, "truth_Z_pT"};
    
    // tau polarisation info. for signal
    VarD m_tau_cos_pol_angle{vlist, "tau_cos_pol_angle"};
    VarD m_tauSpinWeight{vlist, "tauSpinWeight"};
    VarD m_tau_helicity{vlist, "tau_helicity"};

    // Scale factors
    // auto ilist_el             = {SF::Electron};
    // auto ilist_lep            = {SF::Muon, SF::Electron};
    // auto ilist_leptons        = m_jo.channel_tautau_lephad() == "Zee" ?  ilist_el : ilist_lep;
    // auto ilist_tau            = {SF::Tau};
    // auto ilist_jet            = {SF::Jet};  // This gives jet-by-jet JVT SF and B-tag SF
    // auto ilist_global         = {SF::Global_btag, SF::Global_jvt}; // This gives the global JVT SF and B-tag SF
    // auto ilist_pu             = {SF::Pileup};
    // auto ilist_pu_lpx         = {SF::Pileup, SF::LPX};

    FillScaleFactors m_scale_factors_event{vlist, "", {SF::Type::Pileup}, m_systInfoList, m_SUSYTools};

    FillScaleFactors m_scale_factors_tau_0{vlist, "tau_0_SF", {SF::Type::Tau}, m_systInfoList, m_SUSYTools};
    FillScaleFactors m_scale_factors_el_0{vlist, "el_0_SF", {SF::Type::Electron}, m_systInfoList, m_SUSYTools};
    FillScaleFactors m_scale_factors_mu_0{vlist, "mu_0_SF", {SF::Type::Muon}, m_systInfoList, m_SUSYTools};
    FillScaleFactors m_scale_factors_el_1{vlist, "el_1_SF", {SF::Type::Electron}, m_systInfoList, m_SUSYTools};
    FillScaleFactors m_scale_factors_mu_1{vlist, "mu_1_SF", {SF::Type::Muon}, m_systInfoList, m_SUSYTools};
    FillScaleFactors m_scale_factors_ph_0{vlist, "ph_0_SF", {SF::Type::Photon}, m_systInfoList, m_SUSYTools};
    FillScaleFactors m_scale_factors_ph_1{vlist, "ph_1_SF", {SF::Type::Photon}, m_systInfoList, m_SUSYTools};

    // std::vector<FillScaleFactors> m_vec_scale_factors_jet; //!< scale factors for jets including btag
    FillScaleFactors m_scale_factors_global_jets{vlist,
                                                 "jets",
                                                 {SF::Type::Global_btag, SF::Type::Global_jvt},
                                                 m_systInfoList,
                                                 m_SUSYTools};  //!< global scale factors for btag

    FillScaleFactors m_scale_factors_global_muons{
        vlist, "muons", {SF::Type::Global_muon_trigger}, m_systInfoList, m_SUSYTools};  //!< global scale factors for muon trigger

    // args.cp_tools);

    const xAOD::TauJet *SelectTau(const FillBranchesArgs &args);
    const xAOD::IParticle *SelectElectron(const FillBranchesArgs &args);
    const xAOD::IParticle *SelectMuon(const FillBranchesArgs &args);

    void doTriggerMatching(const FillBranchesArgs &args);
    void FillImpactParameters(const FillBranchesArgs &args);
    bool checkSameFlavourPair(const FillBranchesArgs &args);
    bool FillCommon(const FillBranchesArgs &args);

    std::vector<std::string> m_trigger_list_muon;
    std::vector<std::string> m_trigger_list_electron;
    
  public:
    int m_vetoed;

    LFVSelection(const std::string &prefix, TFile *outputFile, xAOD::TStore *store, xAOD::TEvent *event, EL::IWorker *wk,
                 const std::vector<ST::SystInfo> &systInfoList, const std::map<std::string, int> &theory_systs,
                 std::shared_ptr<custom_SUSYObjDef_xAOD> SUSYTools);

    std::vector<std::string> GetTriggersFinal() const {
        return vlist.getTriggers();
    }
    std::vector<std::string> GetVariablesFinal() const {
        return vlist.getVarNames();
    }

    bool FillBranches(const FillBranchesArgs &args);
    void FillDeferredBranches(const FillBranchesArgs &args);
    
};

#endif
