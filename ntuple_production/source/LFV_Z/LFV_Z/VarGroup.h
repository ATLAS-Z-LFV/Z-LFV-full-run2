/**
 * @file: VarGroup.h
 * @author: daniel.scheirich@cern.ch
 *
 * Forked from xTauFW's xVarGroup
 *
 * Base struct for the variable group filling struct
 * @see: VarGroups.h for example of usage
 *
 */

#ifndef HAVE_VARGROUP_H
#define HAVE_VARGROUP_H

#include "VarList.h"

struct VarGroup {
    /**
     * @brief: constructor
     * @param: groupName   name of the variable group. It is used as a prefix of all the variables in the group
     */
    VarGroup(VarList& varList, std::string groupName) : vlist(varList), group(groupName) {}

    /**
     * @brief: name of the variable group. It is used as a prefix of all the variables in the group
     */
    VarList& vlist;
    const std::string group;
};

#endif
