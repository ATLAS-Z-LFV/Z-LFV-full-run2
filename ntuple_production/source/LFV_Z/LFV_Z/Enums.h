#ifndef HAVE_ENUMS_H
#define HAVE_ENUMS_H

#include <array>
#include <iostream>
#include <sstream>
#include <string>

/**
 * @short Structure template for iterating across enums
 * @tparam T Represents the enumeration group
 * @
 */
template <typename T, T... args>
struct EnumIter : public std::iterator<std::input_iterator_tag, const T*, const T&> {
    static constexpr T values[] = {args...};              //!< the name of the point
    static constexpr std::size_t size = sizeof...(args);  //!< size of enumerated list of attributes

    int pos;  //!< position in set of value

    EnumIter()  //!< Constructor: No value is end
        : pos(size) {}

    EnumIter(int start)  //!< Constructor: special position
        : pos(start) {}

    /**
     * @name Constructor
     * @param val List of enumerated attributes
     */
    EnumIter(T val) : pos(std::distance(&values[0], std::find(&values[0], &values[size], val))) {}

    /**
     * @name Operators overloaded used when iterating
     * @{
     */
    const T& operator*() const {
        return values[pos];
    }
    EnumIter& operator++() {
        ++pos;
        return *this;
    }
    EnumIter operator++(int) {
        EnumIter r(*this);
        this->operator++();
        return r;
    }
    bool operator==(EnumIter const& rhs) {
        return pos == rhs.pos;
    }
    bool operator!=(EnumIter const& rhs) {
        return pos != rhs.pos;
    }
    /**
     * @}*/
};

/** Declaration of @c EnumIter */
template <typename T, T... args>
constexpr T EnumIter<T, args...>::values[];

/**
 * Enumeration used for the egamma LH and the AsgElectronLikelihoodTool
 */

enum EleLH { EL_LH_LOOSE, EL_LH_MEDIUM, EL_LH_TIGHT };
static std::array<std::string, 3> EleLH_names{{"EL_LH_LOOSE", "EL_LH_MEDIUM", "EL_LH_TIGHT"}};
typedef struct EnumIter<EleLH, EL_LH_LOOSE, EL_LH_MEDIUM, EL_LH_TIGHT> EleLH_itr;

template <typename T, typename std::enable_if<std::is_enum<T>::value && std::is_same<EleLH, T>::value, int>::type = 0>
inline std::string to_string(const T e) {
    return EleLH_names[e];
}

/**
 * Enumeration used for the @c MuonSelectionTool
 * @note Tight (0), Medium (1), Loose (2), VeryLoose (3)
 * @sa MuonSelectorTools/MuonSelectionTool.h
 */
enum MuonQuality { MU_QUAL_TIGHT, MU_QUAL_MEDIUM, MU_QUAL_LOOSE, MU_QUAL_VERYLOOSE };
typedef struct EnumIter<MuonQuality, MU_QUAL_TIGHT, MU_QUAL_MEDIUM, MU_QUAL_LOOSE, MU_QUAL_VERYLOOSE> MuonQuality_itr;

/**
 * Enumeration used for the @c IsolationSelectionTool
 */
enum IsoSel { ISO_UNDEFINED, ISO_LOOSE, ISO_TIGHT, ISO_TIGHTTRACKONLY, ISO_TIGHTTRACKONLY_FIXEDRAD };
typedef struct EnumIter<IsoSel, ISO_UNDEFINED, ISO_LOOSE, ISO_TIGHT, ISO_TIGHTTRACKONLY, ISO_TIGHTTRACKONLY_FIXEDRAD>
    IsoSel_itr;

static std::array<std::string, 5> IsoSel_names{
    {"ISO_UNDEFINED", "ISO_LOOSE", "ISO_TIGHT", "ISO_TIGHTTRACKONLY", "ISO_TIGHTTRACKONLY_FIXEDRAD"}};

static std::array<std::string, 5> muIsoSel_WPs{{"Undefined", "FCLoose", "FCTight", "FCTightTrackOnly", "FCTightTrackOnly_FixedRad"}};

static std::array<std::string, 5> elIsoSel_WPs_mu{{"Undefined", "FCLoose", "FCTight", "TightTrackOnly", "TightTrackOnly_FixedRad"}};

template <typename T, typename std::enable_if<std::is_enum<T>::value && std::is_same<IsoSel, T>::value, int>::type = 0>
inline std::string toMuIsoWp(const T e) {
    return muIsoSel_WPs[e];
}

template <typename T, typename std::enable_if<std::is_enum<T>::value && std::is_same<IsoSel, T>::value, int>::type = 0>
inline std::string toElIsoWp(const T e) {
    return elIsoSel_WPs_mu[e];
}

template <typename T, typename std::enable_if<std::is_enum<T>::value && std::is_same<IsoSel, T>::value, int>::type = 0>
inline std::string to_string(const T e) {
    return IsoSel_names[e];
}

namespace SF {
    /**
     * Enumeration used for the scale factors
     */
    enum class Type : unsigned int {
        Muon,
        Electron,
        Tau,
        Photon,
        Jet,
        Pileup,
        Global_muon_trigger,
        Global_btag,
        Global_jvt,
    };
}

#endif
