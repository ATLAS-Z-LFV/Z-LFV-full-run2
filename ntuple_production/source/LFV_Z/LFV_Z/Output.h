// vim: ts=4 sw=4
#ifndef HAVE_OUTPUT_H
#define HAVE_OUTPUT_H

#include <TObject.h>
#include <TTree.h>
#include <TH1.h>
#include <TDirectory.h>
#include <type_traits>
#include <vector>
//#include "xEventLoopIncludes/xEventLoopIncludes.h"
#include "LFV_Z/SystematicFunctions.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "SUSYTools/ISUSYObjDef_xAODTool.h"

/** @brief: singleton class that holds list of registered TObjects
 */
class ObjList {
  public:
    static ObjList& getInstance() {
        static ObjList instance;
        return instance;
    }

    /** add a new TObject
     */
    void add(TObject* obj) {
        m_objects.push_back(obj);
    }

    /** set directory of histograms and trees
     */
    void set_directory(TDirectory* file) {
        for (auto obj : m_objects) {
            if (obj->InheritsFrom("TH1")) {
                dynamic_cast<TH1*>(obj)->SetDirectory(file);
            }
            else if (obj->InheritsFrom("TTree")) {
                dynamic_cast<TTree*>(obj)->SetDirectory(file);
            }
        }
    }

    /** write and delete histograms and trees
     */
    void write(TDirectory* file) {
        auto current_dir = gDirectory;
        file->cd();
        for (auto obj : m_objects) {
            if (obj->InheritsFrom("TH1")) {
                dynamic_cast<TH1*>(obj)->Write();
                delete obj;
            }
            else if (obj->InheritsFrom("TTree")) {
                dynamic_cast<TTree*>(obj)->Write();
                delete obj;
            }
        }
        m_objects.clear();
        current_dir->cd();
    }

  private:
    /** Disabled constructors
     */
    ObjList() {}
    ObjList(ObjList const&) = delete;
    void operator=(ObjList const&) = delete;

    /** private attributes
     */
    std::vector<TObject*> m_objects;
};

/** class that creates and holds pointer of type T*
 *  and registers it in the ObjList. The stored pointer can be
 *  accessed and dereferenced using operators ->
 *  The original object can be cloned for each systematic variation
 *  using cloneForSystematics method. Operator [] can be used to
 *  access cloned objects.
 *  T must be derived from TObject (so it is ROOT file storable)
 *
 *  @example:
 *    // in the header file:
 *    Output<TH1D> hist_of_this;
 *    Output<TH1D> hist_of_that;
 *
 *    // in the initialize
 *    hist_of_this = new TH1D("this","", 10, 0, 1);
 *    hist_of_that = new TH1D("that","", 100, -10, 10);
 *    hist_of_that.cloneForSystematics( m_systematics_list );
 *    ObjList::getInstance().store(wk());  // register in output stream
 *
 *    // in the body:
 *    if( m_sysListItr == m_systematics_list.cbegin() ) {
 *      // will only for nominal claibration
 *      hist_of_this->Fill( 12.3 );
 *    }
 *
 *    // fill for each syst. variation
 *    hist_of_that[m_sysListItr]->Fill( 1. );
 *
 */
template <class T, typename std::enable_if<std::is_base_of<TObject, T>::value>::type* = nullptr>
class Output {
  public:
    /** @brief: default constructor.
     */
    Output() : m_systematicNames(1, ""), m_name("") { /* nothing here */
    }

    /** @brief: constructor with initialization
     *  @param: pointer to the TObject-derived class (e.g. histogram)
     */
    Output(T* object) : m_objects(1, object), m_systematicNames(1, object->GetName()), m_name(object->GetName()) {
        // Info("Output::Output", "Adding %s class \"%s\" to the output stream.", object->ClassName(), object->GetName());
        // register the object in the ObjList so that it can be stored in the output stream later.
        ObjList::getInstance().add(m_objects[0]);
    }

    /** @brief: assignment operator
     *  @note: if the Output class is already initialized, the original pointer will be lost
     *  @param: pointer to the TObject-derived class (e.g. histogram)
     *  @returns: the assigned pointer
     */
    Output<T>& operator=(T* object) {
        // cache the pointer to the original object
        m_objects.clear();
        m_objects.push_back(object);
        m_name = object->GetName();

        Info("Output::operator= ", "Adding %s class \"%s\" to the output stream.", object->ClassName(), object->GetName());

        // register the object in the ObjList so that it can be stored in the output stream later.
        ObjList::getInstance().add(m_objects[0]);
        return *this;
    }

    /** @brief: clones the original object creating copy for each systematic variation
     *
     * For each systematic variation creates a clone of the original object. The original
     *  object will be renamed to m_objects[0]->GetName()+"_"+systematics[0].name().
     *  Other clones will be named m_objects[0]->GetName()+"_"+systematics[i].name()
     *
     *  @param: list of systematics
     *  @throws: std::out_of_range if the original object pointer is not initialized or
     *           if the systematics vector is empty
     *  @returns: reference to the vector of cached object pointers
     */
    std::vector<T*>& cloneForSystematics(const std::vector<ST::SystInfo>& systematics) {
        if (systematics.empty()) {
            Warning("Output::cloneForSystematics", "SystematicSet vector is empty");
        }

        // get pointer to the original object and the 1st systematic variation name (usually NOMINAL)
        // range is checked here and the exception will be thrown if the 1st pointer is not initialized
        T* original = dynamic_cast<T*>(m_objects.at(0));
        std::string nominalName = m_systematicNames[0] != "" ? m_systematicNames[0] : "NOMINAL";

        const unsigned int N = std::count_if(systematics.begin(), systematics.end(),
                                             [&](const ST::SystInfo& s) -> bool { return s.affectsKinematics; });
        if (N > 1) {
            Info("Output::cloneForSystematics", "Cloning %s class \"%s\" for %i systematic variations", original->ClassName(),
                 original->GetName(), N);
        }

        // rename the original object so that it contains the name of the first systematic variation
        original->SetName((m_name + ("_" + nominalName)).c_str());

        // loop over remaining systematics and clone the object
        int j = 1;  // we need an extra counter to keep track of the indices in m_objects
        for (size_t i = 1; i < systematics.size(); ++i) {
            if (not systematics[i].affectsKinematics) continue;

            std::string systName = systematics[i].systset.name() != "" ? systematics[i].systset.name() : "NOMINAL";
            std::string histName = StringUtils::ReplaceSubstr(m_name + "_" + systName, "__", "_");

            std::string title = original->GetTitle();
            title += " for " + StringUtils::ReplaceSubstr(systName, "__", "_");

            Info("Output::cloneForSystematics", "Histogram name %s", histName.c_str());
            T* clone = dynamic_cast<T*>(original->Clone(histName.c_str()));
            clone->SetTitle(title.c_str());
            m_objects.push_back(clone);

            // std::string name = systematics[i].systset.name();
            m_systematicNames.push_back(systName);
            m_systematicIdx[systName] = j;

            ++j;

            // register the clone in the ObjList so that it can be stored in the output stream later.
            ObjList::getInstance().add(clone);
        }

        return m_objects;
    }

    /** @brief: default access operator
     *  @returns: pointer to the original object (i.e. the first one)
     *  @throws: std::out_of_range if the original object pointer is not initialized
     */
    T* operator->() {
        return m_objects.at(0);
    }

    /** @brief: i-th item access operator
     *  @param: item index
     *  @returns: pointer to the i-th clone of the original object. Performs no range check.
     */
    T* operator[](size_t i) {
        return m_objects[i];
    }

    /** @brief: item access operator with the systematics iterator
     *  @param:  the systematics list iterator
     *  @returns: pointer to the i-th clone of the original object. Performs no range check.
     */
    T* operator[](const ST::SystInfo& syst) {
        // return m_systematicNames.size() > 0 ? m_objects[indexOf(syst)] : m_objects[0];
        if (m_systematicNames.size() == 0 || syst.systset.name() == "") {
            return m_objects[0];
        }

        std::string systName = syst.systset.name() != "" ? syst.systset.name() : "NOMINAL";
        const auto& itr = m_systematicIdx.find(systName);
        if (itr == m_systematicIdx.end()) {
            // std::cout << "name " << systName << " not found" << std::endl;
            return nullptr;
        }

        return m_objects[itr->second];
    }

    /** @brief: i-th item access method
     *  @returns: pointer to the i-th clone of the original object.
     *  @throws: std::out_of_range if the index is out of range
     */
    T* at(size_t i) {
        return m_objects.at(i);
    }

    /** @brief: item access operator with the systematics iterator
     *  @param:  the systematics list iterator
     *  @returns: pointer to the i-th clone of the original object. Performs no range check.
     */
    T* at(const ST::SystInfo& syst) {
        return m_systematicNames.size() > 0 ? m_objects[indexOf(syst)] : m_objects[0];
    }

    /** @brief: item access method with the systematics iterator
     *  @returns: pointer to the i-th clone of the original object.
     *  @throws: std::out_of_range if the index is out of range
     */
    // T* at(std::vector<CP::SystematicSet>::const_iterator systItr) {
    ////std::cout << "No. of systematics: " << m_systematics->size() << ", no of objects: " << m_objects.size() << std::endl;
    // return m_systematics ? m_objects.at(systItr - m_systematics->cbegin()) : m_objects.at(0);
    //}

    /** @brief: number of stored clones
     *  @returns: pointer to the i-th clone of the original object.
     *  @throws: std::out_of_range if the index is out of range
     */
    size_t size() {
        return m_objects.size();
    }

    void print_content_with_labels() {
        for (auto i = 1; i <= m_objects.at(0)->GetNbinsX(); ++i) {
            if (strlen(m_objects.at(0)->GetXaxis()->GetBinLabel(i)) == 0 && m_objects.at(0)->GetBinContent(i) == 0) {
                // no label, no entries -> assume this is unused to don't print
                continue;
            }
            std::cout << std::setw(3) << i << std::setw(50) << m_objects.at(0)->GetXaxis()->GetBinLabel(i) << " : "
                      << m_objects.at(0)->GetBinContent(i) << std::endl;
        }
    }

  private:
    size_t indexOf(const ST::SystInfo& sys) {
        // return m_systematicIdx[sys.systset.name()];
        return m_systematicIdx.at(sys.systset.name());

        // if(m_systematicNames.size() == 0) return 0;
        // for(size_t i = 0; i < m_systematicNames.size(); ++i) {
        // std::cerr << "Comparing " << sys.systset.name() << " and " << m_systematicNames[i] << std::endl;
        // if(sys.systset.name() == m_systematicNames[i]) {
        // return i;
        //}
        //}
        //// systItr points to systematics which is not contained in the m_systematics vector. This should not happen :-(
        // return 0;
    }

    std::vector<T*> m_objects;  //!< cached pointers to the TObject-derived classes (e.g. histograms)
    // const std::vector<ST::SystInfo>* m_systematics;       //!< cached systematic list
    std::vector<std::string> m_systematicNames;  //!< cached systematic list
    std::unordered_map<std::string, int> m_systematicIdx;
    std::string m_name;  //!< cached name of the stored TObject-derived class
};

#endif
