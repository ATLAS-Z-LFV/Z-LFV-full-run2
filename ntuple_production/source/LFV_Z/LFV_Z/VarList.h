/**
 * @file: VarList.h
 * @author: daniel.scheirich@cern.ch
 *
 * Class that holds all output variabls. Variables must be derived from VarBase class
 * Forked from xTauFW's Varlist
 *
 * @short Additions: Z.Zinonos - zenon@cern.ch
 *
 * -# empty() function
 * -# checkDuplication(VarBase* var) function
 * -# xSystematicsCommon
 */

#ifndef HAVE_VARLIST_H
#define HAVE_VARLIST_H

#include <TTree.h>
#include <algorithm>
#include <memory>
#include <vector>
#include "LFV_Z/SystematicFunctions.h"
#include "VarBase.h"

/**
 * @class VarList
 */
class VarList {
  public:
    /**
     * @brief: constructor
     */
    VarList() {}

    /**
     * @brief: add a new variable
     * @param: var  pointer to the VarBase-derived class
     */
    void add(VarBase* var) {
        checkDuplication(var);
        m_variables.push_back(var);
    }

    /**
     * @brief: Access operator
     * @param: i  -- element index
     * @returns: -- i-th elements
     */
    VarBase* operator[](size_t i) {
        return m_variables[i];
    }

    /**
     * @brief check if variable to be added exists already in the list of variables
     */
    void checkDuplication(VarBase* var) {
        if (std::find_if(begin(m_variables), end(m_variables),
                         [&, var](VarBase* elem) { return (var->branchName() == elem->branchName()); }) != end(m_variables))
            Warning("VarList::checkDuplication", "Attempting to duplicate variable %s", var->branchName().c_str());
    }

    /**
     * @brief: set all the variables to the default value
     */
    void clear() {
        for (auto& v : m_variables) {
            v->clear();
        }
    }

    /**
     * @brief: createBranches
     * Adds branches to the tree using all variables as proxies.
     * @param tree
     */
    void createBranches(TTree* tree) {
        for (auto& v : m_variables) {
            // std::cout << "createBranches(): variable " << v->name() << std::endl;
            if (v->isScaleFactor()) {
                // std::cout << "createBranches(): variable " << v->name() << " is a scale factor" << std::endl;
                if (v->isNominalScaleFactor()) {
                    // std::cout << "createBranches(): isNominalScaleFactor" << std::endl;
                    v->createBranch(tree);
                } else {
                    // std::cout << "createBranches(): NOT isNominalScaleFactor" << std::endl;
                    if (SystematicFunctions::HasNominalName(tree->GetName())) {
                        v->createBranch(tree);
                    } else {
                        std::cout << "createBranches(): not adding var " << v->name() << " to tree " << tree->GetName()
                                  << " - not nominal" << std::endl;
                    }
                }
            } else {
                v->createBranch(tree);
                // std::cout << "createBranches(): variable " << v->name() << " is a NOT scale factor" << std::endl;
            }

        }  // variables loop
    }

    /**
     * @brief: prints the variable names
     */
    void print() const {
        for (auto& v : m_variables) v->print();
    }

    /**
     * @brief: size
     * @return: number of variables
     */
    size_t size() const {
        return m_variables.size();
    }

    /**
     * @brief: empty
     * @return: if list has zero number of elements
     */
    bool empty() const {
        return m_variables.empty();
    }

    /**
     * @brief: getTriggers
     * Returns list of variable names which were marked as ones to store trigger decisions.
     * It is used by the TriggerDecisionTool
     * @return: vector of trigger signatures
     */
    std::vector<std::string> getTriggers() const {
        std::vector<std::string> ret;
        for (auto& var : m_variables) {
            if (var->isTrigger()) ret.push_back(var->name());
        }
        return ret;
    }

    /**
     * @brief: getVarNames
     * @return: vector of variable names
     */
    std::vector<std::string> getVarNames() const {
        std::vector<std::string> ret;
        for (auto& var : m_variables) {
            ret.push_back(var->name());
        }
        return ret;
    }

  private:
    /**
     * @brief: m_variables
     * List of pointers to classes representing output tree variables
     */
    std::vector<VarBase*> m_variables;
};

#endif
