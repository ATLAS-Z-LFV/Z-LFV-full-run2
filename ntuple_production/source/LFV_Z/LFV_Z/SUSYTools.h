#ifndef CUSTOM_SUSYTools_H
#define CUSTOM_SUSYTools_H

#include "EventLoop/StatusCode.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "IsolationSelection/IIsolationSelectionTool.h"
#include "TauAnalysisTools/ITauSmearingTool.h"

// We define a custom SUSYTools class that inherits from the original SUSYTools
// This allow us to gain access to all the CP tools possessed by SUSYTools
// We cannot override functions though, because they were all declared final

class custom_SUSYObjDef_xAOD : public ST::SUSYObjDef_xAOD {
    public:
    custom_SUSYObjDef_xAOD(const std::string& name) : ST::SUSYObjDef_xAOD(name) {}
    
    EL::StatusCode SwitchOffInsituTESCorrection();
    EL::StatusCode UseCaloTES();
    
    void FlagIsoMuons(xAOD::MuonContainer*& copy);
    void FlagIsoElectrons(xAOD::ElectronContainer*& copy);
    
};

#endif
