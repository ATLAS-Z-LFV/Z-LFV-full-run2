/**
 * @file SystematicsFunctions.h
 *
 * @brief Namespace which defines common functionalities, properties etc
 *
 * Forked from xTauFW's xSystematicsCommon class
 *
 * @author Z. Zinonos - zenon@cern.ch
 * @author Geert-Jan Besjes <geert-jan.besjes@cern.ch>
 *
 * @date  Feb 2016
 */

#ifndef HAVE_SYSTEMATICFUNCTIONS_H
#define HAVE_SYSTEMATICFUNCTIONS_H

/**
 * Standard includes
 */
#include <algorithm>
#include <iostream>
#include <iterator>
#include <memory>
#include <string>
#include <vector>

#include "PATInterfaces/SystematicSet.h"
#include "PATInterfaces/SystematicVariation.h"

namespace StringUtils {
    /**
     * @short Helper function to replace substrings in strings
     * @param subject input big string
     * @param search input little substring to be replaced
     * @param replace replacement substring
     */
    extern auto ReplaceSubstr(std::string subject, const std::string &search, const std::string &replace) -> decltype(subject);
    /**
     * @name Identify prefix substring in trigger name
     * @details input: big string output: substring prefix
     * @return true if string contains the given substring
     */
    extern auto HasPrefix(const std::string &str, const std::string &prefix) -> bool;
    /**
     * @name Identify substring in string
     * @details input: big string output: substring
     * @return true if string contains the given substring
     */
    extern auto HasString(const std::string &str, const std::string &sub) -> bool;

}  // namespace

namespace SystematicFunctions {

    using namespace StringUtils;

    /**
     * Name of the nominal tree
     * @note: by default is empty
     */
    extern const std::string nominal_name;
    extern const std::vector<std::string> m_weight_syst;

    extern bool IsVariation(const std::string &str);
    extern bool IsVariation(const std::vector<CP::SystematicSet>::const_iterator &iter);

    extern bool IsWeight(const std::string &s);

    /**
     * @short Naming rules used to adjust the names of systematics
     * @param s input systematic name
     * @return string corresponding the to tree name
     */
    extern auto RenamingRule(const std::string &s) -> std::string;

    /**
     * @short Checks if a given systematic set is the nominal point.
     * @input[in] syst The input systematic set.
     * @return True if the systematic set is nominal.
     */
    extern bool IsNominal(const CP::SystematicSet &syst);

    /**
     * @short Checks if a given iterator of a vector of systematic set is the nominal point.
     * @input[in] iter The input systematic set iterator.
     * @return True if the systematic set is nominal.
     */
    extern bool IsNominal(const std::vector<CP::SystematicSet>::const_iterator &iter);

    /**
     * @short Checks if a string name of a @c SystematicSet is the nominal point.
     * @input[in] str String of the input systematic set iterator.
     * @return True if the systematic set is nominal.
     */
    extern bool IsNominal(const std::string &str);

    /**
     * @short Checks if a string contains the name of the nominal case.
     * @input[in] str String of the input string.
     * @return True if the input string has the nominal name.
     */
    extern bool HasNominalName(const std::string &str);

    /**
     * @short Checks if a string contains the name of the nominal case.
     * @input[in] str String of the input string.
     * @return True if the input string has the nominal name.
     */
    extern bool IsNominalName(const std::string &str);

    /**
     * @short Obtain the name of the nominal case
     * @return String of the nominal case.
     */
    extern auto NominalName() -> decltype(nominal_name);

    /**
     * @short Naming rules used to adjust the names of systematics
     * @param syst input <code>CP::SystematicSet</code>
     * @return string corresponding the to tree name
     */
    extern auto RenamingRule(const CP::SystematicSet &syst) -> std::string;
}

#endif
