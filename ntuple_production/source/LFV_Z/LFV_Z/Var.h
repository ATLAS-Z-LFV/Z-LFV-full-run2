/**
 * @file: Var.h
 * @author: daniel.scheirich@cern.ch
 *
 * Templated class Var. Represents the proxy for
 * the output TTree branch. Type T must be arithmetic type,
 * i.e. either integral or floating point
 *
 * @short Additions: Z.Zinonos - zenon@cern.ch
 * -# TreeVarType template
 * -# Align to ROOT predefined portable types
 */

#ifndef Var_H
#define Var_H

#include <type_traits>
#include "Var.h"
#include "VarList.h"

/**
   * @short Variable type recognition for TTree::Branch.
   * The variable name and the variable type are separated by a
   * slash (/). The variable type must be 1 character. (Characters
   * after the first are legal and will be appended to the visible
   * name of the leaf, but have no effect.) If no type is given, the
   * type of the variable is assumed to be the same as the previous
   * variable. If the first variable does not have a type, it is
   * assumed of type F by default. The list of currently supported
   * types is given below.
   * <a href="https://root.cern.ch/root/html/TTree.html">Root TTree</a>
   *  - B : an 8 bit signed integer (Char_t)
   *  - b : an 8 bit unsigned integer (UChar_t)
   *  - S : a 16 bit signed integer (Short_t)
   *  - s : a 16 bit unsigned integer (UShort_t)
   *  - I : a 32 bit signed integer (Int_t)
   *  - i : a 32 bit unsigned integer (UInt_t)
   *  - F : a 32 bit floating point (Float_t)
   *  - D : a 64 bit floating point (Double_t)
   *  - L : a 64 bit signed integer (Long64_t)
   *  - l : a 64 bit unsigned integer (ULong64_t)
   *  - O : [the letter 'o', not a zero] a boolean (Bool_t)
   * @tparam T type of branch variable
   * @input var constant reference to branch variable
   * @return character corresponding to the input branch variable and with is supported by TTree
   *
   */
template <typename T, typename std::enable_if<std::is_arithmetic<T>::value, int>::type = 0>
char TreeVarType(const T& /*var*/) {
    if (std::is_same<T, Char_t>::value)
        return 'B';
    else if (std::is_same<T, UChar_t>::value)
        return 'b';
    else if (std::is_same<T, Short_t>::value)
        return 'S';
    else if (std::is_same<T, UShort_t>::value)
        return 's';
    else if (std::is_same<T, Int_t>::value)
        return 'I';
    else if (std::is_same<T, UInt_t>::value)
        return 'i';
    else if (std::is_same<T, Float_t>::value)
        return 'F';
    else if (std::is_same<T, Double_t>::value)
        return 'D';
    else if (std::is_same<T, Long64_t>::value)
        return 'L';
    else if (std::is_same<T, ULong64_t>::value)
        return 'l';
    else if (std::is_same<T, Bool_t>::value)
        return 'O';
    else {
        Warning("Var::TreeVarType", "Type %s not predefined in ROOT as TTree Branch type. Returning Float_t.", typeid(T).name());
    }

    return 'F';
}

template <class T, typename std::enable_if<std::is_arithmetic<T>::value>::type* = nullptr>
class Var : public VarBase {
  public:
    /**
     * @brief: constructor
     * @param[in] variables  reference to the variable list
     * @param[in] name       name of the variable. Used as part of the branch name
     * @param[in] groupName  name of the variable group. Used as prefix in the branch name
     */
    Var(VarList& variables, const std::string name, const std::string groupName = "", const T& defaultValue = 0)
        : VarBase(name, groupName), m_defaultValue(defaultValue) {
        // store the variable in the variable list
        variables.add(this);

        // initialize
        clear();
    }

    /**
     * @brief: destructor
     */
    virtual ~Var() {}

    /**
     * @brief: createBranch
     * Adds a branch to the tree using this variable as a proxy.
     */
    virtual void createBranch(TTree* tree) {
        Info("Var::createBranch", "Adding branch %s to the tree %s", branchName().c_str(), tree->GetName());
        // tree->Branch(branchName().c_str(), &m_value);
        std::string name(branchName());
        // eliminate dashes
        while (name.find("-") != std::string::npos) name.replace(name.find("-"), 1, "_");
        // book branch
        auto b = tree->Branch(name.c_str(), &m_value, (name + "/" + TreeVarType(m_value)).c_str());
        b->SetBasketSize(8*1024);
    }

    /**
     * @brief: operator =
     * Assigns the rvalue to the cached value m_value
     * @param: rvalue  assigned value
     */
    Var<T>& operator=(const T& rvalue) {
        m_value = rvalue;
        return *this;
    }

    /**
     * @brief: clear
     * Resets the cached value
     */
    virtual void clear() {
        m_value = m_defaultValue;
    }

    /**
     * @brief: operator *
     * Constant dereference operator
     * @return: const reference to the stored value
     */
    const T& operator*() const {
        return m_value;
    }

    /**
     * @brief: operator *
     * Dereference operator
     * @return: reference to the stored value
     */
    T& operator*() {
        return m_value;
    }

    /**
     * @brief: operator ()
     * Constant dereference operator
     * @return: const reference to the stored value
     */
    const T& operator()() const {
        return m_value;
    }

    /**
     * @brief: operator ()
     * Dereference operator
     * @return: reference to the stored value
     */
    T& operator()() {
        return m_value;
    }

    /**
     * @brief: set default value
     * Sets the default value
     */
    void setDefaultValue(const T& defaultValue) {
        m_defaultValue = defaultValue;
    }

    /**
     * @brief: access to default value
     * Returns a constant reference to the default value
     * @return: constant reference to the default value
     */
    const T& defaultValue() const {
        return m_defaultValue;
    }

  private:
    T m_value;
    T m_defaultValue;
};

template <class T, typename std::enable_if<std::is_arithmetic<T>::value>::type* = nullptr>
class VectorVar : public VarBase {
  public:
    using vecT = std::vector<T>;
    /**
   * @brief: constructor
   * @param[in] variables  reference to the variable list
   * @param[in] name       name of the variable. Used as part of the branch name
   * @param[in] groupName  name of the variable group. Used as prefix in the branch name
   */
    VectorVar(VarList& variables, const std::string name, const std::string groupName = "") : VarBase(name, groupName) {
        // store the variable in the variable list
        variables.add(this);

        // initialize
        clear();
    }

    /**
   * @brief: destructor
   */
    virtual ~VectorVar() {}

    /**
   * @brief: createBranch
   * Adds a branch to the tree using this variable as a proxy.
   */
    virtual void createBranch(TTree* tree) {
        Info("Var::createBranch", "Adding vector branch %s to the tree %s", branchName().c_str(), tree->GetName());
        auto b = tree->Branch(branchName().c_str(), &m_value);
        b->SetBasketSize(16*1024);
    }

    /**
   * @brief: operator =
   * Assigns the rvalue to the cached value m_value
   * @param: rvalue  assigned value
   */
    VectorVar<T>& operator=(const vecT& rvalue) {
        m_value = rvalue;
        return *this;
    }

    /**
   * @brief: clear
   * Resets the cached value
   */
    virtual void clear() {
        // std::cout << "Clearing vector branch " << branchName() << std::endl;
        m_value.clear();
    }

    /**
   * @brief: operator *
   * Constant dereference operator
   * @return: const reference to the stored value
   */
    const vecT& operator*() const {
        return m_value;
    }

    /**
   * @brief: operator *
   * Dereference operator
   * @return: reference to the stored value
   */
    vecT& operator*() {
        return m_value;
    }

    /**
   * @brief: operator ()
   * Constant dereference operator
   * @return: const reference to the stored value
   */
    const vecT& operator()() const {
        return m_value;
    }

    /**
   * @brief: operator ()
   * Dereference operator
   * @return: reference to the stored value
   */
    vecT& operator()() {
        return m_value;
    }

  private:
    vecT m_value;
};

using VarB = Var<bool>;
using VarD = Var<Double_t>;
using VarF = Var<Float_t>;
using VarI = Var<Int_t>;
using VarUI = Var<UInt_t>;
using VarUC = Var<UChar_t>;
using VarC = Var<Char_t>;
using VarUS = Var<UShort_t>;
using VarS = Var<Short_t>;
using VarL = Var<Long64_t>;
using VarUL = Var<ULong64_t>;
using VVarB = VectorVar<bool>;
using VVarF = VectorVar<Float_t>;
using VVarD = VectorVar<Double_t>;
using VVarC = VectorVar<Char_t>;
using VVarS = VectorVar<Short_t>;
using VVarI = VectorVar<Int_t>;
using VVarUS = VectorVar<UShort_t>;
using VVarUI = VectorVar<UInt_t>;

// typedef Var<Double_t> VarD;
// typedef Var<Float_t> VarF;
// typedef Var<Int_t> VarI;
// typedef Var<UInt_t> VarUI;
// typedef Var<UShort_t> VarUS;
// typedef Var<Short_t> VarS;
// typedef Var<Long64_t> VarL;
// typedef Var<ULong64_t> VarUL;
// typedef VectorVar<Float_t> VecF;
// typedef VectorVar<Double_t> VecD;
// typedef VectorVar<Int_t> VecI;
// typedef VectorVar<UInt_t> VecUI;

#endif
