/**
 * @file: VarBase.h
 * @author: daniel.scheirich@cern.ch
 *
 * Base class of the Var templated class. Represents the proxy for
 * the output TTree branch
 */

#ifndef VarBase_H
#define VarBase_H

#include <string>

#include <TTree.h>
#include "xAODBase/IParticle.h"
#include "LFV_Z/SystematicFunctions.h"

/**
 * @class VarBase
 */
class VarBase {
  public:
    /**
     * @brief: constructor
     * @param[in] name      name of the variable. Used as part of the branch name
     * @param[in] groupName name of the variable group. Used as prefix in the branch name
     */
    VarBase(const std::string name, const std::string groupName = "")
        : m_name(name), m_group(groupName), m_isTrigger(false), m_isScaleFactor(false), m_isNominalScaleFactor(false) {}

    /**
     * @brief: createBranch
     * Adds a branch to the tree using this variable as a proxy.
     * Pure virtual method.
     */
    virtual void createBranch(TTree* tree) = 0;

    /**
     * @brief: clear
     * Resets the variable to its default value. Pure virtual method.
     */
    virtual void clear() = 0;

    /**
     * @brief prints the branch name on the screen
     */
    void print() {
        Info("VarBase::print", "%s", branchName().c_str());
    }

    /**
     * @brief: Returns name of the variable
     * @return: m_name
     */
    std::string name() {
        return m_name;
    }

    /**
     * @brief: returns the name of the variable group
     * @return: m_group
     */
    std::string groupName() {
        return m_group;
    }

    /**
     * @brief: name of the branch in the output tree
     * If the group name is not specified, returns the variable name.
     * Otherwise, returns the name in a format "groupName_varName"
     * @return: branch name
     */
    std::string branchName() {
        return m_group != "" ? m_group + "_" + m_name : m_name;
    }

    /**
     * @brief: isTrigger
     * Is this variable used to store trigger decision?
     * @return: true if the variable stores trigger decision
     */
    bool isTrigger() {
        return m_isTrigger;
    }

    /**
     * @brief: setTrigger
     * Mark this variable as one that stores trigger decision
     */
    void setTrigger() {
        m_isTrigger = true;
    }

    /**
     * @brief Scale Factor attributes
     * @{
     */
    void setScaleFactor() {
        m_isScaleFactor = true;
        m_isNominalScaleFactor = SystematicFunctions::HasNominalName(m_name);
    }

    bool isScaleFactor() {
        return m_isScaleFactor;
    }

    bool isNominalScaleFactor() {
        return m_isNominalScaleFactor;
    }
    /**
     *@} */

  private:
    /**
     * @{
     * Private attributes
     */
    std::string m_name;
    std::string m_group;
    bool m_isTrigger;
    bool m_isScaleFactor;
    bool m_isNominalScaleFactor;  //!< auto-deductive
                                  /**
                                   *@} */
};

#endif
