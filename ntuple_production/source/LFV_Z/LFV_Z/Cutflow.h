// vim: ts=4 sw=4

#ifndef LFV_Z_Cutflow_H
#define LFV_Z_Cutflow_H

#include <iostream>
#include <string>
#include <vector>
#include <memory>

class Cut {
  public:
    Cut(const std::string name, const std::string label) {
        m_label = label;
        m_name = name;
        m_count = 0;
    }

    void setLabel(const std::string&);
    const std::string getLabel() const;

    void setName(const std::string&);
    const std::string getName() const;

    void passCut() {
        ++m_count;
        std::cout << "cut '" << m_name << "' count now " << m_count << std::endl;
    }

    unsigned int getCount() const {
        return m_count;
    }

  private:
    Cut(){};

    std::string m_label;
    std::string m_name;
    unsigned int m_count;
};

class Cutflow {
  public:
    Cutflow(){};
    Cutflow(std::string name) : m_name(name){};

    const std::shared_ptr<Cut> getCut(const std::string& name);
    const std::shared_ptr<Cut> addCut(const std::string& name, const std::string& label);

    void passCut(const std::string& name);

    void printCuts() {
        std::cout << "Cutflow " << m_name << std::endl;
        for (auto& c : m_cuts) {
            std::cout << c->getName() << " -> " << c->getCount() << std::endl;
        }
    }

  private:
    std::string m_name;
    std::vector<std::shared_ptr<Cut> > m_cuts;
};

#endif
