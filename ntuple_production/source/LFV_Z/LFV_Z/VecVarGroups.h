#ifndef HAVE_VECTVARGROUPS_H
#define HAVE_VECTVARGROUPS_H

// Forked from xTauFW's VectVarGroups

#include "LFV_Z/VarGroups.h"
//-------------------------------------------------------------------
/**
 * @brief: The FillLeptonsBasics struct
 * Fills leptons' basic kinematics, isolation, and ID variables. Vector branches are used, so info about multiple leptons can be
 * used.
 */
struct FillLeptonsBasics : VarGroup {
    FillLeptonsBasics(VarList& varList, std::string gName = "leptons") : VarGroup(varList, gName) {}

    /** @brief: output variables */
    VVarI particles{vlist, group};   //!< type of the lepton. 1 for muon, 2 for electron, 0 for other type
    VVarI q{vlist, "q", group};      //!< charge of all accepted leptons
    VVarF e{vlist, "e", group};      //!< energy of all accepted leptons
    VVarF pt{vlist, "pt", group};    //!< pT of all accepted leptons
    VVarF eta{vlist, "eta", group};  //!< eta of all accepted leptons
    VVarF phi{vlist, "phi", group};  //!< phi of all accepted leptons

    // MC classifiers
    VVarI origin{vlist, "origin", group};
    VVarI type{vlist, "type", group};

    VVarUS id_veryloose{vlist, "id_veryloose", group};  //!< passed very loose identification: muons only
    VVarUS id_loose{vlist, "id_loose", group};          //!< passed loose identification: muons, electrons
    VVarUS id_medium{vlist, "id_medium", group};        //!< passed medium identification: muons, electrons
    VVarUS id_tight{vlist, "id_tight", group};          //!< passed tight identification: muons, electrons
    VVarUS id_bad{vlist, "id_bad", group};              //!< passed bad identification: muons only

    VVarD trk_d0{vlist, "trk_d0", group};                    //!< transverse impact parameter of the track
    VVarD trk_delta_z0{vlist, "trk_delta_z0", group};        //!< longitudinal impact parameter relative to a primary vertex
    VVarD trk_d0_sig{vlist, "trk_d0_sig", group};            //!< significance of the transverse impact parameter of the track
    VVarD trk_z0_sintheta{vlist, "trk_z0_sintheta", group};  //!< longitudinal impact parameter of the track * sinTheta
    VVarD trk_pvx_z0_sintheta{vlist, "trk_pvx_z0_sintheta",
                             group};  //!< longitudinal impact parameter of the track * sinTheta relative to a primary vertex
    VVarD trk_a0PV{vlist, "trk_a0PV", group};  //!< distance of closest approach of the track relative to a primary vertex

    VVarUS signal{vlist, "signal", group};                          //!< signal flag
    VVarUS nonIsolatedSignal{vlist, "non_isolated_signal", group};  //!< signal flag
    VVarUS iso_wp{vlist, "iso_wp", group};                          //!< isolation working point.
    //! loose, 11 == loose + gradient loose, etc.

    /** @brief: fill methods */
    void add(const xAOD::IParticle* p);
    void add(const xAOD::MuonContainer* p);
    void add(const xAOD::ElectronContainer* p);
};

/**
 * @brief: The FillMuonsBasics struct
 * Fills muons' basic kinematics, isolation, and ID variables. Vector branches are used, so info about multiple muons can be
 * used.
 */
struct FillMuonsBasics : FillLeptonsBasics {
    FillMuonsBasics(VarList& varList, std::string gName = "muons") : FillLeptonsBasics(varList, gName) {}

    VVarD scatteringCurvatureSignificance{vlist, "scatteringCurvatureSignificance", group};

    void initialize();

    /** @brief: fill methods */
    using FillLeptonsBasics::add;
    void add(const xAOD::MuonContainer* p);

    void add(const xAOD::IParticle* p) {
        (void)p;  // suppress unused parameter warning
        static_assert(true, "Cannot be used for IParticles");
    };
    void add(const xAOD::ElectronContainer* p) {
        (void)p;  // suppress unused parameter warning
        static_assert(true, "Cannot be used for ElectronContainers");
    };
};
//-------------------------------------------------------------------
struct FillVectorBranches {
    FillVectorBranches(VarList& varList) : vecVList(varList) {}

    /**
     * @brief: creates vector branches copies of flat branches from  FillLepton
     */
    void initialize(std::string group);

    /**
     * @brief: Default copy of the flat branches into vector branches.
     */
    void copyValues();

    /**
     * @brief: list of vector variables, copied automatically from parent FillLepton class
     */
    VarList tmpVList;
    VarList& vecVList;

    /**
     * @{
     * @brief: maps between flat and vector variables
     */
    std::vector<std::pair<VarB*, VVarB*> > VVarBList;
    std::vector<std::pair<VarF*, VVarF*> > VVarFList;
    std::vector<std::pair<VarD*, VVarD*> > VVarDList;
    std::vector<std::pair<VarC*, VVarC*> > VVarCList;
    std::vector<std::pair<VarS*, VVarS*> > VVarSList;
    std::vector<std::pair<VarI*, VVarI*> > VVarIList;
    std::vector<std::pair<VarUS*, VVarUS*> > VVarUSList;
    std::vector<std::pair<VarUI*, VVarUI*> > VVarUIList;

    /** @} */
};

//-------------------------------------------------------------------
/**
 * @brief: Fills lepton variables as vector branches
 */
struct FillLeptons : FillVectorBranches, FillLepton {
    FillLeptons(VarList& varList, std::string gName = "leptons") : FillVectorBranches(varList), FillLepton(tmpVList, gName) {}

    /**
     * @brief: creates vector branches copies of flat branches from  FillLepton
     */
    void initialize();

    /** @brief: fill methods */
    void add(const xAOD::IParticle* p);
    void add(const xAOD::MuonContainer* p);
    void add(const xAOD::ElectronContainer* p);
};
//-------------------------------------------------------------------
/**
 * @brief: Fills photons variables as vector branches
 */
struct FillPhotons : FillVectorBranches, FillPhoton {
    FillPhotons(VarList& varList, std::string gName = "photons") : FillVectorBranches(varList), FillPhoton(tmpVList, gName) {}

    /**
     * @brief: creates vector branches copies of flat branches from  FillPhoton
     */
    void initialize();

    /** @brief: fill methods */
    void add(const xAOD::Photon* p);
    void add(const xAOD::PhotonContainer* c);
};
////-------------------------------------------------------------------
/**
* @brief: Fills jet variables as vector branches
*/
struct FillJets : FillVectorBranches, FillJet {
    FillJets(VarList& varList, std::string gName = "jets") : FillVectorBranches(varList), FillJet(tmpVList, gName) {}

    /**
     * @brief: creates vector branches copies of flat branches from  FillJet
     */
    void initialize();

    /** @brief: fill methods */
    void add(const xAOD::Jet* p);
    void add(const xAOD::JetContainer* c);
};
//-------------------------------------------------------------------
/**
* @brief: Fills tau variables as vector branches
*/
struct FillBasicTaus : FillVectorBranches, FillBasicTau {
    FillBasicTaus(VarList& varList, std::string gName = "taus") : FillVectorBranches(varList), FillBasicTau(tmpVList, gName) {}

    /**
     * @brief: creates vector branches copies of flat branches from FillBasicTau
     */
    void initialize();

    /** @brief: fill methods */
    void add(const xAOD::TauJet* p);
    void add(const xAOD::TauJetContainer* c);
};

//-------------------------------------------------------------------
/**
* @brief: Fills tau variables as vector branches
*/
struct FillTaus : FillVectorBranches, FillTau {
    FillTaus(VarList& varList, std::string gName = "taus") : FillVectorBranches(varList), FillTau(tmpVList, gName) {}

    /**
     * @brief: creates vector branches copies of flat branches from FillTau
     */
    void initialize();

    /** @brief: fill methods */
    void add(const xAOD::TauJet* p);
    void add(const xAOD::TauJetContainer* c);
};
//-------------------------------------------------------------------
/**
 * @brief:
 */
struct FillLeptons_MatchedParticle : FillVectorBranches, FillMatchedParticle {
    FillLeptons_MatchedParticle(VarList& varList, std::string gName = "leptons")
        : FillVectorBranches(varList), FillMatchedParticle(tmpVList, gName) {}

    /**
     * @brief: creates vector branches copies of flat branches from  FillMatchedParticle
     */
    void initialize();

    /** @brief: fill methods */
    void add(const xAOD::IParticle* p, const xAOD::IParticleContainer* pc);
    void add(const xAOD::MuonContainer* p, const xAOD::IParticleContainer* pc);
    void add(const xAOD::ElectronContainer* p, const xAOD::IParticleContainer* pc);
};
////-------------------------------------------------------------------
/**
 * @brief:
 */
struct FillLeptons_MatchedTruthLeptonicTauDecay : FillVectorBranches, FillMatchedTruthLeptonicTauDecay {
    FillLeptons_MatchedTruthLeptonicTauDecay(VarList& varList, std::string gName = "leptons")
        : FillVectorBranches(varList), FillMatchedTruthLeptonicTauDecay(tmpVList, gName) {}

    /**
     * @brief: creates vector branches copies of flat branches from  FillMatchedTruthLeptonicTauDecay
     */
    void initialize();

    /** @brief: fill methods */
    void add(const xAOD::IParticle* p, const xAOD::TruthParticleContainer* truth_taus);
    void add(const xAOD::MuonContainer* p, const xAOD::TruthParticleContainer* truth_taus);
    void add(const xAOD::ElectronContainer* p, const xAOD::TruthParticleContainer* truth_taus);
};
////-------------------------------------------------------------------
#endif
