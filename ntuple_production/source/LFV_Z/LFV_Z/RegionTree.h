/**
 * @file RegionTree.h
 *
 * @brief RegionTree class to represent a TTree with attributes.
 *
 * Forked from xTauFW's xChannelTree
 *
 * Serves to describe a tree unit to be used in RegionTreeList
 * class. Each tree has a name which usually corresponds to a
 * systematic variation.
 *
 * @author Z. Zinonos - zenon@cern.ch
 *
 * @date May 2015
 */

#ifndef HAVE_REGIONTREE_H
#define HAVE_REGIONTREE_H

#include <string>
#include <vector>
#include "TTree.h"
#include "TFile.h"
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

class RegionTree {
  private:
    /**
     *  @brief Default constructor
     */
    RegionTree() = default;

  public:
    /**
     * @brief Explicit constructor.
     *
     * Setups member pointer and name for
     * a given reconstructed tree
     * @param s tree name
     * @param t tree name
     */
    explicit RegionTree(const std::string& s, TTree* t) try : m_tree_name(s), m_tree(t) {
        // contructing
    } catch (...) {
        Error("RegionTree::RegionTree", "exception occurred on initialization");
    }

    /**
     * @brief Default copy ctor
     * @param const RegionTree reference
     */
    RegionTree(const RegionTree& /*other*/) = default;

    /**
     * @brief Default 'move' constructor.
     *
     * @param s tree name
     */
    RegionTree(RegionTree&& /*other*/) = default;

    /*RegionTree(RegionTree&& other)
      try
      :
      m_tree_name( std::move(other.m_tree_name)),
      m_tree( std::move(other.m_tree))
      {
    //moving
    } catch(...){
    Error("RegionTree::RegionTree", "'move': exception occurred on initialization");
    }
    */

    /**
     * @brief Default copy assignment constructor
     * @param const RegionTree instance
     */
    RegionTree& operator=(const RegionTree& other) = default;

    /**
     * @brief Default destructor
     */
    ~RegionTree() = default;

    /**
     * @brief Resets the pointers of the private tree members
     */
    void reset() {
        delete m_tree;
        m_tree = NULL;
    }

    /**
     * @brief Prints the tree name and the number of created branches
     */
    void print() {
        Info("RegionTree::print()", "tree %s branches %ui", m_tree_name.c_str(), m_tree->GetNbranches());
    }

    /**
     * @brief Fills the tree
     */
    void fill() {
        m_tree->Fill();
    }

    /**
     * @brief Sets auto flush
     */
    void set_auto_flush(long autof) {
        m_tree->SetAutoFlush(autof);
    }

    /**
     * @brief Sets the tree directory
     * @param f TFile pointer to output root file
     */
    void set_directory(TDirectory* file) {
        m_tree->SetDirectory(file);
    }

    /**
     * @brief Write to an output file and delete
     * @param f TFile pointer to output root file
     */
    void write(TDirectory* file) {
        auto current_dir = gDirectory;
        file->cd();
        
        m_tree->Write();
        reset();
        
        current_dir->cd();
    }

    /**
     * @brief Adds the tree to the EventLoop worker
     * @param wk EventLoop worker pointer
     * DEPRECATED
     */
    //void add_to_worker(EL::IWorker* wk) {
    //    wk->addOutput(m_tree);
    //}

    /**
     * @brief Set the maximum size in bytes of the Tree file
     *
     * <a href="https://root.cern.ch/root/html/TTree.html#TTree:SetMaxTreeSize">SetMaxTreeSize</a>
     */
    void set_max_tree_size() {
        m_tree->SetMaxTreeSize();
    }

    /**
     * @brief Retrieve the tree name
     * @return Tree name
     */
    std::string name() const {
        return m_tree_name;
    }

    /**
     * @brief Access to the tree pointer
     * @return TTree pointer of the tree member
     */
    TTree* ptr() const {
        return m_tree;
    }

    /**
     * @brief Overloaded () operator
     * @return TTree pointer of the tree member
     */
    TTree* operator()() {
        return m_tree;
    }

  private:
    std::string m_tree_name;  //!< Tree name member variable
    TTree* m_tree;            //!< Tree pointer
    long m_entries=0;
};

#endif
