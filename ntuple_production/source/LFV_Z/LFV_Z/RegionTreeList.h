/**
 * @file RegionTreeList.h
 *
 * @short RegionTreeList to represent a list of RegionTree objects
 *
 * Forked from xTauFW's xChannelTreeList.h
 *
 * Vector-based class to keep multiple RegionTree objects.
 * Provides the basic functionalities to link branches to trees
 * and fill trees.
 *
 * @author Z. Zinonos - zenon@cern.h
 * @author Geert-Jan Besjes - geert-jan.besjes@cern.ch
 *
 * @date Feb 2016
 *
 */

#ifndef HAVE_REGIONTREELIST_H
#define HAVE_REGIONTREELIST_H

#include <EventLoop/Worker.h>
#include <TFile.h>
#include <TTree.h>
#include <algorithm>
#include <functional>  //<! std::mem_fun_ref
#include <iterator>
#include <vector>

#include "LFV_Z/RegionTree.h"
#include "LFV_Z/VarList.h"

/**
 * forward declarations
 */
class RegionTree;

/**
 *@short RegionTreeList class
 */

class RegionTreeList {
  public:
    /**
     * @brief Default constructor
     */
    RegionTreeList() = default;

    /**
     * @brief Destructor
     */
    ~RegionTreeList() {
        // do something?
    }

    /**
     * @brief add a tree object
     * @param var const reference to an RegionTree object
     */
    void add(const RegionTree &t) {
        m_trees.push_back(t);
    }

    /**
     * @brief add a tree object by name and pointer
     * @param syst const reference to a systematic variation name
     * @param t pointer to a TTree object
     */
    void add(const std::string &syst, TTree *t) {
        m_trees.emplace_back(syst, t);
    }

    /**
     * @brief add a tree object
     * @param t const reference to a RegionTree class
     * @see add()
     */
    void push_back(const RegionTree &t) {
        m_trees.push_back(t);
    }

    /**
     * @brief: reset tree pointers to null
     */
    void reset() {
        // std::for_each(begin(m_trees), end(m_trees), std::bind(&RegionTree::reset, std::placeholders::_1));
        for (auto &t : m_trees) {
            t.reset();
        }
    }

    /**
     * @brief prints the variable names
     */
    void print() {
        // std::for_each(begin(m_trees), end(m_trees), std::mem_fun_ref(&RegionTree::print));
        for (auto &t : m_trees) {
            t.print();
        }
    }

    /**
     * @brief size
     * @return number of tree objects
     */

    size_t size() const {
        return m_trees.size();
    }

    /**
     * @brief empty
     * @return if list has zero number of elements
     */
    bool empty() const {
        return m_trees.empty();
    }

    /**
     * @brief Fill tree for a given tree name
     * @param tname tree name
     */
    void fill(const std::string &tname) {
        std::vector<RegionTree>::iterator itr =
            std::find_if(begin(m_trees), end(m_trees),
                         std::bind(std::equal_to<std::string>(), std::bind(&RegionTree::name, std::placeholders::_1), tname));

        if (itr != m_trees.end()) {
            itr->fill();
        } else {
            throw std::runtime_error("RegionTreeList::fill() : cannot find tree");
        }
    }

    /**
     * @brief Set auto flush
     */
    void set_auto_flush(long autof) {
        long maxAutof = -64*1024*1024;  // 64 MB
        if (m_trees.size() == 1) {
            m_trees[0].set_auto_flush(autof < maxAutof ? maxAutof : autof);
        }
        else {
            // Distribute the memory usage evenly to each tree
            // The nominal tree gets double the share since it has more branches
            autof /= m_trees.size() + 1;
            for (auto &t : m_trees) {
                if (t.name().find("NOMINAL") == std::string::npos)
                    t.set_auto_flush(autof < maxAutof ? maxAutof : autof);
                else
                    t.set_auto_flush(2*autof < maxAutof ? maxAutof : 2*autof);
            }
        }
    }

    /**
     * @brief Set writing directory for trees
     * @param tfile pointer to root file
     */
    void set_directory(TDirectory* file) {
        // std::for_each(begin(m_trees), end(m_trees), std::bind2nd(std::mem_fun_ref(&RegionTree::set_directory), tfile));
        for (auto &t : m_trees) {
            t.set_directory(file);
        }
    }

    /**
     * @brief Write trees to output file and delete them
     * @param tfile pointer to root file
     */
    void write(TDirectory* file) {
        for (auto &t : m_trees) {
            t.write(file);
        }
        m_trees.clear();
    }

    /**
     * @brief Add trees to the event loop worker
     * @param worker pointer to EventLoop Worker
     * DEPRECATED
     */
    //void add_trees_to_worker(EL::IWorker *worker) {
    //    // std::for_each(begin(m_trees), end(m_trees), std::bind2nd(std::mem_fun_ref(&RegionTree::add_to_worker), worker));
    //
    //    for (auto &t : m_trees) {
    //        t.add_to_worker(worker);
    //    }
    //}

    /**
     * @brief Set maximum size of a tree file
     */
    void set_max_tree_size() {
        // std::for_each(begin(m_trees), end(m_trees), std::mem_fun_ref(&RegionTree::set_max_tree_size));
        for (auto &t : m_trees) {
            t.set_max_tree_size();
        }
    }

    /**
     * @brief Links branches to trees
     * @param v reference to a VarList list of variables
     */
    void link_branches(VarList &v) {
        for (auto &t : m_trees) {
            v.createBranches(t());
        }
    }

  private:
    /**
     * @brief: m_variables
     * List of pointers to classes representing output tree variables
     */
    std::vector<RegionTree> m_trees;
};

#endif
