// vim: ts=4 sw=4

#include <fstream>

#include <LFV_Z/EventSelection.h>

#include <sys/stat.h>

#include <algorithm>
#include <functional>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include "TSystem.h"
#include "TTree.h"

#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"

#include <SampleHandler/MetaData.h>

#include "AsgTools/MsgStream.h"
#include "AsgTools/MsgStreamMacros.h"

#include "AthContainers/AuxElement.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODRootAccess/TActiveStore.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

#include "PATInterfaces/SystematicSet.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTau/TauJetContainer.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <LFV_Z/Histograms.h>
#include <LFV_Z/LFVSelection.h>
#include <LFV_Z/LFVLepSelection.h>
#include <LFV_Z/RegionBuilder.h>
#include <LFV_Z/Utils.h>

#include "AssociationUtils/OverlapRemovalTool.h"
#include "TauAnalysisTools/SharedFilesVersion.h"
#include "TauAnalysisTools/TauSelectionTool.h"
#include "TauAnalysisTools/TauSmearingTool.h"

#include <boost/algorithm/string/predicate.hpp>

//#include <gperftools/profiler.h>

// static SG::AuxElement::Accessor<char> is_passOR("passOR");
// static SG::AuxElement::Accessor<char> is_bad("bad");

// macro to "continue" if statement is true
#define EL_CONTINUE(COND)                           \
    do {                                            \
        const bool result = COND;                   \
        if (result) return EL::StatusCode::SUCCESS; \
    } while (false)

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK(CONTEXT, EXP)                                                        \
    do {                                                                                     \
        if (!EXP.isSuccess()) {                                                              \
            Error(CONTEXT, XAOD_MESSAGE("Failed to execute %s at line %i"), #EXP, __LINE__); \
            return EL::StatusCode::FAILURE;                                                  \
        }                                                                                    \
    } while (false)

// template <typename T, typename std::enable_if<std::is_class<T>::value && std::is_base_of<asg::AsgTool, T>::value, int>::type = 0>
// inline EL::StatusCode initializeTool(T* t) {
//     if (!t->initialize().isSuccess()) {
//         Error("initializeTool", "Failed to properly initialize %s. Exiting.", typeid(T).name());
//         return EL::StatusCode::FAILURE;
//     }
// 
//     return EL::StatusCode::SUCCESS;
// }

// this is needed to distribute the algorithm to the workers
ClassImp(EventSelection)

inline bool fileExists(const std::string& name) {
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

EventSelection::EventSelection()
    /*: is_baseline("baseline"),
      is_signal("signal"),
      is_isol("isol"),
      is_passOR("passOR"),
      is_passBaseID("passBaseID"),
      is_bad("bad"),
      is_bjet("bjet"),
      is_bjet_loose("bjet_loose"),
      prioAcc("selected"),
      acc_effscalefact("effscalefact"),
      acc_jvtscalefact("jvtscalefact"),
      dec_passOR("passOR"),
      dec_baseline("baseline"),
      dec_signal("signal"),
      dec_isol("isol")*/ {
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().

    m_verbose = false;
    this->SetName("EventSelection");
    m_nFilesProcessed = 0.;
    ATH_MSG_INFO("EventSelection::EventSelection");
}

EL::StatusCode EventSelection::setupJob(EL::Job& job) {
    // Here you put code that sets up the job on the submission object
    // so that it is ready to work with your algorithm, e.g. you can
    // request the D3PDReader service or add output files.  Any code you
    // put here could instead also go into the submission script.  The
    // sole advantage of putting it here is that it gets automatically
    // activated/deactivated when you add/remove the algorithm from your
    // job, which may or may not be of value to you.

    ATH_MSG_INFO("EventSelection::setupJob()");

    // For some reason we can't do this directly from python, but we _can_ set booleans
    if (m_verbose) {
        this->msg().setLevel(MSG::VERBOSE);
    }

    //if (this->msg().level() == MSG::DEBUG or this->msg().level() == MSG::VERBOSE) {
    //    CP::SystematicCode::enableFailure();
    //    StatusCode::enableFailure();
    //    xAOD::TReturnCode::enableFailure();
    //    CP::CorrectionCode::enableFailure();
    //}

    std::string filename = std::string(gSystem->GetFromPipe("echo $WorkDir_DIR/data/LFV_Z/").Data()) + m_configFile;
    ATH_MSG_INFO("setupJob(): config = " << filename);
    m_config = Config::getInstance();
    m_config->readConfigFile(filename);

    m_outputLabel = m_config->get("outputName");
    ATH_MSG_INFO("EventSelection::setupJob(): setting output label to " << m_outputLabel);
    EL::OutputStream out(m_outputLabel);
    // out.options()->setString(EL::OutputStream::optContainerSuffix, m_outputLabel.substr(0, 2));
    job.outputAdd(out);

    ATH_MSG_INFO("EventSelection::setupJob(): output stream added successfully");
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventSelection::histInitialize() {
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    std::string filename = std::string(gSystem->GetFromPipe("echo $WorkDir_DIR/data/LFV_Z/").Data()) + m_configFile;
    ATH_MSG_INFO("histInitialize(): config = " << filename);
    m_config = Config::getInstance();
    m_config->readConfigFile(filename);

    int nbins = 6;
    Histograms& hists = Histograms::getInstance();
    hists.cutflow_common = new TH1D("cutflow_common", "Number of accepted events - preselection", nbins, 1, nbins + 1);
    hists.cutflow_common->GetXaxis()->SetBinLabel(1, "All events");
    hists.cutflow_common->GetXaxis()->SetBinLabel(2, "GRL (data) / non-zero RRN (MC)");
    hists.cutflow_common->GetXaxis()->SetBinLabel(3, "Detector cleaning");
    hists.cutflow_common->GetXaxis()->SetBinLabel(4, "Primary vertex check");
    hists.cutflow_common->GetXaxis()->SetBinLabel(5, "Trigger");
    hists.cutflow_common->GetXaxis()->SetBinLabel(6, "Bad jet veto");

    ATH_MSG_INFO("EventSelection::histInitialize()");
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventSelection::fileExecute() {
    // Here you do everything that needs to be done exactly once for every
    // single file, e.g. collect a list of all lumi-blocks processed
    ATH_MSG_INFO("EventSelection::fileExecute()");
    return EL::StatusCode::SUCCESS;
}

void EventSelection::addPreviousFileMetaData() {
    // In case not the entire file has been processed (e.g. using --nEventsPerFile)
    // add only a fraction of the xAOD/DxAOD nEvents/sumW/sumW2 to the total
    // Take the fraction to be (processed sumW)/(total sumW in DAOD)
    if (m_nMCWeightedEvents_thisFile==0 || m_DxAOD_initialSumOfWeights_thisFile==0) {
        return;
    }
    
    m_nMCWeightedEvents += m_nMCWeightedEvents_thisFile;
    
    double processed_ratio = double(m_nMCWeightedEvents_thisFile) / m_DxAOD_initialSumOfWeights_thisFile;
    
    m_nFilesProcessed += processed_ratio;
    
    m_xAOD_initialSumOfEvents
    += processed_ratio * m_xAOD_initialSumOfEvents_thisFile;
    m_xAOD_initialSumOfWeights
    += processed_ratio * m_xAOD_initialSumOfWeights_thisFile;
    m_xAOD_initialSumOfWeightsSquared
    += processed_ratio * m_xAOD_initialSumOfWeightsSquared_thisFile;
    
    m_DxAOD_initialSumOfEvents
    += processed_ratio * m_DxAOD_initialSumOfEvents_thisFile;
    m_DxAOD_initialSumOfWeights
    += processed_ratio * m_DxAOD_initialSumOfWeights_thisFile;
    m_DxAOD_initialSumOfWeightsSquared
    += processed_ratio * m_DxAOD_initialSumOfWeightsSquared_thisFile;
}

EL::StatusCode EventSelection::processMetaData(bool firstFile) {
    // dynamically determine the xAOD/derivation status
    if (firstFile) {
        bool determined_derivation_status = false;
        m_isDerivation = false;
        TTree* MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
        if (MetaData->GetBranch("StreamAOD")) {
            m_isDerivation = false;
            determined_derivation_status = true;
            m_streamType = "AOD";
        } else {
            for (const TObject* branch : *MetaData->GetListOfBranches()) {
                if (!TString(branch->GetName()).Contains("DAOD")) {
                    continue;
                }

                // this is a derivation
                m_isDerivation = true;
                determined_derivation_status = true;

                // pick up name of derivation
                m_streamType = StringUtils::ReplaceSubstr(branch->GetName(), "StreamDAOD_",
                                                          "");  // remove 'StreamDAOD_' from StreamDAOD_XXXX
                break;
            }
        }

        if (!determined_derivation_status) {
            ATH_MSG_FATAL("Unable to determine derivation status");
            return EL::StatusCode::FAILURE;
        }

        ATH_MSG_INFO("Is derivation? " << m_isDerivation << " Name: " << m_streamType);
    }

    if (!firstFile) {
        // add metadata from the previous file to the total
        addPreviousFileMetaData();
    }

    m_xAOD_initialSumOfEvents_thisFile = 0.;
    m_xAOD_initialSumOfWeights_thisFile = 0.;
    m_xAOD_initialSumOfWeightsSquared_thisFile = 0.;

    m_DxAOD_initialSumOfEvents_thisFile = 0.;
    m_DxAOD_initialSumOfWeights_thisFile = 0.;
    m_DxAOD_initialSumOfWeightsSquared_thisFile = 0.;

    m_nMCWeightedEvents_thisFile = 0.;

    // get the MetaData tree once a new file is opened, with
    TTree* MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
    if (!MetaData) {
        ATH_MSG_FATAL("MetaData not found in input file");
        return EL::StatusCode::FAILURE;
    }

    // check for corruption
    const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
    if (!m_event->retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()) {
        ATH_MSG_FATAL("Failed to retrieve IncompleteCutBookkeepers from MetaData!");
        return EL::StatusCode::FAILURE;
    }

    // Find the actual CBK information
    const xAOD::CutBookkeeperContainer* completeCBC = nullptr;
    if (!m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()) {
        ATH_MSG_FATAL("Failed to retrieve CutBookkeepers from MetaData!");
        return EL::StatusCode::FAILURE;
    }
    
    int maxcycle = -1;
    // let's find the right CBK (latest with StreamAOD input before derivations)
    const xAOD::CutBookkeeper* xAOD_CBK = nullptr;
    const xAOD::CutBookkeeper* DxAOD_CBK = nullptr;
    int i = 0;
    for (auto cbk : *completeCBC) {
        std::string weight_name("LHE3Weight_");
        if (StringUtils::HasString(cbk->name(), weight_name)) {
            m_sherpa_theory_syst.insert(std::make_pair(cbk->name(), i));
            ++i;
        }

        // all events
        if (cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxcycle) {
            maxcycle = cbk->cycle();
            xAOD_CBK = cbk;
        }

        // events in derivation
        if (StringUtils::HasString(cbk->name(), m_streamType) && !DxAOD_CBK) {
            DxAOD_CBK = cbk;
        }
    }

    if (!xAOD_CBK) {
        ATH_MSG_ERROR("NULL CutBookkeeper pointer for all events");
        return EL::StatusCode::FAILURE;
    }

    if (m_isDerivation && !DxAOD_CBK) {
        // ATH_MSG_WARNING("NULL CutBookkeeper pointer for derived events");
        ATH_MSG_ERROR("NULL CutBookkeeper pointer for derived events");
        return EL::StatusCode::FAILURE;
    }

    m_xAOD_initialSumOfEvents_thisFile = xAOD_CBK->nAcceptedEvents();
    m_xAOD_initialSumOfWeights_thisFile = xAOD_CBK->sumOfEventWeights();
    m_xAOD_initialSumOfWeightsSquared_thisFile = xAOD_CBK->sumOfEventWeightsSquared();

    if (m_isDerivation && DxAOD_CBK) {
        m_DxAOD_initialSumOfEvents_thisFile = DxAOD_CBK->nAcceptedEvents();
        m_DxAOD_initialSumOfWeights_thisFile = DxAOD_CBK->sumOfEventWeights();
        m_DxAOD_initialSumOfWeightsSquared_thisFile = DxAOD_CBK->sumOfEventWeightsSquared();
    }

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventSelection::changeInput(bool firstFile) {
    // Here you do everything you need to do when we change input files,
    // e.g. resetting branch addresses on trees.  If you are using
    // D3PDReader or a similar service this method is not needed.
    ATH_MSG_INFO("EventSelection::changeInput()");

    m_event = wk()->xaodEvent();

    // process metadata
    if (processMetaData(firstFile) == EL::StatusCode::FAILURE) {
        ATH_MSG_FATAL("processMetaData() failed");
        return EL::StatusCode::FAILURE;
    }

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventSelection::initialize() {
    // Here you do everything that you need to do after the first input
    // file has been connected and before the first event is processed,
    // e.g. create additional histograms based on which variables are
    // available in the input files.  You can also create all of your
    // histograms and trees in here, but be aware that this method
    // doesn't get called if no events are processed.  So any objects
    // you create here won't be available in the output if you have no
    // input events.

    ATH_MSG_INFO("EventSelection::initialize()");

    if (m_verbose) {
        this->msg().setLevel(MSG::VERBOSE);
    }
    
    ATH_MSG_INFO("EventSelection::initialize()");

    m_initialized = true;
    m_config = Config::getInstance();
    
    m_containerSuffix = m_config->has("containerSuffix") ? m_config->get("containerSuffix") : "";
    if (m_config->has("systematicsSelection")) {
        m_systMatch = Utils::splitString(m_config->get("systematicsSelection"), " ");
    }
    m_jetContainerKey = m_config->get("jetContainerKey");
    m_bTaggingTimeStamp = m_config->get("bTaggingTimeStamp");

    // initialization of counters and run info
    m_xAOD_initialSumOfEvents = 0.;
    m_xAOD_initialSumOfWeights = 0.;
    m_xAOD_initialSumOfWeightsSquared = 0.;
    m_DxAOD_initialSumOfEvents = 0.;
    m_DxAOD_initialSumOfWeights = 0.;
    m_DxAOD_initialSumOfWeightsSquared = 0.;
    m_nEventsProcessed = 0;
    m_nAcceptedEvents = 0;
    m_nMCWeightedEvents = 0.;
    m_nPUWeightedEvents = 0.;
    m_nTotalWeightedEvents = 0.;
    m_eventNumber = -1;
    m_runNumber = -1;
    m_mcChannelNumber = -1;

    // booleans for photons & taus
    m_getPhotons = m_config->get<bool>("getPhotons");
    m_getTaus = m_config->get<bool>("getTaus");
    m_doTauTruthMatching = m_config->get<bool>("doTauTruthMatching");

    if (!m_getTaus && m_doTauTruthMatching) {
        m_doTauTruthMatching = false;
    }

    // booleans for SFs
    m_doTauSF = m_config->get<bool>("doTauSF");
    m_doTauTrigSF = m_config->get<bool>("doTauTrigSF");
    m_doMuonSF = m_config->get<bool>("doMuonSF");
    m_doMuonTrigSF = m_config->get<bool>("doMuonTrigSF");
    m_doElectronSF = m_config->get<bool>("doElectronSF");
    m_doElectronTrigSF = m_config->get<bool>("doElectronTrigSF");
    m_doPhotonSF = m_getPhotons ? m_config->get<bool>("doPhotonSF") : false;

    // muon trigger for SF
    if (m_config->has("SF_muonTrig2015")) {
        m_SF_muonTrig2015 = m_config->get<std::string>("SF_muonTrig2015");
        ATH_MSG_INFO("Using the following for 2015 muon trigger SF: " << m_SF_muonTrig2015);
    }

    if (m_config->has("SF_muonTrig2016")) {
        m_SF_muonTrig2016 = m_config->get<std::string>("SF_muonTrig2016");
        ATH_MSG_INFO("Using the following for 2016 muon trigger SF: " << m_SF_muonTrig2016);
    }

    // sanity check on the strings
    if (m_doMuonTrigSF) {
        if (m_SF_muonTrig2015 == "") {
            ATH_MSG_FATAL("You've turned on muon trigger SFs but did not specify the 2015 string for the SF tool");
        }

        if (m_SF_muonTrig2016 == "") {
            ATH_MSG_FATAL("You've turned on muon trigger SFs but did not specify the 2016 string for the SF tool");
        }
    }

    // Load any events we might want to pick - needed if you want to pick a subset of events
    if (m_eventNumbersFilename != "" and fileExists(m_eventNumbersFilename)) {
        ATH_MSG_INFO("Reading event numbers from " << m_eventNumbersFilename);
        std::ifstream f(m_eventNumbersFilename);
        std::copy(std::istream_iterator<unsigned int>(f), std::istream_iterator<unsigned int>(),
                  std::back_inserter(m_eventNumbersToUse));

        ATH_MSG_INFO("Loaded " << m_eventNumbersToUse.size() << " event numbers");
    }

    // Initialize the GRL
    m_GRL = CxxUtils::make_unique<GoodRunsListSelectionTool>("GoodRunsListSelectionTool");
    auto filenames = std::vector<std::string>();
    if (m_config->has("GRL")) {
        auto _filenames = Utils::splitString(m_config->get("GRL"), " ");
        std::transform(begin(_filenames), end(_filenames), std::back_inserter(filenames),
                       std::bind1st(std::plus<std::string>(), "$WorkDir_DIR/data/LFV_Z/"));
    }

    if (filenames.size() > 0) {
        EL_RETURN_CHECK("EventSelection::initialize()", m_GRL->setProperty("GoodRunsListVec", filenames));
        EL_RETURN_CHECK("EventSelection::initialize()", m_GRL->setProperty("PassThrough", false));

        ATH_MSG_INFO("Using the following files for the GRL:");
        std::copy(filenames.begin(), filenames.end(), std::ostream_iterator<std::string>(this->msg(), "\n"));
    } else {
        ATH_MSG_WARNING("No GRL files specified; forcing the GRL to pass-through mode");
        EL_RETURN_CHECK("EventSelection::initialize()", m_GRL->setProperty("PassThrough", true));
    }
    m_GRL->msg().setLevel(this->msg().level());
    EL_RETURN_CHECK("EventSelection::initialize()", m_GRL.get()->initialize());

    // Initialize the TauTruthMatching tool
    if (m_doTauTruthMatching) {
        m_tauTruthMatchingTool = std::make_shared<TauAnalysisTools::TauTruthMatchingTool>("TauTruthMatchingTool");
        EL_RETURN_CHECK("EventSelection::initialize()", m_tauTruthMatchingTool->setProperty("MaxDeltaR", 0.2));
        EL_RETURN_CHECK("EventSelection::initialize()", m_tauTruthMatchingTool->setProperty("WriteTruthTaus", true));
        // m_tauTruthMatchingTool->msg().setLevel(MSG::VERBOSE);
        EL_RETURN_CHECK("EventSelection::initialize()", m_tauTruthMatchingTool.get()->initialize());
    }

    // Initialize the TauSpinner tools
    m_TST_xAODEventReaderTool = new TauSpinnerTools::xAODEventReaderTool("TST_xAODEventReaderTool");
    m_TST_xAODEventReaderTool->msg().setLevel(MSG::INFO);
    EL_RETURN_CHECK("EventSelection::initialize()", m_TST_xAODEventReaderTool->setProperty("DoDeBrem", false));
    EL_RETURN_CHECK("EventSelection::initialize()", m_TST_xAODEventReaderTool->setProperty("IsLFVZprime", true));
    EL_RETURN_CHECK("EventSelection::initialize()", m_TST_xAODEventReaderTool->initialize());
    m_TST_readerHandle = ToolHandle<TauSpinnerTools::IEventReaderToolBase>(m_TST_xAODEventReaderTool);
    
    m_TST_StandardModelTool = std::make_shared<TauSpinnerTools::StandardModelTool>("TST_StandardModelTool");
    m_TST_StandardModelTool->msg().setLevel(MSG::INFO);
    EL_RETURN_CHECK("EventSelection::initialize()", m_TST_StandardModelTool->setProperty("CMSENE", 13000.));
    EL_RETURN_CHECK("EventSelection::initialize()", m_TST_StandardModelTool->setProperty("PDFname", "NNPDF23_lo_as_0130_qed"));
    EL_RETURN_CHECK("EventSelection::initialize()", m_TST_StandardModelTool->setProperty("EventReaderTool", m_TST_readerHandle));
    EL_RETURN_CHECK("EventSelection::initialize()", m_TST_StandardModelTool.get()->initialize());

    // Initialize the MMC
    m_MMC = CxxUtils::make_unique<MissingMassTool>("MMC");
    EL_RETURN_CHECK("EventSelection::initialize()", m_MMC->setProperty("CalibSet", "LFV"));
    EL_RETURN_CHECK("EventSelection::initialize()", m_MMC->setProperty("alg_version", 4));
    EL_RETURN_CHECK("EventSelection::initialize()", m_MMC->setProperty("NiterFit2", 40));
    EL_RETURN_CHECK("EventSelection::initialize()", m_MMC->setProperty("UseTauProbability", 1));
    if (m_verbose) {
        EL_RETURN_CHECK("EventSelection::initialize()", m_MMC->setProperty("UseVerbose", 1));
    }
    EL_RETURN_CHECK("EventSelection::initialize()", m_MMC.get()->initialize());
    
    // Initialise DeltaROverlapTool for custom ph-mu and ph-el overlap removal (DR < 0.2)
    m_DeltaROverlapTool = CxxUtils::make_unique<ORUtils::DeltaROverlapTool>("DeltaROverlapTool");
    EL_RETURN_CHECK("EventSelection::initialize()", m_DeltaROverlapTool->setProperty("DR", 0.2));
    EL_RETURN_CHECK("EventSelection::initialize()", m_DeltaROverlapTool->setProperty("OutputLabel", "passOR"));
    EL_RETURN_CHECK("EventSelection::initialize()", m_DeltaROverlapTool->setProperty("OutputPassValue", true));
    EL_RETURN_CHECK("EventSelection::initialize()", m_DeltaROverlapTool.get()->initialize());

    // Initialise the Sherpa weight tool
    m_PMGSherpaTool = CxxUtils::make_unique<PMGTools::PMGSherpa22VJetsWeightTool>("PMGWeightTool");
    EL_RETURN_CHECK("EventSelection::initialize()", m_PMGSherpaTool.get()->initialize());

    // We don't need to recalculate electron likelihood anymore, use the default flags
    // (Actually we *can't* always recalculate in new derivations due to trimmed variables)
    /*
    // Set up a bunch of electron LH tools
    // TODO: move to function & own file
    for (EleLH_itr itr = EleLH::EL_LH_LOOSE; itr != EleLH_itr(); itr++) {
        std::string toolname = ("AsgElectronLikelihoodTool" + to_string(*itr));
        ATH_MSG_INFO("Will initialize " << toolname);
        m_AsgElectronLikelihoodTool_map[*itr] = CxxUtils::make_unique<AsgElectronLikelihoodTool>(toolname);
        EL_RETURN_CHECK("EventSelection::initialize()",
                        m_AsgElectronLikelihoodTool_map[*itr]->setProperty("primaryVertexContainer", "PrimaryVertices"));

        std::string config_file;
        switch (*itr) {
            case EleLH::EL_LH_LOOSE:
                config_file = "ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronLikelihoodLooseOfflineConfig2015.conf";
                break;
            case EleLH::EL_LH_MEDIUM:
                config_file = "ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronLikelihoodMediumOfflineConfig2015.conf";
                break;
            case EleLH::EL_LH_TIGHT:
                config_file = "ElectronPhotonSelectorTools/offline/mc15_20160113/ElectronLikelihoodTightOfflineConfig2015.conf";
                break;
            default:
                ATH_MSG_FATAL("AsgElectronLikelihoodTool: invalid enum value %i" << static_cast<int>(*itr));
                break;
        }

        EL_RETURN_CHECK("EventSelection::initialize()",
                        m_AsgElectronLikelihoodTool_map[*itr]->setProperty("ConfigFile", config_file));

        EL_RETURN_CHECK("EventSelection::initialize()", m_AsgElectronLikelihoodTool_map[*itr].get()->initialize());
    }
    */

    // Set up a bunch of isolation selection tools
    for (IsoSel_itr itr(IsoSel::ISO_UNDEFINED); itr != IsoSel_itr(); itr++) {
        std::string toolname = "IsolationSelectionTool_" + to_string(*itr);
        ATH_MSG_INFO("Will initialize " << toolname);
        m_IsolationSelectionTool_map[*itr] = CxxUtils::make_unique<CP::IsolationSelectionTool>(toolname);
        EL_RETURN_CHECK("EventSelection::initialize()",
                        m_IsolationSelectionTool_map[*itr]->setProperty("ElectronWP", toElIsoWp(*itr)));
        EL_RETURN_CHECK("EventSelection::initialize()",
                        m_IsolationSelectionTool_map[*itr]->setProperty("MuonWP", toMuIsoWp(*itr)));
        EL_RETURN_CHECK("EventSelection::initialize()", m_IsolationSelectionTool_map[*itr].get()->initialize());
    }
    
    // Connect the event and the TStore
    m_event = wk()->xaodEvent();
    m_store = xAOD::TActiveStore::store();

    // Disable the use of truth taus if we turn off the matching (USE AT YOUR OWN RISK)
    if (m_getTaus and not m_doTauTruthMatching) {
        ATH_MSG_WARNING(
            "Turning off tau truth matching for the tau smearing tool - use only if your derivation lacks truth taus");

        m_tauSmearingTool.setTypeAndName("TauAnalysisTools::TauSmearingTool/TauSmearingTool");
        EL_RETURN_CHECK("EventSelection::initialize()", m_tauSmearingTool.setProperty("SkipTruthMatchCheck", true));
        EL_RETURN_CHECK("EventSelection::initialize()", m_tauSmearingTool.retrieve());
        // tauSmearingTool.isUserConfigured();

        // if ( asg::ToolStore::contains<TauAnalysisTools::TauSmearingTool>("TauSmearingTool") ) {
        // auto tauSmearingTool = asg::ToolStore::get<TauAnalysisTools::TauSmearingTool>("TauSmearingTool");
        // EL_RETURN_CHECK("EventSelection::initialize()", tauSmearingTool->setProperty("SkipTruthMatchCheck", true ));
        //}
    }
    
    if (not m_doTauTruthMatching and m_doTauSF) {
        ATH_MSG_WARNING("Can't get tau SFs without truth matching");
        return EL::StatusCode::FAILURE;
    }
    
    // Initialize SUSY tools and pass it the kinematic systematics
    if (initializeSUSYTools() != EL::StatusCode::SUCCESS) {
        return EL::StatusCode::FAILURE;
    }
    
    // get output file
    m_outputFile = wk()->getOutputFile(m_outputLabel);
    ATH_MSG_INFO("EventSelection::Initialize(): will use file " << m_outputFile->GetPath());

    // Get the PRW handle
    m_PRWTool.setTypeAndName("CP::PileupReweightingTool/PrwTool");
    m_PRWTool.isUserConfigured();
    EL_RETURN_CHECK("EventSelection::initialize()", m_PRWTool.retrieve());

    // Get the tau selection tool
    std::string m_tauId = "medium";
    m_tauSelectionTool.setTypeAndName("TauAnalysisTools::TauSelectionTool/TauSelectionTool_" + m_tauId);
    m_tauSelectionTool.isUserConfigured();
    EL_RETURN_CHECK("EventSelection::initialize()", m_tauSelectionTool.retrieve());

    // Initialise systematics that were asked for
    initializeSystematics();
    for (const auto& sys : m_systInfoList) {
        ATH_MSG_DEBUG("systematics: " << sys.systset.name());
    }

    // Build the view containers
    m_viewElemMuonContainer = CxxUtils::make_unique<xAOD::MuonContainer>(SG::VIEW_ELEMENTS);
    m_viewElemElectronContainer = CxxUtils::make_unique<xAOD::ElectronContainer>(SG::VIEW_ELEMENTS);
    m_viewElemPhotonContainer = CxxUtils::make_unique<xAOD::PhotonContainer>(SG::VIEW_ELEMENTS);
    m_viewElemJetContainer = CxxUtils::make_unique<xAOD::JetContainer>(SG::VIEW_ELEMENTS);
    m_viewElemTauContainer = CxxUtils::make_unique<xAOD::TauJetContainer>(SG::VIEW_ELEMENTS);

    if (not m_config->has("selection")) {
        ATH_MSG_FATAL("Can't find desired selection class in configuration!");
    }

    auto selection = m_config->get<std::string>("selection");

    std::string treePrefix = wk()->metaData()->castString("treePrefix");
    if (!boost::algorithm::ends_with(treePrefix, "_")) {
        treePrefix += "_";
    }

    ATH_MSG_INFO("EventSelection::Initialize(): loaded tree prefix '" << treePrefix << "'");

    // Did you overrule it from the command line?
    if (m_config->has("outputTreePrefix")) {
        auto s = m_config->get("outputTreePrefix");
        if (s.size() > 0) {
            treePrefix = s;
            ATH_MSG_INFO("EventSelection::Initialize(): overriding tree prefix to '" << treePrefix << "'");
        }
    }

    // Now construct the region class through its builder
    if (selection == "LFV") {
        m_region = RegionBuilder<LFVSelection>()
                       //.setPrefix(m_config->get("outputTreePrefix"))
                       .setPrefix(treePrefix)
                       .setOutputFile(m_outputFile)
                       .setStore(m_store)
                       .setEvent(m_event)
                       .setWorker(wk())
                       .setTheorySystematics(m_sherpa_theory_syst)
                       .setSystematics(m_systInfoList)
                       .setSUSYTools(m_SUSYTools)
                       .build();
    } else if (selection == "LFVLep") {
        m_region = RegionBuilder<LFVLepSelection>()
                       //.setPrefix(m_config->get("outputTreePrefix"))
                       .setPrefix(treePrefix)
                       .setOutputFile(m_outputFile)
                       .setStore(m_store)
                       .setEvent(m_event)
                       .setWorker(wk())
                       .setTheorySystematics(m_sherpa_theory_syst)
                       .setSystematics(m_systInfoList)
                       .setSUSYTools(m_SUSYTools)
                       .build();
    }

    // Get all trigger variables from the region
    m_trigger_items = m_region->GetTriggersFinal();
    // for(const auto s: m_trigger_items){
    // std::cout << s << std::endl;
    //}

    // assign default trigger decisions to m_trigger_decisions <string, bool>
    auto trig_ini_val = (Config::getInstance()->get<bool>("trigger_decision_tool_calculate") and
                         Config::getInstance()->get<bool>("trigger_decision_tool_apply"))
                            ? false
                            : true;
    std::vector<bool> tmp_trigger_decisions(m_trigger_items.size(), trig_ini_val);
    std::transform(m_trigger_items.begin(), m_trigger_items.end(), tmp_trigger_decisions.begin(),
                   std::inserter(m_trigger_decisions, m_trigger_decisions.end()),
                   std::make_pair<std::string const&, bool const&>);

    Histograms& hists = Histograms::getInstance();
    hists.metadata = new TH1D("metadata", "Metadata information", 15, -0.5, 14.5);

    // monitoring histograms pT
    hists.monHist_muon_pt = new TH1D("monHist_muon_pt", "Good muons;#font[42]{p}_{T}^{#mu} [GeV]", 100, 0, 150);
    hists.monHist_muon_pt.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_ele_pt = new TH1D("monHist_ele_pt", "Good electrons;#font[42]{p}_{T}^{e} [GeV]", 100, 0, 150);
    hists.monHist_ele_pt.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_tau_pt = new TH1D("monHist_tau_pt", "Good hadronic taus;#font[42]{p}_{T}^{#tau} [GeV]", 100, 0, 150);
    hists.monHist_tau_pt.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_jet_pt = new TH1D("monHist_jet_pt", "Good jets;#font[42]{p}_{T}^{jet} [GeV]", 100, 0, 150);
    hists.monHist_jet_pt.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_ETmiss = new TH1D("monHist_ETmiss", "Calibrated missing ET;#font[42]{E}_{T}^{miss} [GeV]", 100, 0, 150);
    hists.monHist_ETmiss.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_track_ETmiss =
        new TH1D("monHist_track_ETmiss", "Calibrated track missing ET;#font[42]{E}_{T}^{miss} [GeV]", 100, 0, 150);
    hists.monHist_track_ETmiss.cloneForSystematics(m_systInfoList);  // clone for all systematic variations

    // monitoring histograms pT vs. eta
    hists.monHist_muon_pt_eta =
        new TH2D("monHist_muon_pt_eta", "Good muons;#font[42]{p}_{T}^{#mu} [GeV];#font[42]{#eta}({#mu})", 100, 0, 150, 50, -5, 5);
    hists.monHist_muon_pt_eta.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_ele_pt_eta =
        new TH2D("monHist_ele_pt_eta", "Good electrons;#font[42]{p}_{T}^{e} [GeV];#font[42]{#eta}({e})", 100, 0, 150, 50, -5, 5);
    hists.monHist_ele_pt_eta.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_tau_pt_eta = new TH2D(
        "monHist_tau_pt_eta", "Good hadronic taus;#font[42]{p}_{T}^{#tau} [GeV];#font[42]{#eta}({#tau})", 100, 0, 150, 50, -5, 5);
    hists.monHist_tau_pt_eta.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_jet_pt_eta =
        new TH2D("monHist_jet_pt_eta", "Good jets;#font[42]{p}_{T}^{jet} [GeV];#font[42]{#eta}({jet})", 100, 0, 150, 50, -5, 5);
    hists.monHist_jet_pt_eta.cloneForSystematics(m_systInfoList);  // clone for all systematic variations

    // monitoring histograms MET phi
    hists.monHist_ETmiss_phi =
        new TH1D("monHist_ETmiss_phi", "Calibrated missing ET;#font[42]{#phi}(E_{T}^{miss})", 50, 0, TMath::Pi());
    hists.monHist_ETmiss_phi.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_track_ETmiss_phi =
        new TH1D("monHist_track_ETmiss_phi", "Calibrated track missing ET;#font[42]{#phi}(E_{T}^{miss})", 50, 0, TMath::Pi());
    hists.monHist_track_ETmiss_phi.cloneForSystematics(m_systInfoList);  // clone for all systematic variations

    // monitoring histograms phi vs. eta
    hists.monHist_muon_eta_phi = new TH2D("monHist_muon_eta_phi", "Good muons;#font[42]{#eta}(#mu);#font[42]{#phi}(#mu)", 50,
                                          -5.0, 5.0, 50, 0, TMath::Pi());
    hists.monHist_muon_eta_phi.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_ele_eta_phi = new TH2D("monHist_ele_eta_phi", "Good electrons;#font[42]{#eta}(e);#font[42]{#phi}(e)", 50, -5.0,
                                         5.0, 50, 0, TMath::Pi());
    hists.monHist_ele_eta_phi.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_tau_eta_phi = new TH2D("monHist_tau_eta_phi", "Good hadronic taus;#font[42]{#eta}(#tau);#font[42]{#phi}(#tau)",
                                         50, -5.0, 5.0, 50, 0, TMath::Pi());
    hists.monHist_tau_eta_phi.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_jet_eta_phi =
        new TH2D("monHist_jet_eta_phi", "Good jets;#font[42]{#eta}(jet);#font[42]{#phi}(jet)", 50, -5.0, 5.0, 50, 0, TMath::Pi());
    hists.monHist_jet_eta_phi.cloneForSystematics(m_systInfoList);  // clone for all systematic variations

    // monitoring histogram jet pT no overlap removal
    hists.monHist_muon_no_OLR_pt =
        new TH1D("monHist_muon_no_OLR_pt", "Good muons (no overlap removal);#font[42]{p}_{T}^{#mu} [GeV]", 100, 0, 150);
    hists.monHist_muon_no_OLR_pt.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_ele_no_OLR_pt =
        new TH1D("monHist_ele_no_OLR_pt", "Good electrons (no overlap removal);#font[42]{p}_{T}^{e} [GeV]", 100, 0, 150);
    hists.monHist_ele_no_OLR_pt.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_tau_no_OLR_pt =
        new TH1D("monHist_tau_no_OLR_pt", "Good hadronic taus (no overlap removal);#font[42]{p}_{T}^{#tau} [GeV]", 100, 0, 150);
    hists.monHist_tau_no_OLR_pt.cloneForSystematics(m_systInfoList);  // clone for all systematic variations
    hists.monHist_jet_no_OLR_pt =
        new TH1D("monHist_jet_no_OLR_pt", "Good jets (no overlap removal);#font[42]{p}_{T}^{jet} [GeV]", 100, 0, 150);
    hists.monHist_jet_no_OLR_pt.cloneForSystematics(m_systInfoList);  // clone for all systematic variations

    /** Store all histograms in the output stream
     * @note this must be called only once!
     */
    if (hists.set_directory(m_outputFile) != EL::StatusCode::SUCCESS) {
        ATH_MSG_FATAL("Histograms::set_directory() failed");
        return EL::StatusCode::FAILURE;
    }
    
    auto current_dir = gDirectory;
    m_outputFile->cd();
    SH::MetaData<std::string>("submitted_at", m_submitted_at).Write();
    SH::MetaData<std::string>("submitted_by", m_submitted_by).Write();
    SH::MetaData<std::string>("submitted_type", m_submitted_type).Write();
    SH::MetaData<std::string>("submitted_revision", m_submitted_revision).Write();
    current_dir->cd();

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventSelection::execute() {
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.

    //// Terry: Deprecated??
    //// for muon trigger SF
    //if (!m_SUSYTools->setRunNumber(284285).isSuccess()) {
    //    throw std::runtime_error("Could not set reference run number in SUSYTools !");
    //}
    
    // We don't do it this way anymore (AnalysisBase 21.2.38)
    // See EventSelection::getTaus() and EventSelection::getPhotons()
    //if (!m_getTaus) {
    //    // this gets deleted once we clear the store so we have to make them per event
    //    m_empty_taus_base_aux = new xAOD::AuxContainerBase();
    //    m_empty_taus_base->setStore(m_empty_taus_base_aux);  //< Connect the two
    //}
    //
    //if (!m_getPhotons) {
    //    m_empty_photons_base_aux = new xAOD::AuxContainerBase();
    //    m_empty_photons_base->setStore(m_empty_photons_base_aux);  //< Connect the two
    //}

    // xAOD::TEvent* event = wk()->xaodEvent();
    m_event = wk()->xaodEvent();
    m_region->setEvent(m_event);

    Histograms& hists = Histograms::getInstance();

    // Cutflow in the selection class - _not_ the common cutflow
    Output<TH1D>& cutflow = hists.cutflow_LFV;
    auto selection = m_config->get<std::string>("selection");
    if (selection == "LFVLep") {
        cutflow = hists.cutflow_LFVLep;
    }

    ++m_nEventsProcessed;

    hists.cutflow_common[0]->Fill(1);

    // Event initializations
    m_eventWeight = 1.0;  // total event weight
    m_mcWeight = 1.0;     // mc event weight

    // event info
    m_eventInfo = 0;
    if (!m_event->retrieve(m_eventInfo, "EventInfo").isSuccess()) {
        throw std::runtime_error("ObjectBuilder: Could not retrieve EventInfo");
    }

    // const bool isMC = isSimulation();
    m_isMC = isSimulation();
    // ATH_MSG_DEBUG("Running on simulation? " << m_isMC);
    if (m_config->get<bool>("isData") == m_isMC) {
        ATH_MSG_FATAL("Config file says isData=" << m_config->get<bool>("isData") << " but eventInfo gives MC=" << m_isMC);
        return EL::StatusCode::FAILURE;
    }

    if (!m_isMC) {
        m_usePileupWeight = false;
    }// else {
    //    try {
    //        if (m_PRWTool->expert()->GetUnrepresentedDataFraction((Int_t)284500, m_eventInfo->mcChannelNumber()) > 0.05) {
    //            ATH_MSG_INFO("This is an unsupported channel -> will not use pileup weights for this");
    //            m_usePileupWeight = false;
    //        }
    //
    //    } catch (const std::exception& ex) {
    //        ATH_MSG_INFO("This is an unsupported channel -> will not use pileup weights for this");
    //        m_usePileupWeight = false;
    //    }
    //}

    // retrieve the mc weight
    const std::vector<float> weights = m_isMC ? m_eventInfo->mcEventWeights() : std::vector<float>({});
    m_mcWeight = weights.size() > 0 ? weights[0] : 1.;
    m_eventWeight *= m_mcWeight;  // update total event weight

    // Counter for dxAOD cutflow hists
    m_nMCWeightedEvents_thisFile += m_mcWeight;

    // veto the event if we have a non-empty list of numbers to pick and the number is in the list
    if (m_isMC) {
        EL_CONTINUE(!m_eventNumbersToUse.empty() &&
                    std::find(m_eventNumbersToUse.begin(), m_eventNumbersToUse.end(), m_eventInfo->eventNumber()) ==
                        m_eventNumbersToUse.end());

        if (!m_eventNumbersToUse.empty()) {
            if (wk()->treeEntry() % 250 != 0) {
                ATH_MSG_INFO("--------------------------");
                ATH_MSG_INFO("Read event number " << wk()->treeEntry() << " / " << m_event->getEntries()
                                                  << " (total processed = " << m_nEventsProcessed << ")");
                ATH_MSG_INFO("--------------------------");
            }
            ATH_MSG_DEBUG("Picked event number " << m_eventInfo->eventNumber());
        }
    }
    
    if (this->msg().level() <= MSG::DEBUG or wk()->treeEntry() % 250 == 0) {
        // if (wk()->treeEntry() % 250 == 0) {
        ATH_MSG_INFO("--------------------------");
        ATH_MSG_INFO("Read event number " << wk()->treeEntry() << " / " << m_event->getEntries()
                                          << " (total processed = " << m_nEventsProcessed << ")");
        ATH_MSG_INFO("--------------------------");
    }


    // make to use no systematics
    if (m_SUSYTools->resetSystematics() != CP::SystematicCode::Ok) {
        ATH_MSG_ERROR("Cannot reset SUSYTools systematics");
    }

    if (m_isMC && Config::getInstance()->has("doPileupReweighting") and Config::getInstance()->get<bool>("doPileupReweighting")) {
        try {
            EL_RETURN_CHECK("EventSelection::execute()", m_SUSYTools->ApplyPRWTool());
        } catch (std::runtime_error& e) {
            ATH_MSG_WARNING("ApplyPRWTool() failed - turning off PRW and scale factors");
            Config::getInstance()->set("doPileupReweighting", false);
            Config::getInstance()->set("doScaleFactors", false);
            // HACK - SUSYTools doesn't expect this if there's a PRW tool pass
             m_eventInfo->auxdecor<unsigned int>("RandomRunNumber") = 0;
        }
    }

    // Primary vertices
    m_primaryVertexFound = false;
    m_vertices = nullptr;

    // TODO truth-only if-statement: vertices not available in truth only
    EL_RETURN_CHECK("EventSelection::execute()", m_event->retrieve(m_vertices, "PrimaryVertices"));

    // TODO: decorate grl_pass_run_lb ?
    if (not m_isMC) {
        EL_CONTINUE(not passGRL());
    }

    hists.cutflow_common[0]->Fill(2);

    if (not m_isMC) {
        // check for bad DQ or non-collision bkg
        const bool isBad = not eventCheck();
        if (isBad) {
            ATH_MSG_DEBUG("Vetoing on eventCheck()");
        }
        EL_CONTINUE(isBad);
    }

    hists.cutflow_common[0]->Fill(3);

    // TODO: not if truth
    if (not primaryVertexCheck()) {
        ATH_MSG_DEBUG("No primary vertex - skipping event!");
        return EL::StatusCode::SUCCESS;
    }

    hists.cutflow_common[0]->Fill(4);

    // Retrieve the truth particles
    m_truthEventContainer = nullptr;
    m_truthParticleContainer = nullptr;
    m_truthElectronContainer = nullptr;
    m_truthMuonContainer = nullptr;
    m_truthTauContainer = nullptr;
    m_truthJetContainer = nullptr;
    m_truthMETContainer = nullptr;
    m_truthMET = nullptr;
    if (m_isMC) {
        EL_RETURN_CHECK("EventSelection::execute()", m_event->retrieve(m_truthEventContainer, "TruthEvents"));
        EL_RETURN_CHECK("EventSelection::execute()", m_event->retrieve(m_truthParticleContainer, "TruthParticles"));
        EL_RETURN_CHECK("EventSelection::execute()", m_event->retrieve(m_truthMETContainer, "MET_Truth"));
        m_truthMET = (*m_truthMETContainer)["NonInt"];
        if (m_truthMET == nullptr) {
            ATH_MSG_ERROR("Can't find truth MET!");
            return EL::StatusCode::FAILURE;
        }

        xAOD::TruthEventContainer::const_iterator itr = m_truthEventContainer->begin();
        for (float weight : ((*itr)->weights())) {
            // Do whatever with the weights
            // std::cout << weight << std::endl;
            m_LHE3weights.push_back(weight);
        }
        // std::cout << "-----------" << std::endl;

        // get the truth containers for electrons, muons and taus if available.
        if (m_event->contains<xAOD::TruthParticleContainer>("TruthElectrons")) {
            EL_RETURN_CHECK("EventSelection::execute()", m_event->retrieve(m_truthElectronContainer, "TruthElectrons"));
        }
        if (m_event->contains<xAOD::TruthParticleContainer>("TruthMuons")) {
            EL_RETURN_CHECK("EventSelection::execute()", m_event->retrieve(m_truthMuonContainer, "TruthMuons"));
        }

        if (m_doTauTruthMatching) {
            if (m_event->contains<xAOD::TruthParticleContainer>("TruthTaus")) {
                EL_RETURN_CHECK("EventSelection::execute()", m_event->retrieve(m_truthTauContainer, "TruthTaus"));
            } else {
                ATH_MSG_DEBUG("Will ask TauTruthMatchingTool to make truth tau container");
                m_truthTauContainer = m_tauTruthMatchingTool->getTruthTauContainer();
            }
        }

        if (m_event->contains<xAOD::JetContainer>("TruthJets")) {
            EL_RETURN_CHECK("EventSelection::execute()", m_event->retrieve(m_truthJetContainer, "TruthJets"));
        }
    }

    // Retrieve the TrackParticle container, if asked for (usually "InDetTrackParticles")
    m_trackParticleContainer = nullptr;
    if (!m_trackParticleContainerKey.empty()) {
        ATH_MSG_DEBUG("Will retrieve tracks from " << m_trackParticleContainerKey);
        EL_RETURN_CHECK("EventSelection::execute()", m_event->retrieve(m_trackParticleContainer, m_trackParticleContainerKey));
    }

    // fill the trigger info
    if (Config::getInstance()->get<bool>("trigger_decision_tool_calculate")) {
        for (auto& kv : m_trigger_decisions) {
            auto condition = TrigDefs::Physics;
            if (Config::getInstance()->get<bool>("trigger_allow_resurrected_decision")) {
                condition = TrigDefs::Physics | TrigDefs::allowResurrectedDecision;
            }
            kv.second = m_SUSYTools->IsTrigPassed(kv.first, condition);
        }

        unsigned int nFired = std::count_if(m_trigger_decisions.begin(), m_trigger_decisions.end(),
                                            std::mem_fn(&decltype(m_trigger_decisions)::value_type::second));

        // print trigger results
        if (Config::getInstance()->get<bool>("trigger_print_results")) {
            ATH_MSG_INFO("Trigger decision map : " << nFired << "/" << m_trigger_items.size());

            // NB: ATH_MSG_INFO will fail because 'this' can't be derived in std::foreach loops correctly
            std::for_each(m_trigger_decisions.begin(), m_trigger_decisions.end(), [](const std::pair<std::string, bool>& p) {
                std::cout << "\tChain: " << p.first << " -> " << p.second << std::endl;
            });
        }

        if (Config::getInstance()->get<bool>("trigger_decision_tool_apply")) {
            EL_CONTINUE(nFired == 0);
        } else if (nFired == 0) {
            ATH_MSG_DEBUG("No triggers fired, but trigger_decision_tool_apply is false - passing through");
        }
    }

    // Fill trigger cutflow
    hists.cutflow_common[0]->Fill(5);

    // Get some sherpa weights
    m_eventInfo->auxdecor<float>("sherpa_weight") = 1.0;

    if (m_isMC) {
        const auto mcChannel = m_eventInfo->mcChannelNumber();

        const bool isZtautau = (mcChannel >= 363102 && mcChannel <= 363121) || (mcChannel >= 363361 && mcChannel <= 363363) || (mcChannel >= 344772 && mcChannel <= 344782);
        const bool isZmumu = (mcChannel >= 363364 && mcChannel <= 363387);
        const bool isZee = (mcChannel >= 363388 && mcChannel <= 363411);
        const bool isZ = isZtautau || isZee || isZmumu;

        const bool isWtaunu = (mcChannel >= 363331 && mcChannel <= 363354);
        const bool isWenu = (mcChannel >= 363460 && mcChannel <= 363483);
        const bool isWmunu = (mcChannel >= 363436 && mcChannel <= 363459);
        const bool isW = isWtaunu || isWenu || isWmunu;

        if (isZ || isW) {
            const auto sherpa_weight = m_PMGSherpaTool->getWeight();
            ATH_MSG_DEBUG("sherpa weight: " << sherpa_weight);
            m_eventInfo->auxdecor<float>("sherpa_weight") = sherpa_weight;
        }
    }

    for (const auto& sys : m_systInfoList) {
        // Write out the nominal and systematics that affect kinematics
        bool storeVariation = (SystematicFunctions::IsNominal(sys.systset) || sys.affectsKinematics);
        if (not storeVariation) {
            // don't do this for weights - they're processed differently
            continue;
        }

        ATH_MSG_DEBUG("At systematic: " << SystematicFunctions::RenamingRule(sys.systset.name()));

        m_region->ResetVariables();
        hists.setCurrentSystematic(sys);

        // Process pileup, if asked for
        if (Config::getInstance()->has("doPileupReweighting") and Config::getInstance()->get<bool>("doPileupReweighting")) {
            if (m_isMC) {
                ATH_MSG_DEBUG("Calling processPileup()");
                EL_RETURN_CHECK("EventSelection::execute()", processPileup(sys));
            } else if (storeVariation) {
                m_eventInfo->auxdecor<float>("pileup_corrected_averageInteractionsPerCrossing") =
                    m_PRWTool->getCorrectedAverageInteractionsPerCrossing(*m_eventInfo, false);
            }
        }

        // Update the total event weight
        if (SystematicFunctions::IsNominal(sys.systset)) {
            m_nTotalWeightedEvents += m_eventWeight;
        }

        // Get all objects
        processEvent(sys);  // TODO: rename

        // Check for bad jets
        bool haveBadJets = checkBadJets(storeVariation);
        ATH_MSG_DEBUG("haveBadJets = " << haveBadJets);
        if (haveBadJets) {
            // Clear the view containers
            m_viewElemMuonContainer->clear();
            m_viewElemTauContainer->clear();
            m_viewElemElectronContainer->clear();
            m_viewElemPhotonContainer->clear();
            m_viewElemJetContainer->clear();

            // Delete the dummy taus/photons shallow aux containers created by getTaus()/getPhotons()
            //delete m_empty_taus_shallow_aux;
            //delete m_empty_photons_shallow_aux;

            // Containers registered with TStore/TEvent cannot be deleted.
            // Clear transient store instead.
            m_store->clear();

            continue;
        }

        if (SystematicFunctions::IsNominal(sys.systset)) {
            hists.cutflow_common[0]->Fill(6);
        }

        FillBranchesArgs args{
            m_eventInfo,
            m_viewElemMuonContainer.get(),
            m_viewElemElectronContainer.get(),
            m_viewElemPhotonContainer.get(),
            m_viewElemTauContainer.get(),
            m_viewElemJetContainer.get(),
            m_trackParticleContainer,
            {m_primaryVertex, m_vertices},
            {*(m_rebuiltMET->find("Final")), *(m_rebuiltTrackMET->find("Track")), m_truthMET, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {m_truthEventContainer, m_truthParticleContainer, m_truthElectronContainer, m_truthMuonContainer, m_truthTauContainer,
             m_truthJetContainer},
            {m_trigger_decisions},
            m_TST_xAODEventReaderTool,
            m_TST_StandardModelTool.get(),
        };

        bool useEvent = m_region->FillBranches(args);
        if (useEvent) {
            ATH_MSG_DEBUG("Keeping event " << wk()->treeEntry());
            bool isNominal = SystematicFunctions::IsNominal(sys.systset);

            // decorate the object with a bunch of weights
            // NOTE: we only do this here to save time!
            if (m_isMC) {
                processWeightSystematics(not isNominal);  // if we're not the nominal, store only nominal weights
            }

            m_region->FillDeferredBranches(args);
            m_region->FillTree(sys.systset.name());

            // Keep the channel event counter increment at the very end
            if (SystematicFunctions::IsNominal(sys.systset)) {
                ++m_nAcceptedEvents;
            }
        }

        // Clear the view containers
        m_viewElemMuonContainer->clear();
        m_viewElemTauContainer->clear();
        m_viewElemElectronContainer->clear();
        m_viewElemPhotonContainer->clear();
        m_viewElemJetContainer->clear();
    
        // Delete the dummy taus/photons shallow aux containers created by getTaus()/getPhotons()
        //delete m_empty_taus_shallow_aux;
        //delete m_empty_photons_shallow_aux;

        /*! Containers registered with TStore/TEvent cannot be deleted.
         * Clear transient store instead.
         */
        m_store->clear();
    }

    m_region->ResetVariables();
    
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventSelection::postExecute() {
    // Here you do everything that needs to be done after the main event
    // processing.  This is typically very rare, particularly in user
    // code.  It is mainly used in implementing the NTupleSvc.
    
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventSelection::finalize() {
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.  This is different from histFinalize() in that it only
    // gets called on worker nodes that processed input events.

    // ProfilerStop();
    m_region->ResetVariables();
    ATH_MSG_INFO("EventSelection::finalize()");
    
    if (this->msg().level() == MSG::DEBUG or this->msg().level() == MSG::VERBOSE) {
        if(m_TST_xAODEventReaderTool) {
            m_TST_xAODEventReaderTool->summary();
        }
        if(m_TST_StandardModelTool) {
            m_TST_StandardModelTool->summary();
        }
    }

    // Delete the dummy tau and photon containers
    //delete m_empty_photons_base;
    //delete m_empty_taus_base;

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode EventSelection::histFinalize() {
    // This method is the mirror image of histInitialize(), meaning it
    // gets called after the last event has been processed on the worker
    // node and allows you to finish up any objects you created in
    // histInitialize() before they are written to disk.  This is
    // actually fairly rare, since this happens separately for each
    // worker node.  Most of the time you want to do your
    // post-processing on the submission node after all your histogram
    // outputs have been merged.  This is different from finalize() in
    // that it gets called on all worker nodes regardless of whether
    // they processed input events.
    //

    ATH_MSG_INFO("EventSelection::histFinalize()");

    m_viewElemJetContainer.reset();
    m_viewElemJetContainer = nullptr;
    m_viewElemTauContainer.reset();
    m_viewElemTauContainer = nullptr;
    m_viewElemMuonContainer.reset();
    m_viewElemMuonContainer = nullptr;
    m_viewElemElectronContainer.reset();
    m_viewElemElectronContainer = nullptr;
    m_viewElemPhotonContainer.reset();
    m_viewElemPhotonContainer = nullptr;

    /**
     * @brief Fill in event counts and weights
     * @note Attention: Bin with value "n" corresponds to the "n+1"-th bin
     * @note Method initialize() is getting called only if there are events
     * to be analyzed. A protection is added for cases with empty files.
     */

    Histograms& hists = Histograms::getInstance();

    if (m_initialized) {  //!< protection against empty files
        addPreviousFileMetaData();  // The last file processed
        if (m_isDerivation) {
            hists.metadata->Fill(0., m_nFilesProcessed);

            hists.metadata->Fill(1., m_nEventsProcessed);
            hists.metadata->Fill(2., m_nAcceptedEvents);

            hists.metadata->Fill(3., m_nMCWeightedEvents);
            hists.metadata->Fill(4., m_nPUWeightedEvents);
            hists.metadata->Fill(5., m_nTotalWeightedEvents);

            hists.metadata->Fill(6., m_xAOD_initialSumOfEvents);
            hists.metadata->Fill(7., m_xAOD_initialSumOfWeights);
            hists.metadata->Fill(8., m_xAOD_initialSumOfWeightsSquared);

            hists.metadata->Fill(9., m_DxAOD_initialSumOfEvents);
            hists.metadata->Fill(10., m_DxAOD_initialSumOfWeights);
            hists.metadata->Fill(11., m_DxAOD_initialSumOfWeightsSquared);

        } else {
            hists.metadata->Fill(0., m_nFilesProcessed);

            hists.metadata->Fill(1., m_nEventsProcessed);
            hists.metadata->Fill(2., m_nAcceptedEvents);

            hists.metadata->Fill(3., m_nMCWeightedEvents);
            hists.metadata->Fill(4., m_nPUWeightedEvents);
            hists.metadata->Fill(5., m_nTotalWeightedEvents);

            hists.metadata->Fill(6., m_xAOD_initialSumOfEvents);
            hists.metadata->Fill(7., m_xAOD_initialSumOfWeights);
            hists.metadata->Fill(8., m_xAOD_initialSumOfWeightsSquared);
        }

        hists.metadata->GetXaxis()->SetBinLabel(1, "Processed files");
        hists.metadata->GetXaxis()->SetBinLabel(2, "Processed events");
        hists.metadata->GetXaxis()->SetBinLabel(3, "Events passed through selection");
        hists.metadata->GetXaxis()->SetBinLabel(4, "#Sigma MC weighted events");
        hists.metadata->GetXaxis()->SetBinLabel(5, "#Sigma PU weighted events");
        hists.metadata->GetXaxis()->SetBinLabel(6, "#Sigma total weighted events");
        hists.metadata->GetXaxis()->SetBinLabel(7, "xAOD events");
        hists.metadata->GetXaxis()->SetBinLabel(8, "#Sigma xAOD weighted events");
        hists.metadata->GetXaxis()->SetBinLabel(9, "#Sigma xAOD weighted events squared");
        hists.metadata->GetXaxis()->SetBinLabel(10, "DxAOD events");
        hists.metadata->GetXaxis()->SetBinLabel(11, "#Sigma DxAOD weighted events");
        hists.metadata->GetXaxis()->SetBinLabel(12, "#Sigma DxAOD weighted events squared");
    }

    std::cout << "============================================" << std::endl;
    std::cout << " Metadata statistics" << std::endl;
    std::cout << "============================================" << std::endl;
    hists.metadata.print_content_with_labels();
    std::cout << "============================================" << std::endl;
    
    hists.write(m_outputFile);  // And delete
    m_region->WriteTrees(m_outputFile);  // And delete trees

    return EL::StatusCode::SUCCESS;
}
