/**
 * @file SystematicsFunctions.cxx
 *
 * @brief Namespace which defines common functionalities, properties etc
 *
 * Forked from xTauFW's xSystematicsCommon class
 *
 * @author Z. Zinonos - zenon@cern.ch
 * @author Geert-Jan Besjes <geert-jan.besjes@cern.ch>
 *
 * @date  Feb 2016
 */

#include <LFV_Z/SystematicFunctions.h>

#include <string>
#include <vector>

namespace StringUtils {
    /**
     * @short Helper function to replace substrings in strings
     * @param subject input big string
     * @param search input little substring to be replaced
     * @param replace replacement substring
     */
    auto ReplaceSubstr(std::string subject, const std::string &search, const std::string &replace) -> decltype(subject) {
        // replace substring patterns in a string
        size_t pos = 0;
        while ((pos = subject.find(search, pos)) != std::string::npos) {
            subject.replace(pos, search.length(), replace);
            pos += replace.length();
        }

        return subject;
    }

    /**
     * @name Identify prefix substring in trigger name
     * @details input: big string output: substring prefix
     * @return true if string contains the given substring
     */
    auto HasPrefix(const std::string &str, const std::string &prefix) -> bool {
        return (str.size() >= prefix.size() && str.substr(0, prefix.size()).compare(prefix) == 0);
    }

    /**
     * @name Identify substring in string
     * @details input: big string output: substring
     * @return true if string contains the given substring
     */
    auto HasString(const std::string &str, const std::string &sub) -> bool {
        return str.find(sub) != std::string::npos;
    }

}  // namespace

namespace SystematicFunctions {

    using namespace StringUtils;

    /**
     * Name of the nominal tree
     * @note: by default is empty
     */
    const std::string nominal_name{"NOMINAL"};
    const std::vector<std::string> weight_syst = {"ISO", "EFF", "TTVA", "Jvt", "PRW"};

    bool IsVariation(const std::string &str) {
        return !str.empty();
    }

    bool IsVariation(const std::vector<CP::SystematicSet>::const_iterator &iter) {
        return IsVariation(iter->name());
    }

    bool IsWeight(const std::string &s) {
        for (const auto &val : weight_syst) {
            if (HasString(s, val)) return true;
        }

        return false;
    }

    /**
     * @short Naming rules used to adjust the names of systematics
     * @param s input systematic name
     * @return string corresponding the to tree name
     */
    auto RenamingRule(const std::string &s) -> std::string {
        // string.erase(std::remove_if(string.begin(), string.end(), std::isspace), string.end());

        if (s.empty()) return nominal_name;

        auto ss = ReplaceSubstr(s, "__", "_");  //!< ROOT doesn't support double undercore in tree names
        return ReplaceSubstr(ss, " ", "_");     //!< ROOT doesn't support space in tree names
    }

    /**
     * @short Checks if a given systematic set is the nominal point.
     * @input[in] syst The input systematic set.
     * @return True if the systematic set is nominal.
     */
    bool IsNominal(const CP::SystematicSet &syst) {
        return IsNominal(syst.name());
    }

    /**
     * @short Checks if a given iterator of a vector of systematic set is the nominal point.
     * @input[in] iter The input systematic set iterator.
     * @return True if the systematic set is nominal.
     */
    bool IsNominal(const std::vector<CP::SystematicSet>::const_iterator &iter) {
        return IsNominal(iter->name());
    }

    /**
     * @short Checks if a string name of a @c SystematicSet is the nominal point.
     * @input[in] str String of the input systematic set iterator.
     * @return True if the systematic set is nominal.
     */
    bool IsNominal(const std::string &str) {
        return str.empty();
    }

    /**
     * @short Checks if a string contains the name of the nominal case.
     * @input[in] str String of the input string.
     * @return True if the input string has the nominal name.
     */
    bool HasNominalName(const std::string &str) {
        return HasString(str, nominal_name);
    }

    /**
     * @short Checks if a string contains the name of the nominal case.
     * @input[in] str String of the input string.
     * @return True if the input string has the nominal name.
     */
    bool IsNominalName(const std::string &str) {
        return str == nominal_name;
    }

    /**
     * @short Obtain the name of the nominal case
     * @return String of the nominal case.
     */
    auto NominalName() -> decltype(nominal_name) {
        return nominal_name;
    }

    /**
     * @short Naming rules used to adjust the names of systematics
     * @param syst input <code>CP::SystematicSet</code>
     * @return string corresponding the to tree name
     */
    auto RenamingRule(const CP::SystematicSet &syst) -> std::string {
        return RenamingRule(syst.name());
    }
}
