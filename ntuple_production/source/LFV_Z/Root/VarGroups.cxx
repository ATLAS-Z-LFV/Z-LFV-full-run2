// vim: ts=4 sw=4
#include "LFV_Z/VarGroups.h"
#include "CxxUtils/make_unique.h"
#include "LFV_Z/Config.h"
#include "LFV_Z/Enums.h"
#include "LFV_Z/Utils.h"

#include <map>
#include <string>
#include <vector>

#include "xAODTau/TauxAODHelpers.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/xAODTruthHelpers.h"

#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolStore.h"
#include "DiTauMassTools/MissingMassTool.h"
#include "TauAnalysisTools/TauTruthMatchingTool.h"

#include "LHAPDF/LHAPDF.h"

std::shared_ptr<Config> m_config = Config::getInstance();

static SG::AuxElement::ConstAccessor<float> acc_BDTEleScore("BDTEleScore");
static SG::AuxElement::ConstAccessor<float> acc_BDTEleScoreSigTrans("BDTEleScoreSigTrans");
static SG::AuxElement::ConstAccessor<float> acc_BDTEleScore_retuned("BDTEleScore_retuned");
static SG::AuxElement::ConstAccessor<float> acc_BDTEleScoreSigTrans_retuned("BDTEleScoreSigTrans_retuned");
static SG::AuxElement::ConstAccessor<char> acc_BDTEleLoose_retuned("BDTEleLoose_retuned");
static SG::AuxElement::ConstAccessor<char> acc_BDTEleMedium_retuned("BDTEleMedium_retuned");
static SG::AuxElement::ConstAccessor<char> acc_BDTEleTight_retuned("BDTEleTight_retuned");
static SG::AuxElement::ConstAccessor<char> acc_trigmatched("trigmatched");

//-------------------------------------------------------------------
/**
 * @brief: conversion factor to the desired output units
 */
double energyMetric() {
    // convert MeV to GeV
    // TODO: static caching
    if (m_config->has("outputUnits") && m_config->get("outputUnits") == "GeV") return 1e-3;

    // otherwise, do not convert
    return 1;
}

//-------------------------------------------------------------------
FillEventInfo::FillEventInfo(VarList& varList) : VarGroup(varList, "") {}

void FillEventInfo::fill(const xAOD::EventInfo* ei) {
    const bool isData = not ei->eventType(xAOD::EventInfo::IS_SIMULATION);

    // common
    eventNumber = ei->eventNumber();

    if (isData) {
        auto RN = ei->runNumber();
        runNumber = RN;
        lbNumber = ei->lumiBlock();

        grl_pass_run_lb = ei->isAvailable<unsigned int>("grl_pass_run_lb") ? ei->auxdata<unsigned int>("grl_pass_run_lb") : false;

        // to make our life easier, we store the year here as well
        treatAsYear = 2015 + (RN > 284484) + (RN > 311481) + (RN > 340453);
    } else if (not isData) {
        runNumber = ei->mcChannelNumber();

        if (ei->isAvailable<unsigned int>("RandomRunNumber")) {
            auto RRN = ei->auxdata<unsigned int>("RandomRunNumber");
            random_runNumber = RRN;
            treatAsYear = 2015 + (RRN > 284484) + (RRN > 311481) + (RRN > 340453);
        } else {
            random_runNumber = ei->runNumber();
            treatAsYear = 0;
        }

        random_lbNumber =
            ei->isAvailable<unsigned int>("RandomLBNumber") ? ei->auxdata<unsigned int>("RandomLBNumber") : ei->lumiBlock();
        PRWHash = ei->isAvailable<ULong64_t>("pileup_prwhash") ? ei->auxdata<ULong64_t>("pileup_prwhash") : 0;
    }
}
//-------------------------------------------------------------------
FillEventWeights::FillEventWeights(VarList& varList) : VarGroup(varList, "") {
    // TODO
    // weight_pileup = m_jo.pileup_apply_reweighting() ? CxxUtils::make_unique<VarD>(vlist, "weight_pileup") : nullptr;
    // weight_pileup = nullptr;
}

void FillEventWeights::fill(const xAOD::EventInfo* ei) {
    const bool isData = m_config->get<bool>("isData");

    auto mc_w = not isData && ei->eventType(xAOD::EventInfo::IS_SIMULATION) && (ei->mcEventWeights()).size() > 0
                    ? (ei->mcEventWeights())[0]
                    : 1.0;

    auto pu_w = ei->isAvailable<float>(SystematicFunctions::NominalName() + "_pileup_combined_weight")
                    ? ei->auxdata<float>(SystematicFunctions::NominalName() + "_pileup_combined_weight")
                    : 1.0f;

    // normWeight = 1.0;  // this is a dummy later overwritten by a merging script
    auto _sherpaWeight = ei->isAvailable<float>("sherpa_weight") ? ei->auxdata<float>("sherpa_weight") : 1.0;
    sherpaWeight = _sherpaWeight;
    mcWeight = mc_w;
    // pileupWeight = pu_w;
    // totalWeight = mc_w * pu_w * _sherpaWeight;
}
//-------------------------------------------------------------------
void FillMultiplicities::fill(const xAOD::EventInfo* eventInfo, const xAOD::MuonContainer* muons,
                              const xAOD::ElectronContainer* electrons, const xAOD::PhotonContainer* photons,
                              const xAOD::TauJetContainer* taus, const xAOD::JetContainer* jets,
                              const xAOD::VertexContainer* vertices) {
    nMuons = ContainerUtils::Size(muons);
    nElectrons = ContainerUtils::Size(electrons);
    nPhotons = ContainerUtils::Size(photons);
    nTaus = ContainerUtils::Size(taus);
    nTaus_loose = ContainerUtils::Count(taus, "jet_bdt_loose");
    nTaus_medium = ContainerUtils::Count(taus, "jet_bdt_medium");
    nTaus_tight = ContainerUtils::Count(taus, "jet_bdt_tight");
    nJets = ContainerUtils::Size(jets);
    nJets_bad = ContainerUtils::Count(jets, "Bad");
    nBjets = ContainerUtils::Count<char>(jets, "bjet");
    nBjetsLoose = ContainerUtils::Count<char>(jets, "bjet_loose");

    nSignalElectrons = ContainerUtils::Count<char>(electrons, "signal");
    nSignalMuons = ContainerUtils::Count<char>(muons, "signal");
    nSignalTaus = ContainerUtils::Count<char>(taus, "signal");

    nNonIsolatedSignalMuons = ContainerUtils::Count<char>(muons, "QCD");
    nNonIsolatedSignalElectrons = ContainerUtils::Count<char>(electrons, "QCD");

    // TODO: in config?
    const double maxAbsZ = 100.0;
    const unsigned int minNTracks = 2;

    nVertices = VertexUtils::CountVertices(vertices, minNTracks, maxAbsZ);
    nPrimaryVertices = VertexUtils::CountVertices(vertices, minNTracks, maxAbsZ);
    nActualInteractions = eventInfo->actualInteractionsPerCrossing();
    nAverageInteractions = eventInfo->averageInteractionsPerCrossing();

    // store corrected <mu> from tool
    double tmp_avg_int_cor = eventInfo->isAvailable<float>("pileup_corrected_averageInteractionsPerCrossing")
                                 ? eventInfo->auxdata<float>("pileup_corrected_averageInteractionsPerCrossing")
                                 : eventInfo->averageInteractionsPerCrossing();

    // add cosmetic shift of <mu> for data
    if (m_config->get<bool>("isData")) {
        // TODO: config file!!!
        // tmp_avg_int_cor = tmp_avg_int_cor*m_jo.pileup_data_scale_factor();
        // tmp_avg_int_cor = tmp_avg_int_cor / 1.16;
        tmp_avg_int_cor = tmp_avg_int_cor / 1.09;
    }
    nAverageInteractionsCorrected = tmp_avg_int_cor;
}
//-------------------------------------------------------------------
// It is much more useful to fill in position of vertex w.r.t. beam spot
void FillVertex::fill(const TVector3& v, const TVector3& beamPos) {
    vertex = 1;
    x = v.x() - beamPos.X();
    y = v.y() - beamPos.Y();
    z = v.z() - beamPos.Z();
}
void FillVertex::fill(const xAOD::Vertex* v, const TVector3& beamPos) {
    vertex = 1;
    x = v->x() - beamPos.X();
    y = v->y() - beamPos.Y();
    z = v->z() - beamPos.Z();
}
void FillVertex::fill(const xAOD::TruthVertex* v, const TVector3& beamPos) {
    vertex = 1;
    x = v->x() - beamPos.X();
    y = v->y() - beamPos.Y();
    z = v->z() - beamPos.Z();
}
//-------------------------------------------------------------------
void FillParticle::fill(const xAOD::IParticle* p) {
    // fill particle type
    switch (p->type()) {
        case xAOD::Type::Muon:
            particle = 1;
            break;
        case xAOD::Type::Electron:
            particle = 2;
            break;
        case xAOD::Type::Tau:
            particle = 3;
            break;
        case xAOD::Type::Jet:
            particle = 4;
            break;
        case xAOD::Type::TrackParticle:
            particle = 5;
            break;
        case xAOD::Type::TruthParticle:
            particle = 6;
            break;
        default:
            particle = 7;
    }

    // fill variables
    auto p4 = p->p4();
    eta = p4.Eta();
    phi = p4.Phi();
    pt = p4.Pt() * energyMetric();
    et = p4.Et() * energyMetric();
    m = p4.M() * energyMetric();
    q = ParticleUtils::GetCharge(p);


    static SG::AuxElement::ConstAccessor<int> truthOriginAcc("truthOrigin");
    static SG::AuxElement::ConstAccessor<int> truthTypeAcc("truthType");

    if (truthOriginAcc.isAvailable(*p)) {
        origin = truthOriginAcc(*p);
        // std::cout<<" -- particle origin: "<<origin;
    }
    if (truthTypeAcc.isAvailable(*p)) {
        type = truthTypeAcc(*p);
        // std::cout<<" -- particle type: "<<type;
    }
}
void FillParticle::fill(const xAOD::IParticleContainer* c, size_t i) {
    // std::cout << "Filling container 209: " << std::endl;
    if (c->size() > i) {
        fill(c->at(i));
    }
}
void FillParticle::fill(const xAOD::IParticleContainer* c) {
    // std::cout << "Filling container 215: " << std::endl;
    fill(c, 0);
}

//-------------------------------------------------------------------
void FillFourVector::fill(const TLorentzVector& v) {
    // fill variables
    eta = v.Eta();
    phi = v.Phi();
    pt = v.Pt() * energyMetric();
    e = v.E() * energyMetric();
}
//-------------------------------------------------------------------
void FillBasicPhoton::fill(const xAOD::IParticle* p) {
    // fill variables
    eta = p->p4().Eta();
    phi = p->p4().Phi();
    // pt = p->p4().Pt() * energyMetric();
    et = p->p4().Et() * energyMetric();
    signal = p->isAvailable<char>("signal") ? static_cast<int>(p->auxdata<char>("signal")) : 0;
}

void FillBasicPhoton::fill(const xAOD::IParticleContainer* c, size_t i) {
    if (c->size() > i) {
        fill(c->at(i));
    }
}
void FillBasicPhoton::fill(const xAOD::IParticleContainer* c) {
    fill(c, 0);
}

//-------------------------------------------------------------------
void FillLepton::fill(const xAOD::IParticle* p) {
    /** fill all the FillParticle's variables */
    FillParticle::fill(p);

    /** @brief isolation variables */
    float etc20 = -11.;
    float topoetc20 = -11.;
    float ptc20 = -11.;
    float ptvarc20 = -11.;
    float etc30 = -11.;
    float topoetc30 = -11.;
    float ptc30 = -11.;
    float ptvarc30 = -11.;
    float etc40 = -11.;
    float topoetc40 = -11.;
    float ptc40 = -11.;
    float ptvarc40 = -11.;

    /** @brief ID flags */
    int isVeryLoose = -1;
    int isLoose = -1;
    int isMedium = -1;
    int isTight = -1;
    int isBad = -1;

    if (p->type() == xAOD::Type::Electron) {
        // cast particle to electron type
        const xAOD::Electron* el = dynamic_cast<const xAOD::Electron*>(p);

        // electron ID
        isLoose = el->auxdata<int>("llh_loose");
        isMedium = el->auxdata<int>("llh_medium");
        isTight = el->auxdata<int>("llh_tight");
        // isBad = ; //to be implemented

        // electron isolation
        el->isolationValue(etc20, xAOD::Iso::etcone20);
        el->isolationValue(etc30, xAOD::Iso::etcone30);
        el->isolationValue(etc40, xAOD::Iso::etcone40);
        el->isolationValue(topoetc20, xAOD::Iso::topoetcone20);
        el->isolationValue(topoetc30, xAOD::Iso::topoetcone30);
        el->isolationValue(topoetc40, xAOD::Iso::topoetcone40);
        el->isolationValue(ptc20, xAOD::Iso::ptcone20);
        el->isolationValue(ptc30, xAOD::Iso::ptcone30);
        el->isolationValue(ptc40, xAOD::Iso::ptcone40);
        el->isolationValue(ptvarc20, xAOD::Iso::ptvarcone20);
        el->isolationValue(ptvarc30, xAOD::Iso::ptvarcone30);
        el->isolationValue(ptvarc40, xAOD::Iso::ptvarcone40);

        if (el->caloCluster()) {
            clusterEta = el->caloCluster()->eta();
            clusterEtaBE2 = el->caloCluster()->etaBE(2);
        }

    } else if (p->type() == xAOD::Type::Muon) {
        /** cast particle to muon type */
        const xAOD::Muon* mu = dynamic_cast<const xAOD::Muon*>(p);

        /** muonType */
        muonType = mu->muonType();

        /** muon ID */
        isVeryLoose = mu->auxdata<int>("quality_veryloose");
        isLoose = mu->auxdata<int>("quality_loose");
        isMedium = mu->auxdata<int>("quality_medium");
        isTight = mu->auxdata<int>("quality_tight");
        isBad = mu->auxdata<int>("quality_bad");

        /** muon isolation */
        mu->isolation(etc20, xAOD::Iso::etcone20);
        mu->isolation(etc30, xAOD::Iso::etcone30);
        mu->isolation(etc40, xAOD::Iso::etcone40);
        mu->isolation(topoetc20, xAOD::Iso::topoetcone20);
        mu->isolation(topoetc30, xAOD::Iso::topoetcone30);
        mu->isolation(topoetc40, xAOD::Iso::topoetcone40);
        mu->isolation(ptc20, xAOD::Iso::ptcone20);
        mu->isolation(ptc30, xAOD::Iso::ptcone30);
        mu->isolation(ptc40, xAOD::Iso::ptcone40);
        mu->isolation(ptvarc20, xAOD::Iso::ptvarcone20);
        mu->isolation(ptvarc30, xAOD::Iso::ptvarcone30);
        mu->isolation(ptvarc40, xAOD::Iso::ptvarcone40);

    } else {
        Error("FillLepton::fill", "Invalid object type %i", static_cast<int>(p->type()));
    }

    /** common tracking variables */
    /* trk_d0                = p->auxdata<double>("trk_d0");
       trk_d0_sig            = p->auxdata<double>("trk_d0_significance");
       trk_z0                = p->auxdata<double>("trk_z0");
       trk_z0_sintheta       = p->auxdata<double>("trk_z0_sintheta");
       trk_z0_sig            = p->auxdata<double>("trk_z0_significance");
       trk_pvx_z0            = p->auxdata<double>("trk_pvx_z0");
       trk_pvx_z0_sintheta   = p->auxdata<double>("trk_pvx_z0_sintheta");
       trk_pvx_z0_sig        = p->auxdata<double>("trk_pvx_z0_significance");
       trk_pt_error          = p->auxdata<double>("trk_pt_error");
       */

    /** fill iso variables */
    // iso_wp        = p->auxdata<int>("iso_wp");
    etcone20 = etc20;
    etcone30 = etc30;
    etcone40 = etc40;
    topoetcone20 = topoetc20;
    topoetcone30 = topoetc30;
    topoetcone40 = topoetc40;
    ptcone20 = ptc20;
    ptcone30 = ptc30;
    ptcone40 = ptc40;
    ptvarcone20 = ptvarc20;
    ptvarcone30 = ptvarc30;
    ptvarcone40 = ptvarc40;

    /** fill id flags */
    id_veryloose = isVeryLoose;  //!< muon only
    id_loose = isLoose;
    id_medium = isMedium;
    id_tight = isTight;
    id_bad = isBad;  //!< muon only

    signal = p->isAvailable<char>("signal") ? static_cast<int>(p->auxdata<char>("signal")) : 0;
    isolation = static_cast<int>(p->auxdata<char>("isol"));
    passORL = static_cast<int>(p->auxdata<char>("passOR"));
}

//-------------------------------------------------------------------
void FillJet::fill(const xAOD::Jet* p) {
    // fill all the FillParticle's variables:
    FillParticle::fill(p);

    // fill additional variables
    jvf = p->isAvailable<std::vector<float>>("JVF") ? p->auxdata<std::vector<float>>("JVF").at(0) : 0;
    jvt = p->isAvailable<float>("Jvt") ? p->auxdata<float>("Jvt") : 0;
    mvx = p->isAvailable<double>("MVX") ? p->auxdata<double>("MVX") : 0;
    mvx_tagged = p->isAvailable<int>("MVX_tagged") ? p->auxdata<int>("MVX_tagged") : 0;

    int tmpLabel = -1;
    p->getAttribute("HadronConeExclTruthLabelID", tmpLabel);
    flavorlabel = tmpLabel;
    p->getAttribute("PartonTruthLabelID", tmpLabel);
    flavorlabel_part = tmpLabel;
    p->getAttribute("ConeTruthLabelID", tmpLabel);
    flavorlabel_cone = tmpLabel;
}

void FillJet::fill(const xAOD::JetContainer* c, size_t i) {
    if (c->size() > i) fill(c->at(i));
}
void FillJet::fill(const xAOD::JetContainer* c) {
    fill(c, 0);
}

//-------------------------------------------------------------------
void FillBasicTau::fill(const xAOD::TauJet* tau) {
    /** fill all the FillParticle's variables: */
    FillParticle::fill(tau);

    /**
     * @brief fill basic tau-specific variables
     * @see RootCoreBin/include/xAODTau/TauDefs.h
     */

    n_tracks = tau->nTracks();
    n_wide_tracks = tau->nTracks(xAOD::TauJetParameters::TauTrackFlag::wideTrack);

    jet_bdt_veryloose = tau->isTau(xAOD::TauJetParameters::JetBDTSigVeryLoose);
    jet_bdt_loose = tau->isTau(xAOD::TauJetParameters::JetBDTSigLoose);
    jet_bdt_medium = tau->isTau(xAOD::TauJetParameters::JetBDTSigMedium);
    jet_bdt_tight = tau->isTau(xAOD::TauJetParameters::JetBDTSigTight);
    jet_bdt_score = tau->discriminant(xAOD::TauJetParameters::BDTJetScore);
    jet_bdt_score_trans = tau->discriminant(xAOD::TauJetParameters::BDTJetScoreSigTrans);

    jet_rnn_veryloose = tau->isTau(xAOD::TauJetParameters::JetRNNSigVeryLoose);
    jet_rnn_loose = tau->isTau(xAOD::TauJetParameters::JetRNNSigLoose);
    jet_rnn_medium = tau->isTau(xAOD::TauJetParameters::JetRNNSigMedium);
    jet_rnn_tight = tau->isTau(xAOD::TauJetParameters::JetRNNSigTight);
    jet_rnn_score = tau->discriminant(xAOD::TauJetParameters::RNNJetScore);
    jet_rnn_score_trans = tau->discriminant(xAOD::TauJetParameters::RNNJetScoreSigTrans);

    if (acc_BDTEleScore_retuned.isAvailable(*tau)) {
        // Use retuned EleBDT scores/WPs
        ele_bdt_score = acc_BDTEleScore_retuned(*tau);
        ele_bdt_score_trans = acc_BDTEleScoreSigTrans_retuned(*tau);

        ele_bdt_loose = acc_BDTEleLoose_retuned(*tau);
        ele_bdt_medium = acc_BDTEleMedium_retuned(*tau);
        ele_bdt_tight = acc_BDTEleTight_retuned(*tau);
    } else {
        // No retuned EleBDT available, use old one
        ele_bdt_score = acc_BDTEleScore(*tau);
        ele_bdt_score_trans = acc_BDTEleScoreSigTrans(*tau);

        ele_bdt_loose = tau->isTau(xAOD::TauJetParameters::EleBDTLoose);
        ele_bdt_medium = tau->isTau(xAOD::TauJetParameters::EleBDTMedium);
        ele_bdt_tight = tau->isTau(xAOD::TauJetParameters::EleBDTTight);
    }

    int temp_decay_mode = xAOD::TauJetParameters::DecayMode::Mode_Error;
    if (tau->panTauDetail(xAOD::TauJetParameters::PanTauDetails::PanTau_DecayMode, temp_decay_mode)) {
        decay_mode = temp_decay_mode;
    }

    d0 = (tau->auxdata<float>("d0"));
    delta_z0 = (tau->auxdata<float>("delta_z0"));
    d0_sig = (tau->auxdata<float>("d0sig"));
    z0_sintheta = tau->auxdata<float>("z0sinTheta_no_pvtx");
    pvx_z0_sintheta = tau->auxdata<float>("z0sinTheta");
    a0PV = tau->auxdata<float>("a0PV");

    signal = tau->isAvailable<char>("signal") ? static_cast<int>(tau->auxdata<char>("signal")) : 0;
}

//-------------------------------------------------------------------
void FillTau::fill(const xAOD::TauJet* tau) {
    /** fill all the FillBasicTau's variables: */
    FillBasicTau::fill(tau);

    /**
     * @brief fill more tau-specific variables
     * @see RootCoreBin/include/xAODTau/TauDefs.h
     */

    // if( tau->isAvailable<char>("IsTruthMatched") )
    //    isTruthLepMatched = (bool)tau->auxdata<char>("IsTruthMatched");

    //jet_bdt_score = tau->discriminant(xAOD::TauJetParameters::BDTJetScore);
    //ele_bdt_loose = tau->isTau(xAOD::TauJetParameters::EleBDTLoose);
    //ele_bdt_medium = tau->isTau(xAOD::TauJetParameters::EleBDTMedium);
    //ele_bdt_tight = tau->isTau(xAOD::TauJetParameters::EleBDTTight);
    //ele_bdt_score = tau->discriminant(xAOD::TauJetParameters::BDTEleScore);
    ele_bdt_eff_sf = 0;  // tau->auxdata<double>("ele_id_eff_sf"));
    ele_match_lhscore = tau->isAvailable<float>("ele_match_lhscore") ? tau->auxdata<float>("ele_match_lhscore") : -4;
    ele_olr_pass = tau->isAvailable<char>("ele_olr_pass") ? static_cast<int>(tau->auxdata<char>("ele_olr_pass")) : -2;
    if (tau->nTracks() && tau->tauTrackLinks().size() && tau->tauTrackLinks().at(0).isValid()) {
        const auto track = tau->track(0)->track();
        leadTrk_pt = track->pt() * energyMetric();
        leadTrk_eta = track->eta();
        leadTrk_phi = track->phi();
    }
    mvx = tau->isAvailable<double>("MVX") ? tau->auxdata<double>("MVX") : 0;
    mvx_tagged = tau->isAvailable<int>("MVX_tagged") ? tau->auxdata<int>("MVX_tagged") : 0;

    signal = tau->isAvailable<char>("signal") ? static_cast<int>(tau->auxdata<char>("signal")) : 0;
    passORL = static_cast<int>(tau->auxdata<char>("passOR"));
}
//-------------------------------------------------------------------
FillTriggerMatch::FillTriggerMatch(VarList& varList, std::string gName, const std::vector<std::string>& triggerItems)
    : VarGroup(varList, gName) {
    for (auto item : triggerItems) {
        m_trigger_match_map[item] = CxxUtils::make_unique<VarUI>(varList, item, group);
    }
}

void FillTriggerMatch::fill(const xAOD::IParticle* p) {
    if (!p) return;

    //if (!p->isAvailable<char>("trigmatched")) return;
    // bool noMatch = (p->auxdata<int>("trigger_matched") == 0) ? true : false;

    //const bool noMatch = not acc_trigmatched(*p);

    std::for_each(begin(m_trigger_match_map), end(m_trigger_match_map), [&, p](decltype(*m_trigger_match_map.begin())& entry) {
        //if (noMatch) {
        //    *(entry.second) = 0;
        //} else {
        //    *(entry.second) = p->isAvailable<char>(entry.first) ? p->auxdecor<char>(entry.first) : 0;
        //}
        *(entry.second) = p->isAvailable<char>(entry.first) ? p->auxdecor<char>(entry.first) : 0;
    });
}
//-------------------------------------------------------------------
FillTriggers::FillTriggers(VarList& varList, std::string trigger_type, std::string gName) : VarGroup(varList, gName) {
    std::string name = "trigger_list_tau";
    if (trigger_type == "tau") {  // fill tau triggers
        name = "trigger_list_tau";
    } else if (trigger_type == "electron") {
        name = "trigger_list_electron";
    } else if (trigger_type == "muon") {
        name = "trigger_list_muon";
    } else if (trigger_type == "jet") {
        name = "trigger_list_jet";
    } else if (trigger_type == "met") {
        name = "trigger_list_met";
    }

    for (auto jo_trig_item : Config::getInstance()->get<std::vector<std::string>>(name)) {
        if (jo_trig_item == " " or jo_trig_item == "") {
            continue;
        }
        VarI* trigVar = new VarI(vlist, jo_trig_item, group);
        trigVar->setTrigger();
        variables[jo_trig_item] = trigVar;
    }
}
FillTriggers::~FillTriggers() {
    for (auto var : variables) delete var.second;
}
void FillTriggers::fill(const std::map<std::string, bool>& trig_map) {
    for (const auto& trig : trig_map) {
        std::map<std::string, VarI*>::iterator itr = variables.find(trig.first);
        if (itr != variables.end()) {
            *(itr->second) = static_cast<int>(trig.second);  // fill the trigger signature
        }
    }
}
//-------------------------------------------------------------------
void FillMET::fill(const xAOD::MissingET* m) {
    et = m->met() * energyMetric();
    etx = m->mpx() * energyMetric();
    ety = m->mpy() * energyMetric();
    phi = m->phi();
    sumet = m->sumet() * energyMetric();
    /**
     * @short MET significance
     * It is often defined as met/GeV/0.5/sqrt(sum_pt/GeV) where sum_pt is sumet or track based.
     * A unitless definition is used instead.
     */
    sig = m->met() / 1000 / 0.5 / sqrt(m->sumet() / 1000);
}

void FillMET::fill(const xAOD::MissingET* m, const xAOD::Vertex* v) {
    fill(m);
    if (v == 0) return;
    double SumPt_PVOnly = 0;
    for (uint i = 0; i != v->nTrackParticles(); ++i) {
        const xAOD::TrackParticle* trk = v->trackParticle(i);
        if (trk == 0) continue;  // derivation track slimming
        // could make this configurable/add  d0/z0
        uint8_t numberOfPixelHits = 0;
        trk->summaryValue(numberOfPixelHits, xAOD::numberOfPixelHits);
        uint8_t numberOfSCTHits = 0;
        trk->summaryValue(numberOfSCTHits, xAOD::numberOfSCTHits);
        if (numberOfPixelHits < 1 || numberOfSCTHits < 6) continue;
        // todo z0/d0 wrt v
        if (trk->pt() > 1000) SumPt_PVOnly += trk->pt();
    }
    sig_tracks = m->met() / 1000 / 0.5 / sqrt(SumPt_PVOnly / 1000);
}

void FillMET::fill(const xAOD::MissingET* m, const xAOD::VertexContainer* c, size_t i) {
    fill(m, (c->size() > i ? c->at(i) : 0));
}
//-------------------------------------------------------------------
void FillTruthParticle::fill(const xAOD::TruthParticle* p) {
    FillParticle::fill(p);
    pdgId = p->pdgId();
    if (p->nParents() && p->parent(0)) {  // in derivations, it is possible for tp's to be removed
        mother_pdgId = p->parent(0)->pdgId();
    }
    if (p->isAvailable<unsigned>("classifierParticleOrigin")) origin = p->auxdata<unsigned>("classifierParticleOrigin");
    if (p->isAvailable<unsigned>("classifierParticleType")) type = p->auxdata<unsigned>("classifierParticleType");
}
//-------------------------------------------------------------------
void FillTruthTau::fill(const xAOD::IParticle* p) {
    /** cast particle to explicitly become a tau object */
    const xAOD::TauJet* tau = dynamic_cast<const xAOD::TauJet*>(p);

    /** using TauTruthMatchingTool to find matched lepton*/
    if (not asg::ToolStore::contains<TauAnalysisTools::TauTruthMatchingTool>("TauTruthMatchingTool")) {
        throw std::runtime_error("Cannot find 'TauTruthMatchingTool' in the tool store");
    }
    const auto matchingTool = asg::ToolStore::get<TauAnalysisTools::TauTruthMatchingTool>("TauTruthMatchingTool");
    const xAOD::TruthParticle* truthPart = matchingTool->getTruth(*tau);

    if (truthPart) {
        // FillTruthTau::fill( truthPart, cp_tools);
        FillTruthTau::fill(truthPart);
    } else {
        //!< using TauTruthMatchingTool to find matched jet
        // initialize important variables
        pdgId = 0;
        isJet = 0;

        // if(m_jo.GetAuxInfo<int>("isDerivation") && m_jo.GetAuxInfo<unsigned int>("DerivationHasLCJets")){ //protection against
        // missing LC jets

        auto xTruthJetLink = tau->auxdata<ElementLink<xAOD::JetContainer>>("truthJetLink");
        if (xTruthJetLink.isValid()) {  // link is pointing to an object
            const xAOD::Jet* truthJet = xAOD::TauHelpers::getLink<xAOD::Jet>(tau, "truthJetLink");
            // const xAOD::Jet* truthJet = *xTruthJetLink; //or
            FillTruthTau::fill(truthJet);
        }

        // FillTruthTau::fill(tau->jet()); //alternative

        //}//LC jets
    }
}
//-------------------------------------------------------------------
void FillTruthTau::fill(const xAOD::Jet* p) {  //!< p is a truth Jet

    if (!p) return;

    FillTruthParticle::fill(p);

    int tmpLabel(0);
    if (p->getAttribute("PartonTruthLabelID", tmpLabel)) pdgId = tmpLabel;

    if (tmpLabel > 0) isJet = 1;

    /** @note let isJet = 0 if no GhostPartons are associated with the jet, i.e. PartonTruthLabelID = -1
     *  @sa
     * https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/AnalysisCommon/ParticleJetTools/trunk/Root/JetPartonTruthLabel.cxx
     */
}
//-------------------------------------------------------------------
void FillTruthTau::fill(const xAOD::TruthParticle* p) {
    FillTruthParticle::fill(p);

    try {
        // particle flavor
        isTau = p->isTau();

        if (p->isTau()) {
            try {
                isHadTau = static_cast<bool>(p->auxdata<char>("IsHadronicTau"));  // in derivations the bool is stored as char!
            } catch (const SG::ExcAuxTypeMismatch& e) {
                isHadTau = p->auxdata<bool>("IsHadronicTau");
            }
        } else {
            isHadTau = 0;
        }

        isEle = p->isElectron();
        isMuon = p->isMuon();

        // particle classification
        classifierParticleType =
            p->isAvailable<unsigned int>("classifierParticleType") ? p->auxdata<unsigned int>("classifierParticleType") : 0;
        classifierParticleOrigin =
            p->isAvailable<unsigned int>("classifierParticleOrigin") ? p->auxdata<unsigned int>("classifierParticleOrigin") : 0;

        // particle kinematics
        pt_vis = p->auxdata<double>("pt_vis") * energyMetric();
        eta_vis = p->auxdata<double>("eta_vis");
        phi_vis = p->auxdata<double>("phi_vis");
        m_vis = p->auxdata<double>("m_vis") * energyMetric();

        // substructure decay mode
        // if(!cp_tools) decay_mode = xHCP::TauDecayMode(p);
        // else decay_mode = cp_tools->use<TauAnalysisTools::TauTruthMatchingTool>()->getDecayMode(*p);

        if (not asg::ToolStore::contains<TauAnalysisTools::TauTruthMatchingTool>("TauTruthMatchingTool")) {
            throw std::runtime_error("Cannot find 'TauTruthMatchingTool' in the tool store");
        }
        const auto matchingTool = asg::ToolStore::get<TauAnalysisTools::TauTruthMatchingTool>("TauTruthMatchingTool");
        decay_mode = matchingTool->getDecayMode(*p);

        if (p->isAvailable<size_t>("numCharged")) n_charged = p->auxdata<size_t>("numCharged");
        if (p->isAvailable<size_t>("numChargedPion")) n_charged_pion = p->auxdata<size_t>("numChargedPion");
        if (p->isAvailable<size_t>("numNeutral")) n_neutral = p->auxdata<size_t>("numNeutral");
        if (p->isAvailable<size_t>("numNeutralPion")) n_neutral_pion = p->auxdata<size_t>("numNeutralPion");

        // FIXME: cgrefe: these decorations from the TauTruthMatchingTool don't work at the moment
        //    if(p->isAvailable<double>("pt_vis_charged" )) pt_vis_charged  = p->auxdata<double>("pt_vis_charged" );
        //    if(p->isAvailable<double>("eta_vis_charged")) eta_vis_charged = p->auxdata<double>("eta_vis_charged");
        //    if(p->isAvailable<double>("phi_vis_charged")) phi_vis_charged = p->auxdata<double>("phi_vis_charged");
        //    if(p->isAvailable<double>("m_vis_charged"  )) m_vis_charged           = p->auxdata<double>("m_vis_charged"  );
        //
        //    if(p->isAvailable<double>("pt_vis_neutral" )) pt_vis_neutral  = p->auxdata<double>("pt_vis_neutral" );
        //    if(p->isAvailable<double>("eta_vis_neutral")) eta_vis_neutral = p->auxdata<double>("eta_vis_neutral");
        //    if(p->isAvailable<double>("phi_vis_neutral")) phi_vis_neutral = p->auxdata<double>("phi_vis_neutral");
        //    if(p->isAvailable<double>("m_vis_neutral"  )) m_vis_neutral           = p->auxdata<double>("m_vis_neutral"  );

    } catch (SG::ExcBadAuxVar& e) {
        // Warning("FillTruthTau::fill()", "Object is not a truth tau.");
        pt_vis = p->p4().Pt() * energyMetric();
        eta_vis = p->p4().Eta();
        phi_vis = p->p4().Phi();
        m_vis = p->p4().M() * energyMetric();
        try {
            // xHCP::TauDecayProducts decay= xHCP::TauDecayProducts(p);
            //    std::cout << "Charged : " << decay.ncharged() << std::endl;
            //    std::cout << "Neutral : " << decay.nneutral() << std::endl;
            // decay_mode = xHCP::TauDecayMode(decay.ncharged(),decay.nneutral());
            //    std::cout << "Decay Mode : "<< xHCP::TauDecayMode(decay.ncharged(),decay.nneutral()) << std::endl;
        } catch (...) {
        }
    }
}
//-------------------------------------------------------------------
void FillMatchedTruthLeptonicTauDecay::fill(const xAOD::IParticle* p, const xAOD::TruthParticleContainer* truth_taus) {
    // input check
    if (p->type() != xAOD::Type::Muon && p->type() != xAOD::Type::Electron) return;

    if (not asg::ToolStore::contains<TauAnalysisTools::TauTruthMatchingTool>("TauTruthMatchingTool")) {
        throw std::runtime_error("Cannot find 'TauTruthMatchingTool' in the tool store");
    }
    const auto matchingTool = asg::ToolStore::get<TauAnalysisTools::TauTruthMatchingTool>("TauTruthMatchingTool");

    // loop over truth_tau container (N.B. truth_tau container is created by the TruthTauMatchingTool
    // either in the derivation step or on-the-fly
    for (auto truth_tau : *truth_taus) {
        if (!truth_tau->isTau()) {
            continue;
        }
        if (static_cast<bool>(truth_tau->auxdata<char>("IsHadronicTau"))) {
            continue;
        }

        // TruthTauMatchingTool decorates the TruthParticles in the truth_taus collection
        // by various useful information, which is readily available:
        if (truth_tau->isAvailable<std::vector<int>>("DecayModeVector")) {
            std::vector<int> vDecayMode = truth_tau->auxdata<std::vector<int>>("DecayModeVector");

            //      // test
            //      if(p->type() == xAOD::Type::Muon)
            //        std::cout<<"DAN muon"<<std::endl;
            //      else
            //        std::cout<<"DAN electron"<<std::endl;
            //      std::cout << "DAN   vDecayMode.size() = " << vDecayMode.size() << std::endl;
            //      for(auto decayMode : vDecayMode)
            //        std::cout << "DAN     decayMode = " << decayMode << std::endl;

            //      std::cout << "DAN   n daughters = " << truth_tau->decayVtx()->nOutgoingParticles() << std::endl;
            //      for ( size_t iOutgoingParticle = 0; iOutgoingParticle < truth_tau->decayVtx()->nOutgoingParticles();
            //      ++iOutgoingParticle ) {
            //        const xAOD::TruthParticle* xTruthDaughter = truth_tau->decayVtx()->outgoingParticle(iOutgoingParticle);
            //        std::cout << "DAN     pdg = " << xTruthDaughter->pdgId() << std::endl;
            //      }

            for (auto decayMode : vDecayMode) {
                if (p->type() == xAOD::Type::Electron) {
                    if (std::abs(decayMode) != 11) continue;
                } else if (p->type() == xAOD::Type::Muon) {
                    if (std::abs(decayMode) != 13) continue;
                }

                // use the tauTruthMatchingTool to retrieve the true four momentum of the visible part
                TLorentzVector vis = matchingTool->getTruthTauP4Vis(*truth_tau);

                if (vis.DeltaR(p->p4()) < 0.2) {
                    // match found. Fill the tree branches
                    // fill true tau 4-momentum
                    FillTruthParticle::fill(truth_tau);

                    // fill visible 4-momentum
                    pt_vis = vis.Pt();
                    eta_vis = vis.Eta();
                    phi_vis = vis.Phi();
                    m_vis = vis.M();

                    // fill 4-momentum of the neutrino system:
                    TLorentzVector inv = truth_tau->p4() - vis;
                    pt_invis = inv.Pt();
                    eta_invis = inv.Eta();
                    phi_invis = inv.Phi();
                    m_invis = inv.M();
                }
            }  // end of loop over PDG coded of the tau decay products
        }
    }  // end of loop over truth taus
}

//-------------------------------------------------------------------
void FillMatchedParticle::fill(const xAOD::IParticle* p, const xAOD::IParticleContainer* pc, int matchedIdx) {
    /**
     * @brief fill the variables specific to the truth objects matched to each reconstructed case
     * @see RootCoreBin/include/xAODTau/TauDefs.h
     */

    bool isTau(p->type() == xAOD::Type::Tau);
    const xAOD::TruthParticle* match = 0;

    // If you supply a matchedIdx, do fill directly. Useful for BSM who want to use private function for tau truth matching
    if (matchedIdx >= 0) {
        match = dynamic_cast<const xAOD::TruthParticle*>(pc->at(matchedIdx));
        FillParticle::fill(match);
        pdgId = match->pdgId();
        isHad = isTau ? ParticleUtils::IsHadronicTau(match) : 0;
        if (match->nParents() && match->parent(0)) {  // in derivations, it is possible for tp's to be removed
            mother_pdgId = match->parent(0)->pdgId();
        }
        return;
    }  // end of block where you supply index explicitly

    switch (p->type()) {
        case xAOD::Type::Muon:
            matchedIdx = p->auxdata<int>("mu_matchedIdx");
            break;
        case xAOD::Type::Electron:
            matchedIdx = p->auxdata<int>("el_matchedIdx");
            break;
        case xAOD::Type::Tau:
            matchedIdx = 1;
            break;
        default:
            matchedIdx = -1;
    }
    if (matchedIdx >= 0) {
        if (!isTau) {
            match = dynamic_cast<const xAOD::TruthParticle*>(pc->at(matchedIdx));
            isHad = false;
        } else {
            if (p->isAvailable<const xAOD::TruthParticle*>("TruthTau")) {
                match = p->auxdata<const xAOD::TruthParticle*>("TruthTau");
            } else {
                match = xAOD::TauHelpers::getTruthParticle(p);
            }
            isHad = static_cast<bool>(p->auxdata<char>("IsHadronicTau"));
        }

        FillParticle::fill(match);
        pdgId = match->pdgId();
        if (match->nParents() && match->parent(0)) {  // in derivations, it is possible for tp's to be removed
            mother_pdgId = match->parent(0)->pdgId();
        }

        if (match != 0) {
            if (match->isAvailable<unsigned>("classifierParticleType")) type = match->auxdata<unsigned>("classifierParticleType");
            if (match->isAvailable<unsigned>("classifierParticleOrigin"))
                origin = match->auxdata<unsigned>("classifierParticleOrigin");
        }

        //    // fill MCTruthClassifier particle type and particle origin.
        //    // (28-01-2016: this decoration only available for electrons and taus)
        //    type   = xAOD::TruthHelpers::getParticleTruthType(*p);
        //    origin = xAOD::TruthHelpers::getParticleTruthOrigin(*p);
    }
}

void FillMatchedParticle::fill(const xAOD::IParticle* p, const xAOD::IParticleContainer* pc) {
    fill(p, pc, -1);
}
//-------------------------------------------------------------------
void FillTruthJet::fill(const xAOD::Jet* p) {
    eta = p->p4().Eta();
    phi = p->p4().Phi();
    pt = p->p4().Pt() * energyMetric();
    m = p->p4().M() * energyMetric();
}
//-------------------------------------------------------------------
void FillTruthMET::fill(const xAOD::MissingET* m) {
    et = m->met() * energyMetric();
    etx = m->mpx() * energyMetric();
    ety = m->mpy() * energyMetric();
    phi = m->phi();
    sumet = m->sumet() * energyMetric();

    /**
     * @short Truth MET significance
     * It is often defined as met/GeV/0.5/sqrt(sum_pt/GeV) where sum_pt is sumet or track based.
     * A unitless definition is used instead.
     */
    sig = m->met() / 1000 / 0.5 / sqrt(m->sumet() / 1000);
}
//-------------------------------------------------------------------
void FillPhoton::fill(const xAOD::Photon* p) {
    /** fill all the FillParticle's variables */
    FillParticle::fill(p);

    /** @brief isolation variables */
    float etc20 = -11.;
    float topoetc20 = -11.;
    float ptc20 = -11.;
    float ptvarc20 = -11.;
    float etc30 = -11.;
    float topoetc30 = -11.;
    float ptc30 = -11.;
    float ptvarc30 = -11.;
    float etc40 = -11.;
    float topoetc40 = -11.;
    float ptc40 = -11.;
    float ptvarc40 = -11.;

    /** @brief ID flags */
    int isLoose = -1;
    int isMedium = -1;
    int isTight = -1;

    if (p->type() != xAOD::Type::Photon) {
        Error("FillPhoton::fill", "Invalid object type %i", static_cast<int>(p->type()));
        return;
    }

    // cast particle to electron type
    // const xAOD::Photon* ph = dynamic_cast<const xAOD::Photon*>(p);

    // photon ID
    // isLoose  = ph->auxdata<int>("llh_loose");
    // isMedium = ph->auxdata<int>("llh_medium");
    // isTight  = ph->auxdata<int>("llh_tight");

    // photon isolation
    p->isolationValue(etc20, xAOD::Iso::etcone20);
    p->isolationValue(etc30, xAOD::Iso::etcone30);
    p->isolationValue(etc40, xAOD::Iso::etcone40);
    p->isolationValue(topoetc20, xAOD::Iso::topoetcone20);
    p->isolationValue(topoetc30, xAOD::Iso::topoetcone30);
    p->isolationValue(topoetc40, xAOD::Iso::topoetcone40);
    p->isolationValue(ptc20, xAOD::Iso::ptcone20);
    p->isolationValue(ptc30, xAOD::Iso::ptcone30);
    p->isolationValue(ptc40, xAOD::Iso::ptcone40);
    p->isolationValue(ptvarc20, xAOD::Iso::ptvarcone20);
    p->isolationValue(ptvarc30, xAOD::Iso::ptvarcone30);
    p->isolationValue(ptvarc40, xAOD::Iso::ptvarcone40);

    if (p->caloCluster()) {
        clusterEta = p->caloCluster()->eta();
        clusterEtaBE2 = p->caloCluster()->etaBE(2);
    }

    /** fill iso variables */
    // iso_wp        = p->auxdata<int>("iso_wp");
    etcone20 = etc20;
    etcone30 = etc30;
    etcone40 = etc40;
    topoetcone20 = topoetc20;
    topoetcone30 = topoetc30;
    topoetcone40 = topoetc40;
    ptcone20 = ptc20;
    ptcone30 = ptc30;
    ptcone40 = ptc40;
    ptvarcone20 = ptvarc20;
    ptvarcone30 = ptvarc30;
    ptvarcone40 = ptvarc40;

    /** fill id flags */
    id_loose = isLoose;
    id_medium = isMedium;
    id_tight = isTight;

    signal = p->isAvailable<char>("signal") ? static_cast<int>(p->auxdata<char>("signal")) : 0;
    isolation = static_cast<int>(p->auxdata<char>("isol"));
    passORL = static_cast<int>(p->auxdata<char>("passOR"));
}
//-------------------------------------------------------------------
void FillBasicMMC::fill(const xAOD::EventInfo* peventinfo, const xAOD::IParticle* p0, const xAOD::IParticle* p1,
                        const xAOD::JetContainer* pjets, const xAOD::MissingET* pmet) {
    CP::CorrectionCode res = CP::CorrectionCode::Error;

    /** using MMC to find matched lepton*/
    if (not asg::ToolStore::contains<MissingMassTool>("MMC")) {
        throw std::runtime_error("Cannot find 'MMC' in the tool store");
    }
    const auto m_MMC = asg::ToolStore::get<MissingMassTool>("MMC");
    const double mmcEnergyMetric = energyMetric() * 1000.;  // MMC output always in GeV

    mmc_resonance_m = -10;

    // Is the user an idiot?
    if (p0 == nullptr or p1 == nullptr) {
        return;
    }

    // Ignore empty fit warnings
    auto rootErrorLevel = gErrorIgnoreLevel;
    gErrorIgnoreLevel = kError;

    res = m_MMC->apply((*peventinfo), p0, p1, pmet, FillMMC::countMMCJets(pjets));

    if (res != CP::CorrectionCode::Ok) {
        return;
    }

    // Get the fit status and the mass
    if (m_MMC->GetFitStatus(MMCFitMethod::MAXW == 1)) {
        mmc_resonance_m = m_MMC->GetFittedMass(MMCFitMethod::MAXW) * mmcEnergyMetric;
    }

    gErrorIgnoreLevel = rootErrorLevel;
}
//-------------------------------------------------------------------
FillMMC::~FillMMC() {
    for (auto item : mmc_fit_status) {
        if (item) delete item;
    }

    std::vector<std::vector<VarF*>> vectors = {
        mmc_resonance_m, mmc_resonance_pt, mmc_resonance_eta, mmc_resonance_phi, mmc_met_et, mmc_met_phi, mmc_x0, mmc_x1};
    for (auto vector : vectors) {
        for (auto item : vector) {
            if (item) delete item;
        }
    }
}
//-------------------------------------------------------------------
void FillMMC::fill(const xAOD::EventInfo* peventinfo, const xAOD::IParticle* p0, const xAOD::IParticle* p1,
                   const xAOD::JetContainer* pjets, const xAOD::MissingET* pmet) {
    CP::CorrectionCode res = CP::CorrectionCode::Error;

    /** using MMC to find matched lepton*/
    if (not asg::ToolStore::contains<MissingMassTool>("MMC")) {
        throw std::runtime_error("Cannot find 'MMC' in the tool store");
    }
    const auto m_MMC = asg::ToolStore::get<MissingMassTool>("MMC");

    TLorentzVector MMC_TLV[MMCFitMethod::MAX];
    TVector2 MMC_MET[MMCFitMethod::MAX];
    TLorentzVector MMC_tau0[MMCFitMethod::MAX];
    TLorentzVector MMC_tau1[MMCFitMethod::MAX];
    const double mmcEnergyMetric = energyMetric() * 1000.;  // MMC output always in GeV

    // Is the user an idiot?
    if (p0 != nullptr and p1 != nullptr) {
        res = m_MMC->apply((*peventinfo), p0, p1, pmet, FillMMC::countMMCJets(pjets));
    }

    // If tool failed, fill dummy values
    if (res != CP::CorrectionCode::Ok) {
        for (uint i = 0; i < MMCFitMethod::MAX; ++i) {
            *(mmc[i]) = -10;
            *(mmc_fit_status[i]) = -10;
            *(mmc_resonance_m[i]) = -10;
            if (i != MMCFitMethod::MLM) {  // these variables are not filled for MLM
                uint j = i < MMCFitMethod::MLM ? i : i - 1;
                *(mmc_resonance_pt[j]) = -10.;
                *(mmc_resonance_eta[j]) = -10.;
                *(mmc_resonance_phi[j]) = -10.;
                *(mmc_met_et[j]) = -10.;
                *(mmc_met_phi[j]) = -10.;
                *(mmc_x0[j]) = -10.;
                *(mmc_x1[j]) = -10.;
            }
        }
    }

    // Fill branches for all fit methods
    for (uint i = 0; i < MMCFitMethod::MAX; ++i) {
        // Get the fit status and the mass
        *(mmc[i]) = 1;
        *(mmc_fit_status[i]) = m_MMC->GetFitStatus(i);
        *(mmc_resonance_m[i]) = m_MMC->GetFittedMass(i) * mmcEnergyMetric;

        if (i == MMCFitMethod::MLM) {
            continue;
        }

        // these variables are not filled for MLM
        uint j = i < MMCFitMethod::MLM ? i : i - 1;
        MMC_TLV[j] = m_MMC->GetResonanceVec(i);
        MMC_MET[j] = m_MMC->GetFittedMetVec(i);
        MMC_tau0[j] = m_MMC->GetTau4vec(i, 0);
        MMC_tau1[j] = m_MMC->GetTau4vec(i, 1);

        *(mmc_resonance_pt[j]) = MMC_TLV[j].Pt() * mmcEnergyMetric;
        *(mmc_resonance_eta[j]) = MMC_TLV[j].Eta();
        *(mmc_resonance_phi[j]) = MMC_TLV[j].Phi();
        *(mmc_met_et[j]) = MMC_MET[j].Mod() * mmcEnergyMetric;            // FIXME: please check this is what we want
        *(mmc_met_phi[j]) = MMC_MET[j].Phi();                             // FIXME: please check this is what we want
        *(mmc_x0[j]) = p0->p4().P() / MMC_tau0[j].P() / mmcEnergyMetric;  // FIXME: please check this is what we want
        *(mmc_x1[j]) = p1->p4().P() / MMC_tau1[j].P() / mmcEnergyMetric;  // FIXME: please check this is what we want
    }
}
//-------------------------------------------------------------------
void FillMMC::fill(const xAOD::EventInfo* peventinfo, const xAOD::IParticleContainer* pc0, size_t i,
                   const xAOD::IParticleContainer* pc1, size_t j, const xAOD::JetContainer* pjets, const xAOD::MissingET* pmet) {
    if (pc0->size() > i && pc1->size() > j) {
        fill(peventinfo, pc0->at(i), pc1->at(j), pjets, pmet);
    }
}
//-------------------------------------------------------------------
void FillMMC::fill(const xAOD::EventInfo* peventinfo, const xAOD::IParticleContainer* pc0, const xAOD::IParticleContainer* pc1,
                   const xAOD::JetContainer* pjets, const xAOD::MissingET* pmet) {
    fill(peventinfo, pc0, 0, pc1, 0, pjets, pmet);
}
//-------------------------------------------------------------------
void FillMMC::fill(const xAOD::EventInfo* peventinfo, const xAOD::IParticleContainer* pc, size_t i, size_t j,
                   const xAOD::JetContainer* pjets, const xAOD::MissingET* pmet) {
    fill(peventinfo, pc, i, pc, j, pjets, pmet);
}
//-------------------------------------------------------------------
void FillMMC::fill(const xAOD::EventInfo* peventinfo, const xAOD::IParticleContainer* pc, const xAOD::JetContainer* pjets,
                   const xAOD::MissingET* pmet) {
    fill(peventinfo, pc, 0, 1, pjets, pmet);
}
//-------------------------------------------------------------------
size_t FillMMC::countMMCJets(const xAOD::JetContainer* p) {
    return std::count_if(p->begin(), p->end(), [&](const xAOD::IParticle* q) -> bool { return (q->pt() > 30000); });
}
//-------------------------------------------------------------------

/**
 * @note Braced lists cannot be deduced by template argument deduction. So, we must make the type deduction work by specifying the
 * type
 * <code>std::initializer_list<xAOD::Type::ObjectType>( {xAOD::Type::Muon, ...} );</code> or use auto deduction <code>auto il = {
 * ... };</code>
 */
FillScaleFactors::FillScaleFactors(VarList& varList, std::string gName, std::initializer_list<SF::Type> SF_types,
                                   const std::vector<ST::SystInfo>& systInfoList)
    : FillScaleFactors(varList, gName, SF_types, systInfoList, NULL, NULL) {}

FillScaleFactors::FillScaleFactors(VarList& varList, std::string gName, std::initializer_list<SF::Type> SF_types,
                                   const std::vector<ST::SystInfo>& systInfoList, std::shared_ptr<custom_SUSYObjDef_xAOD> SUSYTools)
    : FillScaleFactors(varList, gName, SF_types, systInfoList, SUSYTools, NULL) {}

FillScaleFactors::FillScaleFactors(VarList& varList, std::string gName, std::initializer_list<SF::Type> SF_types,
                                   const std::vector<ST::SystInfo>& systInfoList, std::shared_ptr<custom_SUSYObjDef_xAOD> SUSYTools,
                                   // xTriggerMuonList *mlist);
                                   void* mlist)
    : VarGroup(varList, gName),
      m_systInfoList(systInfoList),
      m_SUSYTools(SUSYTools)  //, m_muon_trigger_sf_list(mlist)
{
    for (auto type : SF_types) {
        if (type == SF::Type::Muon) {
            book_branches_for_muons();
        } else if (type == SF::Type::Global_muon_trigger) {
            book_branches_for_global_muon_trigger();
        } else if (type == SF::Type::Electron) {
            book_branches_for_electrons();
        } else if (type == SF::Type::Tau) {
            book_branches_for_taus();
        } else if (type == SF::Type::Photon) {
            book_branches_for_photons();
        } else if (type == SF::Type::Jet) {
            book_branches_for_jets();
        } else if (type == SF::Type::Pileup) {
            book_branches_for_pileup();
        } else if (type == SF::Type::Global_btag) {
            book_branches_for_global_btag();
            m_fill_global_btag = true;
        } else if (type == SF::Type::Global_jvt) {
            book_branches_for_global_jvt();
            m_fill_global_jvt = true;
        } else {
            Warning("FillScaleFactors::FillScaleFactors", "Nothing to be reserved for object of type %ui",
                    static_cast<unsigned int>(type));
        }
    }
}
////-------------------------------------------------------------------
void FillScaleFactors::add(const std::string& entry_label) {
    add(entry_label, SFtype::GENERAL);
}
//-------------------------------------------------------------------
void FillScaleFactors::add(const std::string& entry_label, const SFtype_t& sf_type) {
    Info("FillScaleFactors::add()", "label: %s", entry_label.c_str());
    VarF* sf = new VarF(vlist, entry_label, group);
    sf->setScaleFactor();
    if (m_scale_factors.find(entry_label) == m_scale_factors.end()) {
        m_scale_factors.emplace(entry_label, sf);
    } else {
        Warning("FillScaleFactors", "Cannot override an existing map entry with key %s", entry_label.c_str());
    }
}
//-------------------------------------------------------------------
void FillScaleFactors::book_branches_for_global_btag() {
    for (const auto& sysInfo : m_systInfoList) {
        if (not SystematicFunctions::IsNominal(sysInfo.systset) and
            not ST::testAffectsObject(xAOD::Type::BTag, sysInfo.affectsType)) {
            continue;
        }
        auto syst = SystematicFunctions::RenamingRule(sysInfo.systset.name());
        this->add("global_btag_SF_MVX_" + syst);
    }
}
//-------------------------------------------------------------------
void FillScaleFactors::book_branches_for_global_muon_trigger() {
    const bool doMuonTrigSF = m_config->get<bool>("doMuonTrigSF");

    if (not doMuonTrigSF) {
        return;
    }

    for (const auto& sysInfo : m_systInfoList) {
        const bool isNominal = SystematicFunctions::IsNominal(sysInfo.systset);
        if (isNominal or sysInfo.affectedWeights.count(ST::Weights::Muon::Trigger) > 0) {
            auto syst = SystematicFunctions::RenamingRule(sysInfo.systset.name());
            this->add("global_trigSF_" + syst);
            m_fill_global_muon_trigger = true;
        }
    }
}
//-------------------------------------------------------------------
void FillScaleFactors::book_branches_for_global_jvt() {
    for (const auto& sysInfo : m_systInfoList) {
        if (not SystematicFunctions::IsNominal(sysInfo.systset) and
            not ST::testAffectsObject(xAOD::Type::Jet, sysInfo.affectsType)) {
            continue;
        }
        auto syst = SystematicFunctions::RenamingRule(sysInfo.systset.name());
        this->add("global_effSF_JVT_" + syst);
    }
}
//-------------------------------------------------------------------
void FillScaleFactors::book_branches_for_pileup() {
    // TODO: this needs some proper fixing
    // Warning("book_branches_for_pileup()", "pileup weights not implemented yet");

    // this->add(syst + "_pileup_combined_weight");
    for (const auto& sysInfo : m_systInfoList) {
        const bool isNominal = SystematicFunctions::IsNominal(sysInfo.systset);

        bool syst_affectsEventWeight = (sysInfo.affectsType == ST::SystObjType::EventWeight);

        if (not isNominal and not syst_affectsEventWeight) {
            // std::cout << "ignorning sysInfo.systset.name() " << std::endl;
            continue;
        }

        auto syst = SystematicFunctions::RenamingRule(sysInfo.systset.name());

        // this->add(syst + "_pileup_combined_weight");
        this->add("pileupWeight_" + syst);
    }

    // exit(0);
}
//-------------------------------------------------------------------
void FillScaleFactors::book_branches_for_electrons() {
    Info("book_branches_for_electrons()", "booking branches for electrons");
    const bool doElectronSF = m_config->get<bool>("doElectronSF");
    const bool doElectronTrigSF = m_config->get<bool>("doElectronTrigSF");

    Info("book_branches_for_electrons()", "doElectronSF=%d", doElectronSF);
    Info("book_branches_for_electrons()", "doElectronTrigSF=%d", doElectronTrigSF);

    for (const auto& sysInfo : m_systInfoList) {
        const bool isNominal = SystematicFunctions::IsNominal(sysInfo.systset);
        if (not isNominal and not ST::testAffectsObject(xAOD::Type::Electron, sysInfo.affectsType)) {
            continue;
        }

        auto syst = SystematicFunctions::RenamingRule(sysInfo.systset.name());

        if (doElectronSF) {
            if (isNominal or sysInfo.affectedWeights.count(ST::Weights::Electron::ID) > 0) {
                Info("book_branches_for_electrons()", "using %s", syst.c_str());
                const std::string idSFName = "idSF_" + syst;
                this->add(idSFName);
            }

            if (isNominal or sysInfo.affectedWeights.count(ST::Weights::Electron::Isolation) > 0) {
                Info("book_branches_for_electrons()", "using %s", syst.c_str());
                const std::string isoSFName = "isoSF_" + syst;
                this->add(isoSFName);
            }

            if (isNominal or sysInfo.affectedWeights.count(ST::Weights::Electron::Reconstruction) > 0) {
                Info("book_branches_for_electrons()", "using %s", syst.c_str());
                const std::string recoSFName = "recoSF_" + syst;
                this->add(recoSFName);
            }
            // if (isNominal) {
            // Info("book_branches_for_electrons()", "using %s", syst.c_str());
            // const std::string chargeFlipSF = "chargeFlipSF_" + syst;
            // this->add(chargeFlipSF);
            //}
        }

        if (doElectronTrigSF and (isNominal or sysInfo.affectedWeights.count(ST::Weights::Electron::Trigger) > 0)) {
            Info("book_branches_for_electrons()", "using %s", syst.c_str());
            const std::string trigSFName = "trigSF_" + syst;
            this->add(trigSFName);
        }
    }
}
//-------------------------------------------------------------------
void FillScaleFactors::book_branches_for_muons() {
    Info("book_branches_for_muons()", "booking branches for muons");
    const bool doMuonSF = m_config->get<bool>("doMuonSF");
    const bool doMuonTrigSF = m_config->get<bool>("doMuonTrigSF");

    Info("book_branches_for_muons()", "doMuonSF=%d", doMuonSF);
    Info("book_branches_for_muons()", "doMuonTrigSF=%d", doMuonTrigSF);

    for (const auto& sysInfo : m_systInfoList) {
        const bool isNominal = SystematicFunctions::IsNominal(sysInfo.systset);
        if (not isNominal and not ST::testAffectsObject(xAOD::Type::Muon, sysInfo.affectsType)) {
            continue;
        }
        auto syst = SystematicFunctions::RenamingRule(sysInfo.systset.name());

        if (doMuonSF) {
            if (isNominal or sysInfo.affectedWeights.count(ST::Weights::Muon::Reconstruction) > 0) {
                Info("book_branches_for_muons()", "using %s", syst.c_str());
                const std::string recoSFName = "recoSF_" + syst;
                this->add(recoSFName);
            }

            if (isNominal or sysInfo.affectedWeights.count(ST::Weights::Muon::Isolation) > 0) {
                Info("book_branches_for_muons()", "using %s", syst.c_str());
                const std::string isoSFName = "isoSF_" + syst;
                this->add(isoSFName);
            }
        }

        if (doMuonTrigSF and (isNominal or sysInfo.affectedWeights.count(ST::Weights::Muon::Trigger) > 0)) {
            Info("book_branches_for_muons()", "using %s", syst.c_str());
            const std::string trigSFName = "trigSF_" + syst;
            this->add(trigSFName);
        }
    }
}
//-------------------------------------------------------------------
void FillScaleFactors::book_branches_for_taus() {
    Info("book_branches_for_taus()", "booking branches for taus");
    const bool doTauSF = m_config->get<bool>("doTauSF");
    const bool doTauTrigSF = m_config->get<bool>("doTauTrigSF");

    Info("book_branches_for_taus()", "doTauSF=%d", doTauSF);
    Info("book_branches_for_taus()", "doTauTrigSF=%d", doTauTrigSF);

    for (const auto& sysInfo : m_systInfoList) {
        const bool isNominal = SystematicFunctions::IsNominal(sysInfo.systset);
        if (not isNominal and not ST::testAffectsObject(xAOD::Type::Tau, sysInfo.affectsType)) {
            continue;
        }
        auto syst = SystematicFunctions::RenamingRule(sysInfo.systset.name());

        const std::string idSFName = "idSF_" + syst;
        const std::string trigSFName = "trigSF_" + syst;

        if (doTauSF and (isNominal or sysInfo.affectedWeights.count(ST::Weights::Tau::Reconstruction) > 0)) {
            Info("book_branches_for_taus()", "using %s", syst.c_str());
            this->add(idSFName);
        }

        if (doTauTrigSF and (isNominal or sysInfo.affectedWeights.count(ST::Weights::Tau::Trigger) > 0)) {
            Info("book_branches_for_taus()", "using %s", syst.c_str());
            this->add(trigSFName);
        }
    }
}
//-------------------------------------------------------------------
void FillScaleFactors::book_branches_for_photons() {
    Info("book_branches_for_photons()", "booking branches for photons");
    const bool doPhotonSF = m_config->get<bool>("doPhotonSF");

    if (not doPhotonSF) {
        return;
    }

    for (const auto& sysInfo : m_systInfoList) {
        const bool isNominal = SystematicFunctions::IsNominal(sysInfo.systset);
        if (not isNominal and sysInfo.affectedWeights.count(ST::Weights::Photon::Reconstruction) == 0 and
            sysInfo.affectedWeights.count(ST::Weights::Photon::Isolation) == 0) {
            break;
        }

        auto syst = SystematicFunctions::RenamingRule(sysInfo.systset.name());

        Info("book_branches_for_photons()", "using %s", syst.c_str());
        const std::string recoSFName = "recoSF_" + syst;
        this->add(recoSFName);

        Info("book_branches_for_photons()", "using %s", syst.c_str());
        const std::string isoSFName = "isoSF_" + syst;
        this->add(isoSFName);
    }
}
//-------------------------------------------------------------------
void FillScaleFactors::book_branches_for_jets() {
    // btagging
    for (const auto& sysInfo : m_systInfoList) {
        if (not SystematicFunctions::IsNominal(sysInfo.systset) and
            not ST::testAffectsObject(xAOD::Type::BTag, sysInfo.affectsType)) {
            continue;
        }
        auto syst = SystematicFunctions::RenamingRule(sysInfo.systset.name());
        this->add("btag_SF_MVX_" + syst);
        // this->add(syst + "_effSF_MVX");
        // this->add(syst + "_ineffSF_MVX");
    }

    // JVT
    for (const auto& sysInfo : m_systInfoList) {
        if (not SystematicFunctions::IsNominal(sysInfo.systset) and
            not ST::testAffectsObject(xAOD::Type::Jet, sysInfo.affectsType)) {
            continue;
        }
        auto syst = SystematicFunctions::RenamingRule(sysInfo.systset.name());
        this->add("JVT_SF_" + syst);
    }
}
//-------------------------------------------------------------------
FillScaleFactors::~FillScaleFactors() {
    for (auto sf : m_scale_factors)
        if (sf.second) delete sf.second;
    m_scale_factors.clear();
}
//-------------------------------------------------------------------
void FillScaleFactors::fill(const xAOD::IParticle* p) {
    // if(m_jo.GetAuxInfo<bool>("isData")) return; //!< no sense to fill SF branches for real data

    if (!p) {
        return;
    }

    for (const auto& kv : m_scale_factors) {
        *(kv.second) = p->isAvailable<float>(kv.first) ? p->auxdecor<float>(kv.first) : 0;
    }
}
//-------------------------------------------------------------------
void FillScaleFactors::fill(const xAOD::EventInfo* ei) {
    // for decorated attributes in m_scale_factors
    std::for_each(begin(m_scale_factors), end(m_scale_factors), [&, ei](decltype(*m_scale_factors.begin())& entry) {
        // std::cout << entry.first << std::endl;
        *(entry.second) = ei->isAvailable<float>(entry.first) ? ei->auxdecor<float>(entry.first) : 0.f;
    });

    // global muon trigger SF is stored in eventinfo
    if (not m_fill_global_muon_trigger) {
        return;
    }

    for (const auto& sysInfo : m_systInfoList) {
        const bool isNominal = SystematicFunctions::IsNominal(sysInfo.systset);
        if (isNominal or sysInfo.affectedWeights.count(ST::Weights::Muon::Trigger) > 0) {
            auto syst = "global_trigSF_" + SystematicFunctions::RenamingRule(sysInfo.systset.name());
            auto trigSF = ei->isAvailable<float>(syst) ? ei->auxdecor<float>(syst) : 0;
            *(m_scale_factors[syst]) = trigSF;
        }
    }
}
//-------------------------------------------------------------------
void FillScaleFactors::fill(const xAOD::JetContainer* c) {
    //!< no sense to fill SF branches for real data or when the BTaggingEfficiencyTool is disactivated
    // if(m_jo.GetAuxInfo<bool>("isData") ) return;

    // btagging eff
    if (m_fill_global_btag) {
        for (const auto& sysInfo : m_systInfoList) {
            // avoid reading systematic SFs when running on a scale systematic variation
            if (not SystematicFunctions::IsNominal(sysInfo.systset) and
                not ST::testAffectsObject(xAOD::Type::BTag, sysInfo.affectsType)) {
                continue;
            }

            auto syst = SystematicFunctions::RenamingRule(sysInfo.systset.name());

            float bjets_sf = 1.0;
            if (SystematicFunctions::IsNominal(sysInfo.systset)) {
                bjets_sf = m_SUSYTools->BtagSF(c);
            } else {
                bjets_sf = m_SUSYTools->BtagSFsys(c, sysInfo.systset);
            }

            *(m_scale_factors["global_btag_SF_MVX_" + syst]) = bjets_sf;
            //*(m_scale_factors[syst + "_global_btag_effSF_MVX"]) = bjets_sf;
            //*(m_scale_factors[syst + "_global_btag_ineffSF_MVX"]) = bjets_ineff_sf;
        }
    }  // btag

    // jvt eff - only useful if jvt is recalculated
    if (m_fill_global_jvt) {
        for (const auto& sysInfo : m_systInfoList) {
            // avoid reading systematic SFs when running on a scale systematic variation
            if (not SystematicFunctions::IsNominal(sysInfo.systset) and
                not ST::testAffectsObject(xAOD::Type::Jet, sysInfo.affectsType)) {
                continue;
            }

            auto syst = SystematicFunctions::RenamingRule(sysInfo.systset.name());

            // loop over jet collection and get JVT SF
            float jet_jvt_sf = 1.0;
            if (m_SUSYTools->evtStore()->contains<xAOD::JetContainer>("AntiKt4TruthJets")) {
                if (SystematicFunctions::IsNominal(sysInfo.systset)) {
                    jet_jvt_sf = m_SUSYTools->JVT_SF(c);
                } else {
                    jet_jvt_sf = m_SUSYTools->JVT_SFsys(c, sysInfo.systset);
                }
            }

            *(m_scale_factors["global_effSF_JVT_" + syst]) = jet_jvt_sf;
        }
    }  // jvt
}

//-------------------------------------------------------------------
FillTheoryWeights::FillTheoryWeights(VarList& varList, std::string gName, const std::map<std::string, int> weight_map)
    : VarGroup(varList, gName) {
    for (auto const& ent1 : weight_map) {
        // check if the variation is a renormaliation/factorisation variation
        if (checkVarName(ent1.first)) {
            VarF* dummy = new VarF(varList, (ent1.first), group);
            theory_weights.push_back(dummy);
        }
    }

    PDF_central_value = CxxUtils::make_unique<VarD>(vlist, "theory_weights_PDF_central_value");
    PDF_error_up = CxxUtils::make_unique<VarD>(vlist, "theory_weights_PDF_error_up");
    PDF_error_down = CxxUtils::make_unique<VarD>(vlist, "theory_weights_PDF_error_down");
}

//-------------------------------------------------------------------
void FillTheoryWeights::fill(const xAOD::TruthEventContainer* truth_event_cont, const std::map<std::string, int> map, int dsid) {
    std::string pdf_weight_name;
    // if( dsid >= m_jo.sherpa_221_Z_start_dsid() && dsid <= m_jo.sherpa_221_Z_end_dsid()) pdf_weight_name = "MUR1_MUF1_PDF261";

    // bool is_Ztautau = false;

    // if( dsid >= 364100 && dsid <= 364141) is_Ztautau = true;
    // if( dsid >= 344772 && dsid <= 344782) is_Ztautau = true;

    // if( is_Ztautau) {
    // pdf_weight_name = "MUR1_MUF1_PDF261";
    //} else {
    // pdf_weight_name = "LHE3Weight_PDFset=904";
    //}

    pdf_weight_name = "MUR1_MUF1_PDF261";

    std::vector<double> var;
    xAOD::TruthEventContainer::const_iterator itr = truth_event_cont->begin();
    int i = 0;
    for (auto const& ent1 : map) {
        // check if the variation is a renormaliation/factorisation variation
        if (checkVarName(ent1.first)) {
            *(theory_weights[i]) = ((*itr)->weights())[ent1.second];
            i++;
        }

        std::size_t found = (ent1.first).find(pdf_weight_name);
        if (found != std::string::npos) {
            var.emplace_back(((*itr)->weights())[ent1.second]);
        }
    }

    if (!var.empty() && var.size() > 5) {
        const LHAPDF::PDFSet NNPPDF("NNPDF30_nnlo_as_0118");
        const LHAPDF::PDFUncertainty NNPPDF_uncert = NNPPDF.uncertainty(var, -1);
        (*PDF_central_value) = NNPPDF_uncert.central;
        (*PDF_error_up) = NNPPDF_uncert.errplus + NNPPDF_uncert.central;
        (*PDF_error_down) = NNPPDF_uncert.central - NNPPDF_uncert.errminus;
    } else {
        (*PDF_central_value) = 1;
        (*PDF_error_up) = 1.0;
        (*PDF_error_down) = 1.0;
    }
}

//-------------------------------------------------------------------
bool FillTheoryWeights::checkVarName(const std::string var_name) {
    std::string names[] = {"MUR05_MUF05_PDF261000", "MUR05_MUF1_PDF261000", "MUR1_MUF05_PDF261000", "MUR1_MUF1_PDF261000",
                           "MUR1_MUF2_PDF261000",   "MUR2_MUF1_PDF261000",  "MUR2_MUF2_PDF261000",  "muR=05,muF=05",
                           "muR=10,muF=05",         "muR=20,muF=05",        "muR=05,muF=10",        "muR=20,muF=10",
                           "muR=05,muF=20",         "muR=10,muF=20",        "muR=20,muF=20"};
    std::vector<std::string> vec_names(names, names + (sizeof(names) / sizeof(std::string)));

    for (std::vector<std::string>::size_type i = 0; i != vec_names.size(); i++) {
        std::size_t found = (var_name).find(vec_names[i]);
        if (found != std::string::npos) {
            return true;
        }
    }
    return false;
}
