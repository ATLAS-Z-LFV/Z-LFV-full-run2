#include <LFV_Z/DummyAlg.h>

#include <LFV_Z/Config.h>
#include <LFV_Z/EventSelection.h>

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#endif

#ifdef __CINT__
#pragma link C++ class EventSelection + ;
#pragma link C++ class Config + ;
#endif

#ifdef __CINT__
#pragma link C++ class DummyAlg + ;
#endif
