#include <LFV_Z/EventSelection.h>
#include <LFV_Z/Utils.h>

#include <memory>
#include <string>
#include <type_traits>

#include "AsgTools/MsgStream.h"
#include "AsgTools/MsgStreamMacros.h"
#include "TauAnalysisTools/TauSelectionTool.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

constexpr int RootCoreReleaseSeries() {
    return ROOTCORE_RELEASE_SERIES;
}

/// Helper macro for checking xAOD::TReturnCode return values
#define EL_RETURN_CHECK(CONTEXT, EXP)                                                        \
    do {                                                                                     \
        if (!EXP.isSuccess()) {                                                              \
            Error(CONTEXT, XAOD_MESSAGE("Failed to execute %s at line %i"), #EXP, __LINE__); \
            return EL::StatusCode::FAILURE;                                                  \
        }                                                                                    \
    } while (false)

template <typename T, typename std::enable_if<std::is_class<T>::value && std::is_base_of<asg::AsgTool, T>::value, int>::type = 0>
inline EL::StatusCode initializeTool(T* t) {
    if (!t->initialize().isSuccess()) {
        Error("initializeTool", "Failed to properly initialize %s. Exiting.", typeid(T).name());
        return EL::StatusCode::FAILURE;
    }

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode custom_SUSYObjDef_xAOD::SwitchOffInsituTESCorrection() {
    ATH_MSG_INFO("Switching off insitu TES correction");
    if (m_tauSmearingTool.empty()) {
        m_tauSmearingTool.setTypeAndName("TauAnalysisTools::TauSmearingTool/TauSmearingTool");
    }
    if(m_tauSmearingTool.isInitialized()) {
        return EL::StatusCode::FAILURE;
    }
    ANA_CHECK(m_tauSmearingTool.setProperty("ApplyInsituCorrection", false));
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode custom_SUSYObjDef_xAOD::UseCaloTES() {
    ATH_MSG_WARNING("Using calo-TES");
    if (m_tauSmearingTool.empty()) {
        m_tauSmearingTool.setTypeAndName("TauAnalysisTools::TauSmearingTool/TauSmearingTool");
    }
    if(m_tauSmearingTool.isInitialized()) {
        return EL::StatusCode::FAILURE;
    }
    ANA_CHECK(m_tauSmearingTool.setProperty("ApplyMVATES", false));
    ANA_CHECK(m_tauSmearingTool.setProperty("ApplyMVATESQualityCheck", false));
    return EL::StatusCode::SUCCESS;
}

// Since this commit: https://gitlab.cern.ch/atlas/athena/commit/32379bd0
// SUSYTools don't flag isolation anymore if it's not part of the object definition
// Use these functions to flag them
void custom_SUSYObjDef_xAOD::FlagIsoMuons(xAOD::MuonContainer*& copy) {
    for (const auto& muon : *copy) {
        ST::dec_isol(*muon) = m_isoTool->accept(*muon);
    }
}
void custom_SUSYObjDef_xAOD::FlagIsoElectrons(xAOD::ElectronContainer*& copy) {
    for (const auto& electron : *copy) {
        ST::dec_isol(*electron) = m_isoTool->accept(*electron);
    }
}

EL::StatusCode EventSelection::configurePRWTool() {
    std::vector<std::string> configFiles;
    std::vector<std::string> lumicalcFiles;
    std::string path = "LFV_Z/";

    // Find files for MC
    auto PRWConfig = "PileupReweightingMC";
    if (m_config->has(PRWConfig) and m_config->get(PRWConfig) != "") {
        auto _filenames = Utils::splitString(m_config->get(PRWConfig), " ");
        for (const auto& s : _filenames) {
            std::cout << s << std::endl;
        }

        std::transform(begin(_filenames), end(_filenames), std::back_inserter(configFiles),
                       std::bind1st(std::plus<std::string>(), path));
    }

    ATH_MSG_INFO("Using the following PRW files for MC:");
    std::copy(configFiles.begin(), configFiles.end(), std::ostream_iterator<std::string>(this->msg(), "\n"));

    if (configFiles.size() == 0) {
        ATH_MSG_ERROR("Could not find PRW files for MC in '" << PRWConfig << "'! Your weights might be nonsense.");
        return StatusCode::FAILURE;
    }

    // Find files for data
    auto lumiCalcConfig = "PileupReweightingLumiCalc";
    if (m_config->has(lumiCalcConfig) and m_config->get(lumiCalcConfig) != "") {
        auto _filenames = Utils::splitString(m_config->get(lumiCalcConfig), " ");
        std::transform(begin(_filenames), end(_filenames), std::back_inserter(lumicalcFiles),
                       std::bind1st(std::plus<std::string>(), path));
    }
    if (lumicalcFiles.size() > 0) {
        ATH_MSG_INFO("Using the following lumicalc files for data:");
        std::copy(lumicalcFiles.begin(), lumicalcFiles.end(), std::ostream_iterator<std::string>(this->msg(), "\n"));
    } else {
        ATH_MSG_ERROR("Cannot find PRW files for MC in '" << lumiCalcConfig << "'! Your weight for data will be nonsense.");
        return StatusCode::FAILURE;
    }

    EL_RETURN_CHECK("initializePRWTool()", m_SUSYTools->setProperty("PRWConfigFiles", configFiles));
    EL_RETURN_CHECK("initializePRWTool()", m_SUSYTools->setProperty("PRWLumiCalcFiles", lumicalcFiles));

    return StatusCode::SUCCESS;
}

EL::StatusCode EventSelection::initializeSUSYTools() {
    auto config = Config::getInstance();
    
    m_SUSYTools =
        std::shared_ptr<custom_SUSYObjDef_xAOD>(new custom_SUSYObjDef_xAOD("SUSYTools"));
    m_SUSYTools->msg().setLevel(MSG::INFO);
    // m_SUSYTools->msg().setLevel(MSG::VERBOSE);
    // m_SUSYTools->setProperty( "DebugMode",  true ).ignore();
    
    std::string m_tauId = "Medium";
    if (asg::ToolStore::contains<TauAnalysisTools::TauSelectionTool>("TauSelectionTool_" + m_tauId)) {
        auto tauSelTool = asg::ToolStore::get<TauAnalysisTools::TauSelectionTool>("TauSelectionTool_" + m_tauId);
        tauSelTool->msg().setLevel(MSG::VERBOSE);
        // tauSelTool->setProperty("CreateControlPlots", true);
    }
    
    auto datasource = m_config->get<bool>("isData")
                          ? ST::ISUSYObjDef_xAODTool::Data
                          : (config->get<bool>("IsAtlfast") ? ST::ISUSYObjDef_xAODTool::DataSource::AtlfastII
                                                            : ST::ISUSYObjDef_xAODTool::FullSim);
    m_SUSYTools->setProperty("DataSource", datasource).ignore();

    // m_SUSYTools->setProperty("METTauTerm", "").ignore();

    if (m_config->get<bool>("isData")) {
        ATH_MSG_INFO("Will set SUSYTools datasource to data");
    } else if (config->get<bool>("IsAtlfast")) {
        ATH_MSG_INFO("Will set SUSYTools datasource to AtlfastII MC");
    } else {
        ATH_MSG_INFO("Will set SUSYTools datasource FullSim MC");
    }
    
    // Check if we have a valid jet type
    xAOD::JetInput::Type jetType = Utils::JetTypeFromString(m_jetContainerKey);
    if (jetType == xAOD::JetInput::Uncategorized) {
        throw std::domain_error("Could not identify JetType");
    }
    
    // Got a config file?
    if (config->has("SUSYToolsConfig")) {
        auto fname = config->get("SUSYToolsConfig");
        ATH_MSG_INFO("Using SUSYTools config file " << fname);
        m_SUSYTools->setProperty("ConfigFile", fname).ignore();
    }
    
    // Pass on settings to the PRW tool
    if (configurePRWTool() != EL::StatusCode::SUCCESS) {
        throw std::runtime_error("Could not configure PRW tool");
    }
    
    // // Switch off insitu TES
    // if (m_SUSYTools->SwitchOffInsituTESCorrection() != EL::StatusCode::SUCCESS) {
    //     throw std::runtime_error("Could not configure tauSmeraingTool!");
    //     //ATH_MSG_WARNING("Could not configure tauSmeraingTool!");
    // }
    
    if (!m_SUSYTools->initialize().isSuccess()) {
        ATH_MSG_WARNING("!!!!!!!");
        throw std::runtime_error("Could not initialise SUSYTools!");
    }
    
    return StatusCode::SUCCESS;
}
