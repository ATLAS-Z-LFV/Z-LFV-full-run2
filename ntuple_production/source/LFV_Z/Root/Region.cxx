#include <LFV_Z/Region.h>
#include <LFV_Z/SystematicFunctions.h>
#include <LFV_Z/Utils.h>

#include <algorithm>
#include <memory>
#include <string>
#include <vector>

/**
 * @brief Constructor
 */
Region::Region(const std::string &prefix, TFile *outputFile, xAOD::TStore *store, xAOD::TEvent *event, EL::IWorker *wk,
               const std::vector<ST::SystInfo> &systInfoList, const std::map<std::string, int> &theory_systs,
               std::shared_ptr<custom_SUSYObjDef_xAOD> SUSYTools)
    : m_treePrefix(prefix),
      m_outputFile(outputFile),
      m_store(store),
      m_event(event),
      m_wk(wk),
      m_theory_systs(theory_systs),
      m_SUSYTools(SUSYTools),
      m_systInfoList(systInfoList) {}

void Region::DefineSystematics() {
    Info("Region::DefineSystematics()", "Found %lu systematics", m_systInfoList.size());
    for (const auto &sysInfo : m_systInfoList) {
        if (sysInfo.affectsWeights) {
            Info("Region::DefineSystematics()", "\t not building tree for weight sys %s", sysInfo.systset.name().c_str());
            continue;  // keep nominal and kinematic
        }
        Info("Region::DefineSystematics()", "\t using kinematic sys %s", sysInfo.systset.name().c_str());
        m_syst_set_vec.push_back(sysInfo.systset);
    }
}

void Region::AdjustSystematicNames() {
    /**
     * @brief: cache systematic names in @c m_syst_set_vec
     * @note: [&,this] as a lambda-introducer to capture the this pointer by value
     */
    Info("Region::AdjustSystematicNames()", "renaming systematics for ROOT");
    std::transform(m_syst_set_vec.begin(), m_syst_set_vec.end(),                        //!< source
                   std::back_inserter(m_syst_name_vec),                                 //!< destination
                   [&, this](decltype(*m_syst_set_vec.begin()) &syst) -> std::string {  //!< CP::SystematicSet &
                       return SystematicFunctions::RenamingRule(syst.name());
                   }  //!< C++ Lambda anonymous function
                   );
}

void Region::InitializeTrees() {
    /**
     * @brief Initialize trees
     * @note TObject needs to be created on the heap,
     * otherwise the worker can not take ownership
     * Do no call delete on the pointers!
     */
    for (const auto &syst : m_syst_name_vec) {
        Info("Region::InitializeTrees()", "initializing for systematic: %s", syst.c_str());
        m_tree_list.add(m_treePrefix + syst, new TTree((m_treePrefix + syst).c_str(), syst.c_str())  // "new" required by workers
                        );
    }
    // Limit total memory usage of ALL trees together to 512 MB
    m_tree_list.set_auto_flush(-512*1024*1024);
}

void Region::AssociateOutputFile() {
    m_tree_list.set_directory(m_outputFile);
}

void Region::LinkVariables() {
    ASSERT_WITH_MESSAGE(!vlist.empty(), "Region::LinkVariables - empty variables list 'vlist'");

    m_tree_list.set_max_tree_size();
    Info("Region::LinkVariables", "printing variable list");
    vlist.print();
    Info("Region::LinkVariables", "linking branches");
    m_tree_list.link_branches(vlist);
}

// Do NOT add trees to worker,
// otherwise they will not be linked to an output file on disk,
// and will keep consuming memory until the job is done!!
//void Region::AddTreesWorkerOutput() {
//    m_tree_list.add_trees_to_worker(m_wk);
//}

void Region::SetupTrees() {
    // tree operations will be executed in the following logical order
    Info("Region::SetupTrees()", "setting up trees");
    /*0*/ DefineSystematics();
    /*1*/ AdjustSystematicNames();
    /*2*/ InitializeTrees();
    /*3*/ AssociateOutputFile();
    /*4*/ LinkVariables();
    // AddTreesWorkerOutput();  // Do NOT add trees to worker

    /*5*/  // further checks?
}

void Region::ResetVariables() {
    vlist.clear();  //<! reset VarList variables
}

void Region::FillTree(const std::string &systName) {
    // Info("Region::FillTree()", "attempting to fill %s", m_treePrefix + SystematicFunctions::RenamingRule(systName));
    m_tree_list.fill(m_treePrefix + SystematicFunctions::RenamingRule(systName));
}

void Region::WriteTrees(TDirectory* file) {
    m_tree_list.write(file);
}

void Region::Finalize() {
    WriteTrees(m_outputFile);
}
