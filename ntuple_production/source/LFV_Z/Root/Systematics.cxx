#include <LFV_Z/EventSelection.h>
#include <string>
#include <vector>
#include "TRegexp.h"

#include "LFV_Z/SystematicFunctions.h"

void EventSelection::initializeSystematics() {
    ATH_MSG_INFO("EventSelection::initializeSystematics()");

    // Do we actually want systematics?
    if (!m_config->get<bool>("doSystematics")) {
        // we can't detect at this stage whether you're running data or not. This is fully up to you.
        initializeNoSystematics();
        return;
    }

    // If we don't select anything, use everything
    std::vector<ST::SystInfo> sysInfos = m_SUSYTools->getSystInfoList();
    if (m_systMatch.empty()) {
        m_systInfoList = sysInfos;
        for (const auto& sys : sysInfos) {
            const CP::SystematicSet& systSet = sys.systset;
            ATH_MSG_INFO(" => Will use systematic " << systSet.name());

            if (SystematicFunctions::IsWeight(systSet.name())) {
                ATH_MSG_DEBUG(" => (weight)");
                m_systInfoList_weights.push_back(sys);
            } else {
                ATH_MSG_DEBUG(" => (kinematic)");
                m_systInfoList_kinematic.push_back(sys);
            }
        }
        return;
    }

    // If we do select, perform a regexp match against the names and pick ones that match
    for (const auto& sys : sysInfos) {
        const CP::SystematicSet& systSet = sys.systset;
        auto name = systSet.name();
        bool matched = false;
        if (name == "") {
            matched = true;
        } else {
            // check if name matches wildcard expression
            for (const auto& s : m_systMatch) {
                if (s == "") continue;  // this is a ridiculous case that only happens if the string is split wrong
                TRegexp re = TRegexp(s.c_str(), kTRUE);
                Ssiz_t l;
                if (re.Index(name, &l) >= 0) {
                    matched = true;
                    break;
                }
            }
        }

        if (matched) {
            m_systInfoList.push_back(sys);
            if (name == "") {
                ATH_MSG_INFO(" => Will use systematic '' (<nominal>) " << name);
            } else {
                ATH_MSG_INFO(" => Will use systematic " << name);
            }

            if (SystematicFunctions::IsWeight(systSet.name())) {
                ATH_MSG_DEBUG(" => (weight)");
                m_systInfoList_weights.push_back(sys);
            } else {
                ATH_MSG_DEBUG(" => (kinematic)");
                m_systInfoList_kinematic.push_back(sys);
            }
        } else {
            ATH_MSG_DEBUG(" => Will _not_ use systematic " << name);
        }
    }
}

void EventSelection::initializeNoSystematics() {
    ATH_MSG_INFO("EventSelection::initializeNoSystematics()");
    // fill with default "no systematics"
    ST::SystInfo infodef;
    infodef.affectsKinematics = false;
    infodef.affectsWeights = false;
    infodef.affectsType = ST::Unknown;
    m_systInfoList.push_back(infodef);
}
