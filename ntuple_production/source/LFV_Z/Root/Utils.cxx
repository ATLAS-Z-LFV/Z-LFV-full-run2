// vim: ts=4 sw=4
#include "LFV_Z/Utils.h"

#include <algorithm>
#include <string>
#include <vector>

#include "AthContainers/AuxElement.h"
#include "xAODRootAccess/TActiveStore.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTau/TauTrack.h"

#include "Extrapolate/Track.h"
#include "Extrapolate/Vertex.h"

static SG::AuxElement::Accessor<float> acc_z0sinTheta("z0sinTheta");
static SG::AuxElement::Accessor<float> acc_d0sig("d0sig");

xAOD::JetInput::Type Utils::JetTypeFromString(const std::string& algname) {
    if (algname.find(xAOD::JetInput::typeName(xAOD::JetInput::LCTopo)) != std::string::npos) {
        return xAOD::JetInput::LCTopo;
    }

    if (algname.find(xAOD::JetInput::typeName(xAOD::JetInput::EMTopo)) != std::string::npos) {
        return xAOD::JetInput::EMTopo;
    }

    if (algname.find(xAOD::JetInput::typeName(xAOD::JetInput::EMPFlow)) != std::string::npos) {
        return xAOD::JetInput::EMPFlow;
    }

    return xAOD::JetInput::Uncategorized;
}

const xAOD::Vertex* Utils::GetPrimaryVertex(xAOD::TEvent& event) {
    const xAOD::VertexContainer* vertices = nullptr;
    if (!event.retrieve(vertices, "PrimaryVertices").isSuccess()) {
        return nullptr;
    }

    for (const auto& vx : *vertices) {
        if (vx->vertexType() == xAOD::VxType::PriVtx) {
            return vx;
        }
    }

    return nullptr;
}

const xAOD::EventInfo* Utils::GetEventInfo(xAOD::TEvent& event) {
    const xAOD::EventInfo* eventInfo = 0;
    if (!event.retrieve(eventInfo, "EventInfo").isSuccess()) {
        throw std::runtime_error("Could not retrieve EventInfo");
    }

    return eventInfo;
}

const std::string Utils::getSystematicSuffix(const CP::SystematicSet& systSet) {
    if (systSet.empty()) {
        return "";
    }
    return "_" + systSet.name();
}

const Utils::EventInfo Utils::ExtractEventInfo(const xAOD::EventInfo* eventInfo, bool isData) {
    EventInfo info;

    info.RunNumber = eventInfo->runNumber();
    info.EventNumber = eventInfo->eventNumber();
    info.LumiBlockNumber = eventInfo->lumiBlock();
    info.mc_channel_number = 0;
    if (!isData) {
        info.mc_channel_number = eventInfo->mcChannelNumber();
    }

    return info;
}

const std::vector<std::string> Utils::splitString(const std::string& s, const std::string& delim) {
    assert(delim.length() == 1);
    std::vector<std::string> retval;
    retval.reserve(std::count(s.begin(), s.end(), delim[0]) + 1);
    size_t last = 0;
    size_t next = 0;
    while ((next = s.find(delim, last)) != std::string::npos) {
        retval.emplace_back(s.substr(last, next - last));
        last = next + delim.length();
    }
    retval.emplace_back(s.substr(last));

    return retval;
}

CP::SystematicSet* Utils::GetCurrentSystematicSet() {
    xAOD::TStore* store = xAOD::TActiveStore::store();
    CP::SystematicSet* currentSystematicSet = 0;
    if (!store->retrieve(currentSystematicSet, "CurrentSystematicSet").isSuccess()) {
        throw std::runtime_error("Could not retrieve CurrentSystematicSet");
    }

    return currentSystematicSet;
}

// TODO: named enum?
int ParticleUtils::IsHadronicTau(const xAOD::TruthParticle* p) {
    if (std::abs(p->pdgId()) != 15) return -1;
    if (p->status() == 3) return -1;

    for (unsigned int i = 0; i < p->nChildren(); ++i) {
        xAOD::TruthParticle* child = (xAOD::TruthParticle*)p->child(i);
        if (!child) return -1;
        if (child->status() == 3) continue;
        if (std::abs(child->pdgId()) == 11 || std::abs(child->pdgId()) == 13) {
            return 0;
        }
    }

    return 1;
}

std::vector<const xAOD::TruthParticle*> ParticleUtils::GetTruthTaus(const xAOD::TruthParticleContainer* c,
                                                                    const std::string& type) {
    static const auto highVisPtOrder = [](const xAOD::TruthParticle* a, const xAOD::TruthParticle* b) {
        return a->auxdata<double>("pt_vis") > b->auxdata<double>("pt_vis");
    };

    std::vector<const xAOD::TruthParticle*> results;
    results.reserve(c->size());
    for (const auto& p : *c) {
        int status = ParticleUtils::IsHadronicTau(p);
        if (type == "Had" && status == 1) {
            results.push_back(p);
        } else if (type == "Lep" && status == 0) {
            results.push_back(p);
        } else if (type == "All" && status != -1) {
            results.push_back(p);
        }
    }
    std::sort(results.begin(), results.end(), highVisPtOrder);
    return results;
}

int ParticleUtils::FindTrueElectron(const xAOD::TruthParticleContainer* c, const xAOD::IParticle* e) {
    return FindTrueParticle(c, e, 11);
}

int ParticleUtils::FindTrueMuon(const xAOD::TruthParticleContainer* c, const xAOD::IParticle* m) {
    return FindTrueParticle(c, m, 13);
}

int ParticleUtils::FindTrueTau(const xAOD::TruthParticleContainer* c, const xAOD::TauJet* tau) {
    if (!c) {
        return -1;
    }
    const float min_dR = 0.2;
    float highPt = 0.;
    int idx = -1;
    const xAOD::TruthParticle* t(0);
    for (unsigned int i = 0; i < c->size(); ++i) {
        t = c->at(i);
        if (t == 0) continue;

        const auto t_pt = t->pt();
        if (t_pt < 10000) continue;

        const auto t_pdgId = t->absPdgId();
        if (t_pdgId > 22) continue;                                     // ignore everything beyond photon
        if (t_pdgId == 12 || t_pdgId == 14 || t_pdgId == 16) continue;  // ignore neutrinos
        if (t_pdgId < 7) continue;                                      // ignore quarks
        if (t_pdgId == 21 && t_pt < 4000.0) continue;                   // ignore soft photons

        if (t_pdgId == 15) {
            // calculate the visible 4-momentum for matching: start with tau momentum and subtract momenta of all neutrino
            // children
            static TLorentzVector vis_tau;
            vis_tau.SetPtEtaPhiE(t->pt(), t->eta(), t->phi(), t->e());
            for (unsigned int j = 0; j < t->nChildren(); j++) {
                const auto child_pdgId = t->child(i)->absPdgId();
                if (child_pdgId == 12 || child_pdgId == 14 || child_pdgId == 16) {
                    vis_tau -= t->child(i)->p4();
                }
            }
            if (vis_tau.DeltaR(tau->p4()) < min_dR) {
                idx = i;
                break;
            }
        } else if ((t_pdgId == 11 or t_pdgId == 13) && t_pt > highPt && t->p4().DeltaR(tau->p4()) < min_dR) {
            // lepton matching
            highPt = t->pt();
            idx = i;
        } else if (t->pt() > highPt && ((t->p4()).DeltaR(tau->p4()) < min_dR)) {
            // match to others
            highPt = t->pt();
            idx = i;
        }
    }

    return idx;
}

int ParticleUtils::FindTrueParticle(const xAOD::TruthParticleContainer* c, const xAOD::IParticle* p, const int absPdgId) {
    if (not c) {
        return -1;
    }
    const float min_dR = 0.2;
    float highPt = 0.;
    int idx = -1;
    const xAOD::TruthParticle* t = nullptr;
    for (unsigned int i = 0; i < c->size(); ++i) {
        t = c->at(i);
        if (t == 0) continue;

        const auto t_pt = t->pt();
        if (t_pt < 10000) continue;

        const auto t_pdgId = t->absPdgId();
        if (t_pdgId == absPdgId && t->p4().DeltaR(p->p4()) < min_dR) {
            // exact PDG id and in cone
            idx = i;
            break;
        }

        if (t_pdgId > 22) continue;                                     // ignore everything beyond photon
        if (t_pdgId == 12 || t_pdgId == 14 || t_pdgId == 16) continue;  // ignore neutrinos
        if (t_pdgId < 7) continue;                                      // ignore quarks
        if (t_pdgId == 21 && t_pt < 4000.0) continue;                   // ignore soft photons

        if (t_pdgId == 21 && t_pt > highPt && t->p4().DeltaR(p->p4()) < 0.4) {
            // photons in 0.4 cone
            highPt = t_pt;
            idx = i;
        } else if (t_pdgId != 21 && t_pt > highPt && t->p4().DeltaR(p->p4()) < min_dR) {
            // anything else in a 0.2 cone
            highPt = t_pt;
            idx = i;
        }
    }
    return idx;
}

bool ParticleUtils::InCrackRegion(const xAOD::Electron* el) {
    if (fabs(el->caloCluster()->etaBE(2)) > 1.37 && fabs(el->caloCluster()->etaBE(2)) < 1.52) {
        return true;
    }
    return false;
}

// TODO: templates!!!!
float ParticleUtils::GetCharge(const xAOD::IParticle* p) {
    if (p->type() == xAOD::Type::Muon)
        return dynamic_cast<const xAOD::Muon*>(p)->charge();
    else if (p->type() == xAOD::Type::Electron)
        return dynamic_cast<const xAOD::Electron*>(p)->charge();
    else if (p->type() == xAOD::Type::Tau)
        return dynamic_cast<const xAOD::TauJet*>(p)->charge();
    else if (p->type() == xAOD::Type::TrackParticle)
        return dynamic_cast<const xAOD::TrackParticle*>(p)->charge();
    else if (p->type() == xAOD::Type::TruthParticle)
        return dynamic_cast<const xAOD::TruthParticle*>(p)->charge();
    else
        return 0;
}

double ParticleUtils::d0sig(const xAOD::Electron* el) {
    double _d0sig = acc_d0sig(*el);
    if (_d0sig != 0) {
        return _d0sig;
    }

    throw std::runtime_error("Cannot calculate d0sig for an electron without a track");
}
double ParticleUtils::d0sig(const xAOD::Muon* mu) {
    double _d0sig = acc_d0sig(*mu);
    if (_d0sig != 0) {
        return _d0sig;
    }

    throw std::runtime_error("Cannot calculate d0sig for a muon without a track");
}
double ParticleUtils::d0sig(const xAOD::TauJet* tau) {
    const int nTracks = tau->nTracks();
    if (nTracks == 1) {
        return xAOD::TrackingHelpers::d0significance(tau->track(0)->track());
    } else if (nTracks == 3) {
        auto vtx = Vertex(Track(tau->track(0)), Track(tau->track(1)), Track(tau->track(2)));
        // auto chi2 = vtx.fit(); // chi2 is not used anyway?
        vtx.fit();
        return vtx.d0sig();
    }

    throw std::runtime_error("Cannot calculate d0sig for non 1,3-prong taus");
    return -999;
}

double ParticleUtils::z0sinTheta(const xAOD::Electron* el) {
    double _z0sinTheta = acc_z0sinTheta(*el);
    if (_z0sinTheta != 0) {
        return _z0sinTheta;
    }

    throw std::runtime_error("Cannot calculate z0sinTheta for an electron without a track");
}
double ParticleUtils::z0sinTheta(const xAOD::Muon* mu) {
    double _z0sinTheta = acc_z0sinTheta(*mu);
    if (_z0sinTheta != 0) {
        return _z0sinTheta;
    }

    throw std::runtime_error("Cannot calculate z0sinTheta for a muon without a track");
}
double ParticleUtils::z0sinTheta(const xAOD::TauJet* tau) {
    const int nTracks = tau->nTracks();
    if (nTracks == 1) {
        const double primvertex_z = 0.0;  // TODO: hack
        const auto track = tau->track(0)->track();
        return (track->z0() + track->vz() - primvertex_z) * TMath::Sin(tau->p4().Theta());
    } else if (nTracks == 3) {
        auto vtx = Vertex(Track(tau->track(0)), Track(tau->track(1)), Track(tau->track(2)));
        // auto chi2 = vtx.fit(); // chi2 is not used anyway?
        vtx.fit();
        return vtx.z0sintheta();
    }
    throw std::runtime_error("Cannot calculate z0sintheta for non 1,3-prong taus");
}

double ParticleUtils::doca(const xAOD::TrackParticle* trk, const xAOD::Vertex* vtx, TVector3& beamPos) {
    double a0 = -999.;

    double phi = trk->phi();
    double eta = trk->eta();

    double x0 = -trk->d0() * std::sin(phi);
    double y0 = trk->d0() * std::cos(phi);
    double z0 = trk->z0() + trk->vz();
    double vtx_x = vtx->x() - beamPos.X();
    double vtx_y = vtx->y() - beamPos.Y();
    double vtx_z = vtx->z() - beamPos.Z();

    TVector3 dx(x0 - vtx_x, y0 - vtx_y, z0 - vtx_z);
    TVector3 p(std::cos(phi), std::sin(phi), std::sinh(eta));
    TVector3 norm = p.Cross(dx).Cross(p).Unit();

    a0 = dx.Dot(norm);

    return a0;
}
double ParticleUtils::doca(Vertex* trk, const xAOD::Vertex* vtx, TVector3& beamPos) {
    double a0 = -999.;

    double phi = trk->phi();
    double eta = -std::log(std::tan(0.5 * trk->theta()));

    double x0 = -trk->d0() * std::sin(phi);
    double y0 = trk->d0() * std::cos(phi);
    double z0 = trk->z0();
    double vtx_x = vtx->x() - beamPos.X();
    double vtx_y = vtx->y() - beamPos.Y();
    double vtx_z = vtx->z() - beamPos.Z();

    TVector3 dx(x0 - vtx_x, y0 - vtx_y, z0 - vtx_z);
    TVector3 p(std::cos(phi), std::sin(phi), std::sinh(eta));
    TVector3 norm = p.Cross(dx).Cross(p).Unit();

    a0 = dx.Dot(norm);

    return a0;
}

unsigned short VertexUtils::CountVertices(const xAOD::VertexContainer* c, xAOD::VxType::VertexType vxType,
                                          unsigned short minNtrks, double maxAbsZ) {
    if (!c) return 0;
    return std::count_if(c->begin(), c->end(), [&vxType, &minNtrks, &maxAbsZ](const xAOD::Vertex* vtx) -> unsigned short {
        return (vtx->vertexType() == vxType &&  // xAOD::VxType::PriVtx &&
                vtx->nTrackParticles() >= minNtrks && std::fabs(vtx->z()) <= maxAbsZ);
    });
}

unsigned short VertexUtils::CountVertices(const xAOD::VertexContainer* c, unsigned short minNtrks, double maxAbsZ) {
    if (!c) return 0;
    return std::count_if(c->begin(), c->end(), [&minNtrks, &maxAbsZ](const xAOD::Vertex* vtx) -> unsigned short {
        return (vtx->nTrackParticles() >= minNtrks && std::fabs(vtx->z()) <= maxAbsZ);
    });
}

void ContainerUtils::PtSort(xAOD::IParticleContainer* c) {
    if (c->empty()) return;

    std::sort(c->begin(), c->end(), [](const xAOD::IParticle* a,
                                       const xAOD::IParticle* b) -> bool { return a->pt() > b->pt(); }  // C++11 lambda function
              );
}

bool ContainerUtils::HasAtLeast(const xAOD::IParticleContainer* c, unsigned int n) {
    return c ? (c->size() >= n) : false;
}

bool ContainerUtils::IsEmpty(const xAOD::IParticleContainer* c) {
    if (!c) return false;
    return c->empty();
}

bool ContainerUtils::IsNonEmpty(const xAOD::IParticleContainer* c) {
    // not the negation of IsEmpty() due to null ptr protection
    if (!c) return false;
    return not c->empty();
}

unsigned short ContainerUtils::Size(const xAOD::IParticleContainer* c) {
    return c ? c->size() : 0;
}

unsigned int Count(const xAOD::IParticleContainer* c, const double pT) {
    if (pT < 0.00001) return c->size();
    return std::count_if(c->begin(), c->end(), [&](decltype(*c->begin()) p) { return p->pt() > pT; });
}

double KinematicUtils::DeltaPhi(const TLorentzVector& v, const TLorentzVector& u) {
    return v.DeltaPhi(u);
}

double KinematicUtils::DeltaPhi(const xAOD::IParticleContainer* c1, const xAOD::IParticleContainer* c2) {
    if (static_cast<unsigned int>(c1->size()) && static_cast<unsigned int>(c2->size())) {
        return std::fabs((*c1)[0]->p4().DeltaPhi((*c2)[0]->p4()));
    } else {
        Warning("KinematicUtils::DeltaPhi", "at least one container has no elements, %lui or %lui  ", c1->size(), c2->size());
    }

    return -11;
}

double KinematicUtils::DeltaPhi(const xAOD::IParticleContainer* c1, unsigned int i, const xAOD::IParticleContainer* c2,
                                unsigned int j) {
    if (i < static_cast<unsigned int>(c1->size()) && j < static_cast<unsigned int>(c2->size()))
        return std::fabs((*c1)[i]->p4().DeltaPhi((*c2)[j]->p4()));
    else
        Warning("KinematicUtils::DeltaPhi", "elements %i or %i outside the range of the input containers... ", i, j);

    return -11;
}

double KinematicUtils::DeltaPhi(const xAOD::IParticleContainer* c) {
    if (c->size() > 1) {
        return std::fabs((*c)[0]->p4().DeltaPhi((*c)[1]->p4()));
    } else {
        Warning("KinematicUtils::DeltaPhi", "too few elements in the container : %lui ", c->size());
    }

    return -11;
}

double KinematicUtils::DeltaPhi(const xAOD::IParticle* p1, const xAOD::IParticle* p2) {
    return DeltaPhi(p1->phi(), p2->phi());
}

double KinematicUtils::DeltaPhi(const xAOD::IParticle* p, const xAOD::MissingET* met) {
    return DeltaPhi(p->phi(), met->phi());
}

double KinematicUtils::DeltaPhi(const xAOD::IParticleContainer* c, unsigned int i, const xAOD::MissingET* met) {
    if (c->size() > i) {
        return (DeltaPhi((*c)[i]->phi(), met->phi()));
    } else {
        Warning("KinematicUtils::DeltaPhi", "too few elements (%lui) for the input container ", c->size());
    }
    return -11;
}

double KinematicUtils::DeltaPhi(const xAOD::IParticleContainer* c, const xAOD::MissingET* met) {
    if (c->size() > 0) {
        return DeltaPhi((*c)[0]->phi(), met->phi());
    } else {
        Warning("KinematicUtils::DeltaPhi", "too few elements (%lui) for the input container ", c->size());
    }
    return -11;
}

double KinematicUtils::DeltaPhi(double phi1, double phi2) {
    return TMath::Pi() - std::fabs(std::fabs(phi1 - phi2) - TMath::Pi());
}

double KinematicUtils::CosDeltaPhi(double phi1, double phi2) {
    return std::cos(DeltaPhi(phi1, phi2));
}

double KinematicUtils::CosDeltaPhi(const xAOD::IParticleContainer* c1, const xAOD::IParticleContainer* c2) {
    if (static_cast<unsigned int>(c1->size()) && static_cast<unsigned int>(c2->size())) {
        return std::cos(std::fabs((*c1)[0]->p4().DeltaPhi((*c2)[0]->p4())));
    } else {
        Warning("KinematicUtils::CosDeltaPhi", "at least one container has no elements, %lui or %lui  ", c1->size(), c2->size());
    }

    return -11;
}

double KinematicUtils::mT(const xAOD::IParticle* p, const xAOD::MissingET* met) {
    if (!p) {
        return -1;
    }
    return std::sqrt(2 * p->pt() * met->met() * (1 - CosDeltaPhi(p->phi(), met->phi())));
}

double KinematicUtils::mT(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::MissingET* met) {
    if (!p1 or !p2) {
        return -1;
    }
    TLorentzVector p = p1->p4() + p2->p4();
    return std::sqrt(2 * p.Pt() * met->met() * (1 - CosDeltaPhi(p.Phi(), met->phi())));
}

double KinematicUtils::mT(const xAOD::IParticleContainer* c, const xAOD::MissingET* met) {
    if (static_cast<unsigned int>(c->size())) {
        return mT((*c)[0], met);
    } else {
        Warning("KinematicUtils::mT", "input container has no elements %lui", c->size());
    }

    return -1;
}

double KinematicUtils::mT(const xAOD::IParticleContainer* c, unsigned int i, const xAOD::MissingET* met) {
    if (i < static_cast<unsigned int>(c->size()))
        return mT((*c)[i], met);
    else
        Warning("KinematicUtils::mT", "cannot access element %i of input container with size %lui", i, c->size());

    return -1;
}

double KinematicUtils::mT(const TLorentzVector& p, const xAOD::MissingET* met) {
    return std::sqrt(2 * p.Pt() * met->met() * (1 - CosDeltaPhi(p.Phi(), met->phi())));
}

double KinematicUtils::DeltaR(const TLorentzVector& v, const TLorentzVector& u) {
    return v.DeltaR(u);
}

double KinematicUtils::DeltaR(const xAOD::IParticleContainer* c, unsigned int i, unsigned int j) {
    if (i < static_cast<unsigned int>(c->size()) && j < static_cast<unsigned int>(c->size())) {
        return (*c)[i]->p4().DeltaR((*c)[j]->p4());
    } else {
        Warning("KinematicUtils::DeltaR", "elements %i or %i outside the range of  the container ... ", i, j);
    }

    return -1;
}

double KinematicUtils::DeltaR(const xAOD::IParticleContainer* c) {
    if (c->size() > 1) {
        return (*c)[0]->p4().DeltaR((*c)[1]->p4());
    } else {
        Warning("KinematicUtils::DeltaR", "too few elements in the container ");
    }

    return -1;
}

double KinematicUtils::DeltaR(const xAOD::IParticle* p1, const xAOD::IParticle* p2) {
    return p1->p4().DeltaR(p2->p4());
}

double KinematicUtils::DeltaR(const xAOD::IParticleContainer* c1, const xAOD::IParticleContainer* c2) {
    if (static_cast<unsigned int>(c1->size()) && static_cast<unsigned int>(c2->size())) {
        return (*c1)[0]->p4().DeltaR((*c2)[0]->p4());
    } else {
        Warning("KinematicUtils::DeltaR", "at least one container has no elements, %lui or %lui  ", c1->size(), c2->size());
    }
    return -1;
}

double KinematicUtils::DeltaR(const xAOD::IParticleContainer* c1, unsigned int i, const xAOD::IParticleContainer* c2,
                              unsigned int j) {
    if (i < static_cast<unsigned int>(c1->size()) && j < static_cast<unsigned int>(c2->size())) {
        return (*c1)[i]->p4().DeltaR((*c2)[j]->p4());
    } else {
        Warning("KinematicUtils::DeltaR", "elements %i or %i outside the range of the input containers", i, j);
    }

    return -1;
}

double KinematicUtils::DeltaEta(const TLorentzVector& v, const TLorentzVector& u) {
    return std::fabs(v.Eta() - u.Eta());
}

double KinematicUtils::DeltaEta(const xAOD::IParticle* p1, const xAOD::IParticle* p2) {
    return std::fabs(p1->eta() - p2->eta());
}

double KinematicUtils::DeltaEta(const xAOD::IParticleContainer* c, unsigned int i, unsigned int j) {
    if (i < static_cast<unsigned int>(c->size()) && j < static_cast<unsigned int>(c->size())) {
        return std::fabs((*c)[i]->eta() - (*c)[j]->eta());
    } else {
        Warning("KinematicUtils::DeltaEta", "elements %i, %i outside the range of  the container ", i, j);
    }
    return -1;
}

double KinematicUtils::DeltaEta(const xAOD::IParticleContainer* c) {
    if (c->size() > 1) {
        return std::fabs((*c)[0]->eta() - ((*c)[1]->eta()));
    } else {
        Warning("KinematicUtils::DeltaEta", "too few elements in the container ");
    }

    return -1;
}

double KinematicUtils::DeltaEta(const xAOD::IParticleContainer* c1, const xAOD::IParticleContainer* c2) {
    if (static_cast<unsigned int>(c1->size()) && static_cast<unsigned int>(c2->size())) {
        return std::fabs((*c1)[0]->eta() - (*c2)[0]->eta());
    } else {
        Warning("KinematicUtils::DeltaEta", "at least one container has no elements, %lui or %lui  ", c1->size(), c2->size());
    }
    return -1;
}

double KinematicUtils::DeltaEta(const xAOD::IParticleContainer* c1, unsigned int i, const xAOD::IParticleContainer* c2,
                                unsigned int j) {
    if (i < static_cast<unsigned int>(c1->size()) && j < static_cast<unsigned int>(c2->size())) {
        return std::fabs((*c1)[i]->eta() - (*c2)[j]->eta());
    } else {
        Warning("KinematicUtils::DeltaEta", "elements %i or %i outside the range of the input containers... ", i, j);
    }
    return -1;
}

double KinematicUtils::DeltaEta(double eta1, double eta2) {
    return std::fabs(eta1 - eta2);
}

double KinematicUtils::VisibleMass(const xAOD::IParticle* p1, const xAOD::IParticle* p2) {
    if (!p1 and !p2) {
        Warning("KinematicUtils::VisibleMass", "Null pointers passed to the function");
        return -1;
    }
    return (p1->p4() + p2->p4()).M();
}

double KinematicUtils::VisibleMass(const xAOD::IParticle* p1, const xAOD::IParticle* p2, const xAOD::IParticle* p3) {
    if (!p1 or !p2 or !p3) {
        Warning("KinematicUtils::VisibleMass", "Null pointers passed to the function");
        return -1;
    }
    return (p1->p4() + p2->p4() + p3->p4()).M();
}

double KinematicUtils::VisibleMass(const xAOD::IParticleContainer* c1, const xAOD::IParticleContainer* c2) {
    if (!c1 or !c2) {
        Warning("KinematicUtils::VisibleMass", "at least one container is a nullptr");
        return -1;
    }

    if (c1->size() == 0 || c2->size() == 0) {
        Warning("KinematicUtils::VisibleMass", "at least one container has no elements, %lui or %lui  ", c1->size(), c2->size());
        return -1;
    }

    return ((*c1)[0]->p4() + (*c2)[0]->p4()).M();
}

double KinematicUtils::VisibleMass(const xAOD::IParticleContainer* c1, unsigned int i, const xAOD::IParticleContainer* c2,
                                   unsigned int j) {
    if (!c1 or !c2) {
        Warning("KinematicUtils::VisibleMass", "at least one container is a nullptr");
        return -1;
    }

    if (c1->size() <= i || c2->size() <= j) {
        Warning("KinematicUtils::VisibleMass", "at least one container doesn't contain enough elements (%lui and %lui)",
                c1->size(), c2->size());
        return -1;
    }

    return ((*c1)[i]->p4() + (*c2)[j]->p4()).M();
}

double KinematicUtils::VisibleMass(const xAOD::IParticleContainer* c) {
    if (c->size() < 2) {
        Warning("KinematicUtils::VisibleMass", "too few elements for the input container %lui ", c->size());
        return -1;
    }

    return ((*c)[0]->p4() + (*c)[1]->p4()).M();
}

double KinematicUtils::InvariantMass(std::initializer_list<TLorentzVector> l) {
    return KinematicUtils::Mass(l);
}

double KinematicUtils::Mass(std::initializer_list<TLorentzVector> l) {
    TLorentzVector v;
    // TODO: ought to be re-written without TLorentzVectors for speed
    for (const auto& u : l) {
        v += u;
    }

    return v.M();
}

double KinematicUtils::ColinearMass(const xAOD::TauJet* tau, const xAOD::IParticle* lep, const xAOD::MissingET* met) {
    return CollinearMass(tau, lep, met);
}

double KinematicUtils::CollinearMass(const xAOD::TauJet* tau, const xAOD::IParticle* lep, const xAOD::MissingET* met) {
    TLorentzVector neutrino_p4;
    neutrino_p4.SetPtEtaPhiM(met->met(), tau->eta(), met->phi(), 0.0);

    // TODO: is the compiler clever enough to figure this out or is it going to allocate 2 TLorentzVectors?
    TLorentzVector sum_p4 = (tau->p4() + neutrino_p4) + lep->p4();
    return sum_p4.M();
}

double KinematicUtils::CollinearMass(const xAOD::TauJet* tau, const xAOD::IParticle* lep, const xAOD::Photon* photon,
                                     const xAOD::MissingET* met) {
    TLorentzVector neutrino_p4;
    neutrino_p4.SetPtEtaPhiM(met->met(), tau->eta(), met->phi(), 0.0);

    // TODO: is the compiler clever enough to figure this out or is it going to allocate 2 TLorentzVectors?
    TLorentzVector sum_p4 = (tau->p4() + neutrino_p4) + lep->p4() + photon->p4();
    return sum_p4.M();
}

double KinematicUtils::CollinearMass(const xAOD::IParticle* lep_0, const xAOD::IParticle* lep_1, const xAOD::MissingET* met) {
    TLorentzVector neutrino_p4;
    neutrino_p4.SetPtEtaPhiM(met->met(), lep_0->eta(), met->phi(), 0.0);

    // TODO: is the compiler clever enough to figure this out or is it going to allocate 2 TLorentzVectors?
    TLorentzVector sum_p4 = (lep_0->p4() + neutrino_p4) + lep_1->p4();
    return sum_p4.M();
}

double KinematicUtils::CollinearMass(const xAOD::IParticle* lep_0, const xAOD::IParticle* lep_1, const xAOD::Photon* photon,
                                     const xAOD::MissingET* met) {
    TLorentzVector neutrino_p4;
    neutrino_p4.SetPtEtaPhiM(met->met(), lep_0->eta(), met->phi(), 0.0);

    // TODO: is the compiler clever enough to figure this out or is it going to allocate 2 TLorentzVectors?
    TLorentzVector sum_p4 = (lep_0->p4() + neutrino_p4) + lep_1->p4() + photon->p4();
    return sum_p4.M();
}

double KinematicUtils::InvariantMass(std::initializer_list<const xAOD::IParticle*> l) {
    TLorentzVector v;

    // TODO: ought to be re-written without TLorentzVectors for speed
    for (const auto& p : l) {
        v += p->p4();
    }

    return v.M();
}

double KinematicUtils::Alpha1(const xAOD::TauJet* tau, const xAOD::IParticle* p) {
    const double mZ = 91.1876;
    const double mTau = 1.77682;
    const double alpha1 = (mZ * mZ - mTau * mTau) / (2 * (tau->p4() * p->p4()));
    return 1000 * 1000 * alpha1;
}

double KinematicUtils::Alpha2(const xAOD::TauJet* tau, const xAOD::IParticle* p) {
    const double alpha2 = p->pt() / tau->pt();
    return alpha2;
}

double KinematicUtils::DeltaAlpha(const xAOD::TauJet* tau, const xAOD::IParticle* p) {
    if (!tau or !p) {
        Warning("KinematicUtils::DeltaAlpha", "tau or particle is null");
        return -999;
    }

    return Alpha2(tau, p) - Alpha1(tau, p);
}

double KinematicUtils::Alpha1(const xAOD::IParticle* lep1, const xAOD::IParticle* lep2) {
    const double mZ = 91.1876;
    const double mTau = 1.77682;
    const double alpha1 = (mZ * mZ - mTau * mTau) / (2 * (lep1->p4() * lep2->p4()));
    return 1000 * 1000 * alpha1;
}

double KinematicUtils::Alpha2(const xAOD::IParticle* lep1, const xAOD::IParticle* lep2) {
  double alpha_aux = -999.;
    if (lep2->pt() > lep1->pt()) {
      alpha_aux = lep2->pt() / lep1->pt(); }
    else {
      alpha_aux = lep1->pt() / lep2->pt(); }
    const double alpha2 = alpha_aux;
    return alpha2;
}

double KinematicUtils::DeltaAlpha(const xAOD::IParticle* lep1, const xAOD::IParticle* lep2) {
    if (!lep1 or !lep2) {
        Warning("KinematicUtils::DeltaAlpha", "lepton1 or lepton2 is null");
        return -999;
    }

    return Alpha2(lep1, lep2) - Alpha1(lep1, lep2);
}

TLorentzVector KinematicUtils::TruthVisibleP4(const xAOD::TruthParticle* p) {
    TLorentzVector p4 = p->p4();
    auto radiated = RadiatedTruthParticle(p);
    for (int i=0; i<radiated->nChildren(); i++) {
        if (radiated->child(i)->isNeutrino()) {
            p4 -= radiated->child(i)->p4();
        }
    }
    return p4;
}

const xAOD::TruthParticle* KinematicUtils::RadiatedTruthParticle(const xAOD::TruthParticle* p) {
    for (int i=0; i<p->nChildren(); i++) {
        if (p->child(i)->pdgId() == p->pdgId()) {
            return RadiatedTruthParticle(p->child(i));
        }
    }
    return p;
}
