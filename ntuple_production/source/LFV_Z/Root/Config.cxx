#include <LFV_Z/Config.h>
#include <LFV_Z/Utils.h>
#include <string>
#include <vector>

// Boost 1.58 doesn't have this, so copy it in ourselves
//#include <LFV_Z/boost/property_tree/json_parser.hpp>

#include <LFV_Z/ini_parser.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>

std::mutex Config::_mutex;

// can't lexical_cast to a vector
template <>
std::vector<std::string> Config::get<std::vector<std::string>>(const std::string& name) {
    return Utils::splitString(m_settings[name], " ");
}

void Config::readConfigFile(std::string filename) {
    std::cout << "Config::readConfigFile(): reading from " << filename << std::endl;

    if(!boost::filesystem::exists(filename)) {
        if(filename.find("slc6") != std::string::npos) {
            std::cout << "Config::readConfigFile(): " << filename << " not found!" << std::endl;
            filename.replace(filename.find("slc6"), 4, "centos7");
            std::cout << "Config::readConfigFile(): trying " << filename << " instead" << std::endl;
        }
        else if(filename.find("centos7") != std::string::npos) {
            std::cout << "Config::readConfigFile(): " << filename << " not found!" << std::endl;
            filename.replace(filename.find("centos7"), 7, "slc6");
            std::cout << "Config::readConfigFile(): trying " << filename << " instead" << std::endl;
        }
    }

    assert(boost::filesystem::exists(filename));

    boost::property_tree::ptree pt;
    boost::property_tree::ini_parser::read_ini(filename, pt);

    for (auto& section : pt) {
        // std::cout << '[' << section.first << "]\n";
        // Only read global settings
        if (section.first != "Global" && section.first != "EventSelection") {
            continue;
        }
        for (auto& key : section.second) {
            auto val = key.second.get_value<std::string>();
            std::cout << key.first << "=" << val << std::endl;
            if (val == "True") {
                this->set(key.first, true);
            } else if (val == "False") {
                this->set(key.first, false);
            } else {
                this->set(key.first, val);
            }
        }
    }
}
