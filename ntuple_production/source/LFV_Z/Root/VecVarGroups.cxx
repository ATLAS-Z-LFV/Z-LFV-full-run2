#include "LFV_Z/VecVarGroups.h"
#include "LFV_Z/Utils.h"

#include <string>

#include "IsolationSelection/IsolationSelectionTool.h"

extern double energyMetric();

//-------------------------------------------------------------------
void FillLeptonsBasics::add(const xAOD::IParticle* p) {
    // cout << "Filling leptons vecvargroups " << endl;

    // fill particle type
    if (p->type() == xAOD::Type::Muon) {
        particles().emplace_back(1);
    } else if (p->type() == xAOD::Type::Electron) {
        particles().emplace_back(2);
    } else {
        particles().emplace_back(0);
    }

    // fill kinematics
    q().emplace_back(ParticleUtils::GetCharge(p));
    e().emplace_back(p->e() * energyMetric());
    pt().emplace_back(p->pt() * energyMetric());
    eta().emplace_back(p->eta());
    phi().emplace_back(p->phi());

    static SG::AuxElement::ConstAccessor<int> truthOriginAcc("truthOrigin");
    static SG::AuxElement::ConstAccessor<int> truthTypeAcc("truthType");

    if (truthOriginAcc.isAvailable(*p)) {
        origin().emplace_back(truthOriginAcc(*p));
        // std::cout<<" -- particle origin: "<<origin;
    }
    if (truthTypeAcc.isAvailable(*p)) {
        type().emplace_back(truthTypeAcc(*p));
        // std::cout<<" -- particle type: "<<type;
    }

    trk_d0().emplace_back(p->auxdata<float>("d0"));
    trk_delta_z0().emplace_back(p->auxdata<float>("delta_z0"));
    trk_d0_sig().emplace_back(p->auxdata<float>("d0sig"));
    trk_z0_sintheta().emplace_back(p->auxdata<float>("z0sinTheta_no_pvtx"));

    // NB: this is what comes from SUSYTools - and yes it is wrt the prim vtx
    trk_pvx_z0_sintheta().emplace_back(p->auxdata<float>("z0sinTheta"));
    trk_a0PV().emplace_back(p->auxdata<float>("a0PV"));

    if (p->type() == xAOD::Type::Muon) {
        id_veryloose().emplace_back(p->auxdata<int>("quality_veryloose"));
        id_loose().emplace_back(p->auxdata<int>("quality_loose"));
        id_medium().emplace_back(p->auxdata<int>("quality_medium"));
        id_tight().emplace_back(p->auxdata<int>("quality_tight"));
        id_bad().emplace_back(p->auxdata<int>("quality_bad"));
    } else if (p->type() == xAOD::Type::Electron) {
        // We don't need to recalculate electron likelihood anymore, use the default flags
        // (Actually we *can't* always recalculate in new derivations due to trimmed variables)
        id_loose().emplace_back(p->auxdata<char>("DFCommonElectronsLHLoose"));
        id_medium().emplace_back(p->auxdata<char>("DFCommonElectronsLHMedium"));
        id_tight().emplace_back(p->auxdata<char>("DFCommonElectronsLHTight"));
        //id_loose().emplace_back(p->auxdata<int>("llh_loose"));
        //id_medium().emplace_back(p->auxdata<int>("llh_medium"));
        //id_tight().emplace_back(p->auxdata<int>("llh_tight"));
    } else {
        id_loose().emplace_back(-1);
        id_medium().emplace_back(-1);
        id_tight().emplace_back(-1);
    }

    auto _signal = p->isAvailable<char>("signal") ? static_cast<int>(p->auxdata<char>("signal")) : 0;
    auto _QCD = p->isAvailable<char>("QCD") ? static_cast<int>(p->auxdata<char>("QCD")) : 0;
    signal().emplace_back(_signal);
    nonIsolatedSignal().emplace_back(_QCD);

    iso_wp().emplace_back(p->auxdata<char>("iso_wp"));
}
//-------------------------------------------------------------------
void FillLeptonsBasics::add(const xAOD::MuonContainer* p) {
    for (auto muon : *p) {
        add(muon);
    }
}
//-------------------------------------------------------------------
void FillLeptonsBasics::add(const xAOD::ElectronContainer* p) {
    for (auto el : *p) {
        add(el);
    }
}
//-------------------------------------------------------------------
void FillMuonsBasics::add(const xAOD::MuonContainer* p) {
    for (auto muon : *p) {
        FillLeptonsBasics::add(muon);

        // scatteringCurvatureSignificance().emplace_back(muon->auxdata<float>("scatteringCurvatureSignificance"));
    }
}
//-------------------------------------------------------------------
void FillVectorBranches::initialize(std::string group) {
    // loop over flat branches
    for (size_t i = 0; i < tmpVList.size(); ++i) {
        VarBase* tmpV = tmpVList[i];
        std::string tmpGroup = group;
        if (tmpV->name() == group) tmpGroup = "";
        // create a vector copy of the original flat branch
        // at the moment, only types B, C, S, I, US, UI, F and D are supported
        if (dynamic_cast<VarF*>(tmpV)) {
            VVarF* vecVar = new VVarF(vecVList, tmpV->name(), tmpGroup);
            if (tmpV->isTrigger()) vecVar->setTrigger();
            if (tmpV->isScaleFactor()) vecVar->setScaleFactor();
            VVarFList.push_back({dynamic_cast<VarF*>(tmpV), vecVar});
            // std::cout << "VVarFList: pushing pair " << tmpV->name() << ", " << vecVar->name() << std::endl;
        } else if (dynamic_cast<VarD*>(tmpV)) {
            VVarD* vecVar = new VVarD(vecVList, tmpV->name(), tmpGroup);
            if (tmpV->isTrigger()) vecVar->setTrigger();
            if (tmpV->isScaleFactor()) vecVar->setScaleFactor();
            VVarDList.push_back({dynamic_cast<VarD*>(tmpV), vecVar});
            // std::cout << "VVarDList: pushing pair " << tmpV->name() << ", " << vecVar->name() << std::endl;
        } else if (dynamic_cast<VarI*>(tmpV)) {
            VVarI* vecVar = new VVarI(vecVList, tmpV->name(), tmpGroup);
            if (tmpV->isTrigger()) vecVar->setTrigger();
            if (tmpV->isScaleFactor()) vecVar->setScaleFactor();
            VVarIList.push_back({dynamic_cast<VarI*>(tmpV), vecVar});
            // std::cout << "VVarIList: pushing pair " << tmpV->name() << ", " << vecVar->name() << std::endl;
        } else if (dynamic_cast<VarUI*>(tmpV)) {
            VVarUI* vecVar = new VVarUI(vecVList, tmpV->name(), tmpGroup);
            if (tmpV->isTrigger()) vecVar->setTrigger();
            if (tmpV->isScaleFactor()) vecVar->setScaleFactor();
            VVarUIList.push_back({dynamic_cast<VarUI*>(tmpV), vecVar});
            // std::cout << "VVarUIList: pushing pair " << tmpV->name() << ", " << vecVar->name() << std::endl;
        } else if (dynamic_cast<VarB*>(tmpV)) {
            VVarB* vecVar = new VVarB(vecVList, tmpV->name(), tmpGroup);
            if (tmpV->isTrigger()) vecVar->setTrigger();
            if (tmpV->isScaleFactor()) vecVar->setScaleFactor();
            VVarBList.push_back({dynamic_cast<VarB*>(tmpV), vecVar});
            // std::cout << "VVarUIList: pushing pair " << tmpV->name() << ", " << vecVar->name() << std::endl;
        } else if (dynamic_cast<VarC*>(tmpV)) {
            VVarC* vecVar = new VVarC(vecVList, tmpV->name(), tmpGroup);
            if (tmpV->isTrigger()) vecVar->setTrigger();
            if (tmpV->isScaleFactor()) vecVar->setScaleFactor();
            VVarCList.push_back({dynamic_cast<VarC*>(tmpV), vecVar});
            // std::cout << "VVarUIList: pushing pair " << tmpV->name() << ", " << vecVar->name() << std::endl;
        } else if (dynamic_cast<VarS*>(tmpV)) {
            VVarS* vecVar = new VVarS(vecVList, tmpV->name(), tmpGroup);
            if (tmpV->isTrigger()) vecVar->setTrigger();
            if (tmpV->isScaleFactor()) vecVar->setScaleFactor();
            VVarSList.push_back({dynamic_cast<VarS*>(tmpV), vecVar});
            // std::cout << "VVarUIList: pushing pair " << tmpV->name() << ", " << vecVar->name() << std::endl;
        } else if (dynamic_cast<VarUS*>(tmpV)) {
            VVarUS* vecVar = new VVarUS(vecVList, tmpV->name(), tmpGroup);
            if (tmpV->isTrigger()) vecVar->setTrigger();
            if (tmpV->isScaleFactor()) vecVar->setScaleFactor();
            VVarUSList.push_back({dynamic_cast<VarUS*>(tmpV), vecVar});
            // std::cout << "vecBList: pushing pair " << tmpV->name() << ", " << vecVar->name() << std::endl;
        } else {
            Warning("FillVectorBranches::initialize", "Trying to duplicate variable %s of unknown type. Will be omitted...",
                    tmpV->name().c_str());
        }
    }
}
//-------------------------------------------------------------------
void FillVectorBranches::copyValues() {
    // copy the information into the vector branches
    for (auto& var : VVarFList) (*var.second)().emplace_back((*var.first)());
    for (auto& var : VVarDList) (*var.second)().emplace_back((*var.first)());
    for (auto& var : VVarBList) (*var.second)().emplace_back((*var.first)());
    for (auto& var : VVarCList) (*var.second)().emplace_back((*var.first)());
    for (auto& var : VVarSList) (*var.second)().emplace_back((*var.first)());
    for (auto& var : VVarIList) (*var.second)().emplace_back((*var.first)());
    for (auto& var : VVarUSList) (*var.second)().emplace_back((*var.first)());
    for (auto& var : VVarUIList) (*var.second)().emplace_back((*var.first)());
}
//-------------------------------------------------------------------
void FillLeptons::initialize() {
    // initialize vector branches
    FillVectorBranches::initialize(group);
}
//-------------------------------------------------------------------
void FillLeptons::add(const xAOD::IParticle* p) {
    vlist.clear();  // set all the temporary flat variables to the default value
    fill(p);        // fill the flat variables
    copyValues();   // copy the information into the vector branches
}

void FillLeptons::add(const xAOD::MuonContainer* p) {
    for (auto muon : *p) {
        add(muon);
    }
}

void FillLeptons::add(const xAOD::ElectronContainer* p) {
    for (auto el : *p) {
        add(el);
    }
}
//-------------------------------------------------------------------
void FillJets::initialize() {
    // initialize vector branches
    FillVectorBranches::initialize(group);
}

void FillJets::add(const xAOD::Jet* p) {
    vlist.clear();  // set all the temporary flat variables to the default value
    fill(p);        // fill the flat variables
    copyValues();   // copy the information into the vector branches
}

void FillJets::add(const xAOD::JetContainer* p) {
    for (auto jet : *p) {
        add(jet);
    }
}
//-------------------------------------------------------------------
void FillPhotons::initialize() {
    // initialize vector branches
    FillVectorBranches::initialize(group);
}

void FillPhotons::add(const xAOD::Photon* p) {
    vlist.clear();  // set all the temporary flat variables to the default value
    fill(p);        // fill the flat variables
    copyValues();   // copy the information into the vector branches
}

void FillPhotons::add(const xAOD::PhotonContainer* c) {
    for (auto ph : *c) {
        add(ph);
    }
}
//-------------------------------------------------------------------
void FillBasicTaus::initialize() {
    // initialize vector branches
    FillVectorBranches::initialize(group);
}

void FillBasicTaus::add(const xAOD::TauJet* p) {
    vlist.clear();  // set all the temporary flat variables to the default value
    fill(p);        // fill the flat variables
    copyValues();   // copy the information into the vector branches
}

void FillBasicTaus::add(const xAOD::TauJetContainer* c) {
    for (auto tau : *c) {
        add(tau);
    }
}
//-------------------------------------------------------------------
void FillTaus::initialize() {
    // initialize vector branches
    FillVectorBranches::initialize(group);
}

void FillTaus::add(const xAOD::TauJet* p) {
    vlist.clear();  // set all the temporary flat variables to the default value
    fill(p);        // fill the flat variables
    copyValues();   // copy the information into the vector branches
}

void FillTaus::add(const xAOD::TauJetContainer* c) {
    for (auto tau : *c) {
        add(tau);
    }
}
//-------------------------------------------------------------------
void FillLeptons_MatchedParticle::initialize() {
    // initialize vector branches
    FillVectorBranches::initialize(group);
}

void FillLeptons_MatchedParticle::add(const xAOD::IParticle* p, const xAOD::IParticleContainer* pc) {
    vlist.clear();  // set all the temporary flat variables to the default value
    fill(p, pc);    // fill the flat variables
    copyValues();   // copy the information into the vector branches
}

void FillLeptons_MatchedParticle::add(const xAOD::MuonContainer* p, const xAOD::IParticleContainer* pc) {
    for (auto muon : *p) {
        add(muon, pc);
    }
}

void FillLeptons_MatchedParticle::add(const xAOD::ElectronContainer* p, const xAOD::IParticleContainer* pc) {
    for (auto el : *p) {
        add(el, pc);
    }
}
//-------------------------------------------------------------------
void FillLeptons_MatchedTruthLeptonicTauDecay::initialize() {
    // initialize vector branches
    FillVectorBranches::initialize(group);
}

void FillLeptons_MatchedTruthLeptonicTauDecay::add(const xAOD::IParticle* p, const xAOD::TruthParticleContainer* truth_taus) {
    vlist.clear();        // set all the temporary flat variables to the default value
    fill(p, truth_taus);  // fill the flat variables
    copyValues();         // copy the information into the vector branches
}

void FillLeptons_MatchedTruthLeptonicTauDecay::add(const xAOD::MuonContainer* p, const xAOD::TruthParticleContainer* truth_taus) {
    for (auto muon : *p) {
        add(muon, truth_taus);
    }
}

void FillLeptons_MatchedTruthLeptonicTauDecay::add(const xAOD::ElectronContainer* p,
                                                   const xAOD::TruthParticleContainer* truth_taus) {
    for (auto el : *p) {
        add(el, truth_taus);
    }
}
