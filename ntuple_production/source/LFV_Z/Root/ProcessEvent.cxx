// vim: ts=4 sw=4
#include <LFV_Z/EventSelection.h>
#include <LFV_Z/Utils.h>
#include "AthContainers/AuxElement.h"
#include "xAODRootAccess/TActiveStore.h"

// static SG::AuxElement::Accessor<char> is_baseline("baseline");
// static SG::AuxElement::Accessor<char> is_signal("signal");
// static SG::AuxElement::Accessor<char> is_isol("isol");
// static SG::AuxElement::Accessor<char> is_passOR("passOR");
// static SG::AuxElement::Accessor<char> is_passBaseID("passBaseID");

// static SG::AuxElement::Accessor<double> acc_effscalefact("effscalefact");
// static SG::AuxElement::Accessor<double> acc_jvtscalefact("jvtscalefact");
//// static SG::AuxElement::Accessor<double> acc_trigscalefact("trigscalefact");

// static SG::AuxElement::Decorator<char> dec_passOR("passOR");

#include <LFV_Z/Histograms.h>
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "xAODBase/IParticleHelpers.h"

void EventSelection::getJets(bool storeVariation) {
    m_jets = 0;
    m_jets_aux = 0;

    const std::string containerName = "SUSYJets" + m_containerSuffix + m_systematicSuffix;
    const std::string containerNameAux = containerName + "_Aux.";

    // (AthDerivation 21.2.72.0+ / p3954+)
    // For b-tagging to work, time-stamped shallow-copied jet container has to be used instead
    // However, this messes up with MET association later on
    // So we have to manually re-link it back to the original jet container
    const xAOD::JetContainer* jets(0);
    if (!m_event->retrieve(jets, m_jetContainerKey+"_"+m_bTaggingTimeStamp).isSuccess()) {
        throw std::runtime_error("EventSelection::getJets: Could not retrieve " + m_jetContainerKey+"_"+m_bTaggingTimeStamp);
    }
    const xAOD::JetContainer* original_jets(0);
    if (!m_event->retrieve(original_jets, m_jetContainerKey).isSuccess()) {
        throw std::runtime_error("EventSelection::getJets: Could not retrieve " + m_jetContainerKey);
    }
    std::pair<xAOD::JetContainer*, xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*jets);
    m_jets = shallowcopy.first;
    m_jets_aux = shallowcopy.second;
    bool setLinks = xAOD::setOriginalObjectLink(*original_jets, *m_jets);

    if (!m_SUSYTools->GetJets(m_jets, m_jets_aux, false).isSuccess()) {
        throw std::runtime_error("Could not retrieve Jets");
    }
    if (storeVariation) {
        if (!m_store->record(m_jets, containerName).isSuccess()) {
            throw std::runtime_error("Could not store " + containerName);
        }
        if (!m_store->record(m_jets_aux, containerNameAux).isSuccess()) {
            throw std::runtime_error("Could not store " + containerNameAux);
        }
    }

    ATH_MSG_DEBUG("'Jets" + m_containerSuffix + m_systematicSuffix + "' jets: " << m_jets->size());
    for (const auto& jet : *m_jets) {
        ATH_MSG_DEBUG(" jet " << jet->pt() << " " << jet->eta() << " " << jet->phi() << " baseline "
                              << static_cast<int>(is_baseline(*jet)) << " signal " << static_cast<int>(is_signal(*jet))
                              << " bjet " << static_cast<int>(is_bjet(*jet)) << " bjet_loose "
                              << static_cast<int>(is_bjet_loose(*jet)));
        //<< " prio " << static_cast<int>(prioAcc(*jet)));
    }
}

void EventSelection::getTaus(bool storeVariation) {
    if (!m_getTaus) {
        // return the dummy
        ATH_MSG_DEBUG("Returning dummy tau container");

        //// the original aux container has to be in the store
        //m_store->record(m_empty_taus_base_aux, "TauJetsAux.").ignore();
        //
        //auto shallowtaus = xAOD::shallowCopyContainer(*m_empty_taus_base);
        //m_taus = shallowtaus.first;
        //m_taus_aux = shallowtaus.second;
        //
        //if (!xAOD::setOriginalObjectLink(*m_empty_taus_base, *m_taus)) {
        //    ATH_MSG_WARNING("Failed to set original object links on dummy tau container");
        //}
        
        // xAOD::shallowCopyContainer crashed (AnalysisBase 21.2.38)
        // we will do similar things ourselves (in the simplest way)
        // Do we actually have to link the shallowed container to anything? The container should be of size 0 anyway...?
        m_empty_taus_base = new xAOD::TauJetContainer();
        m_empty_taus_shallow_aux = new xAOD::ShallowAuxContainer();
        m_empty_taus_base->setStore( m_empty_taus_shallow_aux );
        m_taus = m_empty_taus_base;
        m_taus_aux = m_empty_taus_shallow_aux;

        if (storeVariation) {
            const std::string containerName = "SUSYTaus" + m_containerSuffix + m_systematicSuffix;
            const std::string containerNameAux = containerName + "_Aux.";
            if (!m_store->record(m_taus, containerName).isSuccess()) {
                throw std::runtime_error("Could not store " + containerName);
            }
            if (!m_store->record(m_taus_aux, containerNameAux).isSuccess()) {
                throw std::runtime_error("Could not store " + containerNameAux);
            }
        }
    
        return;
    }

    m_taus = 0;
    m_taus_aux = 0;

    // This is needed to ensure we have a truth matched bunch of taus in a full AOD - otherwise TauSmearingTool will crash
    // TODO: can we figure out ourselves whether we're on a full AOD?
    const std::string taukey = "TauJets";
    const xAOD::TauJetContainer* taus(0);
    m_event->retrieve(taus, taukey).ignore();
    std::pair<xAOD::TauJetContainer*, xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*taus);

    m_taus = shallowcopy.first;
    m_taus_aux = shallowcopy.second;

    bool setLinks = xAOD::setOriginalObjectLink(*taus, *m_taus);
    if (!setLinks) {
        ATH_MSG_WARNING("Failed to set original object links on " << taukey);
    }

    if (m_doTauTruthMatching) {
        ATH_MSG_VERBOSE("Starting tau truth matching");
        for (const auto& tau : *m_taus) {
            const auto match = m_tauTruthMatchingTool->getTruth(*tau);
            if (match) {
                ATH_MSG_DEBUG("Found tau truth match with pT=" << match->pt() << " eta=" << match->eta()
                                                               << " pdgId=" << match->pdgId());
            }
        }
    }

    if (!m_SUSYTools->GetTaus(m_taus, m_taus_aux, false).isSuccess()) {
        throw std::runtime_error("Could not retrieve taus");
    }

    const std::string containerName = "SUSYTaus" + m_containerSuffix + m_systematicSuffix;
    const std::string containerNameAux = containerName + "_Aux.";

    if (storeVariation) {
        if (!m_store->record(m_taus, containerName).isSuccess()) {
            throw std::runtime_error("Could not store " + containerName);
        }
        if (!m_store->record(m_taus_aux, containerNameAux).isSuccess()) {
            throw std::runtime_error("Could not store " + containerNameAux);
        }
    }

    float tauSF = 1.0;
    ATH_MSG_DEBUG("'Taus" + m_containerSuffix + m_systematicSuffix + "' taus: " << m_taus->size());
    for (const auto& tau : *m_taus) {
        if (!m_isData && is_signal(*tau)) {
            float sf = m_SUSYTools->GetSignalTauSF(*tau);
            tau->auxdecor<float>("sf") = sf;
            tauSF *= sf;
        } else {
            tau->auxdecor<float>("sf") = 1.0;
        }

        ATH_MSG_DEBUG(" tau " << tau->pt() << " " << tau->eta() << " " << tau->phi() << " baseline "
                              << static_cast<int>(is_baseline(*tau)) << " signal " << static_cast<int>(is_signal(*tau)));
        ATH_MSG_DEBUG("       nTracks " << tau->nTracks());  // << " prio " << static_cast<int>(prioAcc(*tau)));
        ATH_MSG_DEBUG("       BDT: " << tau->isTau(xAOD::TauJetParameters::JetBDTSigLoose) << " "
                                     << tau->isTau(xAOD::TauJetParameters::JetBDTSigMedium) << " "
                                     << tau->isTau(xAOD::TauJetParameters::JetBDTSigTight));
    }
}

void EventSelection::getMuons(bool storeVariation) {
    m_muons = 0;
    m_muons_aux = 0;
    if (!m_SUSYTools->GetMuons(m_muons, m_muons_aux, false).isSuccess()) {
        throw std::runtime_error("Could not retrieve Muons");
    }
    m_SUSYTools->FlagIsoMuons(m_muons);

    const std::string containerName = "SUSYMuons" + m_containerSuffix + m_systematicSuffix;
    const std::string containerNameAux = containerName + "_Aux.";

    if (storeVariation) {
        if (!m_store->record(m_muons, containerName).isSuccess()) {
            throw std::runtime_error("Could not store " + containerName);
        }
        if (!m_store->record(m_muons_aux, containerNameAux).isSuccess()) {
            throw std::runtime_error("Could not store " + containerNameAux);
        }
    }

    // TODO: this ought to be done more centrally; not here
    std::string muQual = "";
    auto m_muId = m_SUSYTools->getProperty<int>("MuonId");
    switch (*m_muId) {
        case (int)xAOD::Muon::Quality(xAOD::Muon::VeryLoose):
            muQual = "VeryLoose";
            ATH_MSG_WARNING("No muon scale factors are available for VeryLoose working point.");
            break;
        case (int)xAOD::Muon::Quality(xAOD::Muon::Loose):
            muQual = "Loose";
            break;
        case (int)xAOD::Muon::Quality(xAOD::Muon::Medium):
            muQual = "Medium";
            break;
        case (int)xAOD::Muon::Quality(xAOD::Muon::Tight):
            muQual = "Tight";
            break;
        case 4:
            muQual = "HighPt";
            break;
        default:
            ATH_MSG_ERROR("Invalid muon working point provided: " << m_muId << ". Cannot initialise!");
            break;
    }
    auto toolName = "MuonSelectionTool_" + muQual;
    // auto muonSelectionTool = asg::ToolStore::get<CP::MuonSelectionTool>(toolName);

    asg::AnaToolHandle<CP::IMuonSelectionTool> muonSelectionTool;
    muonSelectionTool.setTypeAndName("CP::MuonSelectionTool/" + toolName);
    muonSelectionTool.retrieve().ignore();

    ATH_MSG_DEBUG("'Muons" + m_containerSuffix + m_systematicSuffix + "' muons: " << m_muons->size());
    for (const auto& mu : *m_muons) {
        // declare calo and forward muons non baseline so they don't get
        // used in MET
        if (mu->muonType() != xAOD::Muon::Combined && mu->muonType() != xAOD::Muon::MuonStandAlone &&
            mu->muonType() != xAOD::Muon::SegmentTagged) {
            
            if (mu->muonType() == xAOD::Muon::CaloTagged) {
                if (static_cast<int>(is_baseline(*mu)) == 1) {
                    mu->auxdecor<char>("baseline_calo_muon") = 1;
                } else {
                    mu->auxdecor<char>("baseline_calo_muon") = 0;
                }
            }

            mu->auxdecor<char>("baseline") = 0;
        }

        // flag the isolation here, to make sure we can identify non-isolated signal leptons without calling SUSYTools twice
        mu->auxdecor<char>("QCD") = 0;
        if (is_signal(*mu) and not is_isol(*mu)) {
            // => passes all the cuts but isolation, i.e. QCD
            mu->auxdecor<char>("QCD") = 1;
        }

        dec_signal(*mu) &= is_isol(*mu);

        ATH_MSG_DEBUG("Muon " << mu->pt() << " " << mu->eta() << " " << mu->phi() << " bad "
                              << static_cast<int>(mu->auxdata<char>("bad")) << " calo-tagged "
                              << (mu->muonType() == xAOD::Muon::CaloTagged) << " baseline " << static_cast<int>(is_baseline(*mu))
                              << " QCD " << static_cast<int>(mu->auxdata<char>("QCD")) << " signal "
                              << static_cast<int>(is_signal(*mu)));

        // find truth if MC
        if (m_isMC) {
            int matchedIdx(-1);
            matchedIdx = ParticleUtils::FindTrueParticle(m_truthParticleContainer, mu, 13);
            mu->auxdata<int>("mu_matchedIdx") = static_cast<int>(matchedIdx);
        }

        // don't bother decorating stuff we don't save
        // this gives nasty crashes for very soft stuff anyway
        if (not static_cast<bool>(is_baseline(*mu))) continue;

        xAOD::Muon::Quality mu_quality = muonSelectionTool->getQuality(*mu);  // VeryLoose==3, Loose==2, Medium==1, Tight==0
        mu->auxdata<int>("quality_veryloose") = (mu_quality <= xAOD::Muon::VeryLoose);
        mu->auxdata<int>("quality_loose") = (mu_quality <= xAOD::Muon::Loose);
        mu->auxdata<int>("quality_medium") = (mu_quality <= xAOD::Muon::Medium);
        mu->auxdata<int>("quality_tight") = (mu_quality <= xAOD::Muon::Tight);
        mu->auxdata<int>("quality_bad") = (mu_quality <= xAOD::Muon::VeryLoose) && !(mu_quality <= xAOD::Muon::Loose);

        mu->auxdecor<char>("iso_wp") = getIsolationWP(*mu);
    }
}

void EventSelection::getElectrons(bool storeVariation) {
    m_electrons = 0;
    m_electrons_aux = 0;
    if (!m_SUSYTools->GetElectrons(m_electrons, m_electrons_aux, false).isSuccess()) {
        throw std::runtime_error("Could not retrieve Electrons");
    }
    m_SUSYTools->FlagIsoElectrons(m_electrons);

    const std::string containerName = "SUSYElectrons" + m_containerSuffix + m_systematicSuffix;
    const std::string containerNameAux = containerName + "_Aux.";

    if (storeVariation) {
        if (!m_store->record(m_electrons, containerName).isSuccess()) {
            throw std::runtime_error("Could not store " + containerName);
        }
        if (!m_store->record(m_electrons_aux, containerNameAux).isSuccess()) {
            throw std::runtime_error("Could not store " + containerNameAux);
        }
    }

    for (const auto& el : *m_electrons) {
        el->auxdecor<char>("iso_wp") = getIsolationWP(*el);

        // We don't need to recalculate electron likelihood anymore, use the default flags
        // (Actually we *can't* always recalculate in new derivations due to trimmed variables)
        /*
        // Flag the additional LLH cuts - move to a function
        // TODO: is the loop needed?
        for (EleLH_itr itr = EleLH::EL_LH_LOOSE; itr != EleLH_itr(); itr++) {
            std::string flag;
            switch (*itr) {
                case EleLH::EL_LH_LOOSE:
                    flag = "llh_loose";
                    break;
                case EleLH::EL_LH_MEDIUM:
                    flag = "llh_medium";
                    break;
                case EleLH::EL_LH_TIGHT:
                    flag = "llh_tight";
                    break;
                default:
                    ATH_MSG_FATAL("AsgElectronLikelihoodTool: invalid enum value %i" << static_cast<int>(*itr));
                    break;
            }

            if (m_AsgElectronLikelihoodTool_map[*itr]->accept(el)) {
                el->auxdata<int>(flag) = 1;
            } else {
                el->auxdata<int>(flag) = 0;
            }
        }
        */
    }

    ATH_MSG_DEBUG("'Electrons" + m_containerSuffix + m_systematicSuffix + "' electrons: " << m_electrons->size());
    for (const auto& el : *m_electrons) {
        // check for the crack region
        if (ParticleUtils::InCrackRegion(el)) {
            ATH_MSG_DEBUG(" Electron: flagging as in crack region");
            el->auxdata<char>("signal") = 0;
        }

        // flag the isolation here, to make sure we can identify non-isolated signal leptons without calling SUSYTools twice
        el->auxdecor<char>("QCD") = 0;
        if (is_signal(*el) and not is_isol(*el)) {
            // => passes all the cuts but isolation, i.e. QCD
            el->auxdecor<char>("QCD") = 1;
        }

        dec_signal(*el) &= is_isol(*el);

        // find truth if MC
        if (m_isMC) {
            int matchedIdx(-1);
            matchedIdx = ParticleUtils::FindTrueParticle(m_truthParticleContainer, el, 11);
            el->auxdata<int>("el_matchedIdx") = static_cast<int>(matchedIdx);
        }

        ATH_MSG_DEBUG(" Electron " << el->pt() << " " << el->eta() << " " << el->phi() << " baseline "
                                   << static_cast<int>(is_baseline(*el)) << " signal " << static_cast<int>(is_signal(*el))
                                   << " QCD " << static_cast<int>(el->auxdata<char>("QCD")) << " passBaseID "
                                   << static_cast<int>(is_passBaseID(*el)));
        ATH_MSG_DEBUG("            (cluster eta: " << el->caloCluster()->eta() << "; etaBE(2): " << el->caloCluster()->etaBE(2)
                                                   << ")");
    }
}

void EventSelection::getPhotons(bool storeVariation) {
    if (!m_getPhotons) {
        // return the dummy
        ATH_MSG_DEBUG("Returning dummy photon container");

        //// the original aux container has to be in the store
        //m_store->record(m_empty_photons_base_aux, "PhotonsAux.").ignore();
        //
        //auto shallowphotons = xAOD::shallowCopyContainer(*m_empty_photons_base);
        //m_photons = shallowphotons.first;
        //m_photons_aux = shallowphotons.second;
        //
        //if (!xAOD::setOriginalObjectLink(*m_empty_photons_base, *m_photons)) {
        //    ATH_MSG_WARNING("Failed to set original object links on dummy photon container");
        //}

        // xAOD::shallowCopyContainer crashed (AnalysisBase 21.2.38)
        // we will do similar things ourselves (in the simplest way)
        // Do we actually have to link the shallowed container to anything? The container should be of size 0 anyway...?
        m_empty_photons_base = new xAOD::PhotonContainer();
        m_empty_photons_shallow_aux = new xAOD::ShallowAuxContainer();
        m_empty_photons_base->setStore( m_empty_photons_shallow_aux );
        m_photons = m_empty_photons_base;
        m_photons_aux = m_empty_photons_shallow_aux;
        
        if (storeVariation) {
            const std::string containerName = "SUSYPhotons" + m_containerSuffix + m_systematicSuffix;
            const std::string containerNameAux = containerName + "_Aux.";
            if (!m_store->record(m_photons, containerName).isSuccess()) {
                throw std::runtime_error("Could not store " + containerName);
            }
            if (!m_store->record(m_photons_aux, containerNameAux).isSuccess()) {
                throw std::runtime_error("Could not store " + containerNameAux);
            }
        }

        return;
    }

    m_photons = 0;
    m_photons_aux = 0;
    if (!m_SUSYTools->GetPhotons(m_photons, m_photons_aux, false).isSuccess()) {
        throw std::runtime_error("Could not retrieve Photons");
    }

    const std::string containerName = "SUSYPhotons" + m_containerSuffix + m_systematicSuffix;
    const std::string containerNameAux = containerName + "_Aux.";

    if (storeVariation) {
        if (!m_store->record(m_photons, containerName).isSuccess()) {
            throw std::runtime_error("Could not store " + containerName);
        }
        if (!m_store->record(m_photons_aux, containerNameAux).isSuccess()) {
            throw std::runtime_error("Could not store " + containerNameAux);
        }
    }

    ATH_MSG_DEBUG("'Photons" + m_containerSuffix + m_systematicSuffix + "' photons: " << m_photons->size());
    float phSF = 1;
    for (const auto& ph : *m_photons) {
        if (!m_isData && is_signal(*ph)) {
            float sf = m_SUSYTools->GetSignalPhotonSF(*ph);
            ph->auxdecor<float>("sf") = sf;
            phSF *= sf;
        } else {
            ph->auxdecor<float>("sf") = 1.0;
        }

        ATH_MSG_DEBUG(" Photon " << ph->pt() << " " << ph->eta() << " " << ph->phi() << " baseline "
                                 << static_cast<int>(is_baseline(*ph)) << " signal " << static_cast<int>(is_signal(*ph)));
    }

    m_eventInfo->auxdecor<float>("phSF") = phSF;
}

void EventSelection::removeOverlaps() {
    // Overlap removal
    ATH_MSG_DEBUG("Performing overlap removal");

    ATH_MSG_VERBOSE("Before OLR");
    ATH_MSG_VERBOSE("nBaselineMuons: " << ContainerUtils::Count<char>(m_muons, "baseline") << " "
                                       << ContainerUtils::Count<char>(m_muons, "passOR"));
    ATH_MSG_VERBOSE("nBaselineTaus: " << ContainerUtils::Count<char>(m_taus, "baseline") << " "
                                      << ContainerUtils::Count<char>(m_taus, "passOR"));
    ATH_MSG_VERBOSE("nBaselineElectrons: " << ContainerUtils::Count<char>(m_electrons, "baseline") << " "
                                           << ContainerUtils::Count<char>(m_electrons, "passOR"));
    ATH_MSG_VERBOSE("nBaselinePhotons: " << ContainerUtils::Count<char>(m_photons, "baseline") << " "
                                         << ContainerUtils::Count<char>(m_photons, "passOR"));
    ATH_MSG_VERBOSE("nSignalMuons: " << ContainerUtils::Count<char>(m_muons, "signal") << " "
                                     << ContainerUtils::Count<char>(m_muons, "passOR"));
    ATH_MSG_VERBOSE("nSignalTaus: " << ContainerUtils::Count<char>(m_taus, "signal") << " "
                                    << ContainerUtils::Count<char>(m_taus, "passOR"));
    ATH_MSG_VERBOSE("nSignalElectrons: " << ContainerUtils::Count<char>(m_electrons, "signal") << " "
                                         << ContainerUtils::Count<char>(m_electrons, "passOR"));
    ATH_MSG_VERBOSE("nSignalPhotons: " << ContainerUtils::Count<char>(m_photons, "signal") << " "
                                       << ContainerUtils::Count<char>(m_photons, "passOR"));

    if (!m_SUSYTools->OverlapRemoval(m_electrons, m_muons, m_jets, m_photons, m_taus).isSuccess()) {
        throw std::runtime_error("Error in OverlapRemoval");
    }
    
    // Reset the passOR decorations for photons first
    for (auto ph: *m_photons) {
        dec_passOR(*ph) = true;
    }
    // Do custom ph-el and ph-mu overlap removals with DR < 0.2
    if (m_DeltaROverlapTool->findOverlaps(*m_photons, *m_electrons).isFailure() ||
        m_DeltaROverlapTool->findOverlaps(*m_photons, *m_muons).isFailure()) {
        throw std::runtime_error("Error in custom photon OverlapRemoval");
    }

    ATH_MSG_VERBOSE("After OLR");
    ATH_MSG_VERBOSE("nBaselineMuons: " << ContainerUtils::Count<char>(m_muons, std::vector<std::string>({"baseline", "passOR"})));
    ATH_MSG_VERBOSE("nBaselineTaus: " << ContainerUtils::Count<char>(m_taus, std::vector<std::string>({"baseline", "passOR"})));
    ATH_MSG_VERBOSE(
        "nBaselineElectrons: " << ContainerUtils::Count<char>(m_electrons, std::vector<std::string>({"baseline", "passOR"})));
    ATH_MSG_VERBOSE(
        "nBaselinePhotons: " << ContainerUtils::Count<char>(m_photons, std::vector<std::string>({"baseline", "passOR"})));
    ATH_MSG_VERBOSE("nSignalMuons: " << ContainerUtils::Count<char>(m_muons, std::vector<std::string>({"signal", "passOR"})));
    ATH_MSG_VERBOSE("nSignalTaus: " << ContainerUtils::Count<char>(m_taus, std::vector<std::string>({"signal", "passOR"})));
    ATH_MSG_VERBOSE(
        "nSignalElectrons: " << ContainerUtils::Count<char>(m_electrons, std::vector<std::string>({"signal", "passOR"})));
    ATH_MSG_VERBOSE("nSignalPhotons: " << ContainerUtils::Count<char>(m_photons, std::vector<std::string>({"signal", "passOR"})));

    // Check for taus overlapping with calo muons. Do not check for baseline ID - often, calo tagged muons do not pass. Also, do it here a not in your selection class: those only know about baseline objects.
    for(const auto &tau: *m_taus) { 
        tau->auxdecor<char>("overlaps_with_calo_muon") = false;
        
        // Do we overlap with a calo-tagged baseline muon?
        for (const auto &muon_for_orl : *m_muons) {
            if (muon_for_orl->muonType() != xAOD::Muon::CaloTagged) continue;  // only care about calo-tagged muons

            // std::cout << "baseline muon " << static_cast<int>(is_baseline(*muon_for_orl)) << std::endl;
            // std::cout << "baseline calo muon " << static_cast<int>(muon_for_orl->auxdata<char>("baseline_calo_muon")) << std::endl;

            const auto dR = tau->p4().DeltaR(muon_for_orl->p4());
            // std::cout << "dR = " << dR << std::endl;
            if (dR < 0.2) {
                tau->auxdecor<char>("overlaps_with_calo_muon") = true;
                break;
            }
        }
    }
}

void EventSelection::setViewContainers() {
    Histograms& hists = Histograms::getInstance();

    // Now set the view containers
    if (m_jets) {
        for (const auto& jet : *m_jets) {
            // TODO: see xTools::Goodness for this
            // TODO: make configurable in config??
            if (is_baseline(*jet) == 1 && is_passOR(*jet) == 1) {
                m_viewElemJetContainer->emplace_back(jet);

                auto h_pt = hists.monHist_jet_pt[hists.currentSystematic()];
                if (h_pt) h_pt->Fill(jet->pt() / 1000.0);

                auto h_pt_eta = hists.monHist_jet_pt_eta[hists.currentSystematic()];
                if (h_pt_eta) h_pt_eta->Fill(jet->pt() / 1000.0, jet->eta());

                auto h_eta_phi = hists.monHist_jet_eta_phi[hists.currentSystematic()];
                if (h_eta_phi) h_eta_phi->Fill(jet->phi(), jet->eta());
            }

            if (is_baseline(*jet) == 1) {
                auto h_pt = hists.monHist_jet_no_OLR_pt[hists.currentSystematic()];
                if (h_pt) h_pt->Fill(jet->pt() / 1000.0);
            }
        }
    }

    if (m_taus) {
        for (const auto& tau : *m_taus) {
            if (is_baseline(*tau) == 1 && is_passOR(*tau) == 1) {
                m_viewElemTauContainer->emplace_back(tau);
                auto h_pt = hists.monHist_tau_pt[hists.currentSystematic()];
                if (h_pt) h_pt->Fill(tau->pt() / 1000.0);

                auto h_pt_eta = hists.monHist_tau_pt_eta[hists.currentSystematic()];
                if (h_pt_eta) h_pt_eta->Fill(tau->pt() / 1000.0, tau->eta());

                auto h_eta_phi = hists.monHist_tau_eta_phi[hists.currentSystematic()];
                if (h_eta_phi) h_eta_phi->Fill(tau->eta(), tau->phi());
            }

            if (is_baseline(*tau) == 1) {
                auto h_pt = hists.monHist_tau_no_OLR_pt[hists.currentSystematic()];
                if (h_pt) h_pt->Fill(tau->pt() / 1000.0);
            }
        }
    }

    if (m_photons) {
        for (const auto& ph : *m_photons) {
            if (is_baseline(*ph) == 1 && (!m_photonInOR || is_passOR(*ph) == 1)) {
                m_viewElemPhotonContainer->emplace_back(ph);
            }
        }
    }

    if (m_electrons) {
        for (const auto& el : *m_electrons) {
            if (is_baseline(*el) == 1 && is_passOR(*el) == 1) {
                m_viewElemElectronContainer->emplace_back(el);
                auto h_pt = hists.monHist_ele_pt[hists.currentSystematic()];
                if (h_pt) h_pt->Fill(el->pt() / 1000.0);

                auto h_pt_eta = hists.monHist_ele_pt_eta[hists.currentSystematic()];
                if (h_pt_eta) h_pt_eta->Fill(el->pt() / 1000.0, el->eta());

                auto h_eta_phi = hists.monHist_ele_eta_phi[hists.currentSystematic()];
                if (h_eta_phi) h_eta_phi->Fill(el->eta(), el->phi());
            }

            if (is_baseline(*el) == 1) {
                auto h_pt = hists.monHist_ele_no_OLR_pt[hists.currentSystematic()];
                if (h_pt) h_pt->Fill(el->pt() / 1000.0);
            }
        }
    }

    if (m_muons) {
        for (const auto& mu : *m_muons) {
            if (is_baseline(*mu) == 1 && is_passOR(*mu) == 1) {
                m_viewElemMuonContainer->emplace_back(mu);
                auto h_pt = hists.monHist_muon_pt[hists.currentSystematic()];
                if (h_pt) h_pt->Fill(mu->pt() / 1000.0);

                auto h_pt_eta = hists.monHist_muon_pt_eta[hists.currentSystematic()];
                if (h_pt_eta) h_pt_eta->Fill(mu->pt() / 1000.0, mu->eta());

                auto h_eta_phi = hists.monHist_muon_eta_phi[hists.currentSystematic()];
                if (h_eta_phi) h_eta_phi->Fill(mu->eta(), mu->phi());
            }

            if (is_baseline(*mu) == 1) {
                auto h_pt = hists.monHist_muon_no_OLR_pt[hists.currentSystematic()];
                if (h_pt) h_pt->Fill(mu->pt() / 1000.0);
            }
        }
    }

    ContainerUtils::PtSort(m_viewElemJetContainer.get());
    ContainerUtils::PtSort(m_viewElemTauContainer.get());
    ContainerUtils::PtSort(m_viewElemPhotonContainer.get());
    ContainerUtils::PtSort(m_viewElemElectronContainer.get());
    ContainerUtils::PtSort(m_viewElemMuonContainer.get());
}

void EventSelection::getScaleFactors(bool isNominal) {
    // TODO: this ought to be configurable. do we need all the SF?

    auto config = Config::getInstance();
    if (!config->has("doScaleFactors") or !config->get<bool>("doScaleFactors")) {
        ATH_MSG_DEBUG("getScaleFactors(): not calculating scale factors");
        return;
    }

    // GetTotalMuonSF also test for OR
    float muSF = 1.0;
    if (m_isMC && config->has("m_doMuonSF") and config->get<bool>("m_doMuonSF")) {
        std::string trigName;
        if (m_SUSYTools->treatAsYear() == 2015) {
            trigName = m_SF_muonTrig2015;
        } else {
            trigName = m_SF_muonTrig2016;
        }
        muSF = m_SUSYTools->GetTotalMuonSF(*m_muons, true, true, trigName);
    }
    m_eventInfo->auxdecor<float>("muSF") = muSF;

    // idem for GetTotalElectronSF
    float elSF = 1.0;
    if (m_isMC && config->has("m_doElectronSF") and config->get<bool>("m_doElectronSF")) {
        elSF = m_SUSYTools->GetTotalElectronSF(*m_electrons);
        // std::cout << "GetScaleFactors(): elSF = " << elSF << std::endl;
    }
    m_eventInfo->auxdecor<float>("elSF") = elSF;

    float tauSF = 1.0;
    if (m_isMC && config->has("m_doTauSF") and config->get<bool>("m_doTauSF")) {
        bool m_doTauTrigSF = false;
        if (config->has("m_doTauTrigSF") and config->get<bool>("m_doTauTrigSF")) {
            m_doTauTrigSF = true;
        }
        tauSF = m_SUSYTools->GetTotalTauSF(*m_taus, true, m_doTauTrigSF);
        // std::cout << "GetScaleFactors(): tauSF = " << tauSF << std::endl;
    }
    m_eventInfo->auxdecor<float>("tauSF") = tauSF;

    float btagSF = 1.0;
    if (m_isMC && config->has("doBtagSF") and config->get<bool>("doBtagSF")) {
        btagSF = m_SUSYTools->BtagSF(m_jets);
        // std::cout << "GetScaleFactors(): btagSF = " << btagSF << std::endl;
    }
    m_eventInfo->auxdecor<float>("btagSF") = btagSF;

    if (isNominal) {
        m_eventWeight *= muSF * elSF * tauSF;
    }
}

void EventSelection::rebuildMET(bool storeVariation) {
    ATH_MSG_DEBUG("Rebuilding MET");

    m_rebuiltMET = new xAOD::MissingETContainer();
    m_rebuiltMET_aux = new xAOD::MissingETAuxContainer();
    m_rebuiltMET->setStore(m_rebuiltMET_aux);

    const std::string containerName = "MET_ZLFV" + m_containerSuffix + m_systematicSuffix;
    const std::string containerNameAux = containerName + "_Aux.";

    if (storeVariation) {
        ATH_MSG_DEBUG("Storing MET variation");
        if (!m_store->record(m_rebuiltMET, containerName).isSuccess()) {
            throw std::runtime_error("Unable to store MissingETContainer " + containerName);
        }
        if (!m_store->record(m_rebuiltMET_aux, containerNameAux).isSuccess()) {
            throw std::runtime_error("Unable to store MissingETAuxContainer " + containerNameAux);
        }
    }

    if (!m_SUSYTools->GetMET(*m_rebuiltMET, m_jets, m_electrons, m_muons, m_photons, m_taus, true, true, 0).isSuccess()) {
        throw std::runtime_error("Error in GetMET");
    }

    // Do we have a rebuilt MET?
    xAOD::MissingETContainer::const_iterator met_it = m_rebuiltMET->find("Final");
    if (met_it == m_rebuiltMET->end()) {
        throw std::runtime_error("Could not find Final MET after running GetMET");
    }

    // now retrieve the result and put in the store
    TVector2* MissingET = new TVector2(0., 0.);
    MissingET->Set((*met_it)->mpx(), (*met_it)->mpy());
    if (!m_store->record(MissingET, "SUSYMET" + m_containerSuffix + m_systematicSuffix).isSuccess()) {
        throw std::runtime_error("Could not store SUSYMET with tag SUSYMET" + m_containerSuffix + m_systematicSuffix);
    }

    Histograms& hists = Histograms::getInstance();
    // std::cout << "current syst = '" << hists.currentSystematic().systset.name() << "'" << std::endl;
    auto h = hists.monHist_ETmiss[hists.currentSystematic()];
    if (h) h->Fill((*(m_rebuiltMET->find("Final")))->met() / 1000.0);

    auto h_phi = hists.monHist_ETmiss_phi[hists.currentSystematic()];
    if (h_phi) h_phi->Fill((*(m_rebuiltMET->find("Final")))->phi());

    ATH_MSG_DEBUG("rebuildMET(): got MET = " << (*met_it)->met() << " MET_x = " << (*met_it)->mpx()
                                             << " MET_y = " << (*met_it)->mpy());
}

void EventSelection::rebuildTrackMET(bool storeVariation) {
    ATH_MSG_DEBUG("Rebuilding TrackMET");

    m_rebuiltTrackMET = new xAOD::MissingETContainer();
    m_rebuiltTrackMET_aux = new xAOD::MissingETAuxContainer();
    m_rebuiltTrackMET->setStore(m_rebuiltTrackMET_aux);

    const std::string containerName = "TrackMET_ZLFV" + m_containerSuffix + m_systematicSuffix;
    const std::string containerNameAux = containerName + "_Aux.";

    if (storeVariation) {
        ATH_MSG_DEBUG("Storing TrackMET variation");
        if (!m_store->record(m_rebuiltTrackMET, containerName).isSuccess()) {
            throw std::runtime_error("Unable to store MissingETContainer " + containerName);
        }
        if (!m_store->record(m_rebuiltTrackMET_aux, containerNameAux).isSuccess()) {
            throw std::runtime_error("Unable to store MissingETAuxContainer " + containerNameAux);
        }
    }

    if (!m_SUSYTools->GetTrackMET(*m_rebuiltTrackMET, m_jets, m_electrons, m_muons).isSuccess()) {
        throw std::runtime_error("Error in GetTrackMET");
    }

    // Do we have a rebuilt TrackMET?
    xAOD::MissingETContainer::const_iterator met_it = m_rebuiltTrackMET->find("Track");
    if (met_it == m_rebuiltTrackMET->end()) {
        throw std::runtime_error("Could not find Track TrackMET after running GetTrackMET");
    }

    // now retrieve the result and put in the store
    TVector2* MissingET = new TVector2(0., 0.);
    MissingET->Set((*met_it)->mpx(), (*met_it)->mpy());
    if (!m_store->record(MissingET, "SUSYTrackMET" + m_containerSuffix + m_systematicSuffix).isSuccess()) {
        throw std::runtime_error("Could not store SUSYTrackMET with tag SUSYTrackMET" + m_containerSuffix + m_systematicSuffix);
    }

    Histograms& hists = Histograms::getInstance();
    // std::cout << "current syst = '" << hists.currentSystematic().systset.name() << "'" << std::endl;
    auto h = hists.monHist_track_ETmiss[hists.currentSystematic()];
    if (h) h->Fill((*(m_rebuiltTrackMET->find("Track")))->met() / 1000.0);

    auto h_phi = hists.monHist_track_ETmiss_phi[hists.currentSystematic()];
    if (h_phi) h_phi->Fill((*(m_rebuiltTrackMET->find("Track")))->phi());

    ATH_MSG_DEBUG("rebuildTrackMET(): got TrackMET = " << (*met_it)->met() << " TrackMET_x = " << (*met_it)->mpx()
                                                       << " TrackMET_y = " << (*met_it)->mpy());
}

char EventSelection::getIsolationWP(const xAOD::Muon& mu) {
    // Isolation flag
    char iso_wp = 0b0000;
    if ((bool)m_IsolationSelectionTool_map[IsoSel::ISO_LOOSE]->accept(mu)) {
        iso_wp |= 0b0001;
    }

    if ((bool)m_IsolationSelectionTool_map[IsoSel::ISO_TIGHT]->accept(mu)) {
        iso_wp |= 0b0010;
    }

    if ((bool)m_IsolationSelectionTool_map[IsoSel::ISO_TIGHTTRACKONLY]->accept(mu)) {
        iso_wp |= 0b0100;
    }

    if ((bool)m_IsolationSelectionTool_map[IsoSel::ISO_TIGHTTRACKONLY_FIXEDRAD]->accept(mu)) {
        iso_wp |= 0b1000;
    }

    return iso_wp;
}

char EventSelection::getIsolationWP(const xAOD::Electron& el) {
    // Isolation flag
    char iso_wp = 0b0000;
    if ((bool)m_IsolationSelectionTool_map[IsoSel::ISO_LOOSE]->accept(el)) {
        iso_wp |= 0b0001;
    }

    if ((bool)m_IsolationSelectionTool_map[IsoSel::ISO_TIGHT]->accept(el)) {
        iso_wp |= 0b0010;
    }

    if ((bool)m_IsolationSelectionTool_map[IsoSel::ISO_TIGHTTRACKONLY]->accept(el)) {
        iso_wp |= 0b0100;
    }

    if ((bool)m_IsolationSelectionTool_map[IsoSel::ISO_TIGHTTRACKONLY_FIXEDRAD]->accept(el)) {
        iso_wp |= 0b1000;
    }

    return iso_wp;
}

void EventSelection::processEvent(const ST::SystInfo& sys) {
    const CP::SystematicSet& systSet = sys.systset;
    m_systematicSuffix = Utils::getSystematicSuffix(systSet);

    if (m_SUSYTools->applySystematicVariation(systSet) != CP::SystematicCode::Ok) {
        throw std::runtime_error("Could not apply systematics " + systSet.name());
    }

    ATH_MSG_DEBUG("Running systematic set with name '" << systSet.name() << "'");

    bool storeVariation = (systSet.name() == "" || sys.affectsKinematics);
    ATH_MSG_DEBUG("storeVaration = " << storeVariation);

    m_jets = 0;
    m_jets_aux = 0;
    m_taus = 0;
    m_taus_aux = 0;
    m_muons = 0;
    m_muons_aux = 0;
    m_electrons = 0;
    m_electrons_aux = 0;
    m_photons = 0;
    m_photons_aux = 0;

    getJets(storeVariation);
    getMuons(storeVariation);
    getElectrons(storeVariation);
    getTaus(storeVariation);

    // if (m_getPhotons) {
    getPhotons(storeVariation);
    //}

    // NOTE: we don't perform trigger matching here. it is very slow, so we do it only at the very end of the event selection, for
    // those events that we actually keep!

    removeOverlaps();
    getScaleFactors(SystematicFunctions::IsNominal(sys.systset));
    rebuildMET(storeVariation);
    rebuildTrackMET(storeVariation);
    setViewContainers();

    // m_store->print();  // TODO: debug flag

    // delete shallow copies if not needed later
    if (!storeVariation) {
        delete m_jets;
        delete m_jets_aux;
        delete m_taus;
        delete m_taus_aux;
        delete m_muons;
        delete m_muons_aux;
        delete m_electrons;
        delete m_electrons_aux;
        delete m_photons;
        delete m_photons_aux;
        delete m_rebuiltMET;
        delete m_rebuiltMET_aux;
        delete m_rebuiltTrackMET;
        delete m_rebuiltTrackMET_aux;
    }
}

bool EventSelection::checkBadJets(bool storeVariation) {
    bool haveBadJet = false;

    // Check for bad jets
    for (const auto& jet : *m_jets) {
        if (!is_passOR(*jet)) continue;
        if (is_bad(*jet)) {
            haveBadJet = true;
            break;
        }
    }

    if (!haveBadJet) {
        return false;
    }

    if (!storeVariation) {
        delete m_jets;
        delete m_jets_aux;
        delete m_taus;
        delete m_taus_aux;
        delete m_muons;
        delete m_muons_aux;
        delete m_electrons;
        delete m_electrons_aux;
        delete m_photons;
        delete m_photons_aux;
        delete m_rebuiltMET;
        delete m_rebuiltMET_aux;
        delete m_rebuiltTrackMET;
        delete m_rebuiltTrackMET_aux;
    }

    return true;
}

void EventSelection::processWeightSystematics(bool onlyNominal) {
    ATH_MSG_DEBUG("processWeightSystematics");
    for (const auto& sys : m_systInfoList) {
        const bool isNominal = SystematicFunctions::IsNominal(sys.systset);
        if (onlyNominal and not isNominal) {
            continue;
        }

        if (not(isNominal or sys.affectsWeights)) {
            continue;
        }
        auto systName = SystematicFunctions::RenamingRule(sys.systset.name());
        ATH_MSG_DEBUG("@weight syst: " << systName << " affects: " << sys.affectsType);

        // apply the variation - the reset method is outside
        // TODO: do we want this in case of the nominal one? if we're nominal, then either outside
        // this loop we're _already_ nominal, or we're a kinematic variation. In the latter case
        // we should probably then not apply again as the pT calibations might be fucked up. move onlyNominal
        // arg to a current syst one? we can keep track of that in the class.
        if (m_SUSYTools->applySystematicVariation(sys.systset) != CP::SystematicCode::Ok) {
            throw std::runtime_error("Error applying systematic variation");
        }

        // NOTE: when writing decorations, you will need to match whatever we do in VarGroups.cxx with the scale factors
        // or you will be punching yourself in the face a lot

        // if nominal, get everything for every type of object
        if (isNominal or sys.affectsType == ST::SystObjType::Tau) {
            if (systName != "NOMINAL") {
                ATH_MSG_VERBOSE("affects taus");
            }

            // we need 2 scale factors here:
            // 1) trigger
            // 2) ID

            const std::string idName = "idSF_" + systName;
            const std::string trigName = "trigSF_" + systName;

            unsigned int i = 0;
            for (const auto& tau : *m_taus) {
                if (not m_doTauSF and not m_doTauTrigSF) break;

                // skip non-baseline objects
                if (!is_baseline(*tau)) continue;

                ++i;
                if (m_doTauSF and (isNominal or sys.affectedWeights.count(ST::Weights::Tau::Reconstruction) > 0)) {
                    float idSF = m_SUSYTools->GetSignalTauSF(*tau, true, false);
                    tau->auxdecor<float>(idName) = idSF;
                    ATH_MSG_VERBOSE("syst: " << systName << " tau " << i << " " << tau << " idSF = " << idSF);
                }

                if (m_doTauTrigSF and (isNominal or sys.affectedWeights.count(ST::Weights::Tau::Trigger) > 0)) {
                    float trigSF = m_SUSYTools->GetSignalTauSF(*tau, false, true);
                    tau->auxdecor<float>(trigName) = trigSF;
                    ATH_MSG_VERBOSE("syst: " << systName << " tau " << i << " " << tau << " trigSF = " << trigSF);
                }
            }
        }

        if (isNominal or sys.affectsType == ST::SystObjType::Muon) {
            if (systName != "NOMINAL") {
                ATH_MSG_DEBUG("affects muons");
            }

            // we are interested in 3 things:
            // 1) iso SF
            // 2) reco SF
            // 3) trigger SF
            //
            // we play a little trick here: SUSYTools' function controls 2 options for iso & id systematics
            // the 3rd one is the trigger expression. if that's empty, the trigger SF is 1. so true/false/empty or
            // false/false/<string>
            // gives us what we want.

            const std::string isoSFName = "isoSF_" + systName;
            const std::string recoSFName = "recoSF_" + systName;
            const std::string trigSFName = "trigSF_" + systName;
            const std::string globalTrigSFName = "global_trigSF_" + systName;

            std::string trigName;
            if (m_SUSYTools->treatAsYear() == 2015) {
                trigName = m_SF_muonTrig2015;
            } else {
                trigName = m_SF_muonTrig2016;
            }

            if (m_doMuonTrigSF and (isNominal or sys.affectedWeights.count(ST::Weights::Muon::Trigger) > 0)) {
                float globalTrigSF = m_SUSYTools->GetTotalMuonSF(*m_muons, false, false, trigName);
                m_eventInfo->auxdecor<float>(globalTrigSFName) = globalTrigSF;
                ATH_MSG_VERBOSE("syst: " << systName << " global muon trigSF = " << globalTrigSF);
            }

            unsigned int i = 0;
            std::unique_ptr<xAOD::MuonContainer> oneMuon = CxxUtils::make_unique<xAOD::MuonContainer>(SG::VIEW_ELEMENTS);
            oneMuon->emplace_back(nullptr);
            for (const auto& mu : *m_muons) {
                if (not m_doMuonSF and not m_doMuonTrigSF) {
                    break;
                }

                // skip non-baseline objects
                if (!is_baseline(*mu)) continue;

                // DataVector<xAOD::MuonContainer> sfmuons(SG::VIEW_ELEMENTS);
                oneMuon->at(0) = mu;

                ++i;
                if (m_doMuonSF) {
                    if (isNominal or sys.affectedWeights.count(ST::Weights::Muon::Reconstruction) > 0) {
                        float recoSF = m_SUSYTools->GetTotalMuonSF(*(oneMuon.get()), true, false, "");
                        mu->auxdecor<float>(recoSFName) = recoSF;
                        ATH_MSG_VERBOSE("syst: " << systName << " muon " << i << " recoSF = " << recoSF);
                    }

                    if (isNominal or sys.affectedWeights.count(ST::Weights::Muon::Isolation) > 0) {
                        float isoSF = m_SUSYTools->GetTotalMuonSF(*(oneMuon.get()), false, true, "");
                        mu->auxdecor<float>(isoSFName) = isoSF;
                        ATH_MSG_VERBOSE("syst: " << systName << " muon " << i << " isoSF = " << isoSF);
                    }
                }

                if (m_doMuonTrigSF) {
                    if (isNominal or sys.affectedWeights.count(ST::Weights::Muon::Trigger) > 0) {
                        float trigSF = m_SUSYTools->GetTotalMuonSF(*(oneMuon.get()), false, false, trigName);
                        mu->auxdecor<float>(trigSFName) = trigSF;
                        ATH_MSG_VERBOSE("syst: " << systName << " muon " << i << " trigSF = " << trigSF);
                    }
                }
            }
        }

        if (isNominal or sys.affectsType == ST::SystObjType::Electron or sys.affectsType == ST::SystObjType::Egamma) {
            if (systName != "NOMINAL") {
                ATH_MSG_DEBUG("affects electrons");
            }

            const std::string idSFName = "idSF_" + systName;
            const std::string isoSFName = "isoSF_" + systName;
            const std::string recoSFName = "recoSF_" + systName;
            const std::string trigSFName = "trigSF_" + systName;
            // const std::string chargeFlipSFName = "chargeFlipSF_" + systName;

            unsigned int i = 0;
            for (const auto& el : *m_electrons) {
                if (not m_doElectronSF and not m_doElectronTrigSF) {
                    break;
                }

                // skip non-baseline objects
                if (!is_baseline(*el)) continue;

                ++i;
                if (m_doElectronSF) {
                    if (isNominal or sys.affectedWeights.count(ST::Weights::Electron::Reconstruction) > 0) {
                        float recoSF = m_SUSYTools->GetSignalElecSF(*el, true, false, false, false, "singleLepton", false);
                        el->auxdecor<float>(recoSFName) = recoSF;
                        ATH_MSG_VERBOSE("syst: " << systName << " electron " << i << " recoSF = " << recoSF);
                    }

                    if (isNominal or sys.affectedWeights.count(ST::Weights::Electron::ID) > 0) {
                        float idSF = m_SUSYTools->GetSignalElecSF(*el, false, true, false, false, "singleLepton", false);
                        el->auxdecor<float>(idSFName) = idSF;
                        ATH_MSG_VERBOSE("syst: " << systName << " electron " << i << " idSF = " << idSF);
                    }

                    if (isNominal or sys.affectedWeights.count(ST::Weights::Electron::Isolation) > 0) {
                        float isoSF = m_SUSYTools->GetSignalElecSF(*el, false, false, false, true, "singleLepton", false);
                        el->auxdecor<float>(isoSFName) = isoSF;
                        ATH_MSG_VERBOSE("syst: " << systName << " electron " << i << " isoSF = " << isoSF);
                    }

                    // if (isNominal or sys.affectedWeights.count(ST::Weights::Electron::ChargeID) > 0) {
                    // float chargeFlipSF = m_SUSYTools->GetSignalElecSF(*el, false, false, false, false, "singleLepton", true);
                    // el->auxdecor<float>(chargeFlipSFName) = chargeFlipSF;
                    // ATH_MSG_VERBOSE("syst: " << systName << " electron " << i << " chargeFlipSF = " << chargeFlipSF);
                    //}
                }

                if (m_doElectronTrigSF) {
                    // electron trigger expression is set in SUSYTools' config file and is activated through 'singleLepton'
                    // TODO: this might warrant an extra config flag
                    if (isNominal or sys.affectedWeights.count(ST::Weights::Electron::Trigger) > 0) {
                        float trigSF = m_SUSYTools->GetSignalElecSF(*el, false, false, true, false, "singleLepton", false);
                        el->auxdecor<float>(trigSFName) = trigSF;
                        ATH_MSG_VERBOSE("syst: " << systName << " electron " << i << " trigSF = " << trigSF);
                    }
                }
            }
        }

        if (m_getPhotons and
            (isNominal or sys.affectsType == ST::SystObjType::Photon or sys.affectsType == ST::SystObjType::Egamma)) {
            if (systName != "NOMINAL") {
                ATH_MSG_DEBUG("affects photons");
            }

            const std::string effName = "recoSF_" + systName;
            const std::string isoName = "isoSF_" + systName;

            unsigned int i = 0;
            for (const auto& ph : *m_photons) {
                if (not m_doPhotonSF) {
                    break;
                }

                // skip non-baseline objects - we get a lot of validity range errors here especially
                if (!is_baseline(*ph)) continue;

                if (not isNominal and sys.affectedWeights.count(ST::Weights::Photon::Reconstruction) == 0 and
                    sys.affectedWeights.count(ST::Weights::Photon::Isolation) == 0) {
                    break;
                }

                ++i;
                if (isNominal or sys.affectedWeights.count(ST::Weights::Photon::Reconstruction) > 0) {
                    auto recoSF = m_SUSYTools->GetSignalPhotonSF(*ph, true, false);
                    ph->auxdecor<float>(effName) = recoSF;
                    ATH_MSG_VERBOSE("syst: " << systName << " photon " << i << " recoSF = " << recoSF);
                }

                if (isNominal or sys.affectedWeights.count(ST::Weights::Photon::Isolation) > 0) {
                    auto isoSF = m_SUSYTools->GetSignalPhotonSF(*ph, false, true);
                    ph->auxdecor<float>(isoName) = isoSF;
                    ATH_MSG_VERBOSE("syst: " << systName << " photon " << i << " isoSF = " << isoSF);
                }
            }
        }

        if ((isNominal or sys.affectedWeights.count(ST::Weights::Jet::JVT)) and m_config->get<bool>("doJVTSF")) {
            if (systName != "NOMINAL") {
                ATH_MSG_DEBUG("affects jets");
            }

            std::string sfName = "JVT_SF_" + systName;
            float globalJvtSF = 1.0;
            if (m_event->contains<xAOD::JetContainer>("AntiKt4TruthJets")) {
                globalJvtSF = m_SUSYTools->JVT_SF(m_jets);
            }

            // global
            ATH_MSG_VERBOSE("syst: " << systName << " global jvtSF = " << globalJvtSF);
            m_eventInfo->auxdecor<float>("global_" + sfName) = globalJvtSF;

            // per jet
            unsigned int i = 0;
            for (const auto& jet : *m_jets) {
                const auto _jvtSF = acc_jvtscalefact(*jet);
                jet->auxdecor<double>(sfName) = _jvtSF;
                ATH_MSG_VERBOSE("syst: " << systName << " jet " << ++i << " jvtSF = " << _jvtSF);
            }
        }
        
        if ((isNominal or sys.affectedWeights.count(ST::Weights::Jet::Btag)) and m_config->get<bool>("doBtagSF")) {
            if (systName != "NOMINAL") {
                ATH_MSG_DEBUG("affects btag");
            }
        
            std::string sfName = "btag_SF_MVX_" + systName;
            const auto globalBtagSF = m_SUSYTools->BtagSF(m_jets);  // this is now the global number
        
            // global
            ATH_MSG_VERBOSE("syst: " << systName << " global btagSF = " << globalBtagSF);
            m_eventInfo->auxdecor<float>("global_" + sfName) = globalBtagSF;
        
            // per jet
            unsigned int i = 0;
            for (const auto& jet : *m_jets) {
                const auto _btagSF = acc_effscalefact(*jet);
                jet->auxdecor<float>(sfName) = _btagSF;
                ATH_MSG_VERBOSE("syst: " << systName << " jet " << ++i << " btagSF = " << _btagSF);
            }
        }

        if (sys.affectsType == ST::SystObjType::EventWeight) {
            ATH_MSG_DEBUG("affects EventWeight");

            // we don't do this for the nominal case, as the nominal case is already called earlier!
            processPileup(sys);
        }

        if (isNominal or sys.affectsType == ST::SystObjType::MET_TST) {
            if (systName != "NOMINAL") {
                ATH_MSG_DEBUG("affects MET TST - shouldn't happen for a weight");
            }
        }

        if (isNominal or sys.affectsType == ST::SystObjType::MET_CST) {
            if (systName != "NOMINAL") {
                ATH_MSG_DEBUG("affects MET CST - shouldn't happen for a weight");
            }
        }
    }

    if (m_SUSYTools->resetSystematics() != CP::SystematicCode::Ok) {
        throw std::runtime_error("Error resetting systematic variation");
    }
}
