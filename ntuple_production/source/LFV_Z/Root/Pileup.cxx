// vim: ts=4 sw=4
#include <LFV_Z/EventSelection.h>
#include <LFV_Z/Utils.h>
#include "AthContainers/AuxElement.h"
#include "xAODRootAccess/TActiveStore.h"

EL::StatusCode EventSelection::processPileup(const ST::SystInfo& sys) {
    const CP::SystematicSet& systSet = sys.systset;

    if (not SystematicFunctions::IsNominal(systSet) and sys.affectsType != ST::SystObjType::EventWeight) {
        // no point in doing something for non-nominal or non-PUW systematics - they're going to return the same
        return EL::StatusCode::SUCCESS;
    }

    /** retrieve the systematic name */
    auto systName = SystematicFunctions::RenamingRule(systSet);

    /** apply the systematic variation: NOMINAL, PRW_DATASF__1down, PRW_DATASF__1up */
    if (m_PRWTool->applySystematicVariation(systSet) == CP::SystematicCode::Unsupported) {
        throw std::runtime_error("Cannot use pileup for systematic set");
    }

    // std::cout << "Processing pileup for " << systName << std::endl;

    /** decorate PU weight with appropriate variation : correct MC for differences in pileup distributions between MC and Data */
    float pileupWeight = m_PRWTool->getCombinedWeight(*m_eventInfo);

    // check that we're not an unsupported channel - if so, we set the weight back to 1.0
    if (!m_usePileupWeight) {
        ATH_MSG_INFO("Overwriting pileupWeight with 1.0 -> this is an unsupported channel");
        pileupWeight = 1.0;
    }

    // m_eventInfo->auxdecor<float>(systName + "_pileup_combined_weight") = pileupWeight;
    m_eventInfo->auxdecor<float>("pileupWeight_" + systName) = pileupWeight;

    // std::cout << "name = " << systName + "_pileup_combined_weight" << std::endl;
    // std::cout << "weight = " << pileupWeight << std::endl;

    // decorate random number
    unsigned int RndRun = m_PRWTool->getRandomRunNumber(*m_eventInfo, true);
    m_eventInfo->auxdecor<unsigned int>(systName + "_RandomRunNumber") = RndRun;

    // decorate random lumiblock number
    unsigned int RandomLB = RndRun ? m_PRWTool->GetRandomLumiBlockNumber(RndRun) : 0;
    m_eventInfo->auxdecor<unsigned int>(systName + "_RandomLBNumber") = RandomLB;

    /** rest only for nominal or kinematic case */
    if (!SystematicFunctions::IsNominal(systSet)) {
        return EL::StatusCode::SUCCESS;
    }

    // decorate the corrected mu : Correct data mu values to account for latest lumi calculation recommendations
    auto nCorr = m_PRWTool->getCorrectedAverageInteractionsPerCrossing(*m_eventInfo, false);
    m_eventInfo->auxdecor<float>("pileup_corrected_averageInteractionsPerCrossing") = nCorr;

    // decorate PRW Hash
    auto hash = m_PRWTool->getPRWHash(*m_eventInfo);
    m_eventInfo->auxdecor<ULong64_t>("pileup_prwhash") = hash;

    // update counters if current global systematic is nominal (avoid double counting due to other kinematic variations)
    if (SystematicFunctions::IsNominal(sys.systset)) {
        m_nPUWeightedEvents += pileupWeight;
        m_eventWeight *= pileupWeight;
    }  // global case is nominal

    return EL::StatusCode::SUCCESS;
}
