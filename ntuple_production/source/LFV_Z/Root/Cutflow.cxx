#include <LFV_Z/Cutflow.h>
#include <string>

void Cut::setLabel(const std::string &s) {
    m_label = s;
}

const std::string Cut::getLabel() const {
    return m_label;
}

void Cut::setName(const std::string &s) {
    m_name = s;
}

const std::string Cut::getName() const {
    return m_name;
}

void Cutflow::passCut(const std::string &name) {
    getCut(name)->passCut();
}

const std::shared_ptr<Cut> Cutflow::getCut(const std::string &name) {
    for (const auto &c : m_cuts) {
        if (c->getName() == name) {
            return c;
        }
    }

    throw std::runtime_error("Cut asked for does not exist");
}

const std::shared_ptr<Cut> Cutflow::addCut(const std::string &name, const std::string &label) {
    std::shared_ptr<Cut> c(new Cut(name, label));
    m_cuts.push_back(c);

    return c;
}
