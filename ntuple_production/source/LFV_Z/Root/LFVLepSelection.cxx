#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <LFV_Z/Histograms.h>
#include <LFV_Z/LFVLepSelection.h>
#include <LFV_Z/Utils.h>

#include <iostream>
#include <string>
#include <vector>

#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "Extrapolate/Track.h"
#include "Extrapolate/Vertex.h"

extern double energyMetric();

const static SG::AuxElement::Accessor<char> is_baseline("baseline");

LFVLepSelection::LFVLepSelection(const std::string &prefix, TFile *outputFile, xAOD::TStore *store, xAOD::TEvent *event, EL::IWorker *wk,
                           const std::vector<ST::SystInfo> &systInfoList, const std::map<std::string, int> &theory_systs,
                           std::shared_ptr<custom_SUSYObjDef_xAOD> SUSYTools)
    : Region(prefix, outputFile, store, event, wk, systInfoList, theory_systs, SUSYTools), is_signal("signal") {
    int nbins = 3;
    Histograms &hists = Histograms::getInstance();
    hists.cutflow_LFVLep = new TH1D("cutflow", "Number of accepted events", nbins, 1, nbins + 1);
    hists.cutflow_LFVLep->GetXaxis()->SetBinLabel(1, "Start");
    hists.cutflow_LFVLep->GetXaxis()->SetBinLabel(2, ">= 1 lepton");
    hists.cutflow_LFVLep->GetXaxis()->SetBinLabel(3, ">= 1 tau OR >= 2 signal leptons");
    hists.cutflow_LFVLep.cloneForSystematics(systInfoList);

    std::cout << "Constructed LFVLep selection class" << std::endl;

    // m_photons.initialize();
    m_jets.initialize();
    // m_electrons.initialize();
    // m_muons.initialize();
    m_taus.initialize();

    m_trigger_list_muon = m_config->get<std::vector<std::string>>("trigger_list_muon");
    m_trigger_list_electron = m_config->get<std::vector<std::string>>("trigger_list_electron");

    std::cout << "adding theory weights" << std::endl;

    m_theory_weights = CxxUtils::make_unique<FillTheoryWeights>(vlist, "theory_weights", theory_systs);

    /**
     * @short Region base function to initialise output tree and its branches
     */
    SetupTrees();
}

bool LFVLepSelection::checkSameFlavourPair(const FillBranchesArgs &args) {
    const auto nElectrons = ContainerUtils::Count<char>(args.electrons, "signal");
    const auto nMuons = ContainerUtils::Count<char>(args.muons, "signal");

    m_leplep_invmass = -99.0;  // fill it with garbage on purpose

    if (nElectrons != 2 and nMuons != 2) {
        return false;
    }

    if (nElectrons == 2 and nMuons == 2) {
        // this is probably very rare, but let's be safe.
        return false;
    }

    // actually check the charge
    if (nElectrons == 2) {
        const auto el_0 = ContainerUtils::GetSelectedParticle<char>(args.electrons, {"signal"}, 0);
        const auto el_1 = ContainerUtils::GetSelectedParticle<char>(args.electrons, {"signal"}, 1);

        const auto q_0 = ParticleUtils::GetCharge(el_0);
        const auto q_1 = ParticleUtils::GetCharge(el_1);

        if (q_0 * q_1 > 0) {
            return false;
        }

        const auto _m = KinematicUtils::InvariantMass({el_0, el_1}) * energyMetric();
        m_leplep_invmass = _m;
        // std::cout << "found OS-SF el-el pair; m_inv = " << _m << std::endl;
    }

    if (nMuons == 2) {
        const auto mu_0 = ContainerUtils::GetSelectedParticle<char>(args.muons, {"signal"}, 0);
        const auto mu_1 = ContainerUtils::GetSelectedParticle<char>(args.muons, {"signal"}, 1);

        const auto q_0 = ParticleUtils::GetCharge(mu_0);
        const auto q_1 = ParticleUtils::GetCharge(mu_1);

        if (q_0 * q_1 > 0) {
            return false;
        }

        const auto _m = KinematicUtils::InvariantMass({mu_0, mu_1}) * energyMetric();
        m_leplep_invmass = _m;
        // std::cout << "found OS-SF mu-mu pair; m_inv = " << _m << std::endl;
    }

    return true;
}

bool LFVLepSelection::checkOppositeSignLeptonPair(const FillBranchesArgs &args) {
    const auto nElectrons = ContainerUtils::Count<char>(args.electrons, "signal");
    const auto nMuons = ContainerUtils::Count<char>(args.muons, "signal");

    bool passed_selection = false;
    m_leplep_invmass = -99.0;  // fill it with garbage on purpose
    m_elel_invmass = -99.0;  // fill it with garbage on purpose
    m_mumu_invmass = -99.0;  // fill it with garbage on purpose
    m_muel_invmass = -99.0;  // fill it with garbage on purpose

    if ( nElectrons+nMuons < 2 ) {
        return false;
    }
    // std::cout << "LIGHT LEPTONS   # = " << nElectrons+nMuons << std::endl;
    // std::cout << "found # ELECTRONS = " << nElectrons << std::endl;
    // std::cout << "found # MUONS     = " << nMuons << std::endl;

    // actually check the charge
    if (nElectrons >= 2) {
        const auto el_0 = ContainerUtils::GetSelectedParticle<char>(args.electrons, {"signal"}, 0);
        const auto el_1 = ContainerUtils::GetSelectedParticle<char>(args.electrons, {"signal"}, 1);

        const auto q_0 = ParticleUtils::GetCharge(el_0);
        const auto q_1 = ParticleUtils::GetCharge(el_1);

        if (q_0 * q_1 < 0) {
	    passed_selection = true;
	    const auto _m = KinematicUtils::InvariantMass({el_0, el_1}) * energyMetric();
	    m_elel_invmass   = _m;
	    m_leplep_invmass = _m;
	    // std::cout << "found OS-SF el-el pair; m_inv = " << _m << std::endl;
	}
    }

    if (nMuons >= 2) {
        const auto mu_0 = ContainerUtils::GetSelectedParticle<char>(args.muons, {"signal"}, 0);
        const auto mu_1 = ContainerUtils::GetSelectedParticle<char>(args.muons, {"signal"}, 1);

        const auto q_0 = ParticleUtils::GetCharge(mu_0);
        const auto q_1 = ParticleUtils::GetCharge(mu_1);

        if (q_0 * q_1 < 0) {
	    passed_selection = true;
	    const auto _m = KinematicUtils::InvariantMass({mu_0, mu_1}) * energyMetric();
	    m_mumu_invmass   = _m;
	    m_leplep_invmass = _m;
	    // std::cout << "found OS-SF mu-mu pair; m_inv = " << _m << std::endl;
	}
    }

    if (nElectrons >= 1 and nMuons >= 1) {
        const auto mu = ContainerUtils::GetSelectedParticle<char>(args.muons, {"signal"}, 0);
	const auto el = ContainerUtils::GetSelectedParticle<char>(args.electrons, {"signal"}, 0);


        const auto q_mu = ParticleUtils::GetCharge(mu);
        const auto q_el = ParticleUtils::GetCharge(el);

        if (q_mu * q_el < 0) {
	    passed_selection = true;
            const auto _m = KinematicUtils::InvariantMass({mu, el}) * energyMetric();
	    m_muel_invmass = _m;
	    // std::cout << "found OS e-mu pair; m_inv = " << _m << std::endl;
	}
    }

    return passed_selection;
}

bool LFVLepSelection::checkLeptonPair(const FillBranchesArgs &args) {
    const auto nElectrons = ContainerUtils::Count<char>(args.electrons, "signal");
    const auto nMuons = ContainerUtils::Count<char>(args.muons, "signal");

    m_leplep_invmass = -99.0;  // fill it with garbage on purpose
    m_elel_invmass = -99.0;  // fill it with garbage on purpose
    m_mumu_invmass = -99.0;  // fill it with garbage on purpose
    m_muel_invmass = -99.0;  // fill it with garbage on purpose
    m_leplepSS_invmass = -99.0;  // fill it with garbage on purpose
    m_elelSS_invmass = -99.0;  // fill it with garbage on purpose
    m_mumuSS_invmass = -99.0;  // fill it with garbage on purpose
    m_muelSS_invmass = -99.0;  // fill it with garbage on purpose

    if ( nElectrons+nMuons < 2 ) {
        return false;
    }
    // std::cout << "LIGHT LEPTONS   # = " << nElectrons+nMuons << std::endl;
    // std::cout << "found # ELECTRONS = " << nElectrons << std::endl;
    // std::cout << "found # MUONS     = " << nMuons << std::endl;

    // actually check the charge
    if (nElectrons >= 2) {
        const auto el_0 = ContainerUtils::GetSelectedParticle<char>(args.electrons, {"signal"}, 0);
        const auto el_1 = ContainerUtils::GetSelectedParticle<char>(args.electrons, {"signal"}, 1);

        const auto q_0 = ParticleUtils::GetCharge(el_0);
        const auto q_1 = ParticleUtils::GetCharge(el_1);

	const auto _m = KinematicUtils::InvariantMass({el_0, el_1}) * energyMetric();
        if (q_0 * q_1 < 0) {
	    m_elel_invmass   = _m;
	    m_leplep_invmass = _m;
	    // std::cout << "found OS-SF el-el pair; m_inv = " << _m << std::endl;
	}
	else {
	    m_elelSS_invmass   = _m;
	    m_leplepSS_invmass = _m;
	}
    }

    if (nMuons >= 2) {
        const auto mu_0 = ContainerUtils::GetSelectedParticle<char>(args.muons, {"signal"}, 0);
        const auto mu_1 = ContainerUtils::GetSelectedParticle<char>(args.muons, {"signal"}, 1);

        const auto q_0 = ParticleUtils::GetCharge(mu_0);
        const auto q_1 = ParticleUtils::GetCharge(mu_1);

	const auto _m = KinematicUtils::InvariantMass({mu_0, mu_1}) * energyMetric();
        if (q_0 * q_1 < 0) {
	    m_mumu_invmass   = _m;
	    m_leplep_invmass = _m;
	    // std::cout << "found OS-SF mu-mu pair; m_inv = " << _m << std::endl;
	}
	else {
	    m_mumuSS_invmass   = _m;
	    m_leplepSS_invmass = _m;
	}
    }

    if (nElectrons >= 1 and nMuons >= 1) {
        const auto mu = ContainerUtils::GetSelectedParticle<char>(args.muons, {"signal"}, 0);
	const auto el = ContainerUtils::GetSelectedParticle<char>(args.electrons, {"signal"}, 0);

        const auto q_mu = ParticleUtils::GetCharge(mu);
        const auto q_el = ParticleUtils::GetCharge(el);

	const auto _m = KinematicUtils::InvariantMass({mu, el}) * energyMetric();
        if (q_mu * q_el < 0) {
	    m_muel_invmass = _m;
	    // std::cout << "found OS e-mu pair; m_inv = " << _m << std::endl;
	}
	else {
	    m_muelSS_invmass = _m;
	}
    }

    return true;
}

/* If this method returns true; store event. If false, don't store. */
bool LFVLepSelection::FillBranches(const FillBranchesArgs &args) {
    Histograms &hists = Histograms::getInstance();
    auto h = hists.cutflow_LFVLep[hists.currentSystematic()];
    if (h) h->Fill(1);

    // At least one lepton
    if (args.electrons->size() == 0 and args.muons->size() == 0) {
        return false;
    }

    if (h) h->Fill(2);

    // Select at least 1 tau OR two signal leptons
    // auto haveOSSFPair = checkSameFlavourPair(args);  // the function call is needed to set m_ll
    // auto haveOSSFPair = checkOppositeSignLeptonPair(args);
    auto haveLepLepPair = checkLeptonPair(args);

    if (args.taus->size() == 0 and not haveLepLepPair) {
        // std::cout << "nTaus = " << args.taus->size() << " OS pair: " << haveOSSFPair << std::endl;
        return false;
    }

    if (h) h->Fill(3);

    // No b-jet veto or OS cut anymore for CRs - we should do something cleverer once
    // we have a set of regions! Better to have multiple smaller trees than one big one.

    FillImpactParameters(args);
    doTriggerMatching(args);

    if (not FillCommon(args)) {
        return false;
    }

    // NB: put your pT cuts for leptons, tau, etc here
    // (plus any additional cuts on dEta or MMC mass, or mT, or ...)

    return true;
}

void LFVLepSelection::doTriggerMatching(const FillBranchesArgs &args) {
    // For muons and electrons only, for now.

    // Only match triggers that fired
    for (const auto &s : m_trigger_list_muon) {
        if (args.trigger.decision.at(s) == 0) continue;
        m_SUSYTools->TrigMatch(args.muons, s);
    }

    for (const auto &s : m_trigger_list_electron) {
        if (args.trigger.decision.at(s) == 0) continue;
        m_SUSYTools->TrigMatch(args.electrons, s);
    }
}

// calculate the tau IP parameters
void LFVLepSelection::FillImpactParameters(const FillBranchesArgs &args) {
    const auto evtInfo = args.eventInfo;
    const auto p_vtx = args.vertex.primary;

    TVector3 beamPos(evtInfo->beamPosX(), evtInfo->beamPosY(), evtInfo->beamPosZ());

    double p_vtx_z = p_vtx ? p_vtx->z() : 0;
    p_vtx_z -= beamPos.Z();  // w.r.t. beam spot

    // Write z0sintheta not wrt the primary vertex for muons
    for (const auto &mu : *(args.muons)) {
        auto track = mu->primaryTrackParticle();
        if (!track) continue;

        const double z0 = track->z0() + track->vz();
        mu->auxdecor<float>("d0") = track->d0();
        mu->auxdecor<float>("delta_z0") = z0 - p_vtx_z;
        mu->auxdecor<float>("z0sinTheta_no_pvtx") = z0 * std::fabs(std::sin(track->theta()));
        mu->auxdecor<float>("a0PV") = ParticleUtils::doca(track, p_vtx, beamPos);
    }

    // Write z0sintheta not wrt the primary vertex for electrons
    for (const auto &el : *(args.electrons)) {
        auto track = el->trackParticle();
        if (!track) continue;

        const double z0 = track->z0() + track->vz();
        el->auxdecor<float>("d0") = track->d0();
        el->auxdecor<float>("delta_z0") = z0 - p_vtx_z;
        el->auxdecor<float>("z0sinTheta_no_pvtx") = z0 * std::fabs(std::sin(track->theta()));
        el->auxdecor<float>("a0PV") = ParticleUtils::doca(track, p_vtx, beamPos);
    }

    for (const auto &tau : *(args.taus)) {
        double d0 = -999.0;
        double z0 = -999.0;
        double d0sig = -999.0;
        double z0sintheta = -999.0;
        double z0sintheta_no_pvtx = -999.0;
        double a0PV = -999.0;

        const int nTracks = tau->nTracks();
        try {
            if (nTracks == 1) {
                // const double sinTheta = std::sin(tau->p4().Theta()); // Be consistent?
                const auto track = tau->track(0)->track();
                const double sinTheta = std::sin(track->theta());  // Be consistent?
                d0 = track->d0();
                z0 = track->z0() + track->vz();
                z0sintheta = (z0 - p_vtx_z) * sinTheta;
                z0sintheta_no_pvtx = z0 * sinTheta;
                d0sig = xAOD::TrackingHelpers::d0significance(track, evtInfo->beamPosSigmaX(), evtInfo->beamPosSigmaY(), evtInfo->beamPosSigmaXY());
                a0PV = ParticleUtils::doca(track, p_vtx, beamPos);
            } else if (nTracks == 3) {
                auto track = Vertex(Track(tau->track(0)), Track(tau->track(1)), Track(tau->track(2)));
                // auto chi2 = track.fit(); // chi2 is not used anyway?
                track.fit();

                d0 = track.d0();
                z0 = track.z0();  // Don't add vz(), it is already done in Extrapolate/Track.h
                const auto theta = track.theta();
                const double d0err = track.d0err();

                // see
                // http://acode-browser2.usatlas.bnl.gov/lxr-rel20/source/atlas/Event/xAOD/xAODTracking/Root/TrackParticlexAODHelpers.cxx#0048
                const double d0uncert_beamspot_2 = xAOD::TrackingHelpers::d0UncertaintyBeamSpot2(
                    track.phi(), evtInfo->beamPosSigmaX(), evtInfo->beamPosSigmaY(), evtInfo->beamPosSigmaXY());

                d0sig = d0 / (d0err + d0uncert_beamspot_2);
                z0sintheta_no_pvtx = z0 * std::sin(theta);
                z0sintheta = (z0 - p_vtx_z) * std::sin(theta);
                a0PV = ParticleUtils::doca(&track, p_vtx, beamPos);
            }
        }
        catch(...) {
            // std::cout << "Warning: failed to get tau track information! Filling impact parameters with dummy values (-999)!" << std::endl;
            // Terry: This happens since R21. Might be due to changes in the derivation, or changes in the xAOD tau codes. But we are not using these information at the moment anyway, and the occurrence rate is quite low, so let's save this for another day.
        }

        tau->auxdecor<float>("d0") = d0;
        tau->auxdecor<float>("delta_z0") = z0 - p_vtx_z;
        tau->auxdecor<float>("z0sinTheta_no_pvtx") = z0sintheta_no_pvtx;
        tau->auxdecor<float>("z0sinTheta") = z0sintheta;
        tau->auxdecor<float>("d0sig") = d0sig;
        tau->auxdecor<float>("a0PV") = a0PV;
    }
}

bool LFVLepSelection::FillCommon(const FillBranchesArgs &args) {
    // Fill all the branches with variables; return false means the event will not be written
    // NOTE: this method currently needs a cleanup. --GJ 29/04/16

    // TODO: do we want ProcessEvent to give us only signal objects or also baseline ones?
    // TODO: store multiplicities; e/mu switches

    // Now fill the branches
    m_eventInfo.fill(args.eventInfo);     //!< basic event information
    m_eventWeights.fill(args.eventInfo);  //!< MC event weights
    m_multiplicities.fill(args.eventInfo, args.muons, args.electrons, args.photons, args.taus, args.jets,
                          args.vertex.container);  //!< object multiplicities

    // Write trigger info
    m_trigger_el.fill(args.trigger.decision);
    m_trigger_mu.fill(args.trigger.decision);
    // m_trigger_tau.fill(args.trigger.decision);

    // Write vertex info
    // This is position w.r.t. beam spot
    m_primaryVertex.fill({args.vertex.primary->x(), args.vertex.primary->y(), args.vertex.primary->z()},
                         {args.eventInfo->beamPosX(), args.eventInfo->beamPosY(), args.eventInfo->beamPosZ()});
    m_primaryVertex_nTracks = args.vertex.primary->nTrackParticles();

    // Fill MET
    m_met.fill(args.met.reco);

    // Fill track MET
    m_trackmet.fill(args.met.reco_track);

    // fill jets & leptons
    // m_photons.add(args.photons);
    m_jets.add(args.jets);
    m_taus.add(args.taus);
    m_muons.add(args.muons);
    m_electrons.add(args.electrons);

    // Take the leading baseline particles for the calculations
    const xAOD::TauJet *tau = args.taus->size() > 0 ? args.taus->at(0) : nullptr;
    const xAOD::IParticle *el = args.electrons->size() > 0 ? args.electrons->at(0) : nullptr;
    const xAOD::IParticle *el_1 = args.electrons->size() > 1 ? args.electrons->at(1) : nullptr;
    const xAOD::IParticle *el_2 = args.electrons->size() > 2 ? args.electrons->at(2) : nullptr;
    const xAOD::IParticle *mu = args.muons->size() > 0 ? args.muons->at(0) : nullptr;
    const xAOD::IParticle *mu_1 = args.muons->size() > 1 ? args.muons->at(1) : nullptr;
    const xAOD::IParticle *mu_2 = args.muons->size() > 2 ? args.muons->at(2) : nullptr;
    const xAOD::Photon *ph = args.photons->size() > 0 ? args.photons->at(0) : nullptr;

    // Fill photon
    m_ph_0.fill(args.photons, 0);
    m_ph_1.fill(args.photons, 1);

    // Trigger matching
    m_el_0_trigmatch.fill(el);
    m_mu_0_trigmatch.fill(mu);

    // Transverse masses
    m_tau_0_mT = KinematicUtils::mT(tau, args.met.reco) * energyMetric();
    m_el_0_mT = KinematicUtils::mT(el, args.met.reco) * energyMetric();
    m_el_1_mT = KinematicUtils::mT(el_1, args.met.reco) * energyMetric();
    m_el_2_mT = KinematicUtils::mT(el_2, args.met.reco) * energyMetric();
    m_mu_0_mT = KinematicUtils::mT(mu, args.met.reco) * energyMetric();
    m_mu_1_mT = KinematicUtils::mT(mu_1, args.met.reco) * energyMetric();
    m_mu_2_mT = KinematicUtils::mT(mu_2, args.met.reco) * energyMetric();
    m_ph_0_mT = KinematicUtils::mT(ph, args.met.reco) * energyMetric();
    m_mu_ph_0_mT = KinematicUtils::mT(mu, ph, args.met.reco) * energyMetric();
    m_el_ph_0_mT = KinematicUtils::mT(el, ph, args.met.reco) * energyMetric();

    if (tau) {
        // Lead track four-vector
        if (tau->nTracks() > 0) {
            m_tau_0_lead_track.fill(tau->track(0)->p4());
        }

        m_tau_0_overlaps_with_calo_muon = tau->auxdecor<char>("overlaps_with_calo_muon");
    }

    // Kinematic variables of pairs
    if (tau && el) {
        m_tau_el_delta_alpha = KinematicUtils::DeltaAlpha(tau, el);
        m_tau_el_mvis = KinematicUtils::VisibleMass(tau, el) * energyMetric();
        m_tau_el_mcoll = KinematicUtils::CollinearMass(tau, el, args.met.reco) * energyMetric();
        m_tau_el_mmc.fill(args.eventInfo, tau, el, args.jets, args.met.reco);

        m_tautrack_el_invmass = -999.;
        if (tau->nTracks() > 0) {
            m_tautrack_el_invmass = KinematicUtils::InvariantMass({tau->track(0)->p4(), el->p4()}) * energyMetric();
        }

        if (ph) {
            m_tau_el_ph_mcoll = KinematicUtils::CollinearMass(tau, el, ph, args.met.reco) * energyMetric();
            m_tau_el_ph_mvis = KinematicUtils::VisibleMass(tau, el, ph) * energyMetric();
        }
    }

    if (tau && mu) {
        m_tau_mu_delta_alpha = KinematicUtils::DeltaAlpha(tau, mu);
        m_tau_mu_mvis = KinematicUtils::VisibleMass(tau, mu) * energyMetric();
        m_tau_mu_mcoll = KinematicUtils::CollinearMass(tau, mu, args.met.reco) * energyMetric();
        m_tau_mu_mmc.fill(args.eventInfo, tau, mu, args.jets, args.met.reco);

        m_tautrack_mu_invmass = -999.;
        if (tau->nTracks() > 0) {
            m_tautrack_mu_invmass = KinematicUtils::InvariantMass({tau->track(0)->p4(), mu->p4()}) * energyMetric();
        }

        if (ph) {
            m_tau_mu_ph_mcoll = KinematicUtils::CollinearMass(tau, mu, ph, args.met.reco) * energyMetric();
            m_tau_mu_ph_mvis = KinematicUtils::VisibleMass(tau, mu, ph) * energyMetric();
        }
    }

    if (mu && el) {
        m_mu_el_delta_alpha = KinematicUtils::DeltaAlpha(mu, el);
	m_el_mu_delta_alpha = KinematicUtils::DeltaAlpha(el, mu);
        m_mu_el_mvis = KinematicUtils::VisibleMass(mu, el) * energyMetric();
        m_mu_el_mcoll = KinematicUtils::CollinearMass(mu, el, args.met.reco) * energyMetric();
	m_el_mu_mcoll = KinematicUtils::CollinearMass(el, mu, args.met.reco) * energyMetric();
        m_mu_el_mmc.fill(args.eventInfo, mu, el, args.jets, args.met.reco);
	m_el_mu_mmc.fill(args.eventInfo, el, mu, args.jets, args.met.reco);

        m_mu_el_invmass = -999.;
        m_mu_el_invmass = KinematicUtils::InvariantMass({mu->p4(), el->p4()}) * energyMetric();

        if (ph) {
	    m_mu_el_ph_mcoll = KinematicUtils::CollinearMass(mu, el, ph, args.met.reco) * energyMetric();
	    m_el_mu_ph_mcoll = KinematicUtils::CollinearMass(el, mu, ph, args.met.reco) * energyMetric();
            m_mu_el_ph_mvis = KinematicUtils::VisibleMass(mu, el, ph) * energyMetric();
        }
    }

    if (mu && mu_1) {
        m_mu_mu_mvis = KinematicUtils::VisibleMass(mu, mu_1) * energyMetric();
	if ( mu->p4().Pt()>mu_1->p4().Pt() ) {
	  m_mu_mu_mcoll = KinematicUtils::CollinearMass(mu_1, mu, args.met.reco) * energyMetric();
	  m_mu_mu_delta_alpha = KinematicUtils::DeltaAlpha(mu_1, mu);
	  m_mu_mu_mmc.fill(args.eventInfo, mu_1, mu, args.jets, args.met.reco); }
	else {
	  m_mu_mu_mcoll = KinematicUtils::CollinearMass(mu, mu_1, args.met.reco) * energyMetric();
	  m_mu_mu_delta_alpha = KinematicUtils::DeltaAlpha(mu, mu_1);
	  m_mu_mu_mmc.fill(args.eventInfo, mu, mu_1, args.jets, args.met.reco); }

        m_mu_mu_invmass = -999.;
        m_mu_mu_invmass = KinematicUtils::InvariantMass({mu->p4(), mu_1->p4()}) * energyMetric();

        if (ph) {
	    if ( mu->p4().Pt()>mu_1->p4().Pt() ) {
	      m_mu_mu_ph_mcoll = KinematicUtils::CollinearMass(mu_1, mu, ph, args.met.reco) * energyMetric(); }
	    else {
	      m_mu_mu_ph_mcoll = KinematicUtils::CollinearMass(mu, mu_1, ph, args.met.reco) * energyMetric(); }
            m_mu_mu_ph_mvis = KinematicUtils::VisibleMass(mu, mu_1, ph) * energyMetric();
        }
    }

    if (el && el_1) {
        m_el_el_mvis = KinematicUtils::VisibleMass(el, el_1) * energyMetric();
        if ( el->p4().Pt()>el_1->p4().Pt() ) {
	  m_el_el_mcoll = KinematicUtils::CollinearMass(el_1, el, args.met.reco) * energyMetric();
	  m_el_el_delta_alpha = KinematicUtils::DeltaAlpha(el_1, el);
	  m_el_el_mmc.fill(args.eventInfo, el_1, el, args.jets, args.met.reco); }
        else {
	  m_el_el_mcoll = KinematicUtils::CollinearMass(el, el_1, args.met.reco) * energyMetric();
	  m_el_el_delta_alpha = KinematicUtils::DeltaAlpha(el, el_1);
	  m_el_el_mmc.fill(args.eventInfo, el, el_1, args.jets, args.met.reco); }

        m_el_el_invmass = -999.;
        m_el_el_invmass = KinematicUtils::InvariantMass({el->p4(), el_1->p4()}) * energyMetric();

        if (ph) {
            if ( el->p4().Pt()>el_1->p4().Pt() ) {
	      m_el_el_ph_mcoll = KinematicUtils::CollinearMass(el_1, el, ph, args.met.reco) * energyMetric(); }
            else {
	      m_el_el_ph_mcoll = KinematicUtils::CollinearMass(el, el_1, ph, args.met.reco) * energyMetric(); }
	    m_el_el_ph_mvis = KinematicUtils::VisibleMass(el, el_1, ph) * energyMetric();
        }
    }

    m_elelel_invmass = -999.;
    m_elelmu_invmass = -999.;
    m_elmumu_invmass = -999.;
    m_mumumu_invmass = -999.;
    if (el && el_1 && el_2) {
      m_elelel_invmass = KinematicUtils::InvariantMass({el->p4(), el_1->p4(), el_2->p4()}) * energyMetric();
    }
    if (el && el_1 && mu) {
      m_elelmu_invmass = KinematicUtils::InvariantMass({el->p4(), el_1->p4(), mu->p4()}) * energyMetric();
    }
    if (el && mu && mu_1) {
      m_elmumu_invmass = KinematicUtils::InvariantMass({el->p4(), mu->p4(), mu_1->p4()}) * energyMetric();
    }
    if (mu && mu_1 && mu_2) {
      m_mumumu_invmass = KinematicUtils::InvariantMass({mu->p4(), mu_1->p4(), mu_2->p4()}) * energyMetric();
    }


    // Some truth stuff
    if (args.eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
        if (tau) m_truth_tau_0.fill(tau);
        if (mu) m_truth_muon_0.fill(mu, args.truth.particles);
        if (el) m_truth_electron_0.fill(el, args.truth.particles);
    }

    if (args.eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
        m_theory_weights->fill(args.truth.event_info, m_theory_systs, args.eventInfo->mcChannelNumber());
    }

    // For signal and Z->tautau, find the truth Z pT
    // For signal, also write cos(tau polarisation angle) and tau spinner weights
    if (args.eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) &&
        (args.eventInfo->mcChannelNumber() == 303778 || args.eventInfo->mcChannelNumber() == 303779)) {
        // Z->tau+lep

        m_truth_Z_pT = -999.0;
        m_tau_cos_pol_angle = -999.0;

        m_tau_nu_pT = 99.0;
        m_mu_nu_pT  = -99.0;
        m_el_nu_pT  = -99.0;
        m_tau_nu_mu_nu_pT = -99.0;
        m_tau_nu_el_nu_pT = -99.0;
        m_mu_nu_pT_div_tau_nu_mu_nu_pT = -99.0;
        m_el_nu_pT_div_tau_nu_el_nu_pT = -99.0;

        // std::cout << "truth taus" << std::endl;
        const xAOD::TruthParticle *truth_tau = nullptr;
	if (args.truth.taus==nullptr) {
	  std::cout << "WARNING: No TruthTauContainer found (LFVLepSelection). " <<std::endl; // TODO should become a proper ATH_MSG_WARNING
	}
	else {
	  for (const auto &tau : *(args.truth.taus)) {
            //auto origin = tau->isAvailable<unsigned int>("classifierParticleOrigin")
            //                  ? tau->auxdata<unsigned int>("classifierParticleOrigin")
            //                  : 0;
            auto type = tau->isAvailable<unsigned int>("classifierParticleType")
	      ? tau->auxdata<unsigned int>("classifierParticleType")
	      : 0;

            if (type != 10) continue;  // ignore a non-isolated tau

            // std::cout << "pT = " << tau->pt() << " eta = " << tau->eta() << " parent = " << tau->parent(0) << " origin = " <<
            // origin << " type = " << type << std::endl;

            int i_tau = -1;
            int i_mu  = -1;
            int i_el  = -1;
            if (tau->nChildren() && tau->nChildren()>2 ) {  // in derivations, it is possible for tp's to be removed
              for (int i=0; i<tau->nChildren(); i++)
                if (tau->child(i) ) {
                  if      ( tau->child(i)->pdgId()==16 || tau->child(i)->pdgId()==-16 )  i_tau = i;
                  else if ( tau->child(i)->pdgId()==14 || tau->child(i)->pdgId()==-14 )  i_mu  = i;
                  else if ( tau->child(i)->pdgId()==12 || tau->child(i)->pdgId()==-12 )  i_el  = i;
                }
            }
            if (i_tau>=0) m_tau_nu_pT=tau->child(i_tau)->p4().Pt() * energyMetric();
            if (i_mu >=0) m_mu_nu_pT =tau->child(i_mu )->p4().Pt() * energyMetric();
            if (i_el >=0) m_el_nu_pT =tau->child(i_el )->p4().Pt() * energyMetric();
            if (i_tau>=0 && i_mu>0) {
              m_tau_nu_mu_nu_pT=( tau->child(i_tau)->p4()+tau->child(i_mu )->p4() ).Pt() * energyMetric();
              m_mu_nu_pT_div_tau_nu_mu_nu_pT=(tau->child(i_mu )->p4().Pt())/(( tau->child(i_tau)->p4()+tau->child(i_mu )->p4() ).Pt());
            }
            if (i_tau>=0 && i_el>0) {
              m_tau_nu_el_nu_pT=( tau->child(i_tau)->p4()+tau->child(i_el )->p4() ).Pt() * energyMetric();
              m_el_nu_pT_div_tau_nu_el_nu_pT=(tau->child(i_el )->p4().Pt())/(( tau->child(i_tau)->p4()+tau->child(i_el )->p4() ).Pt());
            }


            truth_tau = tau;
            break;  // can only have one!
	  }
	}

        const xAOD::TruthParticle *truth_lep = nullptr;
        if (args.eventInfo->mcChannelNumber() == 303778) {
            // Z->tau+mu
            // std::cout << "truth muons" << std::endl;
            for (const auto &muon : *(args.truth.muons)) {
                //auto origin = muon->isAvailable<unsigned int>("classifierParticleOrigin")
                //                  ? muon->auxdata<unsigned int>("classifierParticleOrigin")
                //                  : 0;
                auto type = muon->isAvailable<unsigned int>("classifierParticleType")
                                ? muon->auxdata<unsigned int>("classifierParticleType")
                                : 0;

                if (type != 6) continue;  // ignore a non-isolated muon

                // std::cout << "pT = " << muon->pt() << " eta = " << muon->eta() << " parent = " << muon->parent(0) << " origin =
                // " << origin << " type = " << type << std::endl;

                truth_lep = muon;
                break;  // can only have one!
            }
        } else {
            // Z->tau+e
            // std::cout << "truth electrons" << std::endl;
            for (const auto &electron : *(args.truth.electrons)) {
                //auto origin = electron->isAvailable<unsigned int>("classifierParticleOrigin")
                //                  ? electron->auxdata<unsigned int>("classifierParticleOrigin")
                //                  : 0;
                auto type = electron->isAvailable<unsigned int>("classifierParticleType")
                                ? electron->auxdata<unsigned int>("classifierParticleType")
                                : 0;

                if (type != 2) continue;  // ignore a non-isolated electron

                // std::cout << "pT = " << electron->pt() << " eta = " << electron->eta() << " parent = " << electron->parent(0)
                // << " origin = " << origin << " type = " << type << std::endl;

                truth_lep = electron;
                break;  // can only have one!
            }
        }

        if (truth_tau && truth_lep) {
            auto tau_vec = truth_tau->p4();
            auto Z_vec = tau_vec + truth_lep->p4();
            // std::cout << "Z pT = " << Z_vec.Pt() << std::endl;

            m_truth_Z_pT = Z_vec.Pt();

            // Subtract neutrinos p4 from the full p4 to get the visible p4
            auto tau_vis_vec = tau_vec;
            for (unsigned int i = 0; i < truth_tau->nChildren(); i++) {
                const auto child_pdgId = truth_tau->child(i)->absPdgId();
                if (child_pdgId == 12 || child_pdgId == 14 || child_pdgId == 16) {
                    tau_vis_vec -= truth_tau->child(i)->p4();
                }
            }

            tau_vis_vec.Boost(tau_vec.BoostVector() * (-1));
            tau_vec.Boost(Z_vec.BoostVector() * (-1));
            m_tau_cos_pol_angle = TMath::Cos(tau_vec.Angle(tau_vis_vec.Vect()));
        }

        // Execute TauSpinner tools to obtain tau spin weight and helicity
        m_tauSpinWeight = 1.0;
        m_tau_helicity = 0.0;

        // read in particles from MC block
        auto reader = args.TST_xAODEventReaderTool;
        auto smTool = args.TST_StandardModelTool;
        if (!reader->execute().isSuccess()) {
            throw std::runtime_error("Fail to execute TauSpinner's xAODEventReaderTool");
        }

        // read in the xAOD event with the TauSpinnerTool
        double tauSpinWeight = 1.0;
        smTool->getSpinWeight(tauSpinWeight);

        // helicity MUST BE CALLED AFTER getSpinWeight()!!!!
        double tau_helicity = smTool->getHelicity();
        //std::cout << Form("Event weight: %f, Helicity: %.1f", tauSpinWeight, tau_helicity) << std::endl;

        m_tauSpinWeight = tauSpinWeight;
        m_tau_helicity = tau_helicity;

    }
    else {
        m_tauSpinWeight = 1.0;
        m_tau_helicity = 0.0;
    }

    if (args.eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) &&
        (args.eventInfo->mcChannelNumber() == 364128 ||  // still low pT non-extra one, for testing
         args.eventInfo->mcChannelNumber() == 364137 ||  // higher pT from here
         args.eventInfo->mcChannelNumber() == 364138 || args.eventInfo->mcChannelNumber() == 364139 ||
         args.eventInfo->mcChannelNumber() == 364140 || args.eventInfo->mcChannelNumber() == 364141 ||
         args.eventInfo->mcChannelNumber() == 344772 ||  // lower pT slices with extra filters
         args.eventInfo->mcChannelNumber() == 344774 || args.eventInfo->mcChannelNumber() == 344776 ||
         args.eventInfo->mcChannelNumber() == 344778 || args.eventInfo->mcChannelNumber() == 344780 ||
         args.eventInfo->mcChannelNumber() == 344781)) {
        // Z->tau+tau, Sherpa

        m_truth_Z_pT = -999.0;

        // std::cout << "truth taus" << std::endl;
        const xAOD::TruthParticle *truth_tau_1 = nullptr;
        const xAOD::TruthParticle *truth_tau_2 = nullptr;
        int foundTaus = 0;
	if (args.truth.taus==nullptr) {
	  std::cout << "WARNING: No TruthTauContainer found (LFVLepSelection). " << std::endl; // TODO should become a proper ATH_MSG_WARNING
	}
	else {
	  for (const auto &tau : *(args.truth.taus)) {
            //auto origin = tau->isAvailable<unsigned int>("classifierParticleOrigin")
            //                  ? tau->auxdata<unsigned int>("classifierParticleOrigin")
            //                  : 0;
            auto type = tau->isAvailable<unsigned int>("classifierParticleType")
	      ? tau->auxdata<unsigned int>("classifierParticleType")
	      : 0;

            if (type != 10) continue;  // ignore a non-isolated tau

            // std::cout << "pT = " << tau->pt() << " eta = " << tau->eta() << " parent = " << tau->parent(0) << " origin = " <<
            // origin << " type = " << type << std::endl;

            ++foundTaus;
            if (foundTaus == 1) {
	      truth_tau_1 = tau;
            }
            if (foundTaus == 2) {
	      truth_tau_2 = tau;
	      break;  // can only have 2!
            }
	  }
	}

        if (truth_tau_1 && truth_tau_2) {
            auto Z_vec = truth_tau_1->p4() + truth_tau_2->p4();
            // std::cout << "Z pT = " << Z_vec.Pt() << std::endl;

            m_truth_Z_pT = Z_vec.Pt();
        }
    }

    return true;  //!< fills the event
}

void LFVLepSelection::FillDeferredBranches(const FillBranchesArgs &args) {
    if (args.eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
        const xAOD::TauJet *tau = args.taus->size() > 0 ? args.taus->at(0) : nullptr;
        const xAOD::Muon *muon = args.muons->size() > 0 ? args.muons->at(0) : nullptr;
        const xAOD::Electron *electron = args.electrons->size() > 0 ? args.electrons->at(0) : nullptr;

        const xAOD::Photon *ph_0 = args.photons->size() > 0 ? args.photons->at(0) : nullptr;
        const xAOD::Photon *ph_1 = args.photons->size() > 1 ? args.photons->at(1) : nullptr;

        const xAOD::Muon *mu_1 = args.muons->size() > 1 ? args.muons->at(1) : nullptr;
        const xAOD::Electron *el_1 = args.electrons->size() > 1 ? args.electrons->at(1) : nullptr;

        // Fill scale factors
        m_scale_factors_event.fill(args.eventInfo);
        m_scale_factors_tau_0.fill(tau);
        m_scale_factors_el_0.fill(electron);
        m_scale_factors_mu_0.fill(muon);
        m_scale_factors_el_1.fill(el_1);
        m_scale_factors_mu_1.fill(mu_1);
        m_scale_factors_ph_0.fill(ph_0);
        m_scale_factors_ph_1.fill(ph_1);
        m_scale_factors_global_jets.fill(args.jets); // TODO take out JVT SF
        m_scale_factors_global_muons.fill(args.eventInfo);
    }
}

const xAOD::IParticle *LFVLepSelection::SelectElectron(const FillBranchesArgs &args) {
    const xAOD::IParticle *elPart = 0;
    if (ContainerUtils::Size(args.electrons) > 0) {
        elPart = ContainerUtils::GetSelectedParticle<char>(args.electrons, {"signal"}, 0);
    }

    return elPart;
}

const xAOD::IParticle *LFVLepSelection::SelectMuon(const FillBranchesArgs &args) {
    const xAOD::IParticle *muPart = 0;
    if (ContainerUtils::Size(args.muons) > 0) {
        muPart = ContainerUtils::GetSelectedParticle<char>(args.muons, {"signal"}, 0);
    }

    return muPart;
}

const xAOD::TauJet *LFVLepSelection::SelectTau(const FillBranchesArgs &args) {
    const xAOD::TauJet *tauPart = 0;
    if (ContainerUtils::Size(args.taus) > 0) {
        tauPart = dynamic_cast<const xAOD::TauJet *>(ContainerUtils::GetSelectedParticle<char>(args.taus, {"signal"}, 0));
    }

    return tauPart;
}
