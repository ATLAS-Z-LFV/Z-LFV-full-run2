// vim: ts=4 sw=4
#include <LFV_Z/EventSelection.h>
#include "xAODRootAccess/TActiveStore.h"
#include "xAODRootAccess/tools/Message.h"

/* Helper for checking xAOD::TReturnCode return values */
#define CHECK(CONTEXT, EXP)                                              \
    do {                                                                 \
        if (!EXP.isSuccess()) {                                          \
            Error(CONTEXT, XAOD_MESSAGE("Failed to execute: %s"), #EXP); \
            return EL::StatusCode::FAILURE;                              \
        }                                                                \
    } while (false)
/* -----------------------------------------------------*/

//// DO NOT USE THIS METHOD -> SLOW!
//// Perform common checks for every event and store in the TStore
//bool EventSelection::setPreselectionVariables() {
//    xAOD::TStore* store = xAOD::TActiveStore::store();
//
//    ATH_MSG_DEBUG("setPreselectionVariables(): setting GRL, DQ, primary vtx and MC veto variables");
//
//    // Check for bad DQ flags
//    bool* badDetectorQuality = new bool;
//    *badDetectorQuality = false;
//    if (this->hasBadDetectorQuality()) {
//        *badDetectorQuality = true;
//    }
//    CHECK("setPreselectionVariables()", store->record<bool>(badDetectorQuality, "badDetectorQuality"));
//
//    // Check for non-collision background
//    bool* NCBEventFlag = new bool;
//    *NCBEventFlag = isNonCollisionBackgroundEvent();
//    CHECK("setPreselectionVariables()", store->record<bool>(NCBEventFlag, "NCBEventFlag"));
//
//    // GRL passed?
//    bool* passGRL = new bool;
//    *passGRL = this->passGRL();
//    CHECK("setPreselectionVariables()", store->record<bool>(passGRL, "passGRL"));
//
//    // Primary vertex found?
//    bool* hasPrimaryVertex = new bool;
//    *hasPrimaryVertex = primaryVertexCheck();
//    CHECK("setPreselectionVariables()", store->record<bool>(hasPrimaryVertex, "hasPrimaryVertex"));
//
//    // TODO: might need common flag for MC in case of overlapping events
//    bool* mcAccept = new bool;
//    *mcAccept = true;
//    CHECK("setPreselectionVariables()", store->record<bool>(mcAccept, "mcAccept"));
//
//    return true;
//}

bool EventSelection::isSimulation() {
    if (m_eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
        return true;
    }

    return false;
}

bool EventSelection::isData() {
    return not isSimulation();
}

bool EventSelection::primaryVertexCheck() {
    bool retval = false;
    if (not m_primaryVertexFound) {
        findPrimaryVertex();
    }
    if (m_primaryVertex && m_primaryVertex->nTrackParticles() >= 2) {
        retval = true;
    }
    ATH_MSG_DEBUG("primaryVertexCheck(): vertex OK? " << retval);
    return retval;
}

void EventSelection::findPrimaryVertex() {
    m_primaryVertexFound = true;
    m_primaryVertex = m_SUSYTools->GetPrimVtx();
}

bool EventSelection::hasBadDetectorQuality() {
    if (isSimulation()) {
        ATH_MSG_DEBUG("badDetectorQuality(): running on MC - event OK!");
        return false;
    }

    if (lArErrorCheck()) return true;
    if (tileErrorCheck()) return true;
    if (SCTErrorCheck()) return true;
    if (coreFlagCheck()) return true;
    if (tileTripCheck()) return true;

    ATH_MSG_DEBUG("badDetectorQuality(): running on data - event passed checks OK!");

    return false;
}

bool EventSelection::eventCheck() {
    if (isSimulation()) {
        ATH_MSG_DEBUG("eventCheck(): running on MC: event OK!");
        return true;
    }

    bool retval = true;

    ATH_MSG_DEBUG("eventCheck(): true");
    if (hasBadDetectorQuality()) {
        ATH_MSG_DEBUG("eventCheck(): bad DQ -> false");
        retval = false;
    }

    if (isNonCollisionBackgroundEvent()) {
        ATH_MSG_DEBUG("eventCheck(): failed NCB check -> false");
        retval = false;
    }

    ATH_MSG_DEBUG("eventCheck(): running on data: passed all event checks? -> " << retval);

    return retval;
}

bool EventSelection::isNonCollisionBackgroundEvent() {
    if (isSimulation()) {
        ATH_MSG_DEBUG("isNonCollisionBackgroundEvent(): running on MC - event OK!");
        return false;
    }

    bool NCBEventFlag = false;
    if (m_eventInfo->runNumber() >= 279932) {
        NCBEventFlag = m_eventInfo->isEventFlagBitSet(xAOD::EventInfo::Background, xAOD::EventInfo::HaloMuonTwoSided);
    }

    ATH_MSG_DEBUG("isNonCollisionBackgroundEvent(): running on data - NCBEventFlag = " << NCBEventFlag);

    return NCBEventFlag;
}

bool EventSelection::lArErrorCheck() {
    bool retval = false;
    if (m_eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) {
        retval = true;
    }
    ATH_MSG_DEBUG("LAr check = " << retval);
    return retval;
}

bool EventSelection::SCTErrorCheck() {
    bool retval = false;
    if (m_eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) {
        retval = true;
    }
    ATH_MSG_DEBUG("SCT error check = " << retval);
    return retval;
}

bool EventSelection::tileErrorCheck() {
    bool retval = false;
    if (m_eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) {
        retval = true;
    }
    ATH_MSG_DEBUG("Tile check = " << retval);
    return retval;
}

bool EventSelection::coreFlagCheck() {
    bool retval = false;
    if ((m_eventInfo->eventFlags(xAOD::EventInfo::Core) & 0x40000)) {
        retval = true;
    }
    ATH_MSG_DEBUG("CoreFlag check = " << retval);
    return retval;
}

bool EventSelection::tileTripCheck() {
    // TODO: to be updated
    bool retval = false;
    ATH_MSG_DEBUG("TileTrip check = " << retval);
    return retval;
}

bool EventSelection::passGRL() {
    if (isSimulation()) {
        ATH_MSG_DEBUG("passGRL(): event is simulation");
        return true;
    }

    bool retval = true;
    if (m_GRL) {
        retval = m_GRL->passRunLB(m_eventInfo->runNumber(), m_eventInfo->lumiBlock());
        ATH_MSG_DEBUG("passGRL(): passGRL = " << retval);
    } else {
        ATH_MSG_ERROR("passGRL(): no GRL tool available! returning true.");
        return true;
    }
    return retval;
}
