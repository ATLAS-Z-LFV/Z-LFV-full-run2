#!/usr/bin/env python

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True

import argparse
import getpass
import json
import os
import shutil
import socket
import subprocess
import sys
import time
import uuid

import ConfigParser

#ROOT.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')

# if not ROOT.xAOD.Init().isSuccess():
#     raise RuntimeError

def is_subdir(path, directory):
    path = os.path.realpath(path)
    directory = os.path.realpath(directory)
    relative = os.path.relpath(path, directory)
    return not relative.startswith(os.pardir + os.sep)

def modeToConfigFilename(mode):
    # Maps options for --mode to config filenames
    modeMap = {}
    modeMap["LFV"] = "config-LFV.ini"
    modeMap["LFVHad"] = "config-LFV.ini"
    modeMap["LFVLep"] = "config-LFVLep.ini"

    if mode in modeMap:
        return modeMap[mode]

    filename = "config-{0}.ini".format(mode)
    print("WARNING: could not file mode {0} in known modes. Guessing filename as {1}".format(mode, filename))
    return filename

def setConfiguration(obj, config, name=""):
    if name == "":
        name = obj.GetName()

    for k in config[name]:
        v = config[name][k]
        try:
            setattr(obj, "m_{0}".format(k), v)
            print("{0}: setting '{1}' to '{2}'".format(name, k, v))
        except:
            print("Could not set atttribute {0} to {1}".format(k, v))
            sys.exit()

def getFullConfigPath(filename):
    workdir = os.getenv("WorkDir_DIR")
    return os.path.join(workdir, "data/LFV_Z", filename)

def getConfig(filename):
    fname = getFullConfigPath(filename)
    if not os.path.exists(fname):
        print("Can't find configuration '{0}'".format(fname))
        sys.exit()

    config = ConfigParser.RawConfigParser()
    config.optionxform = str # we prefer case-sensitive options
    config.read(fname)

    return config

def parseConfig(filename):
    config = getConfig(filename)

    data = {}
    for section in config.sections():
        data[section] = dict(config.items(section))
        for k in data[section]:
            if data[section][k] == "True":
                data[section][k] = True
            elif data[section][k] == "False":
                data[section][k] = False
            elif data[section][k].isdigit():
                data[section][k] = int(data[section][k])

    return data

def printArgs(args):
    fmt = "{0:<40}: {1}"
    print "="*80
    print "Configured running with the following flags:"
    print "="*80
    print "-"*80
    print "General settings"
    print "-"*80
    print fmt.format('Configuration file', args.config)
    print fmt.format('MC campaign', args.MC_campaign)
    print fmt.format('Verbose', args.verbose)
    print fmt.format('Submit dir', args.submitDir)
    print fmt.format('Output name', args.outName)
    print fmt.format('Max events', args.maxEvents)
    print fmt.format('Skip events', args.skipEvents)
    print fmt.format('No systematics override', args.noSystematics)
    print fmt.format('Print cut flow', args.printCutflow)
    print "-"*80
    print "Input settings"
    print "-"*80
    print fmt.format('Athena access mode?', args.athenaMode)
    print fmt.format('Sample handling mode', args.sampleHandlingMode)
    print fmt.format('Local path', args.localPath)
    print fmt.format('Local sample', args.localSample)
    print fmt.format('Sample pattern', args.samplePattern)
    print "-"*80
    print "Grid settings"
    print "-"*80
    print fmt.format('Use grid', args.useGrid)
    print fmt.format('Dry run?', args.dryRun)
    print fmt.format('Input dataset', args.inputDS)
    print fmt.format('Nickname', args.nickname)
    print fmt.format('Output suffix', args.outputSuffix)
    print fmt.format('Short generator+process name', args.shortName)
    print fmt.format('Max files per job', args.maxFilesPerJob)
    print fmt.format('Merge output', args.mergeOutput)
    print "="*80
    print ""

def printConfig(config):
    fmt = "{0:<40}: {1}"
    print "="*80
    print "Loaded the following settings from the configuration file and the arguments:"
    print "="*80
    for section in config:
        print "-"*80
        print section
        print "-"*80
        for k in config[section]:
            v = config[section][k]
            if v == "": v = "<empty string>"
            print fmt.format(k, v)
    print "="*80
    print ""

def updateConfigFile(args):
    # Need to copy our config file to config.ini for the grid to work
    updatedConfig = getConfig(args.config)

    if args.outputTreePrefix:
        prefix = args.outputTreePrefix
        if not prefix.endswith("_"):
            prefix += "_"

        updatedConfig.set("Global", "outputTreePrefix", prefix)

    if args.outName:
        updatedConfig.set("Global", "outputName", args.outName)

    if args.noSystematics:
        updatedConfig.set("Global", "doSystematics", False)

    if args.data:
        updatedConfig.set("Global", "isData", True)
        updatedConfig.set("Global", "doSystematics", False) # never do systematics for data

    if not args.MC_campaign == "comb":
        PRW_files = updatedConfig.get("Global", "PileupReweightingMC").split()
        lumi_files = updatedConfig.get("Global", "PileupReweightingLumiCalc").split()
        if args.MC_campaign == "16a":
            PRW_files = [f for f in PRW_files if "16a" in f]
            lumi_files = [f for f in lumi_files if "data15" in f or "data16" in f ]
        elif args.MC_campaign == "16d":
            PRW_files = [f for f in PRW_files if "16d" in f or "data17" in f]
            lumi_files = [f for f in lumi_files if "data17" in f ]
        elif args.MC_campaign == "16e":
            PRW_files = [f for f in PRW_files if "16e" in f or "data18" in f]
            lumi_files = [f for f in lumi_files if "data18" in f ]
        PRW_files = " ".join(PRW_files)
        lumi_files = " ".join(lumi_files)
        updatedConfig.set("Global", "PileupReweightingMC", PRW_files)
        updatedConfig.set("Global", "PileupReweightingLumiCalc", lumi_files)

    with open(getFullConfigPath("config.ini"), "w") as f:
        updatedConfig.write(f)

    return

def findUserName():
    for var_name in ['CERN_USER', 'CERNUSER', 'LOGNAME', 'USER']:
        if var_name in os.environ:
            return os.getenv(var_name)
    return getpass.getuser()

#--------------------------------------------------------------------------------
if __name__ == "__main__":

    if not "WorkDir_DIR" in os.environ and not "ROOTCOREBIN" in os.environ:
        print "This won't work! Did you 'asetup'? (or source rcSetup.sh if you are using r20.7?)"
        sys.exit(-1)

    currentDatetime = time.strftime("%y%m%d_%H%M")

    parser = argparse.ArgumentParser()
    parser.add_argument("--mode", choices=["LFV","LFVHad","LFVLep"], type=str, help="Choose a mode from 'LFV'(='LFVHad') or 'LFVLep')", default="LFV")
    parser.add_argument("--verbose", "-V", default=False, action="store_true", help="Verbose output (default false)")
    parser.add_argument("--submitDir", metavar='PATH', default=None, type=str, help="Output directory. A timestamp + uuid is used by default.")
    parser.add_argument("--maxEvents", "-m", metavar='N', default=0, type=int, help="Max # events to process (if 0, process all)")
    parser.add_argument("--skipEvents", metavar='N', default=0, type=int, help="Skip # events")
    parser.add_argument("--outputTreePrefix", metavar='PREFIX', default=None, type=str, help="Prefix for output tree")
    parser.add_argument("--noSystematics", default=False, action='store_true', help="Run without systematics; override config file (default false)")
    parser.add_argument("--data", default=False, action='store_true', help="Is data? (default false)")
    parser.add_argument("--MC-campaign", type=str, default='comb', choices=['comb','16a','16d','16e'], help="MC campaign, choose among '16a', '16d', '16e' and 'comb' (default 'comb')")
    parser.add_argument("--printCutflow", default=False, action='store_true', help="Print cutflow at end (default false)")
    defaultTreenamesDB = "../data/treenames.json"
    parser.add_argument("--treenamesDB", help="JSON filename with map of pattern => tree name (default='{0}')".format(defaultTreenamesDB), default=defaultTreenamesDB)

    parser_inputs = parser.add_argument_group('Input settings')
    parser_inputs.add_argument("--athenaMode", default=False, action="store_true", help="set xAOD access mode to athena - needed for 20.7 AOD (not DAOD)")
    parser_inputs.add_argument("--sampleHandlingMode", type=str, default=None, choices=["readFileList", "scanDQ2", "addGrid", "scanDir"], help="defines sample handling mode; default is readFileList (local run) or addGrid (Grid run)")
    parser_inputs.add_argument("--localPath",  metavar='PATH', default=None, type=str, help="local input path")
    parser_inputs.add_argument("--localSample",  metavar='FILE', default=None, type=str, help="local input file, full path")
    parser_inputs.add_argument("--samplePattern",  metavar='PATTERN', default="*.root*", type=str, help="local sample pattern; default to match is *.root*")
    parser_inputs.add_argument("--scanDepth",  metavar='DEPTH', default=0, type=int, help="Scan depth when using the 'scanDir' sample handling mode.")

    parser_proof = parser.add_argument_group('PROOF settings')
    parser_proof.add_argument("--useProof", default=False, action='store_true', help="Use PROOF-lite (default false)")
    parser_proof.add_argument("--nCPUs", metavar='N', default=4, type=int, help="Processes to use for PROOF-lite (default 4)")

    parser_grid = parser.add_argument_group('Grid settings')
    parser_grid.add_argument("--useGrid", default=False, action='store_true', help="Submit to grid (default false)")
    parser_grid.add_argument("--excludedSite",  metavar='SITE', default=None, type=str, help="Grid sites to exclude")
    parser_grid.add_argument("--site",  metavar='SITE', default=None, type=str, help="Force job to run at site")
    parser_grid.add_argument("--dryRun", default=False, action='store_true', help="Dry run")
    parser_grid.add_argument("--disableAutoRetry", default=False, action='store_true', help="Pass --disableAutoRetry to prun")
    parser_grid.add_argument("--noBuild", default=False, action='store_true', help="Submit binaries (don't use unless debugging!)")
    parser_grid.add_argument("--inputDS",  metavar='DATASET', default=None, type=str, help="input dataset")
    parser_grid.add_argument("--inputDSFile",  dest="inputDSFiles", action="append", metavar='FILE', type=str, help="file to read input DS from")
    parser_grid.add_argument("--nickname",  metavar='NAME', default=findUserName(), type=str, help="grid nickname (default {0})".format(findUserName()))
    parser_grid.add_argument("--projectName",  metavar='NAME', default="LFVZ", type=str, help="project name to write in output DS name")
    parser_grid.add_argument("--outputSuffix", metavar='SUFFIX', default=currentDatetime, type=str, help="suffix for output DS (default the current date and time: {0})".format(currentDatetime))
    parser_grid.add_argument("--shortName", default=None, type=str, help="Short generator+process name")
    parser_grid.add_argument("--destSE", metavar="SITE", type=str, default=None, help="Destination site for grid output (e.g. UNICPH-NBI_LOCALGROUPDISK)")
    parser_grid.add_argument("--nFiles", metavar='N', default=0, type=int, help="Pick # files from dataset (default 0 = all)")
    parser_grid.add_argument("--numEventsPerJob", metavar='N', default=-1, type=int, help="Num events per job (default 100k(no systmatics)/30k(with systematics))")
    parser_grid.add_argument("--maxFilesPerJob", metavar='N', default=0, type=int, help="Max # files per job, only effective when numEventsPerJob=0 (default 0 = use as few jobs as possible)")
    parser_grid.add_argument("--numFilesPerJob", metavar='N', default=0, type=int, help="Num files per job, only effective when numEventsPerJob=0 (default 0 = use as few jobs as possible)")
    parser_grid.add_argument("--mergeOutput", default=False, action='store_true', help="Merge output (default false)")
    parser_grid.add_argument("--resubmission", default=False, action='store_true', help="It is a resubmission of an existing Grid job.")
    parser_grid.add_argument("--outName", metavar='FILE', default="ntuple", type=str, help="Output name (default outName)")

    parser_expert_grid = parser.add_argument_group('Expert grid settings')
    parser_expert_grid.add_argument("--writeTarball", default=False, action='store_true', help="Write to tarball")
    parser_expert_grid.add_argument("--readTarball", default=False, action='store_true', help="Read from tarball")
    parser_expert_grid.add_argument("--tarballFilename",  metavar='NAME', default="/tmp/{0}/prun-LFVZ-{0}.tar.gz".format(getpass.getuser()), type=str, help="input dataset")

    args = parser.parse_args()

    # What mode are we using?
    if not args.mode:
        print("ERROR: option --mode not set!")
        sys.exit()

    args.config = modeToConfigFilename(args.mode)

    # Can't use PROOF *and* grid
    if args.useGrid and args.useProof:
       print("ERROR: --useGrid and --useProof can't be used at the same time!")
       sys.exit()

    # Catch clever users
    if args.useProof and args.nCPUs < 1:
        print("ERROR: trying to be clever? --useProof requires at least 1 CPU...")
        sys.exit()

    # Require a suffix for the grid
    if args.useGrid and (args.outputSuffix is None or args.outputSuffix == ""):
        print("ERROR: --useGrid requires an output suffix")
        sys.exit()

    # Pedantic check
    if args.useGrid and args.writeTarball and args.readTarball:
        print("ERROR: can't read and write tarball at the same time")
        sys.exit(0)

    if args.useGrid and args.readTarball and not os.path.exists(args.tarballFilename):
        print("ERROR: can't find tarball {0}".format(args.tarballFilename))
        sys.exit(0)

    if args.useGrid and is_subdir(args.submitDir, os.path.join(os.getenv("WorkDir_DIR"), "..")):
        print("ERROR: Your submitDir is a subdirectory of the build directory. This is _extremely_ unadvisable as you would mess up the submission process with larger and larger tarballs.")
        print("Exiting instead.")
        sys.exit()

    if args.useGrid and args.writeTarball and not os.path.exists(os.path.dirname(args.tarballFilename)):
        try:
            os.makedirs(os.path.dirname(args.tarballFilename))
        except:
            print("ERROR: can't find dir {0} to write tarball and could not mkdir it".format(os.path.dirname(args.tarballFilename)))
            sys.exit(0)

    # Check sample handling mode
    if args.sampleHandlingMode is None:
        # Use default
        args.sampleHandlingMode = "addGrid" if args.useGrid else "readFileList"
    elif args.useGrid and not (args.sampleHandlingMode == "scanDQ2" or args.sampleHandlingMode == "addGrid"):
        print("ERROR: --useGrid requires either scanDQ2 or addGrid for --sampleHandlingMode")
        sys.exit()
    elif not args.useGrid and (args.sampleHandlingMode == "scanDQ2" or args.sampleHandlingMode == "addGrid"):
        print("ERROR: running locally; cannot use scanDQ2 or addGrid as --sampleHandlingMode")
        sys.exit()

    # Load files from inputDSFile if asked
    if args.inputDSFiles:
        args.inputDS = ""
        inputs = set()
        for filename in args.inputDSFiles:
            if not os.path.exists(filename) or not os.path.isfile(filename): continue
            with open(filename) as f:
                for L in f.readlines():
                    L = L.strip().strip('/')
                    if L and not L.startswith('#'):
                        inputs.add(L)
        inputs = list(inputs)
        print("Loaded {0} input DS names from the {1} text files".format(len(inputs), len(args.inputDSFiles)))
        args.inputDS += ",".join(inputs)

    # Using the correct input mode for grid?
    if args.useGrid and args.sampleHandlingMode == "addGrid" and "*" in args.inputDS:
        print("ERROR: can't have a wildcard for --sampleHandlingMode=addGrid! It requires the full DS name. Use scanDQ2 instead.")
        sys.exit()

    # Check inputs
    if args.useGrid and args.inputDS is None or args.inputDS == "" or args.inputDS == []:
        print("ERROR: --useGrid requires --inputDS")
        sys.exit()
    elif not args.useGrid and (args.localPath == "" or args.localPath is None) and (args.localSample == "" or args.localSample is None):
        print("ERROR: running locally, but no sample specified; use --localPath or --localSample")
        sys.exit()

    # Check if you're not being an idiot
    if args.useGrid:
        if not args.data and any(x.startswith("data") for x in args.inputDS.split(",")):
            print("ERROR: you did not pass --data but there are inputDS called data*")
            sys.exit()
        if args.data and any(x.startswith("mc") for x in args.inputDS.split(",")):
            print("ERROR: you passed --data but there are inputDS called mc*")
            sys.exit()

        if args.data and args.mergeOutput:
            print("INFO: forcing --mergeOutput to false for data - this takes _much_ more time on the grid than locally, as it's a separate set of jobs and data files are small anyway.")
            print("Waiting 3 seconds...")
            time.sleep(3)
            args.mergeOutput = False

        if args.mergeOutput:
            print("WARNING: are you sure you want to run with mergeOutput? It can take a long time!")
            print("Waiting 5 seconds...")
            time.sleep(5)

    # Load pandatools if on grid
    if args.useGrid:
        try:
            from pandatools import PandaToolsPkgInfo
        except:
            print "ERROR: Cannot find pandatools packages! Did you run lsetup panda?"
            sys.exit()

    # Sanitise submitDir
    if args.submitDir is None or args.submitDir == "":
        args.submitDir =  'run_{0}_{1}'.format(currentDatetime, uuid.uuid4().hex)
    elif os.path.exists(args.submitDir):
        print("ERROR: submitDir {0} already exists!".format(args.submitDir))
        sys.exit()

    # Does the input end in / ?
    if args.inputDS is not None and "/" in args.inputDS:
        args.inputDS = args.inputDS.replace("/", "")

    ## Some sanity checks for grid running
    if args.useGrid:
        p = subprocess.Popen('./require_clean_work_tree.sh "submit grid job"', shell=True)
        p.communicate()
        if p.returncode != 0:
            sys.exit()

    # Never do systematics for data
    if args.data:
        args.noSystematics = True

    # Set default numEventsPerJob
    if args.numEventsPerJob == -1:
        args.numEventsPerJob = 100000 if args.noSystematics else 30000

    # # Secretly make clever choices for the user
    # # Disable auto retry for MC jobs with systematics
    # # because cycles will take too long
    # if args.noSystematics == False:
    #     args.disableAutoRetry = True

    # Show everything
    printArgs(args)

    # Load config
    config = parseConfig(args.config)
    updateConfigFile(args)

    # Load the generated config.ini
    printConfig(parseConfig('config.ini'))

    # Configure the handler based on the mode
    sh = ROOT.SH.SampleHandler()
    if args.sampleHandlingMode == "scanDQ2":
        ROOT.SH.scanDQ2(sh, args.inputDS)
    elif args.sampleHandlingMode == "addGrid":
        for x in args.inputDS.split(","):
            ROOT.SH.addGrid(sh, x)
        sh.setMetaString("nc_grid_filter", args.samplePattern)
    elif args.sampleHandlingMode == "readFileList":
        #ROOT.SH.readFileList(sh, "sample", os.path.abspath(args.localPath))
        with open(os.path.abspath(args.localPath)) as f:
            filenames = [x.strip() for x in f.readlines() if not x.strip().startswith('#')]
            if len(filenames) == 0:
                print("ERROR: your input file is empty!")
                sys.exit()
            DSname = filenames[0].split("/")[-2] # last is the file, next-to-last is dataset... hopefully
            # now emulate the behaviour of --addNthFieldOfInDSToLFN=2,3 by dumping this into the name. (Note: that counts from 1, not 0.)
            DSnameSplit = DSname.split(".")
            if len(DSnameSplit) > 2:
                sampleName = ".".join(DSnameSplit[1:3])
            else:
                sampleName = DSname
        ROOT.SH.readFileList(sh, sampleName, os.path.abspath(args.localPath))
    elif args.sampleHandlingMode == "scanDir":
        dirname = os.path.abspath(args.localPath)
        filelist = ROOT.SH.DiskListLocal(dirname)
        ROOT.SH.ScanDir().sampleDepth(args.scanDepth).filePattern(args.samplePattern).scan(sh, filelist)

    # Load a bunch of tree name patterns from a json file, if given
    treenames = {}
    if os.path.exists(args.treenamesDB) and os.path.exists(args.treenamesDB):
        print("Loading tree names from {0}".format(os.path.abspath(args.treenamesDB)))
        with open(os.path.abspath(args.treenamesDB)) as f:
            treenames = json.load(f)
        print("Loaded {0:d} names from file".format(len(treenames)))
    else:
        print("Tree names DB {0} doesn't exist - ignoring".format(args.treenamesDB))

    # Pass them on to the sample handler
    sh.setMetaString(".*data.*", "treePrefix", "data") # data tree starts with 'data'
    sh.setMetaString(".*physics_Main.*", "treePrefix", "data") # data tree starts with 'data'
    for pattern in treenames:
        name = treenames[pattern]
        sh.setMetaString (".*{0}.*".format(pattern), "treePrefix", "{0}".format(name))

    sh.setMetaString ("nc_tree", "CollectionTree")
    sh.printContent()

    # Fire up the algorithm
    eventSelection = ROOT.EventSelection()
    eventSelection.SetName('EventSelection')
    eventSelection.setConfigFile('config.ini')

    # Pass some submission information along
    user_host = "{0}@{1}".format(getpass.getuser(), socket.getfqdn())
    date = time.strftime("%d/%m/%Y %H:%M:%S", time.localtime())
    try:
        revision = subprocess.check_output(['git', 'describe', '--always', '--tags']).strip()
    except subprocess.CalledProcessError:
        print("WARNING: could not identify git revision (`git describe --always --tags` failed)")
        print("WARNING: could not identify git revision (`git describe --always --tags` failed)")
        revision = "unknown"

    print("Will write the following metadata to the output files:" )
    print("User: {0}".format(user_host))
    print("Date: {0}".format(date))
    print("Rev:  {0}".format(revision))

    eventSelection.m_submitted_at = date
    eventSelection.m_submitted_by = user_host
    eventSelection.m_submitted_revision = revision
    eventSelection.m_submitted_type = "local" if not args.useGrid else "grid"

    # Per-class configuration
    setConfiguration(eventSelection, config, "EventSelection")

    if args.verbose:
        eventSelection.setVerbose()

    # Setup the EventLoop Job
    job = ROOT.EL.Job()
    job.useXAOD()
    job.sampleHandler(sh)
    job.algsAdd(eventSelection)

    if args.maxEvents > 0:
        job.options().setDouble(ROOT.EL.Job.optMaxEvents, args.maxEvents)

    if args.skipEvents > 0:
        job.options().setDouble(ROOT.EL.Job.optSkipEvents, args.skipEvents)

    job.options().setDouble(ROOT.EL.Job.optCacheSize, 20*1024*1024)
    job.options().setDouble(ROOT.EL.Job.optCacheLearnEntries, 20)
    if args.athenaMode:
        job.options().setString(ROOT.EL.Job.optXaodAccessMode, ROOT.EL.Job.optXaodAccessMode_athena)

    # run, run, run!
    if args.useGrid:
        driver = ROOT.EL.PrunDriver()

        suffix = ""
        if args.outputSuffix != "":
            suffix = ".{0}".format(args.outputSuffix)

        singleDS = args.inputDS.split(",")[0]

        project = singleDS.split(".")[0]
        ID = singleDS.split(".")[1]
        tags = singleDS.split(".")[5]
        generatorAndProcess = singleDS.split(".")[2]

        # prun counts name[] indices from 1 and not 0!
        if not "data" in args.inputDS:
            if args.shortName:
                outDSName = "user.{0}.{1}.%in:name[2]%.{3}.%in:name[6]%{2}".format(args.nickname, args.projectName, suffix, args.shortName)
            else:
                outDSName = "user.{0}.{1}.%in:name[2]%.%in:name[3]%.%in:name[6]%{2}".format(args.nickname, args.projectName, suffix)
        else:
            outDSName = "user.{0}.{1}.%in:name[1]%.%in:name[2]%.%in:name[3]%.%in:name[6]%{2}".format(args.nickname, args.projectName, suffix)

        print "Using outputDS = {0}".format(outDSName)
        outDSGuess = outDSName.replace("%in:name[2]%", ID).replace("%in:name[6]%", tags).replace("%in:name[3]%", generatorAndProcess).replace("%in:name[1]%", project)
        print "(Guessing the output name for the first inputDS to become: {0} )".format(outDSGuess)

        if args.destSE is not None and args.destSE != "":
            print("Setting destSE to {0}".format(args.destSE))
            driver.options().setString(ROOT.EL.Job.optGridDestSE, args.destSE)

        extraOpts = ["--excludeFile=*/.svn/*,.git,LFV_Z/scripts/run_*,LFV_Z/scripts/testfiles_*"]
        extraOpts += ["--useContElementBoundary --addNthFieldOfInDSToLFN=2,3"] # add DSID and name to sample name
        if args.noBuild:
            extraOpts += ["--disableAutoRetry --noCompile"]

        if args.maxEvents > 0:
            extraOpts += ["--nEvents={0} ".format(args.maxEvents)]

        if args.numEventsPerJob > 0:
            extraOpts += ["--nEventsPerJob={0} ".format(args.numEventsPerJob)]

        if args.disableAutoRetry:
            extraOpts += ["--disableAutoRetry"]

        if args.readTarball:
            print "Will read tarball from {0}".format(args.tarballFilename)
            extraOpts += ["--inTarBall={0}".format(args.tarballFilename)]
        elif args.writeTarball:
            print "Will write tarball to {0}".format(args.tarballFilename)
            extraOpts += ["--outTarBall={0}".format(args.tarballFilename)]

        if args.resubmission:
            extraOpts += ["--useNewCode"]

        if extraOpts != []:
            job.options().setString(ROOT.EL.Job.optSubmitFlags, " ".join(extraOpts))

        if args.excludedSite:
            job.options().setString(ROOT.EL.Job.optGridExcludedSite, args.excludedSite)

        if args.site:
            job.options().setString(ROOT.EL.Job.optGridSite, args.site)

        driver.options().setDouble("nc_showCmd", 1)
        driver.options().setString("nc_outputSampleName", outDSName)
        if args.numEventsPerJob:
            driver.options().setString(ROOT.EL.Job.optGridNGBPerJob, "")
            #driver.options().setDouble(ROOT.EL.Job.optGridMaxNFilesPerJob, 1)
        elif args.maxFilesPerJob == 0 and args.numFilesPerJob == 0:
            driver.options().setString(ROOT.EL.Job.optGridNGBPerJob, "MAX")
        else:
            if args.maxFilesPerJob > 0:
                driver.options().setDouble(ROOT.EL.Job.optGridMaxNFilesPerJob, args.maxFilesPerJob)
            if args.numFilesPerJob > 0:
                driver.options().setString(ROOT.EL.Job.optGridNFilesPerJob, str(args.numFilesPerJob))

        if args.nFiles > 0:
            driver.options().setDouble(ROOT.EL.Job.optGridNFiles, args.nFiles)

        driver.options().setDouble(ROOT.EL.Job.optGridExpress,1)
        driver.options().setDouble(ROOT.EL.Job.optGridMergeOutput, args.mergeOutput)

        driver.options().setDouble("nc_noSubmit", args.dryRun)

        driver.submitOnly(job, args.submitDir)
    elif args.useProof:
        driver = ROOT.EL.ProofDriver()
        driver.numWorkers = args.nCPUs
        print("PROOF: using {0} workers".format(args.nCPUs))

        job.options().setDouble(ROOT.EL.Job.optPerfTree, 1)
        driver.submit(job, args.submitDir)
    else:
        driver = ROOT.EL.DirectDriver()
        driver.submit(job, args.submitDir)

