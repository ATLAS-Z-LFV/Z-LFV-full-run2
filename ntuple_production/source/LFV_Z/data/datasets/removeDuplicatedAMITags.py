#!/usr/bin/env python

import sys

files = sys.argv[1:]

for file in files:
    with open(file) as f:
        original_DSs = f.readlines()
    
    contains_data = False
    new_DSs = []
    for original_DS in original_DSs:
        if original_DS.startswith("data"):
            contains_data = True
            break
        name = '.'.join(original_DS.split('.')[:-1])
        original_tags = original_DS.split('.')[-1].split('_')
        new_tags = []
        for tag in original_tags:
            if not tag[0] in [t[0] for t in new_tags]:
                new_tags.append(tag)
        new_DSs.append(name + '.' + '_'.join(new_tags))
    
    if contains_data:
        print "WARNING: file '{}' contains data, will skip this file!".format(file)
        continue
    
    with open(file, 'w') as f:
        f.writelines(new_DSs)
