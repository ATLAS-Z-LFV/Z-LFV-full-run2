#!/bin/sh

if [ $# -eq 0 ] ; then
  echo 'Please give me txt files containing dataset names as argument(s)!';
  exit 1
fi

# lsetup pyami
( hash ami &> /dev/null ) || { echo 'Cannot find ami, did you "lsetup pyami"?' ; exit 2 ; }

# lsetup emi
( hash grid-proxy-info &> /dev/null ) && grid-proxy-info -e -v 1:0 || { echo 'Cannot find valid proxy (or the proxy has less than 1 hour remaining), did you "lsetup emi" and/or "voms-proxy-init --voms atlas"?' ; exit 3 ; }

N_total=0
N_ok=0
N_fail=0
N_similar=0
for f in "${@:1}"; do
  while read DS || [[ -n "$DS" ]] ; do
    if [[ $DS != mc* ]] && [[ $DS != data* ]] ; then
      continue
    fi
    
    N_total=$((N_total+1))
    echo "============ $(printf "%03d" $N_total) ============"
    
    FOUND=`ami list datasets $DS`
    if [[ -z $FOUND ]] ; then
      echo "Cannot find $DS" ;
      N_fail=$((N_fail+1))
      
      SIMILAR=`ami list datasets ${DS%.*}%` ;
      if [[ -z $SIMILAR ]] ; then
        echo 'No simlar datasets found... :(' ;
      else
        echo "Found some candidate datasets with different AMI tags:"
        echo $SIMILAR | tr ' ' '\n' 
        N_similar=$((N_similar+1))
      fi
      
    else
      N_ok=$((N_ok+1))
      
    fi
  done < "$f"
done

echo '============================='
echo "Checked $N_total datasets"
echo "$N_ok found"
echo "$N_fail not found"
if [ $N_fail -eq 0 ] ; then
  echo "All good! YAY~"
else
  echo "$N_similar of them have alternative datasets with similar names"
fi
echo ""
exit 0
