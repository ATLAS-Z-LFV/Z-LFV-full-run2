mc16_13TeV.364198.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BVeto.deriv.DAOD_HIGG4D1.e5421_e5984_s3126_r10201_r10210_p3749
mc16_13TeV.364199.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BFilter.deriv.DAOD_HIGG4D1.e5421_e5984_s3126_r10201_r10210_p3749
mc16_13TeV.364200.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_BVeto.deriv.DAOD_HIGG4D1.e5421_e5984_s3126_r10201_r10210_p3749
mc16_13TeV.364201.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_BFilter.deriv.DAOD_HIGG4D1.e5421_e5984_s3126_r10201_r10210_p3749
mc16_13TeV.364202.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV280_E_CMS_BVeto.deriv.DAOD_HIGG4D1.e5421_e5984_s3126_r10201_r10210_p3749
mc16_13TeV.364203.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV280_E_CMS_BFilter.deriv.DAOD_HIGG4D1.e5421_e5984_s3126_r10201_r10210_p3749
mc16_13TeV.364204.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV0_70_BVeto.deriv.DAOD_HIGG4D1.e5421_e5984_s3126_r10201_r10210_p3749
mc16_13TeV.364205.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV0_70_BFilter.deriv.DAOD_HIGG4D1.e5421_e5984_s3126_r10201_r10210_p3749
mc16_13TeV.364206.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV70_280_BVeto.deriv.DAOD_HIGG4D1.e5421_e5984_s3126_r10201_r10210_p3749
mc16_13TeV.364207.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV70_280_BFilter.deriv.DAOD_HIGG4D1.e5421_e5984_s3126_r10201_r10210_p3749
mc16_13TeV.364208.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV280_E_CMS_BVeto.deriv.DAOD_HIGG4D1.e5421_e5984_s3126_r10201_r10210_p3749
mc16_13TeV.364209.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV280_E_CMS_BFilter.deriv.DAOD_HIGG4D1.e5421_e5984_s3126_r10201_r10210_p3749
# mc16_13TeV.308092.Sherpa_221_NNPDF30NNLO_Zee2jets_Min_N_TChannel.deriv.DAOD_HIGG4D1.e5767_e5984_s3126_r10201_r10210_p3749
# mc16_13TeV.308093.Sherpa_221_NNPDF30NNLO_Zmm2jets_Min_N_TChannel.deriv.DAOD_HIGG4D1.e5767_e5984_s3126_r10201_r10210_p3749
