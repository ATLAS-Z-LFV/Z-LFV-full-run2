#!/usr/bin/env python

import os, sys
from subprocess import check_output
from distutils.spawn import find_executable

#-------------------------------------------------
if not find_executable("ami"):
    print "ERROR: Cannot find command 'ami'. Have you set up 'pyami'?"
    sys.exit(1)

print_help = False
if "--help" in sys.argv or "-h" in sys.argv:
    print_help = True

if len(sys.argv) < 3:
    print "Too few arguments."
    print_help = True
else:
    output_file = sys.argv[1]
    input_files = sys.argv[2:]

if print_help:
    print "Usage:"
    print "    ./makePRWList.py <output_file.txt> <input_file_1.txt> [<input_file_2.txt> ...]"
    print "    ('input_file_*.txt' are text files containing AOD/DAOD MC dataset names)"
    sys.exit(1)

#-------------------------------------------------
if os.path.exists(output_file):
    print "WARNING: will recreate output file {}".format(output_file)

datasets = set()
print "Reading files..."
for input_file in input_files:
    print "  {}'".format(input_file)
    for line in open(input_file):
        dataset = line.partition("#")[0].strip()
        if not dataset or dataset.startswith("data"):
            continue
        datasets.add(dataset)
print "Found {} unique MC dataset(s) in {} file(s)".format(len(datasets), len(input_files))

search_patterns = set()
AMI_tags_16a = {}
AMI_tags_16d = {}
AMI_tags_16e = {}
for dataset in datasets:
    split = dataset.split('.')
    DSID = split[1]
    AMI_tag = split[-1]
    
    for i,x in enumerate(split):
        if x in ["recon", "merge"]:
            split[i] = "deriv"
            split[i+1] = "NTUP_PILEUP"
            break
        elif x == "deriv":
            split[i+1] = "NTUP_PILEUP"
            AMI_tag = AMI_tag.rpartition('_p')[0]  # remove derivation p-tag
            split[-1] = AMI_tag
            break
    
    # Check if we have multiple versions of the same dataset
    if "r9364" in AMI_tag:
        AMI_tags = AMI_tags_16a
    elif "r10201" in AMI_tag:
        AMI_tags = AMI_tags_16d
    elif "r10724" in AMI_tag:
        AMI_tags = AMI_tags_16e
    else:
        print "ERROR: Unknown MC campaign (AMI tag: {})".format(AMI_tag)
        sys.exit(1)
    
    if DSID not in AMI_tags:
        AMI_tags[DSID] = AMI_tag
    elif not AMI_tags[DSID] == AMI_tag:
        print "ERROR: Found multiple versions of the same dataset (DSID={}):".format(DSID)
        print "  {}".format(AMI_tag)
        print "  {}".format(AMI_tags[DSID])
        sys.exit(1)
    
    pattern = '.'.join(split) + "_p%"
    search_patterns.add(pattern)

PRW_files = []
not_found = []
for pattern in search_patterns:
    found_PRW_file = check_output(["ami", "list", "datasets", pattern])
    if not found_PRW_file:
        print "WARNING: NTUP_PILEUP file not found: '{}'".format(pattern)
        not_found.append(pattern)
    else:
        found_PRW_file = found_PRW_file.split()[-1]  # Take the last (latest) one if there are multiple
        print "  '{}'".format(pattern)
        print "->'{}'".format(found_PRW_file)
        PRW_files.append(found_PRW_file)

with open(output_file, 'w') as f:
    for l in PRW_files:
        print >> f, l

print "Created file '{}'.".format(output_file)
print "Please download the listed NTUP_PILEUP files (using `rucio_batch_get.sh` or your own way) and merge them using `hadd`."

if not len(not_found) == 0:
    print "WARNING: NTUP_PILEUP files not found for the following samples:"
    for f in not_found:
        print "  {}".format(f)
