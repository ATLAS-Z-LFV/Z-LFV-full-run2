#!/bin/sh

if [ $# -lt 1 ] ; then
  echo ''
  echo 'Give me a list of dataset names in a text file.'
  echo '  Example: "rucio_batch_get.sh list_of_datasets.txt"';
  echo 'Alternatively, use the <() operator.'
  echo "  Example: \"rucio_batch_get.sh <(rucio ls --short '*some_pattern*.root')\"";
  echo ''
  exit 1
fi

DATASETS=$1

#setupGrid

if [[ -z ${RUCIO_PATH+x} ]] ; then
  RUCIO_PATH="rucio"
  RUCIO_OPTIONS="--ndownloader 5"
else
  #RUCIO_PATH="~/bin/rucio"  # Terry: I hacked rucio to loosen its ndownload limit :)
  RUCIO_OPTIONS="--ndownloader 25"
fi

if [[ -z ${PRIMARY_RSE+x} ]] ; then
  echo '$PRIMARY_RSE not set/exported. Will not specify RSE when downloading.'
  echo 'Abort now if you want to specify RSE.'
  sleep 5
fi

TEMP=`mktemp -d -t`
WARNINGS=$TEMP/rucio_batch_get_warnings.log
HAS_WARNINGS=false

cat $DATASETS | \
(
while read f ; do
  if [ "$f" ] && [[ ${f// /} != \#* ]] ; then
    echo "Downloading "$f
    if [[ -z ${PRIMARY_RSE+x} ]] ; then
      outputs=`eval "$RUCIO_PATH get $RUCIO_OPTIONS $f"`
      N_failed=${outputs##* }
    else
      outputs=`eval "$RUCIO_PATH get --rse $PRIMARY_RSE $RUCIO_OPTIONS $f"`
      N_failed=${outputs##* }
      python -c "exit('$N_failed'.isdigit())"
      is_number=$?
      if [ ${is_number} -eq 0 ] || [ ${N_failed##* } -ne 0 ] ; then
        outputs=`eval "$RUCIO_PATH get $RUCIO_OPTIONS $f"`  # Once more with any RSE
        N_failed=${outputs##* }
      fi
    fi
    python -c "exit('$N_failed'.isdigit())"
    is_number=$?
    if [ ${is_number} -eq 0 ] ; then
      HAS_WARNINGS=true
      echo "WARNING: Failed to download $f" >> $WARNINGS
    elif [ ${N_failed} -ne 0 ] ; then
      HAS_WARNINGS=true
      echo "WARNING: ${N_failed} files of $f couldn't be downloaded" >> $WARNINGS
    fi
  fi
done
if $HAS_WARNINGS ; then
  echo '=============================='
  cat $WARNINGS
  echo '=============================='
else
  echo 'All files downloaded successfully!!'
fi
)

