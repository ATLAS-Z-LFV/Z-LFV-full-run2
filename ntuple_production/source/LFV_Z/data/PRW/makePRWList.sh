#!/bin/sh

# lsetup pyAMI
# voms-proxy-init -voms atlas

if [ $# -lt 2 ] ; then
  echo 'I need at least two arguments to work!'
  echo 'Example use: "./makePRWList.sh <output_file.txt> <input_file_1.txt> [<input_file_2.txt> ...]"';
  exit 1
fi

if [ -f $1 ] ; then
  echo "WARNING: recreating output file $1"
  rm $1
fi

for f in "${@:2}"; do
  echo "Reading input file: $f"
  echo 'Finding NTUP_PILEUP files:'
  while read L || [[ -n "$L" ]] ; do
    if [[ $L == *'recon.AOD.'* ]]; then
      pattern=${L/recon.AOD/deriv.NTUP_PILEUP}_p%
    elif [[ $L == *'merge.AOD.'* ]]; then
      pattern=${L/merge.AOD/deriv.NTUP_PILEUP}_p%
    elif [[ $L == *'.DAOD_HIGG4D1.'* ]]; then
      pattern=${L/DAOD_HIGG4D1/NTUP_PILEUP}
      pattern=${pattern%_p*}_p%
    elif [[ $L == *'.DAOD_HIGG4D2.'* ]]; then
      pattern=${L/DAOD_HIGG4D2/NTUP_PILEUP}
      pattern=${pattern%_p*}_p%
    fi
    #echo $pattern
    prw_file=`ami list datasets ${pattern}`
    if [[ -z $prw_file ]] ; then
      echo "  ERROR: NTUP_PILEUP file not found for '$L'"
    else
      prw_file=`tail -n1 <<< "$prw_file"`  # In case there are multiple files, take the last one
      echo "  Found: $prw_file"
      echo $prw_file >> $1
    fi
  done < "$f"
done

exit 0
