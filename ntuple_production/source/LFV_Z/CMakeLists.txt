
################################################
#                LFV_Z package                 #
################################################


cmake_minimum_required( VERSION 3.2 )

atlas_subdir( LFV_Z )


set(CMAKE_CXX_FLAGS "-Wno-unused-variable -Wno-unused-parameter -Wno-sign-compare -Wno-unused-local-typedefs -Wno-unused-but-set-variable -Wno-reorder -O2")

atlas_depends_on_subdirs(
    PUBLIC
    Extrapolate
    TauSpinnerTool
    EventLoop
    EventLoopAlgs
    xAODBTaggingEfficiency
    xAODCutFlow
    xAODEgamma
    xAODEventInfo
    xAODJet
    xAODMuon
    xAODRootAccess
    xAODTruth
    xAODTracking
    SUSYTools
    PMGTools
    GoodRunsLists
    PileupReweighting
    TrigDecisionTool
    IsolationSelection
    ElectronPhotonSelectorTools
    ElectronPhotonFourMomentumCorrection
    ElectronEfficiencyCorrection
    MuonSelectorTools
    MuonMomentumCorrections
    MuonEfficiencyCorrections
    JetSelectorTools
    JetMomentTools
    JetResolution
    JetCalibTools
    JetUncertainties
    TauAnalysisTools
    DiTauMassTools
)

#Externals on which the package relies   
find_package( ROOT )
find_package( Lhapdf )

atlas_add_root_dictionary(
    LFV_ZLib
    LFV_ZDict
    ROOT_HEADERS LFV_Z/*.h Root/LinkDef.h
    EXTERNAL_PACKAGES ROOT Lhapdf
)


atlas_add_library(
    LFV_ZLib
    LFV_Z/*.h Root/*.cxx ${LFV_ZDict}
    PUBLIC_HEADERS LFV_Z
    LINK_LIBRARIES ExtrapolateLib TauSpinnerToolLib EventLoop EventLoopAlgs xAODBTaggingEfficiencyLib xAODCutFlow xAODEgamma xAODEventInfo xAODJet xAODMuon xAODRootAccess xAODTracking xAODTruth SUSYToolsLib PMGToolsLib GoodRunsListsLib PileupReweightingLib TrigDecisionToolLib IsolationSelectionLib ElectronPhotonSelectorToolsLib ElectronPhotonFourMomentumCorrectionLib ElectronEfficiencyCorrectionLib MuonSelectorToolsLib MuonMomentumCorrectionsLib MuonEfficiencyCorrectionsLib JetSelectorToolsLib JetMomentToolsLib JetResolutionLib JetCalibToolsLib JetUncertaintiesLib TauAnalysisToolsLib DiTauMassToolsLib ${ROOT_LIBRARIES} ${LHAPDF_LIBRARIES}
    INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${LHAPDF_INCLUDE_DIRS}
)

atlas_install_data( data/*.ini )
atlas_install_data( data/GRL/*.xml )
atlas_install_data( data/PRW/*.root )
atlas_install_data( data/SUSYTools/*.conf )
atlas_install_data( data/TauAnalysisTools/*.conf )

atlas_install_python_modules( scripts/run.py )
