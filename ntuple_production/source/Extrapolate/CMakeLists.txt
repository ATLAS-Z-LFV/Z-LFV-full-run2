
################################################
#             Extrapolate package              #
################################################


cmake_minimum_required( VERSION 3.2 )

atlas_subdir( Extrapolate )


set(CMAKE_CXX_FLAGS "-Wno-unused-variable -Wno-unused-parameter -Wno-sign-compare -Wno-unused-local-typedefs -Wno-unused-but-set-variable")

atlas_depends_on_subdirs(
    PUBLIC
    xAODTracking
    xAODTau
)


#Externals on which the package relies   
find_package( ROOT )
find_package( Eigen )
      
atlas_add_root_dictionary(
    ExtrapolateLib
    ExtrapolateDict
	  ROOT_HEADERS Extrapolate/*.h Root/LinkDef.h
	  EXTERNAL_PACKAGES ROOT Eigen
)
   
atlas_add_library(
    ExtrapolateLib
    Extrapolate/*.h Root/*.cxx ${ExtrapolateDict}
    PUBLIC_HEADERS Extrapolate
    LINK_LIBRARIES ${ROOT_LIBRARIES} ${EIGEN_LIBRARIES} xAODTracking xAODTau
    INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS}
)


