#ifndef _TRACK_H_
#define _TRACK_H_

#include <TMath.h>
#include <stdio.h>
#include <iostream>
#include "TAxis.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TMatrixD.h"
#include "TMatrixDSym.h"
#include "TVectorD.h"

#include "xAODTracking/TrackParticle.h"
#include "xAODTau/TauTrack.h"

using namespace std;

class Track {
  private:
    TMatrixD covariance;
    TVectorD parameters;
    TVectorD pos, mom;

  public:
    // track parameters
    long double d0, z0, phi0, theta, qoverp;

    // constants
    long double a;
    long double r;
    long double q;
    long double p, pt, pz;

    // variables
    long double l;
    long double phi;
    long double x, y, z;
    long double px, py;

    Track() {}

    Track(const xAOD::TauTrack* track) : Track(*track) {}
    Track(const xAOD::TauTrack& track) : Track(track.track()) {}
    
    Track(const xAOD::TrackParticle* track) : Track(*track) {}
    Track(const xAOD::TrackParticle& track) {
        // Get the parameters and their covariance matrix straight from the xAOD track particle
        // Note: Parameter order documented at:
        // http://acode-browser2.usatlas.bnl.gov/lxr-rel20/source/atlas/Event/xAOD/xAODTracking/xAODTracking/versions/TrackParticle_v1.h#0097

        covariance.ResizeTo(5, 5);
        parameters.ResizeTo(5);

        // This class uses TVectorD and TMatrixD for its storage - wrap the output into that (no conversion methods exist as of
        // May 2016)
        const auto _parameters = track.definingParameters();
        const auto _covariance = track.definingParametersCovMatrix();

        for (unsigned int i = 0; i < 5; ++i) {
            parameters[i] = _parameters[i];
            for (unsigned int j = 0; j < 5; ++j) {
                covariance[i][j] = _covariance.row(i)[j];  // operator [i][j] not allowed for Eigen matrices
            }
        }
        // Shift z0's reference point to zero. And don't worry, tracks in the same collection always use the same reference point
        // vz.
        parameters[1] += track.vz();

        Calc();
    }

    Track(TVectorD par, TVectorD cov) {
        if (par.GetNoElements() != 5 || cov.GetNoElements() != 15) cout << "ERROR:: wrong dimensions" << endl;

        // resize the matrix and vectors
        covariance.ResizeTo(5, 5);
        parameters.ResizeTo(5);

        // put the matrix in right form
        TMatrixD tmp(5, 5);
        int b = 0;
        for (int j = 0; j < 5; j++) {
            for (int i = j; i < 5; i++) tmp[i][j] = tmp[j][i] = cov[i + b];
            b += (4 - j);
        }

        // save the info
        parameters = par;
        covariance = tmp;

        // calc the parameters and px and such
        Calc();
    }

    void Calc() {
        mom.ResizeTo(3);
        pos.ResizeTo(3);
        a = 0.6;

        // constant pars
        d0 = parameters[0];
        z0 = parameters[1];
        phi0 = parameters[2];
        theta = parameters[3];
        qoverp = parameters[4];

        // constant momenta (size)
        p = 1 / fabs(qoverp);  // cout<<"p = "<<p<<endl;
        pt = p * sin(theta);

        // q and r
        if (qoverp > 0) q = 1;
        if (qoverp < 0) q = -1;
        r = pt / (a * q);  // with B*r = 3.33 p [GeV/c] , so r = 3.33/B p [MeV/c] in mm

        // defining the point on the track
        l = 0;
        phi = phi0 + l / r;

        // calculating the variables
        px = pt * cos(phi);
        py = pt * sin(phi);
        pz = p * cos(theta);
        x = r * sin(phi) - r * sin(phi0) - d0 * sin(phi0);
        y = -r * cos(phi) + r * cos(phi0) + d0 * cos(phi0);
        z = z0 + l * pz / pt;

        // fill the pos and mom vector
        mom[0] = px;
        mom[1] = py;
        mom[2] = pz;
        pos[0] = x;
        pos[1] = y;
        pos[2] = z;
    }

    void SetTrack(Track trackje) {
        covariance.ResizeTo(5, 5);
        parameters.ResizeTo(5);
        parameters = trackje.GivePars();
        covariance = trackje.GiveCov();
        Calc();
    }
    TVectorD GiveMom() {
        return mom;
    }
    TVectorD GivePos() {
        return pos;
    }
    TVectorD GivePars() {
        return parameters;
    }
    TMatrixD GiveCov() {
        return covariance;
    }

    TGraph* giveGraph_xy() {
        float l2 = 0;
        float phi_l, x_l, y_l;
        TGraph* g = new TGraph();
        float lmax = 5;        // mm inner detector 1082 = innerdetector
        float npoints = 1000;  // number of points in our array 1083 = 1mm/interval
        for (int i = 0; i < npoints; i++) {
            l2 = l2 + lmax / (npoints - 1);  // changes
            phi_l = phi0 + l2 / r;           // changes
            x_l = r * sin(phi_l) - r * sin(phi0) - d0 * sin(phi0);
            y_l = -r * cos(phi_l) + r * cos(phi0) + d0 * cos(phi0);
            g->SetPoint(i, x_l, y_l);
        }
        g->SetTitle("Tracks");
        g->GetXaxis()->SetTitle("x (mm)");
        g->GetYaxis()->SetTitle("y (mm)");
        //    g->GetXaxis()->SetTitleOffset(2.0);
        g->GetYaxis()->SetTitleOffset(1.5);
        g->SetLineWidth(2);
        g->SetLineColor(kRed);
        return g;
    }
    TGraph* giveGraph_zR() {
        float l2 = 0;
        TGraph* g = new TGraph();
        float lmax = 5;        // mm inner detector 1082 = innerdetector
        float npoints = 1000;  // number of points in our array 1083 = 1mm/interval
        for (int i = 0; i < npoints; i++) {
            l2 += lmax / (npoints - 1);   // changes
            float phi_l = phi0 + l2 / r;  // changes
            float x_l = r * sin(phi_l) - r * sin(phi0) - d0 * sin(phi0);
            float y_l = -r * cos(phi_l) + r * cos(phi0) + d0 * cos(phi0);
            float z_l = z0 + l2 * pz / pt;
            float r_l = sqrt(x_l * x_l + y_l * y * l);
            g->SetPoint(i, z_l, r_l);
        }
        g->SetTitle("Tracks");
        g->GetXaxis()->SetTitle("z (mm)");
        g->GetYaxis()->SetTitle("R (mm)");
        //    g->GetXaxis()->SetTitleOffset(2.0);
        g->GetYaxis()->SetTitleOffset(1.0);
        g->SetLineWidth(2);
        g->SetLineColor(kRed);
        return g;
    }
    TGraph* giveGraph_zy() {
        float l2 = 0;
        TGraph* g = new TGraph();
        float lmax = 5;        // mm inner detector 1082 = innerdetector
        float npoints = 1000;  // number of points in our array 1083 = 1mm/interval
        for (int i = 0; i < npoints; i++) {
            l2 += lmax / (npoints - 1);   // changes
            float phi_l = phi0 + l2 / r;  // changes
            // float x_l = r * sin(phi_l) - r * sin(phi0) - d0 * sin(phi0);
            float y_l = -r * cos(phi_l) + r * cos(phi0) + d0 * cos(phi0);
            float z_l = z0 + l2 * pz / pt;
            // float r_l = sqrt(x_l * x_l + y_l * y * l);
            g->SetPoint(i, z_l, y_l);
        }
        g->SetTitle("Tracks");
        g->GetXaxis()->SetTitle("z (mm)");
        g->GetYaxis()->SetTitle("y (mm)");
        //    g->GetXaxis()->SetTitleOffset(2.0);
        g->GetYaxis()->SetTitleOffset(1.0);
        g->SetLineWidth(2);
        g->SetLineColor(kRed);
        return g;
    }
};

////////////////////
// irrelevant for now
////////////////////

/*   // return a graph of the postion */
/*   TGraph2D* giveGraph_3D() */
/*   { */
/*     float l = 0; */
/*     float phi,x,y,z; */
/*     TGraph2D* g = new TGraph2D(); */
/*     float lmax = 1082; //mm inner detector 1082 = innerdetector */
/*     float npoints = 100; // number of points in our array 1083 = 1mm/interval */
/*     // fill the graph */
/*     for (int i=0; i<npoints; i++) */
/*       { */
/* 	l = l + lmax/(npoints - 1); //changes */
/* 	phi = phi0 + l/r; // changes */

/* 	x =  x0 + r*sin(phi) - r*sin(phi0); */
/* 	y =  y0 - r*cos(phi) + r*cos(phi0); */
/* 	z =  z0 + l/tan(theta); */
/* 	g->SetPoint(i,x,y,z); */
/*       } */

/*     g->SetTitle("Tracks"); */
/*     g->GetXaxis()->SetTitle("x (mm)"); */
/*     g->GetYaxis()->SetTitle("y (mm)"); */
/*     g->GetZaxis()->SetTitle("z (mm)"); */
/*     g->GetXaxis()->SetTitleOffset(2.0); */
/*     g->GetYaxis()->SetTitleOffset(2.0); */
/*     g->GetZaxis()->SetTitleOffset(1.0); */

/*     //g->GetXaxis()->SetRangeUser(-10000,10000); */
/*     //  g->GetYaxis()->SetRangeUser(-10000,10000); */

/*     g->SetLineWidth(2); */
/*     g->SetLineColor(kRed); */
/*     return g; */

/*   } */

/* TGraph* giveGraph_xy() */
/*   { */
/*     float l = 0; */
/*     float phi,x,y; */
/*     TGraph* g = new TGraph(); */
/*     float lmax = 1082; //mm inner detector 1082 = innerdetector */
/*     float npoints = 1000; // number of points in our array 1083 = 1mm/interval */
/*     for (int i=0; i<npoints; i++) */
/*       { */
/* 	l = l + lmax/(npoints - 1); //changes */
/* 	phi = phi0 + l/r; // changes */
/* 	x =  x0 + r*sin(phi) - r*sin(phi0); */
/* 	y =  y0 - r*cos(phi) + r*cos(phi0); */
/* 	g->SetPoint(i,x,y); */
/*       } */

/*     g->SetTitle("Tracks"); */
/*     g->GetXaxis()->SetTitle("x (mm)"); */
/*     g->GetYaxis()->SetTitle("y (mm)"); */
/*     //    g->GetXaxis()->SetTitleOffset(2.0); */
/*     g->GetYaxis()->SetTitleOffset(1.2); */
/*     g->SetLineWidth(2); */
/*     g->SetLineColor(kRed); */
/*     return g; */
/*   } */

/*   // spit out the kinametics */
/*   void kinematics() //const */
/*   { */
/*     CalcKin(); */

/*     cout<<"p  = "<<p<<   " MeV"<< endl; // MeV/ec */
/*     cout<<"pt = "<<pt<<  " MeV"<< endl; */
/*     cout<<"pz = "<<pz<<  " MeV"<< endl; */
/*     cout<<"py = "<<py0<< " MeV"<< endl; */
/*     cout<<"px = "<<px0<< " MeV"<< endl; */
/*     cout<<"x0 = "<<x0<<   " mm"<< endl; */
/*     cout<<"y0 = "<<y0<<   " mm"<< endl; */
/*     cout<<"z0 = "<<z0<<   " mm"<< endl; */
/*     cout<<"r  = "<<r <<   " mm"<< endl;     */
/*     cout<<"q  = "<<q<<    " e" << endl; */
/*   } */

/* } ; */

#endif
