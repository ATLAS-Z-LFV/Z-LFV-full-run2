#ifndef _VERTEX_H_
#define _VERTEX_H_

#include "Track.h"
#include "Util.h"

using namespace std;

class Vertex {
  private:
    Track muon1, muon2, muon3;
    TVectorD M, X, h, r, halfdchi2dx, dX;
    // M: both muon parameters from 0-4 and 5-9
    // X: both muon momentum vector from 0-2 and 3-5
    // V: both covariance matrices - first in top left 5x5 corner, second in bottom 5x5
    // Vi: inverse of V matrix
    TMatrixD V, Vi, H, HT, C, halfd2chi2dx2;
    long double chi2;
    bool output;
    std::pair<TVectorD, TMatrixD> helix;
    TVectorD tauVec;
    TMatrixD helixCovarianceMatrix;
    TMatrixD helixJacobian;
    TMatrixD Ctau;
    TMatrixD J, JT;

  public:
    Vertex() {}

    Vertex(Track mu1, Track mu2, Track mu3) {
        output = 0;
        muon1.SetTrack(mu1);
        muon2.SetTrack(mu2);
        muon3.SetTrack(mu3);
        M.ResizeTo(15);
        h.ResizeTo(15);
        r.ResizeTo(15);
        X.ResizeTo(12);
        dX.ResizeTo(12);
        halfdchi2dx.ResizeTo(12);
        V.ResizeTo(15, 15);
        Vi.ResizeTo(15, 15);
        halfd2chi2dx2.ResizeTo(12, 12);
        C.ResizeTo(12, 12);
        H.ResizeTo(15, 12);
        HT.ResizeTo(12, 15);

        // TODO: move to STL classes for this
        tauVec.ResizeTo(6);
        Ctau.ResizeTo(6, 6);
        J.ResizeTo(6, 12);
        JT.ResizeTo(12, 6);
        helix.first.ResizeTo(5);
        helix.second.ResizeTo(6, 5);
        helixCovarianceMatrix.ResizeTo(5, 5);
        helixCovarianceMatrix.SetTol(1.e-50);
        helixJacobian.ResizeTo(5, 6);

        H.SetTol(1.e-50);
        HT.SetTol(1.e-50);
        V.SetTol(1.e-50);
        Vi.SetTol(1.e-50);
        halfd2chi2dx2.SetTol(1.e-50);
        C.SetTol(1.e-50);

        Build();
    }
    void Build() {
        if (output) {
            cout << "======================" << endl;
            cout << "    Building vertex   " << endl;
            cout << "======================" << endl;
        }
        M.SetSub(0, muon1.GivePars());
        M.SetSub(5, muon2.GivePars());
        M.SetSub(10, muon3.GivePars());
        X.SetSub(3, muon1.GiveMom());
        X.SetSub(6, muon2.GiveMom());
        X.SetSub(9, muon3.GiveMom());  // X.SetSub( 0,  muon3.GivePos()  );
        V.SetSub(0, 0, muon1.GiveCov());
        V.SetSub(5, 5, muon2.GiveCov());
        V.SetSub(10, 10, muon3.GiveCov());
        Vi = V;
        Vi.Invert();
        Update();
        CalcChi2();

        // We build a Jacobian matrix J like this:
        // (1 0 0 0 0 0 0 0 0 0 0 0 )
        // (0 1 0 0 0 0 0 0 0 0 0 0 )
        // (0 0 1 0 0 0 0 0 0 0 0 0 )
        // (0 0 0 1 0 0 1 0 0 1 0 0 )
        // (0 0 0 0 1 0 0 1 0 0 1 0 )
        // (0 0 0 0 0 1 0 0 1 0 0 1 )

        J[0][0] = 1;
        J[1][1] = 1;
        J[2][2] = 1;
        J[3][3] = 1;
        J[4][4] = 1;
        J[5][5] = 1;

        J[3][6] = 1;
        J[4][7] = 1;
        J[5][8] = 1;

        J[3][9] = 1;
        J[4][10] = 1;
        J[5][11] = 1;

        JT.Transpose(J);
    }

    long double CalcChi2() {
        TVectorD tmp(15);
        tmp = Vi * r;
        chi2 = r * tmp;
        if (output) cout << "chi2 = " << chi2 << endl;
        return chi2;
    }

    void FitOneStep() {
        halfdchi2dx = HT * Vi * r;
        halfd2chi2dx2 = HT * Vi * H;
        halfd2chi2dx2.Invert();
        dX = halfd2chi2dx2 * halfdchi2dx;  // dX.Print();
        X = X - dX;
        Update();
        CalcChi2();

        // now build the tau position (= our vertex) and the momentum (= sum of 3 tracks)
        tauVec[0] = X[0];
        tauVec[1] = X[1];
        tauVec[2] = X[2];
        tauVec[3] = 0;
        tauVec[4] = 0;
        tauVec[5] = 0;
        for (unsigned int i = 0; i < 3; ++i) {
            tauVec[3] += X[3 + 3 * i];
            tauVec[4] += X[4 + 3 * i];
            tauVec[5] += X[5 + 3 * i];
        }

        // std::cout << "Found tauVec in (x,p) form:";
        // for (unsigned int i = 0; i < 6; ++i) {
        // std::cout << " " << tauVec[i];
        //}
        // std::cout << std::endl;

        // the covariance matrix for (x,y,z,px,py,pz) representation for all tracks is in halfd2chi2dx2
        // add these together for taus
        Ctau = J * halfd2chi2dx2 * JT;

        // we now convert it to the 5x5 matrix for the helix as Ctau5x5 = J^T Ctaux6x6 J
        // with J the second parameter we get from positionToHelix
        const double q = muon1.q * muon2.q * muon3.q;
        helix = positionToHelix(tauVec, q);

        // std::cout << "Found tauVec in helix form:";
        // for (unsigned int i = 0; i < 5; ++i) {
        // std::cout << " " << helix.first[i];
        //}
        // std::cout << std::endl;

        helixJacobian.Transpose(helix.second);
        helixCovarianceMatrix = helixJacobian * Ctau * helix.second;

        // std::cout << "Covariance matrix sqrt(entry_i)";
        // for (unsigned int i = 0; i < 5; ++i) {
        // std::cout << " " << std::sqrt(helixCovarianceMatrix[i][i]);
        //}
        // std::cout << std::endl;

        //    C = halfd2chi2dx2;    C.SetTol(1.e-50);     C.Invert(); // now it is C
    }

    long double d0() {
        const long double d0 = helix.first[0];
        return d0;
    }

    long double d0err() {
        return std::sqrt(helixCovarianceMatrix[0][0]);
    }

    long double d0sig() {
        const long double d0err = this->d0err();

        return d0() / d0err;
    }

    long double z0() {
        return helix.first[1];  // Shifted z0, i.e. track->z0()+track->vz()
    }

    long double phi() {
        return helix.first[2];
    }

    long double theta() {
        return helix.first[3];
    }

    long double z0sintheta() {
        const long double z0 = this->z0();
        const long double theta = this->theta();

        return z0 * std::sin(theta);
    }

    long double fit() {
        double chi2_start = chi2;
        for (int i = 0; i < 10; i++) {
            FitOneStep();
            if (chi2 > 1.5 * chi2_start) {
                cout << "ERROR: chi2 fit is not converging" << endl;
                chi2 = -1;
                break;
            }
        }
        return chi2;
    }
    TVectorD GivePull(TVectorD truth_pos) {
        TVectorD fit_pos(3);
        fit_pos = X.GetSub(0, 2);
        TVectorD pull(3);
        pull = fit_pos - truth_pos;
        TVectorD sigma(3);
        sigma[0] = sqrt(halfd2chi2dx2[0][0]);
        sigma[1] = sqrt(halfd2chi2dx2[1][1]);
        sigma[2] = sqrt(halfd2chi2dx2[2][2]);
        pull = ElementDiv(pull, sigma);
        return pull;
    }
    TVectorD GivePos() {
        TVectorD fit_pos(3);
        fit_pos = X.GetSub(0, 2);
        return fit_pos;
    }

    double doca() {
        // updated primary vertex position
        double x = X[0];
        double y = X[1];
        double z = X[2];

        double aq = 0.6 * muon1.q;
        // updated muon1 momentum
        double px1 = X[3];
        double py1 = X[4];
        // double pz1     = X[5];
        // double pt1     = sqrt(px1*px1 + py1*py1);
        double px0 = px1 + aq * y;
        double py0 = py1 - aq * x;
        double phi1 = atan2(py1, px1);
        double phi0 = atan2(py0, px0);
        double deltaphi = phi1 - phi0;
        if (deltaphi > TMath::Pi())
            deltaphi -= 2 * TMath::Pi();
        else if (deltaphi < -TMath::Pi())
            deltaphi += 2 * TMath::Pi();

        // and now back to original trajection
        // double l1 = deltaphi * muon1.pt/aq ;
        double r1 = muon1.pt / aq;
        double x1 = r1 * std::sin(deltaphi + muon1.phi0) - r1 * std::sin(muon1.phi0) - muon1.d0 * std::sin(muon1.phi0);
        double y1 = -r1 * std::cos(deltaphi + muon1.phi0) + r1 * std::cos(muon1.phi0) + muon1.d0 * std::cos(muon1.phi0);
        double z1 = muon1.z0 + deltaphi * muon1.pz / aq;  // z0+l*pz/pt

        double aq2 = 0.6 * muon2.q;
        // updated muon2 momentum
        double px2 = X[6];
        double py2 = X[7];
        // double pz2     = X[8];
        // double pt2     = sqrt(px2*px2 + py2*py2);
        double px02 = px2 + aq2 * y;
        double py02 = py2 - aq2 * x;
        double phi2 = atan2(py2, px2);
        double phi02 = atan2(py02, px02);
        deltaphi = phi2 - phi02;
        if (deltaphi > TMath::Pi())
            deltaphi -= 2 * TMath::Pi();
        else if (deltaphi < -TMath::Pi())
            deltaphi += 2 * TMath::Pi();

        // and now back to original trajection
        // double l1 = deltaphi * muon1.pt/aq ;
        double r2 = muon2.pt / aq2;
        double x2 = r2 * std::sin(deltaphi + muon2.phi0) - r2 * std::sin(muon2.phi0) - muon2.d0 * std::sin(muon2.phi0);
        double y2 = -r2 * std::cos(deltaphi + muon2.phi0) + r2 * std::cos(muon2.phi0) + muon2.d0 * std::cos(muon2.phi0);
        double z2 = muon2.z0 + deltaphi * muon2.pz / aq2;  // z0+l*pz/pt

        TVector3 dx(x1 - x2, y1 - y2, z1 - z2);
        TVector3 p1(X[3], X[4], X[5]);
        TVector3 p2(X[6], X[7], X[8]);

        if (output)
            cout << "------------\n"
                 << " Vertex : " << x << " " << y << " " << z << endl
                 << " Trk 1  : " << x1 << " " << y1 << " " << z1 << endl
                 << " Trk 2  : " << x2 << " " << y2 << " " << z2 << endl;

        TVector3 crossMom = p1.Cross(p2);
        double doca = dx.Dot(crossMom.Unit());

        return doca;
    }

    void Update() {
        for (int i = 0; i < 3; i++) {
            long double q = 0;
            if (i == 0) q = muon1.q;
            if (i == 1) q = muon2.q;
            if (i == 2) q = muon3.q;

            long double x = X[0];
            long double y = X[1];
            long double z = X[2];
            long double px = X[3 * i + 3];
            long double py = X[3 * i + 4];
            long double pz = X[3 * i + 5];

            long double aq = 0.6 * q;
            long double pt2 = px * px + py * py;
            long double pt = sqrt(pt2);
            long double p2 = pt2 + pz * pz;
            long double p = sqrt(p2);
            long double p3 = p2 * p;
            long double px0 = px + aq * y;
            long double py0 = py - aq * x;
            long double pt02 = px0 * px0 + py0 * py0;
            long double pt0 = sqrt(pt02);
            long double phi = atan2(py, px);
            long double phi0 = atan2(py0, px0);
            long double deltaphi = phi - phi0;
            if (deltaphi > TMath::Pi())
                deltaphi -= 2 * TMath::Pi();
            else if (deltaphi < -TMath::Pi())
                deltaphi += 2 * TMath::Pi();
            long double l = deltaphi * pt / aq;
            long double d0 = (pt0 - pt) / aq;
            long double z0 = z - l * pz / pt;  // was wrong!!!
            long double qoverp = q / p;
            long double theta = atan2(pt, pz);

            TVectorD h1(5);
            h1[0] = d0;
            h1[1] = z0;
            h1[2] = phi0;
            h1[3] = theta;
            h1[4] = qoverp;
            TMatrixD A;
            A.ResizeTo(3, 5);
            TMatrixD B;
            B.ResizeTo(3, 5);
            // the row with dx
            A[0][0] = -py0 / pt0;
            A[0][1] = -pz * px0 / pt02;
            A[0][2] = -aq * px0 / pt02;
            A[0][3] = 0;
            A[0][4] = 0;
            // the row with dy
            A[1][0] = px0 / pt0;
            A[1][1] = -pz * py0 / pt02;
            A[1][2] = -aq * py0 / pt02;
            A[1][3] = 0;
            A[1][4] = 0;
            // the row with dz
            A[2][0] = 0;
            A[2][1] = 1;
            A[2][2] = 0;
            A[2][3] = 0;
            A[2][4] = 0;
            // the row with px
            B[0][0] = (px0 / pt0 - px / pt) / aq;
            B[0][1] = -pz * (py0 / pt02 - py / pt2) / aq;
            B[0][2] = -py0 / pt02;
            B[0][3] = pz * px / (p2 * pt);  // different from wouter -> check again
            B[0][4] = -q * px / p3;         // different from wouter
            // the row with py
            B[1][0] = (py0 / pt0 - py / pt) / aq;
            B[1][1] = pz * (px0 / pt02 - px / pt2) / aq;
            B[1][2] = px0 / pt02;
            B[1][3] = pz * py / (p2 * pt);  // different from wouter
            B[1][4] = -q * py / p3;         // different from wouter
            // the row with pz
            B[2][0] = 0;
            B[2][1] = -l / pt;
            B[2][2] = 0;
            B[2][3] = -pt / p2;      // different from wouter
            B[2][4] = -q * pz / p3;  // different from wouter

            HT.SetSub(0, 5 * i, A);
            HT.SetSub(3 * i + 3, 5 * i, B);
            h.SetSub(5 * i, h1);
        }
        H.Transpose(HT);
        r = h - M;
        if (r[3] > TMath::Pi())
            r[3] -= 2 * TMath::Pi();
        else if (r[3] < -TMath::Pi())
            r[3] += 2 * TMath::Pi();
    }
};

/* /\*   //checks not relevant to the fit code: *\/ */

/* /\*   TVectorD Xtoh(TVectorD X_tmp, long double q1) *\/ */
/* /\*   { *\/ */
/* /\*     long double x   = X_tmp[0]; *\/ */
/* /\*     long double y   = X_tmp[1]; *\/ */
/* /\*     long double z   = X_tmp[2]; *\/ */
/* /\*     long double px  = X_tmp[3]; *\/ */
/* /\*     long double py  = X_tmp[4]; *\/ */
/* /\*     long double pz  = X_tmp[5]; *\/ */
/* /\*     long double q  = q1; *\/ */

/* /\*     long double aq = 0.6*q;  *\/ */
/* /\*     long double pt2        = px*px + py*py; *\/ */
/* /\*     long double pt         = sqrt(pt2); *\/ */
/* /\*     long double p2         = pt2 + pz*pz; *\/ */
/* /\*     long double p          = sqrt(p2);  *\/ */
/* /\*     long double p3         = p2*p;  *\/ */
/* /\*     long double px0        = px + aq*y; *\/ */
/* /\*     long double py0        = py - aq*x; *\/ */
/* /\*     long double pt02       = px0*px0 + py0*py0; *\/ */
/* /\*     long double pt0        = sqrt(pt02); *\/ */
/* /\*     long double phi        = atan2(py,px); *\/ */
/* /\*     long double phi0       = atan2(py0,px0) ; *\/ */
/* /\*     long double deltaphi   = phi-phi0 ; *\/ */
/* /\*     if(     deltaphi >  TMath::Pi() ) deltaphi -= 2*TMath::Pi() ; *\/ */
/* /\*     else if(deltaphi < -TMath::Pi() ) deltaphi += 2*TMath::Pi() ; *\/ */
/* /\*     long double l      = deltaphi * pt /aq ; *\/ */
/* /\*     long double d0     = (pt0 - pt)  /aq; *\/ */
/* /\*     long double z0     = z - l*pz/pt;// was wrong!!! *\/ */
/* /\*     long double qoverp = q / p; *\/ */
/* /\*     long double theta  = atan2(pt,pz); *\/ */

/* /\*     TVectorD h_tmp(5);    h_tmp[0] = d0;    h_tmp[1] = z0;    h_tmp[2] = phi0;    h_tmp[3] = theta;    h_tmp[4] = qoverp;
 * *\/ */
/* /\*     return h_tmp; *\/ */

/* /\*   } *\/ */

/* /\*  TMatrixD XtoH(TVectorD X_tmp, long double q1) *\/ */
/* /\*   { *\/ */
/* /\*     long double x   = X_tmp[0]; *\/ */
/* /\*     long double y   = X_tmp[1]; *\/ */
/* /\*     long double z   = X_tmp[2]; *\/ */
/* /\*     long double px  = X_tmp[3]; *\/ */
/* /\*     long double py  = X_tmp[4]; *\/ */
/* /\*     long double pz  = X_tmp[5]; *\/ */
/* /\*     long double q  = q1; *\/ */

/* /\*     long double aq = 0.6*q;  *\/ */
/* /\*     long double pt2        = px*px + py*py; *\/ */
/* /\*     long double pt         = sqrt(pt2); *\/ */
/* /\*     long double p2         = pt2 + pz*pz; *\/ */
/* /\*     long double p          = sqrt(p2);  *\/ */
/* /\*     long double p3         = p2*p;  *\/ */
/* /\*     long double px0        = px + aq*y; *\/ */
/* /\*     long double py0        = py - aq*x; *\/ */
/* /\*     long double pt02       = px0*px0 + py0*py0; *\/ */
/* /\*     long double pt0        = sqrt(pt02); *\/ */
/* /\*     long double phi        = atan2(py,px); *\/ */
/* /\*     long double phi0       = atan2(py0,px0) ; *\/ */
/* /\*     long double deltaphi   = phi-phi0 ; *\/ */
/* /\*     if(     deltaphi >  TMath::Pi() ) deltaphi -= 2*TMath::Pi() ; *\/ */
/* /\*     else if(deltaphi < -TMath::Pi() ) deltaphi += 2*TMath::Pi() ; *\/ */
/* /\*     long double l      = deltaphi * pt /aq ; *\/ */

/* /\*     TMatrixD HT_tmp;  HT_tmp.ResizeTo(6,5); *\/ */
/* /\*     TMatrixD A;    A.ResizeTo(3,5); *\/ */
/* /\*     TMatrixD B;    B.ResizeTo(3,5); *\/ */

/* /\*     // the row with dx *\/ */
/* /\*     A[0][0] = -py0/pt0;   *\/ */
/* /\*     A[0][1] = -pz*px0/pt02; *\/ */
/* /\*     A[0][2] = -aq*px0/pt02; *\/ */
/* /\*     A[0][3] =  0; *\/ */
/* /\*     A[0][4] =  0; *\/ */
/* /\*     // the row with dy *\/ */
/* /\*     A[1][0] =  px0/pt0;  *\/ */
/* /\*     A[1][1] = -pz*py0/pt02; *\/ */
/* /\*     A[1][2] = -aq*py0/pt02; *\/ */
/* /\*     A[1][3] =  0; *\/ */
/* /\*     A[1][4] =  0; *\/ */
/* /\*     // the row with dz *\/ */
/* /\*     A[2][0] =  0;  *\/ */
/* /\*     A[2][1] =  1; *\/ */
/* /\*     A[2][2] =  0; *\/ */
/* /\*     A[2][3] =  0; *\/ */
/* /\*     A[2][4] =  0; *\/ */
/* /\*     // the row with px *\/ */
/* /\*     B[0][0] =  (px0/pt0 - px/pt) / aq; *\/ */
/* /\*     B[0][1] = -pz*(py0/pt02 - py/pt2)/aq; *\/ */
/* /\*     B[0][2] = -py0/pt02; *\/ */
/* /\*     B[0][3] =  pz*px/(p2*pt);  //different from wouter -> check again *\/ */
/* /\*     B[0][4] = -q*px/p3; // different from wouter  *\/ */
/* /\*     // the row with py *\/ */
/* /\*     B[1][0] = (py0/pt0 - py/pt) / aq;  *\/ */
/* /\*     B[1][1] =  pz*(px0/pt02 - px/pt2)/aq; *\/ */
/* /\*     B[1][2] =  px0/pt02; *\/ */
/* /\*     B[1][3] =  pz*py/(p2*pt); // different from wouter *\/ */
/* /\*     B[1][4] = -q*py/p3;// different from wouter *\/ */
/* /\*     // the row with pz *\/ */
/* /\*     B[2][0] = 0; *\/ */
/* /\*     B[2][1] = -l/pt; *\/ */
/* /\*     B[2][2] = 0; *\/ */
/* /\*     B[2][3] = -pt/p2; // different from wouter *\/ */
/* /\*     B[2][4] = -q*pz/p3; *\/ */

/* /\*     HT_tmp.SetSub( 0, 0, A); *\/ */
/* /\*     HT_tmp.SetSub( 3, 0, B); *\/ */

/* /\*     return HT_tmp; *\/ */
/* /\*   } *\/ */
/* /\*   void DiffNum() *\/ */
/* /\*   { *\/ */
/* /\*     TVectorD M1(muon1.GivePars() ); *\/ */

/* /\*     // original parameters *\/ */
/* /\*     TVectorD X1(6);   X1.SetSub( 3, muon1.GiveMom()  ); X1.SetSub( 0,  muon1.GivePos()  ); *\/ */
/* /\*     TVectorD h1(5);    h1  = Xtoh(X1,muon1.q); *\/ */

/* /\*     // X little bit shifted up *\/ */
/* /\*     TVectorD pX1dx(X1);  pX1dx[0]  += 1e-8; *\/ */
/* /\*     TVectorD pX1dy(X1);  pX1dy[1]  += 1e-8; *\/ */
/* /\*     TVectorD pX1dz(X1);  pX1dz[2]  += 1e-8; *\/ */
/* /\*     TVectorD pX1dpx(X1); pX1dpx[3] += 0.01; *\/ */
/* /\*     TVectorD pX1dpy(X1); pX1dpy[4] += 0.01; *\/ */
/* /\*     TVectorD pX1dpz(X1); pX1dpz[5] += 0.01; *\/ */

/* /\*     // X little bit shifted down *\/ */
/* /\*     TVectorD mX1dx(X1);  mX1dx[0]  -= 1e-8; *\/ */
/* /\*     TVectorD mX1dy(X1);  mX1dy[1]  -= 1e-8; *\/ */
/* /\*     TVectorD mX1dz(X1);  mX1dz[2]  -= 1e-8; *\/ */
/* /\*     TVectorD mX1dpx(X1); mX1dpx[3] -= 0.01; *\/ */
/* /\*     TVectorD mX1dpy(X1); mX1dpy[4] -= 0.01; *\/ */
/* /\*     TVectorD mX1dpz(X1); mX1dpz[5] -= 0.01; *\/ */

/* /\*     // h little bit shifted up *\/ */
/* /\*     TVectorD ph1dx(5);   ph1dx  = Xtoh(pX1dx,muon1.q); *\/ */
/* /\*     TVectorD ph1dy(5);   ph1dy  = Xtoh(pX1dy,muon1.q); *\/ */
/* /\*     TVectorD ph1dz(5);   ph1dz  = Xtoh(pX1dz,muon1.q); *\/ */
/* /\*     TVectorD ph1dpx(5);  ph1dpx = Xtoh(pX1dpx,muon1.q); *\/ */
/* /\*     TVectorD ph1dpy(5);  ph1dpy = Xtoh(pX1dpy,muon1.q); *\/ */
/* /\*     TVectorD ph1dpz(5);  ph1dpz = Xtoh(pX1dpz,muon1.q); *\/ */

/* /\*     // h little bit shifted up *\/ */
/* /\*     TVectorD mh1dx(5);   mh1dx  = Xtoh(mX1dx,muon1.q); *\/ */
/* /\*     TVectorD mh1dy(5);   mh1dy  = Xtoh(mX1dy,muon1.q); *\/ */
/* /\*     TVectorD mh1dz(5);   mh1dz  = Xtoh(mX1dz,muon1.q); *\/ */
/* /\*     TVectorD mh1dpx(5);  mh1dpx = Xtoh(mX1dpx,muon1.q); *\/ */
/* /\*     TVectorD mh1dpy(5);  mh1dpy = Xtoh(mX1dpy,muon1.q); *\/ */
/* /\*     TVectorD mh1dpz(5);  mh1dpz = Xtoh(mX1dpz,muon1.q); *\/ */

/* /\*     // difference *\/ */
/* /\*     TVectorD dh1dx(5);   dh1dx  = ph1dx  - mh1dx;   *\/ */
/* /\*     TVectorD dh1dy(5);   dh1dy  = ph1dy  - mh1dy;   *\/ */
/* /\*     TVectorD dh1dz(5);   dh1dz  = ph1dz  - mh1dz;   *\/ */
/* /\*     TVectorD dh1dpx(5);  dh1dpx = ph1dpx - mh1dpx;   *\/ */
/* /\*     TVectorD dh1dpy(5);  dh1dpy = ph1dpy - mh1dpy;   *\/ */
/* /\*     TVectorD dh1dpz(5);  dh1dpz = ph1dpz - mh1dpz;   *\/ */

/* /\*     dh1dx  *= 0.5e8; *\/ */
/* /\*     dh1dy  *= 0.5e8; *\/ */
/* /\*     dh1dz  *= 0.5e8; *\/ */
/* /\*     dh1dpx *= 50; *\/ */
/* /\*     dh1dpy *= 50; *\/ */
/* /\*     dh1dpz *= 50; *\/ */

/* /\*     TMatrixD H1(5,6); *\/ */
/* /\*     for(int j=0;j<5;j++) *\/ */
/* /\*       { *\/ */
/* /\*   	H1[j][0] = dh1dx[j]; *\/ */
/* /\*   	H1[j][1] = dh1dy[j]; *\/ */
/* /\*   	H1[j][2] = dh1dz[j]; *\/ */
/* /\*   	H1[j][3] = dh1dpx[j]; *\/ */
/* /\*   	H1[j][4] = dh1dpy[j]; *\/ */
/* /\*   	H1[j][5] = dh1dpz[j]; *\/ */

/* /\*       } *\/ */
/* /\*     TMatrixD H1T_num(6,5); *\/ */
/* /\*     H1T_num.Transpose(H1); *\/ */
/* /\*     cout<<"numerically calculated: "<<endl; *\/ */
/* /\*     cout<<"========================"<<endl; *\/ */
/* /\*     H1T_num.Print(); *\/ */
/* /\*     TMatrixD H1T(6,5); H1T = XtoH(X1,muon1.q); *\/ */
/* /\*     cout<<"analytically calculated: "<<endl; *\/ */
/* /\*     cout<<"========================"<<endl; *\/ */
/* /\*     H1T.Print(); *\/ */
/* /\*     cout<<"difference between the two: "<<endl; *\/ */
/* /\*     cout<<"============================"<<endl; *\/ */
/* /\*     TMatrixD diff(6,5); *\/ */
/* /\*     diff = H1T_num-H1T; *\/ */
/* /\*     diff.Print(); *\/ */

/* /\*     //TVectorD r1(5);   r1 = M1 - h1; *\/ */
/* /\*     //  r1.Print(); *\/ */
/* /\*     //    H1T.Print(); *\/ */
/* /\*   } *\/ */

/* /\*   void TestConversion() *\/ */
/* /\*   { *\/ */
/* /\*     TMatrixD H1T; H1T.ResizeTo(12,15); *\/ */
/* /\*     TMatrixD H1;  H1.ResizeTo(12,15); *\/ */

/* /\*     for( int i=0; i<3;i++) *\/ */
/* /\*       { *\/ */
/* /\* 	Track muon; *\/ */
/* /\* 	if(i==0) { *\/ */
/* /\* 	  chi2 = 0; *\/ */
/* /\* 	  muon.SetTrack(muon1);       *\/ */
/* /\* 	  cout<<"======"<<endl;      cout<<"muon 1"<<endl;      cout<<"======"<<endl;    } *\/ */
/* /\* 	if(i==1) { *\/ */
/* /\* 	  muon.SetTrack(muon2); *\/ */
/* /\* 	  cout<<"======"<<endl;      cout<<"muon 2"<<endl;      cout<<"======"<<endl;    } *\/ */
/* /\* 	if(i==2) { *\/ */
/* /\* 	  muon.SetTrack(muon3); *\/ */
/* /\* 	  cout<<"======"<<endl;      cout<<"muon 3"<<endl;      cout<<"======"<<endl;    } *\/ */

/* /\* 	TVectorD M1(muon.GivePars() ); *\/ */
/* /\* 	TVectorD X1(6);    X1.SetSub( 3, muon.GiveMom()  ); //X1.SetSub( 0,  muon.GivePos()  ); *\/ */
/* /\* 	long double x   = X1[0]; *\/ */
/* /\* 	long double y   = X1[1]; *\/ */
/* /\* 	long double z   = X1[2]; *\/ */
/* /\* 	long double px  = X1[3]; *\/ */
/* /\* 	long double py  = X1[4]; *\/ */
/* /\* 	long double pz  = X1[5]; *\/ */
/* /\* 	long double q  = muon.q; *\/ */
/* /\* 	long double aq = 0.6*q;  *\/ */
/* /\* 	long double pt2        = px*px + py*py; *\/ */
/* /\* 	long double pt         = sqrt(pt2); *\/ */
/* /\* 	long double p2         = pt2 + pz*pz; *\/ */
/* /\* 	long double p          = sqrt(p2);  *\/ */
/* /\* 	long double p3         = p2*p;  *\/ */
/* /\* 	long double px0        = px + aq*y; *\/ */
/* /\* 	long double py0        = py - aq*x; *\/ */
/* /\* 	long double pt02       = px0*px0 + py0*py0; *\/ */
/* /\* 	long double pt0        = sqrt(pt02); *\/ */
/* /\* 	long double phi        = atan2(py,px); *\/ */
/* /\* 	long double phi0       = atan2(py0,px0) ; *\/ */
/* /\* 	long double deltaphi   = phi-phi0 ; *\/ */
/* /\* 	if(     deltaphi >  TMath::Pi() ) deltaphi -= 2*TMath::Pi() ; *\/ */
/* /\* 	else if(deltaphi < -TMath::Pi() ) deltaphi += 2*TMath::Pi() ; *\/ */
/* /\* 	long double l      = deltaphi * pt /aq ; *\/ */
/* /\* 	long double d0     = (pt0 - pt)  /aq; *\/ */
/* /\* 	long double z0     = z - l*pz/pt;// was wrong!!! *\/ */
/* /\* 	long double qoverp = q / p; *\/ */
/* /\* 	long double theta  = atan2(pt,pz); *\/ */

/* /\* 	TVectorD h1(5);    h1[0] = d0;    h1[1] = z0;    h1[2] = phi0;    h1[3] = theta;    h1[4] = qoverp; *\/ */
/* /\* 	TVectorD r1(5);    r1 = M1 - h1;    *\/ */
/* /\* 	TMatrixD V1( muon.GiveCov() );    V1.SetTol(1.e-50);    V1.Invert(); *\/ */
/* /\* 	TVectorD tmp(5);    tmp = V1*r1; *\/ */
/* /\* 	chi2 += r1*tmp; *\/ */

/* /\* 	TMatrixD A;   A.ResizeTo(3,5); *\/ */
/* /\* 	TMatrixD B;   B.ResizeTo(3,5); *\/ */
/* /\* 	// the row with dx *\/ */
/* /\* 	A[0][0] = -py0/pt0;   *\/ */
/* /\* 	A[0][1] = -pz*px0/pt02; *\/ */
/* /\* 	A[0][2] = -aq*px0/pt02; *\/ */
/* /\* 	A[0][3] =  0; *\/ */
/* /\* 	A[0][4] =  0; *\/ */
/* /\* 	// the row with dy *\/ */
/* /\* 	A[1][0] =  px0/pt0;  *\/ */
/* /\* 	A[1][1] = -pz*py0/pt02; *\/ */
/* /\* 	A[1][2] = -aq*py0/pt02; *\/ */
/* /\* 	A[1][3] =  0; *\/ */
/* /\* 	A[1][4] =  0; *\/ */
/* /\* 	// the row with dz *\/ */
/* /\* 	A[2][0] =  0;  *\/ */
/* /\* 	A[2][1] =  1; *\/ */
/* /\* 	A[2][2] =  0; *\/ */
/* /\* 	A[2][3] =  0; *\/ */
/* /\* 	A[2][4] =  0; *\/ */
/* /\* 	// the row with px *\/ */
/* /\* 	B[0][0] =  (px0/pt0 - px/pt) / aq; *\/ */
/* /\* 	B[0][1] = -pz*(py0/pt02 - py/pt2)/aq; *\/ */
/* /\* 	B[0][2] = -py0/pt02; *\/ */
/* /\* 	B[0][3] =  px/p2;  //different from wouter -> check again *\/ */
/* /\* 	B[0][4] = -q*px/p3; // different from wouter  *\/ */
/* /\* 	// the row with py *\/ */
/* /\* 	B[1][0] = (py0/pt0 - py/pt) / aq;  *\/ */
/* /\* 	B[1][1] =  pz*(px0/pt02 - px/pt2)/aq; *\/ */
/* /\* 	B[1][2] =  px0/pt02; *\/ */
/* /\* 	B[1][3] =  py/p2; // different from wouter *\/ */
/* /\* 	B[1][4] = -q*py/p3;// different from wouter *\/ */
/* /\* 	// the row with pz *\/ */
/* /\* 	B[2][0] = 0; *\/ */
/* /\* 	B[2][1] = -l/pt; *\/ */
/* /\* 	B[2][2] = 0; *\/ */
/* /\* 	B[2][3] = -pz/p2; // different from wouter *\/ */
/* /\* 	B[2][4] = -q*pz/p3; *\/ */

/* /\* 	H1T.SetSub(       0, 5*i, A); *\/ */
/* /\* 	H1T.SetSub( 3*i + 3, 5*i, B); *\/ */

/* /\* 	/\\* cout<<"measured:"<<endl; *\\/ *\/ */
/* /\* 	/\\* M1.Print(); *\\/ *\/ */
/* /\* 	/\\* cout<<"predicted:"<<endl; *\\/ *\/ */
/* /\* 	/\\* h1.Print(); *\\/ *\/ */
/* /\* 	/\\* cout<<"covariance matrix of track:"<<endl; *\\/ *\/ */
/* /\* 	/\\* V1.Print(); *\\/ *\/ */
/* /\* 	/\\* cout<<"residual:"<<endl; *\\/ *\/ */
/* /\* 	/\\* r1.Print(); *\\/ *\/ */
/* /\* 	/\\* cout<<"chi2 = "<<chi2<<endl; *\\/ *\/ */
/* /\* 	/\\* cout<<"H:"<<endl; *\\/ *\/ */
/* /\* 	/\\* H1.Print(); *\\/ *\/ */
/* /\* 	/\\* cout<<"H transpose:"<<endl; *\\/ *\/ */
/* /\* 	/\\* H1T.Print(); *\\/ *\/ */
/* /\* 	/\\* cout<<"l = "<<l <<endl;    *\\/ *\/ */
/* /\* 	/\\* cout<<"q = "<<q <<endl;    *\\/ *\/ */

/* /\* 	/\\* A.Print(); *\\/ *\/ */
/* /\* 	/\\* B.Print(); *\\/ *\/ */
/* /\*       } *\/ */

/* /\*     //H1T.Print(); *\/ */
/* /\*     //  HT.Print(); *\/ */

/* /\*     /\\* TMatrixD tmp(12,15); *\\/ *\/ */
/* /\*     /\\* tmp = H1T - HT; *\\/ *\/ */
/* /\*     /\\* tmp.Print(); *\\/ *\/ */

/* /\*     /\\* H1.Transpose(H1T); *\\/ *\/ */

/* /\*   } *\/ */
/* /\* }; *\/ */

/* /\*  /\\* C[0][0] = 100; *\\/ *\/ */
/* /\*     /\\* C[1][1] = 100; *\\/ *\/ */
/* /\*     /\\* C[2][2] = 100; *\\/ *\/ */
/* /\*     /\\* C[3][3] = C[4][4] = C[5][5] = C[6][6] = C[7][7] = C[8][8] = C[9][9] = 200e3; // 200GeV *\\/ *\/ */
/* /\*     //    I.UnitMatrix(); *\/ */

/* /\*    /\\* // Update the residual matrix *\\/ *\/ */
/* /\*     /\\* R  = V + H*C*HT; *\\/ *\/ */
/* /\*     /\\* R.SetTol(1.e-30); R.Invert(); *\\/ *\/ */
/* /\*     /\\* //Ri = R; Ri.SetTol(1.e-23); Ri.Invert(); *\\/ *\/ */

/* /\*     /\\* // residual *\\/ *\/ */
/* /\*     /\\* r = M - h; *\\/ *\/ */

/* /\*     /\\* // gain matrix *\\/ *\/ */
/* /\*     /\\* K = C*HT*R; *\\/ *\/ */

/* /\*     /\\* // update helix parameters *\\/ *\/ */
/* /\*     /\\* TVectorF dX(12); *\\/ *\/ */
/* /\*     /\\* dX = K*r; *\\/ *\/ */
/* /\*     /\\* X = X + dX; *\\/ *\/ */

/* /\*     /\\* /\\\* // update covariance matrix *\\\/ *\\/ *\/ */
/* /\*     /\\* /\\\* TMatrixF tmp(12,12); *\\\/ *\\/ *\/ */
/* /\*     /\\* /\\\* tmp = I - K*H; *\\\/ *\\/ *\/ */
/* /\*     /\\* /\\\* tmp *= C; *\\\/ *\\/ *\/ */
/* /\*     /\\* /\\\* C = tmp; *\\\/ *\\/ *\/ */

/* for(int i=0;i<15;i++) */
/*     for(int j=0;j<15;j++){ */
/* 	h[i] = r[i] = 0; */
/* 	if(i<12){ */
/* 	  dX[i] = halfdchi2dx[i] = 0; */
/* 	  HT[i][j] = 0; */
/* 	  if(j<12) halfd2chi2dx2[i][j] = 0; */
/* 	} */
/* 	if(j<12) H[i][j] = 0; */
/*     } */

#endif
