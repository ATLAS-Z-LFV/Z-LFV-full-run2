#ifndef _bestEta_h_
#define _bestEta_h_
#include "TLorentzVector.h"
double caloRadius(int sample, const float eta);
float eta_PVcorr(int sample, float etas, float Zpv);
float bestEta(float etas1, float etaHPV, float vx_z, int author, int nSingleTrackConv, int nDoubleTrackConv, int convtrk1nPixHits,
              int convtrk1nSCTHits, int convtrk2nPixHits, int convtrk2nSCTHits);
void boosts(const TLorentzVector& muO, const TLorentzVector& phelO, const TVector3& MET, double& costhetaTauMu,
            double& costhetaTauPh, double& boostedTauM, double& boostedWM, double& dPhiW);

#endif
