#ifndef HAVE_UTIL_H
#define HAVE_UTIL_H

#include <vector>
#include "TMatrix.h"
#include "TVector.h"

using TMatrixD = TMatrixT<double>;

long double deltaPhi(const long double phi1, const long double phi2);
std::pair<TVectorD, TMatrixD> positionToHelix(const TVectorD &X, double q);

#endif
