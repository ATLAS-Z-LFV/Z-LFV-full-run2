//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Nov 26 23:02:57 2013 by ROOT version 5.30/05
// from TTree physics/physics
// found on file: v6.root
//////////////////////////////////////////////////////////

#ifndef physv6_h
#define physv6_h

#include <TROOT.h>
#include <TChain.h>
#include <TTree.h>
#include <TFile.h>
#include <vector>
using namespace std;

class physv6 {
  public:
    TTree *fChain;   //! pointer to the analyzed TTree or TChain
    Int_t fCurrent;  //! current Tree number in a TChain

    // Declaration of leaf types
    Bool_t EF_2mu13;
    Bool_t EF_e12Tvh_medium1_mu8;
    Bool_t EF_e24vhi_loose1_mu8;
    Bool_t EF_e24vhi_medium1;
    Bool_t EF_e45_medium1;
    Bool_t EF_e60_medium1;
    Bool_t EF_mu10i_g10_medium;
    Bool_t EF_mu10i_g10_medium_TauMass;
    Bool_t EF_mu10i_loose_g12Tvh_medium;
    Bool_t EF_mu10i_loose_g12Tvh_medium_TauMass;
    Bool_t EF_mu18_tight_mu8_EFFS;
    Bool_t EF_mu20i_tight_g5_loose_TauMass;
    Bool_t EF_mu20i_tight_g5_medium;
    Bool_t EF_mu20i_tight_g5_medium_TauMass;
    Bool_t EF_mu24i_tight;
    Bool_t EF_mu36_tight;
    Bool_t EF_mu4T;
    Bool_t EF_mu4Ti_g20Tvh_medium;
    Bool_t EF_mu4Ti_g20Tvh_medium_TauMass;
    Bool_t EF_tau20_medium1_mu15;
    Bool_t EF_tau29Ti_medium1_xe55_tclcw;
    Bool_t EF_xe80T;
    Bool_t EF_xe80_tclcw;
    UInt_t RunNumber;
    UInt_t EventNumber;
    UInt_t timestamp;
    UInt_t timestamp_ns;
    UInt_t lbn;
    UInt_t bcid;
    UInt_t detmask0;
    UInt_t detmask1;
    Float_t actualIntPerXing;
    Float_t averageIntPerXing;
    UInt_t pixelFlags;
    UInt_t sctFlags;
    UInt_t trtFlags;
    UInt_t larFlags;
    UInt_t tileFlags;
    UInt_t muonFlags;
    UInt_t fwdFlags;
    UInt_t coreFlags;
    UInt_t pixelError;
    UInt_t sctError;
    UInt_t trtError;
    UInt_t larError;
    UInt_t tileError;
    UInt_t muonError;
    UInt_t fwdError;
    UInt_t coreError;
    Bool_t streamDecision_Egamma;
    Bool_t streamDecision_Muons;
    Bool_t streamDecision_JetTauEtmiss;
    UInt_t l1id;
    Bool_t isSimulation;
    Bool_t isCalibration;
    Bool_t isTestBeam;
    Int_t trig_EF_trigmuonef_n;
    vector<int> *trig_EF_trigmuonef_track_n;
    vector<vector<int> > *trig_EF_trigmuonef_track_MuonType;
    vector<vector<float> > *trig_EF_trigmuonef_track_MS_pt;
    vector<vector<float> > *trig_EF_trigmuonef_track_MS_eta;
    vector<vector<float> > *trig_EF_trigmuonef_track_MS_phi;
    vector<vector<int> > *trig_EF_trigmuonef_track_MS_hasMS;
    vector<vector<float> > *trig_EF_trigmuonef_track_SA_pt;
    vector<vector<float> > *trig_EF_trigmuonef_track_SA_eta;
    vector<vector<float> > *trig_EF_trigmuonef_track_SA_phi;
    vector<vector<int> > *trig_EF_trigmuonef_track_SA_hasSA;
    vector<vector<float> > *trig_EF_trigmuonef_track_CB_pt;
    vector<vector<float> > *trig_EF_trigmuonef_track_CB_eta;
    vector<vector<float> > *trig_EF_trigmuonef_track_CB_phi;
    vector<vector<int> > *trig_EF_trigmuonef_track_CB_hasCB;
    Int_t trig_EF_trigmuonefisolation_n;
    vector<float> *trig_EF_trigmuonefisolation_sumTrkPtCone02;
    vector<float> *trig_EF_trigmuonefisolation_sumTrkPtCone03;
    vector<int> *trig_EF_trigmuonefisolation_trackPosition;
    vector<int> *trig_EF_trigmuonefisolation_efinfo_index;
    Int_t el_n;
    vector<float> *el_E;
    vector<float> *el_Et;
    vector<float> *el_pt;
    vector<float> *el_m;
    vector<float> *el_eta;
    vector<float> *el_phi;
    vector<float> *el_px;
    vector<float> *el_py;
    vector<float> *el_pz;
    vector<float> *el_charge;
    vector<int> *el_author;
    vector<unsigned int> *el_isEM;
    vector<unsigned int> *el_isEMLoose;
    vector<unsigned int> *el_isEMMedium;
    vector<unsigned int> *el_isEMTight;
    vector<unsigned int> *el_OQ;
    vector<int> *el_convFlag;
    vector<int> *el_isConv;
    vector<int> *el_nConv;
    vector<int> *el_nSingleTrackConv;
    vector<int> *el_nDoubleTrackConv;
    vector<int> *el_mediumWithoutTrack;
    vector<int> *el_mediumIsoWithoutTrack;
    vector<int> *el_tightWithoutTrack;
    vector<int> *el_tightIsoWithoutTrack;
    vector<int> *el_loose;
    vector<int> *el_looseIso;
    vector<int> *el_medium;
    vector<int> *el_mediumIso;
    vector<int> *el_tight;
    vector<int> *el_tightIso;
    vector<int> *el_loosePP;
    vector<int> *el_loosePPIso;
    vector<int> *el_mediumPP;
    vector<int> *el_mediumPPIso;
    vector<int> *el_tightPP;
    vector<int> *el_tightPPIso;
    vector<int> *el_goodOQ;
    vector<float> *el_Ethad;
    vector<float> *el_Ethad1;
    vector<float> *el_f1;
    vector<float> *el_f1core;
    vector<float> *el_Emins1;
    vector<float> *el_fside;
    vector<float> *el_Emax2;
    vector<float> *el_ws3;
    vector<float> *el_wstot;
    vector<float> *el_emaxs1;
    vector<float> *el_deltaEs;
    vector<float> *el_E233;
    vector<float> *el_E237;
    vector<float> *el_E277;
    vector<float> *el_weta2;
    vector<float> *el_f3;
    vector<float> *el_f3core;
    vector<float> *el_rphiallcalo;
    vector<float> *el_Etcone45;
    vector<float> *el_Etcone15;
    vector<float> *el_Etcone20;
    vector<float> *el_Etcone25;
    vector<float> *el_Etcone30;
    vector<float> *el_Etcone35;
    vector<float> *el_Etcone40;
    vector<float> *el_ptcone20;
    vector<float> *el_ptcone30;
    vector<float> *el_ptcone40;
    vector<float> *el_nucone20;
    vector<float> *el_nucone30;
    vector<float> *el_nucone40;
    vector<float> *el_Etcone15_pt_corrected;
    vector<float> *el_Etcone20_pt_corrected;
    vector<float> *el_Etcone25_pt_corrected;
    vector<float> *el_Etcone30_pt_corrected;
    vector<float> *el_Etcone35_pt_corrected;
    vector<float> *el_Etcone40_pt_corrected;
    vector<float> *el_convanglematch;
    vector<float> *el_convtrackmatch;
    vector<int> *el_hasconv;
    vector<float> *el_convvtxx;
    vector<float> *el_convvtxy;
    vector<float> *el_convvtxz;
    vector<float> *el_Rconv;
    vector<float> *el_zconv;
    vector<float> *el_convvtxchi2;
    vector<float> *el_pt1conv;
    vector<int> *el_convtrk1nBLHits;
    vector<int> *el_convtrk1nPixHits;
    vector<int> *el_convtrk1nSCTHits;
    vector<int> *el_convtrk1nTRTHits;
    vector<float> *el_pt2conv;
    vector<int> *el_convtrk2nBLHits;
    vector<int> *el_convtrk2nPixHits;
    vector<int> *el_convtrk2nSCTHits;
    vector<int> *el_convtrk2nTRTHits;
    vector<float> *el_ptconv;
    vector<float> *el_pzconv;
    vector<float> *el_pos7;
    vector<float> *el_etacorrmag;
    vector<float> *el_deltaeta1;
    vector<float> *el_deltaeta2;
    vector<float> *el_deltaphi2;
    vector<float> *el_deltaphiRescaled;
    vector<float> *el_deltaPhiFromLast;
    vector<float> *el_deltaPhiRot;
    vector<float> *el_expectHitInBLayer;
    vector<float> *el_trackd0_physics;
    vector<float> *el_etaSampling1;
    vector<float> *el_reta;
    vector<float> *el_rphi;
    vector<float> *el_topoEtcone20;
    vector<float> *el_topoEtcone30;
    vector<float> *el_topoEtcone40;
    vector<float> *el_materialTraversed;
    vector<float> *el_EtringnoisedR03sig2;
    vector<float> *el_EtringnoisedR03sig3;
    vector<float> *el_EtringnoisedR03sig4;
    vector<float> *el_ptcone20_zpv05;
    vector<float> *el_ptcone30_zpv05;
    vector<float> *el_ptcone40_zpv05;
    vector<float> *el_nucone20_zpv05;
    vector<float> *el_nucone30_zpv05;
    vector<float> *el_nucone40_zpv05;
    vector<double> *el_isolationlikelihoodjets;
    vector<double> *el_isolationlikelihoodhqelectrons;
    vector<double> *el_electronweight;
    vector<double> *el_electronbgweight;
    vector<double> *el_softeweight;
    vector<double> *el_softebgweight;
    vector<double> *el_neuralnet;
    vector<double> *el_Hmatrix;
    vector<double> *el_Hmatrix5;
    vector<double> *el_adaboost;
    vector<double> *el_softeneuralnet;
    vector<double> *el_ringernn;
    vector<float> *el_zvertex;
    vector<float> *el_errz;
    vector<float> *el_etap;
    vector<float> *el_depth;
    vector<float> *el_Es0;
    vector<float> *el_etas0;
    vector<float> *el_phis0;
    vector<float> *el_Es1;
    vector<float> *el_etas1;
    vector<float> *el_phis1;
    vector<float> *el_Es2;
    vector<float> *el_etas2;
    vector<float> *el_phis2;
    vector<float> *el_Es3;
    vector<float> *el_etas3;
    vector<float> *el_phis3;
    vector<float> *el_E_PreSamplerB;
    vector<float> *el_E_EMB1;
    vector<float> *el_E_EMB2;
    vector<float> *el_E_EMB3;
    vector<float> *el_E_PreSamplerE;
    vector<float> *el_E_EME1;
    vector<float> *el_E_EME2;
    vector<float> *el_E_EME3;
    vector<float> *el_E_HEC0;
    vector<float> *el_E_HEC1;
    vector<float> *el_E_HEC2;
    vector<float> *el_E_HEC3;
    vector<float> *el_E_TileBar0;
    vector<float> *el_E_TileBar1;
    vector<float> *el_E_TileBar2;
    vector<float> *el_E_TileGap1;
    vector<float> *el_E_TileGap2;
    vector<float> *el_E_TileGap3;
    vector<float> *el_E_TileExt0;
    vector<float> *el_E_TileExt1;
    vector<float> *el_E_TileExt2;
    vector<float> *el_E_FCAL0;
    vector<float> *el_E_FCAL1;
    vector<float> *el_E_FCAL2;
    vector<float> *el_cl_E;
    vector<float> *el_cl_pt;
    vector<float> *el_cl_eta;
    vector<float> *el_cl_phi;
    vector<double> *el_cl_etaCalo;
    vector<double> *el_cl_phiCalo;
    vector<float> *el_firstEdens;
    vector<float> *el_cellmaxfrac;
    vector<float> *el_longitudinal;
    vector<float> *el_secondlambda;
    vector<float> *el_lateral;
    vector<float> *el_secondR;
    vector<float> *el_centerlambda;
    vector<float> *el_rawcl_Es0;
    vector<float> *el_rawcl_etas0;
    vector<float> *el_rawcl_phis0;
    vector<float> *el_rawcl_Es1;
    vector<float> *el_rawcl_etas1;
    vector<float> *el_rawcl_phis1;
    vector<float> *el_rawcl_Es2;
    vector<float> *el_rawcl_etas2;
    vector<float> *el_rawcl_phis2;
    vector<float> *el_rawcl_Es3;
    vector<float> *el_rawcl_etas3;
    vector<float> *el_rawcl_phis3;
    vector<float> *el_rawcl_E;
    vector<float> *el_rawcl_pt;
    vector<float> *el_rawcl_eta;
    vector<float> *el_rawcl_phi;
    vector<float> *el_trackd0;
    vector<float> *el_trackz0;
    vector<float> *el_trackphi;
    vector<float> *el_tracktheta;
    vector<float> *el_trackqoverp;
    vector<float> *el_trackpt;
    vector<float> *el_tracketa;
    vector<float> *el_trackfitchi2;
    vector<int> *el_trackfitndof;
    vector<int> *el_nBLHits;
    vector<int> *el_nPixHits;
    vector<int> *el_nSCTHits;
    vector<int> *el_nTRTHits;
    vector<int> *el_nTRTHighTHits;
    vector<int> *el_nTRTXenonHits;
    vector<int> *el_nPixHoles;
    vector<int> *el_nSCTHoles;
    vector<int> *el_nTRTHoles;
    vector<int> *el_nPixelDeadSensors;
    vector<int> *el_nSCTDeadSensors;
    vector<int> *el_nBLSharedHits;
    vector<int> *el_nPixSharedHits;
    vector<int> *el_nSCTSharedHits;
    vector<int> *el_nBLayerSplitHits;
    vector<int> *el_nPixSplitHits;
    vector<int> *el_nBLayerOutliers;
    vector<int> *el_nPixelOutliers;
    vector<int> *el_nSCTOutliers;
    vector<int> *el_nTRTOutliers;
    vector<int> *el_nTRTHighTOutliers;
    vector<int> *el_nContribPixelLayers;
    vector<int> *el_nGangedPixels;
    vector<int> *el_nGangedFlaggedFakes;
    vector<int> *el_nPixelSpoiltHits;
    vector<int> *el_nSCTDoubleHoles;
    vector<int> *el_nSCTSpoiltHits;
    vector<int> *el_expectBLayerHit;
    vector<int> *el_nSiHits;
    vector<float> *el_TRTHighTHitsRatio;
    vector<float> *el_TRTHighTOutliersRatio;
    vector<float> *el_pixeldEdx;
    vector<int> *el_nGoodHitsPixeldEdx;
    vector<float> *el_massPixeldEdx;
    vector<vector<float> > *el_likelihoodsPixeldEdx;
    vector<float> *el_eProbabilityComb;
    vector<float> *el_eProbabilityHT;
    vector<float> *el_eProbabilityToT;
    vector<float> *el_eProbabilityBrem;
    vector<float> *el_vertweight;
    vector<float> *el_vertx;
    vector<float> *el_verty;
    vector<float> *el_vertz;
    vector<float> *el_trackd0beam;
    vector<float> *el_trackz0beam;
    vector<float> *el_tracksigd0beam;
    vector<float> *el_tracksigz0beam;
    vector<float> *el_trackd0pv;
    vector<float> *el_trackz0pv;
    vector<float> *el_tracksigd0pv;
    vector<float> *el_tracksigz0pv;
    vector<float> *el_trackIPEstimate_d0_biasedpvunbiased;
    vector<float> *el_trackIPEstimate_z0_biasedpvunbiased;
    vector<float> *el_trackIPEstimate_sigd0_biasedpvunbiased;
    vector<float> *el_trackIPEstimate_sigz0_biasedpvunbiased;
    vector<float> *el_trackIPEstimate_d0_unbiasedpvunbiased;
    vector<float> *el_trackIPEstimate_z0_unbiasedpvunbiased;
    vector<float> *el_trackIPEstimate_sigd0_unbiasedpvunbiased;
    vector<float> *el_trackIPEstimate_sigz0_unbiasedpvunbiased;
    vector<float> *el_trackd0pvunbiased;
    vector<float> *el_trackz0pvunbiased;
    vector<float> *el_tracksigd0pvunbiased;
    vector<float> *el_tracksigz0pvunbiased;
    vector<float> *el_Unrefittedtrack_d0;
    vector<float> *el_Unrefittedtrack_z0;
    vector<float> *el_Unrefittedtrack_phi;
    vector<float> *el_Unrefittedtrack_theta;
    vector<float> *el_Unrefittedtrack_qoverp;
    vector<float> *el_Unrefittedtrack_pt;
    vector<float> *el_Unrefittedtrack_eta;
    vector<float> *el_theta_LM;
    vector<float> *el_qoverp_LM;
    vector<float> *el_theta_err_LM;
    vector<float> *el_qoverp_err_LM;
    vector<int> *el_hastrack;
    vector<float> *el_deltaEmax2;
    vector<float> *el_calibHitsShowerDepth;
    vector<unsigned int> *el_isIso;
    vector<float> *el_mvaptcone20;
    vector<float> *el_mvaptcone30;
    vector<float> *el_mvaptcone40;
    vector<float> *el_CaloPointing_eta;
    vector<float> *el_CaloPointing_sigma_eta;
    vector<float> *el_CaloPointing_zvertex;
    vector<float> *el_CaloPointing_sigma_zvertex;
    vector<float> *el_HPV_eta;
    vector<float> *el_HPV_sigma_eta;
    vector<float> *el_HPV_zvertex;
    vector<float> *el_HPV_sigma_zvertex;
    vector<float> *el_topoEtcone60;
    vector<float> *el_ES0_real;
    vector<float> *el_ES1_real;
    vector<float> *el_ES2_real;
    vector<float> *el_ES3_real;
    vector<float> *el_EcellS0;
    vector<float> *el_etacellS0;
    vector<float> *el_Etcone40_ED_corrected;
    vector<float> *el_Etcone40_corrected;
    vector<float> *el_topoEtcone20_corrected;
    vector<float> *el_topoEtcone30_corrected;
    vector<float> *el_topoEtcone40_corrected;
    vector<float> *el_ED_median;
    vector<float> *el_ED_sigma;
    vector<float> *el_ED_Njets;
    vector<float> *el_jet_dr;
    vector<float> *el_jet_E;
    vector<float> *el_jet_pt;
    vector<float> *el_jet_m;
    vector<float> *el_jet_eta;
    vector<float> *el_jet_phi;
    vector<int> *el_jet_matched;
    Int_t tau_n;
    vector<float> *tau_Et;
    vector<float> *tau_pt;
    vector<float> *tau_m;
    vector<float> *tau_eta;
    vector<float> *tau_phi;
    vector<float> *tau_charge;
    vector<float> *tau_BDTEleScore;
    vector<float> *tau_BDTJetScore;
    vector<float> *tau_likelihood;
    vector<float> *tau_SafeLikelihood;
    vector<int> *tau_electronVetoLoose;
    vector<int> *tau_electronVetoMedium;
    vector<int> *tau_electronVetoTight;
    vector<int> *tau_muonVeto;
    vector<int> *tau_tauLlhLoose;
    vector<int> *tau_tauLlhMedium;
    vector<int> *tau_tauLlhTight;
    vector<int> *tau_JetBDTSigLoose;
    vector<int> *tau_JetBDTSigMedium;
    vector<int> *tau_JetBDTSigTight;
    vector<int> *tau_EleBDTLoose;
    vector<int> *tau_EleBDTMedium;
    vector<int> *tau_EleBDTTight;
    vector<int> *tau_author;
    vector<int> *tau_RoIWord;
    vector<int> *tau_nProng;
    vector<int> *tau_numTrack;
    vector<int> *tau_seedCalo_numTrack;
    vector<int> *tau_seedCalo_nWideTrk;
    vector<int> *tau_nOtherTrk;
    vector<int> *tau_track_atTJVA_n;
    vector<int> *tau_seedCalo_wideTrk_atTJVA_n;
    vector<int> *tau_otherTrk_atTJVA_n;
    vector<int> *tau_track_n;
    vector<int> *tau_seedCalo_track_n;
    vector<int> *tau_seedCalo_wideTrk_n;
    vector<int> *tau_otherTrk_n;
    Int_t jet_akt4topoLC_n;
    vector<float> *jet_akt4topoLC_E;
    vector<float> *jet_akt4topoLC_pt;
    vector<float> *jet_akt4topoLC_m;
    vector<float> *jet_akt4topoLC_eta;
    vector<float> *jet_akt4topoLC_phi;
    vector<float> *jet_akt4topoLC_WIDTH;
    vector<float> *jet_akt4topoLC_n90;
    vector<float> *jet_akt4topoLC_Timing;
    vector<float> *jet_akt4topoLC_LArQuality;
    vector<float> *jet_akt4topoLC_nTrk;
    vector<float> *jet_akt4topoLC_sumPtTrk;
    vector<float> *jet_akt4topoLC_OriginIndex;
    vector<float> *jet_akt4topoLC_HECQuality;
    vector<float> *jet_akt4topoLC_NegativeE;
    vector<float> *jet_akt4topoLC_AverageLArQF;
    vector<float> *jet_akt4topoLC_BCH_CORR_CELL;
    vector<float> *jet_akt4topoLC_BCH_CORR_DOTX;
    vector<float> *jet_akt4topoLC_BCH_CORR_JET;
    vector<float> *jet_akt4topoLC_BCH_CORR_JET_FORCELL;
    vector<float> *jet_akt4topoLC_ENG_BAD_CELLS;
    vector<float> *jet_akt4topoLC_N_BAD_CELLS;
    vector<float> *jet_akt4topoLC_N_BAD_CELLS_CORR;
    vector<float> *jet_akt4topoLC_BAD_CELLS_CORR_E;
    vector<float> *jet_akt4topoLC_NumTowers;
    vector<float> *jet_akt4topoLC_ootFracCells5;
    vector<float> *jet_akt4topoLC_ootFracCells10;
    vector<float> *jet_akt4topoLC_ootFracClusters5;
    vector<float> *jet_akt4topoLC_ootFracClusters10;
    vector<float> *jet_akt4topoLC_emfrac;
    vector<float> *jet_akt4topoLC_LCJES;
    vector<float> *jet_akt4topoLC_LCJES_EtaCorr;
    vector<float> *jet_akt4topoLC_emscale_E;
    vector<float> *jet_akt4topoLC_emscale_pt;
    vector<float> *jet_akt4topoLC_emscale_m;
    vector<float> *jet_akt4topoLC_emscale_eta;
    vector<float> *jet_akt4topoLC_emscale_phi;
    vector<float> *jet_akt4topoLC_jvtx_x;
    vector<float> *jet_akt4topoLC_jvtx_y;
    vector<float> *jet_akt4topoLC_jvtx_z;
    vector<int> *jet_akt4topoLC_Nconst;
    vector<vector<float> > *jet_akt4topoLC_ptconst_default;
    vector<vector<float> > *jet_akt4topoLC_econst_default;
    vector<vector<float> > *jet_akt4topoLC_etaconst_default;
    vector<vector<float> > *jet_akt4topoLC_phiconst_default;
    vector<vector<float> > *jet_akt4topoLC_weightconst_default;
    vector<float> *jet_akt4topoLC_constscale_E;
    vector<float> *jet_akt4topoLC_constscale_pt;
    vector<float> *jet_akt4topoLC_constscale_m;
    vector<float> *jet_akt4topoLC_constscale_eta;
    vector<float> *jet_akt4topoLC_constscale_phi;
    vector<float> *jet_akt4topoLC_flavor_weight_Comb;
    vector<float> *jet_akt4topoLC_flavor_weight_GbbNN;
    vector<float> *jet_akt4topoLC_flavor_weight_IP2D;
    vector<float> *jet_akt4topoLC_flavor_weight_IP3D;
    vector<float> *jet_akt4topoLC_flavor_weight_JetFitterCOMBNN;
    vector<float> *jet_akt4topoLC_flavor_weight_JetFitterTagNN;
    vector<float> *jet_akt4topoLC_flavor_weight_MV1;
    vector<float> *jet_akt4topoLC_flavor_weight_MV2;
    vector<float> *jet_akt4topoLC_flavor_weight_SV0;
    vector<float> *jet_akt4topoLC_flavor_weight_SV1;
    vector<float> *jet_akt4topoLC_flavor_weight_SV2;
    vector<float> *jet_akt4topoLC_flavor_weight_SecondSoftMuonTagChi2;
    vector<float> *jet_akt4topoLC_flavor_weight_SoftMuonTagChi2;
    vector<int> *jet_akt4topoLC_flavor_assoctrk_n;
    vector<vector<int> > *jet_akt4topoLC_flavor_assoctrk_index;
    vector<float> *jet_akt4topoLC_el_dr;
    vector<int> *jet_akt4topoLC_el_matched;
    vector<float> *jet_akt4topoLC_mu_dr;
    vector<int> *jet_akt4topoLC_mu_matched;
    vector<float> *jet_akt4topoLC_L1_dr;
    vector<int> *jet_akt4topoLC_L1_matched;
    vector<float> *jet_akt4topoLC_L2_dr;
    vector<int> *jet_akt4topoLC_L2_matched;
    vector<float> *jet_akt4topoLC_EF_dr;
    vector<int> *jet_akt4topoLC_EF_matched;
    vector<float> *jet_akt4topoLC_nTrk_pv0_1GeV;
    vector<float> *jet_akt4topoLC_sumPtTrk_pv0_1GeV;
    vector<float> *jet_akt4topoLC_nTrk_allpv_1GeV;
    vector<float> *jet_akt4topoLC_sumPtTrk_allpv_1GeV;
    vector<float> *jet_akt4topoLC_nTrk_pv0_500MeV;
    vector<float> *jet_akt4topoLC_sumPtTrk_pv0_500MeV;
    vector<float> *jet_akt4topoLC_trackWIDTH_pv0_1GeV;
    vector<float> *jet_akt4topoLC_trackWIDTH_allpv_1GeV;
    Float_t MET_Track_etx;
    Float_t MET_Track_ety;
    Float_t MET_Track_phi;
    Float_t MET_Track_et;
    Float_t MET_Track_sumet;
    Float_t MET_TauMuGamma_RefFinal_etx;
    Float_t MET_TauMuGamma_RefFinal_ety;
    Float_t MET_TauMuGamma_RefFinal_phi;
    Float_t MET_TauMuGamma_RefFinal_et;
    Float_t MET_TauMuGamma_RefFinal_sumet;
    Float_t MET_Default_RefFinal_etx;
    Float_t MET_Default_RefFinal_ety;
    Float_t MET_Default_RefFinal_phi;
    Float_t MET_Default_RefFinal_et;
    Float_t MET_Default_RefFinal_sumet;
    Int_t el_MET_TauMuGamma_comp_n;
    vector<vector<float> > *el_MET_TauMuGamma_comp_wpx;
    vector<vector<float> > *el_MET_TauMuGamma_comp_wpy;
    vector<vector<float> > *el_MET_TauMuGamma_comp_wet;
    vector<vector<unsigned int> > *el_MET_TauMuGamma_comp_statusWord;
    Int_t ph_MET_TauMuGamma_comp_n;
    vector<vector<float> > *ph_MET_TauMuGamma_comp_wpx;
    vector<vector<float> > *ph_MET_TauMuGamma_comp_wpy;
    vector<vector<float> > *ph_MET_TauMuGamma_comp_wet;
    vector<vector<unsigned int> > *ph_MET_TauMuGamma_comp_statusWord;
    Int_t mu_staco_MET_TauMuGamma_comp_n;
    vector<vector<float> > *mu_staco_MET_TauMuGamma_comp_wpx;
    vector<vector<float> > *mu_staco_MET_TauMuGamma_comp_wpy;
    vector<vector<float> > *mu_staco_MET_TauMuGamma_comp_wet;
    vector<vector<unsigned int> > *mu_staco_MET_TauMuGamma_comp_statusWord;
    Int_t mu_muid_MET_TauMuGamma_comp_n;
    vector<vector<float> > *mu_muid_MET_TauMuGamma_comp_wpx;
    vector<vector<float> > *mu_muid_MET_TauMuGamma_comp_wpy;
    vector<vector<float> > *mu_muid_MET_TauMuGamma_comp_wet;
    vector<vector<unsigned int> > *mu_muid_MET_TauMuGamma_comp_statusWord;
    Int_t mu_muon_MET_TauMuGamma_comp_n;
    vector<vector<float> > *mu_muon_MET_TauMuGamma_comp_wpx;
    vector<vector<float> > *mu_muon_MET_TauMuGamma_comp_wpy;
    vector<vector<float> > *mu_muon_MET_TauMuGamma_comp_wet;
    vector<vector<unsigned int> > *mu_muon_MET_TauMuGamma_comp_statusWord;
    Int_t tau_MET_TauMuGamma_comp_n;
    vector<vector<float> > *tau_MET_TauMuGamma_comp_wpx;
    vector<vector<float> > *tau_MET_TauMuGamma_comp_wpy;
    vector<vector<float> > *tau_MET_TauMuGamma_comp_wet;
    vector<vector<unsigned int> > *tau_MET_TauMuGamma_comp_statusWord;
    Int_t jet_AntiKt4LCTopo_MET_TauMuGamma_comp_n;
    vector<vector<float> > *jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wpx;
    vector<vector<float> > *jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wpy;
    vector<vector<float> > *jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wet;
    vector<vector<unsigned int> > *jet_AntiKt4LCTopo_MET_TauMuGamma_comp_statusWord;
    Int_t cl_MET_TauMuGamma_comp_n;
    vector<vector<float> > *cl_MET_TauMuGamma_comp_wpx;
    vector<vector<float> > *cl_MET_TauMuGamma_comp_wpy;
    vector<vector<float> > *cl_MET_TauMuGamma_comp_wet;
    vector<vector<unsigned int> > *cl_MET_TauMuGamma_comp_statusWord;
    Int_t trk_MET_TauMuGamma_comp_n;
    vector<vector<float> > *trk_MET_TauMuGamma_comp_wpx;
    vector<vector<float> > *trk_MET_TauMuGamma_comp_wpy;
    vector<vector<float> > *trk_MET_TauMuGamma_comp_wet;
    vector<vector<unsigned int> > *trk_MET_TauMuGamma_comp_statusWord;
    Int_t ph_n;
    vector<float> *ph_E;
    vector<float> *ph_Et;
    vector<float> *ph_pt;
    vector<float> *ph_m;
    vector<float> *ph_eta;
    vector<float> *ph_phi;
    vector<float> *ph_px;
    vector<float> *ph_py;
    vector<float> *ph_pz;
    vector<int> *ph_author;
    vector<int> *ph_isRecovered;
    vector<unsigned int> *ph_isEM;
    vector<unsigned int> *ph_isEMLoose;
    vector<unsigned int> *ph_isEMMedium;
    vector<unsigned int> *ph_isEMTight;
    vector<unsigned int> *ph_OQ;
    vector<int> *ph_convFlag;
    vector<int> *ph_isConv;
    vector<int> *ph_nConv;
    vector<int> *ph_nSingleTrackConv;
    vector<int> *ph_nDoubleTrackConv;
    vector<int> *ph_loose;
    vector<int> *ph_looseIso;
    vector<int> *ph_tight;
    vector<int> *ph_tightIso;
    vector<int> *ph_looseAR;
    vector<int> *ph_looseARIso;
    vector<int> *ph_tightAR;
    vector<int> *ph_tightARIso;
    vector<int> *ph_goodOQ;
    vector<float> *ph_Ethad;
    vector<float> *ph_Ethad1;
    vector<float> *ph_E033;
    vector<float> *ph_f1;
    vector<float> *ph_f1core;
    vector<float> *ph_Emins1;
    vector<float> *ph_fside;
    vector<float> *ph_Emax2;
    vector<float> *ph_ws3;
    vector<float> *ph_wstot;
    vector<float> *ph_E132;
    vector<float> *ph_E1152;
    vector<float> *ph_emaxs1;
    vector<float> *ph_deltaEs;
    vector<float> *ph_E233;
    vector<float> *ph_E237;
    vector<float> *ph_E277;
    vector<float> *ph_weta2;
    vector<float> *ph_f3;
    vector<float> *ph_f3core;
    vector<float> *ph_rphiallcalo;
    vector<float> *ph_Etcone45;
    vector<float> *ph_Etcone15;
    vector<float> *ph_Etcone20;
    vector<float> *ph_Etcone25;
    vector<float> *ph_Etcone30;
    vector<float> *ph_Etcone35;
    vector<float> *ph_Etcone40;
    vector<float> *ph_ptcone20;
    vector<float> *ph_ptcone30;
    vector<float> *ph_ptcone40;
    vector<float> *ph_nucone20;
    vector<float> *ph_nucone30;
    vector<float> *ph_nucone40;
    vector<float> *ph_Etcone15_pt_corrected;
    vector<float> *ph_Etcone20_pt_corrected;
    vector<float> *ph_Etcone25_pt_corrected;
    vector<float> *ph_Etcone30_pt_corrected;
    vector<float> *ph_Etcone35_pt_corrected;
    vector<float> *ph_Etcone40_pt_corrected;
    vector<float> *ph_convanglematch;
    vector<float> *ph_convtrackmatch;
    vector<int> *ph_hasconv;
    vector<float> *ph_convvtxx;
    vector<float> *ph_convvtxy;
    vector<float> *ph_convvtxz;
    vector<float> *ph_Rconv;
    vector<float> *ph_zconv;
    vector<float> *ph_convvtxchi2;
    vector<float> *ph_pt1conv;
    vector<int> *ph_convtrk1nBLHits;
    vector<int> *ph_convtrk1nPixHits;
    vector<int> *ph_convtrk1nSCTHits;
    vector<int> *ph_convtrk1nTRTHits;
    vector<float> *ph_pt2conv;
    vector<int> *ph_convtrk2nBLHits;
    vector<int> *ph_convtrk2nPixHits;
    vector<int> *ph_convtrk2nSCTHits;
    vector<int> *ph_convtrk2nTRTHits;
    vector<float> *ph_ptconv;
    vector<float> *ph_pzconv;
    vector<float> *ph_reta;
    vector<float> *ph_rphi;
    vector<float> *ph_topoEtcone20;
    vector<float> *ph_topoEtcone30;
    vector<float> *ph_topoEtcone40;
    vector<float> *ph_materialTraversed;
    vector<float> *ph_EtringnoisedR03sig2;
    vector<float> *ph_EtringnoisedR03sig3;
    vector<float> *ph_EtringnoisedR03sig4;
    vector<float> *ph_ptcone20_zpv05;
    vector<float> *ph_ptcone30_zpv05;
    vector<float> *ph_ptcone40_zpv05;
    vector<float> *ph_nucone20_zpv05;
    vector<float> *ph_nucone30_zpv05;
    vector<float> *ph_nucone40_zpv05;
    vector<double> *ph_isolationlikelihoodjets;
    vector<double> *ph_isolationlikelihoodhqelectrons;
    vector<double> *ph_loglikelihood;
    vector<double> *ph_photonweight;
    vector<double> *ph_photonbgweight;
    vector<double> *ph_neuralnet;
    vector<double> *ph_Hmatrix;
    vector<double> *ph_Hmatrix5;
    vector<double> *ph_adaboost;
    vector<double> *ph_ringernn;
    vector<float> *ph_zvertex;
    vector<float> *ph_errz;
    vector<float> *ph_etap;
    vector<float> *ph_depth;
    vector<float> *ph_cl_E;
    vector<float> *ph_cl_pt;
    vector<float> *ph_cl_eta;
    vector<float> *ph_cl_phi;
    vector<double> *ph_cl_etaCalo;
    vector<double> *ph_cl_phiCalo;
    vector<float> *ph_Es0;
    vector<float> *ph_etas0;
    vector<float> *ph_phis0;
    vector<float> *ph_Es1;
    vector<float> *ph_etas1;
    vector<float> *ph_phis1;
    vector<float> *ph_Es2;
    vector<float> *ph_etas2;
    vector<float> *ph_phis2;
    vector<float> *ph_Es3;
    vector<float> *ph_etas3;
    vector<float> *ph_phis3;
    vector<float> *ph_rawcl_Es0;
    vector<float> *ph_rawcl_etas0;
    vector<float> *ph_rawcl_phis0;
    vector<float> *ph_rawcl_Es1;
    vector<float> *ph_rawcl_etas1;
    vector<float> *ph_rawcl_phis1;
    vector<float> *ph_rawcl_Es2;
    vector<float> *ph_rawcl_etas2;
    vector<float> *ph_rawcl_phis2;
    vector<float> *ph_rawcl_Es3;
    vector<float> *ph_rawcl_etas3;
    vector<float> *ph_rawcl_phis3;
    vector<float> *ph_rawcl_E;
    vector<float> *ph_rawcl_pt;
    vector<float> *ph_rawcl_eta;
    vector<float> *ph_rawcl_phi;
    vector<float> *ph_convMatchDeltaEta1;
    vector<float> *ph_convMatchDeltaEta2;
    vector<float> *ph_convMatchDeltaPhi1;
    vector<float> *ph_convMatchDeltaPhi2;
    vector<vector<float> > *ph_rings_E;
    vector<int> *ph_vx_n;
    vector<vector<float> > *ph_vx_x;
    vector<vector<float> > *ph_vx_y;
    vector<vector<float> > *ph_vx_z;
    vector<vector<float> > *ph_vx_px;
    vector<vector<float> > *ph_vx_py;
    vector<vector<float> > *ph_vx_pz;
    vector<vector<float> > *ph_vx_E;
    vector<vector<float> > *ph_vx_m;
    vector<vector<int> > *ph_vx_nTracks;
    vector<vector<float> > *ph_vx_sumPt;
    vector<vector<vector<float> > > *ph_vx_convTrk_weight;
    vector<vector<int> > *ph_vx_convTrk_n;
    vector<vector<vector<int> > > *ph_vx_convTrk_fitter;
    vector<vector<vector<int> > > *ph_vx_convTrk_patternReco1;
    vector<vector<vector<int> > > *ph_vx_convTrk_patternReco2;
    vector<vector<vector<int> > > *ph_vx_convTrk_trackProperties;
    vector<vector<vector<int> > > *ph_vx_convTrk_particleHypothesis;
    vector<vector<vector<float> > > *ph_vx_convTrk_chi2;
    vector<vector<vector<int> > > *ph_vx_convTrk_ndof;
    vector<float> *ph_deltaEmax2;
    vector<float> *ph_calibHitsShowerDepth;
    vector<unsigned int> *ph_isIso;
    vector<float> *ph_mvaptcone20;
    vector<float> *ph_mvaptcone30;
    vector<float> *ph_mvaptcone40;
    vector<float> *ph_topoEtcone60;
    vector<vector<float> > *ph_vx_Chi2;
    vector<vector<float> > *ph_vx_Dcottheta;
    vector<vector<float> > *ph_vx_Dphi;
    vector<vector<float> > *ph_vx_Dist;
    vector<vector<float> > *ph_vx_DR1R2;
    vector<float> *ph_CaloPointing_eta;
    vector<float> *ph_CaloPointing_sigma_eta;
    vector<float> *ph_CaloPointing_zvertex;
    vector<float> *ph_CaloPointing_sigma_zvertex;
    vector<float> *ph_HPV_eta;
    vector<float> *ph_HPV_sigma_eta;
    vector<float> *ph_HPV_zvertex;
    vector<float> *ph_HPV_sigma_zvertex;
    vector<int> *ph_NN_passes;
    vector<float> *ph_NN_discriminant;
    vector<float> *ph_ES0_real;
    vector<float> *ph_ES1_real;
    vector<float> *ph_ES2_real;
    vector<float> *ph_ES3_real;
    vector<float> *ph_EcellS0;
    vector<float> *ph_etacellS0;
    vector<float> *ph_Etcone40_ED_corrected;
    vector<float> *ph_Etcone40_corrected;
    vector<float> *ph_topoEtcone20_corrected;
    vector<float> *ph_topoEtcone30_corrected;
    vector<float> *ph_topoEtcone40_corrected;
    vector<float> *ph_ED_median;
    vector<float> *ph_ED_sigma;
    vector<float> *ph_ED_Njets;
    vector<float> *ph_convIP;
    vector<float> *ph_convIPRev;
    vector<float> *ph_jet_dr;
    vector<float> *ph_jet_E;
    vector<float> *ph_jet_pt;
    vector<float> *ph_jet_m;
    vector<float> *ph_jet_eta;
    vector<float> *ph_jet_phi;
    vector<int> *ph_jet_matched;
    vector<float> *ph_topodr;
    vector<float> *ph_topopt;
    vector<float> *ph_topoeta;
    vector<float> *ph_topophi;
    vector<int> *ph_topomatched;
    vector<float> *ph_topoEMdr;
    vector<float> *ph_topoEMpt;
    vector<float> *ph_topoEMeta;
    vector<float> *ph_topoEMphi;
    vector<int> *ph_topoEMmatched;
    vector<int> *ph_el_index;
    Int_t mu_muon_n;
    vector<float> *mu_muon_E;
    vector<float> *mu_muon_pt;
    vector<float> *mu_muon_m;
    vector<float> *mu_muon_eta;
    vector<float> *mu_muon_phi;
    vector<float> *mu_muon_px;
    vector<float> *mu_muon_py;
    vector<float> *mu_muon_pz;
    vector<float> *mu_muon_charge;
    vector<unsigned short> *mu_muon_allauthor;
    vector<int> *mu_muon_author;
    vector<float> *mu_muon_beta;
    vector<float> *mu_muon_isMuonLikelihood;
    vector<float> *mu_muon_matchchi2;
    vector<int> *mu_muon_matchndof;
    vector<float> *mu_muon_etcone20;
    vector<float> *mu_muon_etcone30;
    vector<float> *mu_muon_etcone40;
    vector<float> *mu_muon_nucone20;
    vector<float> *mu_muon_nucone30;
    vector<float> *mu_muon_nucone40;
    vector<float> *mu_muon_ptcone20;
    vector<float> *mu_muon_ptcone30;
    vector<float> *mu_muon_ptcone40;
    vector<float> *mu_muon_etconeNoEm10;
    vector<float> *mu_muon_etconeNoEm20;
    vector<float> *mu_muon_etconeNoEm30;
    vector<float> *mu_muon_etconeNoEm40;
    vector<float> *mu_muon_scatteringCurvatureSignificance;
    vector<float> *mu_muon_scatteringNeighbourSignificance;
    vector<float> *mu_muon_momentumBalanceSignificance;
    vector<float> *mu_muon_energyLossPar;
    vector<float> *mu_muon_energyLossErr;
    vector<float> *mu_muon_etCore;
    vector<float> *mu_muon_energyLossType;
    vector<unsigned short> *mu_muon_caloMuonIdTag;
    vector<double> *mu_muon_caloLRLikelihood;
    vector<float> *mu_muon_Ehad;
    vector<float> *mu_muon_Mhad;
    vector<float> *mu_muon_Es0;
    vector<float> *mu_muon_Ms0;
    vector<float> *mu_muon_Es1;
    vector<float> *mu_muon_Ms1;
    vector<float> *mu_muon_Es2;
    vector<float> *mu_muon_Ms2;
    vector<float> *mu_muon_Es3;
    vector<float> *mu_muon_Ms3;
    vector<int> *mu_muon_bestMatch;
    vector<int> *mu_muon_isStandAloneMuon;
    vector<int> *mu_muon_isCombinedMuon;
    vector<int> *mu_muon_isLowPtReconstructedMuon;
    vector<int> *mu_muon_isSegmentTaggedMuon;
    vector<int> *mu_muon_isCaloMuonId;
    vector<int> *mu_muon_alsoFoundByLowPt;
    vector<int> *mu_muon_alsoFoundByCaloMuonId;
    vector<int> *mu_muon_isSiliconAssociatedForwardMuon;
    vector<int> *mu_muon_loose;
    vector<int> *mu_muon_medium;
    vector<int> *mu_muon_tight;
    vector<float> *mu_muon_d0_exPV;
    vector<float> *mu_muon_z0_exPV;
    vector<float> *mu_muon_phi_exPV;
    vector<float> *mu_muon_theta_exPV;
    vector<float> *mu_muon_qoverp_exPV;
    vector<float> *mu_muon_cb_d0_exPV;
    vector<float> *mu_muon_cb_z0_exPV;
    vector<float> *mu_muon_cb_phi_exPV;
    vector<float> *mu_muon_cb_theta_exPV;
    vector<float> *mu_muon_cb_qoverp_exPV;
    vector<float> *mu_muon_id_d0_exPV;
    vector<float> *mu_muon_id_z0_exPV;
    vector<float> *mu_muon_id_phi_exPV;
    vector<float> *mu_muon_id_theta_exPV;
    vector<float> *mu_muon_id_qoverp_exPV;
    vector<float> *mu_muon_me_d0_exPV;
    vector<float> *mu_muon_me_z0_exPV;
    vector<float> *mu_muon_me_phi_exPV;
    vector<float> *mu_muon_me_theta_exPV;
    vector<float> *mu_muon_me_qoverp_exPV;
    vector<float> *mu_muon_ie_d0_exPV;
    vector<float> *mu_muon_ie_z0_exPV;
    vector<float> *mu_muon_ie_phi_exPV;
    vector<float> *mu_muon_ie_theta_exPV;
    vector<float> *mu_muon_ie_qoverp_exPV;
    vector<vector<int> > *mu_muon_SpaceTime_detID;
    vector<vector<float> > *mu_muon_SpaceTime_t;
    vector<vector<float> > *mu_muon_SpaceTime_tError;
    vector<vector<float> > *mu_muon_SpaceTime_weight;
    vector<vector<float> > *mu_muon_SpaceTime_x;
    vector<vector<float> > *mu_muon_SpaceTime_y;
    vector<vector<float> > *mu_muon_SpaceTime_z;
    vector<float> *mu_muon_cov_d0_exPV;
    vector<float> *mu_muon_cov_z0_exPV;
    vector<float> *mu_muon_cov_phi_exPV;
    vector<float> *mu_muon_cov_theta_exPV;
    vector<float> *mu_muon_cov_qoverp_exPV;
    vector<float> *mu_muon_cov_d0_z0_exPV;
    vector<float> *mu_muon_cov_d0_phi_exPV;
    vector<float> *mu_muon_cov_d0_theta_exPV;
    vector<float> *mu_muon_cov_d0_qoverp_exPV;
    vector<float> *mu_muon_cov_z0_phi_exPV;
    vector<float> *mu_muon_cov_z0_theta_exPV;
    vector<float> *mu_muon_cov_z0_qoverp_exPV;
    vector<float> *mu_muon_cov_phi_theta_exPV;
    vector<float> *mu_muon_cov_phi_qoverp_exPV;
    vector<float> *mu_muon_cov_theta_qoverp_exPV;
    vector<float> *mu_muon_id_cov_d0_exPV;
    vector<float> *mu_muon_id_cov_z0_exPV;
    vector<float> *mu_muon_id_cov_phi_exPV;
    vector<float> *mu_muon_id_cov_theta_exPV;
    vector<float> *mu_muon_id_cov_qoverp_exPV;
    vector<float> *mu_muon_id_cov_d0_z0_exPV;
    vector<float> *mu_muon_id_cov_d0_phi_exPV;
    vector<float> *mu_muon_id_cov_d0_theta_exPV;
    vector<float> *mu_muon_id_cov_d0_qoverp_exPV;
    vector<float> *mu_muon_id_cov_z0_phi_exPV;
    vector<float> *mu_muon_id_cov_z0_theta_exPV;
    vector<float> *mu_muon_id_cov_z0_qoverp_exPV;
    vector<float> *mu_muon_id_cov_phi_theta_exPV;
    vector<float> *mu_muon_id_cov_phi_qoverp_exPV;
    vector<float> *mu_muon_id_cov_theta_qoverp_exPV;
    vector<float> *mu_muon_me_cov_d0_exPV;
    vector<float> *mu_muon_me_cov_z0_exPV;
    vector<float> *mu_muon_me_cov_phi_exPV;
    vector<float> *mu_muon_me_cov_theta_exPV;
    vector<float> *mu_muon_me_cov_qoverp_exPV;
    vector<float> *mu_muon_me_cov_d0_z0_exPV;
    vector<float> *mu_muon_me_cov_d0_phi_exPV;
    vector<float> *mu_muon_me_cov_d0_theta_exPV;
    vector<float> *mu_muon_me_cov_d0_qoverp_exPV;
    vector<float> *mu_muon_me_cov_z0_phi_exPV;
    vector<float> *mu_muon_me_cov_z0_theta_exPV;
    vector<float> *mu_muon_me_cov_z0_qoverp_exPV;
    vector<float> *mu_muon_me_cov_phi_theta_exPV;
    vector<float> *mu_muon_me_cov_phi_qoverp_exPV;
    vector<float> *mu_muon_me_cov_theta_qoverp_exPV;
    vector<float> *mu_muon_ms_d0;
    vector<float> *mu_muon_ms_z0;
    vector<float> *mu_muon_ms_phi;
    vector<float> *mu_muon_ms_theta;
    vector<float> *mu_muon_ms_qoverp;
    vector<float> *mu_muon_id_d0;
    vector<float> *mu_muon_id_z0;
    vector<float> *mu_muon_id_phi;
    vector<float> *mu_muon_id_theta;
    vector<float> *mu_muon_id_qoverp;
    vector<float> *mu_muon_me_d0;
    vector<float> *mu_muon_me_z0;
    vector<float> *mu_muon_me_phi;
    vector<float> *mu_muon_me_theta;
    vector<float> *mu_muon_me_qoverp;
    vector<float> *mu_muon_ie_d0;
    vector<float> *mu_muon_ie_z0;
    vector<float> *mu_muon_ie_phi;
    vector<float> *mu_muon_ie_theta;
    vector<float> *mu_muon_ie_qoverp;
    vector<int> *mu_muon_nOutliersOnTrack;
    vector<int> *mu_muon_nBLHits;
    vector<int> *mu_muon_nPixHits;
    vector<int> *mu_muon_nSCTHits;
    vector<int> *mu_muon_nTRTHits;
    vector<int> *mu_muon_nTRTHighTHits;
    vector<int> *mu_muon_nBLSharedHits;
    vector<int> *mu_muon_nPixSharedHits;
    vector<int> *mu_muon_nPixHoles;
    vector<int> *mu_muon_nSCTSharedHits;
    vector<int> *mu_muon_nSCTHoles;
    vector<int> *mu_muon_nTRTOutliers;
    vector<int> *mu_muon_nTRTHighTOutliers;
    vector<int> *mu_muon_nGangedPixels;
    vector<int> *mu_muon_nPixelDeadSensors;
    vector<int> *mu_muon_nSCTDeadSensors;
    vector<int> *mu_muon_nTRTDeadStraws;
    vector<int> *mu_muon_expectBLayerHit;
    vector<int> *mu_muon_nMDTHits;
    vector<int> *mu_muon_nMDTHoles;
    vector<int> *mu_muon_nCSCEtaHits;
    vector<int> *mu_muon_nCSCEtaHoles;
    vector<int> *mu_muon_nCSCUnspoiledEtaHits;
    vector<int> *mu_muon_nCSCPhiHits;
    vector<int> *mu_muon_nCSCPhiHoles;
    vector<int> *mu_muon_nRPCEtaHits;
    vector<int> *mu_muon_nRPCEtaHoles;
    vector<int> *mu_muon_nRPCPhiHits;
    vector<int> *mu_muon_nRPCPhiHoles;
    vector<int> *mu_muon_nTGCEtaHits;
    vector<int> *mu_muon_nTGCEtaHoles;
    vector<int> *mu_muon_nTGCPhiHits;
    vector<int> *mu_muon_nTGCPhiHoles;
    vector<int> *mu_muon_nprecisionLayers;
    vector<int> *mu_muon_nprecisionHoleLayers;
    vector<int> *mu_muon_nphiLayers;
    vector<int> *mu_muon_ntrigEtaLayers;
    vector<int> *mu_muon_nphiHoleLayers;
    vector<int> *mu_muon_ntrigEtaHoleLayers;
    vector<int> *mu_muon_nMDTBIHits;
    vector<int> *mu_muon_nMDTBMHits;
    vector<int> *mu_muon_nMDTBOHits;
    vector<int> *mu_muon_nMDTBEEHits;
    vector<int> *mu_muon_nMDTBIS78Hits;
    vector<int> *mu_muon_nMDTEIHits;
    vector<int> *mu_muon_nMDTEMHits;
    vector<int> *mu_muon_nMDTEOHits;
    vector<int> *mu_muon_nMDTEEHits;
    vector<int> *mu_muon_nRPCLayer1EtaHits;
    vector<int> *mu_muon_nRPCLayer2EtaHits;
    vector<int> *mu_muon_nRPCLayer3EtaHits;
    vector<int> *mu_muon_nRPCLayer1PhiHits;
    vector<int> *mu_muon_nRPCLayer2PhiHits;
    vector<int> *mu_muon_nRPCLayer3PhiHits;
    vector<int> *mu_muon_nTGCLayer1EtaHits;
    vector<int> *mu_muon_nTGCLayer2EtaHits;
    vector<int> *mu_muon_nTGCLayer3EtaHits;
    vector<int> *mu_muon_nTGCLayer4EtaHits;
    vector<int> *mu_muon_nTGCLayer1PhiHits;
    vector<int> *mu_muon_nTGCLayer2PhiHits;
    vector<int> *mu_muon_nTGCLayer3PhiHits;
    vector<int> *mu_muon_nTGCLayer4PhiHits;
    vector<int> *mu_muon_barrelSectors;
    vector<int> *mu_muon_endcapSectors;
    vector<float> *mu_muon_spec_surf_px;
    vector<float> *mu_muon_spec_surf_py;
    vector<float> *mu_muon_spec_surf_pz;
    vector<float> *mu_muon_spec_surf_x;
    vector<float> *mu_muon_spec_surf_y;
    vector<float> *mu_muon_spec_surf_z;
    vector<float> *mu_muon_trackd0;
    vector<float> *mu_muon_trackz0;
    vector<float> *mu_muon_trackphi;
    vector<float> *mu_muon_tracktheta;
    vector<float> *mu_muon_trackqoverp;
    vector<float> *mu_muon_trackcov_d0;
    vector<float> *mu_muon_trackcov_z0;
    vector<float> *mu_muon_trackcov_phi;
    vector<float> *mu_muon_trackcov_theta;
    vector<float> *mu_muon_trackcov_qoverp;
    vector<float> *mu_muon_trackcov_d0_z0;
    vector<float> *mu_muon_trackcov_d0_phi;
    vector<float> *mu_muon_trackcov_d0_theta;
    vector<float> *mu_muon_trackcov_d0_qoverp;
    vector<float> *mu_muon_trackcov_z0_phi;
    vector<float> *mu_muon_trackcov_z0_theta;
    vector<float> *mu_muon_trackcov_z0_qoverp;
    vector<float> *mu_muon_trackcov_phi_theta;
    vector<float> *mu_muon_trackcov_phi_qoverp;
    vector<float> *mu_muon_trackcov_theta_qoverp;
    vector<float> *mu_muon_trackfitchi2;
    vector<int> *mu_muon_trackfitndof;
    vector<int> *mu_muon_hastrack;
    vector<float> *mu_muon_trackd0beam;
    vector<float> *mu_muon_trackz0beam;
    vector<float> *mu_muon_tracksigd0beam;
    vector<float> *mu_muon_tracksigz0beam;
    vector<float> *mu_muon_trackd0pv;
    vector<float> *mu_muon_trackz0pv;
    vector<float> *mu_muon_tracksigd0pv;
    vector<float> *mu_muon_tracksigz0pv;
    vector<float> *mu_muon_trackIPEstimate_d0_biasedpvunbiased;
    vector<float> *mu_muon_trackIPEstimate_z0_biasedpvunbiased;
    vector<float> *mu_muon_trackIPEstimate_sigd0_biasedpvunbiased;
    vector<float> *mu_muon_trackIPEstimate_sigz0_biasedpvunbiased;
    vector<float> *mu_muon_trackIPEstimate_d0_unbiasedpvunbiased;
    vector<float> *mu_muon_trackIPEstimate_z0_unbiasedpvunbiased;
    vector<float> *mu_muon_trackIPEstimate_sigd0_unbiasedpvunbiased;
    vector<float> *mu_muon_trackIPEstimate_sigz0_unbiasedpvunbiased;
    vector<float> *mu_muon_trackd0pvunbiased;
    vector<float> *mu_muon_trackz0pvunbiased;
    vector<float> *mu_muon_tracksigd0pvunbiased;
    vector<float> *mu_muon_tracksigz0pvunbiased;
    Int_t trk_selec_n;
    vector<float> *trk_selec_pt;
    vector<float> *trk_selec_eta;
    vector<float> *trk_selec_d0_wrtPV;
    vector<float> *trk_selec_z0_wrtPV;
    vector<float> *trk_selec_phi_wrtPV;
    vector<float> *trk_selec_theta_wrtPV;
    vector<float> *trk_selec_qoverp_wrtPV;
    vector<float> *trk_selec_err_d0_wrtPV;
    vector<float> *trk_selec_err_z0_wrtPV;
    vector<float> *trk_selec_err_phi_wrtPV;
    vector<float> *trk_selec_err_theta_wrtPV;
    vector<float> *trk_selec_err_qoverp_wrtPV;
    vector<float> *trk_selec_chi2;
    vector<int> *trk_selec_ndof;
    vector<int> *trk_selec_nBLHits;
    vector<int> *trk_selec_nPixHits;
    vector<int> *trk_selec_nSCTHits;
    vector<int> *trk_selec_nTRTHits;
    vector<int> *trk_selec_nTRTHighTHits;
    vector<int> *trk_selec_nTRTXenonHits;
    Int_t cl_lc_n;
    vector<float> *cl_lc_pt;
    vector<float> *cl_lc_eta;
    vector<float> *cl_lc_phi;
    vector<float> *cl_lc_firstEdens;
    vector<float> *cl_lc_cellmaxfrac;
    vector<float> *cl_lc_longitudinal;
    vector<float> *cl_lc_secondlambda;
    vector<float> *cl_lc_lateral;
    vector<float> *cl_lc_secondR;
    vector<float> *cl_lc_centerlambda;
    vector<float> *cl_lc_deltaTheta;
    vector<float> *cl_lc_deltaPhi;
    vector<float> *cl_lc_centermag;
    vector<float> *cl_lc_time;
    vector<float> *cl_lc_E_PreSamplerB;
    vector<float> *cl_lc_E_EMB1;
    vector<float> *cl_lc_E_EMB2;
    vector<float> *cl_lc_E_EMB3;
    vector<float> *cl_lc_E_PreSamplerE;
    vector<float> *cl_lc_E_EME1;
    vector<float> *cl_lc_E_EME2;
    vector<float> *cl_lc_E_EME3;
    vector<float> *cl_lc_E_HEC0;
    vector<float> *cl_lc_E_HEC1;
    vector<float> *cl_lc_E_HEC2;
    vector<float> *cl_lc_E_HEC3;
    vector<float> *cl_lc_E_TileBar0;
    vector<float> *cl_lc_E_TileBar1;
    vector<float> *cl_lc_E_TileBar2;
    vector<float> *cl_lc_E_TileGap1;
    vector<float> *cl_lc_E_TileGap2;
    vector<float> *cl_lc_E_TileGap3;
    vector<float> *cl_lc_E_TileExt0;
    vector<float> *cl_lc_E_TileExt1;
    vector<float> *cl_lc_E_TileExt2;
    vector<float> *cl_lc_E_FCAL0;
    vector<float> *cl_lc_E_FCAL1;
    vector<float> *cl_lc_E_FCAL2;
    Int_t vxp_n;
    vector<float> *vxp_x;
    vector<float> *vxp_y;
    vector<float> *vxp_z;
    vector<float> *vxp_err_x;
    vector<float> *vxp_err_y;
    vector<float> *vxp_err_z;
    vector<float> *vxp_cov_xy;
    vector<float> *vxp_cov_xz;
    vector<float> *vxp_cov_yz;
    vector<int> *vxp_type;
    vector<float> *vxp_chi2;
    vector<int> *vxp_ndof;
    vector<float> *vxp_px;
    vector<float> *vxp_py;
    vector<float> *vxp_pz;
    vector<float> *vxp_E;
    vector<float> *vxp_m;
    vector<int> *vxp_nTracks;
    vector<float> *vxp_sumPt;
    vector<vector<float> > *vxp_trk_weight;
    vector<int> *vxp_trk_n;
    vector<vector<int> > *vxp_trk_index;
    Float_t beamSpot_x;
    Float_t beamSpot_y;
    Float_t beamSpot_z;
    Float_t beamSpot_sigma_x;
    Float_t beamSpot_sigma_y;
    Float_t beamSpot_sigma_z;
    Float_t beamSpot_tilt_x;
    Float_t beamSpot_tilt_y;
    Float_t Eventshape_rhoKt3EM;
    Float_t Eventshape_rhoKt4EM;
    Float_t Eventshape_rhoKt3LC;
    Float_t Eventshape_rhoKt4LC;

    // List of branches
    TBranch *b_EF_2mu13;                                            //!
    TBranch *b_EF_e12Tvh_medium1_mu8;                               //!
    TBranch *b_EF_e24vhi_loose1_mu8;                                //!
    TBranch *b_EF_e24vhi_medium1;                                   //!
    TBranch *b_EF_e45_medium1;                                      //!
    TBranch *b_EF_e60_medium1;                                      //!
    TBranch *b_EF_mu10i_g10_medium;                                 //!
    TBranch *b_EF_mu10i_g10_medium_TauMass;                         //!
    TBranch *b_EF_mu10i_loose_g12Tvh_medium;                        //!
    TBranch *b_EF_mu10i_loose_g12Tvh_medium_TauMass;                //!
    TBranch *b_EF_mu18_tight_mu8_EFFS;                              //!
    TBranch *b_EF_mu20i_tight_g5_loose_TauMass;                     //!
    TBranch *b_EF_mu20i_tight_g5_medium;                            //!
    TBranch *b_EF_mu20i_tight_g5_medium_TauMass;                    //!
    TBranch *b_EF_mu24i_tight;                                      //!
    TBranch *b_EF_mu36_tight;                                       //!
    TBranch *b_EF_mu4T;                                             //!
    TBranch *b_EF_mu4Ti_g20Tvh_medium;                              //!
    TBranch *b_EF_mu4Ti_g20Tvh_medium_TauMass;                      //!
    TBranch *b_EF_tau20_medium1_mu15;                               //!
    TBranch *b_EF_tau29Ti_medium1_xe55_tclcw;                       //!
    TBranch *b_EF_xe80T;                                            //!
    TBranch *b_EF_xe80_tclcw;                                       //!
    TBranch *b_RunNumber;                                           //!
    TBranch *b_EventNumber;                                         //!
    TBranch *b_timestamp;                                           //!
    TBranch *b_timestamp_ns;                                        //!
    TBranch *b_lbn;                                                 //!
    TBranch *b_bcid;                                                //!
    TBranch *b_detmask0;                                            //!
    TBranch *b_detmask1;                                            //!
    TBranch *b_actualIntPerXing;                                    //!
    TBranch *b_averageIntPerXing;                                   //!
    TBranch *b_pixelFlags;                                          //!
    TBranch *b_sctFlags;                                            //!
    TBranch *b_trtFlags;                                            //!
    TBranch *b_larFlags;                                            //!
    TBranch *b_tileFlags;                                           //!
    TBranch *b_muonFlags;                                           //!
    TBranch *b_fwdFlags;                                            //!
    TBranch *b_coreFlags;                                           //!
    TBranch *b_pixelError;                                          //!
    TBranch *b_sctError;                                            //!
    TBranch *b_trtError;                                            //!
    TBranch *b_larError;                                            //!
    TBranch *b_tileError;                                           //!
    TBranch *b_muonError;                                           //!
    TBranch *b_fwdError;                                            //!
    TBranch *b_coreError;                                           //!
    TBranch *b_streamDecision_Egamma;                               //!
    TBranch *b_streamDecision_Muons;                                //!
    TBranch *b_streamDecision_JetTauEtmiss;                         //!
    TBranch *b_l1id;                                                //!
    TBranch *b_isSimulation;                                        //!
    TBranch *b_isCalibration;                                       //!
    TBranch *b_isTestBeam;                                          //!
    TBranch *b_trig_EF_trigmuonef_n;                                //!
    TBranch *b_trig_EF_trigmuonef_track_n;                          //!
    TBranch *b_trig_EF_trigmuonef_track_MuonType;                   //!
    TBranch *b_trig_EF_trigmuonef_track_MS_pt;                      //!
    TBranch *b_trig_EF_trigmuonef_track_MS_eta;                     //!
    TBranch *b_trig_EF_trigmuonef_track_MS_phi;                     //!
    TBranch *b_trig_EF_trigmuonef_track_MS_hasMS;                   //!
    TBranch *b_trig_EF_trigmuonef_track_SA_pt;                      //!
    TBranch *b_trig_EF_trigmuonef_track_SA_eta;                     //!
    TBranch *b_trig_EF_trigmuonef_track_SA_phi;                     //!
    TBranch *b_trig_EF_trigmuonef_track_SA_hasSA;                   //!
    TBranch *b_trig_EF_trigmuonef_track_CB_pt;                      //!
    TBranch *b_trig_EF_trigmuonef_track_CB_eta;                     //!
    TBranch *b_trig_EF_trigmuonef_track_CB_phi;                     //!
    TBranch *b_trig_EF_trigmuonef_track_CB_hasCB;                   //!
    TBranch *b_trig_EF_trigmuonefisolation_n;                       //!
    TBranch *b_trig_EF_trigmuonefisolation_sumTrkPtCone02;          //!
    TBranch *b_trig_EF_trigmuonefisolation_sumTrkPtCone03;          //!
    TBranch *b_trig_EF_trigmuonefisolation_trackPosition;           //!
    TBranch *b_trig_EF_trigmuonefisolation_efinfo_index;            //!
    TBranch *b_el_n;                                                //!
    TBranch *b_el_E;                                                //!
    TBranch *b_el_Et;                                               //!
    TBranch *b_el_pt;                                               //!
    TBranch *b_el_m;                                                //!
    TBranch *b_el_eta;                                              //!
    TBranch *b_el_phi;                                              //!
    TBranch *b_el_px;                                               //!
    TBranch *b_el_py;                                               //!
    TBranch *b_el_pz;                                               //!
    TBranch *b_el_charge;                                           //!
    TBranch *b_el_author;                                           //!
    TBranch *b_el_isEM;                                             //!
    TBranch *b_el_isEMLoose;                                        //!
    TBranch *b_el_isEMMedium;                                       //!
    TBranch *b_el_isEMTight;                                        //!
    TBranch *b_el_OQ;                                               //!
    TBranch *b_el_convFlag;                                         //!
    TBranch *b_el_isConv;                                           //!
    TBranch *b_el_nConv;                                            //!
    TBranch *b_el_nSingleTrackConv;                                 //!
    TBranch *b_el_nDoubleTrackConv;                                 //!
    TBranch *b_el_mediumWithoutTrack;                               //!
    TBranch *b_el_mediumIsoWithoutTrack;                            //!
    TBranch *b_el_tightWithoutTrack;                                //!
    TBranch *b_el_tightIsoWithoutTrack;                             //!
    TBranch *b_el_loose;                                            //!
    TBranch *b_el_looseIso;                                         //!
    TBranch *b_el_medium;                                           //!
    TBranch *b_el_mediumIso;                                        //!
    TBranch *b_el_tight;                                            //!
    TBranch *b_el_tightIso;                                         //!
    TBranch *b_el_loosePP;                                          //!
    TBranch *b_el_loosePPIso;                                       //!
    TBranch *b_el_mediumPP;                                         //!
    TBranch *b_el_mediumPPIso;                                      //!
    TBranch *b_el_tightPP;                                          //!
    TBranch *b_el_tightPPIso;                                       //!
    TBranch *b_el_goodOQ;                                           //!
    TBranch *b_el_Ethad;                                            //!
    TBranch *b_el_Ethad1;                                           //!
    TBranch *b_el_f1;                                               //!
    TBranch *b_el_f1core;                                           //!
    TBranch *b_el_Emins1;                                           //!
    TBranch *b_el_fside;                                            //!
    TBranch *b_el_Emax2;                                            //!
    TBranch *b_el_ws3;                                              //!
    TBranch *b_el_wstot;                                            //!
    TBranch *b_el_emaxs1;                                           //!
    TBranch *b_el_deltaEs;                                          //!
    TBranch *b_el_E233;                                             //!
    TBranch *b_el_E237;                                             //!
    TBranch *b_el_E277;                                             //!
    TBranch *b_el_weta2;                                            //!
    TBranch *b_el_f3;                                               //!
    TBranch *b_el_f3core;                                           //!
    TBranch *b_el_rphiallcalo;                                      //!
    TBranch *b_el_Etcone45;                                         //!
    TBranch *b_el_Etcone15;                                         //!
    TBranch *b_el_Etcone20;                                         //!
    TBranch *b_el_Etcone25;                                         //!
    TBranch *b_el_Etcone30;                                         //!
    TBranch *b_el_Etcone35;                                         //!
    TBranch *b_el_Etcone40;                                         //!
    TBranch *b_el_ptcone20;                                         //!
    TBranch *b_el_ptcone30;                                         //!
    TBranch *b_el_ptcone40;                                         //!
    TBranch *b_el_nucone20;                                         //!
    TBranch *b_el_nucone30;                                         //!
    TBranch *b_el_nucone40;                                         //!
    TBranch *b_el_Etcone15_pt_corrected;                            //!
    TBranch *b_el_Etcone20_pt_corrected;                            //!
    TBranch *b_el_Etcone25_pt_corrected;                            //!
    TBranch *b_el_Etcone30_pt_corrected;                            //!
    TBranch *b_el_Etcone35_pt_corrected;                            //!
    TBranch *b_el_Etcone40_pt_corrected;                            //!
    TBranch *b_el_convanglematch;                                   //!
    TBranch *b_el_convtrackmatch;                                   //!
    TBranch *b_el_hasconv;                                          //!
    TBranch *b_el_convvtxx;                                         //!
    TBranch *b_el_convvtxy;                                         //!
    TBranch *b_el_convvtxz;                                         //!
    TBranch *b_el_Rconv;                                            //!
    TBranch *b_el_zconv;                                            //!
    TBranch *b_el_convvtxchi2;                                      //!
    TBranch *b_el_pt1conv;                                          //!
    TBranch *b_el_convtrk1nBLHits;                                  //!
    TBranch *b_el_convtrk1nPixHits;                                 //!
    TBranch *b_el_convtrk1nSCTHits;                                 //!
    TBranch *b_el_convtrk1nTRTHits;                                 //!
    TBranch *b_el_pt2conv;                                          //!
    TBranch *b_el_convtrk2nBLHits;                                  //!
    TBranch *b_el_convtrk2nPixHits;                                 //!
    TBranch *b_el_convtrk2nSCTHits;                                 //!
    TBranch *b_el_convtrk2nTRTHits;                                 //!
    TBranch *b_el_ptconv;                                           //!
    TBranch *b_el_pzconv;                                           //!
    TBranch *b_el_pos7;                                             //!
    TBranch *b_el_etacorrmag;                                       //!
    TBranch *b_el_deltaeta1;                                        //!
    TBranch *b_el_deltaeta2;                                        //!
    TBranch *b_el_deltaphi2;                                        //!
    TBranch *b_el_deltaphiRescaled;                                 //!
    TBranch *b_el_deltaPhiFromLast;                                 //!
    TBranch *b_el_deltaPhiRot;                                      //!
    TBranch *b_el_expectHitInBLayer;                                //!
    TBranch *b_el_trackd0_physics;                                  //!
    TBranch *b_el_etaSampling1;                                     //!
    TBranch *b_el_reta;                                             //!
    TBranch *b_el_rphi;                                             //!
    TBranch *b_el_topoEtcone20;                                     //!
    TBranch *b_el_topoEtcone30;                                     //!
    TBranch *b_el_topoEtcone40;                                     //!
    TBranch *b_el_materialTraversed;                                //!
    TBranch *b_el_EtringnoisedR03sig2;                              //!
    TBranch *b_el_EtringnoisedR03sig3;                              //!
    TBranch *b_el_EtringnoisedR03sig4;                              //!
    TBranch *b_el_ptcone20_zpv05;                                   //!
    TBranch *b_el_ptcone30_zpv05;                                   //!
    TBranch *b_el_ptcone40_zpv05;                                   //!
    TBranch *b_el_nucone20_zpv05;                                   //!
    TBranch *b_el_nucone30_zpv05;                                   //!
    TBranch *b_el_nucone40_zpv05;                                   //!
    TBranch *b_el_isolationlikelihoodjets;                          //!
    TBranch *b_el_isolationlikelihoodhqelectrons;                   //!
    TBranch *b_el_electronweight;                                   //!
    TBranch *b_el_electronbgweight;                                 //!
    TBranch *b_el_softeweight;                                      //!
    TBranch *b_el_softebgweight;                                    //!
    TBranch *b_el_neuralnet;                                        //!
    TBranch *b_el_Hmatrix;                                          //!
    TBranch *b_el_Hmatrix5;                                         //!
    TBranch *b_el_adaboost;                                         //!
    TBranch *b_el_softeneuralnet;                                   //!
    TBranch *b_el_ringernn;                                         //!
    TBranch *b_el_zvertex;                                          //!
    TBranch *b_el_errz;                                             //!
    TBranch *b_el_etap;                                             //!
    TBranch *b_el_depth;                                            //!
    TBranch *b_el_Es0;                                              //!
    TBranch *b_el_etas0;                                            //!
    TBranch *b_el_phis0;                                            //!
    TBranch *b_el_Es1;                                              //!
    TBranch *b_el_etas1;                                            //!
    TBranch *b_el_phis1;                                            //!
    TBranch *b_el_Es2;                                              //!
    TBranch *b_el_etas2;                                            //!
    TBranch *b_el_phis2;                                            //!
    TBranch *b_el_Es3;                                              //!
    TBranch *b_el_etas3;                                            //!
    TBranch *b_el_phis3;                                            //!
    TBranch *b_el_E_PreSamplerB;                                    //!
    TBranch *b_el_E_EMB1;                                           //!
    TBranch *b_el_E_EMB2;                                           //!
    TBranch *b_el_E_EMB3;                                           //!
    TBranch *b_el_E_PreSamplerE;                                    //!
    TBranch *b_el_E_EME1;                                           //!
    TBranch *b_el_E_EME2;                                           //!
    TBranch *b_el_E_EME3;                                           //!
    TBranch *b_el_E_HEC0;                                           //!
    TBranch *b_el_E_HEC1;                                           //!
    TBranch *b_el_E_HEC2;                                           //!
    TBranch *b_el_E_HEC3;                                           //!
    TBranch *b_el_E_TileBar0;                                       //!
    TBranch *b_el_E_TileBar1;                                       //!
    TBranch *b_el_E_TileBar2;                                       //!
    TBranch *b_el_E_TileGap1;                                       //!
    TBranch *b_el_E_TileGap2;                                       //!
    TBranch *b_el_E_TileGap3;                                       //!
    TBranch *b_el_E_TileExt0;                                       //!
    TBranch *b_el_E_TileExt1;                                       //!
    TBranch *b_el_E_TileExt2;                                       //!
    TBranch *b_el_E_FCAL0;                                          //!
    TBranch *b_el_E_FCAL1;                                          //!
    TBranch *b_el_E_FCAL2;                                          //!
    TBranch *b_el_cl_E;                                             //!
    TBranch *b_el_cl_pt;                                            //!
    TBranch *b_el_cl_eta;                                           //!
    TBranch *b_el_cl_phi;                                           //!
    TBranch *b_el_cl_etaCalo;                                       //!
    TBranch *b_el_cl_phiCalo;                                       //!
    TBranch *b_el_firstEdens;                                       //!
    TBranch *b_el_cellmaxfrac;                                      //!
    TBranch *b_el_longitudinal;                                     //!
    TBranch *b_el_secondlambda;                                     //!
    TBranch *b_el_lateral;                                          //!
    TBranch *b_el_secondR;                                          //!
    TBranch *b_el_centerlambda;                                     //!
    TBranch *b_el_rawcl_Es0;                                        //!
    TBranch *b_el_rawcl_etas0;                                      //!
    TBranch *b_el_rawcl_phis0;                                      //!
    TBranch *b_el_rawcl_Es1;                                        //!
    TBranch *b_el_rawcl_etas1;                                      //!
    TBranch *b_el_rawcl_phis1;                                      //!
    TBranch *b_el_rawcl_Es2;                                        //!
    TBranch *b_el_rawcl_etas2;                                      //!
    TBranch *b_el_rawcl_phis2;                                      //!
    TBranch *b_el_rawcl_Es3;                                        //!
    TBranch *b_el_rawcl_etas3;                                      //!
    TBranch *b_el_rawcl_phis3;                                      //!
    TBranch *b_el_rawcl_E;                                          //!
    TBranch *b_el_rawcl_pt;                                         //!
    TBranch *b_el_rawcl_eta;                                        //!
    TBranch *b_el_rawcl_phi;                                        //!
    TBranch *b_el_trackd0;                                          //!
    TBranch *b_el_trackz0;                                          //!
    TBranch *b_el_trackphi;                                         //!
    TBranch *b_el_tracktheta;                                       //!
    TBranch *b_el_trackqoverp;                                      //!
    TBranch *b_el_trackpt;                                          //!
    TBranch *b_el_tracketa;                                         //!
    TBranch *b_el_trackfitchi2;                                     //!
    TBranch *b_el_trackfitndof;                                     //!
    TBranch *b_el_nBLHits;                                          //!
    TBranch *b_el_nPixHits;                                         //!
    TBranch *b_el_nSCTHits;                                         //!
    TBranch *b_el_nTRTHits;                                         //!
    TBranch *b_el_nTRTHighTHits;                                    //!
    TBranch *b_el_nTRTXenonHits;                                    //!
    TBranch *b_el_nPixHoles;                                        //!
    TBranch *b_el_nSCTHoles;                                        //!
    TBranch *b_el_nTRTHoles;                                        //!
    TBranch *b_el_nPixelDeadSensors;                                //!
    TBranch *b_el_nSCTDeadSensors;                                  //!
    TBranch *b_el_nBLSharedHits;                                    //!
    TBranch *b_el_nPixSharedHits;                                   //!
    TBranch *b_el_nSCTSharedHits;                                   //!
    TBranch *b_el_nBLayerSplitHits;                                 //!
    TBranch *b_el_nPixSplitHits;                                    //!
    TBranch *b_el_nBLayerOutliers;                                  //!
    TBranch *b_el_nPixelOutliers;                                   //!
    TBranch *b_el_nSCTOutliers;                                     //!
    TBranch *b_el_nTRTOutliers;                                     //!
    TBranch *b_el_nTRTHighTOutliers;                                //!
    TBranch *b_el_nContribPixelLayers;                              //!
    TBranch *b_el_nGangedPixels;                                    //!
    TBranch *b_el_nGangedFlaggedFakes;                              //!
    TBranch *b_el_nPixelSpoiltHits;                                 //!
    TBranch *b_el_nSCTDoubleHoles;                                  //!
    TBranch *b_el_nSCTSpoiltHits;                                   //!
    TBranch *b_el_expectBLayerHit;                                  //!
    TBranch *b_el_nSiHits;                                          //!
    TBranch *b_el_TRTHighTHitsRatio;                                //!
    TBranch *b_el_TRTHighTOutliersRatio;                            //!
    TBranch *b_el_pixeldEdx;                                        //!
    TBranch *b_el_nGoodHitsPixeldEdx;                               //!
    TBranch *b_el_massPixeldEdx;                                    //!
    TBranch *b_el_likelihoodsPixeldEdx;                             //!
    TBranch *b_el_eProbabilityComb;                                 //!
    TBranch *b_el_eProbabilityHT;                                   //!
    TBranch *b_el_eProbabilityToT;                                  //!
    TBranch *b_el_eProbabilityBrem;                                 //!
    TBranch *b_el_vertweight;                                       //!
    TBranch *b_el_vertx;                                            //!
    TBranch *b_el_verty;                                            //!
    TBranch *b_el_vertz;                                            //!
    TBranch *b_el_trackd0beam;                                      //!
    TBranch *b_el_trackz0beam;                                      //!
    TBranch *b_el_tracksigd0beam;                                   //!
    TBranch *b_el_tracksigz0beam;                                   //!
    TBranch *b_el_trackd0pv;                                        //!
    TBranch *b_el_trackz0pv;                                        //!
    TBranch *b_el_tracksigd0pv;                                     //!
    TBranch *b_el_tracksigz0pv;                                     //!
    TBranch *b_el_trackIPEstimate_d0_biasedpvunbiased;              //!
    TBranch *b_el_trackIPEstimate_z0_biasedpvunbiased;              //!
    TBranch *b_el_trackIPEstimate_sigd0_biasedpvunbiased;           //!
    TBranch *b_el_trackIPEstimate_sigz0_biasedpvunbiased;           //!
    TBranch *b_el_trackIPEstimate_d0_unbiasedpvunbiased;            //!
    TBranch *b_el_trackIPEstimate_z0_unbiasedpvunbiased;            //!
    TBranch *b_el_trackIPEstimate_sigd0_unbiasedpvunbiased;         //!
    TBranch *b_el_trackIPEstimate_sigz0_unbiasedpvunbiased;         //!
    TBranch *b_el_trackd0pvunbiased;                                //!
    TBranch *b_el_trackz0pvunbiased;                                //!
    TBranch *b_el_tracksigd0pvunbiased;                             //!
    TBranch *b_el_tracksigz0pvunbiased;                             //!
    TBranch *b_el_Unrefittedtrack_d0;                               //!
    TBranch *b_el_Unrefittedtrack_z0;                               //!
    TBranch *b_el_Unrefittedtrack_phi;                              //!
    TBranch *b_el_Unrefittedtrack_theta;                            //!
    TBranch *b_el_Unrefittedtrack_qoverp;                           //!
    TBranch *b_el_Unrefittedtrack_pt;                               //!
    TBranch *b_el_Unrefittedtrack_eta;                              //!
    TBranch *b_el_theta_LM;                                         //!
    TBranch *b_el_qoverp_LM;                                        //!
    TBranch *b_el_theta_err_LM;                                     //!
    TBranch *b_el_qoverp_err_LM;                                    //!
    TBranch *b_el_hastrack;                                         //!
    TBranch *b_el_deltaEmax2;                                       //!
    TBranch *b_el_calibHitsShowerDepth;                             //!
    TBranch *b_el_isIso;                                            //!
    TBranch *b_el_mvaptcone20;                                      //!
    TBranch *b_el_mvaptcone30;                                      //!
    TBranch *b_el_mvaptcone40;                                      //!
    TBranch *b_el_CaloPointing_eta;                                 //!
    TBranch *b_el_CaloPointing_sigma_eta;                           //!
    TBranch *b_el_CaloPointing_zvertex;                             //!
    TBranch *b_el_CaloPointing_sigma_zvertex;                       //!
    TBranch *b_el_HPV_eta;                                          //!
    TBranch *b_el_HPV_sigma_eta;                                    //!
    TBranch *b_el_HPV_zvertex;                                      //!
    TBranch *b_el_HPV_sigma_zvertex;                                //!
    TBranch *b_el_topoEtcone60;                                     //!
    TBranch *b_el_ES0_real;                                         //!
    TBranch *b_el_ES1_real;                                         //!
    TBranch *b_el_ES2_real;                                         //!
    TBranch *b_el_ES3_real;                                         //!
    TBranch *b_el_EcellS0;                                          //!
    TBranch *b_el_etacellS0;                                        //!
    TBranch *b_el_Etcone40_ED_corrected;                            //!
    TBranch *b_el_Etcone40_corrected;                               //!
    TBranch *b_el_topoEtcone20_corrected;                           //!
    TBranch *b_el_topoEtcone30_corrected;                           //!
    TBranch *b_el_topoEtcone40_corrected;                           //!
    TBranch *b_el_ED_median;                                        //!
    TBranch *b_el_ED_sigma;                                         //!
    TBranch *b_el_ED_Njets;                                         //!
    TBranch *b_el_jet_dr;                                           //!
    TBranch *b_el_jet_E;                                            //!
    TBranch *b_el_jet_pt;                                           //!
    TBranch *b_el_jet_m;                                            //!
    TBranch *b_el_jet_eta;                                          //!
    TBranch *b_el_jet_phi;                                          //!
    TBranch *b_el_jet_matched;                                      //!
    TBranch *b_tau_n;                                               //!
    TBranch *b_tau_Et;                                              //!
    TBranch *b_tau_pt;                                              //!
    TBranch *b_tau_m;                                               //!
    TBranch *b_tau_eta;                                             //!
    TBranch *b_tau_phi;                                             //!
    TBranch *b_tau_charge;                                          //!
    TBranch *b_tau_BDTEleScore;                                     //!
    TBranch *b_tau_BDTJetScore;                                     //!
    TBranch *b_tau_likelihood;                                      //!
    TBranch *b_tau_SafeLikelihood;                                  //!
    TBranch *b_tau_electronVetoLoose;                               //!
    TBranch *b_tau_electronVetoMedium;                              //!
    TBranch *b_tau_electronVetoTight;                               //!
    TBranch *b_tau_muonVeto;                                        //!
    TBranch *b_tau_tauLlhLoose;                                     //!
    TBranch *b_tau_tauLlhMedium;                                    //!
    TBranch *b_tau_tauLlhTight;                                     //!
    TBranch *b_tau_JetBDTSigLoose;                                  //!
    TBranch *b_tau_JetBDTSigMedium;                                 //!
    TBranch *b_tau_JetBDTSigTight;                                  //!
    TBranch *b_tau_EleBDTLoose;                                     //!
    TBranch *b_tau_EleBDTMedium;                                    //!
    TBranch *b_tau_EleBDTTight;                                     //!
    TBranch *b_tau_author;                                          //!
    TBranch *b_tau_RoIWord;                                         //!
    TBranch *b_tau_nProng;                                          //!
    TBranch *b_tau_numTrack;                                        //!
    TBranch *b_tau_seedCalo_numTrack;                               //!
    TBranch *b_tau_seedCalo_nWideTrk;                               //!
    TBranch *b_tau_nOtherTrk;                                       //!
    TBranch *b_tau_track_atTJVA_n;                                  //!
    TBranch *b_tau_seedCalo_wideTrk_atTJVA_n;                       //!
    TBranch *b_tau_otherTrk_atTJVA_n;                               //!
    TBranch *b_tau_track_n;                                         //!
    TBranch *b_tau_seedCalo_track_n;                                //!
    TBranch *b_tau_seedCalo_wideTrk_n;                              //!
    TBranch *b_tau_otherTrk_n;                                      //!
    TBranch *b_jet_akt4topoLC_n;                                    //!
    TBranch *b_jet_akt4topoLC_E;                                    //!
    TBranch *b_jet_akt4topoLC_pt;                                   //!
    TBranch *b_jet_akt4topoLC_m;                                    //!
    TBranch *b_jet_akt4topoLC_eta;                                  //!
    TBranch *b_jet_akt4topoLC_phi;                                  //!
    TBranch *b_jet_akt4topoLC_WIDTH;                                //!
    TBranch *b_jet_akt4topoLC_n90;                                  //!
    TBranch *b_jet_akt4topoLC_Timing;                               //!
    TBranch *b_jet_akt4topoLC_LArQuality;                           //!
    TBranch *b_jet_akt4topoLC_nTrk;                                 //!
    TBranch *b_jet_akt4topoLC_sumPtTrk;                             //!
    TBranch *b_jet_akt4topoLC_OriginIndex;                          //!
    TBranch *b_jet_akt4topoLC_HECQuality;                           //!
    TBranch *b_jet_akt4topoLC_NegativeE;                            //!
    TBranch *b_jet_akt4topoLC_AverageLArQF;                         //!
    TBranch *b_jet_akt4topoLC_BCH_CORR_CELL;                        //!
    TBranch *b_jet_akt4topoLC_BCH_CORR_DOTX;                        //!
    TBranch *b_jet_akt4topoLC_BCH_CORR_JET;                         //!
    TBranch *b_jet_akt4topoLC_BCH_CORR_JET_FORCELL;                 //!
    TBranch *b_jet_akt4topoLC_ENG_BAD_CELLS;                        //!
    TBranch *b_jet_akt4topoLC_N_BAD_CELLS;                          //!
    TBranch *b_jet_akt4topoLC_N_BAD_CELLS_CORR;                     //!
    TBranch *b_jet_akt4topoLC_BAD_CELLS_CORR_E;                     //!
    TBranch *b_jet_akt4topoLC_NumTowers;                            //!
    TBranch *b_jet_akt4topoLC_ootFracCells5;                        //!
    TBranch *b_jet_akt4topoLC_ootFracCells10;                       //!
    TBranch *b_jet_akt4topoLC_ootFracClusters5;                     //!
    TBranch *b_jet_akt4topoLC_ootFracClusters10;                    //!
    TBranch *b_jet_akt4topoLC_emfrac;                               //!
    TBranch *b_jet_akt4topoLC_LCJES;                                //!
    TBranch *b_jet_akt4topoLC_LCJES_EtaCorr;                        //!
    TBranch *b_jet_akt4topoLC_emscale_E;                            //!
    TBranch *b_jet_akt4topoLC_emscale_pt;                           //!
    TBranch *b_jet_akt4topoLC_emscale_m;                            //!
    TBranch *b_jet_akt4topoLC_emscale_eta;                          //!
    TBranch *b_jet_akt4topoLC_emscale_phi;                          //!
    TBranch *b_jet_akt4topoLC_jvtx_x;                               //!
    TBranch *b_jet_akt4topoLC_jvtx_y;                               //!
    TBranch *b_jet_akt4topoLC_jvtx_z;                               //!
    TBranch *b_jet_akt4topoLC_Nconst;                               //!
    TBranch *b_jet_akt4topoLC_ptconst_default;                      //!
    TBranch *b_jet_akt4topoLC_econst_default;                       //!
    TBranch *b_jet_akt4topoLC_etaconst_default;                     //!
    TBranch *b_jet_akt4topoLC_phiconst_default;                     //!
    TBranch *b_jet_akt4topoLC_weightconst_default;                  //!
    TBranch *b_jet_akt4topoLC_constscale_E;                         //!
    TBranch *b_jet_akt4topoLC_constscale_pt;                        //!
    TBranch *b_jet_akt4topoLC_constscale_m;                         //!
    TBranch *b_jet_akt4topoLC_constscale_eta;                       //!
    TBranch *b_jet_akt4topoLC_constscale_phi;                       //!
    TBranch *b_jet_akt4topoLC_flavor_weight_Comb;                   //!
    TBranch *b_jet_akt4topoLC_flavor_weight_GbbNN;                  //!
    TBranch *b_jet_akt4topoLC_flavor_weight_IP2D;                   //!
    TBranch *b_jet_akt4topoLC_flavor_weight_IP3D;                   //!
    TBranch *b_jet_akt4topoLC_flavor_weight_JetFitterCOMBNN;        //!
    TBranch *b_jet_akt4topoLC_flavor_weight_JetFitterTagNN;         //!
    TBranch *b_jet_akt4topoLC_flavor_weight_MV1;                    //!
    TBranch *b_jet_akt4topoLC_flavor_weight_MV2;                    //!
    TBranch *b_jet_akt4topoLC_flavor_weight_SV0;                    //!
    TBranch *b_jet_akt4topoLC_flavor_weight_SV1;                    //!
    TBranch *b_jet_akt4topoLC_flavor_weight_SV2;                    //!
    TBranch *b_jet_akt4topoLC_flavor_weight_SecondSoftMuonTagChi2;  //!
    TBranch *b_jet_akt4topoLC_flavor_weight_SoftMuonTagChi2;        //!
    TBranch *b_jet_akt4topoLC_flavor_assoctrk_n;                    //!
    TBranch *b_jet_akt4topoLC_flavor_assoctrk_index;                //!
    TBranch *b_jet_akt4topoLC_el_dr;                                //!
    TBranch *b_jet_akt4topoLC_el_matched;                           //!
    TBranch *b_jet_akt4topoLC_mu_dr;                                //!
    TBranch *b_jet_akt4topoLC_mu_matched;                           //!
    TBranch *b_jet_akt4topoLC_L1_dr;                                //!
    TBranch *b_jet_akt4topoLC_L1_matched;                           //!
    TBranch *b_jet_akt4topoLC_L2_dr;                                //!
    TBranch *b_jet_akt4topoLC_L2_matched;                           //!
    TBranch *b_jet_akt4topoLC_EF_dr;                                //!
    TBranch *b_jet_akt4topoLC_EF_matched;                           //!
    TBranch *b_jet_akt4topoLC_nTrk_pv0_1GeV;                        //!
    TBranch *b_jet_akt4topoLC_sumPtTrk_pv0_1GeV;                    //!
    TBranch *b_jet_akt4topoLC_nTrk_allpv_1GeV;                      //!
    TBranch *b_jet_akt4topoLC_sumPtTrk_allpv_1GeV;                  //!
    TBranch *b_jet_akt4topoLC_nTrk_pv0_500MeV;                      //!
    TBranch *b_jet_akt4topoLC_sumPtTrk_pv0_500MeV;                  //!
    TBranch *b_jet_akt4topoLC_trackWIDTH_pv0_1GeV;                  //!
    TBranch *b_jet_akt4topoLC_trackWIDTH_allpv_1GeV;                //!
    TBranch *b_MET_Track_etx;                                       //!
    TBranch *b_MET_Track_ety;                                       //!
    TBranch *b_MET_Track_phi;                                       //!
    TBranch *b_MET_Track_et;                                        //!
    TBranch *b_MET_Track_sumet;                                     //!
    TBranch *b_MET_TauMuGamma_RefFinal_etx;                         //!
    TBranch *b_MET_TauMuGamma_RefFinal_ety;                         //!
    TBranch *b_MET_TauMuGamma_RefFinal_phi;                         //!
    TBranch *b_MET_TauMuGamma_RefFinal_et;                          //!
    TBranch *b_MET_TauMuGamma_RefFinal_sumet;                       //!
    TBranch *b_MET_Default_RefFinal_etx;                            //!
    TBranch *b_MET_Default_RefFinal_ety;                            //!
    TBranch *b_MET_Default_RefFinal_phi;                            //!
    TBranch *b_MET_Default_RefFinal_et;                             //!
    TBranch *b_MET_Default_RefFinal_sumet;                          //!
    TBranch *b_el_MET_TauMuGamma_comp_n;                            //!
    TBranch *b_el_MET_TauMuGamma_comp_wpx;                          //!
    TBranch *b_el_MET_TauMuGamma_comp_wpy;                          //!
    TBranch *b_el_MET_TauMuGamma_comp_wet;                          //!
    TBranch *b_el_MET_TauMuGamma_comp_statusWord;                   //!
    TBranch *b_ph_MET_TauMuGamma_comp_n;                            //!
    TBranch *b_ph_MET_TauMuGamma_comp_wpx;                          //!
    TBranch *b_ph_MET_TauMuGamma_comp_wpy;                          //!
    TBranch *b_ph_MET_TauMuGamma_comp_wet;                          //!
    TBranch *b_ph_MET_TauMuGamma_comp_statusWord;                   //!
    TBranch *b_mu_staco_MET_TauMuGamma_comp_n;                      //!
    TBranch *b_mu_staco_MET_TauMuGamma_comp_wpx;                    //!
    TBranch *b_mu_staco_MET_TauMuGamma_comp_wpy;                    //!
    TBranch *b_mu_staco_MET_TauMuGamma_comp_wet;                    //!
    TBranch *b_mu_staco_MET_TauMuGamma_comp_statusWord;             //!
    TBranch *b_mu_muid_MET_TauMuGamma_comp_n;                       //!
    TBranch *b_mu_muid_MET_TauMuGamma_comp_wpx;                     //!
    TBranch *b_mu_muid_MET_TauMuGamma_comp_wpy;                     //!
    TBranch *b_mu_muid_MET_TauMuGamma_comp_wet;                     //!
    TBranch *b_mu_muid_MET_TauMuGamma_comp_statusWord;              //!
    TBranch *b_mu_muon_MET_TauMuGamma_comp_n;                       //!
    TBranch *b_mu_muon_MET_TauMuGamma_comp_wpx;                     //!
    TBranch *b_mu_muon_MET_TauMuGamma_comp_wpy;                     //!
    TBranch *b_mu_muon_MET_TauMuGamma_comp_wet;                     //!
    TBranch *b_mu_muon_MET_TauMuGamma_comp_statusWord;              //!
    TBranch *b_tau_MET_TauMuGamma_comp_n;                           //!
    TBranch *b_tau_MET_TauMuGamma_comp_wpx;                         //!
    TBranch *b_tau_MET_TauMuGamma_comp_wpy;                         //!
    TBranch *b_tau_MET_TauMuGamma_comp_wet;                         //!
    TBranch *b_tau_MET_TauMuGamma_comp_statusWord;                  //!
    TBranch *b_jet_AntiKt4LCTopo_MET_TauMuGamma_comp_n;             //!
    TBranch *b_jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wpx;           //!
    TBranch *b_jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wpy;           //!
    TBranch *b_jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wet;           //!
    TBranch *b_jet_AntiKt4LCTopo_MET_TauMuGamma_comp_statusWord;    //!
    TBranch *b_cl_MET_TauMuGamma_comp_n;                            //!
    TBranch *b_cl_MET_TauMuGamma_comp_wpx;                          //!
    TBranch *b_cl_MET_TauMuGamma_comp_wpy;                          //!
    TBranch *b_cl_MET_TauMuGamma_comp_wet;                          //!
    TBranch *b_cl_MET_TauMuGamma_comp_statusWord;                   //!
    TBranch *b_trk_MET_TauMuGamma_comp_n;                           //!
    TBranch *b_trk_MET_TauMuGamma_comp_wpx;                         //!
    TBranch *b_trk_MET_TauMuGamma_comp_wpy;                         //!
    TBranch *b_trk_MET_TauMuGamma_comp_wet;                         //!
    TBranch *b_trk_MET_TauMuGamma_comp_statusWord;                  //!
    TBranch *b_ph_n;                                                //!
    TBranch *b_ph_E;                                                //!
    TBranch *b_ph_Et;                                               //!
    TBranch *b_ph_pt;                                               //!
    TBranch *b_ph_m;                                                //!
    TBranch *b_ph_eta;                                              //!
    TBranch *b_ph_phi;                                              //!
    TBranch *b_ph_px;                                               //!
    TBranch *b_ph_py;                                               //!
    TBranch *b_ph_pz;                                               //!
    TBranch *b_ph_author;                                           //!
    TBranch *b_ph_isRecovered;                                      //!
    TBranch *b_ph_isEM;                                             //!
    TBranch *b_ph_isEMLoose;                                        //!
    TBranch *b_ph_isEMMedium;                                       //!
    TBranch *b_ph_isEMTight;                                        //!
    TBranch *b_ph_OQ;                                               //!
    TBranch *b_ph_convFlag;                                         //!
    TBranch *b_ph_isConv;                                           //!
    TBranch *b_ph_nConv;                                            //!
    TBranch *b_ph_nSingleTrackConv;                                 //!
    TBranch *b_ph_nDoubleTrackConv;                                 //!
    TBranch *b_ph_loose;                                            //!
    TBranch *b_ph_looseIso;                                         //!
    TBranch *b_ph_tight;                                            //!
    TBranch *b_ph_tightIso;                                         //!
    TBranch *b_ph_looseAR;                                          //!
    TBranch *b_ph_looseARIso;                                       //!
    TBranch *b_ph_tightAR;                                          //!
    TBranch *b_ph_tightARIso;                                       //!
    TBranch *b_ph_goodOQ;                                           //!
    TBranch *b_ph_Ethad;                                            //!
    TBranch *b_ph_Ethad1;                                           //!
    TBranch *b_ph_E033;                                             //!
    TBranch *b_ph_f1;                                               //!
    TBranch *b_ph_f1core;                                           //!
    TBranch *b_ph_Emins1;                                           //!
    TBranch *b_ph_fside;                                            //!
    TBranch *b_ph_Emax2;                                            //!
    TBranch *b_ph_ws3;                                              //!
    TBranch *b_ph_wstot;                                            //!
    TBranch *b_ph_E132;                                             //!
    TBranch *b_ph_E1152;                                            //!
    TBranch *b_ph_emaxs1;                                           //!
    TBranch *b_ph_deltaEs;                                          //!
    TBranch *b_ph_E233;                                             //!
    TBranch *b_ph_E237;                                             //!
    TBranch *b_ph_E277;                                             //!
    TBranch *b_ph_weta2;                                            //!
    TBranch *b_ph_f3;                                               //!
    TBranch *b_ph_f3core;                                           //!
    TBranch *b_ph_rphiallcalo;                                      //!
    TBranch *b_ph_Etcone45;                                         //!
    TBranch *b_ph_Etcone15;                                         //!
    TBranch *b_ph_Etcone20;                                         //!
    TBranch *b_ph_Etcone25;                                         //!
    TBranch *b_ph_Etcone30;                                         //!
    TBranch *b_ph_Etcone35;                                         //!
    TBranch *b_ph_Etcone40;                                         //!
    TBranch *b_ph_ptcone20;                                         //!
    TBranch *b_ph_ptcone30;                                         //!
    TBranch *b_ph_ptcone40;                                         //!
    TBranch *b_ph_nucone20;                                         //!
    TBranch *b_ph_nucone30;                                         //!
    TBranch *b_ph_nucone40;                                         //!
    TBranch *b_ph_Etcone15_pt_corrected;                            //!
    TBranch *b_ph_Etcone20_pt_corrected;                            //!
    TBranch *b_ph_Etcone25_pt_corrected;                            //!
    TBranch *b_ph_Etcone30_pt_corrected;                            //!
    TBranch *b_ph_Etcone35_pt_corrected;                            //!
    TBranch *b_ph_Etcone40_pt_corrected;                            //!
    TBranch *b_ph_convanglematch;                                   //!
    TBranch *b_ph_convtrackmatch;                                   //!
    TBranch *b_ph_hasconv;                                          //!
    TBranch *b_ph_convvtxx;                                         //!
    TBranch *b_ph_convvtxy;                                         //!
    TBranch *b_ph_convvtxz;                                         //!
    TBranch *b_ph_Rconv;                                            //!
    TBranch *b_ph_zconv;                                            //!
    TBranch *b_ph_convvtxchi2;                                      //!
    TBranch *b_ph_pt1conv;                                          //!
    TBranch *b_ph_convtrk1nBLHits;                                  //!
    TBranch *b_ph_convtrk1nPixHits;                                 //!
    TBranch *b_ph_convtrk1nSCTHits;                                 //!
    TBranch *b_ph_convtrk1nTRTHits;                                 //!
    TBranch *b_ph_pt2conv;                                          //!
    TBranch *b_ph_convtrk2nBLHits;                                  //!
    TBranch *b_ph_convtrk2nPixHits;                                 //!
    TBranch *b_ph_convtrk2nSCTHits;                                 //!
    TBranch *b_ph_convtrk2nTRTHits;                                 //!
    TBranch *b_ph_ptconv;                                           //!
    TBranch *b_ph_pzconv;                                           //!
    TBranch *b_ph_reta;                                             //!
    TBranch *b_ph_rphi;                                             //!
    TBranch *b_ph_topoEtcone20;                                     //!
    TBranch *b_ph_topoEtcone30;                                     //!
    TBranch *b_ph_topoEtcone40;                                     //!
    TBranch *b_ph_materialTraversed;                                //!
    TBranch *b_ph_EtringnoisedR03sig2;                              //!
    TBranch *b_ph_EtringnoisedR03sig3;                              //!
    TBranch *b_ph_EtringnoisedR03sig4;                              //!
    TBranch *b_ph_ptcone20_zpv05;                                   //!
    TBranch *b_ph_ptcone30_zpv05;                                   //!
    TBranch *b_ph_ptcone40_zpv05;                                   //!
    TBranch *b_ph_nucone20_zpv05;                                   //!
    TBranch *b_ph_nucone30_zpv05;                                   //!
    TBranch *b_ph_nucone40_zpv05;                                   //!
    TBranch *b_ph_isolationlikelihoodjets;                          //!
    TBranch *b_ph_isolationlikelihoodhqelectrons;                   //!
    TBranch *b_ph_loglikelihood;                                    //!
    TBranch *b_ph_photonweight;                                     //!
    TBranch *b_ph_photonbgweight;                                   //!
    TBranch *b_ph_neuralnet;                                        //!
    TBranch *b_ph_Hmatrix;                                          //!
    TBranch *b_ph_Hmatrix5;                                         //!
    TBranch *b_ph_adaboost;                                         //!
    TBranch *b_ph_ringernn;                                         //!
    TBranch *b_ph_zvertex;                                          //!
    TBranch *b_ph_errz;                                             //!
    TBranch *b_ph_etap;                                             //!
    TBranch *b_ph_depth;                                            //!
    TBranch *b_ph_cl_E;                                             //!
    TBranch *b_ph_cl_pt;                                            //!
    TBranch *b_ph_cl_eta;                                           //!
    TBranch *b_ph_cl_phi;                                           //!
    TBranch *b_ph_cl_etaCalo;                                       //!
    TBranch *b_ph_cl_phiCalo;                                       //!
    TBranch *b_ph_Es0;                                              //!
    TBranch *b_ph_etas0;                                            //!
    TBranch *b_ph_phis0;                                            //!
    TBranch *b_ph_Es1;                                              //!
    TBranch *b_ph_etas1;                                            //!
    TBranch *b_ph_phis1;                                            //!
    TBranch *b_ph_Es2;                                              //!
    TBranch *b_ph_etas2;                                            //!
    TBranch *b_ph_phis2;                                            //!
    TBranch *b_ph_Es3;                                              //!
    TBranch *b_ph_etas3;                                            //!
    TBranch *b_ph_phis3;                                            //!
    TBranch *b_ph_rawcl_Es0;                                        //!
    TBranch *b_ph_rawcl_etas0;                                      //!
    TBranch *b_ph_rawcl_phis0;                                      //!
    TBranch *b_ph_rawcl_Es1;                                        //!
    TBranch *b_ph_rawcl_etas1;                                      //!
    TBranch *b_ph_rawcl_phis1;                                      //!
    TBranch *b_ph_rawcl_Es2;                                        //!
    TBranch *b_ph_rawcl_etas2;                                      //!
    TBranch *b_ph_rawcl_phis2;                                      //!
    TBranch *b_ph_rawcl_Es3;                                        //!
    TBranch *b_ph_rawcl_etas3;                                      //!
    TBranch *b_ph_rawcl_phis3;                                      //!
    TBranch *b_ph_rawcl_E;                                          //!
    TBranch *b_ph_rawcl_pt;                                         //!
    TBranch *b_ph_rawcl_eta;                                        //!
    TBranch *b_ph_rawcl_phi;                                        //!
    TBranch *b_ph_convMatchDeltaEta1;                               //!
    TBranch *b_ph_convMatchDeltaEta2;                               //!
    TBranch *b_ph_convMatchDeltaPhi1;                               //!
    TBranch *b_ph_convMatchDeltaPhi2;                               //!
    TBranch *b_ph_rings_E;                                          //!
    TBranch *b_ph_vx_n;                                             //!
    TBranch *b_ph_vx_x;                                             //!
    TBranch *b_ph_vx_y;                                             //!
    TBranch *b_ph_vx_z;                                             //!
    TBranch *b_ph_vx_px;                                            //!
    TBranch *b_ph_vx_py;                                            //!
    TBranch *b_ph_vx_pz;                                            //!
    TBranch *b_ph_vx_E;                                             //!
    TBranch *b_ph_vx_m;                                             //!
    TBranch *b_ph_vx_nTracks;                                       //!
    TBranch *b_ph_vx_sumPt;                                         //!
    TBranch *b_ph_vx_convTrk_weight;                                //!
    TBranch *b_ph_vx_convTrk_n;                                     //!
    TBranch *b_ph_vx_convTrk_fitter;                                //!
    TBranch *b_ph_vx_convTrk_patternReco1;                          //!
    TBranch *b_ph_vx_convTrk_patternReco2;                          //!
    TBranch *b_ph_vx_convTrk_trackProperties;                       //!
    TBranch *b_ph_vx_convTrk_particleHypothesis;                    //!
    TBranch *b_ph_vx_convTrk_chi2;                                  //!
    TBranch *b_ph_vx_convTrk_ndof;                                  //!
    TBranch *b_ph_deltaEmax2;                                       //!
    TBranch *b_ph_calibHitsShowerDepth;                             //!
    TBranch *b_ph_isIso;                                            //!
    TBranch *b_ph_mvaptcone20;                                      //!
    TBranch *b_ph_mvaptcone30;                                      //!
    TBranch *b_ph_mvaptcone40;                                      //!
    TBranch *b_ph_topoEtcone60;                                     //!
    TBranch *b_ph_vx_Chi2;                                          //!
    TBranch *b_ph_vx_Dcottheta;                                     //!
    TBranch *b_ph_vx_Dphi;                                          //!
    TBranch *b_ph_vx_Dist;                                          //!
    TBranch *b_ph_vx_DR1R2;                                         //!
    TBranch *b_ph_CaloPointing_eta;                                 //!
    TBranch *b_ph_CaloPointing_sigma_eta;                           //!
    TBranch *b_ph_CaloPointing_zvertex;                             //!
    TBranch *b_ph_CaloPointing_sigma_zvertex;                       //!
    TBranch *b_ph_HPV_eta;                                          //!
    TBranch *b_ph_HPV_sigma_eta;                                    //!
    TBranch *b_ph_HPV_zvertex;                                      //!
    TBranch *b_ph_HPV_sigma_zvertex;                                //!
    TBranch *b_ph_NN_passes;                                        //!
    TBranch *b_ph_NN_discriminant;                                  //!
    TBranch *b_ph_ES0_real;                                         //!
    TBranch *b_ph_ES1_real;                                         //!
    TBranch *b_ph_ES2_real;                                         //!
    TBranch *b_ph_ES3_real;                                         //!
    TBranch *b_ph_EcellS0;                                          //!
    TBranch *b_ph_etacellS0;                                        //!
    TBranch *b_ph_Etcone40_ED_corrected;                            //!
    TBranch *b_ph_Etcone40_corrected;                               //!
    TBranch *b_ph_topoEtcone20_corrected;                           //!
    TBranch *b_ph_topoEtcone30_corrected;                           //!
    TBranch *b_ph_topoEtcone40_corrected;                           //!
    TBranch *b_ph_ED_median;                                        //!
    TBranch *b_ph_ED_sigma;                                         //!
    TBranch *b_ph_ED_Njets;                                         //!
    TBranch *b_ph_convIP;                                           //!
    TBranch *b_ph_convIPRev;                                        //!
    TBranch *b_ph_jet_dr;                                           //!
    TBranch *b_ph_jet_E;                                            //!
    TBranch *b_ph_jet_pt;                                           //!
    TBranch *b_ph_jet_m;                                            //!
    TBranch *b_ph_jet_eta;                                          //!
    TBranch *b_ph_jet_phi;                                          //!
    TBranch *b_ph_jet_matched;                                      //!
    TBranch *b_ph_topodr;                                           //!
    TBranch *b_ph_topopt;                                           //!
    TBranch *b_ph_topoeta;                                          //!
    TBranch *b_ph_topophi;                                          //!
    TBranch *b_ph_topomatched;                                      //!
    TBranch *b_ph_topoEMdr;                                         //!
    TBranch *b_ph_topoEMpt;                                         //!
    TBranch *b_ph_topoEMeta;                                        //!
    TBranch *b_ph_topoEMphi;                                        //!
    TBranch *b_ph_topoEMmatched;                                    //!
    TBranch *b_ph_el_index;                                         //!
    TBranch *b_mu_muon_n;                                           //!
    TBranch *b_mu_muon_E;                                           //!
    TBranch *b_mu_muon_pt;                                          //!
    TBranch *b_mu_muon_m;                                           //!
    TBranch *b_mu_muon_eta;                                         //!
    TBranch *b_mu_muon_phi;                                         //!
    TBranch *b_mu_muon_px;                                          //!
    TBranch *b_mu_muon_py;                                          //!
    TBranch *b_mu_muon_pz;                                          //!
    TBranch *b_mu_muon_charge;                                      //!
    TBranch *b_mu_muon_allauthor;                                   //!
    TBranch *b_mu_muon_author;                                      //!
    TBranch *b_mu_muon_beta;                                        //!
    TBranch *b_mu_muon_isMuonLikelihood;                            //!
    TBranch *b_mu_muon_matchchi2;                                   //!
    TBranch *b_mu_muon_matchndof;                                   //!
    TBranch *b_mu_muon_etcone20;                                    //!
    TBranch *b_mu_muon_etcone30;                                    //!
    TBranch *b_mu_muon_etcone40;                                    //!
    TBranch *b_mu_muon_nucone20;                                    //!
    TBranch *b_mu_muon_nucone30;                                    //!
    TBranch *b_mu_muon_nucone40;                                    //!
    TBranch *b_mu_muon_ptcone20;                                    //!
    TBranch *b_mu_muon_ptcone30;                                    //!
    TBranch *b_mu_muon_ptcone40;                                    //!
    TBranch *b_mu_muon_etconeNoEm10;                                //!
    TBranch *b_mu_muon_etconeNoEm20;                                //!
    TBranch *b_mu_muon_etconeNoEm30;                                //!
    TBranch *b_mu_muon_etconeNoEm40;                                //!
    TBranch *b_mu_muon_scatteringCurvatureSignificance;             //!
    TBranch *b_mu_muon_scatteringNeighbourSignificance;             //!
    TBranch *b_mu_muon_momentumBalanceSignificance;                 //!
    TBranch *b_mu_muon_energyLossPar;                               //!
    TBranch *b_mu_muon_energyLossErr;                               //!
    TBranch *b_mu_muon_etCore;                                      //!
    TBranch *b_mu_muon_energyLossType;                              //!
    TBranch *b_mu_muon_caloMuonIdTag;                               //!
    TBranch *b_mu_muon_caloLRLikelihood;                            //!
    TBranch *b_mu_muon_Ehad;                                        //!
    TBranch *b_mu_muon_Mhad;                                        //!
    TBranch *b_mu_muon_Es0;                                         //!
    TBranch *b_mu_muon_Ms0;                                         //!
    TBranch *b_mu_muon_Es1;                                         //!
    TBranch *b_mu_muon_Ms1;                                         //!
    TBranch *b_mu_muon_Es2;                                         //!
    TBranch *b_mu_muon_Ms2;                                         //!
    TBranch *b_mu_muon_Es3;                                         //!
    TBranch *b_mu_muon_Ms3;                                         //!
    TBranch *b_mu_muon_bestMatch;                                   //!
    TBranch *b_mu_muon_isStandAloneMuon;                            //!
    TBranch *b_mu_muon_isCombinedMuon;                              //!
    TBranch *b_mu_muon_isLowPtReconstructedMuon;                    //!
    TBranch *b_mu_muon_isSegmentTaggedMuon;                         //!
    TBranch *b_mu_muon_isCaloMuonId;                                //!
    TBranch *b_mu_muon_alsoFoundByLowPt;                            //!
    TBranch *b_mu_muon_alsoFoundByCaloMuonId;                       //!
    TBranch *b_mu_muon_isSiliconAssociatedForwardMuon;              //!
    TBranch *b_mu_muon_loose;                                       //!
    TBranch *b_mu_muon_medium;                                      //!
    TBranch *b_mu_muon_tight;                                       //!
    TBranch *b_mu_muon_d0_exPV;                                     //!
    TBranch *b_mu_muon_z0_exPV;                                     //!
    TBranch *b_mu_muon_phi_exPV;                                    //!
    TBranch *b_mu_muon_theta_exPV;                                  //!
    TBranch *b_mu_muon_qoverp_exPV;                                 //!
    TBranch *b_mu_muon_cb_d0_exPV;                                  //!
    TBranch *b_mu_muon_cb_z0_exPV;                                  //!
    TBranch *b_mu_muon_cb_phi_exPV;                                 //!
    TBranch *b_mu_muon_cb_theta_exPV;                               //!
    TBranch *b_mu_muon_cb_qoverp_exPV;                              //!
    TBranch *b_mu_muon_id_d0_exPV;                                  //!
    TBranch *b_mu_muon_id_z0_exPV;                                  //!
    TBranch *b_mu_muon_id_phi_exPV;                                 //!
    TBranch *b_mu_muon_id_theta_exPV;                               //!
    TBranch *b_mu_muon_id_qoverp_exPV;                              //!
    TBranch *b_mu_muon_me_d0_exPV;                                  //!
    TBranch *b_mu_muon_me_z0_exPV;                                  //!
    TBranch *b_mu_muon_me_phi_exPV;                                 //!
    TBranch *b_mu_muon_me_theta_exPV;                               //!
    TBranch *b_mu_muon_me_qoverp_exPV;                              //!
    TBranch *b_mu_muon_ie_d0_exPV;                                  //!
    TBranch *b_mu_muon_ie_z0_exPV;                                  //!
    TBranch *b_mu_muon_ie_phi_exPV;                                 //!
    TBranch *b_mu_muon_ie_theta_exPV;                               //!
    TBranch *b_mu_muon_ie_qoverp_exPV;                              //!
    TBranch *b_mu_muon_SpaceTime_detID;                             //!
    TBranch *b_mu_muon_SpaceTime_t;                                 //!
    TBranch *b_mu_muon_SpaceTime_tError;                            //!
    TBranch *b_mu_muon_SpaceTime_weight;                            //!
    TBranch *b_mu_muon_SpaceTime_x;                                 //!
    TBranch *b_mu_muon_SpaceTime_y;                                 //!
    TBranch *b_mu_muon_SpaceTime_z;                                 //!
    TBranch *b_mu_muon_cov_d0_exPV;                                 //!
    TBranch *b_mu_muon_cov_z0_exPV;                                 //!
    TBranch *b_mu_muon_cov_phi_exPV;                                //!
    TBranch *b_mu_muon_cov_theta_exPV;                              //!
    TBranch *b_mu_muon_cov_qoverp_exPV;                             //!
    TBranch *b_mu_muon_cov_d0_z0_exPV;                              //!
    TBranch *b_mu_muon_cov_d0_phi_exPV;                             //!
    TBranch *b_mu_muon_cov_d0_theta_exPV;                           //!
    TBranch *b_mu_muon_cov_d0_qoverp_exPV;                          //!
    TBranch *b_mu_muon_cov_z0_phi_exPV;                             //!
    TBranch *b_mu_muon_cov_z0_theta_exPV;                           //!
    TBranch *b_mu_muon_cov_z0_qoverp_exPV;                          //!
    TBranch *b_mu_muon_cov_phi_theta_exPV;                          //!
    TBranch *b_mu_muon_cov_phi_qoverp_exPV;                         //!
    TBranch *b_mu_muon_cov_theta_qoverp_exPV;                       //!
    TBranch *b_mu_muon_id_cov_d0_exPV;                              //!
    TBranch *b_mu_muon_id_cov_z0_exPV;                              //!
    TBranch *b_mu_muon_id_cov_phi_exPV;                             //!
    TBranch *b_mu_muon_id_cov_theta_exPV;                           //!
    TBranch *b_mu_muon_id_cov_qoverp_exPV;                          //!
    TBranch *b_mu_muon_id_cov_d0_z0_exPV;                           //!
    TBranch *b_mu_muon_id_cov_d0_phi_exPV;                          //!
    TBranch *b_mu_muon_id_cov_d0_theta_exPV;                        //!
    TBranch *b_mu_muon_id_cov_d0_qoverp_exPV;                       //!
    TBranch *b_mu_muon_id_cov_z0_phi_exPV;                          //!
    TBranch *b_mu_muon_id_cov_z0_theta_exPV;                        //!
    TBranch *b_mu_muon_id_cov_z0_qoverp_exPV;                       //!
    TBranch *b_mu_muon_id_cov_phi_theta_exPV;                       //!
    TBranch *b_mu_muon_id_cov_phi_qoverp_exPV;                      //!
    TBranch *b_mu_muon_id_cov_theta_qoverp_exPV;                    //!
    TBranch *b_mu_muon_me_cov_d0_exPV;                              //!
    TBranch *b_mu_muon_me_cov_z0_exPV;                              //!
    TBranch *b_mu_muon_me_cov_phi_exPV;                             //!
    TBranch *b_mu_muon_me_cov_theta_exPV;                           //!
    TBranch *b_mu_muon_me_cov_qoverp_exPV;                          //!
    TBranch *b_mu_muon_me_cov_d0_z0_exPV;                           //!
    TBranch *b_mu_muon_me_cov_d0_phi_exPV;                          //!
    TBranch *b_mu_muon_me_cov_d0_theta_exPV;                        //!
    TBranch *b_mu_muon_me_cov_d0_qoverp_exPV;                       //!
    TBranch *b_mu_muon_me_cov_z0_phi_exPV;                          //!
    TBranch *b_mu_muon_me_cov_z0_theta_exPV;                        //!
    TBranch *b_mu_muon_me_cov_z0_qoverp_exPV;                       //!
    TBranch *b_mu_muon_me_cov_phi_theta_exPV;                       //!
    TBranch *b_mu_muon_me_cov_phi_qoverp_exPV;                      //!
    TBranch *b_mu_muon_me_cov_theta_qoverp_exPV;                    //!
    TBranch *b_mu_muon_ms_d0;                                       //!
    TBranch *b_mu_muon_ms_z0;                                       //!
    TBranch *b_mu_muon_ms_phi;                                      //!
    TBranch *b_mu_muon_ms_theta;                                    //!
    TBranch *b_mu_muon_ms_qoverp;                                   //!
    TBranch *b_mu_muon_id_d0;                                       //!
    TBranch *b_mu_muon_id_z0;                                       //!
    TBranch *b_mu_muon_id_phi;                                      //!
    TBranch *b_mu_muon_id_theta;                                    //!
    TBranch *b_mu_muon_id_qoverp;                                   //!
    TBranch *b_mu_muon_me_d0;                                       //!
    TBranch *b_mu_muon_me_z0;                                       //!
    TBranch *b_mu_muon_me_phi;                                      //!
    TBranch *b_mu_muon_me_theta;                                    //!
    TBranch *b_mu_muon_me_qoverp;                                   //!
    TBranch *b_mu_muon_ie_d0;                                       //!
    TBranch *b_mu_muon_ie_z0;                                       //!
    TBranch *b_mu_muon_ie_phi;                                      //!
    TBranch *b_mu_muon_ie_theta;                                    //!
    TBranch *b_mu_muon_ie_qoverp;                                   //!
    TBranch *b_mu_muon_nOutliersOnTrack;                            //!
    TBranch *b_mu_muon_nBLHits;                                     //!
    TBranch *b_mu_muon_nPixHits;                                    //!
    TBranch *b_mu_muon_nSCTHits;                                    //!
    TBranch *b_mu_muon_nTRTHits;                                    //!
    TBranch *b_mu_muon_nTRTHighTHits;                               //!
    TBranch *b_mu_muon_nBLSharedHits;                               //!
    TBranch *b_mu_muon_nPixSharedHits;                              //!
    TBranch *b_mu_muon_nPixHoles;                                   //!
    TBranch *b_mu_muon_nSCTSharedHits;                              //!
    TBranch *b_mu_muon_nSCTHoles;                                   //!
    TBranch *b_mu_muon_nTRTOutliers;                                //!
    TBranch *b_mu_muon_nTRTHighTOutliers;                           //!
    TBranch *b_mu_muon_nGangedPixels;                               //!
    TBranch *b_mu_muon_nPixelDeadSensors;                           //!
    TBranch *b_mu_muon_nSCTDeadSensors;                             //!
    TBranch *b_mu_muon_nTRTDeadStraws;                              //!
    TBranch *b_mu_muon_expectBLayerHit;                             //!
    TBranch *b_mu_muon_nMDTHits;                                    //!
    TBranch *b_mu_muon_nMDTHoles;                                   //!
    TBranch *b_mu_muon_nCSCEtaHits;                                 //!
    TBranch *b_mu_muon_nCSCEtaHoles;                                //!
    TBranch *b_mu_muon_nCSCUnspoiledEtaHits;                        //!
    TBranch *b_mu_muon_nCSCPhiHits;                                 //!
    TBranch *b_mu_muon_nCSCPhiHoles;                                //!
    TBranch *b_mu_muon_nRPCEtaHits;                                 //!
    TBranch *b_mu_muon_nRPCEtaHoles;                                //!
    TBranch *b_mu_muon_nRPCPhiHits;                                 //!
    TBranch *b_mu_muon_nRPCPhiHoles;                                //!
    TBranch *b_mu_muon_nTGCEtaHits;                                 //!
    TBranch *b_mu_muon_nTGCEtaHoles;                                //!
    TBranch *b_mu_muon_nTGCPhiHits;                                 //!
    TBranch *b_mu_muon_nTGCPhiHoles;                                //!
    TBranch *b_mu_muon_nprecisionLayers;                            //!
    TBranch *b_mu_muon_nprecisionHoleLayers;                        //!
    TBranch *b_mu_muon_nphiLayers;                                  //!
    TBranch *b_mu_muon_ntrigEtaLayers;                              //!
    TBranch *b_mu_muon_nphiHoleLayers;                              //!
    TBranch *b_mu_muon_ntrigEtaHoleLayers;                          //!
    TBranch *b_mu_muon_nMDTBIHits;                                  //!
    TBranch *b_mu_muon_nMDTBMHits;                                  //!
    TBranch *b_mu_muon_nMDTBOHits;                                  //!
    TBranch *b_mu_muon_nMDTBEEHits;                                 //!
    TBranch *b_mu_muon_nMDTBIS78Hits;                               //!
    TBranch *b_mu_muon_nMDTEIHits;                                  //!
    TBranch *b_mu_muon_nMDTEMHits;                                  //!
    TBranch *b_mu_muon_nMDTEOHits;                                  //!
    TBranch *b_mu_muon_nMDTEEHits;                                  //!
    TBranch *b_mu_muon_nRPCLayer1EtaHits;                           //!
    TBranch *b_mu_muon_nRPCLayer2EtaHits;                           //!
    TBranch *b_mu_muon_nRPCLayer3EtaHits;                           //!
    TBranch *b_mu_muon_nRPCLayer1PhiHits;                           //!
    TBranch *b_mu_muon_nRPCLayer2PhiHits;                           //!
    TBranch *b_mu_muon_nRPCLayer3PhiHits;                           //!
    TBranch *b_mu_muon_nTGCLayer1EtaHits;                           //!
    TBranch *b_mu_muon_nTGCLayer2EtaHits;                           //!
    TBranch *b_mu_muon_nTGCLayer3EtaHits;                           //!
    TBranch *b_mu_muon_nTGCLayer4EtaHits;                           //!
    TBranch *b_mu_muon_nTGCLayer1PhiHits;                           //!
    TBranch *b_mu_muon_nTGCLayer2PhiHits;                           //!
    TBranch *b_mu_muon_nTGCLayer3PhiHits;                           //!
    TBranch *b_mu_muon_nTGCLayer4PhiHits;                           //!
    TBranch *b_mu_muon_barrelSectors;                               //!
    TBranch *b_mu_muon_endcapSectors;                               //!
    TBranch *b_mu_muon_spec_surf_px;                                //!
    TBranch *b_mu_muon_spec_surf_py;                                //!
    TBranch *b_mu_muon_spec_surf_pz;                                //!
    TBranch *b_mu_muon_spec_surf_x;                                 //!
    TBranch *b_mu_muon_spec_surf_y;                                 //!
    TBranch *b_mu_muon_spec_surf_z;                                 //!
    TBranch *b_mu_muon_trackd0;                                     //!
    TBranch *b_mu_muon_trackz0;                                     //!
    TBranch *b_mu_muon_trackphi;                                    //!
    TBranch *b_mu_muon_tracktheta;                                  //!
    TBranch *b_mu_muon_trackqoverp;                                 //!
    TBranch *b_mu_muon_trackcov_d0;                                 //!
    TBranch *b_mu_muon_trackcov_z0;                                 //!
    TBranch *b_mu_muon_trackcov_phi;                                //!
    TBranch *b_mu_muon_trackcov_theta;                              //!
    TBranch *b_mu_muon_trackcov_qoverp;                             //!
    TBranch *b_mu_muon_trackcov_d0_z0;                              //!
    TBranch *b_mu_muon_trackcov_d0_phi;                             //!
    TBranch *b_mu_muon_trackcov_d0_theta;                           //!
    TBranch *b_mu_muon_trackcov_d0_qoverp;                          //!
    TBranch *b_mu_muon_trackcov_z0_phi;                             //!
    TBranch *b_mu_muon_trackcov_z0_theta;                           //!
    TBranch *b_mu_muon_trackcov_z0_qoverp;                          //!
    TBranch *b_mu_muon_trackcov_phi_theta;                          //!
    TBranch *b_mu_muon_trackcov_phi_qoverp;                         //!
    TBranch *b_mu_muon_trackcov_theta_qoverp;                       //!
    TBranch *b_mu_muon_trackfitchi2;                                //!
    TBranch *b_mu_muon_trackfitndof;                                //!
    TBranch *b_mu_muon_hastrack;                                    //!
    TBranch *b_mu_muon_trackd0beam;                                 //!
    TBranch *b_mu_muon_trackz0beam;                                 //!
    TBranch *b_mu_muon_tracksigd0beam;                              //!
    TBranch *b_mu_muon_tracksigz0beam;                              //!
    TBranch *b_mu_muon_trackd0pv;                                   //!
    TBranch *b_mu_muon_trackz0pv;                                   //!
    TBranch *b_mu_muon_tracksigd0pv;                                //!
    TBranch *b_mu_muon_tracksigz0pv;                                //!
    TBranch *b_mu_muon_trackIPEstimate_d0_biasedpvunbiased;         //!
    TBranch *b_mu_muon_trackIPEstimate_z0_biasedpvunbiased;         //!
    TBranch *b_mu_muon_trackIPEstimate_sigd0_biasedpvunbiased;      //!
    TBranch *b_mu_muon_trackIPEstimate_sigz0_biasedpvunbiased;      //!
    TBranch *b_mu_muon_trackIPEstimate_d0_unbiasedpvunbiased;       //!
    TBranch *b_mu_muon_trackIPEstimate_z0_unbiasedpvunbiased;       //!
    TBranch *b_mu_muon_trackIPEstimate_sigd0_unbiasedpvunbiased;    //!
    TBranch *b_mu_muon_trackIPEstimate_sigz0_unbiasedpvunbiased;    //!
    TBranch *b_mu_muon_trackd0pvunbiased;                           //!
    TBranch *b_mu_muon_trackz0pvunbiased;                           //!
    TBranch *b_mu_muon_tracksigd0pvunbiased;                        //!
    TBranch *b_mu_muon_tracksigz0pvunbiased;                        //!
    TBranch *b_trk_selec_n;                                         //!
    TBranch *b_trk_selec_pt;                                        //!
    TBranch *b_trk_selec_eta;                                       //!
    TBranch *b_trk_selec_d0_wrtPV;                                  //!
    TBranch *b_trk_selec_z0_wrtPV;                                  //!
    TBranch *b_trk_selec_phi_wrtPV;                                 //!
    TBranch *b_trk_selec_theta_wrtPV;                               //!
    TBranch *b_trk_selec_qoverp_wrtPV;                              //!
    TBranch *b_trk_selec_err_d0_wrtPV;                              //!
    TBranch *b_trk_selec_err_z0_wrtPV;                              //!
    TBranch *b_trk_selec_err_phi_wrtPV;                             //!
    TBranch *b_trk_selec_err_theta_wrtPV;                           //!
    TBranch *b_trk_selec_err_qoverp_wrtPV;                          //!
    TBranch *b_trk_selec_chi2;                                      //!
    TBranch *b_trk_selec_ndof;                                      //!
    TBranch *b_trk_selec_nBLHits;                                   //!
    TBranch *b_trk_selec_nPixHits;                                  //!
    TBranch *b_trk_selec_nSCTHits;                                  //!
    TBranch *b_trk_selec_nTRTHits;                                  //!
    TBranch *b_trk_selec_nTRTHighTHits;                             //!
    TBranch *b_trk_selec_nTRTXenonHits;                             //!
    TBranch *b_cl_lc_n;                                             //!
    TBranch *b_cl_lc_pt;                                            //!
    TBranch *b_cl_lc_eta;                                           //!
    TBranch *b_cl_lc_phi;                                           //!
    TBranch *b_cl_lc_firstEdens;                                    //!
    TBranch *b_cl_lc_cellmaxfrac;                                   //!
    TBranch *b_cl_lc_longitudinal;                                  //!
    TBranch *b_cl_lc_secondlambda;                                  //!
    TBranch *b_cl_lc_lateral;                                       //!
    TBranch *b_cl_lc_secondR;                                       //!
    TBranch *b_cl_lc_centerlambda;                                  //!
    TBranch *b_cl_lc_deltaTheta;                                    //!
    TBranch *b_cl_lc_deltaPhi;                                      //!
    TBranch *b_cl_lc_centermag;                                     //!
    TBranch *b_cl_lc_time;                                          //!
    TBranch *b_cl_lc_E_PreSamplerB;                                 //!
    TBranch *b_cl_lc_E_EMB1;                                        //!
    TBranch *b_cl_lc_E_EMB2;                                        //!
    TBranch *b_cl_lc_E_EMB3;                                        //!
    TBranch *b_cl_lc_E_PreSamplerE;                                 //!
    TBranch *b_cl_lc_E_EME1;                                        //!
    TBranch *b_cl_lc_E_EME2;                                        //!
    TBranch *b_cl_lc_E_EME3;                                        //!
    TBranch *b_cl_lc_E_HEC0;                                        //!
    TBranch *b_cl_lc_E_HEC1;                                        //!
    TBranch *b_cl_lc_E_HEC2;                                        //!
    TBranch *b_cl_lc_E_HEC3;                                        //!
    TBranch *b_cl_lc_E_TileBar0;                                    //!
    TBranch *b_cl_lc_E_TileBar1;                                    //!
    TBranch *b_cl_lc_E_TileBar2;                                    //!
    TBranch *b_cl_lc_E_TileGap1;                                    //!
    TBranch *b_cl_lc_E_TileGap2;                                    //!
    TBranch *b_cl_lc_E_TileGap3;                                    //!
    TBranch *b_cl_lc_E_TileExt0;                                    //!
    TBranch *b_cl_lc_E_TileExt1;                                    //!
    TBranch *b_cl_lc_E_TileExt2;                                    //!
    TBranch *b_cl_lc_E_FCAL0;                                       //!
    TBranch *b_cl_lc_E_FCAL1;                                       //!
    TBranch *b_cl_lc_E_FCAL2;                                       //!
    TBranch *b_vxp_n;                                               //!
    TBranch *b_vxp_x;                                               //!
    TBranch *b_vxp_y;                                               //!
    TBranch *b_vxp_z;                                               //!
    TBranch *b_vxp_err_x;                                           //!
    TBranch *b_vxp_err_y;                                           //!
    TBranch *b_vxp_err_z;                                           //!
    TBranch *b_vxp_cov_xy;                                          //!
    TBranch *b_vxp_cov_xz;                                          //!
    TBranch *b_vxp_cov_yz;                                          //!
    TBranch *b_vxp_type;                                            //!
    TBranch *b_vxp_chi2;                                            //!
    TBranch *b_vxp_ndof;                                            //!
    TBranch *b_vxp_px;                                              //!
    TBranch *b_vxp_py;                                              //!
    TBranch *b_vxp_pz;                                              //!
    TBranch *b_vxp_E;                                               //!
    TBranch *b_vxp_m;                                               //!
    TBranch *b_vxp_nTracks;                                         //!
    TBranch *b_vxp_sumPt;                                           //!
    TBranch *b_vxp_trk_weight;                                      //!
    TBranch *b_vxp_trk_n;                                           //!
    TBranch *b_vxp_trk_index;                                       //!
    TBranch *b_beamSpot_x;                                          //!
    TBranch *b_beamSpot_y;                                          //!
    TBranch *b_beamSpot_z;                                          //!
    TBranch *b_beamSpot_sigma_x;                                    //!
    TBranch *b_beamSpot_sigma_y;                                    //!
    TBranch *b_beamSpot_sigma_z;                                    //!
    TBranch *b_beamSpot_tilt_x;                                     //!
    TBranch *b_beamSpot_tilt_y;                                     //!
    TBranch *b_Eventshape_rhoKt3EM;                                 //!
    TBranch *b_Eventshape_rhoKt4EM;                                 //!
    TBranch *b_Eventshape_rhoKt3LC;                                 //!
    TBranch *b_Eventshape_rhoKt4LC;                                 //!

    physv6(TTree *tree = 0);
    virtual ~physv6();
    virtual Int_t Cut(Long64_t entry);
    virtual Int_t GetEntry(Long64_t entry);
    virtual Long64_t LoadTree(Long64_t entry);
    virtual void Init(TTree *tree);
    virtual void Loop();
    virtual Bool_t Notify();
    virtual void Show(Long64_t entry = -1);
};

#endif

#ifdef physv6_cxx
physv6::physv6(TTree *tree) {
    // if parameter tree is not specified (or zero), connect the file
    // used to generate this class and read the Tree.
    if (tree == 0) {
        TFile *f = (TFile *)gROOT->GetListOfFiles()->FindObject("v6.root");
        if (!f || !f->IsOpen()) {
            f = new TFile("v6.root");
        }
        f->GetObject("physics", tree);
    }
    Init(tree);
}

physv6::~physv6() {
    if (!fChain) return;
    delete fChain->GetCurrentFile();
}

Int_t physv6::GetEntry(Long64_t entry) {
    // Read contents of entry.
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}
Long64_t physv6::LoadTree(Long64_t entry) {
    // Set the environment to read one entry
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
        fCurrent = fChain->GetTreeNumber();
        Notify();
    }
    return centry;
}

void physv6::Init(TTree *tree) {
    // The Init() function is called when the selector needs to initialize
    // a new tree or chain. Typically here the branch addresses and branch
    // pointers of the tree will be set.
    // It is normally not necessary to make changes to the generated
    // code, but the routine can be extended by the user if needed.
    // Init() will be called many times when running on PROOF
    // (once per file to be processed).

    // Set object pointer
    trig_EF_trigmuonef_track_n = 0;
    trig_EF_trigmuonef_track_MuonType = 0;
    trig_EF_trigmuonef_track_MS_pt = 0;
    trig_EF_trigmuonef_track_MS_eta = 0;
    trig_EF_trigmuonef_track_MS_phi = 0;
    trig_EF_trigmuonef_track_MS_hasMS = 0;
    trig_EF_trigmuonef_track_SA_pt = 0;
    trig_EF_trigmuonef_track_SA_eta = 0;
    trig_EF_trigmuonef_track_SA_phi = 0;
    trig_EF_trigmuonef_track_SA_hasSA = 0;
    trig_EF_trigmuonef_track_CB_pt = 0;
    trig_EF_trigmuonef_track_CB_eta = 0;
    trig_EF_trigmuonef_track_CB_phi = 0;
    trig_EF_trigmuonef_track_CB_hasCB = 0;
    trig_EF_trigmuonefisolation_sumTrkPtCone02 = 0;
    trig_EF_trigmuonefisolation_sumTrkPtCone03 = 0;
    trig_EF_trigmuonefisolation_trackPosition = 0;
    trig_EF_trigmuonefisolation_efinfo_index = 0;
    el_E = 0;
    el_Et = 0;
    el_pt = 0;
    el_m = 0;
    el_eta = 0;
    el_phi = 0;
    el_px = 0;
    el_py = 0;
    el_pz = 0;
    el_charge = 0;
    el_author = 0;
    el_isEM = 0;
    el_isEMLoose = 0;
    el_isEMMedium = 0;
    el_isEMTight = 0;
    el_OQ = 0;
    el_convFlag = 0;
    el_isConv = 0;
    el_nConv = 0;
    el_nSingleTrackConv = 0;
    el_nDoubleTrackConv = 0;
    el_mediumWithoutTrack = 0;
    el_mediumIsoWithoutTrack = 0;
    el_tightWithoutTrack = 0;
    el_tightIsoWithoutTrack = 0;
    el_loose = 0;
    el_looseIso = 0;
    el_medium = 0;
    el_mediumIso = 0;
    el_tight = 0;
    el_tightIso = 0;
    el_loosePP = 0;
    el_loosePPIso = 0;
    el_mediumPP = 0;
    el_mediumPPIso = 0;
    el_tightPP = 0;
    el_tightPPIso = 0;
    el_goodOQ = 0;
    el_Ethad = 0;
    el_Ethad1 = 0;
    el_f1 = 0;
    el_f1core = 0;
    el_Emins1 = 0;
    el_fside = 0;
    el_Emax2 = 0;
    el_ws3 = 0;
    el_wstot = 0;
    el_emaxs1 = 0;
    el_deltaEs = 0;
    el_E233 = 0;
    el_E237 = 0;
    el_E277 = 0;
    el_weta2 = 0;
    el_f3 = 0;
    el_f3core = 0;
    el_rphiallcalo = 0;
    el_Etcone45 = 0;
    el_Etcone15 = 0;
    el_Etcone20 = 0;
    el_Etcone25 = 0;
    el_Etcone30 = 0;
    el_Etcone35 = 0;
    el_Etcone40 = 0;
    el_ptcone20 = 0;
    el_ptcone30 = 0;
    el_ptcone40 = 0;
    el_nucone20 = 0;
    el_nucone30 = 0;
    el_nucone40 = 0;
    el_Etcone15_pt_corrected = 0;
    el_Etcone20_pt_corrected = 0;
    el_Etcone25_pt_corrected = 0;
    el_Etcone30_pt_corrected = 0;
    el_Etcone35_pt_corrected = 0;
    el_Etcone40_pt_corrected = 0;
    el_convanglematch = 0;
    el_convtrackmatch = 0;
    el_hasconv = 0;
    el_convvtxx = 0;
    el_convvtxy = 0;
    el_convvtxz = 0;
    el_Rconv = 0;
    el_zconv = 0;
    el_convvtxchi2 = 0;
    el_pt1conv = 0;
    el_convtrk1nBLHits = 0;
    el_convtrk1nPixHits = 0;
    el_convtrk1nSCTHits = 0;
    el_convtrk1nTRTHits = 0;
    el_pt2conv = 0;
    el_convtrk2nBLHits = 0;
    el_convtrk2nPixHits = 0;
    el_convtrk2nSCTHits = 0;
    el_convtrk2nTRTHits = 0;
    el_ptconv = 0;
    el_pzconv = 0;
    el_pos7 = 0;
    el_etacorrmag = 0;
    el_deltaeta1 = 0;
    el_deltaeta2 = 0;
    el_deltaphi2 = 0;
    el_deltaphiRescaled = 0;
    el_deltaPhiFromLast = 0;
    el_deltaPhiRot = 0;
    el_expectHitInBLayer = 0;
    el_trackd0_physics = 0;
    el_etaSampling1 = 0;
    el_reta = 0;
    el_rphi = 0;
    el_topoEtcone20 = 0;
    el_topoEtcone30 = 0;
    el_topoEtcone40 = 0;
    el_materialTraversed = 0;
    el_EtringnoisedR03sig2 = 0;
    el_EtringnoisedR03sig3 = 0;
    el_EtringnoisedR03sig4 = 0;
    el_ptcone20_zpv05 = 0;
    el_ptcone30_zpv05 = 0;
    el_ptcone40_zpv05 = 0;
    el_nucone20_zpv05 = 0;
    el_nucone30_zpv05 = 0;
    el_nucone40_zpv05 = 0;
    el_isolationlikelihoodjets = 0;
    el_isolationlikelihoodhqelectrons = 0;
    el_electronweight = 0;
    el_electronbgweight = 0;
    el_softeweight = 0;
    el_softebgweight = 0;
    el_neuralnet = 0;
    el_Hmatrix = 0;
    el_Hmatrix5 = 0;
    el_adaboost = 0;
    el_softeneuralnet = 0;
    el_ringernn = 0;
    el_zvertex = 0;
    el_errz = 0;
    el_etap = 0;
    el_depth = 0;
    el_Es0 = 0;
    el_etas0 = 0;
    el_phis0 = 0;
    el_Es1 = 0;
    el_etas1 = 0;
    el_phis1 = 0;
    el_Es2 = 0;
    el_etas2 = 0;
    el_phis2 = 0;
    el_Es3 = 0;
    el_etas3 = 0;
    el_phis3 = 0;
    el_E_PreSamplerB = 0;
    el_E_EMB1 = 0;
    el_E_EMB2 = 0;
    el_E_EMB3 = 0;
    el_E_PreSamplerE = 0;
    el_E_EME1 = 0;
    el_E_EME2 = 0;
    el_E_EME3 = 0;
    el_E_HEC0 = 0;
    el_E_HEC1 = 0;
    el_E_HEC2 = 0;
    el_E_HEC3 = 0;
    el_E_TileBar0 = 0;
    el_E_TileBar1 = 0;
    el_E_TileBar2 = 0;
    el_E_TileGap1 = 0;
    el_E_TileGap2 = 0;
    el_E_TileGap3 = 0;
    el_E_TileExt0 = 0;
    el_E_TileExt1 = 0;
    el_E_TileExt2 = 0;
    el_E_FCAL0 = 0;
    el_E_FCAL1 = 0;
    el_E_FCAL2 = 0;
    el_cl_E = 0;
    el_cl_pt = 0;
    el_cl_eta = 0;
    el_cl_phi = 0;
    el_cl_etaCalo = 0;
    el_cl_phiCalo = 0;
    el_firstEdens = 0;
    el_cellmaxfrac = 0;
    el_longitudinal = 0;
    el_secondlambda = 0;
    el_lateral = 0;
    el_secondR = 0;
    el_centerlambda = 0;
    el_rawcl_Es0 = 0;
    el_rawcl_etas0 = 0;
    el_rawcl_phis0 = 0;
    el_rawcl_Es1 = 0;
    el_rawcl_etas1 = 0;
    el_rawcl_phis1 = 0;
    el_rawcl_Es2 = 0;
    el_rawcl_etas2 = 0;
    el_rawcl_phis2 = 0;
    el_rawcl_Es3 = 0;
    el_rawcl_etas3 = 0;
    el_rawcl_phis3 = 0;
    el_rawcl_E = 0;
    el_rawcl_pt = 0;
    el_rawcl_eta = 0;
    el_rawcl_phi = 0;
    el_trackd0 = 0;
    el_trackz0 = 0;
    el_trackphi = 0;
    el_tracktheta = 0;
    el_trackqoverp = 0;
    el_trackpt = 0;
    el_tracketa = 0;
    el_trackfitchi2 = 0;
    el_trackfitndof = 0;
    el_nBLHits = 0;
    el_nPixHits = 0;
    el_nSCTHits = 0;
    el_nTRTHits = 0;
    el_nTRTHighTHits = 0;
    el_nTRTXenonHits = 0;
    el_nPixHoles = 0;
    el_nSCTHoles = 0;
    el_nTRTHoles = 0;
    el_nPixelDeadSensors = 0;
    el_nSCTDeadSensors = 0;
    el_nBLSharedHits = 0;
    el_nPixSharedHits = 0;
    el_nSCTSharedHits = 0;
    el_nBLayerSplitHits = 0;
    el_nPixSplitHits = 0;
    el_nBLayerOutliers = 0;
    el_nPixelOutliers = 0;
    el_nSCTOutliers = 0;
    el_nTRTOutliers = 0;
    el_nTRTHighTOutliers = 0;
    el_nContribPixelLayers = 0;
    el_nGangedPixels = 0;
    el_nGangedFlaggedFakes = 0;
    el_nPixelSpoiltHits = 0;
    el_nSCTDoubleHoles = 0;
    el_nSCTSpoiltHits = 0;
    el_expectBLayerHit = 0;
    el_nSiHits = 0;
    el_TRTHighTHitsRatio = 0;
    el_TRTHighTOutliersRatio = 0;
    el_pixeldEdx = 0;
    el_nGoodHitsPixeldEdx = 0;
    el_massPixeldEdx = 0;
    el_likelihoodsPixeldEdx = 0;
    el_eProbabilityComb = 0;
    el_eProbabilityHT = 0;
    el_eProbabilityToT = 0;
    el_eProbabilityBrem = 0;
    el_vertweight = 0;
    el_vertx = 0;
    el_verty = 0;
    el_vertz = 0;
    el_trackd0beam = 0;
    el_trackz0beam = 0;
    el_tracksigd0beam = 0;
    el_tracksigz0beam = 0;
    el_trackd0pv = 0;
    el_trackz0pv = 0;
    el_tracksigd0pv = 0;
    el_tracksigz0pv = 0;
    el_trackIPEstimate_d0_biasedpvunbiased = 0;
    el_trackIPEstimate_z0_biasedpvunbiased = 0;
    el_trackIPEstimate_sigd0_biasedpvunbiased = 0;
    el_trackIPEstimate_sigz0_biasedpvunbiased = 0;
    el_trackIPEstimate_d0_unbiasedpvunbiased = 0;
    el_trackIPEstimate_z0_unbiasedpvunbiased = 0;
    el_trackIPEstimate_sigd0_unbiasedpvunbiased = 0;
    el_trackIPEstimate_sigz0_unbiasedpvunbiased = 0;
    el_trackd0pvunbiased = 0;
    el_trackz0pvunbiased = 0;
    el_tracksigd0pvunbiased = 0;
    el_tracksigz0pvunbiased = 0;
    el_Unrefittedtrack_d0 = 0;
    el_Unrefittedtrack_z0 = 0;
    el_Unrefittedtrack_phi = 0;
    el_Unrefittedtrack_theta = 0;
    el_Unrefittedtrack_qoverp = 0;
    el_Unrefittedtrack_pt = 0;
    el_Unrefittedtrack_eta = 0;
    el_theta_LM = 0;
    el_qoverp_LM = 0;
    el_theta_err_LM = 0;
    el_qoverp_err_LM = 0;
    el_hastrack = 0;
    el_deltaEmax2 = 0;
    el_calibHitsShowerDepth = 0;
    el_isIso = 0;
    el_mvaptcone20 = 0;
    el_mvaptcone30 = 0;
    el_mvaptcone40 = 0;
    el_CaloPointing_eta = 0;
    el_CaloPointing_sigma_eta = 0;
    el_CaloPointing_zvertex = 0;
    el_CaloPointing_sigma_zvertex = 0;
    el_HPV_eta = 0;
    el_HPV_sigma_eta = 0;
    el_HPV_zvertex = 0;
    el_HPV_sigma_zvertex = 0;
    el_topoEtcone60 = 0;
    el_ES0_real = 0;
    el_ES1_real = 0;
    el_ES2_real = 0;
    el_ES3_real = 0;
    el_EcellS0 = 0;
    el_etacellS0 = 0;
    el_Etcone40_ED_corrected = 0;
    el_Etcone40_corrected = 0;
    el_topoEtcone20_corrected = 0;
    el_topoEtcone30_corrected = 0;
    el_topoEtcone40_corrected = 0;
    el_ED_median = 0;
    el_ED_sigma = 0;
    el_ED_Njets = 0;
    el_jet_dr = 0;
    el_jet_E = 0;
    el_jet_pt = 0;
    el_jet_m = 0;
    el_jet_eta = 0;
    el_jet_phi = 0;
    el_jet_matched = 0;
    tau_Et = 0;
    tau_pt = 0;
    tau_m = 0;
    tau_eta = 0;
    tau_phi = 0;
    tau_charge = 0;
    tau_BDTEleScore = 0;
    tau_BDTJetScore = 0;
    tau_likelihood = 0;
    tau_SafeLikelihood = 0;
    tau_electronVetoLoose = 0;
    tau_electronVetoMedium = 0;
    tau_electronVetoTight = 0;
    tau_muonVeto = 0;
    tau_tauLlhLoose = 0;
    tau_tauLlhMedium = 0;
    tau_tauLlhTight = 0;
    tau_JetBDTSigLoose = 0;
    tau_JetBDTSigMedium = 0;
    tau_JetBDTSigTight = 0;
    tau_EleBDTLoose = 0;
    tau_EleBDTMedium = 0;
    tau_EleBDTTight = 0;
    tau_author = 0;
    tau_RoIWord = 0;
    tau_nProng = 0;
    tau_numTrack = 0;
    tau_seedCalo_numTrack = 0;
    tau_seedCalo_nWideTrk = 0;
    tau_nOtherTrk = 0;
    tau_track_atTJVA_n = 0;
    tau_seedCalo_wideTrk_atTJVA_n = 0;
    tau_otherTrk_atTJVA_n = 0;
    tau_track_n = 0;
    tau_seedCalo_track_n = 0;
    tau_seedCalo_wideTrk_n = 0;
    tau_otherTrk_n = 0;
    jet_akt4topoLC_E = 0;
    jet_akt4topoLC_pt = 0;
    jet_akt4topoLC_m = 0;
    jet_akt4topoLC_eta = 0;
    jet_akt4topoLC_phi = 0;
    jet_akt4topoLC_WIDTH = 0;
    jet_akt4topoLC_n90 = 0;
    jet_akt4topoLC_Timing = 0;
    jet_akt4topoLC_LArQuality = 0;
    jet_akt4topoLC_nTrk = 0;
    jet_akt4topoLC_sumPtTrk = 0;
    jet_akt4topoLC_OriginIndex = 0;
    jet_akt4topoLC_HECQuality = 0;
    jet_akt4topoLC_NegativeE = 0;
    jet_akt4topoLC_AverageLArQF = 0;
    jet_akt4topoLC_BCH_CORR_CELL = 0;
    jet_akt4topoLC_BCH_CORR_DOTX = 0;
    jet_akt4topoLC_BCH_CORR_JET = 0;
    jet_akt4topoLC_BCH_CORR_JET_FORCELL = 0;
    jet_akt4topoLC_ENG_BAD_CELLS = 0;
    jet_akt4topoLC_N_BAD_CELLS = 0;
    jet_akt4topoLC_N_BAD_CELLS_CORR = 0;
    jet_akt4topoLC_BAD_CELLS_CORR_E = 0;
    jet_akt4topoLC_NumTowers = 0;
    jet_akt4topoLC_ootFracCells5 = 0;
    jet_akt4topoLC_ootFracCells10 = 0;
    jet_akt4topoLC_ootFracClusters5 = 0;
    jet_akt4topoLC_ootFracClusters10 = 0;
    jet_akt4topoLC_emfrac = 0;
    jet_akt4topoLC_LCJES = 0;
    jet_akt4topoLC_LCJES_EtaCorr = 0;
    jet_akt4topoLC_emscale_E = 0;
    jet_akt4topoLC_emscale_pt = 0;
    jet_akt4topoLC_emscale_m = 0;
    jet_akt4topoLC_emscale_eta = 0;
    jet_akt4topoLC_emscale_phi = 0;
    jet_akt4topoLC_jvtx_x = 0;
    jet_akt4topoLC_jvtx_y = 0;
    jet_akt4topoLC_jvtx_z = 0;
    jet_akt4topoLC_Nconst = 0;
    jet_akt4topoLC_ptconst_default = 0;
    jet_akt4topoLC_econst_default = 0;
    jet_akt4topoLC_etaconst_default = 0;
    jet_akt4topoLC_phiconst_default = 0;
    jet_akt4topoLC_weightconst_default = 0;
    jet_akt4topoLC_constscale_E = 0;
    jet_akt4topoLC_constscale_pt = 0;
    jet_akt4topoLC_constscale_m = 0;
    jet_akt4topoLC_constscale_eta = 0;
    jet_akt4topoLC_constscale_phi = 0;
    jet_akt4topoLC_flavor_weight_Comb = 0;
    jet_akt4topoLC_flavor_weight_GbbNN = 0;
    jet_akt4topoLC_flavor_weight_IP2D = 0;
    jet_akt4topoLC_flavor_weight_IP3D = 0;
    jet_akt4topoLC_flavor_weight_JetFitterCOMBNN = 0;
    jet_akt4topoLC_flavor_weight_JetFitterTagNN = 0;
    jet_akt4topoLC_flavor_weight_MV1 = 0;
    jet_akt4topoLC_flavor_weight_MV2 = 0;
    jet_akt4topoLC_flavor_weight_SV0 = 0;
    jet_akt4topoLC_flavor_weight_SV1 = 0;
    jet_akt4topoLC_flavor_weight_SV2 = 0;
    jet_akt4topoLC_flavor_weight_SecondSoftMuonTagChi2 = 0;
    jet_akt4topoLC_flavor_weight_SoftMuonTagChi2 = 0;
    jet_akt4topoLC_flavor_assoctrk_n = 0;
    jet_akt4topoLC_flavor_assoctrk_index = 0;
    jet_akt4topoLC_el_dr = 0;
    jet_akt4topoLC_el_matched = 0;
    jet_akt4topoLC_mu_dr = 0;
    jet_akt4topoLC_mu_matched = 0;
    jet_akt4topoLC_L1_dr = 0;
    jet_akt4topoLC_L1_matched = 0;
    jet_akt4topoLC_L2_dr = 0;
    jet_akt4topoLC_L2_matched = 0;
    jet_akt4topoLC_EF_dr = 0;
    jet_akt4topoLC_EF_matched = 0;
    jet_akt4topoLC_nTrk_pv0_1GeV = 0;
    jet_akt4topoLC_sumPtTrk_pv0_1GeV = 0;
    jet_akt4topoLC_nTrk_allpv_1GeV = 0;
    jet_akt4topoLC_sumPtTrk_allpv_1GeV = 0;
    jet_akt4topoLC_nTrk_pv0_500MeV = 0;
    jet_akt4topoLC_sumPtTrk_pv0_500MeV = 0;
    jet_akt4topoLC_trackWIDTH_pv0_1GeV = 0;
    jet_akt4topoLC_trackWIDTH_allpv_1GeV = 0;
    el_MET_TauMuGamma_comp_wpx = 0;
    el_MET_TauMuGamma_comp_wpy = 0;
    el_MET_TauMuGamma_comp_wet = 0;
    el_MET_TauMuGamma_comp_statusWord = 0;
    ph_MET_TauMuGamma_comp_wpx = 0;
    ph_MET_TauMuGamma_comp_wpy = 0;
    ph_MET_TauMuGamma_comp_wet = 0;
    ph_MET_TauMuGamma_comp_statusWord = 0;
    mu_staco_MET_TauMuGamma_comp_wpx = 0;
    mu_staco_MET_TauMuGamma_comp_wpy = 0;
    mu_staco_MET_TauMuGamma_comp_wet = 0;
    mu_staco_MET_TauMuGamma_comp_statusWord = 0;
    mu_muid_MET_TauMuGamma_comp_wpx = 0;
    mu_muid_MET_TauMuGamma_comp_wpy = 0;
    mu_muid_MET_TauMuGamma_comp_wet = 0;
    mu_muid_MET_TauMuGamma_comp_statusWord = 0;
    mu_muon_MET_TauMuGamma_comp_wpx = 0;
    mu_muon_MET_TauMuGamma_comp_wpy = 0;
    mu_muon_MET_TauMuGamma_comp_wet = 0;
    mu_muon_MET_TauMuGamma_comp_statusWord = 0;
    tau_MET_TauMuGamma_comp_wpx = 0;
    tau_MET_TauMuGamma_comp_wpy = 0;
    tau_MET_TauMuGamma_comp_wet = 0;
    tau_MET_TauMuGamma_comp_statusWord = 0;
    jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wpx = 0;
    jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wpy = 0;
    jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wet = 0;
    jet_AntiKt4LCTopo_MET_TauMuGamma_comp_statusWord = 0;
    cl_MET_TauMuGamma_comp_wpx = 0;
    cl_MET_TauMuGamma_comp_wpy = 0;
    cl_MET_TauMuGamma_comp_wet = 0;
    cl_MET_TauMuGamma_comp_statusWord = 0;
    trk_MET_TauMuGamma_comp_wpx = 0;
    trk_MET_TauMuGamma_comp_wpy = 0;
    trk_MET_TauMuGamma_comp_wet = 0;
    trk_MET_TauMuGamma_comp_statusWord = 0;
    ph_E = 0;
    ph_Et = 0;
    ph_pt = 0;
    ph_m = 0;
    ph_eta = 0;
    ph_phi = 0;
    ph_px = 0;
    ph_py = 0;
    ph_pz = 0;
    ph_author = 0;
    ph_isRecovered = 0;
    ph_isEM = 0;
    ph_isEMLoose = 0;
    ph_isEMMedium = 0;
    ph_isEMTight = 0;
    ph_OQ = 0;
    ph_convFlag = 0;
    ph_isConv = 0;
    ph_nConv = 0;
    ph_nSingleTrackConv = 0;
    ph_nDoubleTrackConv = 0;
    ph_loose = 0;
    ph_looseIso = 0;
    ph_tight = 0;
    ph_tightIso = 0;
    ph_looseAR = 0;
    ph_looseARIso = 0;
    ph_tightAR = 0;
    ph_tightARIso = 0;
    ph_goodOQ = 0;
    ph_Ethad = 0;
    ph_Ethad1 = 0;
    ph_E033 = 0;
    ph_f1 = 0;
    ph_f1core = 0;
    ph_Emins1 = 0;
    ph_fside = 0;
    ph_Emax2 = 0;
    ph_ws3 = 0;
    ph_wstot = 0;
    ph_E132 = 0;
    ph_E1152 = 0;
    ph_emaxs1 = 0;
    ph_deltaEs = 0;
    ph_E233 = 0;
    ph_E237 = 0;
    ph_E277 = 0;
    ph_weta2 = 0;
    ph_f3 = 0;
    ph_f3core = 0;
    ph_rphiallcalo = 0;
    ph_Etcone45 = 0;
    ph_Etcone15 = 0;
    ph_Etcone20 = 0;
    ph_Etcone25 = 0;
    ph_Etcone30 = 0;
    ph_Etcone35 = 0;
    ph_Etcone40 = 0;
    ph_ptcone20 = 0;
    ph_ptcone30 = 0;
    ph_ptcone40 = 0;
    ph_nucone20 = 0;
    ph_nucone30 = 0;
    ph_nucone40 = 0;
    ph_Etcone15_pt_corrected = 0;
    ph_Etcone20_pt_corrected = 0;
    ph_Etcone25_pt_corrected = 0;
    ph_Etcone30_pt_corrected = 0;
    ph_Etcone35_pt_corrected = 0;
    ph_Etcone40_pt_corrected = 0;
    ph_convanglematch = 0;
    ph_convtrackmatch = 0;
    ph_hasconv = 0;
    ph_convvtxx = 0;
    ph_convvtxy = 0;
    ph_convvtxz = 0;
    ph_Rconv = 0;
    ph_zconv = 0;
    ph_convvtxchi2 = 0;
    ph_pt1conv = 0;
    ph_convtrk1nBLHits = 0;
    ph_convtrk1nPixHits = 0;
    ph_convtrk1nSCTHits = 0;
    ph_convtrk1nTRTHits = 0;
    ph_pt2conv = 0;
    ph_convtrk2nBLHits = 0;
    ph_convtrk2nPixHits = 0;
    ph_convtrk2nSCTHits = 0;
    ph_convtrk2nTRTHits = 0;
    ph_ptconv = 0;
    ph_pzconv = 0;
    ph_reta = 0;
    ph_rphi = 0;
    ph_topoEtcone20 = 0;
    ph_topoEtcone30 = 0;
    ph_topoEtcone40 = 0;
    ph_materialTraversed = 0;
    ph_EtringnoisedR03sig2 = 0;
    ph_EtringnoisedR03sig3 = 0;
    ph_EtringnoisedR03sig4 = 0;
    ph_ptcone20_zpv05 = 0;
    ph_ptcone30_zpv05 = 0;
    ph_ptcone40_zpv05 = 0;
    ph_nucone20_zpv05 = 0;
    ph_nucone30_zpv05 = 0;
    ph_nucone40_zpv05 = 0;
    ph_isolationlikelihoodjets = 0;
    ph_isolationlikelihoodhqelectrons = 0;
    ph_loglikelihood = 0;
    ph_photonweight = 0;
    ph_photonbgweight = 0;
    ph_neuralnet = 0;
    ph_Hmatrix = 0;
    ph_Hmatrix5 = 0;
    ph_adaboost = 0;
    ph_ringernn = 0;
    ph_zvertex = 0;
    ph_errz = 0;
    ph_etap = 0;
    ph_depth = 0;
    ph_cl_E = 0;
    ph_cl_pt = 0;
    ph_cl_eta = 0;
    ph_cl_phi = 0;
    ph_cl_etaCalo = 0;
    ph_cl_phiCalo = 0;
    ph_Es0 = 0;
    ph_etas0 = 0;
    ph_phis0 = 0;
    ph_Es1 = 0;
    ph_etas1 = 0;
    ph_phis1 = 0;
    ph_Es2 = 0;
    ph_etas2 = 0;
    ph_phis2 = 0;
    ph_Es3 = 0;
    ph_etas3 = 0;
    ph_phis3 = 0;
    ph_rawcl_Es0 = 0;
    ph_rawcl_etas0 = 0;
    ph_rawcl_phis0 = 0;
    ph_rawcl_Es1 = 0;
    ph_rawcl_etas1 = 0;
    ph_rawcl_phis1 = 0;
    ph_rawcl_Es2 = 0;
    ph_rawcl_etas2 = 0;
    ph_rawcl_phis2 = 0;
    ph_rawcl_Es3 = 0;
    ph_rawcl_etas3 = 0;
    ph_rawcl_phis3 = 0;
    ph_rawcl_E = 0;
    ph_rawcl_pt = 0;
    ph_rawcl_eta = 0;
    ph_rawcl_phi = 0;
    ph_convMatchDeltaEta1 = 0;
    ph_convMatchDeltaEta2 = 0;
    ph_convMatchDeltaPhi1 = 0;
    ph_convMatchDeltaPhi2 = 0;
    ph_rings_E = 0;
    ph_vx_n = 0;
    ph_vx_x = 0;
    ph_vx_y = 0;
    ph_vx_z = 0;
    ph_vx_px = 0;
    ph_vx_py = 0;
    ph_vx_pz = 0;
    ph_vx_E = 0;
    ph_vx_m = 0;
    ph_vx_nTracks = 0;
    ph_vx_sumPt = 0;
    ph_vx_convTrk_weight = 0;
    ph_vx_convTrk_n = 0;
    ph_vx_convTrk_fitter = 0;
    ph_vx_convTrk_patternReco1 = 0;
    ph_vx_convTrk_patternReco2 = 0;
    ph_vx_convTrk_trackProperties = 0;
    ph_vx_convTrk_particleHypothesis = 0;
    ph_vx_convTrk_chi2 = 0;
    ph_vx_convTrk_ndof = 0;
    ph_deltaEmax2 = 0;
    ph_calibHitsShowerDepth = 0;
    ph_isIso = 0;
    ph_mvaptcone20 = 0;
    ph_mvaptcone30 = 0;
    ph_mvaptcone40 = 0;
    ph_topoEtcone60 = 0;
    ph_vx_Chi2 = 0;
    ph_vx_Dcottheta = 0;
    ph_vx_Dphi = 0;
    ph_vx_Dist = 0;
    ph_vx_DR1R2 = 0;
    ph_CaloPointing_eta = 0;
    ph_CaloPointing_sigma_eta = 0;
    ph_CaloPointing_zvertex = 0;
    ph_CaloPointing_sigma_zvertex = 0;
    ph_HPV_eta = 0;
    ph_HPV_sigma_eta = 0;
    ph_HPV_zvertex = 0;
    ph_HPV_sigma_zvertex = 0;
    ph_NN_passes = 0;
    ph_NN_discriminant = 0;
    ph_ES0_real = 0;
    ph_ES1_real = 0;
    ph_ES2_real = 0;
    ph_ES3_real = 0;
    ph_EcellS0 = 0;
    ph_etacellS0 = 0;
    ph_Etcone40_ED_corrected = 0;
    ph_Etcone40_corrected = 0;
    ph_topoEtcone20_corrected = 0;
    ph_topoEtcone30_corrected = 0;
    ph_topoEtcone40_corrected = 0;
    ph_ED_median = 0;
    ph_ED_sigma = 0;
    ph_ED_Njets = 0;
    ph_convIP = 0;
    ph_convIPRev = 0;
    ph_jet_dr = 0;
    ph_jet_E = 0;
    ph_jet_pt = 0;
    ph_jet_m = 0;
    ph_jet_eta = 0;
    ph_jet_phi = 0;
    ph_jet_matched = 0;
    ph_topodr = 0;
    ph_topopt = 0;
    ph_topoeta = 0;
    ph_topophi = 0;
    ph_topomatched = 0;
    ph_topoEMdr = 0;
    ph_topoEMpt = 0;
    ph_topoEMeta = 0;
    ph_topoEMphi = 0;
    ph_topoEMmatched = 0;
    ph_el_index = 0;
    mu_muon_E = 0;
    mu_muon_pt = 0;
    mu_muon_m = 0;
    mu_muon_eta = 0;
    mu_muon_phi = 0;
    mu_muon_px = 0;
    mu_muon_py = 0;
    mu_muon_pz = 0;
    mu_muon_charge = 0;
    mu_muon_allauthor = 0;
    mu_muon_author = 0;
    mu_muon_beta = 0;
    mu_muon_isMuonLikelihood = 0;
    mu_muon_matchchi2 = 0;
    mu_muon_matchndof = 0;
    mu_muon_etcone20 = 0;
    mu_muon_etcone30 = 0;
    mu_muon_etcone40 = 0;
    mu_muon_nucone20 = 0;
    mu_muon_nucone30 = 0;
    mu_muon_nucone40 = 0;
    mu_muon_ptcone20 = 0;
    mu_muon_ptcone30 = 0;
    mu_muon_ptcone40 = 0;
    mu_muon_etconeNoEm10 = 0;
    mu_muon_etconeNoEm20 = 0;
    mu_muon_etconeNoEm30 = 0;
    mu_muon_etconeNoEm40 = 0;
    mu_muon_scatteringCurvatureSignificance = 0;
    mu_muon_scatteringNeighbourSignificance = 0;
    mu_muon_momentumBalanceSignificance = 0;
    mu_muon_energyLossPar = 0;
    mu_muon_energyLossErr = 0;
    mu_muon_etCore = 0;
    mu_muon_energyLossType = 0;
    mu_muon_caloMuonIdTag = 0;
    mu_muon_caloLRLikelihood = 0;
    mu_muon_Ehad = 0;
    mu_muon_Mhad = 0;
    mu_muon_Es0 = 0;
    mu_muon_Ms0 = 0;
    mu_muon_Es1 = 0;
    mu_muon_Ms1 = 0;
    mu_muon_Es2 = 0;
    mu_muon_Ms2 = 0;
    mu_muon_Es3 = 0;
    mu_muon_Ms3 = 0;
    mu_muon_bestMatch = 0;
    mu_muon_isStandAloneMuon = 0;
    mu_muon_isCombinedMuon = 0;
    mu_muon_isLowPtReconstructedMuon = 0;
    mu_muon_isSegmentTaggedMuon = 0;
    mu_muon_isCaloMuonId = 0;
    mu_muon_alsoFoundByLowPt = 0;
    mu_muon_alsoFoundByCaloMuonId = 0;
    mu_muon_isSiliconAssociatedForwardMuon = 0;
    mu_muon_loose = 0;
    mu_muon_medium = 0;
    mu_muon_tight = 0;
    mu_muon_d0_exPV = 0;
    mu_muon_z0_exPV = 0;
    mu_muon_phi_exPV = 0;
    mu_muon_theta_exPV = 0;
    mu_muon_qoverp_exPV = 0;
    mu_muon_cb_d0_exPV = 0;
    mu_muon_cb_z0_exPV = 0;
    mu_muon_cb_phi_exPV = 0;
    mu_muon_cb_theta_exPV = 0;
    mu_muon_cb_qoverp_exPV = 0;
    mu_muon_id_d0_exPV = 0;
    mu_muon_id_z0_exPV = 0;
    mu_muon_id_phi_exPV = 0;
    mu_muon_id_theta_exPV = 0;
    mu_muon_id_qoverp_exPV = 0;
    mu_muon_me_d0_exPV = 0;
    mu_muon_me_z0_exPV = 0;
    mu_muon_me_phi_exPV = 0;
    mu_muon_me_theta_exPV = 0;
    mu_muon_me_qoverp_exPV = 0;
    mu_muon_ie_d0_exPV = 0;
    mu_muon_ie_z0_exPV = 0;
    mu_muon_ie_phi_exPV = 0;
    mu_muon_ie_theta_exPV = 0;
    mu_muon_ie_qoverp_exPV = 0;
    mu_muon_SpaceTime_detID = 0;
    mu_muon_SpaceTime_t = 0;
    mu_muon_SpaceTime_tError = 0;
    mu_muon_SpaceTime_weight = 0;
    mu_muon_SpaceTime_x = 0;
    mu_muon_SpaceTime_y = 0;
    mu_muon_SpaceTime_z = 0;
    mu_muon_cov_d0_exPV = 0;
    mu_muon_cov_z0_exPV = 0;
    mu_muon_cov_phi_exPV = 0;
    mu_muon_cov_theta_exPV = 0;
    mu_muon_cov_qoverp_exPV = 0;
    mu_muon_cov_d0_z0_exPV = 0;
    mu_muon_cov_d0_phi_exPV = 0;
    mu_muon_cov_d0_theta_exPV = 0;
    mu_muon_cov_d0_qoverp_exPV = 0;
    mu_muon_cov_z0_phi_exPV = 0;
    mu_muon_cov_z0_theta_exPV = 0;
    mu_muon_cov_z0_qoverp_exPV = 0;
    mu_muon_cov_phi_theta_exPV = 0;
    mu_muon_cov_phi_qoverp_exPV = 0;
    mu_muon_cov_theta_qoverp_exPV = 0;
    mu_muon_id_cov_d0_exPV = 0;
    mu_muon_id_cov_z0_exPV = 0;
    mu_muon_id_cov_phi_exPV = 0;
    mu_muon_id_cov_theta_exPV = 0;
    mu_muon_id_cov_qoverp_exPV = 0;
    mu_muon_id_cov_d0_z0_exPV = 0;
    mu_muon_id_cov_d0_phi_exPV = 0;
    mu_muon_id_cov_d0_theta_exPV = 0;
    mu_muon_id_cov_d0_qoverp_exPV = 0;
    mu_muon_id_cov_z0_phi_exPV = 0;
    mu_muon_id_cov_z0_theta_exPV = 0;
    mu_muon_id_cov_z0_qoverp_exPV = 0;
    mu_muon_id_cov_phi_theta_exPV = 0;
    mu_muon_id_cov_phi_qoverp_exPV = 0;
    mu_muon_id_cov_theta_qoverp_exPV = 0;
    mu_muon_me_cov_d0_exPV = 0;
    mu_muon_me_cov_z0_exPV = 0;
    mu_muon_me_cov_phi_exPV = 0;
    mu_muon_me_cov_theta_exPV = 0;
    mu_muon_me_cov_qoverp_exPV = 0;
    mu_muon_me_cov_d0_z0_exPV = 0;
    mu_muon_me_cov_d0_phi_exPV = 0;
    mu_muon_me_cov_d0_theta_exPV = 0;
    mu_muon_me_cov_d0_qoverp_exPV = 0;
    mu_muon_me_cov_z0_phi_exPV = 0;
    mu_muon_me_cov_z0_theta_exPV = 0;
    mu_muon_me_cov_z0_qoverp_exPV = 0;
    mu_muon_me_cov_phi_theta_exPV = 0;
    mu_muon_me_cov_phi_qoverp_exPV = 0;
    mu_muon_me_cov_theta_qoverp_exPV = 0;
    mu_muon_ms_d0 = 0;
    mu_muon_ms_z0 = 0;
    mu_muon_ms_phi = 0;
    mu_muon_ms_theta = 0;
    mu_muon_ms_qoverp = 0;
    mu_muon_id_d0 = 0;
    mu_muon_id_z0 = 0;
    mu_muon_id_phi = 0;
    mu_muon_id_theta = 0;
    mu_muon_id_qoverp = 0;
    mu_muon_me_d0 = 0;
    mu_muon_me_z0 = 0;
    mu_muon_me_phi = 0;
    mu_muon_me_theta = 0;
    mu_muon_me_qoverp = 0;
    mu_muon_ie_d0 = 0;
    mu_muon_ie_z0 = 0;
    mu_muon_ie_phi = 0;
    mu_muon_ie_theta = 0;
    mu_muon_ie_qoverp = 0;
    mu_muon_nOutliersOnTrack = 0;
    mu_muon_nBLHits = 0;
    mu_muon_nPixHits = 0;
    mu_muon_nSCTHits = 0;
    mu_muon_nTRTHits = 0;
    mu_muon_nTRTHighTHits = 0;
    mu_muon_nBLSharedHits = 0;
    mu_muon_nPixSharedHits = 0;
    mu_muon_nPixHoles = 0;
    mu_muon_nSCTSharedHits = 0;
    mu_muon_nSCTHoles = 0;
    mu_muon_nTRTOutliers = 0;
    mu_muon_nTRTHighTOutliers = 0;
    mu_muon_nGangedPixels = 0;
    mu_muon_nPixelDeadSensors = 0;
    mu_muon_nSCTDeadSensors = 0;
    mu_muon_nTRTDeadStraws = 0;
    mu_muon_expectBLayerHit = 0;
    mu_muon_nMDTHits = 0;
    mu_muon_nMDTHoles = 0;
    mu_muon_nCSCEtaHits = 0;
    mu_muon_nCSCEtaHoles = 0;
    mu_muon_nCSCUnspoiledEtaHits = 0;
    mu_muon_nCSCPhiHits = 0;
    mu_muon_nCSCPhiHoles = 0;
    mu_muon_nRPCEtaHits = 0;
    mu_muon_nRPCEtaHoles = 0;
    mu_muon_nRPCPhiHits = 0;
    mu_muon_nRPCPhiHoles = 0;
    mu_muon_nTGCEtaHits = 0;
    mu_muon_nTGCEtaHoles = 0;
    mu_muon_nTGCPhiHits = 0;
    mu_muon_nTGCPhiHoles = 0;
    mu_muon_nprecisionLayers = 0;
    mu_muon_nprecisionHoleLayers = 0;
    mu_muon_nphiLayers = 0;
    mu_muon_ntrigEtaLayers = 0;
    mu_muon_nphiHoleLayers = 0;
    mu_muon_ntrigEtaHoleLayers = 0;
    mu_muon_nMDTBIHits = 0;
    mu_muon_nMDTBMHits = 0;
    mu_muon_nMDTBOHits = 0;
    mu_muon_nMDTBEEHits = 0;
    mu_muon_nMDTBIS78Hits = 0;
    mu_muon_nMDTEIHits = 0;
    mu_muon_nMDTEMHits = 0;
    mu_muon_nMDTEOHits = 0;
    mu_muon_nMDTEEHits = 0;
    mu_muon_nRPCLayer1EtaHits = 0;
    mu_muon_nRPCLayer2EtaHits = 0;
    mu_muon_nRPCLayer3EtaHits = 0;
    mu_muon_nRPCLayer1PhiHits = 0;
    mu_muon_nRPCLayer2PhiHits = 0;
    mu_muon_nRPCLayer3PhiHits = 0;
    mu_muon_nTGCLayer1EtaHits = 0;
    mu_muon_nTGCLayer2EtaHits = 0;
    mu_muon_nTGCLayer3EtaHits = 0;
    mu_muon_nTGCLayer4EtaHits = 0;
    mu_muon_nTGCLayer1PhiHits = 0;
    mu_muon_nTGCLayer2PhiHits = 0;
    mu_muon_nTGCLayer3PhiHits = 0;
    mu_muon_nTGCLayer4PhiHits = 0;
    mu_muon_barrelSectors = 0;
    mu_muon_endcapSectors = 0;
    mu_muon_spec_surf_px = 0;
    mu_muon_spec_surf_py = 0;
    mu_muon_spec_surf_pz = 0;
    mu_muon_spec_surf_x = 0;
    mu_muon_spec_surf_y = 0;
    mu_muon_spec_surf_z = 0;
    mu_muon_trackd0 = 0;
    mu_muon_trackz0 = 0;
    mu_muon_trackphi = 0;
    mu_muon_tracktheta = 0;
    mu_muon_trackqoverp = 0;
    mu_muon_trackcov_d0 = 0;
    mu_muon_trackcov_z0 = 0;
    mu_muon_trackcov_phi = 0;
    mu_muon_trackcov_theta = 0;
    mu_muon_trackcov_qoverp = 0;
    mu_muon_trackcov_d0_z0 = 0;
    mu_muon_trackcov_d0_phi = 0;
    mu_muon_trackcov_d0_theta = 0;
    mu_muon_trackcov_d0_qoverp = 0;
    mu_muon_trackcov_z0_phi = 0;
    mu_muon_trackcov_z0_theta = 0;
    mu_muon_trackcov_z0_qoverp = 0;
    mu_muon_trackcov_phi_theta = 0;
    mu_muon_trackcov_phi_qoverp = 0;
    mu_muon_trackcov_theta_qoverp = 0;
    mu_muon_trackfitchi2 = 0;
    mu_muon_trackfitndof = 0;
    mu_muon_hastrack = 0;
    mu_muon_trackd0beam = 0;
    mu_muon_trackz0beam = 0;
    mu_muon_tracksigd0beam = 0;
    mu_muon_tracksigz0beam = 0;
    mu_muon_trackd0pv = 0;
    mu_muon_trackz0pv = 0;
    mu_muon_tracksigd0pv = 0;
    mu_muon_tracksigz0pv = 0;
    mu_muon_trackIPEstimate_d0_biasedpvunbiased = 0;
    mu_muon_trackIPEstimate_z0_biasedpvunbiased = 0;
    mu_muon_trackIPEstimate_sigd0_biasedpvunbiased = 0;
    mu_muon_trackIPEstimate_sigz0_biasedpvunbiased = 0;
    mu_muon_trackIPEstimate_d0_unbiasedpvunbiased = 0;
    mu_muon_trackIPEstimate_z0_unbiasedpvunbiased = 0;
    mu_muon_trackIPEstimate_sigd0_unbiasedpvunbiased = 0;
    mu_muon_trackIPEstimate_sigz0_unbiasedpvunbiased = 0;
    mu_muon_trackd0pvunbiased = 0;
    mu_muon_trackz0pvunbiased = 0;
    mu_muon_tracksigd0pvunbiased = 0;
    mu_muon_tracksigz0pvunbiased = 0;
    trk_selec_pt = 0;
    trk_selec_eta = 0;
    trk_selec_d0_wrtPV = 0;
    trk_selec_z0_wrtPV = 0;
    trk_selec_phi_wrtPV = 0;
    trk_selec_theta_wrtPV = 0;
    trk_selec_qoverp_wrtPV = 0;
    trk_selec_err_d0_wrtPV = 0;
    trk_selec_err_z0_wrtPV = 0;
    trk_selec_err_phi_wrtPV = 0;
    trk_selec_err_theta_wrtPV = 0;
    trk_selec_err_qoverp_wrtPV = 0;
    trk_selec_chi2 = 0;
    trk_selec_ndof = 0;
    trk_selec_nBLHits = 0;
    trk_selec_nPixHits = 0;
    trk_selec_nSCTHits = 0;
    trk_selec_nTRTHits = 0;
    trk_selec_nTRTHighTHits = 0;
    trk_selec_nTRTXenonHits = 0;
    cl_lc_pt = 0;
    cl_lc_eta = 0;
    cl_lc_phi = 0;
    cl_lc_firstEdens = 0;
    cl_lc_cellmaxfrac = 0;
    cl_lc_longitudinal = 0;
    cl_lc_secondlambda = 0;
    cl_lc_lateral = 0;
    cl_lc_secondR = 0;
    cl_lc_centerlambda = 0;
    cl_lc_deltaTheta = 0;
    cl_lc_deltaPhi = 0;
    cl_lc_centermag = 0;
    cl_lc_time = 0;
    cl_lc_E_PreSamplerB = 0;
    cl_lc_E_EMB1 = 0;
    cl_lc_E_EMB2 = 0;
    cl_lc_E_EMB3 = 0;
    cl_lc_E_PreSamplerE = 0;
    cl_lc_E_EME1 = 0;
    cl_lc_E_EME2 = 0;
    cl_lc_E_EME3 = 0;
    cl_lc_E_HEC0 = 0;
    cl_lc_E_HEC1 = 0;
    cl_lc_E_HEC2 = 0;
    cl_lc_E_HEC3 = 0;
    cl_lc_E_TileBar0 = 0;
    cl_lc_E_TileBar1 = 0;
    cl_lc_E_TileBar2 = 0;
    cl_lc_E_TileGap1 = 0;
    cl_lc_E_TileGap2 = 0;
    cl_lc_E_TileGap3 = 0;
    cl_lc_E_TileExt0 = 0;
    cl_lc_E_TileExt1 = 0;
    cl_lc_E_TileExt2 = 0;
    cl_lc_E_FCAL0 = 0;
    cl_lc_E_FCAL1 = 0;
    cl_lc_E_FCAL2 = 0;
    vxp_x = 0;
    vxp_y = 0;
    vxp_z = 0;
    vxp_err_x = 0;
    vxp_err_y = 0;
    vxp_err_z = 0;
    vxp_cov_xy = 0;
    vxp_cov_xz = 0;
    vxp_cov_yz = 0;
    vxp_type = 0;
    vxp_chi2 = 0;
    vxp_ndof = 0;
    vxp_px = 0;
    vxp_py = 0;
    vxp_pz = 0;
    vxp_E = 0;
    vxp_m = 0;
    vxp_nTracks = 0;
    vxp_sumPt = 0;
    vxp_trk_weight = 0;
    vxp_trk_n = 0;
    vxp_trk_index = 0;
    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);

    fChain->SetBranchAddress("EF_2mu13", &EF_2mu13, &b_EF_2mu13);
    fChain->SetBranchAddress("EF_e12Tvh_medium1_mu8", &EF_e12Tvh_medium1_mu8, &b_EF_e12Tvh_medium1_mu8);
    fChain->SetBranchAddress("EF_e24vhi_loose1_mu8", &EF_e24vhi_loose1_mu8, &b_EF_e24vhi_loose1_mu8);
    fChain->SetBranchAddress("EF_e24vhi_medium1", &EF_e24vhi_medium1, &b_EF_e24vhi_medium1);
    fChain->SetBranchAddress("EF_e45_medium1", &EF_e45_medium1, &b_EF_e45_medium1);
    fChain->SetBranchAddress("EF_e60_medium1", &EF_e60_medium1, &b_EF_e60_medium1);
    fChain->SetBranchAddress("EF_mu10i_g10_medium", &EF_mu10i_g10_medium, &b_EF_mu10i_g10_medium);
    fChain->SetBranchAddress("EF_mu10i_g10_medium_TauMass", &EF_mu10i_g10_medium_TauMass, &b_EF_mu10i_g10_medium_TauMass);
    fChain->SetBranchAddress("EF_mu10i_loose_g12Tvh_medium", &EF_mu10i_loose_g12Tvh_medium, &b_EF_mu10i_loose_g12Tvh_medium);
    fChain->SetBranchAddress("EF_mu10i_loose_g12Tvh_medium_TauMass", &EF_mu10i_loose_g12Tvh_medium_TauMass,
                             &b_EF_mu10i_loose_g12Tvh_medium_TauMass);
    fChain->SetBranchAddress("EF_mu18_tight_mu8_EFFS", &EF_mu18_tight_mu8_EFFS, &b_EF_mu18_tight_mu8_EFFS);
    fChain->SetBranchAddress("EF_mu20i_tight_g5_loose_TauMass", &EF_mu20i_tight_g5_loose_TauMass,
                             &b_EF_mu20i_tight_g5_loose_TauMass);
    fChain->SetBranchAddress("EF_mu20i_tight_g5_medium", &EF_mu20i_tight_g5_medium, &b_EF_mu20i_tight_g5_medium);
    fChain->SetBranchAddress("EF_mu20i_tight_g5_medium_TauMass", &EF_mu20i_tight_g5_medium_TauMass,
                             &b_EF_mu20i_tight_g5_medium_TauMass);
    fChain->SetBranchAddress("EF_mu24i_tight", &EF_mu24i_tight, &b_EF_mu24i_tight);
    fChain->SetBranchAddress("EF_mu36_tight", &EF_mu36_tight, &b_EF_mu36_tight);
    fChain->SetBranchAddress("EF_mu4T", &EF_mu4T, &b_EF_mu4T);
    fChain->SetBranchAddress("EF_mu4Ti_g20Tvh_medium", &EF_mu4Ti_g20Tvh_medium, &b_EF_mu4Ti_g20Tvh_medium);
    fChain->SetBranchAddress("EF_mu4Ti_g20Tvh_medium_TauMass", &EF_mu4Ti_g20Tvh_medium_TauMass,
                             &b_EF_mu4Ti_g20Tvh_medium_TauMass);
    fChain->SetBranchAddress("EF_tau20_medium1_mu15", &EF_tau20_medium1_mu15, &b_EF_tau20_medium1_mu15);
    fChain->SetBranchAddress("EF_tau29Ti_medium1_xe55_tclcw", &EF_tau29Ti_medium1_xe55_tclcw, &b_EF_tau29Ti_medium1_xe55_tclcw);
    fChain->SetBranchAddress("EF_xe80T", &EF_xe80T, &b_EF_xe80T);
    fChain->SetBranchAddress("EF_xe80_tclcw", &EF_xe80_tclcw, &b_EF_xe80_tclcw);
    fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
    fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
    fChain->SetBranchAddress("timestamp", &timestamp, &b_timestamp);
    fChain->SetBranchAddress("timestamp_ns", &timestamp_ns, &b_timestamp_ns);
    fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
    fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
    fChain->SetBranchAddress("detmask0", &detmask0, &b_detmask0);
    fChain->SetBranchAddress("detmask1", &detmask1, &b_detmask1);
    fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
    fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
    fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
    fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
    fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);
    fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
    fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
    fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
    fChain->SetBranchAddress("fwdFlags", &fwdFlags, &b_fwdFlags);
    fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
    fChain->SetBranchAddress("pixelError", &pixelError, &b_pixelError);
    fChain->SetBranchAddress("sctError", &sctError, &b_sctError);
    fChain->SetBranchAddress("trtError", &trtError, &b_trtError);
    fChain->SetBranchAddress("larError", &larError, &b_larError);
    fChain->SetBranchAddress("tileError", &tileError, &b_tileError);
    fChain->SetBranchAddress("muonError", &muonError, &b_muonError);
    fChain->SetBranchAddress("fwdError", &fwdError, &b_fwdError);
    fChain->SetBranchAddress("coreError", &coreError, &b_coreError);
    fChain->SetBranchAddress("streamDecision_Egamma", &streamDecision_Egamma, &b_streamDecision_Egamma);
    fChain->SetBranchAddress("streamDecision_Muons", &streamDecision_Muons, &b_streamDecision_Muons);
    fChain->SetBranchAddress("streamDecision_JetTauEtmiss", &streamDecision_JetTauEtmiss, &b_streamDecision_JetTauEtmiss);
    fChain->SetBranchAddress("l1id", &l1id, &b_l1id);
    fChain->SetBranchAddress("isSimulation", &isSimulation, &b_isSimulation);
    fChain->SetBranchAddress("isCalibration", &isCalibration, &b_isCalibration);
    fChain->SetBranchAddress("isTestBeam", &isTestBeam, &b_isTestBeam);
    fChain->SetBranchAddress("trig_EF_trigmuonef_n", &trig_EF_trigmuonef_n, &b_trig_EF_trigmuonef_n);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_n", &trig_EF_trigmuonef_track_n, &b_trig_EF_trigmuonef_track_n);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_MuonType", &trig_EF_trigmuonef_track_MuonType,
                             &b_trig_EF_trigmuonef_track_MuonType);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_MS_pt", &trig_EF_trigmuonef_track_MS_pt,
                             &b_trig_EF_trigmuonef_track_MS_pt);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_MS_eta", &trig_EF_trigmuonef_track_MS_eta,
                             &b_trig_EF_trigmuonef_track_MS_eta);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_MS_phi", &trig_EF_trigmuonef_track_MS_phi,
                             &b_trig_EF_trigmuonef_track_MS_phi);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_MS_hasMS", &trig_EF_trigmuonef_track_MS_hasMS,
                             &b_trig_EF_trigmuonef_track_MS_hasMS);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_SA_pt", &trig_EF_trigmuonef_track_SA_pt,
                             &b_trig_EF_trigmuonef_track_SA_pt);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_SA_eta", &trig_EF_trigmuonef_track_SA_eta,
                             &b_trig_EF_trigmuonef_track_SA_eta);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_SA_phi", &trig_EF_trigmuonef_track_SA_phi,
                             &b_trig_EF_trigmuonef_track_SA_phi);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_SA_hasSA", &trig_EF_trigmuonef_track_SA_hasSA,
                             &b_trig_EF_trigmuonef_track_SA_hasSA);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_CB_pt", &trig_EF_trigmuonef_track_CB_pt,
                             &b_trig_EF_trigmuonef_track_CB_pt);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_CB_eta", &trig_EF_trigmuonef_track_CB_eta,
                             &b_trig_EF_trigmuonef_track_CB_eta);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_CB_phi", &trig_EF_trigmuonef_track_CB_phi,
                             &b_trig_EF_trigmuonef_track_CB_phi);
    fChain->SetBranchAddress("trig_EF_trigmuonef_track_CB_hasCB", &trig_EF_trigmuonef_track_CB_hasCB,
                             &b_trig_EF_trigmuonef_track_CB_hasCB);
    fChain->SetBranchAddress("trig_EF_trigmuonefisolation_n", &trig_EF_trigmuonefisolation_n, &b_trig_EF_trigmuonefisolation_n);
    fChain->SetBranchAddress("trig_EF_trigmuonefisolation_sumTrkPtCone02", &trig_EF_trigmuonefisolation_sumTrkPtCone02,
                             &b_trig_EF_trigmuonefisolation_sumTrkPtCone02);
    fChain->SetBranchAddress("trig_EF_trigmuonefisolation_sumTrkPtCone03", &trig_EF_trigmuonefisolation_sumTrkPtCone03,
                             &b_trig_EF_trigmuonefisolation_sumTrkPtCone03);
    fChain->SetBranchAddress("trig_EF_trigmuonefisolation_trackPosition", &trig_EF_trigmuonefisolation_trackPosition,
                             &b_trig_EF_trigmuonefisolation_trackPosition);
    fChain->SetBranchAddress("trig_EF_trigmuonefisolation_efinfo_index", &trig_EF_trigmuonefisolation_efinfo_index,
                             &b_trig_EF_trigmuonefisolation_efinfo_index);
    fChain->SetBranchAddress("el_n", &el_n, &b_el_n);
    fChain->SetBranchAddress("el_E", &el_E, &b_el_E);
    fChain->SetBranchAddress("el_Et", &el_Et, &b_el_Et);
    fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
    fChain->SetBranchAddress("el_m", &el_m, &b_el_m);
    fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
    fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
    fChain->SetBranchAddress("el_px", &el_px, &b_el_px);
    fChain->SetBranchAddress("el_py", &el_py, &b_el_py);
    fChain->SetBranchAddress("el_pz", &el_pz, &b_el_pz);
    fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
    fChain->SetBranchAddress("el_author", &el_author, &b_el_author);
    fChain->SetBranchAddress("el_isEM", &el_isEM, &b_el_isEM);
    fChain->SetBranchAddress("el_isEMLoose", &el_isEMLoose, &b_el_isEMLoose);
    fChain->SetBranchAddress("el_isEMMedium", &el_isEMMedium, &b_el_isEMMedium);
    fChain->SetBranchAddress("el_isEMTight", &el_isEMTight, &b_el_isEMTight);
    fChain->SetBranchAddress("el_OQ", &el_OQ, &b_el_OQ);
    fChain->SetBranchAddress("el_convFlag", &el_convFlag, &b_el_convFlag);
    fChain->SetBranchAddress("el_isConv", &el_isConv, &b_el_isConv);
    fChain->SetBranchAddress("el_nConv", &el_nConv, &b_el_nConv);
    fChain->SetBranchAddress("el_nSingleTrackConv", &el_nSingleTrackConv, &b_el_nSingleTrackConv);
    fChain->SetBranchAddress("el_nDoubleTrackConv", &el_nDoubleTrackConv, &b_el_nDoubleTrackConv);
    fChain->SetBranchAddress("el_mediumWithoutTrack", &el_mediumWithoutTrack, &b_el_mediumWithoutTrack);
    fChain->SetBranchAddress("el_mediumIsoWithoutTrack", &el_mediumIsoWithoutTrack, &b_el_mediumIsoWithoutTrack);
    fChain->SetBranchAddress("el_tightWithoutTrack", &el_tightWithoutTrack, &b_el_tightWithoutTrack);
    fChain->SetBranchAddress("el_tightIsoWithoutTrack", &el_tightIsoWithoutTrack, &b_el_tightIsoWithoutTrack);
    fChain->SetBranchAddress("el_loose", &el_loose, &b_el_loose);
    fChain->SetBranchAddress("el_looseIso", &el_looseIso, &b_el_looseIso);
    fChain->SetBranchAddress("el_medium", &el_medium, &b_el_medium);
    fChain->SetBranchAddress("el_mediumIso", &el_mediumIso, &b_el_mediumIso);
    fChain->SetBranchAddress("el_tight", &el_tight, &b_el_tight);
    fChain->SetBranchAddress("el_tightIso", &el_tightIso, &b_el_tightIso);
    fChain->SetBranchAddress("el_loosePP", &el_loosePP, &b_el_loosePP);
    fChain->SetBranchAddress("el_loosePPIso", &el_loosePPIso, &b_el_loosePPIso);
    fChain->SetBranchAddress("el_mediumPP", &el_mediumPP, &b_el_mediumPP);
    fChain->SetBranchAddress("el_mediumPPIso", &el_mediumPPIso, &b_el_mediumPPIso);
    fChain->SetBranchAddress("el_tightPP", &el_tightPP, &b_el_tightPP);
    fChain->SetBranchAddress("el_tightPPIso", &el_tightPPIso, &b_el_tightPPIso);
    fChain->SetBranchAddress("el_goodOQ", &el_goodOQ, &b_el_goodOQ);
    fChain->SetBranchAddress("el_Ethad", &el_Ethad, &b_el_Ethad);
    fChain->SetBranchAddress("el_Ethad1", &el_Ethad1, &b_el_Ethad1);
    fChain->SetBranchAddress("el_f1", &el_f1, &b_el_f1);
    fChain->SetBranchAddress("el_f1core", &el_f1core, &b_el_f1core);
    fChain->SetBranchAddress("el_Emins1", &el_Emins1, &b_el_Emins1);
    fChain->SetBranchAddress("el_fside", &el_fside, &b_el_fside);
    fChain->SetBranchAddress("el_Emax2", &el_Emax2, &b_el_Emax2);
    fChain->SetBranchAddress("el_ws3", &el_ws3, &b_el_ws3);
    fChain->SetBranchAddress("el_wstot", &el_wstot, &b_el_wstot);
    fChain->SetBranchAddress("el_emaxs1", &el_emaxs1, &b_el_emaxs1);
    fChain->SetBranchAddress("el_deltaEs", &el_deltaEs, &b_el_deltaEs);
    fChain->SetBranchAddress("el_E233", &el_E233, &b_el_E233);
    fChain->SetBranchAddress("el_E237", &el_E237, &b_el_E237);
    fChain->SetBranchAddress("el_E277", &el_E277, &b_el_E277);
    fChain->SetBranchAddress("el_weta2", &el_weta2, &b_el_weta2);
    fChain->SetBranchAddress("el_f3", &el_f3, &b_el_f3);
    fChain->SetBranchAddress("el_f3core", &el_f3core, &b_el_f3core);
    fChain->SetBranchAddress("el_rphiallcalo", &el_rphiallcalo, &b_el_rphiallcalo);
    fChain->SetBranchAddress("el_Etcone45", &el_Etcone45, &b_el_Etcone45);
    fChain->SetBranchAddress("el_Etcone15", &el_Etcone15, &b_el_Etcone15);
    fChain->SetBranchAddress("el_Etcone20", &el_Etcone20, &b_el_Etcone20);
    fChain->SetBranchAddress("el_Etcone25", &el_Etcone25, &b_el_Etcone25);
    fChain->SetBranchAddress("el_Etcone30", &el_Etcone30, &b_el_Etcone30);
    fChain->SetBranchAddress("el_Etcone35", &el_Etcone35, &b_el_Etcone35);
    fChain->SetBranchAddress("el_Etcone40", &el_Etcone40, &b_el_Etcone40);
    fChain->SetBranchAddress("el_ptcone20", &el_ptcone20, &b_el_ptcone20);
    fChain->SetBranchAddress("el_ptcone30", &el_ptcone30, &b_el_ptcone30);
    fChain->SetBranchAddress("el_ptcone40", &el_ptcone40, &b_el_ptcone40);
    fChain->SetBranchAddress("el_nucone20", &el_nucone20, &b_el_nucone20);
    fChain->SetBranchAddress("el_nucone30", &el_nucone30, &b_el_nucone30);
    fChain->SetBranchAddress("el_nucone40", &el_nucone40, &b_el_nucone40);
    fChain->SetBranchAddress("el_Etcone15_pt_corrected", &el_Etcone15_pt_corrected, &b_el_Etcone15_pt_corrected);
    fChain->SetBranchAddress("el_Etcone20_pt_corrected", &el_Etcone20_pt_corrected, &b_el_Etcone20_pt_corrected);
    fChain->SetBranchAddress("el_Etcone25_pt_corrected", &el_Etcone25_pt_corrected, &b_el_Etcone25_pt_corrected);
    fChain->SetBranchAddress("el_Etcone30_pt_corrected", &el_Etcone30_pt_corrected, &b_el_Etcone30_pt_corrected);
    fChain->SetBranchAddress("el_Etcone35_pt_corrected", &el_Etcone35_pt_corrected, &b_el_Etcone35_pt_corrected);
    fChain->SetBranchAddress("el_Etcone40_pt_corrected", &el_Etcone40_pt_corrected, &b_el_Etcone40_pt_corrected);
    fChain->SetBranchAddress("el_convanglematch", &el_convanglematch, &b_el_convanglematch);
    fChain->SetBranchAddress("el_convtrackmatch", &el_convtrackmatch, &b_el_convtrackmatch);
    fChain->SetBranchAddress("el_hasconv", &el_hasconv, &b_el_hasconv);
    fChain->SetBranchAddress("el_convvtxx", &el_convvtxx, &b_el_convvtxx);
    fChain->SetBranchAddress("el_convvtxy", &el_convvtxy, &b_el_convvtxy);
    fChain->SetBranchAddress("el_convvtxz", &el_convvtxz, &b_el_convvtxz);
    fChain->SetBranchAddress("el_Rconv", &el_Rconv, &b_el_Rconv);
    fChain->SetBranchAddress("el_zconv", &el_zconv, &b_el_zconv);
    fChain->SetBranchAddress("el_convvtxchi2", &el_convvtxchi2, &b_el_convvtxchi2);
    fChain->SetBranchAddress("el_pt1conv", &el_pt1conv, &b_el_pt1conv);
    fChain->SetBranchAddress("el_convtrk1nBLHits", &el_convtrk1nBLHits, &b_el_convtrk1nBLHits);
    fChain->SetBranchAddress("el_convtrk1nPixHits", &el_convtrk1nPixHits, &b_el_convtrk1nPixHits);
    fChain->SetBranchAddress("el_convtrk1nSCTHits", &el_convtrk1nSCTHits, &b_el_convtrk1nSCTHits);
    fChain->SetBranchAddress("el_convtrk1nTRTHits", &el_convtrk1nTRTHits, &b_el_convtrk1nTRTHits);
    fChain->SetBranchAddress("el_pt2conv", &el_pt2conv, &b_el_pt2conv);
    fChain->SetBranchAddress("el_convtrk2nBLHits", &el_convtrk2nBLHits, &b_el_convtrk2nBLHits);
    fChain->SetBranchAddress("el_convtrk2nPixHits", &el_convtrk2nPixHits, &b_el_convtrk2nPixHits);
    fChain->SetBranchAddress("el_convtrk2nSCTHits", &el_convtrk2nSCTHits, &b_el_convtrk2nSCTHits);
    fChain->SetBranchAddress("el_convtrk2nTRTHits", &el_convtrk2nTRTHits, &b_el_convtrk2nTRTHits);
    fChain->SetBranchAddress("el_ptconv", &el_ptconv, &b_el_ptconv);
    fChain->SetBranchAddress("el_pzconv", &el_pzconv, &b_el_pzconv);
    fChain->SetBranchAddress("el_pos7", &el_pos7, &b_el_pos7);
    fChain->SetBranchAddress("el_etacorrmag", &el_etacorrmag, &b_el_etacorrmag);
    fChain->SetBranchAddress("el_deltaeta1", &el_deltaeta1, &b_el_deltaeta1);
    fChain->SetBranchAddress("el_deltaeta2", &el_deltaeta2, &b_el_deltaeta2);
    fChain->SetBranchAddress("el_deltaphi2", &el_deltaphi2, &b_el_deltaphi2);
    fChain->SetBranchAddress("el_deltaphiRescaled", &el_deltaphiRescaled, &b_el_deltaphiRescaled);
    fChain->SetBranchAddress("el_deltaPhiFromLast", &el_deltaPhiFromLast, &b_el_deltaPhiFromLast);
    fChain->SetBranchAddress("el_deltaPhiRot", &el_deltaPhiRot, &b_el_deltaPhiRot);
    fChain->SetBranchAddress("el_expectHitInBLayer", &el_expectHitInBLayer, &b_el_expectHitInBLayer);
    fChain->SetBranchAddress("el_trackd0_physics", &el_trackd0_physics, &b_el_trackd0_physics);
    fChain->SetBranchAddress("el_etaSampling1", &el_etaSampling1, &b_el_etaSampling1);
    fChain->SetBranchAddress("el_reta", &el_reta, &b_el_reta);
    fChain->SetBranchAddress("el_rphi", &el_rphi, &b_el_rphi);
    fChain->SetBranchAddress("el_topoEtcone20", &el_topoEtcone20, &b_el_topoEtcone20);
    fChain->SetBranchAddress("el_topoEtcone30", &el_topoEtcone30, &b_el_topoEtcone30);
    fChain->SetBranchAddress("el_topoEtcone40", &el_topoEtcone40, &b_el_topoEtcone40);
    fChain->SetBranchAddress("el_materialTraversed", &el_materialTraversed, &b_el_materialTraversed);
    fChain->SetBranchAddress("el_EtringnoisedR03sig2", &el_EtringnoisedR03sig2, &b_el_EtringnoisedR03sig2);
    fChain->SetBranchAddress("el_EtringnoisedR03sig3", &el_EtringnoisedR03sig3, &b_el_EtringnoisedR03sig3);
    fChain->SetBranchAddress("el_EtringnoisedR03sig4", &el_EtringnoisedR03sig4, &b_el_EtringnoisedR03sig4);
    fChain->SetBranchAddress("el_ptcone20_zpv05", &el_ptcone20_zpv05, &b_el_ptcone20_zpv05);
    fChain->SetBranchAddress("el_ptcone30_zpv05", &el_ptcone30_zpv05, &b_el_ptcone30_zpv05);
    fChain->SetBranchAddress("el_ptcone40_zpv05", &el_ptcone40_zpv05, &b_el_ptcone40_zpv05);
    fChain->SetBranchAddress("el_nucone20_zpv05", &el_nucone20_zpv05, &b_el_nucone20_zpv05);
    fChain->SetBranchAddress("el_nucone30_zpv05", &el_nucone30_zpv05, &b_el_nucone30_zpv05);
    fChain->SetBranchAddress("el_nucone40_zpv05", &el_nucone40_zpv05, &b_el_nucone40_zpv05);
    fChain->SetBranchAddress("el_isolationlikelihoodjets", &el_isolationlikelihoodjets, &b_el_isolationlikelihoodjets);
    fChain->SetBranchAddress("el_isolationlikelihoodhqelectrons", &el_isolationlikelihoodhqelectrons,
                             &b_el_isolationlikelihoodhqelectrons);
    fChain->SetBranchAddress("el_electronweight", &el_electronweight, &b_el_electronweight);
    fChain->SetBranchAddress("el_electronbgweight", &el_electronbgweight, &b_el_electronbgweight);
    fChain->SetBranchAddress("el_softeweight", &el_softeweight, &b_el_softeweight);
    fChain->SetBranchAddress("el_softebgweight", &el_softebgweight, &b_el_softebgweight);
    fChain->SetBranchAddress("el_neuralnet", &el_neuralnet, &b_el_neuralnet);
    fChain->SetBranchAddress("el_Hmatrix", &el_Hmatrix, &b_el_Hmatrix);
    fChain->SetBranchAddress("el_Hmatrix5", &el_Hmatrix5, &b_el_Hmatrix5);
    fChain->SetBranchAddress("el_adaboost", &el_adaboost, &b_el_adaboost);
    fChain->SetBranchAddress("el_softeneuralnet", &el_softeneuralnet, &b_el_softeneuralnet);
    fChain->SetBranchAddress("el_ringernn", &el_ringernn, &b_el_ringernn);
    fChain->SetBranchAddress("el_zvertex", &el_zvertex, &b_el_zvertex);
    fChain->SetBranchAddress("el_errz", &el_errz, &b_el_errz);
    fChain->SetBranchAddress("el_etap", &el_etap, &b_el_etap);
    fChain->SetBranchAddress("el_depth", &el_depth, &b_el_depth);
    fChain->SetBranchAddress("el_Es0", &el_Es0, &b_el_Es0);
    fChain->SetBranchAddress("el_etas0", &el_etas0, &b_el_etas0);
    fChain->SetBranchAddress("el_phis0", &el_phis0, &b_el_phis0);
    fChain->SetBranchAddress("el_Es1", &el_Es1, &b_el_Es1);
    fChain->SetBranchAddress("el_etas1", &el_etas1, &b_el_etas1);
    fChain->SetBranchAddress("el_phis1", &el_phis1, &b_el_phis1);
    fChain->SetBranchAddress("el_Es2", &el_Es2, &b_el_Es2);
    fChain->SetBranchAddress("el_etas2", &el_etas2, &b_el_etas2);
    fChain->SetBranchAddress("el_phis2", &el_phis2, &b_el_phis2);
    fChain->SetBranchAddress("el_Es3", &el_Es3, &b_el_Es3);
    fChain->SetBranchAddress("el_etas3", &el_etas3, &b_el_etas3);
    fChain->SetBranchAddress("el_phis3", &el_phis3, &b_el_phis3);
    fChain->SetBranchAddress("el_E_PreSamplerB", &el_E_PreSamplerB, &b_el_E_PreSamplerB);
    fChain->SetBranchAddress("el_E_EMB1", &el_E_EMB1, &b_el_E_EMB1);
    fChain->SetBranchAddress("el_E_EMB2", &el_E_EMB2, &b_el_E_EMB2);
    fChain->SetBranchAddress("el_E_EMB3", &el_E_EMB3, &b_el_E_EMB3);
    fChain->SetBranchAddress("el_E_PreSamplerE", &el_E_PreSamplerE, &b_el_E_PreSamplerE);
    fChain->SetBranchAddress("el_E_EME1", &el_E_EME1, &b_el_E_EME1);
    fChain->SetBranchAddress("el_E_EME2", &el_E_EME2, &b_el_E_EME2);
    fChain->SetBranchAddress("el_E_EME3", &el_E_EME3, &b_el_E_EME3);
    fChain->SetBranchAddress("el_E_HEC0", &el_E_HEC0, &b_el_E_HEC0);
    fChain->SetBranchAddress("el_E_HEC1", &el_E_HEC1, &b_el_E_HEC1);
    fChain->SetBranchAddress("el_E_HEC2", &el_E_HEC2, &b_el_E_HEC2);
    fChain->SetBranchAddress("el_E_HEC3", &el_E_HEC3, &b_el_E_HEC3);
    fChain->SetBranchAddress("el_E_TileBar0", &el_E_TileBar0, &b_el_E_TileBar0);
    fChain->SetBranchAddress("el_E_TileBar1", &el_E_TileBar1, &b_el_E_TileBar1);
    fChain->SetBranchAddress("el_E_TileBar2", &el_E_TileBar2, &b_el_E_TileBar2);
    fChain->SetBranchAddress("el_E_TileGap1", &el_E_TileGap1, &b_el_E_TileGap1);
    fChain->SetBranchAddress("el_E_TileGap2", &el_E_TileGap2, &b_el_E_TileGap2);
    fChain->SetBranchAddress("el_E_TileGap3", &el_E_TileGap3, &b_el_E_TileGap3);
    fChain->SetBranchAddress("el_E_TileExt0", &el_E_TileExt0, &b_el_E_TileExt0);
    fChain->SetBranchAddress("el_E_TileExt1", &el_E_TileExt1, &b_el_E_TileExt1);
    fChain->SetBranchAddress("el_E_TileExt2", &el_E_TileExt2, &b_el_E_TileExt2);
    fChain->SetBranchAddress("el_E_FCAL0", &el_E_FCAL0, &b_el_E_FCAL0);
    fChain->SetBranchAddress("el_E_FCAL1", &el_E_FCAL1, &b_el_E_FCAL1);
    fChain->SetBranchAddress("el_E_FCAL2", &el_E_FCAL2, &b_el_E_FCAL2);
    fChain->SetBranchAddress("el_cl_E", &el_cl_E, &b_el_cl_E);
    fChain->SetBranchAddress("el_cl_pt", &el_cl_pt, &b_el_cl_pt);
    fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
    fChain->SetBranchAddress("el_cl_phi", &el_cl_phi, &b_el_cl_phi);
    fChain->SetBranchAddress("el_cl_etaCalo", &el_cl_etaCalo, &b_el_cl_etaCalo);
    fChain->SetBranchAddress("el_cl_phiCalo", &el_cl_phiCalo, &b_el_cl_phiCalo);
    fChain->SetBranchAddress("el_firstEdens", &el_firstEdens, &b_el_firstEdens);
    fChain->SetBranchAddress("el_cellmaxfrac", &el_cellmaxfrac, &b_el_cellmaxfrac);
    fChain->SetBranchAddress("el_longitudinal", &el_longitudinal, &b_el_longitudinal);
    fChain->SetBranchAddress("el_secondlambda", &el_secondlambda, &b_el_secondlambda);
    fChain->SetBranchAddress("el_lateral", &el_lateral, &b_el_lateral);
    fChain->SetBranchAddress("el_secondR", &el_secondR, &b_el_secondR);
    fChain->SetBranchAddress("el_centerlambda", &el_centerlambda, &b_el_centerlambda);
    fChain->SetBranchAddress("el_rawcl_Es0", &el_rawcl_Es0, &b_el_rawcl_Es0);
    fChain->SetBranchAddress("el_rawcl_etas0", &el_rawcl_etas0, &b_el_rawcl_etas0);
    fChain->SetBranchAddress("el_rawcl_phis0", &el_rawcl_phis0, &b_el_rawcl_phis0);
    fChain->SetBranchAddress("el_rawcl_Es1", &el_rawcl_Es1, &b_el_rawcl_Es1);
    fChain->SetBranchAddress("el_rawcl_etas1", &el_rawcl_etas1, &b_el_rawcl_etas1);
    fChain->SetBranchAddress("el_rawcl_phis1", &el_rawcl_phis1, &b_el_rawcl_phis1);
    fChain->SetBranchAddress("el_rawcl_Es2", &el_rawcl_Es2, &b_el_rawcl_Es2);
    fChain->SetBranchAddress("el_rawcl_etas2", &el_rawcl_etas2, &b_el_rawcl_etas2);
    fChain->SetBranchAddress("el_rawcl_phis2", &el_rawcl_phis2, &b_el_rawcl_phis2);
    fChain->SetBranchAddress("el_rawcl_Es3", &el_rawcl_Es3, &b_el_rawcl_Es3);
    fChain->SetBranchAddress("el_rawcl_etas3", &el_rawcl_etas3, &b_el_rawcl_etas3);
    fChain->SetBranchAddress("el_rawcl_phis3", &el_rawcl_phis3, &b_el_rawcl_phis3);
    fChain->SetBranchAddress("el_rawcl_E", &el_rawcl_E, &b_el_rawcl_E);
    fChain->SetBranchAddress("el_rawcl_pt", &el_rawcl_pt, &b_el_rawcl_pt);
    fChain->SetBranchAddress("el_rawcl_eta", &el_rawcl_eta, &b_el_rawcl_eta);
    fChain->SetBranchAddress("el_rawcl_phi", &el_rawcl_phi, &b_el_rawcl_phi);
    fChain->SetBranchAddress("el_trackd0", &el_trackd0, &b_el_trackd0);
    fChain->SetBranchAddress("el_trackz0", &el_trackz0, &b_el_trackz0);
    fChain->SetBranchAddress("el_trackphi", &el_trackphi, &b_el_trackphi);
    fChain->SetBranchAddress("el_tracktheta", &el_tracktheta, &b_el_tracktheta);
    fChain->SetBranchAddress("el_trackqoverp", &el_trackqoverp, &b_el_trackqoverp);
    fChain->SetBranchAddress("el_trackpt", &el_trackpt, &b_el_trackpt);
    fChain->SetBranchAddress("el_tracketa", &el_tracketa, &b_el_tracketa);
    fChain->SetBranchAddress("el_trackfitchi2", &el_trackfitchi2, &b_el_trackfitchi2);
    fChain->SetBranchAddress("el_trackfitndof", &el_trackfitndof, &b_el_trackfitndof);
    fChain->SetBranchAddress("el_nBLHits", &el_nBLHits, &b_el_nBLHits);
    fChain->SetBranchAddress("el_nPixHits", &el_nPixHits, &b_el_nPixHits);
    fChain->SetBranchAddress("el_nSCTHits", &el_nSCTHits, &b_el_nSCTHits);
    fChain->SetBranchAddress("el_nTRTHits", &el_nTRTHits, &b_el_nTRTHits);
    fChain->SetBranchAddress("el_nTRTHighTHits", &el_nTRTHighTHits, &b_el_nTRTHighTHits);
    fChain->SetBranchAddress("el_nTRTXenonHits", &el_nTRTXenonHits, &b_el_nTRTXenonHits);
    fChain->SetBranchAddress("el_nPixHoles", &el_nPixHoles, &b_el_nPixHoles);
    fChain->SetBranchAddress("el_nSCTHoles", &el_nSCTHoles, &b_el_nSCTHoles);
    fChain->SetBranchAddress("el_nTRTHoles", &el_nTRTHoles, &b_el_nTRTHoles);
    fChain->SetBranchAddress("el_nPixelDeadSensors", &el_nPixelDeadSensors, &b_el_nPixelDeadSensors);
    fChain->SetBranchAddress("el_nSCTDeadSensors", &el_nSCTDeadSensors, &b_el_nSCTDeadSensors);
    fChain->SetBranchAddress("el_nBLSharedHits", &el_nBLSharedHits, &b_el_nBLSharedHits);
    fChain->SetBranchAddress("el_nPixSharedHits", &el_nPixSharedHits, &b_el_nPixSharedHits);
    fChain->SetBranchAddress("el_nSCTSharedHits", &el_nSCTSharedHits, &b_el_nSCTSharedHits);
    fChain->SetBranchAddress("el_nBLayerSplitHits", &el_nBLayerSplitHits, &b_el_nBLayerSplitHits);
    fChain->SetBranchAddress("el_nPixSplitHits", &el_nPixSplitHits, &b_el_nPixSplitHits);
    fChain->SetBranchAddress("el_nBLayerOutliers", &el_nBLayerOutliers, &b_el_nBLayerOutliers);
    fChain->SetBranchAddress("el_nPixelOutliers", &el_nPixelOutliers, &b_el_nPixelOutliers);
    fChain->SetBranchAddress("el_nSCTOutliers", &el_nSCTOutliers, &b_el_nSCTOutliers);
    fChain->SetBranchAddress("el_nTRTOutliers", &el_nTRTOutliers, &b_el_nTRTOutliers);
    fChain->SetBranchAddress("el_nTRTHighTOutliers", &el_nTRTHighTOutliers, &b_el_nTRTHighTOutliers);
    fChain->SetBranchAddress("el_nContribPixelLayers", &el_nContribPixelLayers, &b_el_nContribPixelLayers);
    fChain->SetBranchAddress("el_nGangedPixels", &el_nGangedPixels, &b_el_nGangedPixels);
    fChain->SetBranchAddress("el_nGangedFlaggedFakes", &el_nGangedFlaggedFakes, &b_el_nGangedFlaggedFakes);
    fChain->SetBranchAddress("el_nPixelSpoiltHits", &el_nPixelSpoiltHits, &b_el_nPixelSpoiltHits);
    fChain->SetBranchAddress("el_nSCTDoubleHoles", &el_nSCTDoubleHoles, &b_el_nSCTDoubleHoles);
    fChain->SetBranchAddress("el_nSCTSpoiltHits", &el_nSCTSpoiltHits, &b_el_nSCTSpoiltHits);
    fChain->SetBranchAddress("el_expectBLayerHit", &el_expectBLayerHit, &b_el_expectBLayerHit);
    fChain->SetBranchAddress("el_nSiHits", &el_nSiHits, &b_el_nSiHits);
    fChain->SetBranchAddress("el_TRTHighTHitsRatio", &el_TRTHighTHitsRatio, &b_el_TRTHighTHitsRatio);
    fChain->SetBranchAddress("el_TRTHighTOutliersRatio", &el_TRTHighTOutliersRatio, &b_el_TRTHighTOutliersRatio);
    fChain->SetBranchAddress("el_pixeldEdx", &el_pixeldEdx, &b_el_pixeldEdx);
    fChain->SetBranchAddress("el_nGoodHitsPixeldEdx", &el_nGoodHitsPixeldEdx, &b_el_nGoodHitsPixeldEdx);
    fChain->SetBranchAddress("el_massPixeldEdx", &el_massPixeldEdx, &b_el_massPixeldEdx);
    fChain->SetBranchAddress("el_likelihoodsPixeldEdx", &el_likelihoodsPixeldEdx, &b_el_likelihoodsPixeldEdx);
    fChain->SetBranchAddress("el_eProbabilityComb", &el_eProbabilityComb, &b_el_eProbabilityComb);
    fChain->SetBranchAddress("el_eProbabilityHT", &el_eProbabilityHT, &b_el_eProbabilityHT);
    fChain->SetBranchAddress("el_eProbabilityToT", &el_eProbabilityToT, &b_el_eProbabilityToT);
    fChain->SetBranchAddress("el_eProbabilityBrem", &el_eProbabilityBrem, &b_el_eProbabilityBrem);
    fChain->SetBranchAddress("el_vertweight", &el_vertweight, &b_el_vertweight);
    fChain->SetBranchAddress("el_vertx", &el_vertx, &b_el_vertx);
    fChain->SetBranchAddress("el_verty", &el_verty, &b_el_verty);
    fChain->SetBranchAddress("el_vertz", &el_vertz, &b_el_vertz);
    fChain->SetBranchAddress("el_trackd0beam", &el_trackd0beam, &b_el_trackd0beam);
    fChain->SetBranchAddress("el_trackz0beam", &el_trackz0beam, &b_el_trackz0beam);
    fChain->SetBranchAddress("el_tracksigd0beam", &el_tracksigd0beam, &b_el_tracksigd0beam);
    fChain->SetBranchAddress("el_tracksigz0beam", &el_tracksigz0beam, &b_el_tracksigz0beam);
    fChain->SetBranchAddress("el_trackd0pv", &el_trackd0pv, &b_el_trackd0pv);
    fChain->SetBranchAddress("el_trackz0pv", &el_trackz0pv, &b_el_trackz0pv);
    fChain->SetBranchAddress("el_tracksigd0pv", &el_tracksigd0pv, &b_el_tracksigd0pv);
    fChain->SetBranchAddress("el_tracksigz0pv", &el_tracksigz0pv, &b_el_tracksigz0pv);
    fChain->SetBranchAddress("el_trackIPEstimate_d0_biasedpvunbiased", &el_trackIPEstimate_d0_biasedpvunbiased,
                             &b_el_trackIPEstimate_d0_biasedpvunbiased);
    fChain->SetBranchAddress("el_trackIPEstimate_z0_biasedpvunbiased", &el_trackIPEstimate_z0_biasedpvunbiased,
                             &b_el_trackIPEstimate_z0_biasedpvunbiased);
    fChain->SetBranchAddress("el_trackIPEstimate_sigd0_biasedpvunbiased", &el_trackIPEstimate_sigd0_biasedpvunbiased,
                             &b_el_trackIPEstimate_sigd0_biasedpvunbiased);
    fChain->SetBranchAddress("el_trackIPEstimate_sigz0_biasedpvunbiased", &el_trackIPEstimate_sigz0_biasedpvunbiased,
                             &b_el_trackIPEstimate_sigz0_biasedpvunbiased);
    fChain->SetBranchAddress("el_trackIPEstimate_d0_unbiasedpvunbiased", &el_trackIPEstimate_d0_unbiasedpvunbiased,
                             &b_el_trackIPEstimate_d0_unbiasedpvunbiased);
    fChain->SetBranchAddress("el_trackIPEstimate_z0_unbiasedpvunbiased", &el_trackIPEstimate_z0_unbiasedpvunbiased,
                             &b_el_trackIPEstimate_z0_unbiasedpvunbiased);
    fChain->SetBranchAddress("el_trackIPEstimate_sigd0_unbiasedpvunbiased", &el_trackIPEstimate_sigd0_unbiasedpvunbiased,
                             &b_el_trackIPEstimate_sigd0_unbiasedpvunbiased);
    fChain->SetBranchAddress("el_trackIPEstimate_sigz0_unbiasedpvunbiased", &el_trackIPEstimate_sigz0_unbiasedpvunbiased,
                             &b_el_trackIPEstimate_sigz0_unbiasedpvunbiased);
    fChain->SetBranchAddress("el_trackd0pvunbiased", &el_trackd0pvunbiased, &b_el_trackd0pvunbiased);
    fChain->SetBranchAddress("el_trackz0pvunbiased", &el_trackz0pvunbiased, &b_el_trackz0pvunbiased);
    fChain->SetBranchAddress("el_tracksigd0pvunbiased", &el_tracksigd0pvunbiased, &b_el_tracksigd0pvunbiased);
    fChain->SetBranchAddress("el_tracksigz0pvunbiased", &el_tracksigz0pvunbiased, &b_el_tracksigz0pvunbiased);
    fChain->SetBranchAddress("el_Unrefittedtrack_d0", &el_Unrefittedtrack_d0, &b_el_Unrefittedtrack_d0);
    fChain->SetBranchAddress("el_Unrefittedtrack_z0", &el_Unrefittedtrack_z0, &b_el_Unrefittedtrack_z0);
    fChain->SetBranchAddress("el_Unrefittedtrack_phi", &el_Unrefittedtrack_phi, &b_el_Unrefittedtrack_phi);
    fChain->SetBranchAddress("el_Unrefittedtrack_theta", &el_Unrefittedtrack_theta, &b_el_Unrefittedtrack_theta);
    fChain->SetBranchAddress("el_Unrefittedtrack_qoverp", &el_Unrefittedtrack_qoverp, &b_el_Unrefittedtrack_qoverp);
    fChain->SetBranchAddress("el_Unrefittedtrack_pt", &el_Unrefittedtrack_pt, &b_el_Unrefittedtrack_pt);
    fChain->SetBranchAddress("el_Unrefittedtrack_eta", &el_Unrefittedtrack_eta, &b_el_Unrefittedtrack_eta);
    fChain->SetBranchAddress("el_theta_LM", &el_theta_LM, &b_el_theta_LM);
    fChain->SetBranchAddress("el_qoverp_LM", &el_qoverp_LM, &b_el_qoverp_LM);
    fChain->SetBranchAddress("el_theta_err_LM", &el_theta_err_LM, &b_el_theta_err_LM);
    fChain->SetBranchAddress("el_qoverp_err_LM", &el_qoverp_err_LM, &b_el_qoverp_err_LM);
    fChain->SetBranchAddress("el_hastrack", &el_hastrack, &b_el_hastrack);
    fChain->SetBranchAddress("el_deltaEmax2", &el_deltaEmax2, &b_el_deltaEmax2);
    fChain->SetBranchAddress("el_calibHitsShowerDepth", &el_calibHitsShowerDepth, &b_el_calibHitsShowerDepth);
    fChain->SetBranchAddress("el_isIso", &el_isIso, &b_el_isIso);
    fChain->SetBranchAddress("el_mvaptcone20", &el_mvaptcone20, &b_el_mvaptcone20);
    fChain->SetBranchAddress("el_mvaptcone30", &el_mvaptcone30, &b_el_mvaptcone30);
    fChain->SetBranchAddress("el_mvaptcone40", &el_mvaptcone40, &b_el_mvaptcone40);
    fChain->SetBranchAddress("el_CaloPointing_eta", &el_CaloPointing_eta, &b_el_CaloPointing_eta);
    fChain->SetBranchAddress("el_CaloPointing_sigma_eta", &el_CaloPointing_sigma_eta, &b_el_CaloPointing_sigma_eta);
    fChain->SetBranchAddress("el_CaloPointing_zvertex", &el_CaloPointing_zvertex, &b_el_CaloPointing_zvertex);
    fChain->SetBranchAddress("el_CaloPointing_sigma_zvertex", &el_CaloPointing_sigma_zvertex, &b_el_CaloPointing_sigma_zvertex);
    fChain->SetBranchAddress("el_HPV_eta", &el_HPV_eta, &b_el_HPV_eta);
    fChain->SetBranchAddress("el_HPV_sigma_eta", &el_HPV_sigma_eta, &b_el_HPV_sigma_eta);
    fChain->SetBranchAddress("el_HPV_zvertex", &el_HPV_zvertex, &b_el_HPV_zvertex);
    fChain->SetBranchAddress("el_HPV_sigma_zvertex", &el_HPV_sigma_zvertex, &b_el_HPV_sigma_zvertex);
    fChain->SetBranchAddress("el_topoEtcone60", &el_topoEtcone60, &b_el_topoEtcone60);
    fChain->SetBranchAddress("el_ES0_real", &el_ES0_real, &b_el_ES0_real);
    fChain->SetBranchAddress("el_ES1_real", &el_ES1_real, &b_el_ES1_real);
    fChain->SetBranchAddress("el_ES2_real", &el_ES2_real, &b_el_ES2_real);
    fChain->SetBranchAddress("el_ES3_real", &el_ES3_real, &b_el_ES3_real);
    fChain->SetBranchAddress("el_EcellS0", &el_EcellS0, &b_el_EcellS0);
    fChain->SetBranchAddress("el_etacellS0", &el_etacellS0, &b_el_etacellS0);
    fChain->SetBranchAddress("el_Etcone40_ED_corrected", &el_Etcone40_ED_corrected, &b_el_Etcone40_ED_corrected);
    fChain->SetBranchAddress("el_Etcone40_corrected", &el_Etcone40_corrected, &b_el_Etcone40_corrected);
    fChain->SetBranchAddress("el_topoEtcone20_corrected", &el_topoEtcone20_corrected, &b_el_topoEtcone20_corrected);
    fChain->SetBranchAddress("el_topoEtcone30_corrected", &el_topoEtcone30_corrected, &b_el_topoEtcone30_corrected);
    fChain->SetBranchAddress("el_topoEtcone40_corrected", &el_topoEtcone40_corrected, &b_el_topoEtcone40_corrected);
    fChain->SetBranchAddress("el_ED_median", &el_ED_median, &b_el_ED_median);
    fChain->SetBranchAddress("el_ED_sigma", &el_ED_sigma, &b_el_ED_sigma);
    fChain->SetBranchAddress("el_ED_Njets", &el_ED_Njets, &b_el_ED_Njets);
    fChain->SetBranchAddress("el_jet_dr", &el_jet_dr, &b_el_jet_dr);
    fChain->SetBranchAddress("el_jet_E", &el_jet_E, &b_el_jet_E);
    fChain->SetBranchAddress("el_jet_pt", &el_jet_pt, &b_el_jet_pt);
    fChain->SetBranchAddress("el_jet_m", &el_jet_m, &b_el_jet_m);
    fChain->SetBranchAddress("el_jet_eta", &el_jet_eta, &b_el_jet_eta);
    fChain->SetBranchAddress("el_jet_phi", &el_jet_phi, &b_el_jet_phi);
    fChain->SetBranchAddress("el_jet_matched", &el_jet_matched, &b_el_jet_matched);
    fChain->SetBranchAddress("tau_n", &tau_n, &b_tau_n);
    fChain->SetBranchAddress("tau_Et", &tau_Et, &b_tau_Et);
    fChain->SetBranchAddress("tau_pt", &tau_pt, &b_tau_pt);
    fChain->SetBranchAddress("tau_m", &tau_m, &b_tau_m);
    fChain->SetBranchAddress("tau_eta", &tau_eta, &b_tau_eta);
    fChain->SetBranchAddress("tau_phi", &tau_phi, &b_tau_phi);
    fChain->SetBranchAddress("tau_charge", &tau_charge, &b_tau_charge);
    fChain->SetBranchAddress("tau_BDTEleScore", &tau_BDTEleScore, &b_tau_BDTEleScore);
    fChain->SetBranchAddress("tau_BDTJetScore", &tau_BDTJetScore, &b_tau_BDTJetScore);
    fChain->SetBranchAddress("tau_likelihood", &tau_likelihood, &b_tau_likelihood);
    fChain->SetBranchAddress("tau_SafeLikelihood", &tau_SafeLikelihood, &b_tau_SafeLikelihood);
    fChain->SetBranchAddress("tau_electronVetoLoose", &tau_electronVetoLoose, &b_tau_electronVetoLoose);
    fChain->SetBranchAddress("tau_electronVetoMedium", &tau_electronVetoMedium, &b_tau_electronVetoMedium);
    fChain->SetBranchAddress("tau_electronVetoTight", &tau_electronVetoTight, &b_tau_electronVetoTight);
    fChain->SetBranchAddress("tau_muonVeto", &tau_muonVeto, &b_tau_muonVeto);
    fChain->SetBranchAddress("tau_tauLlhLoose", &tau_tauLlhLoose, &b_tau_tauLlhLoose);
    fChain->SetBranchAddress("tau_tauLlhMedium", &tau_tauLlhMedium, &b_tau_tauLlhMedium);
    fChain->SetBranchAddress("tau_tauLlhTight", &tau_tauLlhTight, &b_tau_tauLlhTight);
    fChain->SetBranchAddress("tau_JetBDTSigLoose", &tau_JetBDTSigLoose, &b_tau_JetBDTSigLoose);
    fChain->SetBranchAddress("tau_JetBDTSigMedium", &tau_JetBDTSigMedium, &b_tau_JetBDTSigMedium);
    fChain->SetBranchAddress("tau_JetBDTSigTight", &tau_JetBDTSigTight, &b_tau_JetBDTSigTight);
    fChain->SetBranchAddress("tau_EleBDTLoose", &tau_EleBDTLoose, &b_tau_EleBDTLoose);
    fChain->SetBranchAddress("tau_EleBDTMedium", &tau_EleBDTMedium, &b_tau_EleBDTMedium);
    fChain->SetBranchAddress("tau_EleBDTTight", &tau_EleBDTTight, &b_tau_EleBDTTight);
    fChain->SetBranchAddress("tau_author", &tau_author, &b_tau_author);
    fChain->SetBranchAddress("tau_RoIWord", &tau_RoIWord, &b_tau_RoIWord);
    fChain->SetBranchAddress("tau_nProng", &tau_nProng, &b_tau_nProng);
    fChain->SetBranchAddress("tau_numTrack", &tau_numTrack, &b_tau_numTrack);
    fChain->SetBranchAddress("tau_seedCalo_numTrack", &tau_seedCalo_numTrack, &b_tau_seedCalo_numTrack);
    fChain->SetBranchAddress("tau_seedCalo_nWideTrk", &tau_seedCalo_nWideTrk, &b_tau_seedCalo_nWideTrk);
    fChain->SetBranchAddress("tau_nOtherTrk", &tau_nOtherTrk, &b_tau_nOtherTrk);
    fChain->SetBranchAddress("tau_track_atTJVA_n", &tau_track_atTJVA_n, &b_tau_track_atTJVA_n);
    fChain->SetBranchAddress("tau_seedCalo_wideTrk_atTJVA_n", &tau_seedCalo_wideTrk_atTJVA_n, &b_tau_seedCalo_wideTrk_atTJVA_n);
    fChain->SetBranchAddress("tau_otherTrk_atTJVA_n", &tau_otherTrk_atTJVA_n, &b_tau_otherTrk_atTJVA_n);
    fChain->SetBranchAddress("tau_track_n", &tau_track_n, &b_tau_track_n);
    fChain->SetBranchAddress("tau_seedCalo_track_n", &tau_seedCalo_track_n, &b_tau_seedCalo_track_n);
    fChain->SetBranchAddress("tau_seedCalo_wideTrk_n", &tau_seedCalo_wideTrk_n, &b_tau_seedCalo_wideTrk_n);
    fChain->SetBranchAddress("tau_otherTrk_n", &tau_otherTrk_n, &b_tau_otherTrk_n);
    fChain->SetBranchAddress("jet_akt4topoLC_n", &jet_akt4topoLC_n, &b_jet_akt4topoLC_n);
    fChain->SetBranchAddress("jet_akt4topoLC_E", &jet_akt4topoLC_E, &b_jet_akt4topoLC_E);
    fChain->SetBranchAddress("jet_akt4topoLC_pt", &jet_akt4topoLC_pt, &b_jet_akt4topoLC_pt);
    fChain->SetBranchAddress("jet_akt4topoLC_m", &jet_akt4topoLC_m, &b_jet_akt4topoLC_m);
    fChain->SetBranchAddress("jet_akt4topoLC_eta", &jet_akt4topoLC_eta, &b_jet_akt4topoLC_eta);
    fChain->SetBranchAddress("jet_akt4topoLC_phi", &jet_akt4topoLC_phi, &b_jet_akt4topoLC_phi);
    fChain->SetBranchAddress("jet_akt4topoLC_WIDTH", &jet_akt4topoLC_WIDTH, &b_jet_akt4topoLC_WIDTH);
    fChain->SetBranchAddress("jet_akt4topoLC_n90", &jet_akt4topoLC_n90, &b_jet_akt4topoLC_n90);
    fChain->SetBranchAddress("jet_akt4topoLC_Timing", &jet_akt4topoLC_Timing, &b_jet_akt4topoLC_Timing);
    fChain->SetBranchAddress("jet_akt4topoLC_LArQuality", &jet_akt4topoLC_LArQuality, &b_jet_akt4topoLC_LArQuality);
    fChain->SetBranchAddress("jet_akt4topoLC_nTrk", &jet_akt4topoLC_nTrk, &b_jet_akt4topoLC_nTrk);
    fChain->SetBranchAddress("jet_akt4topoLC_sumPtTrk", &jet_akt4topoLC_sumPtTrk, &b_jet_akt4topoLC_sumPtTrk);
    fChain->SetBranchAddress("jet_akt4topoLC_OriginIndex", &jet_akt4topoLC_OriginIndex, &b_jet_akt4topoLC_OriginIndex);
    fChain->SetBranchAddress("jet_akt4topoLC_HECQuality", &jet_akt4topoLC_HECQuality, &b_jet_akt4topoLC_HECQuality);
    fChain->SetBranchAddress("jet_akt4topoLC_NegativeE", &jet_akt4topoLC_NegativeE, &b_jet_akt4topoLC_NegativeE);
    fChain->SetBranchAddress("jet_akt4topoLC_AverageLArQF", &jet_akt4topoLC_AverageLArQF, &b_jet_akt4topoLC_AverageLArQF);
    fChain->SetBranchAddress("jet_akt4topoLC_BCH_CORR_CELL", &jet_akt4topoLC_BCH_CORR_CELL, &b_jet_akt4topoLC_BCH_CORR_CELL);
    fChain->SetBranchAddress("jet_akt4topoLC_BCH_CORR_DOTX", &jet_akt4topoLC_BCH_CORR_DOTX, &b_jet_akt4topoLC_BCH_CORR_DOTX);
    fChain->SetBranchAddress("jet_akt4topoLC_BCH_CORR_JET", &jet_akt4topoLC_BCH_CORR_JET, &b_jet_akt4topoLC_BCH_CORR_JET);
    fChain->SetBranchAddress("jet_akt4topoLC_BCH_CORR_JET_FORCELL", &jet_akt4topoLC_BCH_CORR_JET_FORCELL,
                             &b_jet_akt4topoLC_BCH_CORR_JET_FORCELL);
    fChain->SetBranchAddress("jet_akt4topoLC_ENG_BAD_CELLS", &jet_akt4topoLC_ENG_BAD_CELLS, &b_jet_akt4topoLC_ENG_BAD_CELLS);
    fChain->SetBranchAddress("jet_akt4topoLC_N_BAD_CELLS", &jet_akt4topoLC_N_BAD_CELLS, &b_jet_akt4topoLC_N_BAD_CELLS);
    fChain->SetBranchAddress("jet_akt4topoLC_N_BAD_CELLS_CORR", &jet_akt4topoLC_N_BAD_CELLS_CORR,
                             &b_jet_akt4topoLC_N_BAD_CELLS_CORR);
    fChain->SetBranchAddress("jet_akt4topoLC_BAD_CELLS_CORR_E", &jet_akt4topoLC_BAD_CELLS_CORR_E,
                             &b_jet_akt4topoLC_BAD_CELLS_CORR_E);
    fChain->SetBranchAddress("jet_akt4topoLC_NumTowers", &jet_akt4topoLC_NumTowers, &b_jet_akt4topoLC_NumTowers);
    fChain->SetBranchAddress("jet_akt4topoLC_ootFracCells5", &jet_akt4topoLC_ootFracCells5, &b_jet_akt4topoLC_ootFracCells5);
    fChain->SetBranchAddress("jet_akt4topoLC_ootFracCells10", &jet_akt4topoLC_ootFracCells10, &b_jet_akt4topoLC_ootFracCells10);
    fChain->SetBranchAddress("jet_akt4topoLC_ootFracClusters5", &jet_akt4topoLC_ootFracClusters5,
                             &b_jet_akt4topoLC_ootFracClusters5);
    fChain->SetBranchAddress("jet_akt4topoLC_ootFracClusters10", &jet_akt4topoLC_ootFracClusters10,
                             &b_jet_akt4topoLC_ootFracClusters10);
    fChain->SetBranchAddress("jet_akt4topoLC_emfrac", &jet_akt4topoLC_emfrac, &b_jet_akt4topoLC_emfrac);
    fChain->SetBranchAddress("jet_akt4topoLC_LCJES", &jet_akt4topoLC_LCJES, &b_jet_akt4topoLC_LCJES);
    fChain->SetBranchAddress("jet_akt4topoLC_LCJES_EtaCorr", &jet_akt4topoLC_LCJES_EtaCorr, &b_jet_akt4topoLC_LCJES_EtaCorr);
    fChain->SetBranchAddress("jet_akt4topoLC_emscale_E", &jet_akt4topoLC_emscale_E, &b_jet_akt4topoLC_emscale_E);
    fChain->SetBranchAddress("jet_akt4topoLC_emscale_pt", &jet_akt4topoLC_emscale_pt, &b_jet_akt4topoLC_emscale_pt);
    fChain->SetBranchAddress("jet_akt4topoLC_emscale_m", &jet_akt4topoLC_emscale_m, &b_jet_akt4topoLC_emscale_m);
    fChain->SetBranchAddress("jet_akt4topoLC_emscale_eta", &jet_akt4topoLC_emscale_eta, &b_jet_akt4topoLC_emscale_eta);
    fChain->SetBranchAddress("jet_akt4topoLC_emscale_phi", &jet_akt4topoLC_emscale_phi, &b_jet_akt4topoLC_emscale_phi);
    fChain->SetBranchAddress("jet_akt4topoLC_jvtx_x", &jet_akt4topoLC_jvtx_x, &b_jet_akt4topoLC_jvtx_x);
    fChain->SetBranchAddress("jet_akt4topoLC_jvtx_y", &jet_akt4topoLC_jvtx_y, &b_jet_akt4topoLC_jvtx_y);
    fChain->SetBranchAddress("jet_akt4topoLC_jvtx_z", &jet_akt4topoLC_jvtx_z, &b_jet_akt4topoLC_jvtx_z);
    fChain->SetBranchAddress("jet_akt4topoLC_Nconst", &jet_akt4topoLC_Nconst, &b_jet_akt4topoLC_Nconst);
    fChain->SetBranchAddress("jet_akt4topoLC_ptconst_default", &jet_akt4topoLC_ptconst_default,
                             &b_jet_akt4topoLC_ptconst_default);
    fChain->SetBranchAddress("jet_akt4topoLC_econst_default", &jet_akt4topoLC_econst_default, &b_jet_akt4topoLC_econst_default);
    fChain->SetBranchAddress("jet_akt4topoLC_etaconst_default", &jet_akt4topoLC_etaconst_default,
                             &b_jet_akt4topoLC_etaconst_default);
    fChain->SetBranchAddress("jet_akt4topoLC_phiconst_default", &jet_akt4topoLC_phiconst_default,
                             &b_jet_akt4topoLC_phiconst_default);
    fChain->SetBranchAddress("jet_akt4topoLC_weightconst_default", &jet_akt4topoLC_weightconst_default,
                             &b_jet_akt4topoLC_weightconst_default);
    fChain->SetBranchAddress("jet_akt4topoLC_constscale_E", &jet_akt4topoLC_constscale_E, &b_jet_akt4topoLC_constscale_E);
    fChain->SetBranchAddress("jet_akt4topoLC_constscale_pt", &jet_akt4topoLC_constscale_pt, &b_jet_akt4topoLC_constscale_pt);
    fChain->SetBranchAddress("jet_akt4topoLC_constscale_m", &jet_akt4topoLC_constscale_m, &b_jet_akt4topoLC_constscale_m);
    fChain->SetBranchAddress("jet_akt4topoLC_constscale_eta", &jet_akt4topoLC_constscale_eta, &b_jet_akt4topoLC_constscale_eta);
    fChain->SetBranchAddress("jet_akt4topoLC_constscale_phi", &jet_akt4topoLC_constscale_phi, &b_jet_akt4topoLC_constscale_phi);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_Comb", &jet_akt4topoLC_flavor_weight_Comb,
                             &b_jet_akt4topoLC_flavor_weight_Comb);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_GbbNN", &jet_akt4topoLC_flavor_weight_GbbNN,
                             &b_jet_akt4topoLC_flavor_weight_GbbNN);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_IP2D", &jet_akt4topoLC_flavor_weight_IP2D,
                             &b_jet_akt4topoLC_flavor_weight_IP2D);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_IP3D", &jet_akt4topoLC_flavor_weight_IP3D,
                             &b_jet_akt4topoLC_flavor_weight_IP3D);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_JetFitterCOMBNN", &jet_akt4topoLC_flavor_weight_JetFitterCOMBNN,
                             &b_jet_akt4topoLC_flavor_weight_JetFitterCOMBNN);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_JetFitterTagNN", &jet_akt4topoLC_flavor_weight_JetFitterTagNN,
                             &b_jet_akt4topoLC_flavor_weight_JetFitterTagNN);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_MV1", &jet_akt4topoLC_flavor_weight_MV1,
                             &b_jet_akt4topoLC_flavor_weight_MV1);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_MV2", &jet_akt4topoLC_flavor_weight_MV2,
                             &b_jet_akt4topoLC_flavor_weight_MV2);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_SV0", &jet_akt4topoLC_flavor_weight_SV0,
                             &b_jet_akt4topoLC_flavor_weight_SV0);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_SV1", &jet_akt4topoLC_flavor_weight_SV1,
                             &b_jet_akt4topoLC_flavor_weight_SV1);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_SV2", &jet_akt4topoLC_flavor_weight_SV2,
                             &b_jet_akt4topoLC_flavor_weight_SV2);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_SecondSoftMuonTagChi2",
                             &jet_akt4topoLC_flavor_weight_SecondSoftMuonTagChi2,
                             &b_jet_akt4topoLC_flavor_weight_SecondSoftMuonTagChi2);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_weight_SoftMuonTagChi2", &jet_akt4topoLC_flavor_weight_SoftMuonTagChi2,
                             &b_jet_akt4topoLC_flavor_weight_SoftMuonTagChi2);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_assoctrk_n", &jet_akt4topoLC_flavor_assoctrk_n,
                             &b_jet_akt4topoLC_flavor_assoctrk_n);
    fChain->SetBranchAddress("jet_akt4topoLC_flavor_assoctrk_index", &jet_akt4topoLC_flavor_assoctrk_index,
                             &b_jet_akt4topoLC_flavor_assoctrk_index);
    fChain->SetBranchAddress("jet_akt4topoLC_el_dr", &jet_akt4topoLC_el_dr, &b_jet_akt4topoLC_el_dr);
    fChain->SetBranchAddress("jet_akt4topoLC_el_matched", &jet_akt4topoLC_el_matched, &b_jet_akt4topoLC_el_matched);
    fChain->SetBranchAddress("jet_akt4topoLC_mu_dr", &jet_akt4topoLC_mu_dr, &b_jet_akt4topoLC_mu_dr);
    fChain->SetBranchAddress("jet_akt4topoLC_mu_matched", &jet_akt4topoLC_mu_matched, &b_jet_akt4topoLC_mu_matched);
    fChain->SetBranchAddress("jet_akt4topoLC_L1_dr", &jet_akt4topoLC_L1_dr, &b_jet_akt4topoLC_L1_dr);
    fChain->SetBranchAddress("jet_akt4topoLC_L1_matched", &jet_akt4topoLC_L1_matched, &b_jet_akt4topoLC_L1_matched);
    fChain->SetBranchAddress("jet_akt4topoLC_L2_dr", &jet_akt4topoLC_L2_dr, &b_jet_akt4topoLC_L2_dr);
    fChain->SetBranchAddress("jet_akt4topoLC_L2_matched", &jet_akt4topoLC_L2_matched, &b_jet_akt4topoLC_L2_matched);
    fChain->SetBranchAddress("jet_akt4topoLC_EF_dr", &jet_akt4topoLC_EF_dr, &b_jet_akt4topoLC_EF_dr);
    fChain->SetBranchAddress("jet_akt4topoLC_EF_matched", &jet_akt4topoLC_EF_matched, &b_jet_akt4topoLC_EF_matched);
    fChain->SetBranchAddress("jet_akt4topoLC_nTrk_pv0_1GeV", &jet_akt4topoLC_nTrk_pv0_1GeV, &b_jet_akt4topoLC_nTrk_pv0_1GeV);
    fChain->SetBranchAddress("jet_akt4topoLC_sumPtTrk_pv0_1GeV", &jet_akt4topoLC_sumPtTrk_pv0_1GeV,
                             &b_jet_akt4topoLC_sumPtTrk_pv0_1GeV);
    fChain->SetBranchAddress("jet_akt4topoLC_nTrk_allpv_1GeV", &jet_akt4topoLC_nTrk_allpv_1GeV,
                             &b_jet_akt4topoLC_nTrk_allpv_1GeV);
    fChain->SetBranchAddress("jet_akt4topoLC_sumPtTrk_allpv_1GeV", &jet_akt4topoLC_sumPtTrk_allpv_1GeV,
                             &b_jet_akt4topoLC_sumPtTrk_allpv_1GeV);
    fChain->SetBranchAddress("jet_akt4topoLC_nTrk_pv0_500MeV", &jet_akt4topoLC_nTrk_pv0_500MeV,
                             &b_jet_akt4topoLC_nTrk_pv0_500MeV);
    fChain->SetBranchAddress("jet_akt4topoLC_sumPtTrk_pv0_500MeV", &jet_akt4topoLC_sumPtTrk_pv0_500MeV,
                             &b_jet_akt4topoLC_sumPtTrk_pv0_500MeV);
    fChain->SetBranchAddress("jet_akt4topoLC_trackWIDTH_pv0_1GeV", &jet_akt4topoLC_trackWIDTH_pv0_1GeV,
                             &b_jet_akt4topoLC_trackWIDTH_pv0_1GeV);
    fChain->SetBranchAddress("jet_akt4topoLC_trackWIDTH_allpv_1GeV", &jet_akt4topoLC_trackWIDTH_allpv_1GeV,
                             &b_jet_akt4topoLC_trackWIDTH_allpv_1GeV);
    fChain->SetBranchAddress("MET_Track_etx", &MET_Track_etx, &b_MET_Track_etx);
    fChain->SetBranchAddress("MET_Track_ety", &MET_Track_ety, &b_MET_Track_ety);
    fChain->SetBranchAddress("MET_Track_phi", &MET_Track_phi, &b_MET_Track_phi);
    fChain->SetBranchAddress("MET_Track_et", &MET_Track_et, &b_MET_Track_et);
    fChain->SetBranchAddress("MET_Track_sumet", &MET_Track_sumet, &b_MET_Track_sumet);
    fChain->SetBranchAddress("MET_TauMuGamma_RefFinal_etx", &MET_TauMuGamma_RefFinal_etx, &b_MET_TauMuGamma_RefFinal_etx);
    fChain->SetBranchAddress("MET_TauMuGamma_RefFinal_ety", &MET_TauMuGamma_RefFinal_ety, &b_MET_TauMuGamma_RefFinal_ety);
    fChain->SetBranchAddress("MET_TauMuGamma_RefFinal_phi", &MET_TauMuGamma_RefFinal_phi, &b_MET_TauMuGamma_RefFinal_phi);
    fChain->SetBranchAddress("MET_TauMuGamma_RefFinal_et", &MET_TauMuGamma_RefFinal_et, &b_MET_TauMuGamma_RefFinal_et);
    fChain->SetBranchAddress("MET_TauMuGamma_RefFinal_sumet", &MET_TauMuGamma_RefFinal_sumet, &b_MET_TauMuGamma_RefFinal_sumet);
    fChain->SetBranchAddress("MET_Default_RefFinal_etx", &MET_Default_RefFinal_etx, &b_MET_Default_RefFinal_etx);
    fChain->SetBranchAddress("MET_Default_RefFinal_ety", &MET_Default_RefFinal_ety, &b_MET_Default_RefFinal_ety);
    fChain->SetBranchAddress("MET_Default_RefFinal_phi", &MET_Default_RefFinal_phi, &b_MET_Default_RefFinal_phi);
    fChain->SetBranchAddress("MET_Default_RefFinal_et", &MET_Default_RefFinal_et, &b_MET_Default_RefFinal_et);
    fChain->SetBranchAddress("MET_Default_RefFinal_sumet", &MET_Default_RefFinal_sumet, &b_MET_Default_RefFinal_sumet);
    fChain->SetBranchAddress("el_MET_TauMuGamma_comp_n", &el_MET_TauMuGamma_comp_n, &b_el_MET_TauMuGamma_comp_n);
    fChain->SetBranchAddress("el_MET_TauMuGamma_comp_wpx", &el_MET_TauMuGamma_comp_wpx, &b_el_MET_TauMuGamma_comp_wpx);
    fChain->SetBranchAddress("el_MET_TauMuGamma_comp_wpy", &el_MET_TauMuGamma_comp_wpy, &b_el_MET_TauMuGamma_comp_wpy);
    fChain->SetBranchAddress("el_MET_TauMuGamma_comp_wet", &el_MET_TauMuGamma_comp_wet, &b_el_MET_TauMuGamma_comp_wet);
    fChain->SetBranchAddress("el_MET_TauMuGamma_comp_statusWord", &el_MET_TauMuGamma_comp_statusWord,
                             &b_el_MET_TauMuGamma_comp_statusWord);
    fChain->SetBranchAddress("ph_MET_TauMuGamma_comp_n", &ph_MET_TauMuGamma_comp_n, &b_ph_MET_TauMuGamma_comp_n);
    fChain->SetBranchAddress("ph_MET_TauMuGamma_comp_wpx", &ph_MET_TauMuGamma_comp_wpx, &b_ph_MET_TauMuGamma_comp_wpx);
    fChain->SetBranchAddress("ph_MET_TauMuGamma_comp_wpy", &ph_MET_TauMuGamma_comp_wpy, &b_ph_MET_TauMuGamma_comp_wpy);
    fChain->SetBranchAddress("ph_MET_TauMuGamma_comp_wet", &ph_MET_TauMuGamma_comp_wet, &b_ph_MET_TauMuGamma_comp_wet);
    fChain->SetBranchAddress("ph_MET_TauMuGamma_comp_statusWord", &ph_MET_TauMuGamma_comp_statusWord,
                             &b_ph_MET_TauMuGamma_comp_statusWord);
    fChain->SetBranchAddress("mu_staco_MET_TauMuGamma_comp_n", &mu_staco_MET_TauMuGamma_comp_n,
                             &b_mu_staco_MET_TauMuGamma_comp_n);
    fChain->SetBranchAddress("mu_staco_MET_TauMuGamma_comp_wpx", &mu_staco_MET_TauMuGamma_comp_wpx,
                             &b_mu_staco_MET_TauMuGamma_comp_wpx);
    fChain->SetBranchAddress("mu_staco_MET_TauMuGamma_comp_wpy", &mu_staco_MET_TauMuGamma_comp_wpy,
                             &b_mu_staco_MET_TauMuGamma_comp_wpy);
    fChain->SetBranchAddress("mu_staco_MET_TauMuGamma_comp_wet", &mu_staco_MET_TauMuGamma_comp_wet,
                             &b_mu_staco_MET_TauMuGamma_comp_wet);
    fChain->SetBranchAddress("mu_staco_MET_TauMuGamma_comp_statusWord", &mu_staco_MET_TauMuGamma_comp_statusWord,
                             &b_mu_staco_MET_TauMuGamma_comp_statusWord);
    fChain->SetBranchAddress("mu_muid_MET_TauMuGamma_comp_n", &mu_muid_MET_TauMuGamma_comp_n, &b_mu_muid_MET_TauMuGamma_comp_n);
    fChain->SetBranchAddress("mu_muid_MET_TauMuGamma_comp_wpx", &mu_muid_MET_TauMuGamma_comp_wpx,
                             &b_mu_muid_MET_TauMuGamma_comp_wpx);
    fChain->SetBranchAddress("mu_muid_MET_TauMuGamma_comp_wpy", &mu_muid_MET_TauMuGamma_comp_wpy,
                             &b_mu_muid_MET_TauMuGamma_comp_wpy);
    fChain->SetBranchAddress("mu_muid_MET_TauMuGamma_comp_wet", &mu_muid_MET_TauMuGamma_comp_wet,
                             &b_mu_muid_MET_TauMuGamma_comp_wet);
    fChain->SetBranchAddress("mu_muid_MET_TauMuGamma_comp_statusWord", &mu_muid_MET_TauMuGamma_comp_statusWord,
                             &b_mu_muid_MET_TauMuGamma_comp_statusWord);
    fChain->SetBranchAddress("mu_muon_MET_TauMuGamma_comp_n", &mu_muon_MET_TauMuGamma_comp_n, &b_mu_muon_MET_TauMuGamma_comp_n);
    fChain->SetBranchAddress("mu_muon_MET_TauMuGamma_comp_wpx", &mu_muon_MET_TauMuGamma_comp_wpx,
                             &b_mu_muon_MET_TauMuGamma_comp_wpx);
    fChain->SetBranchAddress("mu_muon_MET_TauMuGamma_comp_wpy", &mu_muon_MET_TauMuGamma_comp_wpy,
                             &b_mu_muon_MET_TauMuGamma_comp_wpy);
    fChain->SetBranchAddress("mu_muon_MET_TauMuGamma_comp_wet", &mu_muon_MET_TauMuGamma_comp_wet,
                             &b_mu_muon_MET_TauMuGamma_comp_wet);
    fChain->SetBranchAddress("mu_muon_MET_TauMuGamma_comp_statusWord", &mu_muon_MET_TauMuGamma_comp_statusWord,
                             &b_mu_muon_MET_TauMuGamma_comp_statusWord);
    fChain->SetBranchAddress("tau_MET_TauMuGamma_comp_n", &tau_MET_TauMuGamma_comp_n, &b_tau_MET_TauMuGamma_comp_n);
    fChain->SetBranchAddress("tau_MET_TauMuGamma_comp_wpx", &tau_MET_TauMuGamma_comp_wpx, &b_tau_MET_TauMuGamma_comp_wpx);
    fChain->SetBranchAddress("tau_MET_TauMuGamma_comp_wpy", &tau_MET_TauMuGamma_comp_wpy, &b_tau_MET_TauMuGamma_comp_wpy);
    fChain->SetBranchAddress("tau_MET_TauMuGamma_comp_wet", &tau_MET_TauMuGamma_comp_wet, &b_tau_MET_TauMuGamma_comp_wet);
    fChain->SetBranchAddress("tau_MET_TauMuGamma_comp_statusWord", &tau_MET_TauMuGamma_comp_statusWord,
                             &b_tau_MET_TauMuGamma_comp_statusWord);
    fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_TauMuGamma_comp_n", &jet_AntiKt4LCTopo_MET_TauMuGamma_comp_n,
                             &b_jet_AntiKt4LCTopo_MET_TauMuGamma_comp_n);
    fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wpx", &jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wpx,
                             &b_jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wpx);
    fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wpy", &jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wpy,
                             &b_jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wpy);
    fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wet", &jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wet,
                             &b_jet_AntiKt4LCTopo_MET_TauMuGamma_comp_wet);
    fChain->SetBranchAddress("jet_AntiKt4LCTopo_MET_TauMuGamma_comp_statusWord",
                             &jet_AntiKt4LCTopo_MET_TauMuGamma_comp_statusWord,
                             &b_jet_AntiKt4LCTopo_MET_TauMuGamma_comp_statusWord);
    fChain->SetBranchAddress("cl_MET_TauMuGamma_comp_n", &cl_MET_TauMuGamma_comp_n, &b_cl_MET_TauMuGamma_comp_n);
    fChain->SetBranchAddress("cl_MET_TauMuGamma_comp_wpx", &cl_MET_TauMuGamma_comp_wpx, &b_cl_MET_TauMuGamma_comp_wpx);
    fChain->SetBranchAddress("cl_MET_TauMuGamma_comp_wpy", &cl_MET_TauMuGamma_comp_wpy, &b_cl_MET_TauMuGamma_comp_wpy);
    fChain->SetBranchAddress("cl_MET_TauMuGamma_comp_wet", &cl_MET_TauMuGamma_comp_wet, &b_cl_MET_TauMuGamma_comp_wet);
    fChain->SetBranchAddress("cl_MET_TauMuGamma_comp_statusWord", &cl_MET_TauMuGamma_comp_statusWord,
                             &b_cl_MET_TauMuGamma_comp_statusWord);
    fChain->SetBranchAddress("trk_MET_TauMuGamma_comp_n", &trk_MET_TauMuGamma_comp_n, &b_trk_MET_TauMuGamma_comp_n);
    fChain->SetBranchAddress("trk_MET_TauMuGamma_comp_wpx", &trk_MET_TauMuGamma_comp_wpx, &b_trk_MET_TauMuGamma_comp_wpx);
    fChain->SetBranchAddress("trk_MET_TauMuGamma_comp_wpy", &trk_MET_TauMuGamma_comp_wpy, &b_trk_MET_TauMuGamma_comp_wpy);
    fChain->SetBranchAddress("trk_MET_TauMuGamma_comp_wet", &trk_MET_TauMuGamma_comp_wet, &b_trk_MET_TauMuGamma_comp_wet);
    fChain->SetBranchAddress("trk_MET_TauMuGamma_comp_statusWord", &trk_MET_TauMuGamma_comp_statusWord,
                             &b_trk_MET_TauMuGamma_comp_statusWord);
    fChain->SetBranchAddress("ph_n", &ph_n, &b_ph_n);
    fChain->SetBranchAddress("ph_E", &ph_E, &b_ph_E);
    fChain->SetBranchAddress("ph_Et", &ph_Et, &b_ph_Et);
    fChain->SetBranchAddress("ph_pt", &ph_pt, &b_ph_pt);
    fChain->SetBranchAddress("ph_m", &ph_m, &b_ph_m);
    fChain->SetBranchAddress("ph_eta", &ph_eta, &b_ph_eta);
    fChain->SetBranchAddress("ph_phi", &ph_phi, &b_ph_phi);
    fChain->SetBranchAddress("ph_px", &ph_px, &b_ph_px);
    fChain->SetBranchAddress("ph_py", &ph_py, &b_ph_py);
    fChain->SetBranchAddress("ph_pz", &ph_pz, &b_ph_pz);
    fChain->SetBranchAddress("ph_author", &ph_author, &b_ph_author);
    fChain->SetBranchAddress("ph_isRecovered", &ph_isRecovered, &b_ph_isRecovered);
    fChain->SetBranchAddress("ph_isEM", &ph_isEM, &b_ph_isEM);
    fChain->SetBranchAddress("ph_isEMLoose", &ph_isEMLoose, &b_ph_isEMLoose);
    fChain->SetBranchAddress("ph_isEMMedium", &ph_isEMMedium, &b_ph_isEMMedium);
    fChain->SetBranchAddress("ph_isEMTight", &ph_isEMTight, &b_ph_isEMTight);
    fChain->SetBranchAddress("ph_OQ", &ph_OQ, &b_ph_OQ);
    fChain->SetBranchAddress("ph_convFlag", &ph_convFlag, &b_ph_convFlag);
    fChain->SetBranchAddress("ph_isConv", &ph_isConv, &b_ph_isConv);
    fChain->SetBranchAddress("ph_nConv", &ph_nConv, &b_ph_nConv);
    fChain->SetBranchAddress("ph_nSingleTrackConv", &ph_nSingleTrackConv, &b_ph_nSingleTrackConv);
    fChain->SetBranchAddress("ph_nDoubleTrackConv", &ph_nDoubleTrackConv, &b_ph_nDoubleTrackConv);
    fChain->SetBranchAddress("ph_loose", &ph_loose, &b_ph_loose);
    fChain->SetBranchAddress("ph_looseIso", &ph_looseIso, &b_ph_looseIso);
    fChain->SetBranchAddress("ph_tight", &ph_tight, &b_ph_tight);
    fChain->SetBranchAddress("ph_tightIso", &ph_tightIso, &b_ph_tightIso);
    fChain->SetBranchAddress("ph_looseAR", &ph_looseAR, &b_ph_looseAR);
    fChain->SetBranchAddress("ph_looseARIso", &ph_looseARIso, &b_ph_looseARIso);
    fChain->SetBranchAddress("ph_tightAR", &ph_tightAR, &b_ph_tightAR);
    fChain->SetBranchAddress("ph_tightARIso", &ph_tightARIso, &b_ph_tightARIso);
    fChain->SetBranchAddress("ph_goodOQ", &ph_goodOQ, &b_ph_goodOQ);
    fChain->SetBranchAddress("ph_Ethad", &ph_Ethad, &b_ph_Ethad);
    fChain->SetBranchAddress("ph_Ethad1", &ph_Ethad1, &b_ph_Ethad1);
    fChain->SetBranchAddress("ph_E033", &ph_E033, &b_ph_E033);
    fChain->SetBranchAddress("ph_f1", &ph_f1, &b_ph_f1);
    fChain->SetBranchAddress("ph_f1core", &ph_f1core, &b_ph_f1core);
    fChain->SetBranchAddress("ph_Emins1", &ph_Emins1, &b_ph_Emins1);
    fChain->SetBranchAddress("ph_fside", &ph_fside, &b_ph_fside);
    fChain->SetBranchAddress("ph_Emax2", &ph_Emax2, &b_ph_Emax2);
    fChain->SetBranchAddress("ph_ws3", &ph_ws3, &b_ph_ws3);
    fChain->SetBranchAddress("ph_wstot", &ph_wstot, &b_ph_wstot);
    fChain->SetBranchAddress("ph_E132", &ph_E132, &b_ph_E132);
    fChain->SetBranchAddress("ph_E1152", &ph_E1152, &b_ph_E1152);
    fChain->SetBranchAddress("ph_emaxs1", &ph_emaxs1, &b_ph_emaxs1);
    fChain->SetBranchAddress("ph_deltaEs", &ph_deltaEs, &b_ph_deltaEs);
    fChain->SetBranchAddress("ph_E233", &ph_E233, &b_ph_E233);
    fChain->SetBranchAddress("ph_E237", &ph_E237, &b_ph_E237);
    fChain->SetBranchAddress("ph_E277", &ph_E277, &b_ph_E277);
    fChain->SetBranchAddress("ph_weta2", &ph_weta2, &b_ph_weta2);
    fChain->SetBranchAddress("ph_f3", &ph_f3, &b_ph_f3);
    fChain->SetBranchAddress("ph_f3core", &ph_f3core, &b_ph_f3core);
    fChain->SetBranchAddress("ph_rphiallcalo", &ph_rphiallcalo, &b_ph_rphiallcalo);
    fChain->SetBranchAddress("ph_Etcone45", &ph_Etcone45, &b_ph_Etcone45);
    fChain->SetBranchAddress("ph_Etcone15", &ph_Etcone15, &b_ph_Etcone15);
    fChain->SetBranchAddress("ph_Etcone20", &ph_Etcone20, &b_ph_Etcone20);
    fChain->SetBranchAddress("ph_Etcone25", &ph_Etcone25, &b_ph_Etcone25);
    fChain->SetBranchAddress("ph_Etcone30", &ph_Etcone30, &b_ph_Etcone30);
    fChain->SetBranchAddress("ph_Etcone35", &ph_Etcone35, &b_ph_Etcone35);
    fChain->SetBranchAddress("ph_Etcone40", &ph_Etcone40, &b_ph_Etcone40);
    fChain->SetBranchAddress("ph_ptcone20", &ph_ptcone20, &b_ph_ptcone20);
    fChain->SetBranchAddress("ph_ptcone30", &ph_ptcone30, &b_ph_ptcone30);
    fChain->SetBranchAddress("ph_ptcone40", &ph_ptcone40, &b_ph_ptcone40);
    fChain->SetBranchAddress("ph_nucone20", &ph_nucone20, &b_ph_nucone20);
    fChain->SetBranchAddress("ph_nucone30", &ph_nucone30, &b_ph_nucone30);
    fChain->SetBranchAddress("ph_nucone40", &ph_nucone40, &b_ph_nucone40);
    fChain->SetBranchAddress("ph_Etcone15_pt_corrected", &ph_Etcone15_pt_corrected, &b_ph_Etcone15_pt_corrected);
    fChain->SetBranchAddress("ph_Etcone20_pt_corrected", &ph_Etcone20_pt_corrected, &b_ph_Etcone20_pt_corrected);
    fChain->SetBranchAddress("ph_Etcone25_pt_corrected", &ph_Etcone25_pt_corrected, &b_ph_Etcone25_pt_corrected);
    fChain->SetBranchAddress("ph_Etcone30_pt_corrected", &ph_Etcone30_pt_corrected, &b_ph_Etcone30_pt_corrected);
    fChain->SetBranchAddress("ph_Etcone35_pt_corrected", &ph_Etcone35_pt_corrected, &b_ph_Etcone35_pt_corrected);
    fChain->SetBranchAddress("ph_Etcone40_pt_corrected", &ph_Etcone40_pt_corrected, &b_ph_Etcone40_pt_corrected);
    fChain->SetBranchAddress("ph_convanglematch", &ph_convanglematch, &b_ph_convanglematch);
    fChain->SetBranchAddress("ph_convtrackmatch", &ph_convtrackmatch, &b_ph_convtrackmatch);
    fChain->SetBranchAddress("ph_hasconv", &ph_hasconv, &b_ph_hasconv);
    fChain->SetBranchAddress("ph_convvtxx", &ph_convvtxx, &b_ph_convvtxx);
    fChain->SetBranchAddress("ph_convvtxy", &ph_convvtxy, &b_ph_convvtxy);
    fChain->SetBranchAddress("ph_convvtxz", &ph_convvtxz, &b_ph_convvtxz);
    fChain->SetBranchAddress("ph_Rconv", &ph_Rconv, &b_ph_Rconv);
    fChain->SetBranchAddress("ph_zconv", &ph_zconv, &b_ph_zconv);
    fChain->SetBranchAddress("ph_convvtxchi2", &ph_convvtxchi2, &b_ph_convvtxchi2);
    fChain->SetBranchAddress("ph_pt1conv", &ph_pt1conv, &b_ph_pt1conv);
    fChain->SetBranchAddress("ph_convtrk1nBLHits", &ph_convtrk1nBLHits, &b_ph_convtrk1nBLHits);
    fChain->SetBranchAddress("ph_convtrk1nPixHits", &ph_convtrk1nPixHits, &b_ph_convtrk1nPixHits);
    fChain->SetBranchAddress("ph_convtrk1nSCTHits", &ph_convtrk1nSCTHits, &b_ph_convtrk1nSCTHits);
    fChain->SetBranchAddress("ph_convtrk1nTRTHits", &ph_convtrk1nTRTHits, &b_ph_convtrk1nTRTHits);
    fChain->SetBranchAddress("ph_pt2conv", &ph_pt2conv, &b_ph_pt2conv);
    fChain->SetBranchAddress("ph_convtrk2nBLHits", &ph_convtrk2nBLHits, &b_ph_convtrk2nBLHits);
    fChain->SetBranchAddress("ph_convtrk2nPixHits", &ph_convtrk2nPixHits, &b_ph_convtrk2nPixHits);
    fChain->SetBranchAddress("ph_convtrk2nSCTHits", &ph_convtrk2nSCTHits, &b_ph_convtrk2nSCTHits);
    fChain->SetBranchAddress("ph_convtrk2nTRTHits", &ph_convtrk2nTRTHits, &b_ph_convtrk2nTRTHits);
    fChain->SetBranchAddress("ph_ptconv", &ph_ptconv, &b_ph_ptconv);
    fChain->SetBranchAddress("ph_pzconv", &ph_pzconv, &b_ph_pzconv);
    fChain->SetBranchAddress("ph_reta", &ph_reta, &b_ph_reta);
    fChain->SetBranchAddress("ph_rphi", &ph_rphi, &b_ph_rphi);
    fChain->SetBranchAddress("ph_topoEtcone20", &ph_topoEtcone20, &b_ph_topoEtcone20);
    fChain->SetBranchAddress("ph_topoEtcone30", &ph_topoEtcone30, &b_ph_topoEtcone30);
    fChain->SetBranchAddress("ph_topoEtcone40", &ph_topoEtcone40, &b_ph_topoEtcone40);
    fChain->SetBranchAddress("ph_materialTraversed", &ph_materialTraversed, &b_ph_materialTraversed);
    fChain->SetBranchAddress("ph_EtringnoisedR03sig2", &ph_EtringnoisedR03sig2, &b_ph_EtringnoisedR03sig2);
    fChain->SetBranchAddress("ph_EtringnoisedR03sig3", &ph_EtringnoisedR03sig3, &b_ph_EtringnoisedR03sig3);
    fChain->SetBranchAddress("ph_EtringnoisedR03sig4", &ph_EtringnoisedR03sig4, &b_ph_EtringnoisedR03sig4);
    fChain->SetBranchAddress("ph_ptcone20_zpv05", &ph_ptcone20_zpv05, &b_ph_ptcone20_zpv05);
    fChain->SetBranchAddress("ph_ptcone30_zpv05", &ph_ptcone30_zpv05, &b_ph_ptcone30_zpv05);
    fChain->SetBranchAddress("ph_ptcone40_zpv05", &ph_ptcone40_zpv05, &b_ph_ptcone40_zpv05);
    fChain->SetBranchAddress("ph_nucone20_zpv05", &ph_nucone20_zpv05, &b_ph_nucone20_zpv05);
    fChain->SetBranchAddress("ph_nucone30_zpv05", &ph_nucone30_zpv05, &b_ph_nucone30_zpv05);
    fChain->SetBranchAddress("ph_nucone40_zpv05", &ph_nucone40_zpv05, &b_ph_nucone40_zpv05);
    fChain->SetBranchAddress("ph_isolationlikelihoodjets", &ph_isolationlikelihoodjets, &b_ph_isolationlikelihoodjets);
    fChain->SetBranchAddress("ph_isolationlikelihoodhqelectrons", &ph_isolationlikelihoodhqelectrons,
                             &b_ph_isolationlikelihoodhqelectrons);
    fChain->SetBranchAddress("ph_loglikelihood", &ph_loglikelihood, &b_ph_loglikelihood);
    fChain->SetBranchAddress("ph_photonweight", &ph_photonweight, &b_ph_photonweight);
    fChain->SetBranchAddress("ph_photonbgweight", &ph_photonbgweight, &b_ph_photonbgweight);
    fChain->SetBranchAddress("ph_neuralnet", &ph_neuralnet, &b_ph_neuralnet);
    fChain->SetBranchAddress("ph_Hmatrix", &ph_Hmatrix, &b_ph_Hmatrix);
    fChain->SetBranchAddress("ph_Hmatrix5", &ph_Hmatrix5, &b_ph_Hmatrix5);
    fChain->SetBranchAddress("ph_adaboost", &ph_adaboost, &b_ph_adaboost);
    fChain->SetBranchAddress("ph_ringernn", &ph_ringernn, &b_ph_ringernn);
    fChain->SetBranchAddress("ph_zvertex", &ph_zvertex, &b_ph_zvertex);
    fChain->SetBranchAddress("ph_errz", &ph_errz, &b_ph_errz);
    fChain->SetBranchAddress("ph_etap", &ph_etap, &b_ph_etap);
    fChain->SetBranchAddress("ph_depth", &ph_depth, &b_ph_depth);
    fChain->SetBranchAddress("ph_cl_E", &ph_cl_E, &b_ph_cl_E);
    fChain->SetBranchAddress("ph_cl_pt", &ph_cl_pt, &b_ph_cl_pt);
    fChain->SetBranchAddress("ph_cl_eta", &ph_cl_eta, &b_ph_cl_eta);
    fChain->SetBranchAddress("ph_cl_phi", &ph_cl_phi, &b_ph_cl_phi);
    fChain->SetBranchAddress("ph_cl_etaCalo", &ph_cl_etaCalo, &b_ph_cl_etaCalo);
    fChain->SetBranchAddress("ph_cl_phiCalo", &ph_cl_phiCalo, &b_ph_cl_phiCalo);
    fChain->SetBranchAddress("ph_Es0", &ph_Es0, &b_ph_Es0);
    fChain->SetBranchAddress("ph_etas0", &ph_etas0, &b_ph_etas0);
    fChain->SetBranchAddress("ph_phis0", &ph_phis0, &b_ph_phis0);
    fChain->SetBranchAddress("ph_Es1", &ph_Es1, &b_ph_Es1);
    fChain->SetBranchAddress("ph_etas1", &ph_etas1, &b_ph_etas1);
    fChain->SetBranchAddress("ph_phis1", &ph_phis1, &b_ph_phis1);
    fChain->SetBranchAddress("ph_Es2", &ph_Es2, &b_ph_Es2);
    fChain->SetBranchAddress("ph_etas2", &ph_etas2, &b_ph_etas2);
    fChain->SetBranchAddress("ph_phis2", &ph_phis2, &b_ph_phis2);
    fChain->SetBranchAddress("ph_Es3", &ph_Es3, &b_ph_Es3);
    fChain->SetBranchAddress("ph_etas3", &ph_etas3, &b_ph_etas3);
    fChain->SetBranchAddress("ph_phis3", &ph_phis3, &b_ph_phis3);
    fChain->SetBranchAddress("ph_rawcl_Es0", &ph_rawcl_Es0, &b_ph_rawcl_Es0);
    fChain->SetBranchAddress("ph_rawcl_etas0", &ph_rawcl_etas0, &b_ph_rawcl_etas0);
    fChain->SetBranchAddress("ph_rawcl_phis0", &ph_rawcl_phis0, &b_ph_rawcl_phis0);
    fChain->SetBranchAddress("ph_rawcl_Es1", &ph_rawcl_Es1, &b_ph_rawcl_Es1);
    fChain->SetBranchAddress("ph_rawcl_etas1", &ph_rawcl_etas1, &b_ph_rawcl_etas1);
    fChain->SetBranchAddress("ph_rawcl_phis1", &ph_rawcl_phis1, &b_ph_rawcl_phis1);
    fChain->SetBranchAddress("ph_rawcl_Es2", &ph_rawcl_Es2, &b_ph_rawcl_Es2);
    fChain->SetBranchAddress("ph_rawcl_etas2", &ph_rawcl_etas2, &b_ph_rawcl_etas2);
    fChain->SetBranchAddress("ph_rawcl_phis2", &ph_rawcl_phis2, &b_ph_rawcl_phis2);
    fChain->SetBranchAddress("ph_rawcl_Es3", &ph_rawcl_Es3, &b_ph_rawcl_Es3);
    fChain->SetBranchAddress("ph_rawcl_etas3", &ph_rawcl_etas3, &b_ph_rawcl_etas3);
    fChain->SetBranchAddress("ph_rawcl_phis3", &ph_rawcl_phis3, &b_ph_rawcl_phis3);
    fChain->SetBranchAddress("ph_rawcl_E", &ph_rawcl_E, &b_ph_rawcl_E);
    fChain->SetBranchAddress("ph_rawcl_pt", &ph_rawcl_pt, &b_ph_rawcl_pt);
    fChain->SetBranchAddress("ph_rawcl_eta", &ph_rawcl_eta, &b_ph_rawcl_eta);
    fChain->SetBranchAddress("ph_rawcl_phi", &ph_rawcl_phi, &b_ph_rawcl_phi);
    fChain->SetBranchAddress("ph_convMatchDeltaEta1", &ph_convMatchDeltaEta1, &b_ph_convMatchDeltaEta1);
    fChain->SetBranchAddress("ph_convMatchDeltaEta2", &ph_convMatchDeltaEta2, &b_ph_convMatchDeltaEta2);
    fChain->SetBranchAddress("ph_convMatchDeltaPhi1", &ph_convMatchDeltaPhi1, &b_ph_convMatchDeltaPhi1);
    fChain->SetBranchAddress("ph_convMatchDeltaPhi2", &ph_convMatchDeltaPhi2, &b_ph_convMatchDeltaPhi2);
    fChain->SetBranchAddress("ph_rings_E", &ph_rings_E, &b_ph_rings_E);
    fChain->SetBranchAddress("ph_vx_n", &ph_vx_n, &b_ph_vx_n);
    fChain->SetBranchAddress("ph_vx_x", &ph_vx_x, &b_ph_vx_x);
    fChain->SetBranchAddress("ph_vx_y", &ph_vx_y, &b_ph_vx_y);
    fChain->SetBranchAddress("ph_vx_z", &ph_vx_z, &b_ph_vx_z);
    fChain->SetBranchAddress("ph_vx_px", &ph_vx_px, &b_ph_vx_px);
    fChain->SetBranchAddress("ph_vx_py", &ph_vx_py, &b_ph_vx_py);
    fChain->SetBranchAddress("ph_vx_pz", &ph_vx_pz, &b_ph_vx_pz);
    fChain->SetBranchAddress("ph_vx_E", &ph_vx_E, &b_ph_vx_E);
    fChain->SetBranchAddress("ph_vx_m", &ph_vx_m, &b_ph_vx_m);
    fChain->SetBranchAddress("ph_vx_nTracks", &ph_vx_nTracks, &b_ph_vx_nTracks);
    fChain->SetBranchAddress("ph_vx_sumPt", &ph_vx_sumPt, &b_ph_vx_sumPt);
    fChain->SetBranchAddress("ph_vx_convTrk_weight", &ph_vx_convTrk_weight, &b_ph_vx_convTrk_weight);
    fChain->SetBranchAddress("ph_vx_convTrk_n", &ph_vx_convTrk_n, &b_ph_vx_convTrk_n);
    fChain->SetBranchAddress("ph_vx_convTrk_fitter", &ph_vx_convTrk_fitter, &b_ph_vx_convTrk_fitter);
    fChain->SetBranchAddress("ph_vx_convTrk_patternReco1", &ph_vx_convTrk_patternReco1, &b_ph_vx_convTrk_patternReco1);
    fChain->SetBranchAddress("ph_vx_convTrk_patternReco2", &ph_vx_convTrk_patternReco2, &b_ph_vx_convTrk_patternReco2);
    fChain->SetBranchAddress("ph_vx_convTrk_trackProperties", &ph_vx_convTrk_trackProperties, &b_ph_vx_convTrk_trackProperties);
    fChain->SetBranchAddress("ph_vx_convTrk_particleHypothesis", &ph_vx_convTrk_particleHypothesis,
                             &b_ph_vx_convTrk_particleHypothesis);
    fChain->SetBranchAddress("ph_vx_convTrk_chi2", &ph_vx_convTrk_chi2, &b_ph_vx_convTrk_chi2);
    fChain->SetBranchAddress("ph_vx_convTrk_ndof", &ph_vx_convTrk_ndof, &b_ph_vx_convTrk_ndof);
    fChain->SetBranchAddress("ph_deltaEmax2", &ph_deltaEmax2, &b_ph_deltaEmax2);
    fChain->SetBranchAddress("ph_calibHitsShowerDepth", &ph_calibHitsShowerDepth, &b_ph_calibHitsShowerDepth);
    fChain->SetBranchAddress("ph_isIso", &ph_isIso, &b_ph_isIso);
    fChain->SetBranchAddress("ph_mvaptcone20", &ph_mvaptcone20, &b_ph_mvaptcone20);
    fChain->SetBranchAddress("ph_mvaptcone30", &ph_mvaptcone30, &b_ph_mvaptcone30);
    fChain->SetBranchAddress("ph_mvaptcone40", &ph_mvaptcone40, &b_ph_mvaptcone40);
    fChain->SetBranchAddress("ph_topoEtcone60", &ph_topoEtcone60, &b_ph_topoEtcone60);
    fChain->SetBranchAddress("ph_vx_Chi2", &ph_vx_Chi2, &b_ph_vx_Chi2);
    fChain->SetBranchAddress("ph_vx_Dcottheta", &ph_vx_Dcottheta, &b_ph_vx_Dcottheta);
    fChain->SetBranchAddress("ph_vx_Dphi", &ph_vx_Dphi, &b_ph_vx_Dphi);
    fChain->SetBranchAddress("ph_vx_Dist", &ph_vx_Dist, &b_ph_vx_Dist);
    fChain->SetBranchAddress("ph_vx_DR1R2", &ph_vx_DR1R2, &b_ph_vx_DR1R2);
    fChain->SetBranchAddress("ph_CaloPointing_eta", &ph_CaloPointing_eta, &b_ph_CaloPointing_eta);
    fChain->SetBranchAddress("ph_CaloPointing_sigma_eta", &ph_CaloPointing_sigma_eta, &b_ph_CaloPointing_sigma_eta);
    fChain->SetBranchAddress("ph_CaloPointing_zvertex", &ph_CaloPointing_zvertex, &b_ph_CaloPointing_zvertex);
    fChain->SetBranchAddress("ph_CaloPointing_sigma_zvertex", &ph_CaloPointing_sigma_zvertex, &b_ph_CaloPointing_sigma_zvertex);
    fChain->SetBranchAddress("ph_HPV_eta", &ph_HPV_eta, &b_ph_HPV_eta);
    fChain->SetBranchAddress("ph_HPV_sigma_eta", &ph_HPV_sigma_eta, &b_ph_HPV_sigma_eta);
    fChain->SetBranchAddress("ph_HPV_zvertex", &ph_HPV_zvertex, &b_ph_HPV_zvertex);
    fChain->SetBranchAddress("ph_HPV_sigma_zvertex", &ph_HPV_sigma_zvertex, &b_ph_HPV_sigma_zvertex);
    fChain->SetBranchAddress("ph_NN_passes", &ph_NN_passes, &b_ph_NN_passes);
    fChain->SetBranchAddress("ph_NN_discriminant", &ph_NN_discriminant, &b_ph_NN_discriminant);
    fChain->SetBranchAddress("ph_ES0_real", &ph_ES0_real, &b_ph_ES0_real);
    fChain->SetBranchAddress("ph_ES1_real", &ph_ES1_real, &b_ph_ES1_real);
    fChain->SetBranchAddress("ph_ES2_real", &ph_ES2_real, &b_ph_ES2_real);
    fChain->SetBranchAddress("ph_ES3_real", &ph_ES3_real, &b_ph_ES3_real);
    fChain->SetBranchAddress("ph_EcellS0", &ph_EcellS0, &b_ph_EcellS0);
    fChain->SetBranchAddress("ph_etacellS0", &ph_etacellS0, &b_ph_etacellS0);
    fChain->SetBranchAddress("ph_Etcone40_ED_corrected", &ph_Etcone40_ED_corrected, &b_ph_Etcone40_ED_corrected);
    fChain->SetBranchAddress("ph_Etcone40_corrected", &ph_Etcone40_corrected, &b_ph_Etcone40_corrected);
    fChain->SetBranchAddress("ph_topoEtcone20_corrected", &ph_topoEtcone20_corrected, &b_ph_topoEtcone20_corrected);
    fChain->SetBranchAddress("ph_topoEtcone30_corrected", &ph_topoEtcone30_corrected, &b_ph_topoEtcone30_corrected);
    fChain->SetBranchAddress("ph_topoEtcone40_corrected", &ph_topoEtcone40_corrected, &b_ph_topoEtcone40_corrected);
    fChain->SetBranchAddress("ph_ED_median", &ph_ED_median, &b_ph_ED_median);
    fChain->SetBranchAddress("ph_ED_sigma", &ph_ED_sigma, &b_ph_ED_sigma);
    fChain->SetBranchAddress("ph_ED_Njets", &ph_ED_Njets, &b_ph_ED_Njets);
    fChain->SetBranchAddress("ph_convIP", &ph_convIP, &b_ph_convIP);
    fChain->SetBranchAddress("ph_convIPRev", &ph_convIPRev, &b_ph_convIPRev);
    fChain->SetBranchAddress("ph_jet_dr", &ph_jet_dr, &b_ph_jet_dr);
    fChain->SetBranchAddress("ph_jet_E", &ph_jet_E, &b_ph_jet_E);
    fChain->SetBranchAddress("ph_jet_pt", &ph_jet_pt, &b_ph_jet_pt);
    fChain->SetBranchAddress("ph_jet_m", &ph_jet_m, &b_ph_jet_m);
    fChain->SetBranchAddress("ph_jet_eta", &ph_jet_eta, &b_ph_jet_eta);
    fChain->SetBranchAddress("ph_jet_phi", &ph_jet_phi, &b_ph_jet_phi);
    fChain->SetBranchAddress("ph_jet_matched", &ph_jet_matched, &b_ph_jet_matched);
    fChain->SetBranchAddress("ph_topodr", &ph_topodr, &b_ph_topodr);
    fChain->SetBranchAddress("ph_topopt", &ph_topopt, &b_ph_topopt);
    fChain->SetBranchAddress("ph_topoeta", &ph_topoeta, &b_ph_topoeta);
    fChain->SetBranchAddress("ph_topophi", &ph_topophi, &b_ph_topophi);
    fChain->SetBranchAddress("ph_topomatched", &ph_topomatched, &b_ph_topomatched);
    fChain->SetBranchAddress("ph_topoEMdr", &ph_topoEMdr, &b_ph_topoEMdr);
    fChain->SetBranchAddress("ph_topoEMpt", &ph_topoEMpt, &b_ph_topoEMpt);
    fChain->SetBranchAddress("ph_topoEMeta", &ph_topoEMeta, &b_ph_topoEMeta);
    fChain->SetBranchAddress("ph_topoEMphi", &ph_topoEMphi, &b_ph_topoEMphi);
    fChain->SetBranchAddress("ph_topoEMmatched", &ph_topoEMmatched, &b_ph_topoEMmatched);
    fChain->SetBranchAddress("ph_el_index", &ph_el_index, &b_ph_el_index);
    fChain->SetBranchAddress("mu_muon_n", &mu_muon_n, &b_mu_muon_n);
    fChain->SetBranchAddress("mu_muon_E", &mu_muon_E, &b_mu_muon_E);
    fChain->SetBranchAddress("mu_muon_pt", &mu_muon_pt, &b_mu_muon_pt);
    fChain->SetBranchAddress("mu_muon_m", &mu_muon_m, &b_mu_muon_m);
    fChain->SetBranchAddress("mu_muon_eta", &mu_muon_eta, &b_mu_muon_eta);
    fChain->SetBranchAddress("mu_muon_phi", &mu_muon_phi, &b_mu_muon_phi);
    fChain->SetBranchAddress("mu_muon_px", &mu_muon_px, &b_mu_muon_px);
    fChain->SetBranchAddress("mu_muon_py", &mu_muon_py, &b_mu_muon_py);
    fChain->SetBranchAddress("mu_muon_pz", &mu_muon_pz, &b_mu_muon_pz);
    fChain->SetBranchAddress("mu_muon_charge", &mu_muon_charge, &b_mu_muon_charge);
    fChain->SetBranchAddress("mu_muon_allauthor", &mu_muon_allauthor, &b_mu_muon_allauthor);
    fChain->SetBranchAddress("mu_muon_author", &mu_muon_author, &b_mu_muon_author);
    fChain->SetBranchAddress("mu_muon_beta", &mu_muon_beta, &b_mu_muon_beta);
    fChain->SetBranchAddress("mu_muon_isMuonLikelihood", &mu_muon_isMuonLikelihood, &b_mu_muon_isMuonLikelihood);
    fChain->SetBranchAddress("mu_muon_matchchi2", &mu_muon_matchchi2, &b_mu_muon_matchchi2);
    fChain->SetBranchAddress("mu_muon_matchndof", &mu_muon_matchndof, &b_mu_muon_matchndof);
    fChain->SetBranchAddress("mu_muon_etcone20", &mu_muon_etcone20, &b_mu_muon_etcone20);
    fChain->SetBranchAddress("mu_muon_etcone30", &mu_muon_etcone30, &b_mu_muon_etcone30);
    fChain->SetBranchAddress("mu_muon_etcone40", &mu_muon_etcone40, &b_mu_muon_etcone40);
    fChain->SetBranchAddress("mu_muon_nucone20", &mu_muon_nucone20, &b_mu_muon_nucone20);
    fChain->SetBranchAddress("mu_muon_nucone30", &mu_muon_nucone30, &b_mu_muon_nucone30);
    fChain->SetBranchAddress("mu_muon_nucone40", &mu_muon_nucone40, &b_mu_muon_nucone40);
    fChain->SetBranchAddress("mu_muon_ptcone20", &mu_muon_ptcone20, &b_mu_muon_ptcone20);
    fChain->SetBranchAddress("mu_muon_ptcone30", &mu_muon_ptcone30, &b_mu_muon_ptcone30);
    fChain->SetBranchAddress("mu_muon_ptcone40", &mu_muon_ptcone40, &b_mu_muon_ptcone40);
    fChain->SetBranchAddress("mu_muon_etconeNoEm10", &mu_muon_etconeNoEm10, &b_mu_muon_etconeNoEm10);
    fChain->SetBranchAddress("mu_muon_etconeNoEm20", &mu_muon_etconeNoEm20, &b_mu_muon_etconeNoEm20);
    fChain->SetBranchAddress("mu_muon_etconeNoEm30", &mu_muon_etconeNoEm30, &b_mu_muon_etconeNoEm30);
    fChain->SetBranchAddress("mu_muon_etconeNoEm40", &mu_muon_etconeNoEm40, &b_mu_muon_etconeNoEm40);
    fChain->SetBranchAddress("mu_muon_scatteringCurvatureSignificance", &mu_muon_scatteringCurvatureSignificance,
                             &b_mu_muon_scatteringCurvatureSignificance);
    fChain->SetBranchAddress("mu_muon_scatteringNeighbourSignificance", &mu_muon_scatteringNeighbourSignificance,
                             &b_mu_muon_scatteringNeighbourSignificance);
    fChain->SetBranchAddress("mu_muon_momentumBalanceSignificance", &mu_muon_momentumBalanceSignificance,
                             &b_mu_muon_momentumBalanceSignificance);
    fChain->SetBranchAddress("mu_muon_energyLossPar", &mu_muon_energyLossPar, &b_mu_muon_energyLossPar);
    fChain->SetBranchAddress("mu_muon_energyLossErr", &mu_muon_energyLossErr, &b_mu_muon_energyLossErr);
    fChain->SetBranchAddress("mu_muon_etCore", &mu_muon_etCore, &b_mu_muon_etCore);
    fChain->SetBranchAddress("mu_muon_energyLossType", &mu_muon_energyLossType, &b_mu_muon_energyLossType);
    fChain->SetBranchAddress("mu_muon_caloMuonIdTag", &mu_muon_caloMuonIdTag, &b_mu_muon_caloMuonIdTag);
    fChain->SetBranchAddress("mu_muon_caloLRLikelihood", &mu_muon_caloLRLikelihood, &b_mu_muon_caloLRLikelihood);
    fChain->SetBranchAddress("mu_muon_Ehad", &mu_muon_Ehad, &b_mu_muon_Ehad);
    fChain->SetBranchAddress("mu_muon_Mhad", &mu_muon_Mhad, &b_mu_muon_Mhad);
    fChain->SetBranchAddress("mu_muon_Es0", &mu_muon_Es0, &b_mu_muon_Es0);
    fChain->SetBranchAddress("mu_muon_Ms0", &mu_muon_Ms0, &b_mu_muon_Ms0);
    fChain->SetBranchAddress("mu_muon_Es1", &mu_muon_Es1, &b_mu_muon_Es1);
    fChain->SetBranchAddress("mu_muon_Ms1", &mu_muon_Ms1, &b_mu_muon_Ms1);
    fChain->SetBranchAddress("mu_muon_Es2", &mu_muon_Es2, &b_mu_muon_Es2);
    fChain->SetBranchAddress("mu_muon_Ms2", &mu_muon_Ms2, &b_mu_muon_Ms2);
    fChain->SetBranchAddress("mu_muon_Es3", &mu_muon_Es3, &b_mu_muon_Es3);
    fChain->SetBranchAddress("mu_muon_Ms3", &mu_muon_Ms3, &b_mu_muon_Ms3);
    fChain->SetBranchAddress("mu_muon_bestMatch", &mu_muon_bestMatch, &b_mu_muon_bestMatch);
    fChain->SetBranchAddress("mu_muon_isStandAloneMuon", &mu_muon_isStandAloneMuon, &b_mu_muon_isStandAloneMuon);
    fChain->SetBranchAddress("mu_muon_isCombinedMuon", &mu_muon_isCombinedMuon, &b_mu_muon_isCombinedMuon);
    fChain->SetBranchAddress("mu_muon_isLowPtReconstructedMuon", &mu_muon_isLowPtReconstructedMuon,
                             &b_mu_muon_isLowPtReconstructedMuon);
    fChain->SetBranchAddress("mu_muon_isSegmentTaggedMuon", &mu_muon_isSegmentTaggedMuon, &b_mu_muon_isSegmentTaggedMuon);
    fChain->SetBranchAddress("mu_muon_isCaloMuonId", &mu_muon_isCaloMuonId, &b_mu_muon_isCaloMuonId);
    fChain->SetBranchAddress("mu_muon_alsoFoundByLowPt", &mu_muon_alsoFoundByLowPt, &b_mu_muon_alsoFoundByLowPt);
    fChain->SetBranchAddress("mu_muon_alsoFoundByCaloMuonId", &mu_muon_alsoFoundByCaloMuonId, &b_mu_muon_alsoFoundByCaloMuonId);
    fChain->SetBranchAddress("mu_muon_isSiliconAssociatedForwardMuon", &mu_muon_isSiliconAssociatedForwardMuon,
                             &b_mu_muon_isSiliconAssociatedForwardMuon);
    fChain->SetBranchAddress("mu_muon_loose", &mu_muon_loose, &b_mu_muon_loose);
    fChain->SetBranchAddress("mu_muon_medium", &mu_muon_medium, &b_mu_muon_medium);
    fChain->SetBranchAddress("mu_muon_tight", &mu_muon_tight, &b_mu_muon_tight);
    fChain->SetBranchAddress("mu_muon_d0_exPV", &mu_muon_d0_exPV, &b_mu_muon_d0_exPV);
    fChain->SetBranchAddress("mu_muon_z0_exPV", &mu_muon_z0_exPV, &b_mu_muon_z0_exPV);
    fChain->SetBranchAddress("mu_muon_phi_exPV", &mu_muon_phi_exPV, &b_mu_muon_phi_exPV);
    fChain->SetBranchAddress("mu_muon_theta_exPV", &mu_muon_theta_exPV, &b_mu_muon_theta_exPV);
    fChain->SetBranchAddress("mu_muon_qoverp_exPV", &mu_muon_qoverp_exPV, &b_mu_muon_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_cb_d0_exPV", &mu_muon_cb_d0_exPV, &b_mu_muon_cb_d0_exPV);
    fChain->SetBranchAddress("mu_muon_cb_z0_exPV", &mu_muon_cb_z0_exPV, &b_mu_muon_cb_z0_exPV);
    fChain->SetBranchAddress("mu_muon_cb_phi_exPV", &mu_muon_cb_phi_exPV, &b_mu_muon_cb_phi_exPV);
    fChain->SetBranchAddress("mu_muon_cb_theta_exPV", &mu_muon_cb_theta_exPV, &b_mu_muon_cb_theta_exPV);
    fChain->SetBranchAddress("mu_muon_cb_qoverp_exPV", &mu_muon_cb_qoverp_exPV, &b_mu_muon_cb_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_id_d0_exPV", &mu_muon_id_d0_exPV, &b_mu_muon_id_d0_exPV);
    fChain->SetBranchAddress("mu_muon_id_z0_exPV", &mu_muon_id_z0_exPV, &b_mu_muon_id_z0_exPV);
    fChain->SetBranchAddress("mu_muon_id_phi_exPV", &mu_muon_id_phi_exPV, &b_mu_muon_id_phi_exPV);
    fChain->SetBranchAddress("mu_muon_id_theta_exPV", &mu_muon_id_theta_exPV, &b_mu_muon_id_theta_exPV);
    fChain->SetBranchAddress("mu_muon_id_qoverp_exPV", &mu_muon_id_qoverp_exPV, &b_mu_muon_id_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_me_d0_exPV", &mu_muon_me_d0_exPV, &b_mu_muon_me_d0_exPV);
    fChain->SetBranchAddress("mu_muon_me_z0_exPV", &mu_muon_me_z0_exPV, &b_mu_muon_me_z0_exPV);
    fChain->SetBranchAddress("mu_muon_me_phi_exPV", &mu_muon_me_phi_exPV, &b_mu_muon_me_phi_exPV);
    fChain->SetBranchAddress("mu_muon_me_theta_exPV", &mu_muon_me_theta_exPV, &b_mu_muon_me_theta_exPV);
    fChain->SetBranchAddress("mu_muon_me_qoverp_exPV", &mu_muon_me_qoverp_exPV, &b_mu_muon_me_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_ie_d0_exPV", &mu_muon_ie_d0_exPV, &b_mu_muon_ie_d0_exPV);
    fChain->SetBranchAddress("mu_muon_ie_z0_exPV", &mu_muon_ie_z0_exPV, &b_mu_muon_ie_z0_exPV);
    fChain->SetBranchAddress("mu_muon_ie_phi_exPV", &mu_muon_ie_phi_exPV, &b_mu_muon_ie_phi_exPV);
    fChain->SetBranchAddress("mu_muon_ie_theta_exPV", &mu_muon_ie_theta_exPV, &b_mu_muon_ie_theta_exPV);
    fChain->SetBranchAddress("mu_muon_ie_qoverp_exPV", &mu_muon_ie_qoverp_exPV, &b_mu_muon_ie_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_SpaceTime_detID", &mu_muon_SpaceTime_detID, &b_mu_muon_SpaceTime_detID);
    fChain->SetBranchAddress("mu_muon_SpaceTime_t", &mu_muon_SpaceTime_t, &b_mu_muon_SpaceTime_t);
    fChain->SetBranchAddress("mu_muon_SpaceTime_tError", &mu_muon_SpaceTime_tError, &b_mu_muon_SpaceTime_tError);
    fChain->SetBranchAddress("mu_muon_SpaceTime_weight", &mu_muon_SpaceTime_weight, &b_mu_muon_SpaceTime_weight);
    fChain->SetBranchAddress("mu_muon_SpaceTime_x", &mu_muon_SpaceTime_x, &b_mu_muon_SpaceTime_x);
    fChain->SetBranchAddress("mu_muon_SpaceTime_y", &mu_muon_SpaceTime_y, &b_mu_muon_SpaceTime_y);
    fChain->SetBranchAddress("mu_muon_SpaceTime_z", &mu_muon_SpaceTime_z, &b_mu_muon_SpaceTime_z);
    fChain->SetBranchAddress("mu_muon_cov_d0_exPV", &mu_muon_cov_d0_exPV, &b_mu_muon_cov_d0_exPV);
    fChain->SetBranchAddress("mu_muon_cov_z0_exPV", &mu_muon_cov_z0_exPV, &b_mu_muon_cov_z0_exPV);
    fChain->SetBranchAddress("mu_muon_cov_phi_exPV", &mu_muon_cov_phi_exPV, &b_mu_muon_cov_phi_exPV);
    fChain->SetBranchAddress("mu_muon_cov_theta_exPV", &mu_muon_cov_theta_exPV, &b_mu_muon_cov_theta_exPV);
    fChain->SetBranchAddress("mu_muon_cov_qoverp_exPV", &mu_muon_cov_qoverp_exPV, &b_mu_muon_cov_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_cov_d0_z0_exPV", &mu_muon_cov_d0_z0_exPV, &b_mu_muon_cov_d0_z0_exPV);
    fChain->SetBranchAddress("mu_muon_cov_d0_phi_exPV", &mu_muon_cov_d0_phi_exPV, &b_mu_muon_cov_d0_phi_exPV);
    fChain->SetBranchAddress("mu_muon_cov_d0_theta_exPV", &mu_muon_cov_d0_theta_exPV, &b_mu_muon_cov_d0_theta_exPV);
    fChain->SetBranchAddress("mu_muon_cov_d0_qoverp_exPV", &mu_muon_cov_d0_qoverp_exPV, &b_mu_muon_cov_d0_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_cov_z0_phi_exPV", &mu_muon_cov_z0_phi_exPV, &b_mu_muon_cov_z0_phi_exPV);
    fChain->SetBranchAddress("mu_muon_cov_z0_theta_exPV", &mu_muon_cov_z0_theta_exPV, &b_mu_muon_cov_z0_theta_exPV);
    fChain->SetBranchAddress("mu_muon_cov_z0_qoverp_exPV", &mu_muon_cov_z0_qoverp_exPV, &b_mu_muon_cov_z0_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_cov_phi_theta_exPV", &mu_muon_cov_phi_theta_exPV, &b_mu_muon_cov_phi_theta_exPV);
    fChain->SetBranchAddress("mu_muon_cov_phi_qoverp_exPV", &mu_muon_cov_phi_qoverp_exPV, &b_mu_muon_cov_phi_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_cov_theta_qoverp_exPV", &mu_muon_cov_theta_qoverp_exPV, &b_mu_muon_cov_theta_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_d0_exPV", &mu_muon_id_cov_d0_exPV, &b_mu_muon_id_cov_d0_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_z0_exPV", &mu_muon_id_cov_z0_exPV, &b_mu_muon_id_cov_z0_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_phi_exPV", &mu_muon_id_cov_phi_exPV, &b_mu_muon_id_cov_phi_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_theta_exPV", &mu_muon_id_cov_theta_exPV, &b_mu_muon_id_cov_theta_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_qoverp_exPV", &mu_muon_id_cov_qoverp_exPV, &b_mu_muon_id_cov_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_d0_z0_exPV", &mu_muon_id_cov_d0_z0_exPV, &b_mu_muon_id_cov_d0_z0_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_d0_phi_exPV", &mu_muon_id_cov_d0_phi_exPV, &b_mu_muon_id_cov_d0_phi_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_d0_theta_exPV", &mu_muon_id_cov_d0_theta_exPV, &b_mu_muon_id_cov_d0_theta_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_d0_qoverp_exPV", &mu_muon_id_cov_d0_qoverp_exPV, &b_mu_muon_id_cov_d0_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_z0_phi_exPV", &mu_muon_id_cov_z0_phi_exPV, &b_mu_muon_id_cov_z0_phi_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_z0_theta_exPV", &mu_muon_id_cov_z0_theta_exPV, &b_mu_muon_id_cov_z0_theta_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_z0_qoverp_exPV", &mu_muon_id_cov_z0_qoverp_exPV, &b_mu_muon_id_cov_z0_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_phi_theta_exPV", &mu_muon_id_cov_phi_theta_exPV, &b_mu_muon_id_cov_phi_theta_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_phi_qoverp_exPV", &mu_muon_id_cov_phi_qoverp_exPV,
                             &b_mu_muon_id_cov_phi_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_id_cov_theta_qoverp_exPV", &mu_muon_id_cov_theta_qoverp_exPV,
                             &b_mu_muon_id_cov_theta_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_d0_exPV", &mu_muon_me_cov_d0_exPV, &b_mu_muon_me_cov_d0_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_z0_exPV", &mu_muon_me_cov_z0_exPV, &b_mu_muon_me_cov_z0_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_phi_exPV", &mu_muon_me_cov_phi_exPV, &b_mu_muon_me_cov_phi_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_theta_exPV", &mu_muon_me_cov_theta_exPV, &b_mu_muon_me_cov_theta_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_qoverp_exPV", &mu_muon_me_cov_qoverp_exPV, &b_mu_muon_me_cov_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_d0_z0_exPV", &mu_muon_me_cov_d0_z0_exPV, &b_mu_muon_me_cov_d0_z0_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_d0_phi_exPV", &mu_muon_me_cov_d0_phi_exPV, &b_mu_muon_me_cov_d0_phi_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_d0_theta_exPV", &mu_muon_me_cov_d0_theta_exPV, &b_mu_muon_me_cov_d0_theta_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_d0_qoverp_exPV", &mu_muon_me_cov_d0_qoverp_exPV, &b_mu_muon_me_cov_d0_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_z0_phi_exPV", &mu_muon_me_cov_z0_phi_exPV, &b_mu_muon_me_cov_z0_phi_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_z0_theta_exPV", &mu_muon_me_cov_z0_theta_exPV, &b_mu_muon_me_cov_z0_theta_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_z0_qoverp_exPV", &mu_muon_me_cov_z0_qoverp_exPV, &b_mu_muon_me_cov_z0_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_phi_theta_exPV", &mu_muon_me_cov_phi_theta_exPV, &b_mu_muon_me_cov_phi_theta_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_phi_qoverp_exPV", &mu_muon_me_cov_phi_qoverp_exPV,
                             &b_mu_muon_me_cov_phi_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_me_cov_theta_qoverp_exPV", &mu_muon_me_cov_theta_qoverp_exPV,
                             &b_mu_muon_me_cov_theta_qoverp_exPV);
    fChain->SetBranchAddress("mu_muon_ms_d0", &mu_muon_ms_d0, &b_mu_muon_ms_d0);
    fChain->SetBranchAddress("mu_muon_ms_z0", &mu_muon_ms_z0, &b_mu_muon_ms_z0);
    fChain->SetBranchAddress("mu_muon_ms_phi", &mu_muon_ms_phi, &b_mu_muon_ms_phi);
    fChain->SetBranchAddress("mu_muon_ms_theta", &mu_muon_ms_theta, &b_mu_muon_ms_theta);
    fChain->SetBranchAddress("mu_muon_ms_qoverp", &mu_muon_ms_qoverp, &b_mu_muon_ms_qoverp);
    fChain->SetBranchAddress("mu_muon_id_d0", &mu_muon_id_d0, &b_mu_muon_id_d0);
    fChain->SetBranchAddress("mu_muon_id_z0", &mu_muon_id_z0, &b_mu_muon_id_z0);
    fChain->SetBranchAddress("mu_muon_id_phi", &mu_muon_id_phi, &b_mu_muon_id_phi);
    fChain->SetBranchAddress("mu_muon_id_theta", &mu_muon_id_theta, &b_mu_muon_id_theta);
    fChain->SetBranchAddress("mu_muon_id_qoverp", &mu_muon_id_qoverp, &b_mu_muon_id_qoverp);
    fChain->SetBranchAddress("mu_muon_me_d0", &mu_muon_me_d0, &b_mu_muon_me_d0);
    fChain->SetBranchAddress("mu_muon_me_z0", &mu_muon_me_z0, &b_mu_muon_me_z0);
    fChain->SetBranchAddress("mu_muon_me_phi", &mu_muon_me_phi, &b_mu_muon_me_phi);
    fChain->SetBranchAddress("mu_muon_me_theta", &mu_muon_me_theta, &b_mu_muon_me_theta);
    fChain->SetBranchAddress("mu_muon_me_qoverp", &mu_muon_me_qoverp, &b_mu_muon_me_qoverp);
    fChain->SetBranchAddress("mu_muon_ie_d0", &mu_muon_ie_d0, &b_mu_muon_ie_d0);
    fChain->SetBranchAddress("mu_muon_ie_z0", &mu_muon_ie_z0, &b_mu_muon_ie_z0);
    fChain->SetBranchAddress("mu_muon_ie_phi", &mu_muon_ie_phi, &b_mu_muon_ie_phi);
    fChain->SetBranchAddress("mu_muon_ie_theta", &mu_muon_ie_theta, &b_mu_muon_ie_theta);
    fChain->SetBranchAddress("mu_muon_ie_qoverp", &mu_muon_ie_qoverp, &b_mu_muon_ie_qoverp);
    fChain->SetBranchAddress("mu_muon_nOutliersOnTrack", &mu_muon_nOutliersOnTrack, &b_mu_muon_nOutliersOnTrack);
    fChain->SetBranchAddress("mu_muon_nBLHits", &mu_muon_nBLHits, &b_mu_muon_nBLHits);
    fChain->SetBranchAddress("mu_muon_nPixHits", &mu_muon_nPixHits, &b_mu_muon_nPixHits);
    fChain->SetBranchAddress("mu_muon_nSCTHits", &mu_muon_nSCTHits, &b_mu_muon_nSCTHits);
    fChain->SetBranchAddress("mu_muon_nTRTHits", &mu_muon_nTRTHits, &b_mu_muon_nTRTHits);
    fChain->SetBranchAddress("mu_muon_nTRTHighTHits", &mu_muon_nTRTHighTHits, &b_mu_muon_nTRTHighTHits);
    fChain->SetBranchAddress("mu_muon_nBLSharedHits", &mu_muon_nBLSharedHits, &b_mu_muon_nBLSharedHits);
    fChain->SetBranchAddress("mu_muon_nPixSharedHits", &mu_muon_nPixSharedHits, &b_mu_muon_nPixSharedHits);
    fChain->SetBranchAddress("mu_muon_nPixHoles", &mu_muon_nPixHoles, &b_mu_muon_nPixHoles);
    fChain->SetBranchAddress("mu_muon_nSCTSharedHits", &mu_muon_nSCTSharedHits, &b_mu_muon_nSCTSharedHits);
    fChain->SetBranchAddress("mu_muon_nSCTHoles", &mu_muon_nSCTHoles, &b_mu_muon_nSCTHoles);
    fChain->SetBranchAddress("mu_muon_nTRTOutliers", &mu_muon_nTRTOutliers, &b_mu_muon_nTRTOutliers);
    fChain->SetBranchAddress("mu_muon_nTRTHighTOutliers", &mu_muon_nTRTHighTOutliers, &b_mu_muon_nTRTHighTOutliers);
    fChain->SetBranchAddress("mu_muon_nGangedPixels", &mu_muon_nGangedPixels, &b_mu_muon_nGangedPixels);
    fChain->SetBranchAddress("mu_muon_nPixelDeadSensors", &mu_muon_nPixelDeadSensors, &b_mu_muon_nPixelDeadSensors);
    fChain->SetBranchAddress("mu_muon_nSCTDeadSensors", &mu_muon_nSCTDeadSensors, &b_mu_muon_nSCTDeadSensors);
    fChain->SetBranchAddress("mu_muon_nTRTDeadStraws", &mu_muon_nTRTDeadStraws, &b_mu_muon_nTRTDeadStraws);
    fChain->SetBranchAddress("mu_muon_expectBLayerHit", &mu_muon_expectBLayerHit, &b_mu_muon_expectBLayerHit);
    fChain->SetBranchAddress("mu_muon_nMDTHits", &mu_muon_nMDTHits, &b_mu_muon_nMDTHits);
    fChain->SetBranchAddress("mu_muon_nMDTHoles", &mu_muon_nMDTHoles, &b_mu_muon_nMDTHoles);
    fChain->SetBranchAddress("mu_muon_nCSCEtaHits", &mu_muon_nCSCEtaHits, &b_mu_muon_nCSCEtaHits);
    fChain->SetBranchAddress("mu_muon_nCSCEtaHoles", &mu_muon_nCSCEtaHoles, &b_mu_muon_nCSCEtaHoles);
    fChain->SetBranchAddress("mu_muon_nCSCUnspoiledEtaHits", &mu_muon_nCSCUnspoiledEtaHits, &b_mu_muon_nCSCUnspoiledEtaHits);
    fChain->SetBranchAddress("mu_muon_nCSCPhiHits", &mu_muon_nCSCPhiHits, &b_mu_muon_nCSCPhiHits);
    fChain->SetBranchAddress("mu_muon_nCSCPhiHoles", &mu_muon_nCSCPhiHoles, &b_mu_muon_nCSCPhiHoles);
    fChain->SetBranchAddress("mu_muon_nRPCEtaHits", &mu_muon_nRPCEtaHits, &b_mu_muon_nRPCEtaHits);
    fChain->SetBranchAddress("mu_muon_nRPCEtaHoles", &mu_muon_nRPCEtaHoles, &b_mu_muon_nRPCEtaHoles);
    fChain->SetBranchAddress("mu_muon_nRPCPhiHits", &mu_muon_nRPCPhiHits, &b_mu_muon_nRPCPhiHits);
    fChain->SetBranchAddress("mu_muon_nRPCPhiHoles", &mu_muon_nRPCPhiHoles, &b_mu_muon_nRPCPhiHoles);
    fChain->SetBranchAddress("mu_muon_nTGCEtaHits", &mu_muon_nTGCEtaHits, &b_mu_muon_nTGCEtaHits);
    fChain->SetBranchAddress("mu_muon_nTGCEtaHoles", &mu_muon_nTGCEtaHoles, &b_mu_muon_nTGCEtaHoles);
    fChain->SetBranchAddress("mu_muon_nTGCPhiHits", &mu_muon_nTGCPhiHits, &b_mu_muon_nTGCPhiHits);
    fChain->SetBranchAddress("mu_muon_nTGCPhiHoles", &mu_muon_nTGCPhiHoles, &b_mu_muon_nTGCPhiHoles);
    fChain->SetBranchAddress("mu_muon_nprecisionLayers", &mu_muon_nprecisionLayers, &b_mu_muon_nprecisionLayers);
    fChain->SetBranchAddress("mu_muon_nprecisionHoleLayers", &mu_muon_nprecisionHoleLayers, &b_mu_muon_nprecisionHoleLayers);
    fChain->SetBranchAddress("mu_muon_nphiLayers", &mu_muon_nphiLayers, &b_mu_muon_nphiLayers);
    fChain->SetBranchAddress("mu_muon_ntrigEtaLayers", &mu_muon_ntrigEtaLayers, &b_mu_muon_ntrigEtaLayers);
    fChain->SetBranchAddress("mu_muon_nphiHoleLayers", &mu_muon_nphiHoleLayers, &b_mu_muon_nphiHoleLayers);
    fChain->SetBranchAddress("mu_muon_ntrigEtaHoleLayers", &mu_muon_ntrigEtaHoleLayers, &b_mu_muon_ntrigEtaHoleLayers);
    fChain->SetBranchAddress("mu_muon_nMDTBIHits", &mu_muon_nMDTBIHits, &b_mu_muon_nMDTBIHits);
    fChain->SetBranchAddress("mu_muon_nMDTBMHits", &mu_muon_nMDTBMHits, &b_mu_muon_nMDTBMHits);
    fChain->SetBranchAddress("mu_muon_nMDTBOHits", &mu_muon_nMDTBOHits, &b_mu_muon_nMDTBOHits);
    fChain->SetBranchAddress("mu_muon_nMDTBEEHits", &mu_muon_nMDTBEEHits, &b_mu_muon_nMDTBEEHits);
    fChain->SetBranchAddress("mu_muon_nMDTBIS78Hits", &mu_muon_nMDTBIS78Hits, &b_mu_muon_nMDTBIS78Hits);
    fChain->SetBranchAddress("mu_muon_nMDTEIHits", &mu_muon_nMDTEIHits, &b_mu_muon_nMDTEIHits);
    fChain->SetBranchAddress("mu_muon_nMDTEMHits", &mu_muon_nMDTEMHits, &b_mu_muon_nMDTEMHits);
    fChain->SetBranchAddress("mu_muon_nMDTEOHits", &mu_muon_nMDTEOHits, &b_mu_muon_nMDTEOHits);
    fChain->SetBranchAddress("mu_muon_nMDTEEHits", &mu_muon_nMDTEEHits, &b_mu_muon_nMDTEEHits);
    fChain->SetBranchAddress("mu_muon_nRPCLayer1EtaHits", &mu_muon_nRPCLayer1EtaHits, &b_mu_muon_nRPCLayer1EtaHits);
    fChain->SetBranchAddress("mu_muon_nRPCLayer2EtaHits", &mu_muon_nRPCLayer2EtaHits, &b_mu_muon_nRPCLayer2EtaHits);
    fChain->SetBranchAddress("mu_muon_nRPCLayer3EtaHits", &mu_muon_nRPCLayer3EtaHits, &b_mu_muon_nRPCLayer3EtaHits);
    fChain->SetBranchAddress("mu_muon_nRPCLayer1PhiHits", &mu_muon_nRPCLayer1PhiHits, &b_mu_muon_nRPCLayer1PhiHits);
    fChain->SetBranchAddress("mu_muon_nRPCLayer2PhiHits", &mu_muon_nRPCLayer2PhiHits, &b_mu_muon_nRPCLayer2PhiHits);
    fChain->SetBranchAddress("mu_muon_nRPCLayer3PhiHits", &mu_muon_nRPCLayer3PhiHits, &b_mu_muon_nRPCLayer3PhiHits);
    fChain->SetBranchAddress("mu_muon_nTGCLayer1EtaHits", &mu_muon_nTGCLayer1EtaHits, &b_mu_muon_nTGCLayer1EtaHits);
    fChain->SetBranchAddress("mu_muon_nTGCLayer2EtaHits", &mu_muon_nTGCLayer2EtaHits, &b_mu_muon_nTGCLayer2EtaHits);
    fChain->SetBranchAddress("mu_muon_nTGCLayer3EtaHits", &mu_muon_nTGCLayer3EtaHits, &b_mu_muon_nTGCLayer3EtaHits);
    fChain->SetBranchAddress("mu_muon_nTGCLayer4EtaHits", &mu_muon_nTGCLayer4EtaHits, &b_mu_muon_nTGCLayer4EtaHits);
    fChain->SetBranchAddress("mu_muon_nTGCLayer1PhiHits", &mu_muon_nTGCLayer1PhiHits, &b_mu_muon_nTGCLayer1PhiHits);
    fChain->SetBranchAddress("mu_muon_nTGCLayer2PhiHits", &mu_muon_nTGCLayer2PhiHits, &b_mu_muon_nTGCLayer2PhiHits);
    fChain->SetBranchAddress("mu_muon_nTGCLayer3PhiHits", &mu_muon_nTGCLayer3PhiHits, &b_mu_muon_nTGCLayer3PhiHits);
    fChain->SetBranchAddress("mu_muon_nTGCLayer4PhiHits", &mu_muon_nTGCLayer4PhiHits, &b_mu_muon_nTGCLayer4PhiHits);
    fChain->SetBranchAddress("mu_muon_barrelSectors", &mu_muon_barrelSectors, &b_mu_muon_barrelSectors);
    fChain->SetBranchAddress("mu_muon_endcapSectors", &mu_muon_endcapSectors, &b_mu_muon_endcapSectors);
    fChain->SetBranchAddress("mu_muon_spec_surf_px", &mu_muon_spec_surf_px, &b_mu_muon_spec_surf_px);
    fChain->SetBranchAddress("mu_muon_spec_surf_py", &mu_muon_spec_surf_py, &b_mu_muon_spec_surf_py);
    fChain->SetBranchAddress("mu_muon_spec_surf_pz", &mu_muon_spec_surf_pz, &b_mu_muon_spec_surf_pz);
    fChain->SetBranchAddress("mu_muon_spec_surf_x", &mu_muon_spec_surf_x, &b_mu_muon_spec_surf_x);
    fChain->SetBranchAddress("mu_muon_spec_surf_y", &mu_muon_spec_surf_y, &b_mu_muon_spec_surf_y);
    fChain->SetBranchAddress("mu_muon_spec_surf_z", &mu_muon_spec_surf_z, &b_mu_muon_spec_surf_z);
    fChain->SetBranchAddress("mu_muon_trackd0", &mu_muon_trackd0, &b_mu_muon_trackd0);
    fChain->SetBranchAddress("mu_muon_trackz0", &mu_muon_trackz0, &b_mu_muon_trackz0);
    fChain->SetBranchAddress("mu_muon_trackphi", &mu_muon_trackphi, &b_mu_muon_trackphi);
    fChain->SetBranchAddress("mu_muon_tracktheta", &mu_muon_tracktheta, &b_mu_muon_tracktheta);
    fChain->SetBranchAddress("mu_muon_trackqoverp", &mu_muon_trackqoverp, &b_mu_muon_trackqoverp);
    fChain->SetBranchAddress("mu_muon_trackcov_d0", &mu_muon_trackcov_d0, &b_mu_muon_trackcov_d0);
    fChain->SetBranchAddress("mu_muon_trackcov_z0", &mu_muon_trackcov_z0, &b_mu_muon_trackcov_z0);
    fChain->SetBranchAddress("mu_muon_trackcov_phi", &mu_muon_trackcov_phi, &b_mu_muon_trackcov_phi);
    fChain->SetBranchAddress("mu_muon_trackcov_theta", &mu_muon_trackcov_theta, &b_mu_muon_trackcov_theta);
    fChain->SetBranchAddress("mu_muon_trackcov_qoverp", &mu_muon_trackcov_qoverp, &b_mu_muon_trackcov_qoverp);
    fChain->SetBranchAddress("mu_muon_trackcov_d0_z0", &mu_muon_trackcov_d0_z0, &b_mu_muon_trackcov_d0_z0);
    fChain->SetBranchAddress("mu_muon_trackcov_d0_phi", &mu_muon_trackcov_d0_phi, &b_mu_muon_trackcov_d0_phi);
    fChain->SetBranchAddress("mu_muon_trackcov_d0_theta", &mu_muon_trackcov_d0_theta, &b_mu_muon_trackcov_d0_theta);
    fChain->SetBranchAddress("mu_muon_trackcov_d0_qoverp", &mu_muon_trackcov_d0_qoverp, &b_mu_muon_trackcov_d0_qoverp);
    fChain->SetBranchAddress("mu_muon_trackcov_z0_phi", &mu_muon_trackcov_z0_phi, &b_mu_muon_trackcov_z0_phi);
    fChain->SetBranchAddress("mu_muon_trackcov_z0_theta", &mu_muon_trackcov_z0_theta, &b_mu_muon_trackcov_z0_theta);
    fChain->SetBranchAddress("mu_muon_trackcov_z0_qoverp", &mu_muon_trackcov_z0_qoverp, &b_mu_muon_trackcov_z0_qoverp);
    fChain->SetBranchAddress("mu_muon_trackcov_phi_theta", &mu_muon_trackcov_phi_theta, &b_mu_muon_trackcov_phi_theta);
    fChain->SetBranchAddress("mu_muon_trackcov_phi_qoverp", &mu_muon_trackcov_phi_qoverp, &b_mu_muon_trackcov_phi_qoverp);
    fChain->SetBranchAddress("mu_muon_trackcov_theta_qoverp", &mu_muon_trackcov_theta_qoverp, &b_mu_muon_trackcov_theta_qoverp);
    fChain->SetBranchAddress("mu_muon_trackfitchi2", &mu_muon_trackfitchi2, &b_mu_muon_trackfitchi2);
    fChain->SetBranchAddress("mu_muon_trackfitndof", &mu_muon_trackfitndof, &b_mu_muon_trackfitndof);
    fChain->SetBranchAddress("mu_muon_hastrack", &mu_muon_hastrack, &b_mu_muon_hastrack);
    fChain->SetBranchAddress("mu_muon_trackd0beam", &mu_muon_trackd0beam, &b_mu_muon_trackd0beam);
    fChain->SetBranchAddress("mu_muon_trackz0beam", &mu_muon_trackz0beam, &b_mu_muon_trackz0beam);
    fChain->SetBranchAddress("mu_muon_tracksigd0beam", &mu_muon_tracksigd0beam, &b_mu_muon_tracksigd0beam);
    fChain->SetBranchAddress("mu_muon_tracksigz0beam", &mu_muon_tracksigz0beam, &b_mu_muon_tracksigz0beam);
    fChain->SetBranchAddress("mu_muon_trackd0pv", &mu_muon_trackd0pv, &b_mu_muon_trackd0pv);
    fChain->SetBranchAddress("mu_muon_trackz0pv", &mu_muon_trackz0pv, &b_mu_muon_trackz0pv);
    fChain->SetBranchAddress("mu_muon_tracksigd0pv", &mu_muon_tracksigd0pv, &b_mu_muon_tracksigd0pv);
    fChain->SetBranchAddress("mu_muon_tracksigz0pv", &mu_muon_tracksigz0pv, &b_mu_muon_tracksigz0pv);
    fChain->SetBranchAddress("mu_muon_trackIPEstimate_d0_biasedpvunbiased", &mu_muon_trackIPEstimate_d0_biasedpvunbiased,
                             &b_mu_muon_trackIPEstimate_d0_biasedpvunbiased);
    fChain->SetBranchAddress("mu_muon_trackIPEstimate_z0_biasedpvunbiased", &mu_muon_trackIPEstimate_z0_biasedpvunbiased,
                             &b_mu_muon_trackIPEstimate_z0_biasedpvunbiased);
    fChain->SetBranchAddress("mu_muon_trackIPEstimate_sigd0_biasedpvunbiased", &mu_muon_trackIPEstimate_sigd0_biasedpvunbiased,
                             &b_mu_muon_trackIPEstimate_sigd0_biasedpvunbiased);
    fChain->SetBranchAddress("mu_muon_trackIPEstimate_sigz0_biasedpvunbiased", &mu_muon_trackIPEstimate_sigz0_biasedpvunbiased,
                             &b_mu_muon_trackIPEstimate_sigz0_biasedpvunbiased);
    fChain->SetBranchAddress("mu_muon_trackIPEstimate_d0_unbiasedpvunbiased", &mu_muon_trackIPEstimate_d0_unbiasedpvunbiased,
                             &b_mu_muon_trackIPEstimate_d0_unbiasedpvunbiased);
    fChain->SetBranchAddress("mu_muon_trackIPEstimate_z0_unbiasedpvunbiased", &mu_muon_trackIPEstimate_z0_unbiasedpvunbiased,
                             &b_mu_muon_trackIPEstimate_z0_unbiasedpvunbiased);
    fChain->SetBranchAddress("mu_muon_trackIPEstimate_sigd0_unbiasedpvunbiased",
                             &mu_muon_trackIPEstimate_sigd0_unbiasedpvunbiased,
                             &b_mu_muon_trackIPEstimate_sigd0_unbiasedpvunbiased);
    fChain->SetBranchAddress("mu_muon_trackIPEstimate_sigz0_unbiasedpvunbiased",
                             &mu_muon_trackIPEstimate_sigz0_unbiasedpvunbiased,
                             &b_mu_muon_trackIPEstimate_sigz0_unbiasedpvunbiased);
    fChain->SetBranchAddress("mu_muon_trackd0pvunbiased", &mu_muon_trackd0pvunbiased, &b_mu_muon_trackd0pvunbiased);
    fChain->SetBranchAddress("mu_muon_trackz0pvunbiased", &mu_muon_trackz0pvunbiased, &b_mu_muon_trackz0pvunbiased);
    fChain->SetBranchAddress("mu_muon_tracksigd0pvunbiased", &mu_muon_tracksigd0pvunbiased, &b_mu_muon_tracksigd0pvunbiased);
    fChain->SetBranchAddress("mu_muon_tracksigz0pvunbiased", &mu_muon_tracksigz0pvunbiased, &b_mu_muon_tracksigz0pvunbiased);
    fChain->SetBranchAddress("trk_selec_n", &trk_selec_n, &b_trk_selec_n);
    fChain->SetBranchAddress("trk_selec_pt", &trk_selec_pt, &b_trk_selec_pt);
    fChain->SetBranchAddress("trk_selec_eta", &trk_selec_eta, &b_trk_selec_eta);
    fChain->SetBranchAddress("trk_selec_d0_wrtPV", &trk_selec_d0_wrtPV, &b_trk_selec_d0_wrtPV);
    fChain->SetBranchAddress("trk_selec_z0_wrtPV", &trk_selec_z0_wrtPV, &b_trk_selec_z0_wrtPV);
    fChain->SetBranchAddress("trk_selec_phi_wrtPV", &trk_selec_phi_wrtPV, &b_trk_selec_phi_wrtPV);
    fChain->SetBranchAddress("trk_selec_theta_wrtPV", &trk_selec_theta_wrtPV, &b_trk_selec_theta_wrtPV);
    fChain->SetBranchAddress("trk_selec_qoverp_wrtPV", &trk_selec_qoverp_wrtPV, &b_trk_selec_qoverp_wrtPV);
    fChain->SetBranchAddress("trk_selec_err_d0_wrtPV", &trk_selec_err_d0_wrtPV, &b_trk_selec_err_d0_wrtPV);
    fChain->SetBranchAddress("trk_selec_err_z0_wrtPV", &trk_selec_err_z0_wrtPV, &b_trk_selec_err_z0_wrtPV);
    fChain->SetBranchAddress("trk_selec_err_phi_wrtPV", &trk_selec_err_phi_wrtPV, &b_trk_selec_err_phi_wrtPV);
    fChain->SetBranchAddress("trk_selec_err_theta_wrtPV", &trk_selec_err_theta_wrtPV, &b_trk_selec_err_theta_wrtPV);
    fChain->SetBranchAddress("trk_selec_err_qoverp_wrtPV", &trk_selec_err_qoverp_wrtPV, &b_trk_selec_err_qoverp_wrtPV);
    fChain->SetBranchAddress("trk_selec_chi2", &trk_selec_chi2, &b_trk_selec_chi2);
    fChain->SetBranchAddress("trk_selec_ndof", &trk_selec_ndof, &b_trk_selec_ndof);
    fChain->SetBranchAddress("trk_selec_nBLHits", &trk_selec_nBLHits, &b_trk_selec_nBLHits);
    fChain->SetBranchAddress("trk_selec_nPixHits", &trk_selec_nPixHits, &b_trk_selec_nPixHits);
    fChain->SetBranchAddress("trk_selec_nSCTHits", &trk_selec_nSCTHits, &b_trk_selec_nSCTHits);
    fChain->SetBranchAddress("trk_selec_nTRTHits", &trk_selec_nTRTHits, &b_trk_selec_nTRTHits);
    fChain->SetBranchAddress("trk_selec_nTRTHighTHits", &trk_selec_nTRTHighTHits, &b_trk_selec_nTRTHighTHits);
    fChain->SetBranchAddress("trk_selec_nTRTXenonHits", &trk_selec_nTRTXenonHits, &b_trk_selec_nTRTXenonHits);
    fChain->SetBranchAddress("cl_lc_n", &cl_lc_n, &b_cl_lc_n);
    fChain->SetBranchAddress("cl_lc_pt", &cl_lc_pt, &b_cl_lc_pt);
    fChain->SetBranchAddress("cl_lc_eta", &cl_lc_eta, &b_cl_lc_eta);
    fChain->SetBranchAddress("cl_lc_phi", &cl_lc_phi, &b_cl_lc_phi);
    fChain->SetBranchAddress("cl_lc_firstEdens", &cl_lc_firstEdens, &b_cl_lc_firstEdens);
    fChain->SetBranchAddress("cl_lc_cellmaxfrac", &cl_lc_cellmaxfrac, &b_cl_lc_cellmaxfrac);
    fChain->SetBranchAddress("cl_lc_longitudinal", &cl_lc_longitudinal, &b_cl_lc_longitudinal);
    fChain->SetBranchAddress("cl_lc_secondlambda", &cl_lc_secondlambda, &b_cl_lc_secondlambda);
    fChain->SetBranchAddress("cl_lc_lateral", &cl_lc_lateral, &b_cl_lc_lateral);
    fChain->SetBranchAddress("cl_lc_secondR", &cl_lc_secondR, &b_cl_lc_secondR);
    fChain->SetBranchAddress("cl_lc_centerlambda", &cl_lc_centerlambda, &b_cl_lc_centerlambda);
    fChain->SetBranchAddress("cl_lc_deltaTheta", &cl_lc_deltaTheta, &b_cl_lc_deltaTheta);
    fChain->SetBranchAddress("cl_lc_deltaPhi", &cl_lc_deltaPhi, &b_cl_lc_deltaPhi);
    fChain->SetBranchAddress("cl_lc_centermag", &cl_lc_centermag, &b_cl_lc_centermag);
    fChain->SetBranchAddress("cl_lc_time", &cl_lc_time, &b_cl_lc_time);
    fChain->SetBranchAddress("cl_lc_E_PreSamplerB", &cl_lc_E_PreSamplerB, &b_cl_lc_E_PreSamplerB);
    fChain->SetBranchAddress("cl_lc_E_EMB1", &cl_lc_E_EMB1, &b_cl_lc_E_EMB1);
    fChain->SetBranchAddress("cl_lc_E_EMB2", &cl_lc_E_EMB2, &b_cl_lc_E_EMB2);
    fChain->SetBranchAddress("cl_lc_E_EMB3", &cl_lc_E_EMB3, &b_cl_lc_E_EMB3);
    fChain->SetBranchAddress("cl_lc_E_PreSamplerE", &cl_lc_E_PreSamplerE, &b_cl_lc_E_PreSamplerE);
    fChain->SetBranchAddress("cl_lc_E_EME1", &cl_lc_E_EME1, &b_cl_lc_E_EME1);
    fChain->SetBranchAddress("cl_lc_E_EME2", &cl_lc_E_EME2, &b_cl_lc_E_EME2);
    fChain->SetBranchAddress("cl_lc_E_EME3", &cl_lc_E_EME3, &b_cl_lc_E_EME3);
    fChain->SetBranchAddress("cl_lc_E_HEC0", &cl_lc_E_HEC0, &b_cl_lc_E_HEC0);
    fChain->SetBranchAddress("cl_lc_E_HEC1", &cl_lc_E_HEC1, &b_cl_lc_E_HEC1);
    fChain->SetBranchAddress("cl_lc_E_HEC2", &cl_lc_E_HEC2, &b_cl_lc_E_HEC2);
    fChain->SetBranchAddress("cl_lc_E_HEC3", &cl_lc_E_HEC3, &b_cl_lc_E_HEC3);
    fChain->SetBranchAddress("cl_lc_E_TileBar0", &cl_lc_E_TileBar0, &b_cl_lc_E_TileBar0);
    fChain->SetBranchAddress("cl_lc_E_TileBar1", &cl_lc_E_TileBar1, &b_cl_lc_E_TileBar1);
    fChain->SetBranchAddress("cl_lc_E_TileBar2", &cl_lc_E_TileBar2, &b_cl_lc_E_TileBar2);
    fChain->SetBranchAddress("cl_lc_E_TileGap1", &cl_lc_E_TileGap1, &b_cl_lc_E_TileGap1);
    fChain->SetBranchAddress("cl_lc_E_TileGap2", &cl_lc_E_TileGap2, &b_cl_lc_E_TileGap2);
    fChain->SetBranchAddress("cl_lc_E_TileGap3", &cl_lc_E_TileGap3, &b_cl_lc_E_TileGap3);
    fChain->SetBranchAddress("cl_lc_E_TileExt0", &cl_lc_E_TileExt0, &b_cl_lc_E_TileExt0);
    fChain->SetBranchAddress("cl_lc_E_TileExt1", &cl_lc_E_TileExt1, &b_cl_lc_E_TileExt1);
    fChain->SetBranchAddress("cl_lc_E_TileExt2", &cl_lc_E_TileExt2, &b_cl_lc_E_TileExt2);
    fChain->SetBranchAddress("cl_lc_E_FCAL0", &cl_lc_E_FCAL0, &b_cl_lc_E_FCAL0);
    fChain->SetBranchAddress("cl_lc_E_FCAL1", &cl_lc_E_FCAL1, &b_cl_lc_E_FCAL1);
    fChain->SetBranchAddress("cl_lc_E_FCAL2", &cl_lc_E_FCAL2, &b_cl_lc_E_FCAL2);
    fChain->SetBranchAddress("vxp_n", &vxp_n, &b_vxp_n);
    fChain->SetBranchAddress("vxp_x", &vxp_x, &b_vxp_x);
    fChain->SetBranchAddress("vxp_y", &vxp_y, &b_vxp_y);
    fChain->SetBranchAddress("vxp_z", &vxp_z, &b_vxp_z);
    fChain->SetBranchAddress("vxp_err_x", &vxp_err_x, &b_vxp_err_x);
    fChain->SetBranchAddress("vxp_err_y", &vxp_err_y, &b_vxp_err_y);
    fChain->SetBranchAddress("vxp_err_z", &vxp_err_z, &b_vxp_err_z);
    fChain->SetBranchAddress("vxp_cov_xy", &vxp_cov_xy, &b_vxp_cov_xy);
    fChain->SetBranchAddress("vxp_cov_xz", &vxp_cov_xz, &b_vxp_cov_xz);
    fChain->SetBranchAddress("vxp_cov_yz", &vxp_cov_yz, &b_vxp_cov_yz);
    fChain->SetBranchAddress("vxp_type", &vxp_type, &b_vxp_type);
    fChain->SetBranchAddress("vxp_chi2", &vxp_chi2, &b_vxp_chi2);
    fChain->SetBranchAddress("vxp_ndof", &vxp_ndof, &b_vxp_ndof);
    fChain->SetBranchAddress("vxp_px", &vxp_px, &b_vxp_px);
    fChain->SetBranchAddress("vxp_py", &vxp_py, &b_vxp_py);
    fChain->SetBranchAddress("vxp_pz", &vxp_pz, &b_vxp_pz);
    fChain->SetBranchAddress("vxp_E", &vxp_E, &b_vxp_E);
    fChain->SetBranchAddress("vxp_m", &vxp_m, &b_vxp_m);
    fChain->SetBranchAddress("vxp_nTracks", &vxp_nTracks, &b_vxp_nTracks);
    fChain->SetBranchAddress("vxp_sumPt", &vxp_sumPt, &b_vxp_sumPt);
    fChain->SetBranchAddress("vxp_trk_weight", &vxp_trk_weight, &b_vxp_trk_weight);
    fChain->SetBranchAddress("vxp_trk_n", &vxp_trk_n, &b_vxp_trk_n);
    fChain->SetBranchAddress("vxp_trk_index", &vxp_trk_index, &b_vxp_trk_index);
    fChain->SetBranchAddress("beamSpot_x", &beamSpot_x, &b_beamSpot_x);
    fChain->SetBranchAddress("beamSpot_y", &beamSpot_y, &b_beamSpot_y);
    fChain->SetBranchAddress("beamSpot_z", &beamSpot_z, &b_beamSpot_z);
    fChain->SetBranchAddress("beamSpot_sigma_x", &beamSpot_sigma_x, &b_beamSpot_sigma_x);
    fChain->SetBranchAddress("beamSpot_sigma_y", &beamSpot_sigma_y, &b_beamSpot_sigma_y);
    fChain->SetBranchAddress("beamSpot_sigma_z", &beamSpot_sigma_z, &b_beamSpot_sigma_z);
    fChain->SetBranchAddress("beamSpot_tilt_x", &beamSpot_tilt_x, &b_beamSpot_tilt_x);
    fChain->SetBranchAddress("beamSpot_tilt_y", &beamSpot_tilt_y, &b_beamSpot_tilt_y);
    fChain->SetBranchAddress("Eventshape_rhoKt3EM", &Eventshape_rhoKt3EM, &b_Eventshape_rhoKt3EM);
    fChain->SetBranchAddress("Eventshape_rhoKt4EM", &Eventshape_rhoKt4EM, &b_Eventshape_rhoKt4EM);
    fChain->SetBranchAddress("Eventshape_rhoKt3LC", &Eventshape_rhoKt3LC, &b_Eventshape_rhoKt3LC);
    fChain->SetBranchAddress("Eventshape_rhoKt4LC", &Eventshape_rhoKt4LC, &b_Eventshape_rhoKt4LC);
    Notify();
}

Bool_t physv6::Notify() {
    // The Notify() function is called when a new file is opened. This
    // can be either for a new TTree in a TChain or when when a new TTree
    // is started when using PROOF. It is normally not necessary to make changes
    // to the generated code, but the routine can be extended by the
    // user if needed. The return value is currently not used.

    return kTRUE;
}

void physv6::Show(Long64_t entry) {
    // Print contents of entry.
    // If entry is not specified, print current entry
    if (!fChain) return;
    fChain->Show(entry);
}
Int_t physv6::Cut(Long64_t entry) {
    // This function may be called from Loop.
    // returns  1 if entry is accepted.
    // returns -1 otherwise.
    return 1;
}
#endif  // #ifdef physv6_cxx
