#ifndef _OOextrapolate_h
#define _OOextrapolate_h
float extrapolate(double pt, double eta, double q, double d0, double z0, double phi0, double &phiC, double &etaC);
#endif
