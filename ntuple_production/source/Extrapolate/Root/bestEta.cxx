#include "Extrapolate/bestEta.h"
#include <math.h>

double caloRadius(int sample, const float eta) {
    double radius = 0;

    float aeta = fabs(eta);

    if (aeta < 1.474) {  // barrel
        if (sample == 0) {
            radius = 1422.3;
        } else if (sample == 1) {
            if (aeta < 0.8)
                radius = (1558.859292 - 4.990838 * aeta - 21.144279 * aeta * aeta);
            else
                radius = (1522.775373 + 27.970192 * aeta - 21.104108 * aeta * aeta);
        } else if (sample == 2) {
            radius = (1698.990944 - 49.431767 * aeta - 24.504976 * aeta * aeta);
        } else if (sample == 3) {
            if (aeta < 0.8)
                radius = (1833.88 - 106.25 * aeta);
            else
                radius = (2038.40 - 286. * aeta);
        }
    } else {  // endcap
        if (sample == 0) {
            radius = 3600.;
            if (eta < 0.) radius = -radius;
        } else if (sample == 1) {
            if (aeta < 1.5)
                radius = (12453.297448 - 5735.787116 * aeta);
            else
                radius = 3790.671754;
            if (eta < 0.) radius = -radius;
        } else if (sample == 2) {
            if (aeta < 1.5)
                radius = (8027.574119 - 2717.653528 * aeta);
            else
                radius = (3473.473909 + 453.941515 * aeta - 119.101945 * aeta * aeta);
            if (eta < 0.) radius = -radius;
        } else if (sample == 3) {
            radius = 4150.;
            if (eta < 0.) radius = -radius;
        }

        radius /= sinh(eta);
    }
    return radius;
}

float eta_PVcorr(int sample, float etas, float Zpv) {
    double R_photon_front;
    double Z_photon_front;
    R_photon_front = caloRadius(sample, etas);
    Z_photon_front = R_photon_front * sinh(etas);

    return asinh((Z_photon_front - Zpv) / R_photon_front);
}

float bestEta(float etas1, float etaHPV, float vx_z, int author, int nSingleTrackConv, int nDoubleTrackConv, int convtrk1nPixHits,
              int convtrk1nSCTHits, int convtrk2nPixHits, int convtrk2nSCTHits) {
    if (author == 4 || (author == 16 && nDoubleTrackConv == 1 && convtrk1nPixHits == 0 && convtrk1nSCTHits == 0 &&
                        convtrk2nPixHits == 0 && convtrk2nSCTHits == 0) ||
        (author == 16 && nSingleTrackConv == 1 && convtrk1nPixHits == 0 && convtrk1nSCTHits == 0) ||
        (author == 16 && nSingleTrackConv == 0 && nDoubleTrackConv == 0)) {
        return eta_PVcorr(1, etas1, vx_z);
    } else {
        return etaHPV;
    }
}

void boosts(const TLorentzVector& muO, const TLorentzVector& phelO, const TVector3& MET, double& costhetaTauMu,
            double& costhetaTauPh, double& boostedTauM, double& boostedWM, double& dphiW) {
    double tauRF_LBoost_E, tauRF_LBoost_m, tauRF_LBoost_pt, tauRF_LBoost_eta, tauRF_LBoost_phi;
    double tauRF_TBoost_E, tauRF_TBoost_m, tauRF_TBoost_pt, tauRF_TBoost_eta, tauRF_TBoost_phi;
    double tauRF_FBoost_E, tauRF_FBoost_m, tauRF_FBoost_pt, tauRF_FBoost_eta, tauRF_FBoost_phi;

    double muRF_LBoost_E, muRF_LBoost_m, muRF_LBoost_pt, muRF_LBoost_eta, muRF_LBoost_phi;
    double muRF_TBoost_E, muRF_TBoost_m, muRF_TBoost_pt, muRF_TBoost_eta, muRF_TBoost_phi;
    double muRF_FBoost_E, muRF_FBoost_m, muRF_FBoost_pt, muRF_FBoost_eta, muRF_FBoost_phi;

    double phRF_LBoost_E, phRF_LBoost_m, phRF_LBoost_pt, phRF_LBoost_eta, phRF_LBoost_phi;
    double phRF_TBoost_E, phRF_TBoost_m, phRF_TBoost_pt, phRF_TBoost_eta, phRF_TBoost_phi;
    double phRF_FBoost_E, phRF_FBoost_m, phRF_FBoost_pt, phRF_FBoost_eta, phRF_FBoost_phi;

    TLorentzVector mu(muO);
    TLorentzVector phel(phelO);

    TLorentzVector tau = mu + phel;  // ############ Redefine tau

    // step one: longitudinal boost
    TVector3 vBETA_L = (1. / tau.E()) * tau.Vect();
    vBETA_L.SetX(0.0);
    vBETA_L.SetY(0.0);
    // do the boost
    mu.Boost(-vBETA_L);
    phel.Boost(-vBETA_L);
    tau.Boost(-vBETA_L);

    //*********************
    tauRF_LBoost_E = tau.E();
    tauRF_LBoost_m = tau.M();
    tauRF_LBoost_pt = tau.Pt();
    tauRF_LBoost_eta = tau.Eta();
    tauRF_LBoost_phi = tau.Phi();

    muRF_LBoost_E = mu.E();
    muRF_LBoost_m = mu.M();
    muRF_LBoost_pt = mu.Pt();
    muRF_LBoost_eta = mu.Eta();
    muRF_LBoost_phi = mu.Phi();

    phRF_LBoost_E = phel.E();
    phRF_LBoost_m = phel.M();
    phRF_LBoost_pt = phel.Pt();
    phRF_LBoost_eta = phel.Eta();
    phRF_LBoost_phi = phel.Phi();
    //*********************

    // step two: do transverse boost from lab frame to W rest frame
    boostedWM = sqrt(2. * MET.Mag() * tau.E() - 2. * MET.Dot(tau.Vect()) + tau.M2());
    TVector3 vBETA_T = (1. / (MET.Mag() + tau.E())) * (MET + tau.Vect());
    // do the boost

    // cout << "MW is " << MW << endl;
    // cout << "MET.Mag is " << MET.Mag() << endl;
    // cout << "Tau Mag is " << tau.M2() << endl;

    mu.Boost(-vBETA_T);
    phel.Boost(-vBETA_T);
    tau.Boost(-vBETA_T);

    //*********************
    tauRF_TBoost_E = tau.E();
    tauRF_TBoost_m = tau.M();
    tauRF_TBoost_pt = tau.Pt();
    tauRF_TBoost_eta = tau.Eta();
    tauRF_TBoost_phi = tau.Phi();

    muRF_TBoost_E = mu.E();
    muRF_TBoost_m = mu.M();
    muRF_TBoost_pt = mu.Pt();
    muRF_TBoost_eta = mu.Eta();
    muRF_TBoost_phi = mu.Phi();

    phRF_TBoost_E = phel.E();
    phRF_TBoost_m = phel.M();
    phRF_TBoost_pt = phel.Pt();
    phRF_TBoost_eta = phel.Eta();
    phRF_TBoost_phi = phel.Phi();
    //*********************
    // Now in W rest frame
    double costhetaW = tau.Vect().Unit().Dot(vBETA_T.Unit());
    dphiW = tau.Vect().DeltaPhi(vBETA_T.Unit());

    // Now we want to go to tau rest frame
    TVector3 vBETA_WtoTau = (1. / tau.E()) * tau.Vect();

    mu.Boost(-vBETA_WtoTau);
    phel.Boost(-vBETA_WtoTau);
    tau.Boost(-vBETA_WtoTau);

    //*********************
    tauRF_FBoost_E = tau.E();
    tauRF_FBoost_m = tau.M();
    boostedTauM = tauRF_FBoost_m;
    tauRF_FBoost_pt = tau.Pt();
    tauRF_FBoost_eta = tau.Eta();
    tauRF_FBoost_phi = tau.Phi();

    muRF_FBoost_E = mu.E();
    muRF_FBoost_m = mu.M();
    muRF_FBoost_pt = mu.Pt();
    muRF_FBoost_eta = mu.Eta();
    muRF_FBoost_phi = mu.Phi();

    phRF_FBoost_E = phel.E();
    phRF_FBoost_m = phel.M();
    phRF_FBoost_pt = phel.Pt();

    phRF_FBoost_eta = phel.Eta();
    phRF_FBoost_phi = phel.Phi();
    //*********************

    // cout << tau.P() << endl; //should be zero

    // calculate tau decay angel:
    costhetaTauMu = mu.Vect().Unit().Dot(vBETA_WtoTau.Unit());
    costhetaTauPh = phel.Vect().Unit().Dot(vBETA_WtoTau.Unit());
    // double costhetaTauDoubl = mu.Vect().Unit().Dot(vBETA_WtoTau.Unit());
}
