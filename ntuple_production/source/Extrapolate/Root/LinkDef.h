#include "Extrapolate/extrapolate.h"
#include "Extrapolate/physv6.h"
#include "Extrapolate/bestEta.h"
#include "Extrapolate/Track.h"
#include "Extrapolate/Vertex.h"

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ function extrapolate(double, double, double, double, double, double, double&, double&);
#pragma link C++ class physv6 + ;
#pragma link C++ function caloRadius(int, const float);
#pragma link C++ function eta_PVcorr(int, float, float);
#pragma link C++ function bestEta(float, float, float, int, int, int, int, int, int, int);
#pragma link C++ function boosts(const TLorentzVector&, const TLorentzVector&, const TVector3&, float&, float&, float&, float&, \
                                 float&);
#pragma link C++ class Track + ;
#pragma link C++ class Vertex + ;

#endif
