#include "Extrapolate/Util.h"

#include <cmath>

long double deltaPhi(const long double phi1, const long double phi2) {
    long double deltaphi = phi2 - phi1;
    if (deltaphi > M_PI)
        deltaphi -= 2 * M_PI;
    else if (deltaphi < -M_PI)
        deltaphi += 2 * M_PI;

    return deltaphi;
}

std::pair<TVectorD, TMatrixD> positionToHelix(const TVectorD &X, double q) {
    // see (36) in physics/0503191v1

    // retrieve the original representation
    const long double x = X[0];
    const long double y = X[1];
    const long double z = X[2];
    const long double px = X[3];
    const long double py = X[4];
    const long double pz = X[5];

    // a * q, where 'a' is a magic constant
    const long double aq = 0.6 * q;

    // transverse momentum
    const long double pt2 = px * px + py * py;
    const long double pt = std::sqrt(pt2);

    // total momentum
    const long double p2 = pt2 + pz * pz;
    const long double p = std::sqrt(p2);
    const long double p3 = p2 * p;

    // definitions of p_{x0} and p_{y0}
    const long double px0 = px + aq * y;
    const long double py0 = py - aq * x;

    // definition of p_{T0}
    const long double pt02 = px0 * px0 + py0 * py0;
    const long double pt0 = std::sqrt(pt02);

    // angles
    const long double phi = std::atan2(py, px);
    const long double phi0 = std::atan2(py0, px0);
    const long double deltaphi = deltaPhi(phi0, phi);

    const long double l = deltaphi * pt / aq;
    const long double d0 = (pt0 - pt) / aq;
    const long double z0 = z - l * pz / pt;  // was wrong!!!
    const long double qoverp = q / p;
    const long double theta = std::atan2(pt, pz);

    // Now build h(x)
    TVectorD h1(5);
    h1[0] = d0;
    h1[1] = z0;
    h1[2] = phi0;
    h1[3] = theta;
    h1[4] = qoverp;

    // And the matrix H^T used for the covariance matrix
    // We build 2 matrices A and B and build a block-diagonal matrix H out of them that we then transpose
    // See (37) of the same paper

    TMatrixD HT_tmp;
    HT_tmp.ResizeTo(6, 5);

    TMatrixD A;
    A.ResizeTo(3, 5);

    TMatrixD B;
    B.ResizeTo(3, 5);

    // the row with dx
    A[0][0] = -py0 / pt0;
    A[0][1] = -pz * px0 / pt02;
    A[0][2] = -aq * px0 / pt02;
    A[0][3] = 0;
    A[0][4] = 0;

    // the row with dy
    A[1][0] = px0 / pt0;
    A[1][1] = -pz * py0 / pt02;
    A[1][2] = -aq * py0 / pt02;
    A[1][3] = 0;
    A[1][4] = 0;

    // the row with dz
    A[2][0] = 0;
    A[2][1] = 1;
    A[2][2] = 0;
    A[2][3] = 0;
    A[2][4] = 0;

    // the row with px
    B[0][0] = (px0 / pt0 - px / pt) / aq;
    B[0][1] = -pz * (py0 / pt02 - py / pt2) / aq;
    B[0][2] = -py0 / pt02;
    B[0][3] = pz * px / (p2 * pt);  // different from wouter -> check again
    B[0][4] = -q * px / p3;         // different from wouter
    // the row with py
    B[1][0] = (py0 / pt0 - py / pt) / aq;
    B[1][1] = pz * (px0 / pt02 - px / pt2) / aq;
    B[1][2] = px0 / pt02;
    B[1][3] = pz * py / (p2 * pt);  // different from wouter
    B[1][4] = -q * py / p3;         // different from wouter
    // the row with pz
    B[2][0] = 0;
    B[2][1] = -l / pt;
    B[2][2] = 0;
    B[2][3] = -pt / p2;  // different from wouter
    B[2][4] = -q * pz / p3;

    HT_tmp.SetSub(0, 0, A);
    HT_tmp.SetSub(3, 0, B);

    return std::make_pair(h1, HT_tmp);
}
