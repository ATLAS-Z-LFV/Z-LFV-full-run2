#include "TMath.h"
/*
     double muonEtaCalo, muonPhiCalo;
     extrapolateToCalo(mu_muon_pt->at(0), mu_muon_eta->at(0), mu_muon_charge->at(0),
                       mu_muon_d0_exPV->at(0), mu_muon_z0_exPV->at(0), mu_muon_phi->at(0), // need to fix d0, z0 here
                       muonPhiCalo, muonEtaCalo);
     if( fabs(phel_etas2->at(0)-mu_muon_eta->at(0))<0.06 && fabs(phel_phis2->at(0)-muonPhiCalo)<0.01 )
       continue;


 */

float extrapolate(double pt, double eta, double q, double d0, double z0, double phi0, double &phiC, double &etaC) {
    /*----------------------------------------------------------------------*
     * Author : John Baines                        Date : 12/11/97          *
     *          from an original concept by Simon George                    *
     *----------------------------------------------------------------------*/
    static const double BFIELD = 2.0;
    double Rcurv, r, z, xD, xN, dphi;

    static const double rC = 1470.;
    static const double zC = 3800.;
    static const float pi = TMath::Pi();
    static const float pi_2 = TMath::PiOver2();

    double theta = 2. * atan(exp(-fabs(eta)));
    double pz = fabs(pt) / tan(theta);
    if (eta < 0) pz = -pz;
    phiC = 0;
    etaC = 0;
    if (rC > 0 && fabs(zC) > 0 && pt != 0 && fabs(z0) < zC) {
        /* r, z of intersection with cylinder */

        z = z0 + pz * (rC - fabs(d0)) / fabs(pt);
        if (fabs(z) > fabs(zC)) {
            /* track passes through the end of the cylinder */
            if (z > 0)
                z = fabs(zC);
            else
                z = -fabs(zC);
            r = fabs(d0) + (z - z0) * fabs(pt) / pz;
            r = fabs(r); /* NOT SURE IF -ve r is handled correctly */
        } else
            r = rC; /* hits barrel of cylinder */

        theta = atan2(r, fabs(z)) / 2.;
        if (z > 0)
            etaC = -log(tan(theta));
        else
            etaC = log(tan(theta));

        /* Now calculate phiC */

        if (q == 0) { /* Neutral Track */
            if (fabs(d0) <= r) phiC = phi0 - asin(fabs(d0) / r);
        } else { /* Charged track */
            Rcurv = fabs(pt) / (0.3 * BFIELD);
            xD = 2. * r * (Rcurv + d0);
            if (xD != 0) {
                xN = d0 * d0 + r * r + 2. * Rcurv * d0;
                if (fabs(xN) / fabs(xD) > 1) {
                    /* trajectory does not reach r, track loops
                   ***temporary set dphi to 0 for this case */
                    phiC = 2. * M_PI;
                } else {
                    dphi = q * acos(xN / xD);
                    /* two solutions from the 2 intersections of the 2 circles,
                       but phi0 defines a direction a direction, so the first solution
                       should be the one we want....*/
                    phiC = phi0 - q * pi_2 + dphi;
                }
            } else
                phiC = 0; /* means track is orbiting the origin */
        }
        /* fold back into -pi to pi */
        while (phiC > pi) phiC = phiC - 2. * pi;
        while (phiC < -pi) phiC = phiC + 2. * pi;
    }
    // cout << "EXTR " << phiC << " " << etaC << endl;
    return 0;
}
