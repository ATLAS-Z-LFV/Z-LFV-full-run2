# Ntuple production

This directory contains all the codes needed to produce ntuples from xAODs/DAODs for the analysis.

<BR>

## Setup

To set up for the first time, simply do:
```
source setup.sh
```

The setup should be done fully automatically if you are on Nikhef Stoomboot or using a machine with cvmfs (e.g. lxplus).

After the first-time setup, if the codes do not need updates, simply do:
```
source setup_fast.sh
```
each time you log in to the machine.

Blame Terry (ws.chan@cern.ch) if the setup scripts don't work.

<BR>

## Running locally

To run the code locally, you would want to have the the xAODs/DAODs located on a local disk.

Create a text file with the paths to the xAODs/DAODs on separate lines (similar to "ntuple_production/source/LFV_Z/scripts/testfiles-Ztautau.txt"), and provide it to the script by doing, e.g.:
```
cd run  # just a symlink to source/LFV_Z/scripts
./run.py --noSystematics --localPath testfiles-Ztautau.txt
```

For more options, see:
```
./run.py -h
```

<BR>

## Running on Grid

To run the code on Grid, you would have to provide lists of xAODs/DAODs to run on. You can find the lists in source/LFV_Z/data/datasets.

Example commands:
```
lsetup panda
cd run  # just a symlink to source/LFV_Z/scripts
./run.py --useGrid --destSE CERN-PROD_SCRATCHDISK --outputSuffix v1.0 --submitDir ../../../submits/Ztt --inputDSFile ../data/datasets/Ztautau_incl_MC16c.txt
./run.py --useGrid --destSE CERN-PROD_SCRATCHDISK --outputSuffix v1.0 --submitDir ../../../submits/data --inputDSFile ../data/datasets/data16.txt --data
```

<BR>

## Merging ntuples

It is necessary to merge ntuples into one file (even if there is only one ntuple to "merge") in order to compute the correct normalisation weights.

Example commands:
```
cd merge
./mergeNtuples.py -i /dcache/atlas/wschan/Z_LFV_ntuples/v24/pre_merge -o /dcache/atlas/wschan/Z_LFV_ntuples/v24/Ztautau.root -p Ztautau --prefix Ztautau --dry-run
# Check the dry-run printouts before actually merging
unbuffer ./mergeNtuples.py -i /dcache/atlas/wschan/Z_LFV_ntuples/v24/pre_merge -o /dcache/atlas/wschan/Z_LFV_ntuples/v24/Ztautau.root -p Ztautau --prefix Ztautau 2>&1 | tee merge_Ztautau.log
```
