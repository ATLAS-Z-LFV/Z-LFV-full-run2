#!/usr/bin/env python

import os, sys, argparse
import fnmatch
from glob import glob
from copy import copy
from collections import OrderedDict, defaultdict

import ROOT
ROOT.gROOT.SetBatch()

#from rootpy.io import root_open
#from rootpy.tree import Tree, TreeBuffer
### Terry: Somehow rootpy doesn't work happily in LCG gcc62 versions...(?)
### Perhaps there are ways, but these workarounds are also good enough:

class root_open(ROOT.TFile):
    def __enter__(self):
        return self
    def __exit__(self, type, value, traceback):
        if self and self.IsOpen():
            self.Close()

from array import array
class buffer(object):
    # Convert 'TTree' type codes to 'array' type codes
    convert = {'F':'f', # float
               'D':'d', # double
               'I':'l', # 32-bit signed int
               'i':'L', # 32-bit unsigned int
              }
    def __init__(self, type_code):
        self.address = array(buffer.convert[type_code], [0])
    def set_value(self, value):
        self.address[0] = value

class PyTTree(ROOT.TTree):
    def __init__(self, name, title=""):
        super(PyTTree, self).__init__(name, title)
        super(PyTTree, self).__setattr__('buffers', {})
    def __setattr__(self, name, value):
        self.buffers[name].set_value(value)
    def fill(self):
        self.Fill()
    def write(self):
        self.Write()
    def create_branches(self, branches):
        for name,type_code in branches.iteritems():
            self.buffers[name] = buffer(type_code)
            self.Branch(name, self.buffers[name].address, "{}/{}".format(name, type_code))

def setRootVerbosityLevel(level="Error"):
    if isinstance(level, str):
        ROOT.gROOT.ProcessLine("{gErrorIgnoreLevel = k%s;}" % level)
    elif isinstance(level, int):
        ROOT.gROOT.ProcessLine("{gErrorIgnoreLevel = %d;}" % level)
    else:
        print "WARNING: Don't know how to set ROOT verbosity level to '{}'".format(level)

class rootVerbosityLevel(object):
    def __init__(self, level):
        self.level = level
        x = ROOT.gROOT.GetGlobal("gErrorIgnoreLevel")
        self.original_level = x.__get__(x)
    def __enter__(self):
        setRootVerbosityLevel(self.level)
    def __exit__(self, type, value, traceback):
        setRootVerbosityLevel(self.original_level)


#------------------------------------------------------------------------------
def parseArguments(argv):
    parser = argparse.ArgumentParser("./mergeNtuples.py")

    input_args = parser.add_argument_group('Input')
    input_args.add_argument("--input-dir", "-i", dest="input_dirs", type=str, default=[], action="append", help="Input directory to scan for datasets.")

    output_args = parser.add_argument_group('Output')
    parser.add_argument("--output-file", "-o", type=str, default=None, help="Output filename.")
    parser.add_argument("--prefix", type=str, default=None, help="Force a prefix for the output trees.")

    dir_args = parser.add_argument_group('Input directory scan options')
    dir_args.add_argument("--scan-pattern", type=str, default="user.*.LFV*ntuple.root", help="Scan pattern to use when scanning directories (wildcard='*').")
    dir_args.add_argument("--ignore-pattern", dest="ignore_patterns", type=str, default=[], action="append", help="Ignore datasets matching this pattern (wildcard='*').")
    dir_args.add_argument("--process-pattern", "-p", dest="process_patterns", type=str, default=[], action="append", help="Process pattern to match against, e.g. 'Wenu' (wildcard='*'). Note that datasets only have to match any one of the --process-pattern(s) or --DSID(s).")
    dir_args.add_argument("--DSID", dest="DSIDs", type=str, default=[], action="append", help="Comma-separated list of DSIDs/DISD ranges to match against, e.g. '361106,361107,364137-364141'. Note that datasets only have to match any one of the --process-pattern(s) or --DSID(s).")

    file_args = parser.add_argument_group('Input file scan options')
    file_args.add_argument("--file-pattern", default="*", type=str, help="only files that match the pattern are merged.")

    tree_args = parser.add_argument_group('Input tree scan options')
    tree_args.add_argument("--nominal-only", default=False, action="store_true", help="Merge only the nominal trees.")
    tree_args.add_argument("--tree-pattern", default="*", type=str, help="only trees that match the pattern are merged.")

    merge_args = parser.add_argument_group('Merge options')
    merge_args.add_argument("--MC-campaign", type=str, default='comb', choices=['comb','16a','16d','16e'], help="MC campaign. Choose among '16a', '16d', '16e' and 'comb'.")
    merge_args.add_argument("--shared-norm", default=False, action="store_true", help="Calculate shared normalisation weight for multiple MC campaign combined. Use this if the ntuples were produced with option `--MC-campaign comb`")
    merge_args.add_argument("--data", default=False, action="store_true", help="Merging data instead of MCs?")
    merge_args.add_argument("--signal", default=False, action="store_true", help="Merging signal instead of background MCs?")
    merge_args.add_argument("--ignore-branches", type=str, default="theory_weights_LHE*", help="A comma-separated list of branches to be ignored.")
    merge_args.add_argument("--ignore-merge-warnings", default=False, action="store_true", help="Ignore merging warnings.")
    merge_args.add_argument("--multiple-revisions", default=False, action="store_true", help="Allow merging files submitted with multiple revisions. Use at your own risk!")
    merge_args.add_argument("--xsecfile", type=str, default="susy_xsec_13TeV.txt", help="Cross section file to use. If empty, use the PMG default cross section tool.")

    other_args = parser.add_argument_group('Other options')
    other_args.add_argument("--dry-run", "-D", default=False, action="store_true", help="Dry run. Print the datasets that will be merged and exit before actually merging.")

    try:
        args = parser.parse_args(argv)

        if args.ignore_branches:
            args.ignore_branches = args.ignore_branches.split(',')

        checkArguments(args)

    except:
        parser.print_help()
        raise

    return args

#------------------------------------------------------------------------------
def checkArguments(args):
    if not args.input_dirs:
        raise Exception("No input directories provided!")
    else:
        for d in args.input_dirs:
            if os.path.exists(d) and os.path.isdir(d):
                continue
            raise Exception("Input directory {} does not exist!".format(d))

    if not args.output_file:
        raise Exception("No output file name specified!")
    elif os.path.dirname(args.output_file) and not os.path.exists(os.path.dirname(args.output_file)):
        os.makedirs(os.path.dirname(args.output_file))

    if not args.output_file.endswith(".root"):
        args.output_file += ".root"

#------------------------------------------------------------------------------
def getDSIDList(DSIDs):
    DSID_list = set()

    # DSIDs is a list of strings
    for string in DSIDs:
        # e.g. string = "361106,361107,364137-364141"
        ranges = string.split(',')
        for r in ranges:
            r = r.split('-')
            if len(r)==1 and r[0].isdigit():
                DSID_list.add(int(r[0]))
            elif len(r)==2 and r[0].isdigit() and r[1].isdigit():
                min = int(r[0])
                max = int(r[1])
                if min > max: min, max = max, min
                DSID_list = DSID_list.union(range(min, max+1))
            else:
                raise Exception("Don't know how to interpret '{}'".format(string))

    return DSID_list

#------------------------------------------------------------------------------
def relativeNormWeight(MC_campaign):
    if MC_campaign == "16a":
        return 0.2605918  # 36.21496 / 138.972
    if MC_campaign == "16d":
        return 0.3188225  # 44.3074 / 138.972
    if MC_campaign == "16e":
        return 0.4205890  # 58.4501 / 138.972
    return 1.

#------------------------------------------------------------------------------
def MCCampaign(dataset_name):
    if "r9364" in dataset_name or "data15" in dataset_name or "data16" in dataset_name:
        return "16a"
    elif "r10201" in dataset_name or "r10239" in dataset_name or "data17" in dataset_name:
        return "16d"
    elif "r10724" in dataset_name or "data18" in dataset_name:
        return "16e"
    elif "COMB" in dataset_name:
        return 'COMB'
    else:
        raise Exception("Unknown MC campaign for '{}'".format(dataset_name))

#------------------------------------------------------------------------------
def prepareCombinedDatasets(args):
    _args = copy(args)
    _args.dry_run = False  # Save original option

    datasets = scanDirectories(_args)

    incomplete = False
    for DSID,MC_campaigns in datasets.iteritems():
        if not "16a" in MC_campaigns:
            print "FATAL: DSID '{}' does not have MC16a samples.".format(DSID)
            incomplete = True
        if not "16d" in MC_campaigns:
            print "FATAL: DSID '{}' does not have MC16d samples.".format(DSID)
            incomplete = True
        if not "16e" in MC_campaigns:
            print "FATAL: DSID '{}' does not have MC16e samples.".format(DSID)
            incomplete = True
    if incomplete:
        raise Exception("You specified 'combined' MC campaign, but I can't find every datasets in all of MC16a, MC16d and MC16e.")

    for DSID in datasets:
        dir_16a, basename = os.path.split(datasets[DSID]["16a"])
        split = basename.split('.')
        split[5] = "COMB"  # Replace the AMI tags by "COMB"
        new_dir = os.path.join(dir_16a, '.'.join(split))
        if not os.path.exists(new_dir):
            os.makedirs(new_dir)

        for f in os.listdir(datasets[DSID]["16a"]):
            f_path = os.path.join(datasets[DSID]["16a"], f)
            src = os.path.relpath(f_path, new_dir)
            link = os.path.join(new_dir, f)
            if not os.path.exists(link):
                os.symlink(src, link)

        for f in os.listdir(datasets[DSID]["16d"]):
            f_path = os.path.join(datasets[DSID]["16d"], f)
            src = os.path.relpath(f_path, new_dir)
            link = os.path.join(new_dir, f)
            if not os.path.exists(link):
                os.symlink(src, link)

        for f in os.listdir(datasets[DSID]["16e"]):
            f_path = os.path.join(datasets[DSID]["16e"], f)
            src = os.path.relpath(f_path, new_dir)
            link = os.path.join(new_dir, f)
            if not os.path.exists(link):
                os.symlink(src, link)

    print "Created combined datasets with special tag 'COMB'."
    return

#------------------------------------------------------------------------------
def scanDirectories(args):
    datasets = defaultdict(dict)

    DSID_pos = 3 + args.data
    warnings = set()  # Warn user if there are multiple directories for the same DSID
    #warning_paths = []  # TChain does stupid things when the path contains ".root"

    for dir in args.input_dirs:
        # Must match scan_pattern
        scanned_list = set(glob(os.path.join(dir, args.scan_pattern)))
        #if not args.data:
        #    MC_campaign_pattern = "*{}*".format(MCTag(args.MC_campaign))
        #    scanned_list &= set(glob(os.path.join(dir, MC_campaign_pattern)))

        # Must match ANY one of the process_patterns or DSIDs
        process_list = set()
        for process in args.process_patterns:
            pattern = "*{}*".format(process)
            process_list |= set(glob(os.path.join(dir, pattern)))
        DSID_pattern = "*.00{}.*" if args.data else "*.{}.*"
        for DSID in getDSIDList(args.DSIDs):
            pattern = DSID_pattern.format(DSID)
            process_list |= set(glob(os.path.join(dir, pattern)))

        # Must NOT match ANY of the ignore_patterns
        ignore_list = set()
        for pattern in args.ignore_patterns:
            pattern = "*{}*".format(pattern)
            ignore_list |= set(glob(os.path.join(dir, pattern)))

        process_list = (scanned_list & process_list) - ignore_list

        for path in process_list:
            basename = os.path.basename(path)
            DSID = basename.split(".")[DSID_pos]
            assert DSID.isdigit()
            MC_campaign = MCCampaign(basename)
            if MC_campaign in datasets[DSID]:
                warnings.add((DSID, MC_campaign))
            datasets[DSID][MC_campaign] = path

    #for DSID, path in datasets.iteritems():
    #    if not ".root" in path:
    #        continue
    #    warning_paths.append(path)
    #    os.rename(path, path.replace(".root", "_root"))
    #    datasets[DSID] = path.replace(".root", "_root")
    #
    #if warning_paths:
    #    print "WARNING: The following paths contain '.root':"
    #    for path in warning_paths:
    #        print "  " + path
    #    print "WARNING: Renamed them with '.root' -> '_root' to avoid troubles"

    if warnings:
        print "WARNING: Found multiple datasets for the following DSIDs:"
        for DSID,MC_campaign in warnings:
            print "  {}({})".format(DSID, MC_campaign)
        print "WARNING: Please make sure the following list make sense! (Look for the '[WARNING]' signs)"

    if len(datasets) == 0:
        print "Didn't find any datasets."
    else:
        print "Found the following datasets:"
        for DSID in datasets:
            for MC_campaign,path in datasets[DSID].iteritems():
                if args.MC_campaign=="comb" and args.shared_norm ^ (MC_campaign=="COMB"):
                    continue
                if not args.MC_campaign=="comb" and not MC_campaign==args.MC_campaign:
                    continue
                if (DSID,MC_campaign) in warnings:
                    print "  {}({}): [WARNING] {}".format(DSID, MC_campaign, path)
                else:
                    print "  {}({}): {}".format(DSID, MC_campaign, path)

    if args.dry_run:
        sys.exit(0)

    return datasets

#------------------------------------------------------------------------------
def isZombie(file_name):
    is_zombie = False
    with root_open(file_name, "READ") as f:
        is_zombie = f.IsZombie() or f.TestBit(ROOT.TFile.kRecovered)
    return is_zombie

#------------------------------------------------------------------------------
def checkMetadata(file_name, metadata):
    with root_open(file_name, "READ") as f:
        for attr in metadata:
            if hasattr(f, attr):
                val = getattr(f, attr)
                if not isinstance(val, str): val = val.value
                metadata[attr].add(val)
            elif attr == "submitted_revision":
                if args.multiple_revisions:
                    metadata[attr].add("UNKNOWN")
                    continue
                print "    ERROR: Cannot check revision! This is unsafe, aborting..."
                sys.exit(8)

    # Check if all files have the same revision!
    if len(metadata["submitted_revision"]) > 1 and not args.multiple_revisions:
        print "    ERROR: Found multiple revisions: {}! This is unsafe, aborting...".format(metadata["submitted_revision"])
        sys.exit(9)

    return

#------------------------------------------------------------------------------
def writeMetadata(file, metadata):
    with root_open(file, "UPDATE") as f:
        for attr in metadata:
            joined = ','.join(metadata[attr])
            print "Writing metadata '{}' : '{}'".format(attr, joined)
            f.WriteObject(ROOT.SH.MetaData(ROOT.std.string)(attr, joined), attr)
    return

#------------------------------------------------------------------------------
def getFilenames(dir, metadata=None):
    file_names = []

    if args.file_pattern == "" or args.file_pattern == "*":
        listdir = os.listdir(dir)
    else:
        listdir = glob(os.path.join(dir, args.file_pattern))

    for f in listdir:
        f = os.path.join(dir, f)

        if os.path.basename(f) == "merged.root":
            # this is likely a locally merged file already!!
            continue

        s = f.split('.')
        if s[-2]=="root" and s[-1].isdigit():
            # Somehow ROOT doesn't like file names that end with ".root.2" or something like that...
            # So let's try to rename it
            s = s[:-1]
            new_f = '.'.join(s)
            if os.path.exists(new_f):
                if os.path.samefile(f, new_f):
                    # At least one of them must be a symlink
                    if os.path.islink(f):
                        os.remove(f)
                        f = new_f
                    else:
                        assert os.path.islink(new_f)
                        os.remove(new_f)
                else:
                    raise Exception("Found files '{}' and '{}'. Not sure if I should merge them. Better safe than sorry - aborting...".format(new_f, f))
            else:
                print "    WARNING: Renaming '{}' to '{}'".format(f, new_f)
                os.rename(f, new_f)
                f = new_f
        if not s[-1]=="root":
            continue

        if isZombie(f):
            print "    WARNING: Skipping bad file {}".format(f)
            continue

        if metadata is not None:
            checkMetadata(f, metadata)

        file_names.append(f)

    return file_names

#------------------------------------------------------------------------------
def getSignalCrossSection(DSID):
    DS_eff = {} # dictionary of efficiencies of the generator filter (from AMI)
    DS_eff[303778] = 6.9251E-01 # Ztaumu
    DS_eff[303779] = 6.8658E-01 # Ztaue
    DS_eff[307503] = 6.6756E-01 # Ztaumugamma
    DS_eff[307504] = 6.6176E-01 # Ztauegamma

    DS_BR = {} # dictionary of branching ratios
    DS_BR[303778] = 1.0e-05
    DS_BR[303779] = 1.0e-05
    DS_BR[307503] = 1.0e-05
    DS_BR[307504] = 1.0e-05

    if not DSID in DS_eff:
        return (0.0, 0)

    xsec = 1970. / 0.033658 # Total Z xsec = xsec(Z->ll) / BR(Z->ll)
    xsec *= DS_BR[DSID] # braching ratio
    xsec *= DS_eff[DSID] # efficiency of the generator filter

    rel_uncert = 0.0 # TODO: to be implemented??

    return (xsec, rel_uncert)

#------------------------------------------------------------------------------
def processFile(filename):
    print "    Processing {0}".format(os.path.basename(filename))

    if not os.path.isfile(filename):
        print "    ERROR: {0} is not a file".format(os.path.basename(filename))
        return None

    with root_open(filename, "READ") as f:
        h_metadata = f.Get("metadata")

        if not h_metadata:
            print "    ERROR: can't find metadata in {}".format(os.path.basename(filename))
            return None

        N_processed = int(h_metadata.GetBinContent(2))
        sumW = h_metadata.GetBinContent(8) # this is m_xAOD_initialSumOfWeights in EventSelection.cxx

        h = f.Get("EventLoop_EventCount")

        ### Since the memory usage fix (v31.1.3),
        ### EventLoop_EventCount is in another output stream
        ### But from experience, we can quite safely skip the sanity check
        #if not h:
        #    print "    WARNING: can't find EventLoop_EventCount in {0}".format(filename)
        #else:
        #    N = int(h.GetBinContent(1))
        #    if N != N_processed:
        #        print "    WARNING: metadata and EventLoop_EventCount disagree on how many events we saw!"
        #        time.sleep(1)

        print "      Found {0:d} events in metadata; sumWeights = {1:.2f}".format(N_processed, sumW)

    return (sumW, N_processed)

#------------------------------------------------------------------------------
def processDataset(args, xsecDBs, DSID, path, metadata):
    if not args.signal and int(DSID) in [303778, 303779, 307503, 307504]:
        print "  WARNING: DSID {} is a signal dataset. Flagging as such!".format(DSID)
        args.signal = True

    if not os.path.isdir(path):
        print "  WARNING: skipping - {} is not a directory".format(os.path.basename(path))
        return None
    file_names = getFilenames(path, metadata)

    print "  Found the following files:"
    for f in file_names:
        print "    {}".format(os.path.basename(f))

    if args.data:
        xsec = 1
        rel_uncert = 0
    elif args.signal:
        xsec, rel_uncert = getSignalCrossSection(int(DSID))
        if xsec is None:
            print "    WARNING: Unknown xsec for signal DSID {}!!!".format(DSID)
            sys.exit(5)
    else:
        xsec = xsecDBs[0].xsectTimesEff(int(DSID))
        rel_uncert = xsecDBs[0].rel_uncertainty(int(DSID))
        if (xsec is None or xsec < 0) and len(xsecDBs) > 1:
            print "    WARNING: DSID {} not found in custom xsec file".format(DSID)
            print "             Will use PMG default xsec instead."
            xsec = xsecDBs[1].xsectTimesEff(int(DSID))
            rel_uncert = xsecDBs[1].rel_uncertainty(int(DSID))
        if xsec is None or xsec < 0:
            print "    ERROR: Unknown xsec for DSID {}!!!".format(DSID)
            sys.exit(6)

    print "  Extracted xsec*eff = {0:e} pb (rel_unc = {1:.2e})".format(xsec, rel_uncert)

    print "  Processing files"
    sumW = 0
    N_processed = 0
    for f in file_names:
        info = processFile(f)
        if info is None:
            print "    ERROR: Encountered errors when processing file {}!!!".format(f)
            sys.exit(7)
        sumW += info[0]
        N_processed += info[1]

    if args.data:
        normWeight = 1
    else:
        normWeight = xsec / sumW

    return {"files":file_names, "normWeight":normWeight, "xsec":xsec, "xsec_rel_unc":rel_uncert, "sumW":sumW, "N_processed":N_processed}

#------------------------------------------------------------------------------
def getListOfHistograms(filename, nominal_only=False):
    with root_open(filename, "READ") as f:
        names = []
        keys = f.GetListOfKeys()
        for k in keys:
            name = k.GetName()
            obj = f.Get(name)
            if not obj.IsA().InheritsFrom(ROOT.TH1D.Class()) and not obj.IsA().InheritsFrom(ROOT.TH2D.Class()):
                continue

            ### Since the memory usage fix (v31.1.3), EventLoop_* is in another output stream
            if name.startswith("EventLoop_"):
                continue

            if nominal_only and not "metadata" in name and not "common" in name and not name.endswith("NOMINAL"):
                continue

            names.append(name)

    return names

#------------------------------------------------------------------------------
def histogramNeedsWeight(name):
    return name not in ["metadata", "EventLoop_EventCount", "EventLoop_RunTime"]

#------------------------------------------------------------------------------
def cloneAndScaleHistograms(file_name, names, weight):
    hists = {}

    with root_open(file_name, "READ") as f:
        for n in names:
            h = f.Get(n).Clone()

            if histogramNeedsWeight(n):
                h.Scale(weight)

            h.SetDirectory(0)
            hists[n] = h

    return hists

#------------------------------------------------------------------------------
def mergeHistograms(file, input_files, file_infos, is_data=False, nominal_only=False):
    if len(input_files) == 0:
        print "mergeHistograms(): no input files provided!"
        return

    hists = getListOfHistograms(input_files[0], nominal_only=nominal_only)

    if hists == []:
        print "mergeHistograms(): no histograms to merge!"
        return

    print "Identified the following histograms:"
    for h in hists:
        print "  {}".format(h)

    f = ROOT.TFile(file, "UPDATE")
    existing_objects = [k.GetName() for k in f.GetListOfKeys()]
    for name in list(hists):
        if name in existing_objects:
            print "mergeHistograms(): {} already exists in output file! Skipping...".format(name)
            hists.remove(name)

    # Make output hists by cloning from the first file
    output_hists = cloneAndScaleHistograms(input_files[0], hists, file_infos[input_files[0]]["normWeight"])

    # Ensure they end up in the correct file
    for name in hists:
        output_hists[name].name = name

    # Now add the rest
    n_files = len(input_files) - 1
    for i, file_name in enumerate(input_files[1:]):
        sys.stdout.write("  mergeHistograms(): at file {0:5d} / {1:d}\r".format(i+1, n_files))
        sys.stdout.flush()
        if not is_data and file_name not in file_infos:
            print "ERROR: file {} with unknown xsec!".format(file_name)
            sys.exit(10)

        with root_open(file_name, "READ") as f_in:
            for name in hists:
                h = f_in.Get(name)
                if not h or h is None:
                    print "ERROR: Can't find {} in {}!!".format(name, file_name)
                    sys.exit(11)

                if not is_data and histogramNeedsWeight(name):
                    output_hists[name].Add(h, file_infos[file_name]["normWeight"])
                else:
                    output_hists[name].Add(h)

    f.cd() # otherwise we're no longer in the output file!
    for name in hists:
        output_hists[name].SetDirectory(f)
        output_hists[name].Write()

    for name in hists:
        del output_hists[name]
    del output_hists

    f.Close()

    return

#------------------------------------------------------------------------------
def getListOfTrees(file_name, tree_pattern="*", nominal_only=False):
    names = []

    with root_open(file_name, "READ") as f:
        keys = f.GetListOfKeys()
        for k in keys:
            name = k.GetName()
            obj = f.Get(name)
            if not obj.IsA().InheritsFrom(ROOT.TTree.Class()):
                continue

            ### Since the memory usage fix (v31.1.3), EventLoop_* is in another output stream
            if name.startswith("EventLoop_"):
                continue

            if nominal_only and not name.endswith("NOMINAL"):
                continue

            if not fnmatch.fnmatch(name, tree_pattern):
                continue

            names.append(name)

    return names

#------------------------------------------------------------------------------
def make_chain(tree_name, files, output_tree_name=None, indices=None):
    nfiles = 0

    if not output_tree_name:
        output_tree_name = tree_name

    chain = ROOT.TChain(output_tree_name, "")
    if indices:
        if isinstance(indices, str):
            chain.BuildIndex(indices)
        elif len(indices) == 1:
            chain.BuildIndex(indices[0])
        elif len(indices) == 2:
            chain.BuildIndex(indices[0], indices[1])
        else:
            raise Exception("ROOT cannot build more than two indices: {}".format(indices))

    for file in files:
        nfiles += 1

        found_tree = False
        input_name = tree_name
        with root_open(file, "READ") as f:
            # find the correct input for the treename
            keys = f.GetListOfKeys()
            for k in keys:
                obj = f.Get(k.GetName())
                if not obj.IsA().InheritsFrom(ROOT.TTree.Class()):
                    continue
                if input_name in k.GetName():
                    input_name = copy(k.GetName())
                    found_tree = True
                    break

        if found_tree:
            chain.Add("{}/{}".format(file, input_name))
        else:
            print "WARNING: Tree is not found in {}".format(file)

    return chain

#------------------------------------------------------------------------------
def mergeTrees(file, input_files, file_infos, dataset_infos, is_data, prefix="", tree_pattern="*", nominal_only=False, ignore_branches=[]):
    if len(input_files) == 0:
        print "mergeTrees(): no input files provided!"
        return

    trees = getListOfTrees(input_files[0], tree_pattern, nominal_only)

    if trees == []:
        print "mergeTrees(): no trees to merge!"
        return

    print ""
    print "Identified the following trees:"
    for t in trees:
        print "  {}".format(t)

    # Write a tree with weights
    ###if not is_data:
    # We do this also for data, with dummy normWeight = 1
    # so that we can merge data with MC for fakes
    # without having troubles with tree indices

    with root_open(file, "UPDATE") as f:
        existing_objects = [k.GetName() for k in f.GetListOfKeys()]
        if not "weights" in existing_objects:
            weights_tree = PyTTree("weights")
            if args.shared_norm:
                branches = OrderedDict({'runNumber':'I', 'normWeight':'F', 'xsec':'F'})
            else:
                branches = OrderedDict({'runNumber':'I', 'treatAsYear':'I', 'normWeight':'F', 'xsec':'F'})
            weights_tree.create_branches(branches)

            normWeight = 1
            xsec = 0
            for DSID in dataset_infos:
                weights_tree.runNumber = int(DSID)
                for MC_campaign,dataset_info in dataset_infos[DSID].iteritems():
                    if not is_data:
                        normWeight = dataset_info["normWeight"]
                        xsec = dataset_info["xsec"]
                    print "{}({}) => normWeight = {} / xsec = {}".format(DSID, MC_campaign, normWeight, xsec)
                    weights_tree.normWeight = normWeight
                    weights_tree.xsec = xsec
                    if args.shared_norm:
                        weights_tree.fill()
                    else:
                        if MC_campaign == "16a" or MC_campaign == "COMB":
                            weights_tree.treatAsYear = 2015
                            weights_tree.fill()
                            weights_tree.treatAsYear = 2016
                            weights_tree.fill()
                        if MC_campaign == "16d" or MC_campaign == "COMB":
                            weights_tree.treatAsYear = 2017
                            weights_tree.fill()
                        if MC_campaign == "16e" or MC_campaign == "COMB":
                            weights_tree.treatAsYear = 2018
                            weights_tree.fill()

            if args.shared_norm:
                weights_tree.BuildIndex("runNumber")
            else:
                weights_tree.BuildIndex("runNumber", "treatAsYear")
            weights_tree.write()

    # Now move towards the real trees
    nTrees = len(trees)
    for i, t in enumerate(trees):
        ROOT.gROOT.cd()

        output_treename = copy(t)
        if prefix and not "EventLoop_" in t:
            output_treename = prefix + "_" + "_".join(t.split("_")[1:])

        print ""
        print "Processing {0} ({1}/{2})".format(output_treename, i+1, nTrees)

        if output_treename in existing_objects:
            print "  Tree already exists in output file! Skipping..."
            continue

        _ignore_branches = [] if "EventLoop_" in t else ignore_branches

        if prefix and not "EventLoop_" in output_treename:
            chain = make_chain("_".join(t.split("_")[1:]), input_files, output_treename, indices=("runNumber", "eventNumber"))
        else:
            chain = make_chain(t, input_files)

        with root_open(file, "UPDATE") as f:
            branches = [b.GetName() for b in chain.GetListOfBranches()]
            to_be_ignored = []
            for pattern in _ignore_branches:
                to_be_ignored += fnmatch.filter(branches, pattern)
            for branch in set(to_be_ignored):
                print "  Ignoring branch '{}' while merging.".format(branch)
                chain.SetBranchStatus(branch, 0)

            if args.ignore_merge_warnings:
                with rootVerbosityLevel("Error"):
                    chain.Merge(f, 0, "keep")
            else:
                chain.Merge(f, 0, "keep")

        print "  Wrote tree '{}'".format(output_treename)

    print ""
    print "All done!"
    print ""

    return

#------------------------------------------------------------------------------
def mergeDatasets(datasets, dataset_infos, metadata, output_file, is_data, prefix="", tree_pattern="*", nominal_only=False, ignore_branches=[]):
    print "Will write output to {0}".format(output_file)

    # Get all files and flatten dataset_infos into file_infos
    file_names = []
    file_infos = {}
    for DSID in dataset_infos:
        for dataset_info in dataset_infos[DSID].values():
            for file_name in dataset_info["files"]:
                file_names.append(file_name)
                file_infos[file_name] = dataset_info

    # Now merge the whole thing
    writeMetadata(output_file, metadata)
    print ""
    mergeHistograms(output_file, file_names, file_infos, is_data, nominal_only=True)
    print ""
    mergeTrees(output_file, file_names, file_infos, dataset_infos, is_data, prefix, tree_pattern, nominal_only=nominal_only, ignore_branches=ignore_branches)

    return

#------------------------------------------------------------------------------
def getSignalName(DSID):
    names = {}
    names[303778] = "Ztaumu"
    names[303779] = "Ztaue"
    names[307503] = "Ztaumugamma"
    names[307504] = "Ztauegamma"

    if DSID in names:
        return names[DSID]

    return "Unknown signal"

#------------------------------------------------------------------------------
def writeTexOutput(dataset_infos, xsecDBs, process, output_file, is_signal=False, is_data=False):

    label = process
    if process is None or process == "":
        label = os.path.basename(output_file).replace(".tex", "")

    with open(output_file, "w+") as f:
        f.write("\\definecolor{lightgray}{gray}{0.925}\n")
        f.write("\\begin{sidewaystable}\n")
        f.write("\\footnotesize\n")
        f.write("\\rowcolors{1}{lightgray}{}\n")
        f.write("\\begin{tabularx}{\\textwidth}{\n")
        f.write("    l % DSID\n")
        f.write("    X % Name\n")
        # NOTE: make sure that these numbers always match the print specifiers below: if you print 4 digits but only have space for 3, the table looks weird
        f.write("    S[table-format=1.2e+2] % xsec*eff\n")
        f.write("    S[table-format=1.2] % k-factor\n")
        f.write("    S[table-format=8] %S[table-format=3.2,table-number-alignment=right] % Ngen\n")
        f.write("    S[table-format=1.2e+2] % sumW\n")
        f.write("    S[table-format=1.2e+2] % lumi\n")
        f.write("    }\n")
        f.write("\\toprule\n")
        f.write("\\rowcolor{white}\n")
        # Use multicolumn as a way to left-align
        f.write("\\multicolumn{1}{l}{\\bfseries DSID} & \\multicolumn{1}{l}{\\bfseries Name} & \\multicolumn{1}{l}{$\\mathbf{\\sigma\\cdot\\epsilon}$~[\\si{\\pb}]} & \\multicolumn{1}{l}{$k\  extup{-factor}$} & \\multicolumn{1}{l}{$N_{\\mathrm{gen}}$} & \\multicolumn{1}{l}{$\\Sigma\\, w$} & \\multicolumn{1}{l}{$\\mathcal{L}$~[\\si{\\ifb}]} \\\\\n")
        f.write("\\midrule\n")

        if not is_data and len(dataset_infos) > 0:
            print "Norm. weights for 139 fb-1:"

        for DSID in dataset_infos:
            for MC_campaign,weights in dataset_infos[DSID].iteritems():
                normWeight = weights["normWeight"]
                xsec = weights["xsec"]
                sumW = weights["sumW"]
                N_processed = weights["N_processed"]

                lumi = sumW / (xsec * 1000.0)

                # if int(DSID) == 361108 or int(DSID) == 361107:
                #     # Powheg Ztautau, Zmumu counts different internally
                #     sumW /= 1000.0
                #     lumi /= 1000.0

                if is_data:
                    raw_xsec = 0
                    k_factor = 1
                    name = "Data"
                    lumi = 0
                elif is_signal:
                    raw_xsec = xsec
                    k_factor = 1
                    name = getSignalName(int(DSID))
                else:
                    name = xsecDBs[0].name(int(DSID)).replace('_','\_')
                    raw_xsec = xsecDBs[0].rawxsect(int(DSID)) * xsecDBs[0].efficiency(int(DSID))
                    k_factor = xsecDBs[0].kfactor(int(DSID))
                    if raw_xsec is None or raw_xsec < 0:
                        name = xsecDBs[1].name(int(DSID)).replace('_','\_')
                        raw_xsec = xsecDBs[1].rawxsect(int(DSID)) * xsecDBs[1].efficiency(int(DSID))
                        k_factor = xsecDBs[1].kfactor(int(DSID))

                if args.shared_norm:
                    f.write("{:d} & {:s} & {:.2e} & {:.2f} & {:5d} & {:.2e} & {:.2e} \\\\\n".format(int(DSID), name, raw_xsec, k_factor, N_processed, int(sumW), lumi))
                else:
                    f.write("{:d}({:s}) & {:s} & {:.2e} & {:.2f} & {:5d} & {:.2e} & {:.2e} \\\\\n".format(int(DSID), MC_campaign, name, raw_xsec, k_factor, N_processed, int(sumW), lumi))

                if not is_data:
                    print "  {:d}({:s}) => {:.3g}".format(int(DSID), MC_campaign, normWeight * 138972.0)

        f.write("\\bottomrule\n")
        f.write("\\end{tabularx}\n")
        f.write("\\caption{Summary of samples used in the analysis. $\sigma \cdot \epsilon$ corresponds to the cross-section provided by the Monte Carlo generator multiplied with a possible truth-level filter efficiency. The $k$-factor is used to normalise the generator cross-section to the best known for a process. $\sum w$ includes the generator-level weights and the pileup reweighting effect. $\mathcal{L}_{int}$ is the generated equivalent integrated luminosity for the sample.}\n")
        f.write("\\label{tab:crosssection_%s}\n" % label)
        f.write("\\end{sidewaystable}\n")

    return

#------------------------------------------------------------------------------
def mergeNtuples(args):

    ROOT.gROOT.SetBatch(True)
    ROOT.TTree.SetMaxTreeSize(long(4)*1024*1024*1024*1024)

    if not "TestArea" in os.environ:
        print "ERROR: $TestArea is not set. Perhaps you did not do `asetup` yet?"
        sys.exit(3)

    if not args.data and args.MC_campaign == "comb" and args.shared_norm:
        print "You have chosen combined MC campaign with shared normalisation!"
        print "Will create combined dataset directories."
        prepareCombinedDatasets(args)

    datasets = scanDirectories(args)

    if not datasets:
        print "ERROR: no datasets found with your criteria! Exiting."
        sys.exit(4)

    xsecDBs = []
    if not args.data:
        if args.xsecfile:
            print "Using xsec file: {}".format(args.xsecfile)
            print ""
            db = ROOT.SUSY.CrossSectionDB("", False, False, False)
            db.loadFile(args.xsecfile)
            xsecDBs.append(db)
        else:
            print "Using PMG's default cross sections."
        xsecDBs.append(ROOT.SUSY.CrossSectionDB())

    metadata = {"submitted_revision":set(), "submitted_by":set(), "submitted_at":set(), "submitted_type":set()}

    print "Processing all datasets..."
    dataset_infos = defaultdict(dict)
    N = len(datasets)
    for i, DSID in enumerate(datasets):
        print "  {0} / {1} - DSID: {2}".format(i+1, N, DSID)
        for MC_campaign,path in datasets[DSID].iteritems():
            if args.MC_campaign=="comb" and args.shared_norm ^ (MC_campaign=="COMB"):
                continue
            if not args.MC_campaign=="comb" and not MC_campaign==args.MC_campaign:
                continue
            print "    {}: processing {}".format(MC_campaign, os.path.basename(path))
            dataset_info = processDataset(args, xsecDBs, DSID, path, metadata)
            if dataset_info is not None:
                if args.MC_campaign == "comb" and not args.shared_norm:
                    rel_wgt = relativeNormWeight(MC_campaign)
                    print "    normWeight = {normWeight} (*{0}), xsec = {xsec}, sumW = {sumW}, N = {N_processed}".format(rel_wgt, **dataset_info)
                    dataset_info["normWeight"] *= rel_wgt
                else:
                    print "    normWeight = {normWeight}, xsec = {xsec}, sumW = {sumW}, N = {N_processed}".format(**dataset_info)
                dataset_infos[DSID][MC_campaign] = dataset_info
        print ""

    mergeDatasets(datasets, dataset_infos, metadata, args.output_file, args.data, args.prefix, args.tree_pattern, args.nominal_only, args.ignore_branches)
    ### TODO: improve the TeX output function
    writeTexOutput(dataset_infos, xsecDBs, args.process_patterns, args.output_file.replace(".root", ".tex"), args.signal, args.data)

#==============================================================================
if __name__ == "__main__":
    args = parseArguments(sys.argv[1:])
    mergeNtuples(args)
