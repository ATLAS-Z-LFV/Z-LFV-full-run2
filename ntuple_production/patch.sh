#-------------------------------------------------------------------------------
# Checkout and patch EventLoop

if [[ ! -d source/EventLoop ]] ; then
  DIR=AnalysisBase/21.2.96/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoop
  if [[ -d /cvmfs/atlas.cern.ch/repo/sw/software/21.2/${DIR} ]] ; then
    ROOT=/cvmfs/atlas.cern.ch/repo/sw/software/21.2
  elif [[ -d /usr/${DIR} ]] ; then
    ROOT=/usr  # atlas/analysisbase Docker image
  else
    echo "ERROR: Could not check out EventLoop from /cvmfs or /usr !"
    return 11
  fi
  echo "Checking out EventLoop from ${ROOT}/${DIR}..."
  cp -r ${ROOT}/${DIR} source/EventLoop
fi

if [[ ! -f source/EventLoop/patched ]] ; then
  echo "Patching EventLoop (21.2.96)..."
  patch -p0 < patches/EventLoop-21-2-96.diff
  if [[ $? -ne 0 ]] ; then
    echo "ERROR: Failed to patch EventLoop."
    echo "       If you had checked out EventLoop before, consider removing the directory and rerun."
    return 12
  fi
  echo "21.2.96 20200609" > source/EventLoop/patched
elif ! grep -q '21.2.96 20200609' source/EventLoop/patched && \
     ! grep -q '21.2.90 20191015' source/EventLoop/patched ; then
  echo "EventLoop has previously been checked out and patched, and is incompatible with current version."
  echo "Please remove completely 'source/EventLoop/' and rerun."
  return 13
fi

#-------------------------------------------------------------------------------
# Checkout and patch EventLoopGrid

if [[ ! -d source/EventLoopGrid ]] ; then
  DIR=AnalysisBase/21.2.96/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopGrid
  if [[ -d /cvmfs/atlas.cern.ch/repo/sw/software/21.2/${DIR} ]] ; then
    ROOT=/cvmfs/atlas.cern.ch/repo/sw/software/21.2
  elif [[ -d /usr/${DIR} ]] ; then
    ROOT=/usr  # atlas/analysisbase Docker image
  else
    echo "ERROR: Could not check out EventLoopGrid from /cvmfs or /usr !"
    return 14
  fi
  echo "Checking out EventLoopGrid from ${ROOT}/${DIR}..."
  cp -r ${ROOT}/${DIR} source/EventLoopGrid
fi

if [[ ! -f source/EventLoopGrid/patched ]] ; then
  echo "Patching EventLoopGrid (21.2.96)..."
  patch -p0 < patches/EventLoopGrid-21-2-96.diff
  if [[ $? -ne 0 ]] ; then
    echo "ERROR: Failed to patch EventLoopGrid."
    echo "       If you had checked out EventLoopGrid before, consider removing the directory and rerun."
    return 15
  fi
  echo "21.2.96 20200609" > source/EventLoopGrid/patched
elif ! grep -q '21.2.96 20200609' source/EventLoopGrid/patched && \
     ! grep -q '21.2.90 20191015' source/EventLoopGrid/patched ; then
  echo "EventLoopGrid has previously been checked out and patched, and is incompatible with current version."
  echo "Please remove completely 'source/EventLoopGrid/' and rerun."
  return 16
fi

#-------------------------------------------------------------------------------
# Checkout and patch SUSYTools

if [[ ! -d source/SUSYTools ]] ; then
  DIR=AnalysisBase/21.2.96/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/SUSYPhys/SUSYTools
  if [[ -d /cvmfs/atlas.cern.ch/repo/sw/software/21.2/${DIR} ]] ; then
    ROOT=/cvmfs/atlas.cern.ch/repo/sw/software/21.2
  elif [[ -d /usr/${DIR} ]] ; then
    ROOT=/usr  # atlas/analysisbase Docker image
  else
    echo "ERROR: Could not check out SUSYTools from /cvmfs or /usr !"
    return 17
  fi
  echo "Checking out SUSYTools from ${ROOT}/${DIR}..."
  cp -r ${ROOT}/${DIR} source/SUSYTools
fi

if [[ ! -f source/SUSYTools/patched ]] ; then
  echo "Patching SUSYTools (21.2.96)..."
  patch -p0 < patches/SUSYTools-21-2-96.diff
  if [[ $? -ne 0 ]] ; then
    echo "ERROR: Failed to patch SUSYTools."
    echo "       If you had checked out SUSYTools before, consider removing the directory and rerun."
    return 18
  fi
  echo "21.2.96 20200609" > source/SUSYTools/patched
elif ! grep -q '21.2.96 20200609' source/SUSYTools/patched && \
     ! grep -q '21.2.90 20191015' source/SUSYTools/patched ; then
  echo "SUSYTools has previously been checked out and patched, and is incompatible with current version."
  echo "Please remove completely 'source/SUSYTools/' and rerun."
  return 19
fi
