RELEASE="AnalysisBase 21.2.96,here"

[[ -z ${QUICK_SETUP+x} ]] && QUICK_SETUP=false

#-------------------------------------------------------------------------------
# setupATLAS

( hash asetup &> /dev/null )
if [[ $? -ne 0 ]] ; then
  # asetup not hashed, for sure you haven't setupATLAS! Try to do so now

  if [[ -z ${ATLAS_LOCAL_ROOT_BASE+x} ]] ; then
    # ATLAS_LOCAL_ROOT_BASE is not set, try to set it now
    echo "Setting ATLAS_LOCAL_ROOT_BASE..."

    if [[ `hostname -f` = stbc*.nikhef.nl ]] ; then
      # Good! We are on Nikhef's Stoomboot, we know how to set up :)
      source /project/atlas/nikhef/cvmfs/setup.sh

    elif [[ -d /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase ]] ; then
      # We have cvmfs, try setting ATLAS_LOCAL_ROOT_BASE there
      export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"

    else
      echo "ERROR: ATLAS_LOCAL_ROOT_BASE is not set. Please set it first."
      return 1

    fi
  fi

  echo "Setting up ATLAS..."
  source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh > /dev/null
fi

( hash asetup &> /dev/null )
if [[ $? -ne 0 ]] ; then
  # asetup still not hashed, giving up...
  echo "ERROR: Failed to setup ATLAS."
  return 2
fi

#-------------------------------------------------------------------------------
# Patch EventLoop, EventLoopGrid and SUSYTools
source patch.sh
if [[ $? -ne 0 ]] ; then
  return $?
fi

#-------------------------------------------------------------------------------
# asetup and build

cd ./source
[[ -z ${TestArea+x} || $TestArea != `readlink -f $PWD` ]] && asetup $RELEASE
[[ -f LFV_Z/data/config.ini ]] || touch LFV_Z/data/config.ini  # Otherwise there would be no symlink in build/

[[ ! -d ../build ]] && mkdir ../build
cd ../build
[[ $QUICK_SETUP = false ]] && cmake ../source
source x*-opt/setup.sh
unset AtlasBuildStamp  # Needed to avoid some bugs

[[ $QUICK_SETUP = false ]] && make -j4

cd ..
unset QUICK_SETUP
return 0
