#!/usr/bin/env python

import os, sys
import argparse
from time import sleep

from modules.Inputs import Inputs

from modules.general_utils import findDefaultConfigs, checkMakeDirs
from modules.general_utils import rootVerbosityLevel, root_open

from ROOT import gROOT, TFile, TTree, TChain
from ROOT.TObject import kOverwrite

import numpy as np
from root_numpy import tree2array, array2tree


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser("./createFakes.py")
    
    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    
    parser.add_argument("--output-dir", "-d", default="", type=str, help="Output directory for writing the split trees. Default = input directory.")
    parser.add_argument("--output-file", "-o", default="fakes.root", type=str, help="Output file name. Default = 'fakes.root'.")
    parser.add_argument("--tree-name", "-t", default="fakes_NOMINAL", type=str, help="Output tree name. Default = 'fakes_NOMINAL'.")
    
    parser.add_argument("--alt-bkgs", action="store_true", default=False, help="Use alternative background samples.")
    parser.add_argument("--fail-selection", default="taus_signal[0]==0&&taus_jet_rnn_loose[0]==1", type=str, help="Fail (aka 'anti-tau') selection. Default = 'taus_signal[0] == 0 && taus_jet_rnn_loose[0] == 1'")
    parser.add_argument("--truth-selection", default="tau_0_truth_isEle||tau_0_truth_isMuon||tau_0_truth_isTau", type=str, help="Truth (i.e. lep->tau) selection, applied only to MCs. Default = 'tau_0_truth_isEle || tau_0_truth_isMuon || tau_0_truth_isTau'")
    
    parser.add_argument("--ignore-merge-warnings", default=False, action="store_true", help="Ignore merging warnings.")
    
    parser.add_argument("--force-recreate", "-f", action='store_true', default=False, help="Force recreation of the output files.")
    parser.add_argument("--keep-old-cycles", action='store_true', default=False, help="Keep old cycles. This keeps tree backups, but might waste disk space.")
    
    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args)
        
        checkArgs(args)
        
    except:
        parser.print_help()
        raise
    
    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    
    checkMakeDirs(args.output_dir)

#-------------------------------------------------------------------------------
def createFakes(args):
    
    gROOT.SetBatch(True)
    TTree.SetMaxTreeSize(long(2)*1024*1024*1024*1024)  # 2TB
    
    inputs = Inputs(args.inputs_db)
    inputs.useAlt(args.alt_bkgs)
    
    if not args.output_dir:
        args.output_dir = inputs.dir
    checkMakeDirs(args.output_dir)
    
    output_file_name = os.path.join(args.output_dir, args.output_file)
    if os.path.exists(output_file_name) and args.force_recreate:
        print "WARNING: Will force recreation of '{}'".format(output_file_name)
        sleep(3)  # panic now!
    
    chain = TChain("merge", "merge")
    ###chain.BuildIndex("runNumber", "eventNumber")
    
    w_chain = TChain("w_merge", "w_merge")
    
    # Always add data after MCs because MC trees have more branches
    # TChain.Merge() doesn't like seeing branches that are
    # not in the first tree of the chain
    it = (f for x in inputs.bkgs.values()+[inputs.data] for f in x.files.values())
    for f in it:
        print "  Adding to chain: '{}:{}_NOMINAL'".format(f.name, f.tree)
        chain.Add("{}/{}_NOMINAL".format(f.name, f.tree))
        w_chain.Add("{}/weights".format(f.name))
    
    selection = "({}) && (lbNumber>0 || ({}))".format(args.fail_selection, args.truth_selection)
    #selection = "taus_signal[0] == 0 && taus_jet_rnn_loose[0] == 1 && (lbNumber>0||tau_0_truth_isEle||tau_0_truth_isMuon||tau_0_truth_isTau)"
    print "Copying events with '{}'".format(selection)
    
    output_file_mode = "RECREATE" if args.force_recreate else "UPDATE"
    with root_open(output_file_name, output_file_mode) as f:
        # Open the file before copying, so that ROOT can write on disk
        # Also allows us to check the progress by looking at the file size
        if args.ignore_merge_warnings:
            with rootVerbosityLevel("Error"):
                t = chain.CopyTree(selection, "")
        else:
            t = chain.CopyTree(selection, "")
            ###t = chain.CopyTree(selection, "", 100000, 80658515)  # Debug: ~10k MCs and ~10k data
        t.SetNameTitle(args.tree_name, args.tree_name)
        
        # The weights tree is a bit tricky
        # We need to give a dummy normWeight to data for the plotting to work
        # But if the same runNumber also corresponds to an MC dataset
        # DON'T add the dummy normWeight, otherwise the MC will get the wrong weight
        arr = tree2array(w_chain)
        mc_arr = arr[arr["xsec"]>0]
        data_arr = arr[arr["xsec"]==0]
        out_arr = mc_arr
        for entry in data_arr:
            if entry["runNumber"] not in mc_arr["runNumber"]:
                out_arr = np.append(out_arr, entry)
        w_t = array2tree(out_arr, "weights")
        
        # Write the trees
        if args.keep_old_cycles:
            f.WriteTObject(t)
            f.WriteTObject(w_t)
        else:
            f.WriteTObject(t, "", "Overwrite")
            f.WriteTObject(w_t, "", "Overwrite")
        
    
    print ""
    print "{}d file '{}'".format(output_file_mode.title(), output_file_name)
    print ""

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    createFakes(args)
