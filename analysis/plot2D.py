#!/usr/bin/env python

import os, sys
import argparse
from collections import OrderedDict

from modules.general_utils import findDefaultConfigs, flattenCommaSeparatedLists, checkMakeDirs

from modules.Inputs import Inputs
from modules.Regions import Regions, Cut
from modules.Histograms import Histograms
from modules.Plots import Plots

from ROOT import gROOT, gStyle
from ROOT import TFile, TCanvas, TH2D, TPaveText
from ROOT import kInvertedDarkBodyRadiator


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser(prog='./plot2D.py')

    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")
    parser.add_argument("--histograms-db", "-H", default="", type=str, help="Histograms config in YAML.")
    parser.add_argument("--plots-db", "-P", default="", type=str, help="Plots config in YAML.")

    parser.add_argument("--tag", "-t", default="foo", type=str, help="The input/output tag. The script will read cached histograms from './data/<tag>.root' if already exists, and write results to the directory './results/plots2D_<tag>'.")
    parser.add_argument("--NN-tag", default="", type=str, help="NN tag.")
    parser.add_argument("--force-recreate", "-f", action="store_true", default=False, help="Force recreation of the histogram caches.")

    parser.add_argument("--region", "-r", dest="regions", default=[], type=str, action='append', help="Region to plot. Can have multiple regions.")
    parser.add_argument("--variable", "-v", dest="variables", default=[], type=str, action='append', help="Variable to plot. Can have multiple variables. Overrides --plots-db.")
    parser.add_argument("--sample", "-s", dest="samples", default=[], type=str, action='append', help="Samples (processes) to plot. Can have multiple samples. If not specified, plot all in --inputs-db.")
    parser.add_argument("--temporary-cuts", action="store_true", default=False, help="Ignore cuts that have been marked 'ignore_temporarily' in the YAML file.")
    parser.add_argument("--normalise", action="store_true", default=False, help="Normalise total yield to unity.")

    parser.add_argument("--alt-bkgs", action="store_true", default=False, help="Use alternative background samples.")
    parser.add_argument("--use-MC-fakes", action="store_true", default=False, help="Use fakes estimated by MC, instead of the Fake Factor method.")
    parser.add_argument("--use-full-trees", action="store_true", default=False, help="Use full trees instead of region trees.")

    parser.add_argument("--no-split-prongs", dest="split_prongs", action="store_false", default=True, help="Do not plot events with 1-prong and 3-prong taus separately.")
    parser.add_argument("--split-mode", type=str, choices=["1p3p", "1p0nXn3p"], default="1p3p", help="How to split tau_had decay mode ('1p3p' or '1p0nXn3p').")

    parser.add_argument("--atlas-label", default="Internal", type=str, help="Text that follows the ATLAS label.")

    parser.add_argument("--v22", action="store_true", help="Backward compatibility with v22 n-tuples.")
    parser.add_argument("--no-ZWeight", action="store_true", help="Do not apply Z pT reweighting.")

    # Fiddle fiddle fiddle
    parser.add_argument("--hide-region-label", action="store_true", help="Hide the region label.")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args, default_split_inputs=(not args.use_full_trees))

        args.regions = flattenCommaSeparatedLists(args.regions)
        args.variables = flattenCommaSeparatedLists(args.variables)
        args.samples = flattenCommaSeparatedLists(args.samples)

        if args.atlas_label.lower() == "none":
            args.atlas_label = ""

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))
    if not os.path.exists(args.histograms_db):
        raise Exception("Cannot find histograms YAML file '{}'".format(args.histograms_db))
    if not args.variables and not os.path.exists(args.plots_db):
        raise Exception("Cannot find plots YAML file '{}'".format(args.plots_db))

    if not args.regions:
        raise Exception("No regions specified.")



#-------------------------------------------------------------------------------
def plot2D(args):

    gROOT.SetBatch()
    gROOT.ProcessLine("{gErrorIgnoreLevel = kWarning;}")
    gStyle.SetOptStat(0)
    gStyle.SetPalette(kInvertedDarkBodyRadiator)

    inputs = Inputs(args.inputs_db)
    regions = Regions(args.regions_db)
    histograms = Histograms(args.histograms_db)

    if args.variables:
        variables = {r:args.variables for r in args.regions}
    else:
        plots = Plots(args.plots_db)
        variables = {r:plots[r] for r in plots if r in args.regions}

    if args.alt_bkgs:
        inputs.useAlt()

    output_dir = "results/plots2D_{}".format(args.tag)
    checkMakeDirs(output_dir)
    cached = TFile("data/{}.root".format(args.tag), "UPDATE")

    #-------------------------------------------------
    tau_prongs = {"":"1"}
    if args.split_prongs:
        if args.split_mode == "1p3p":
            tau_prongs.update({"_1P":"taus_n_tracks[0]==1", "_3P":"taus_n_tracks[0]==3"})
        elif args.split_mode == "1p0nXn3p":
            tau_prongs.update({"_1p0n":"taus_n_tracks[0]==1&&taus_decay_mode[0]==0",
                               "_1pXn":"taus_n_tracks[0]==1&&taus_decay_mode[0]!=0",
                               "_3p":"taus_n_tracks[0]==3"})
    # TODO: We can plot 1P and 3P first, then add them together to make the inclusive

    # Loops over regions, prongness, variables, samples
    for r in args.regions:
        region = regions[r]

        samples = OrderedDict(inputs.bkgs)
        if not args.use_MC_fakes:
            samples["fakes"] = inputs.fakes
        for sig in region.signals:
            samples[sig] = inputs[sig]
        samples["data"] = inputs.data
        for s in samples.keys():
            if args.samples and s not in args.samples:
                del samples[s]

        for p in tau_prongs:
            region_name = r + p
            print "="*30
            print "Plotting region: '{}'...".format(region_name)

            if args.use_full_trees:
                cut = region.temporary_selection if args.temporary_cuts else region.selection
            else:
                cut = Cut("1") if args.temporary_cuts else region.ignored_selection
            cut &= tau_prongs[p]
            # Truth selections will later be added per channel per background sample in loops

            for v in variables[r]:
                print "  variable: '{}'".format(v)
                var = histograms[v]
                if not var.is_2D: continue

                for s,sam in samples.iteritems():
                    print "    sample: '{}'".format(s)

                    c = TCanvas("c", "c", 800, 700)
                    c.SetLeftMargin(0.12)
                    c.SetBottomMargin(0.10)
                    c.SetRightMargin(0.15)
                    c.SetTopMargin(0.05)
                    c.SetLogz(var.logscale)
                    c.SetTicks(1, 1)

                    h_name = "h_{r}_{v}_{s}".format(r=region_name, v=v, s=s)

                    read_from_tree = True
                    if not args.force_recreate:
                        for key in cached.GetListOfKeys():
                            if not key.GetClassName().startswith("TH2"):
                                continue
                            if h_name == key.GetName():
                                print "    found cached histogram, re-drawing..."
                                read_from_tree = False
                                h = cached.Get(h_name)
                                break

                    if read_from_tree:
                        gROOT.cd()
                        h = TH2D(h_name, "", var.nBinsX, var.minX, var.maxX, var.nBinsY, var.minY, var.maxY)

                        ### ### For getting mu->tau fakes SFs
                        ### from array import array
                        ### x_bin = array('d', [0.1, 0.7, 2.0, 2.5])
                        ### y_bin = array('d', [25, 40, 80])
                        ### h = TH2D(h_name, "", len(x_bin)-1, x_bin, len(y_bin)-1, y_bin)

                        ### ### For getting e->tau fakes SFs
                        ### from array import array
                        ### x_bin = array('d', [0.0, 0.8, 1.37, 1.52, 2.5])
                        ### y_bin = array('d', [25, 40, 80])
                        ### h = TH2D(h_name, "", len(x_bin)-1, x_bin, len(y_bin)-1, y_bin)

                        _cut = Cut(cut)
                        if not args.use_MC_fakes:
                            if not (sam.is_data or sam.is_signal or s=="fakes"):
                                _cut &= region.truth_selection
                        cut_string = _cut.string

                        weight_string = region.weight_string(inputs.lumi)
                        if not args.no_ZWeight and s in ["Zll", "Ztautau", "Ztaumu", "Ztaue"]:
                            weight_string += "*ZWeight_NOMINAL"
                        if s=="fakes":
                            weight_string = "((lbNumber>0)-(lbNumber==0)*{})*FF_NOMINAL".format(weight_string)

                        if sam.is_data:
                            draw_string = cut_string
                        else:
                            draw_string = "({})*({})".format(cut_string, weight_string)

                        for file in sam.values():
                            f = TFile(file.name)

                            if args.use_full_trees:
                                tree = "{}_NOMINAL".format(file.tree)
                            else:
                                tree = "{}_{}_NOMINAL".format(file.tree, region.split_tree_region)
                            t = f.Get(tree)

                            if not sam.is_data:
                                t.AddFriend("weights")
                            if args.NN_tag:
                                t.AddFriend("NN_"+tree, "{}.NN.{}.friend".format(file.name, args.NN_tag))
                            if not args.no_ZWeight and s in ["Zll", "Ztautau", "Ztaumu", "Ztaue"]:
                                t.AddFriend("ZWeights_"+tree, file.name+".ZWeights.friend")
                            if s=="fakes":
                                t.AddFriend("FF_"+tree, file.name+".FF.friend")

                            gROOT.cd()
                            t.Project("+"+h_name, var.branch, draw_string)
                            f.Close()

                    if var.unitX:
                        h.GetXaxis().SetTitle("{} [{}]".format(var.root_labelX, var.unitX))
                    else:
                        h.GetXaxis().SetTitle(var.root_labelX)
                    h.GetXaxis().SetTitleSize(0.042)
                    h.GetXaxis().SetLabelSize(0.037)

                    if var.unitY:
                        h.GetYaxis().SetTitle("{} [{}]".format(var.root_labelY, var.unitY))
                    else:
                        h.GetYaxis().SetTitle(var.root_labelY)
                    #h.GetYaxis().SetTitle("#it{m}_{T}(#it{#tau}_{had-vis}, #it{E}^{miss}_{T}) [GeV]")
                    h.GetYaxis().SetTitleSize(0.042)
                    h.GetYaxis().SetLabelSize(0.037)

                    if read_from_tree:
                        cached.cd()
                        h.Write()
                        gROOT.cd()

                    if args.normalise:
                        h.Scale(1. / h.Integral(0, h.GetNbinsX()+1, 0, h.GetNbinsY()+1))

                    h.Draw("colz")

                    if args.atlas_label:
                        atlas_label = TPaveText(0.14, 0.75, 0.5, 0.93, "NDC")
                    else:
                        atlas_label = TPaveText(0.14, 0.80, 0.5, 0.93, "NDC")
                    atlas_label.SetBorderSize(0)
                    atlas_label.SetFillStyle(0)
                    atlas_label.SetLineStyle(0)
                    if args.atlas_label:
                        text = atlas_label.AddText("#font[72]{ATLAS}")
                        text.SetTextSize(0.045)
                        text.SetTextAlign(13)
                        if s in ["data", "fakes"]:
                            text = atlas_label.AddText("#font[42]{%s}" % args.atlas_label)
                        else:
                            text = atlas_label.AddText("#font[42]{Simulation %s}" % args.atlas_label)
                        text.SetTextSize(0.045)
                        text.SetTextAlign(13)
                    else:
                        if s in ["data", "fakes"]:
                            text = atlas_label.AddText("#font[72]{ATLAS}")
                        else:
                            text = atlas_label.AddText("#font[72]{ATLAS} #font[42]{Simulation}")
                        text.SetTextSize(0.045)
                        text.SetTextAlign(13)
                    if s in ["data", "fakes"]:
                        if inputs.lumi < 100:
                            lumi_str = "%.1f" % inputs.lumi  # 36.2 fb-1 / 79.8 fb-1
                        else:
                            lumi_str = "%.0f" % inputs.lumi  # 139 fb-1
                        text = atlas_label.AddText("#font[42]{#sqrt{s} = 13 TeV, %s fb^{-1}} " % lumi_str)
                    else:
                        text = atlas_label.AddText("#font[42]{#sqrt{s} = 13 TeV}")
                    text.SetTextFont(42)
                    text.SetTextSize(0.040)
                    text.SetTextAlign(13)
                    atlas_label.Draw()

                    input_label = TPaveText(0.13, 0.85, 0.85, 0.95, "NDC")
                    input_label.SetBorderSize(0)
                    input_label.SetFillStyle(0)
                    input_label.SetLineStyle(0)
                    if not args.hide_region_label:
                        if args.split_prongs:
                            text = input_label.AddText("#font[42]{%s %s}" % (region.root_title, p))
                        else:
                            text = input_label.AddText("#font[42]{%s}" % region.root_title)
                        text.SetTextSize(0.045)
                        text.SetTextAlign(33)
                    if sam.is_signal:
                        sam.root_label = "Signal "+sam.root_label
                    text = input_label.AddText("#font[42]{%s}" % sam.root_label)
                    text.SetTextSize(0.045)
                    text.SetTextAlign(33)
                    input_label.Draw()

                    c.SaveAs(os.path.join(output_dir, "Plot2D_{}.pdf".format(h_name[2:])))

                    del c

    cached.Close()
    print "Done!!"

#-------------------------------------------------------------------------------
if __name__ == '__main__':
    args = parseArgs(sys.argv[1:])
    plot2D(args)

