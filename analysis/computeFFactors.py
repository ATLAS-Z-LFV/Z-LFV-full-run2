#!/usr/bin/env python

import os, sys
import argparse
from collections import OrderedDict, defaultdict
from copy import copy
from itertools import cycle
from multiprocessing import Process, Lock
from time import sleep

from ROOT import TObject

from modules.Inputs import Inputs
from modules.Regions import Regions
from modules.general_utils import findDefaultConfigs, checkMakeDirs, flattenCommaSeparatedLists
from modules.general_utils import checkCpuCount, acquire
from modules.general_utils import saveRootObjects
from modules.fakes_utils import *


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser("./computeFFactors.py")

    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")

    parser.add_argument("--output-dir", "-o", default="fakes", type=str, help="Output directory for writing saving the results. Default = './fakes'")
    parser.add_argument("--tag", "-t", default="foo", type=str, help="Output tag. Default = 'foo'")
    parser.add_argument("--force-recreate", "-f", action='store_true', default=False, help="Force recreation of the output files.")

    parser.add_argument("--fakes-region", "-r", dest="FRs", action='append', default=[], type=str, help="The Fakes-enriched region(s). Special aliases: 'MU' = ['FRW_mu','FRZll_mu','FRT_mu','FRQ_mu'], 'EL' = ['FRW_el','FRZll_el','FRT_el','FRQ_el'], 'ALL' = ['MU','EL']")
    parser.add_argument("--alt-bkgs", action="store_true", default=False, help="Use alternative background samples.")
    parser.add_argument("--BDT-ID", default=False, action="store_true", help="Using BDT tau jet-ID instead of the RNN one.")

    parser.add_argument("--split-mode", type=str, choices=["1p3p", "1p0nXn3p"], default="1p3p", help="How to split tau_had decay mode ('1p3p' or '1p0nXn3p').")

    parser.add_argument("--NN-tag", type=str, default=None, help="NN tag. Needed if NN outputs are used to define the region(s).")

    parser.add_argument("--parallel", action='store_true', default=False, help="Use parallel multiprocessing.")
    parser.add_argument("--max-threads", default=16, type=int, help="Maximum number of threads to use for parallel multiprocessing.")

    #parser.add_argument("--combine-emu", action="store_true", default=False, help="Calculate FFs for the electron and muon channels combined.")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args)

        FRs = flattenCommaSeparatedLists(args.FRs)
        FRs = set(FRs)
        if 'MU' in FRs:
            FRs.remove('MU')
            FRs = FRs.union(['FRW_mu','FRZll_mu','FRT_mu','FRQ_mu'])
        if 'EL' in FRs:
            FRs.remove('EL')
            FRs = FRs.union(['FRW_el','FRZll_el','FRT_el','FRQ_el'])
        if 'ALL' in FRs:
            FRs.remove('ALL')
            FRs = FRs.union(['FRW_mu','FRZll_mu','FRT_mu','FRQ_mu'])
            FRs = FRs.union(['FRW_el','FRZll_el','FRT_el','FRQ_el'])
        args.FRs = list(FRs)

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))

    checkMakeDirs(os.path.join(args.output_dir, args.tag))

    if len(args.FRs) == 0:
        raise Exception("You did not specify any fakes-enriched regions (FRs)!")

#-------------------------------------------------------------------------------
def computeFFactors(args, lock=None):
    inputs = Inputs(args.inputs_db)
    regions = Regions(args.regions_db)

    if args.parallel and len(args.FRs) > 1:
        n_threads = checkCpuCount(min(args.max_threads, len(args.FRs)))
        print "Will use multiprocessing with {} threads to process the FRs:".format(n_threads)
        print "  {}\n".format(args.FRs)
        lock = Lock()
        p = [None]*n_threads
        for FR in args.FRs:
            for i,x in cycle(enumerate(p)):
                if x is None or not x.is_alive(): break
            _args = copy(args)
            _args.FRs = [FR]
            p[i] = Process(target=computeFFactors, args=(_args, lock))
            p[i].start()
        while any(x is not None and x.is_alive() for x in p):
            # some threads are still alive, wait for 1 second and check again
            sleep(1)
        return

    inputs.useAlt(args.alt_bkgs)

    for FR in args.FRs:
        output_file_name = os.path.join(args.output_dir, args.tag, "FFactors_{}.root".format(FR))
        if os.path.exists(output_file_name) and not args.force_recreate:
            print "File '{}' already exists. Skipping. (Use '--force-recreate' to force recreation of the file.)".format(output_file_name)
            continue

        region = regions[FR]
        if not region.type == "FR" or region.background is None:
            raise Exception("Region '{}' is not a FR (or it has no target background specified).".format(FR))

        target = region.background
        if target == "QCD":
            target_bkgs = []
            other_bkgs = inputs.bkgs.values()
        elif target == "ALL":
            target_bkgs = inputs.bkgs.values()
            other_bkgs = []
        else:
            target_bkgs = [inputs.bkgs[target]]
            other_bkgs = [b for b in inputs.bkgs.values() if not b.name == target]
        data = inputs.data

        region_pass = region
        cuts_pass = region_pass.selection
        weights = region_pass.weight_string(lumi=inputs.lumi*1000.)

        region_fail = failRegion(region_pass, args.BDT_ID)
        cuts_fail = region_fail.selection

        with acquire(lock):
            print "="*30
            print "Processing '{}'\n".format(FR)
            print "  Target background: {}\n".format(target)
            print "  selection (pass): {}\n".format(cuts_pass)
            print "  selection (fail): {}\n".format(cuts_fail)
            print "  weights: {}\n".format(weights)

        if args.split_mode == "1p3p":
            tau_modes = ["1p", "3p"]
            extra_cuts = {"1p": "taus_n_tracks[0]==1",
                          "3p": "taus_n_tracks[0]==3"}
            branches = {"1p": "tau_0_lead_track_pt:taus_pt[0]",  # 'y:x' not 'x:y'
                        "3p": "taus_pt[0]"}
            binnings = {"1p": [[25, 30, 40, 55, 80],  # tau pt
                               [0, 10, 20, 25, 30, 80]],  # tau track pt
                        "3p": [[25, 30, 40, 55, 80]]}
        elif args.split_mode == "1p0nXn3p":
            tau_modes = ["1p0n", "1pXn", "3p"]
            extra_cuts = {"1p0n": "taus_n_tracks[0]==1&&taus_decay_mode[0]==0",
                          "1pXn": "taus_n_tracks[0]==1&&taus_decay_mode[0]!=0",
                          "3p": "taus_n_tracks[0]==3"}
            branches = {"1p0n": "tau_0_lead_track_pt:taus_pt[0]",  # 'y:x' not 'x:y'
                        "1pXn": "tau_0_lead_track_pt:taus_pt[0]",  # 'y:x' not 'x:y'
                        "3p": "taus_pt[0]"}
            binnings = {"1p0n": [[20, 30, 40, 55, 100],  # tau pt
                                 [0, 10, 20, 25, 30, 100]],  # tau track pt
                        "1pXn": [[20, 30, 40, 55, 100],  # tau pt
                                 [0, 10, 20, 25, 30, 100]],  # tau track pt
                        "3p": [[20, 30, 40, 55, 100]]}

        for tau_mode in tau_modes:
            hists = getFFactorComponents(FR_name=FR+"_"+tau_mode,
                                         extra_cut=extra_cuts[tau_mode],
                                         branch=branches[tau_mode],
                                         binnings=binnings[tau_mode],
                                         cuts_pass=cuts_pass,
                                         cuts_fail=cuts_fail,
                                         truth_sel=region.truth_selection,
                                         weights=weights,
                                         data=data,
                                         target_bkgs=target_bkgs,
                                         other_bkgs=other_bkgs,
                                         NN_tag=args.NN_tag,
                                         lock=lock,
                                        )

            getFFactors(hists, "F_{}_{}".format(target, tau_mode))
            if not target == "QCD":
                getkFactors(hists, "k_pass_{}_{}".format(target, tau_mode), "pass")
                getkFactors(hists, "k_fail_{}_{}".format(target, tau_mode), "fail")

            saveRootObjects(hists.values(), output_file_name, "UPDATE")

        with acquire(lock):
            if lock: print "="*30
            print "Completed processing '{}, {}'\n".format(FR, tau_mode)
            if tau_mode == tau_modes[-1]:
                print "'{}' Done!\n".format(FR)

    print "="*30

#-------------------------------------------------------------------------------
def correctFFactors(args):
    regions = Regions(args.regions_db)

    if args.split_mode == "1p3p":
        tau_modes = ["1p", "3p"]
    elif args.split_mode == "1p0nXn3p":
        tau_modes = ["1p0n", "1pXn", "3p"]

    for FR in args.FRs:
        print "Appling k-factor correction to FFs in '{}'".format(FR)

        # Try to find all the files first
        all_FRs = [regions[r] for r in regions[FR].fakes_regions]
        file_names = {}
        all_files_found = True
        for r in all_FRs:
            file_names[r.name] = os.path.join(args.output_dir, args.tag, "FFactors_{}.root".format(r.name))
            if not os.path.exists(file_names[r.name]) and not r.background == "QCD":
                print "WARNING: Could not found '{}'".format(file_name)
                all_files_found = False
        if not all_files_found:
            print "WARNING: Skip k-factor correction for FFs in '{}'".format(FR)
            continue

        for tau_mode in tau_modes:
            # First correct the 'pass' and 'fail' MC terms separately, and update the ROOT file
            for category in ["pass", "fail"]:
                # Read all the k-factors
                k = {}
                all_k_factors_found = True
                for r in all_FRs:
                    if r.background == "QCD":
                        continue
                    with root_open(file_names[r.name]) as f:
                        hist_name = "k_{}_{}_{}".format(category, r.background, tau_mode)
                        h = f.Get(hist_name+"_without_k")
                        if not h:
                            h = f.Get(hist_name)
                        if not h:
                            print "WARNING: Could not read k-factors '{}' from '{}'".format(hist_name, file_names[r.name])
                            all_k_factors_found = False
                        h.SetDirectory(0)
                        k[r.background] = h
                if not all_k_factors_found:
                    print "WARNING: Skip k-factor correction for FFs in '{} {}'".format(FR, tau_mode)
                    continue

                with root_open(file_names[FR], "UPDATE") as f:
                    prefix = "{}_{}_{}_".format(FR, tau_mode, category)
                    prefix_real = prefix + "real_"
                    prefix_fake = prefix + "fake_"

                    keys = [key.GetName() for key in f.GetListOfKeys()]
                    for key in keys:
                        if not key.startswith(prefix_fake):
                            continue
                        if key+"_without_k" in keys:
                            continue
                        background = key.replace(prefix_fake, "").replace("_without_k", "")
                        if not background in k:
                            continue

                        h = f.Get(key)

                        if not key.endswith("_without_k"):
                            h_original = h.Clone()
                            h_original.SetNameTitle(key+"_without_k", key+"_without_k")
                            h_original.Write("")

                        h.Multiply(k[background])
                        h_name = h.GetName()
                        if key.endswith("_without_k"):
                            h.SetNameTitle(key.replace("_without_k", ""), key.replace("_without_k", ""))
                        h.Write("", TObject.kOverwrite)

                    key = prefix + "otherMC"
                    if key+"_without_k" in keys:
                        key += "_without_k"
                    h_total = f.Get(key)

                    if not key.endswith("_without_k"):
                        h_total_original = h_total.Clone()
                        h_total_original.SetNameTitle(key+"_without_k", key+"_without_k")
                        h_total_original.Write("", TObject.kOverwrite)

                    h_total.Reset()
                    for _key in keys:
                        if not (_key.startswith(prefix_real) or _key.startswith(prefix_fake)):
                            continue
                        if _key.endswith("targetMC"):
                            continue
                        if _key.endswith("_without_k"):
                            continue
                        h_total.Add(f.Get(_key))
                    if key.endswith("_without_k"):
                        h_total.SetNameTitle(key.replace("_without_k", ""), key.replace("_without_k", ""))
                    h_total.Write("", TObject.kOverwrite)

            # Now correct the F factors
            key = "F_{}_{}".format(regions[FR].background, tau_mode)
            if key+"_without_k" not in keys:
                with root_open(file_names[FR], "UPDATE") as f:
                    h = f.Get(key).Clone()
                    h.SetNameTitle(key+"_without_k", key+"_without_k")
                    h.Write("", TObject.kOverwrite)
            hists = getFFactorComponentsFromCache(file_names[FR], FR, tau_mode)
            getFFactors(hists, key)
            saveRootObjects(hists.values(), file_names[FR], "UPDATE", "Overwrite")

        print "'{}' Done!\n".format(FR)

    print "="*30

#-------------------------------------------------------------------------------
def computeCombinedFFactors(args):
    regions = Regions(args.regions_db)

    FRs = list(set([r.replace("_el","_comb").replace("_mu","_comb") for r in args.FRs]))

    if args.split_mode == "1p3p":
        tau_modes = ["1p", "3p"]
    elif args.split_mode == "1p0nXn3p":
        tau_modes = ["1p0n", "1pXn", "3p"]

    for FR in FRs:
        print "Computing FFs in '{}'".format(FR)
        region = regions[FR]
        target = region.background
        output_file_name = os.path.join(args.output_dir, args.tag, "FFactors_{}.root".format(FR))

        subFRs = [FR.replace("comb", "el")]
        #if not FR.startswith("FRQ"):
        #    # FRQ is not pure in the muon channel, so we skip it in the combined FF approach
        #    subFRs.append(FR.replace("comb", "mu"))
        subFRs.append(FR.replace("comb", "mu"))

        hists = defaultdict(OrderedDict)

        for subFR in subFRs:
            cache_file_name = os.path.join(args.output_dir, args.tag, "FFactors_{}.root".format(subFR))

            for tau_mode in tau_modes:
                print cache_file_name, subFR, tau_mode
                sub_hists = getFFactorComponentsFromCache(cache_file_name, subFR, tau_mode)
                for h in sub_hists:
                    if h in hists[tau_mode]:
                        hists[tau_mode][h].Add(sub_hists[h])
                    else:
                        hists[tau_mode][h] = sub_hists[h]

        for tau_mode in tau_modes:
            getFFactors(hists[tau_mode], "F_{}_{}".format(target, tau_mode))
            if not target == "QCD":
                getkFactors(hists[tau_mode], "k_pass_{}_{}".format(target, tau_mode), "pass")
                getkFactors(hists[tau_mode], "k_fail_{}_{}".format(target, tau_mode), "fail")

            for h in hists[tau_mode].values():
                if "_el_" in h.GetName():
                    h.SetName(h.GetName().replace("_el_", "_comb_"))
                if "_el_" in h.GetTitle():
                    h.SetTitle(h.GetTitle().replace("_el_", "_comb_"))
            saveRootObjects(hists[tau_mode].values(), output_file_name, "UPDATE")

        print "'{}' Done!\n".format(FR)

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    computeFFactors(args)
    correctFFactors(args)  # apply k-factor corrections
    computeCombinedFFactors(args)
