# Configs
This directory contains the baseline config/database files that are read by the analysis scripts.

If you want to create temporary, local config/database files, you may add a leading underscore (e.g. ``_regions_test.yaml``) to prevent the file from being tracked by git and contaminating the repository.
