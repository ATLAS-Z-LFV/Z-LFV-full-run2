#!/usr/bin/env python

import os, sys
import argparse

import numpy as np
from numpy.lib.recfunctions import drop_fields, append_fields

from modules.Inputs import Inputs, InputFile
from modules.general_utils import findDefaultConfigs, checkMakeDirs, flattenCommaSeparatedLists
from modules.general_utils import root_open
from modules.plot_utils import removeHistErrors, findHistValues, getMaxBinIndex

from ROOT import gROOT, TH1D
TH1D.SetDefaultSumw2()
from root_numpy import tree2array, array2tree


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser("./decorateZPtSFs.py")

    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")

    parser.add_argument("--output-dir", "-o", default="", type=str, help="Output directory. Default = input directory.")

    parser.add_argument("--sf-file", default="ZWeights/fiducial_Z_pT_SFs.root", type=str, help="File containing the Z pT scale factor histograms.")

    parser.add_argument("--Zll-sf-hist", default="Zll_truth_Z_pT_SFs", type=str, help="Name of the Zll scale factor histogram. Skip reweighting Zll if empty.")
    parser.add_argument("--Ztt-sf-hist", default="Ztautau_truth_Z_pT_SFs", type=str, help="Name of the Ztautau scale factor histogram. Skip reweighting Ztautau if empty.")
    parser.add_argument("--sig-sf-hist", default="Ztaulep_truth_Z_pT_SFs", type=str, help="Name of the signal scale factor histogram. Skip reweighting signals if empty.")

    parser.add_argument("--add-mu-fakes-SFs", action="store_true", default=False, help="Decorate mu-to-tau fakes scale factors as well.")
    parser.add_argument("--mu-fakes-sf-file", default="ZWeights/mu_to_tau_fakes_SFs.root", type=str, help="File containing the mu-to-tau fakes scale factor histogram.")
    parser.add_argument("--mu-fakes-sf-hist", default="mu_to_tau_fakes_pt_vs_eta_SFs", type=str, help="Name of the mu-to-tau fakes scale factor histogram.")

    parser.add_argument("--add-el-fakes-SFs", action="store_true", default=False, help="Decorate e-to-tau fakes scale factors as well.")
    parser.add_argument("--el-fakes-sf-file", default="ZWeights/el_to_tau_fakes_SFs.root", type=str, help="File containing the e-to-tau fakes scale factor histogram.")
    parser.add_argument("--el-fakes-sf-hist", default="el_to_tau_fakes_pt_vs_eta_SFs", type=str, help="Name of the e-to-tau fakes scale factor histogram.")

    parser.add_argument("--region", "-r", dest="regions", action='append', default=[], type=str, help="Only decorate tree(s) of the specified region(s). Decorate all trees if not specified.")
    parser.add_argument("--no-syst", action='store_true', default=False, help="Skip trees for systematic variations.")
    parser.add_argument("--use-full-trees", action="store_true", default=False, help="Use full trees instead of region trees.")

    parser.add_argument("--keep-old-cycles", action='store_true', default=False, help="Keep old cycles. This keeps tree backups, but might waste disk space.")

    #parser.add_argument("--taulep", action='store_true', default=False, help="Treat leptonically decaying taus. Can only be used together with use-full-trees. ")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args, default_split_inputs=True)

        if args.use_full_trees:
            args.regions = None
        else:
            args.regions = flattenCommaSeparatedLists(args.regions)

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))

    if args.output_dir:
        checkMakeDirs(args.output_dir)

#-------------------------------------------------------------------------------
def decorateZPtSFs(args):
    gROOT.SetBatch(True)

    inputs = Inputs(args.inputs_db)

    if not args.output_dir:
        args.output_dir = inputs.dir

    with root_open(args.sf_file) as f:
        Zll_SFs = None
        if args.Zll_sf_hist in f.GetListOfKeys():
            Zll_SFs = f.Get(args.Zll_sf_hist)
            Zll_SFs.SetDirectory(0)
        Ztt_SFs = None
        if args.Ztt_sf_hist in f.GetListOfKeys():
            Ztt_SFs = f.Get(args.Ztt_sf_hist)
            Ztt_SFs.SetDirectory(0)
        sig_SFs = None
        if args.sig_sf_hist in f.GetListOfKeys():
            sig_SFs = f.Get(args.sig_sf_hist)
            sig_SFs.SetDirectory(0)

    if args.add_mu_fakes_SFs:
        with root_open(args.mu_fakes_sf_file) as f:
            mu_fakes_SFs = None
            if args.mu_fakes_sf_hist in f.GetListOfKeys():
                mu_fakes_SFs = f.Get(args.mu_fakes_sf_hist)
                mu_fakes_SFs.SetDirectory(0)

    if args.add_el_fakes_SFs:
        with root_open(args.el_fakes_sf_file) as f:
            el_fakes_SFs = None
            if args.el_fakes_sf_hist in f.GetListOfKeys():
                el_fakes_SFs = f.Get(args.el_fakes_sf_hist)
                el_fakes_SFs.SetDirectory(0)
            ### tauCP SFs / VRZll non-closure uncert
            #el_fakes_SFs_stat = None
            #if args.el_fakes_sf_hist + "_stat" in f.GetListOfKeys():
            #    el_fakes_SFs_stat = f.Get(args.el_fakes_sf_hist + "_stat")
            #    el_fakes_SFs_stat.SetDirectory(0)
            #el_fakes_SFs_syst = None
            #if args.el_fakes_sf_hist + "_syst" in f.GetListOfKeys():
            #    el_fakes_SFs_syst = f.Get(args.el_fakes_sf_hist + "_syst")
            #    el_fakes_SFs_syst.SetDirectory(0)

    #-------------------------------------------------
    # Decorate Signals
    if sig_SFs is not None:
        print "Decorating signals..."
        sf = sig_SFs
        sf_stat_up, sf_stat_down = statToSyst(sf)

        for file,tree in getListOfTrees(inputs["Ztaumu"].values() + inputs["Ztaue"].values(), args.regions):
            with root_open(file) as f:
                t = f.Get(tree)
                if t.GetEntries() == 0:
                    print "  Skipping '{}' with zero entries".format(tree)
                    continue
                else:
                    print "  Decorating '{}'".format(tree)
                arr = tree2array(t, ["runNumber", "eventNumber", "truth_Z_pT"])
            truth_Z_pT = arr["truth_Z_pT"]
            arr = drop_fields(arr, "truth_Z_pT", usemask=False)

            nom = findHistValues(sf, truth_Z_pT)
            up = findHistValues(sf_stat_up, truth_Z_pT)
            down = findHistValues(sf_stat_down, truth_Z_pT)

            arr = append_fields(arr, ["ZWeight_NOMINAL", "ZWeight_Z_REWEIGHTING_SIG_STAT_1up", "ZWeight_Z_REWEIGHTING_SIG_STAT_1down"], [nom, up, down], usemask=False)
            out_file = os.path.join(args.output_dir, os.path.basename(file)+".ZWeights.friend")
            with root_open(out_file, "UPDATE") as f:
                fd_t = array2tree(arr, "ZWeights_{}".format(tree))
                ###fd_t.BuildIndex("runNumber", "eventNumber")
                if args.keep_old_cycles:
                    f.WriteTObject(fd_t)
                else:
                    f.WriteTObject(fd_t, "", "Overwrite")

    #-------------------------------------------------
    # Decorate Ztautau
    if Ztt_SFs is not None:
        print "Decorating Ztautau..."
        sf = Ztt_SFs
        sf_stat_up, sf_stat_down = statToSyst(sf)

        for file,tree in getListOfTrees(inputs["Ztautau"].values(), args.regions):
            with root_open(file) as f:
                t = f.Get(tree)
                if t.GetEntries() == 0:
                    print "  Skipping '{}' with zero entries".format(tree)
                    continue
                else:
                    print "  Decorating '{}'".format(tree)
                arr = tree2array(t, ["runNumber", "eventNumber", "truth_Z_pT"])
            truth_Z_pT = arr["truth_Z_pT"]
            arr = drop_fields(arr, "truth_Z_pT", usemask=False)

            nom = findHistValues(sf, truth_Z_pT)
            up = findHistValues(sf_stat_up, truth_Z_pT)
            down = findHistValues(sf_stat_down, truth_Z_pT)

            arr = append_fields(arr, ["ZWeight_NOMINAL", "ZWeight_Z_REWEIGHTING_ZTAUTAU_STAT_1up", "ZWeight_Z_REWEIGHTING_ZTAUTAU_STAT_1down"], [nom, up, down], usemask=False)
            out_file = os.path.join(args.output_dir, os.path.basename(file)+".ZWeights.friend")
            with root_open(out_file, "UPDATE") as f:
                fd_t = array2tree(arr, "ZWeights_{}".format(tree))
                ###fd_t.BuildIndex("runNumber", "eventNumber")
                if args.keep_old_cycles:
                    f.WriteTObject(fd_t)
                else:
                    f.WriteTObject(fd_t, "", "Overwrite")

    #-------------------------------------------------
    # Decorate Zll
    if Zll_SFs is not None:
        print "Decorating Zll..."
        sf = Zll_SFs
        sf_stat_up, sf_stat_down = statToSyst(sf)

        if args.add_mu_fakes_SFs:
            mu_fakes_sf = mu_fakes_SFs
            mu_fakes_stat_up, mu_fakes_stat_down = statToSyst(mu_fakes_sf)

        if args.add_el_fakes_SFs:
            el_fakes_sf = el_fakes_SFs
            el_fakes_sf_varied = []
            el_fakes_syst_names = []

            ### VRZll SFs
            # correlated stat errors
            el_fakes_stat_up, el_fakes_stat_down = statToSyst(el_fakes_sf)
            el_fakes_sf_varied += [el_fakes_stat_up, el_fakes_stat_down]
            el_fakes_syst_names += ["TAUS_TRUEELECTRON_1P_EFF_SF_STAT_1up", "TAUS_TRUEELECTRON_1P_EFF_SF_STAT_1down"]

            ### VRZll SFs split stat errors
            #N_eta = el_fakes_sf.GetNbinsX()
            #N_pt = el_fakes_sf.GetNbinsY()
            #for iPt in xrange(1, N_pt+1):
            #    for iEta in xrange(1, N_eta+1):
            #        idx = el_fakes_sf.GetBin(iEta, iPt)
            #        val = el_fakes_sf.GetBinContent(idx)
            #        err = el_fakes_sf.GetBinError(idx)
            #
            #        syst = "TAUS_TRUEELECTRON_1P_EFF_SF_PTBIN{}_ETABIN{}_STAT_1up".format(iPt, iEta)
            #        varied_sf = el_fakes_sf.Clone()
            #        varied_sf.SetBinContent(idx, val+err)
            #        el_fakes_sf_varied.append(varied_sf)
            #        el_fakes_syst_names.append(syst)
            #
            #        syst = "TAUS_TRUEELECTRON_1P_EFF_SF_PTBIN{}_ETABIN{}_STAT_1down".format(iPt, iEta)
            #        varied_sf = el_fakes_sf.Clone()
            #        varied_sf.SetBinContent(idx, val-err)
            #        el_fakes_sf_varied.append(varied_sf)
            #        el_fakes_syst_names.append(syst)

            ### tauCP SFs / VRZll non-closure uncert
            #el_fakes_stat_up = el_fakes_sf.Clone()
            #el_fakes_stat_up.Add(el_fakes_SFs_stat, 1.)
            #el_fakes_stat_down = el_fakes_sf.Clone()
            #el_fakes_stat_down.Add(el_fakes_SFs_stat, -1.)
            #el_fakes_sf_varied += [el_fakes_stat_up, el_fakes_stat_down]
            #el_fakes_syst_names += ["TAUS_TRUEELECTRON_1P_EFF_ELEBDT_STAT_1up","TAUS_TRUEELECTRON_1P_EFF_ELEBDT_STAT_1down"]
            #if el_fakes_SFs_syst is not None:
            #    el_fakes_syst_up = el_fakes_sf.Clone()
            #    el_fakes_syst_up.Add(el_fakes_SFs_syst, 1.)
            #    el_fakes_syst_down = el_fakes_sf.Clone()
            #    el_fakes_syst_down.Add(el_fakes_SFs_syst, -1.)
            #    el_fakes_sf_varied += [el_fakes_syst_up, el_fakes_syst_down]
            #    el_fakes_syst_names += ["TAUS_TRUEELECTRON_1P_EFF_ELEBDT_SYST_1up","TAUS_TRUEELECTRON_1P_EFF_ELEBDT_SYST_1down"]

        for file,tree in getListOfTrees(inputs["Zll"].values(), args.regions):
            with root_open(file) as f:
                t = f.Get(tree)
                if t.GetEntries() == 0:
                    print "  Skipping '{}' with zero entries".format(tree)
                    continue
                else:
                    print "  Decorating '{}'".format(tree)
                if (args.add_mu_fakes_SFs or args.add_el_fakes_SFs) and not "NRZll" in tree:
                    arr = tree2array(t, ["runNumber", "eventNumber", "truth_Z_pT", "taus_pt[0]", "taus_eta[0]"])
                else:
                    arr = tree2array(t, ["runNumber", "eventNumber", "truth_Z_pT"])
            truth_Z_pT = arr["truth_Z_pT"]
            arr = drop_fields(arr, "truth_Z_pT", usemask=False)
            if (args.add_mu_fakes_SFs or args.add_el_fakes_SFs) and not "NRZll" in tree:
                tau_pT = arr["taus_pt[0]"]
                tau_abs_eta = np.abs(arr["taus_eta[0]"])
                arr = drop_fields(arr, ["taus_pt[0]", "taus_eta[0]"], usemask=False)

            nom = findHistValues(sf, truth_Z_pT)
            up = findHistValues(sf_stat_up, truth_Z_pT)
            down = findHistValues(sf_stat_down, truth_Z_pT)

            arr = append_fields(arr, ["ZWeight_NOMINAL", "ZWeight_Z_REWEIGHTING_ZLL_STAT_1up", "ZWeight_Z_REWEIGHTING_ZLL_STAT_1down"], [nom, up, down], usemask=False)

            if args.add_mu_fakes_SFs and not "NRZll" in tree:
                mu_fakes_nom = findHistValues(mu_fakes_sf, tau_abs_eta, tau_pT)
                mu_fakes_up = findHistValues(mu_fakes_stat_up, tau_abs_eta, tau_pT)
                mu_fakes_down = findHistValues(mu_fakes_stat_down, tau_abs_eta, tau_pT)

                field_names = ["muTauFakesSF_NOMINAL", "muTauFakesSF_TAUS_TRUEMUON_1P_EFF_SF_STAT_1up", "muTauFakesSF_TAUS_TRUEMUON_1P_EFF_SF_STAT_1down"]
                field_arrays = [mu_fakes_nom, mu_fakes_up, mu_fakes_down]

                arr = append_fields(arr, field_names, field_arrays, usemask=False)

            if args.add_el_fakes_SFs and not "NRZll" in tree:
                field_names = ["elTauFakesSF_NOMINAL"]
                field_arrays = [findHistValues(el_fakes_sf, tau_abs_eta, tau_pT)]
                for syst_name in el_fakes_syst_names:
                    field_names.append("elTauFakesSF_"+syst_name)
                for sf_varied in el_fakes_sf_varied:
                    field_arrays.append(findHistValues(sf_varied, tau_abs_eta, tau_pT))
                arr = append_fields(arr, field_names, field_arrays, usemask=False)

            out_file = os.path.join(args.output_dir, os.path.basename(file)+".ZWeights.friend")
            with root_open(out_file, "UPDATE") as f:
                fd_t = array2tree(arr, "ZWeights_{}".format(tree))
                ###fd_t.BuildIndex("runNumber", "eventNumber")
                if args.keep_old_cycles:
                    f.WriteTObject(fd_t)
                else:
                    f.WriteTObject(fd_t, "", "Overwrite")

    print "Done!"

#-------------------------------------------------------------------------------
def getListOfTrees(files, regions=None):
    trees = []
    for file in files:
        if isinstance(file, InputFile):
            file = file.name
        with root_open(file) as f:
            for t in f.GetListOfKeys():
                if not t.GetClassName() == "TTree":
                    continue
                tree = t.GetName()
                if tree == "weights":
                    continue
                if args.no_syst and not tree.endswith("_NOMINAL"):
                    continue
                #if "NRZll" in tree:
                #    continue
                if regions and next((False for r in regions if r in tree), True):
                    continue
                trees.append((file, tree))
    return trees

#-------------------------------------------------------------------------------
def statToSyst(hist):
    up = hist.Clone(hist.GetName()+"_stat_up")
    down = hist.Clone(hist.GetName()+"_stat_down")
    max_idx = getMaxBinIndex(hist)
    for i in xrange(max_idx+1):
        val = hist.GetBinContent(i)
        err = hist.GetBinError(i)
        if val<1: err *= -1
        up.SetBinContent(i, val+err)
        down.SetBinContent(i, val-err)
    return up, down

#-------------------------------------------------------------------------------
def calculateRecoZPt(p1_pt, p1_phi, p2_pt, p2_phi):
    p1_pt = p1_pt.astype(float)
    p1_phi = p1_phi.astype(float)
    p2_pt = p2_pt.astype(float)
    p2_phi = p2_phi.astype(float)
    # np.hypot(x1,x2) = sqrt(x1**2 + x2**2)
    return np.hypot(p1_pt*np.cos(p1_phi) + p2_pt*np.cos(p2_phi),
                    p1_pt*np.sin(p1_phi) + p2_pt*np.sin(p2_phi))

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    decorateZPtSFs(args)
