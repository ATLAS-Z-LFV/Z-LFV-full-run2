#!/usr/bin/env python

import os, sys
import argparse

from ROOT import *

from modules.Regions import Regions
from modules.general_utils import findDefaultConfigs, flattenCommaSeparatedLists, checkMakeDirs
from modules.general_utils import root_open
from modules.fakes_utils import *

#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser(prog='./plotFF.py')

    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")

    parser.add_argument("--tag", "-t", required=True, default=None, type=str, help="The fake factor tag.")
    parser.add_argument("--region", "-r", dest="regions", default=[], type=str, action='append', help="Region(s) to plot the R-factors.")
    parser.add_argument("--output-dir", "-o", default=".", type=str, help="Output directory for writing the split trees. Default = input directory.")
    parser.add_argument("--split-mode", type=str, choices=["1P3P", "1p3p", "1p0nXn3p"], default="1p3p", help="How to split tau_had decay mode ('1P3P', '1p3p' or '1p0nXn3p').")

    parser.add_argument("--allow-out-of-range-R", default=False, action='store_true', help="Do not normalise k*R, i.e. allow k*R to be negative or beyond unity.")

    parser.add_argument("--no-atlas-label", action="store_true", default=False, help="Remove the ATLAS label (for thesis).")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args, default_split_inputs=True)

        args.regions = flattenCommaSeparatedLists(args.regions)

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    checkMakeDirs(args.output_dir)

#-------------------------------------------------------------------------------
def plotFF(args):

    regions = Regions(args.regions_db)

    gROOT.SetBatch()
    gStyle.SetOptStat(0)
    gStyle.SetPalette(8)
    gStyle.SetPaintTextFormat("4.2f")

    bkgs = ["Wjets", "Zll", "top", "QCD"]
    map = {"Wjets":"W", "Zll":"Z", "top":"top", "QCD":"QCD"}
    FRs_comb = ["FRW_comb", "FRZll_comb", "FRT_comb", "FRQ_comb"]
    FRs_el = ["FRW_el", "FRZll_el", "FRT_el", "FRQ_el"]
    FRs_mu = ["FRW_mu", "FRZll_mu", "FRT_mu", "FRQ_mu"]
    SS_FRs = ["SS_"+FR for FR in FRs_comb + FRs_el + FRs_mu]

    ch_labels = {"el": "#it{e#tau} ", "mu": "#it{#mu#tau} ", "comb": ""}
    ch_suffixes = {"el": "_el", "mu": "_mu", "comb": ""}

    if args.split_mode == "1P3P":
        tau_modes = ["1P", "3P"]
    elif args.split_mode == "1p3p":
        tau_modes = ["1p", "3p"]
    elif args.split_mode == "1p0nXn3p":
        tau_modes = ["1p0n", "1pXn", "3p"]
    draw = {}
    for m in tau_modes:
        if m.startswith("1"):
            draw[m] = draw2D
        else:
            draw[m] = draw1D

    #-------------------------------------------------
    # Plot F-factors
    for FR in FRs_comb + FRs_el + FRs_mu + SS_FRs:
        bkg = regions[FR].background
        prefix = "SS_" if FR.startswith("SS_") else ""
        ch = FR.split("_")[-1]
        file = "fakes/{}/FFactors_{}.root".format(args.tag, FR)
        with root_open(file) as f:
            for m in tau_modes:
                h = f.Get("F_{}_{}".format(bkg, m))
                draw[m](h, "#it{F}^{%s} (%s%s)" % (map[bkg], ch_labels[ch], m.upper()),
                        os.path.join(args.output_dir, "{}F_{}{}_{}.pdf".format(prefix, bkg, ch_suffixes[ch], m)))

    #-------------------------------------------------
    # Plot k-factors
    for FR in FRs_comb + FRs_el + FRs_mu + SS_FRs:
        bkg = regions[FR].background
        if bkg == "QCD":
            continue
        prefix = "SS_" if FR.startswith("SS_") else ""
        ch = FR.split("_")[-1]
        file = "fakes/{}/FFactors_{}.root".format(args.tag, FR)
        with root_open(file) as f:
            for m in tau_modes:
                h = f.Get("k_pass_{}_{}".format(bkg, m))
                draw[m](h, "#it{k}^{%s}_{pass} (%s%s)" % (map[bkg], ch_labels[ch], m.upper()),
                        os.path.join(args.output_dir, "{}k_pass_{}{}_{}.pdf".format(prefix, bkg, ch_suffixes[ch], m)))
                h = f.Get("k_fail_{}_{}".format(bkg, m))
                draw[m](h, "#it{k}^{%s}_{fail} (%s%s)" % (map[bkg], ch_labels[ch], m.upper()),
                        os.path.join(args.output_dir, "{}k_fail_{}{}_{}.pdf".format(prefix, bkg, ch_suffixes[ch], m)))

    #-------------------------------------------------
    # Plot k*R
    for r in args.regions:
        region = regions[r]

        k = {}
        R = {}
        for m in tau_modes:
            k[m] = {}
            R[m] = {}

        for FR in region.fakes_regions:
            bkg = regions[FR].background
            if bkg=="QCD":
                continue  # QCD does not have k-factors
            file = "fakes/{}/FFactors_{}.root".format(args.tag, FR)
            with root_open(file) as f:
                for m in tau_modes:
                    k[m][bkg] = f.Get("k_fail_{}_{}".format(bkg, m))
                    k[m][bkg].SetDirectory(0)

        file = "fakes/{}/RFactors_{}.root".format(args.tag, r)
        with root_open(file) as f:
            for m in tau_modes:
                for bkg in k[m]:
                    R[m][bkg] = f.Get("R_{}_{}_{}".format(bkg, r, m))
                    R[m][bkg].SetDirectory(0)

        for m in tau_modes:
            for bkg in R[m]:
                R[m][bkg].Multiply(k[m][bkg])
                if not args.allow_out_of_range_R:
                    keepRFactorsInRange(R[m].values())

        for m in tau_modes:
            R[m]["QCD"] = getRFactorsQCD(R[m].values())

        ## Don't need stat error on R
        #removeHistErrors(R_1p.values())
        #removeHistErrors(R_3p.values())

        for m in tau_modes:
            for bkg, h in R[m].iteritems():
                #if bkg=="QCD":
                #    title = "Rederived #it{R}^{QCD}_{%s %s}" % (region.root_title, m.upper())
                #else:
                #    title = "#it{k}^{%s}_{fail}#times#it{R}^{%s}_{%s %s}" % (
                #             map[bkg], map[bkg], region.root_title, m.upper())
                title = "#it{R}^{%s}_{%s %s}" % (map[bkg], region.root_title, m.upper())
                draw[m](h, title,
                        os.path.join(args.output_dir, "R_{}_{}_{}.pdf".format(bkg, r, m)),
                       )

    print "Done!!"
    print "(If you reached here, it should be safe to ignore the subsequent error(s).)"

#-------------------------------------------------------------------------------
def draw2D(h, title, output_name):
    c = TCanvas("c", "c", 800, 700)
    c.SetLeftMargin(0.12)
    c.SetBottomMargin(0.10)
    c.SetRightMargin(0.15)
    c.SetTopMargin(0.05)

    h.SetTitle("")
    h.SetMarkerColor(kBlack)
    h.SetMarkerSize(1.5)

    h.GetXaxis().SetTitle("#it{p}_{T}(#it{#tau}_{had-vis}) [GeV]")
    h.GetXaxis().SetTitleSize(0.042)
    h.GetXaxis().SetLabelSize(0.037)
    h.GetXaxis().SetNdivisions(010)

    h.GetYaxis().SetTitle("#it{p}_{T}(#it{#tau} track) [GeV]")
    h.GetYaxis().SetTitleSize(0.042)
    h.GetYaxis().SetLabelSize(0.037)
    h.GetYaxis().SetNdivisions(010)

    h.SetMaximum(2 * h.GetMaximum())
    h.GetZaxis().SetRange(11)

    h.Draw("colz")

    l = TLine()
    l.SetLineStyle(2)
    for i in range(h.GetNbinsX()):
        x = h.GetXaxis().GetBinLowEdge(i+1)
        y_lo = h.GetYaxis().GetBinLowEdge(1)
        y_hi = h.GetYaxis().GetBinLowEdge(h.GetNbinsY()+1)
        l.DrawLine(x, y_lo, x, y_hi)
    for j in range(h.GetNbinsY()):
        y = h.GetYaxis().GetBinLowEdge(j+1)
        x_lo = h.GetXaxis().GetBinLowEdge(1)
        x_hi = h.GetXaxis().GetBinLowEdge(h.GetNbinsX()+1)
        l.DrawLine(x_lo, y, x_hi, y)

    h.Draw("text E same")

    if not args.no_atlas_label:
        atlas_label = TPaveText(0.13, 0.9, 0.5, 0.94, "NDC")
        atlas_label.SetBorderSize(0)
        atlas_label.SetFillStyle(0)
        atlas_label.SetLineStyle(0)
        text = atlas_label.AddText("#font[72]{ATLAS} #font[42]{Internal}")
        text.SetTextSize(0.045)
        text.SetTextAlign(13)
        atlas_label.Draw()

    title_label = TPaveText(0.15, 0.92, 0.87, 0.96, "NDC")
    title_label.SetBorderSize(0)
    title_label.SetFillStyle(0)
    title_label.SetLineStyle(0)
    text = title_label.AddText("#font[42]{%s}" % title)
    text.SetTextSize(0.045)
    text.SetTextAlign(33)
    title_label.Draw()

    c.SaveAs(output_name)

    del c

#-------------------------------------------------------------------------------
def draw1D(h, title, output_name):
    c = TCanvas("c", "c", 800, 700)
    c.SetLeftMargin(0.18)
    c.SetBottomMargin(0.10)
    c.SetRightMargin(0.09)
    c.SetTopMargin(0.05)

    h.SetTitle("")
    h.SetLineColor(kBlue)
    h.SetLineWidth(2)
    h.SetMarkerColor(kRed)
    h.SetMarkerSize(1.5)

    h.SetMaximum(1.5 * h.GetMaximum())
    h.SetMinimum(0)

    h.GetXaxis().SetTitle("#it{p}_{T}(#it{#tau}_{had-vis}) [GeV]")
    h.GetXaxis().SetTitleSize(0.042)
    h.GetXaxis().SetLabelSize(0.037)

    h.GetYaxis().SetTitle(title)
    h.GetYaxis().SetTitleSize(0.042)
    h.GetYaxis().SetLabelSize(0.037)

    h.Draw("text0 E")

    if not args.no_atlas_label:
        atlas_label = TPaveText(0.19, 0.9, 0.56, 0.94, "NDC")
        atlas_label.SetBorderSize(0)
        atlas_label.SetFillStyle(0)
        atlas_label.SetLineStyle(0)
        text = atlas_label.AddText("#font[72]{ATLAS} #font[42]{Internal}")
        text.SetTextSize(0.045)
        text.SetTextAlign(13)
        atlas_label.Draw()

    c.SaveAs(output_name)

    del c

#-------------------------------------------------------------------------------
if __name__ == '__main__':
    args = parseArgs(sys.argv[1:])
    plotFF(args)

