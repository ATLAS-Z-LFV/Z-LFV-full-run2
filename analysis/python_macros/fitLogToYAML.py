#!/usr/bin/env python

import os, sys, yaml

with open(sys.argv[1], "r") as f:
    lines = f.readlines()

l = lines[-8]
val = float(l.strip("\n").split()[-3])
errors = l.strip("\n").split()[-2][1:-1].split(",")
err_hi = float(errors[0])
err_lo = float(errors[1])

d = {"value": val,
     "error_high": err_hi,
     "error_low": err_lo,
    }

with open(sys.argv[2], "w") as f:
    dump = yaml.dump(d, f, default_flow_style=False)
