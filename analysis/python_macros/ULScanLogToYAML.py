#!/usr/bin/env python

import os, sys, yaml

with open(sys.argv[1], "r") as f:
    lines = f.readlines()

for l in reversed(lines):
    if   "expected limit (median)" in l:
        exp_med = float(l.strip("\n").split()[-1])
    elif "expected limit (-1 sig)" in l:
        exp_m1  = float(l.strip("\n").split()[-1])
    elif "expected limit (+1 sig)" in l:
        exp_p1  = float(l.strip("\n").split()[-1])
    elif "expected limit (-2 sig)" in l:
        exp_m2  = float(l.strip("\n").split()[-1])
    elif "expected limit (+2 sig)" in l:
        exp_p2  = float(l.strip("\n").split()[-1])
    elif "The computed upper limit is:" in l:
        obs = float(l.strip("\n").split()[-3])
        break

d = {"observed": obs,
     "expected_m1sig": exp_m1,
     "expected_p1sig": exp_p1,
     "expected_m2sig": exp_m2,
     "expected_p2sig": exp_p2,
     "expected_median": exp_med,
    }

with open(sys.argv[2], "w") as f:
    dump = yaml.dump(d, f, default_flow_style=False)
