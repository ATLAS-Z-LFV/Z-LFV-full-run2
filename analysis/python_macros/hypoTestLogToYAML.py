#!/usr/bin/env python

import os, sys, yaml

with open(sys.argv[1], "r") as f:
    lines = f.readlines()

for l in reversed(lines):
    if   "Significance = " in l:
        sig = float(l.strip("\n").split()[-1])
    elif "Null p-value = " in l:
        p = float(l.strip("\n").split()[-1])
        break

d = {"p-value": p,
     "sigma": sig}

with open(sys.argv[2], "w") as f:
    dump = yaml.dump(d, f, default_flow_style=False)
