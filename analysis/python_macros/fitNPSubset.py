#!/usr/bin/env python

import os, sys, errno
from fnmatch import fnmatch
from ROOT import *
import numpy as np

gROOT.SetBatch()
gStyle.SetOptStat(0)

#in_file = "../results/fit_v37syst_unblindFit_SR_mu_fitOnly/Fit_Ztaumu_combined_NormalMeasurement_model_afterFit.root"
#fix_NPs = "alpha_Z*,alpha_*THEORY,alpha_LUMI,alpha_TAU*,alpha_E*,alpha_M*,alpha_MET*,alpha_JET*,alpha_BTAG*"  # stat uncertainties only
in_file = sys.argv[1]
fix_NPs = sys.argv[2]

f = TFile(in_file)

w = f.Get("w")
model = w.obj("ModelConfig")

all_params = {}

POIs = model.GetParametersOfInterest()
poi = POIs.createIterator().Next()  # Assuming there is only one POI
all_params[poi.GetName()] = poi

NPs = model.GetNuisanceParameters()
iter = NPs.createIterator()
while(True):
    np = iter.Next()
    if not np:
        break
    for pattern in fix_NPs.split(","):
        if fnmatch(np.GetName(), pattern):
            print "Will fix parameter '{}'".format(np.GetName())
            np.setConstant(True)
            break
    all_params[np.GetName()] = np

_InitialHesse = RooCmdArg(RooFit.InitialHesse(False))
_Hesse = RooCmdArg(RooFit.Hesse(False))
_Minimizer = RooCmdArg(RooFit.Minimizer("Minuit2", "Migrad"))
_Strategy = RooCmdArg(RooFit.Strategy(0))
_Strategy_alt = RooCmdArg(RooFit.Strategy(1))
_Verbose = RooCmdArg(RooFit.Verbose(0))
_PrintLevel = RooCmdArg(RooFit.PrintLevel(0+1))

constrainParams = RooArgSet()
constrainParams.add(NPs)
_Constrain = RooCmdArg(RooFit.Constrain(constrainParams))

fitArgs = RooLinkedList()
fitArgs.Add(_InitialHesse)
fitArgs.Add(_Hesse)
fitArgs.Add(_Minimizer)
fitArgs.Add(_Verbose)
fitArgs.Add(_PrintLevel)
fitArgs.Add(_Constrain)
fitArgs.Add(_Strategy)

minosParams = RooArgSet()
minosParams.add(poi)
_Minos = RooCmdArg(RooFit.Minos(minosParams))

fitArgsMinos = RooLinkedList()
fitArgsMinos.Add(_Minos)

model.GetPdf().fitTo(w.data("obsData"), fitArgs)
model.GetPdf().fitTo(w.data("obsData"), fitArgsMinos)
