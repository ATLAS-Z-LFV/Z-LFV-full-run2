#!/usr/bin/env python

import os, sys, errno
from ROOT import *
import numpy as np

gROOT.SetBatch()
gStyle.SetOptStat(0)

#in_file = "../results/fit_v37syst_unblindFit_SR_mu_fitOnly/Fit_Ztaumu_combined_NormalMeasurement_model_afterFit.root"
#out_dir = "./fitParameterProjections/TAUS_TRUEHADTAU_1P_SME_TES_INSITUFIT_FORWARD"
#varying_np = "alpha_TAUS_TRUEHADTAU_1P_SME_TES_INSITUFIT_FORWARD"
in_file = sys.argv[1]
out_dir = sys.argv[2]
varying_np = sys.argv[3]

scan_points = np.arange(-2.0, 2.1, 0.1)

f = TFile(in_file)

w = f.Get("w")
model = w.obj("ModelConfig")

all_params = {}

POIs = model.GetParametersOfInterest()
poi = POIs.createIterator().Next()  # Assuming there is only one POI
all_params[poi.GetName()] = poi

NPs = model.GetNuisanceParameters()
iter = NPs.createIterator()
while(True):
    np = iter.Next()
    if not np:
        break
    all_params[np.GetName()] = np

if varying_np not in all_params:
    raise Exception("Cannot find nuisance paramater '{}'".format(varying_np))
best_fit_value = all_params[varying_np].getVal()
best_fit_up = best_fit_value + all_params[varying_np].getErrorHi()
best_fit_down = best_fit_value + all_params[varying_np].getErrorLo()

constrainParams = RooArgSet()
constrainParams.add(NPs)

_InitialHesse = RooCmdArg(RooFit.InitialHesse(False))
_Hesse = RooCmdArg(RooFit.Hesse(False))
_Minimizer = RooCmdArg(RooFit.Minimizer("Minuit2", "Migrad"))
_Strategy = RooCmdArg(RooFit.Strategy(0))
_Strategy_alt = RooCmdArg(RooFit.Strategy(1))
_Verbose = RooCmdArg(RooFit.Verbose(0))
_PrintLevel = RooCmdArg(RooFit.PrintLevel(0+1))
_Constrain = RooCmdArg(RooFit.Constrain(constrainParams))

fitArgs = RooLinkedList()
fitArgs.Add(_InitialHesse)
fitArgs.Add(_Hesse)
fitArgs.Add(_Minimizer)
fitArgs.Add(_Verbose)
fitArgs.Add(_PrintLevel)
fitArgs.Add(_Constrain)
fitArgs.Add(_Strategy)

plots = {np: TGraph(len(scan_points)) for np in all_params}

for i,x in enumerate(scan_points):
    all_params[varying_np].setVal(x)
    all_params[varying_np].setConstant(True)
    model.GetPdf().fitTo(w.data("obsData"), fitArgs)
    for np in all_params:
        plots[np].SetPoint(i, x, all_params[np].getVal())

try:
    os.makedirs(out_dir)
except OSError as err:
    if err.errno == errno.EEXIST and os.path.isdir(out_dir):
        pass
    else:
        raise err

c1 = TCanvas("c1", "", 800, 800)
c1.SetMargin(0.18, 0.05, 0.10, 0.05)
solid_line = TLine()
dashed_line = TLine()
dashed_line.SetLineStyle(2)
for np,g in plots.iteritems():
    c1.Clear()
    g.SetTitle("")
    g.GetXaxis().SetTitle(varying_np)
    g.GetYaxis().SetTitle(np)
    min, max = g.GetYaxis().GetXmin(), g.GetYaxis().GetXmax()
    g.SetMarkerStyle(kFullCircle)
    g.SetLineWidth(2)
    g.Draw()
    solid_line.DrawLine(best_fit_value, min, best_fit_value, max)
    dashed_line.DrawLine(best_fit_up, min, best_fit_up, max)
    dashed_line.DrawLine(best_fit_down, min, best_fit_down, max)
    c1.Print(os.path.join(out_dir, np+".pdf"))

f.Close()
