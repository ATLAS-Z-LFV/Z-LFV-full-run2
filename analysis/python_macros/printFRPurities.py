#!/usr/bin/env python

import os, sys
from ROOT import *

if len(sys.argv) < 2:
    print "I need the FF version tag as an argument."
    print "e.g.:"
    print "    './printFRPurities.py v35'."
    sys.exit(1)

dir = "../fakes/{}/".format(sys.argv[-1])
channels = ["el", "mu"]
processes = ["W","Zll","T"]
#tau_modes = ["1p0n", "1pXn", "3p"]
tau_modes = ["1p", "3p"]

for c in ["el", "mu"]:
    k = {}
    for p in processes:

        f = TFile(os.path.join(dir, "FFactors_FR{}_{}.root".format(p, c)))
        m = tau_modes[0]
        data  = f.Get("FR{}_{}_{}_fail_data".format(p, c, m)).Integral()
        otherMC  = f.Get("FR{}_{}_{}_fail_otherMC".format(p, c, m)).Integral()
        real_targetMC  = f.Get("FR{}_{}_{}_fail_real_targetMC".format(p, c, m)).Integral()
        fake_targetMC  = f.Get("FR{}_{}_{}_fail_fake_targetMC".format(p, c, m)).Integral()
        for m in tau_modes[1:]:
            data += f.Get("FR{}_{}_{}_fail_data".format(p, c, m)).Integral()
            otherMC += f.Get("FR{}_{}_{}_fail_otherMC".format(p, c, m)).Integral()
            real_targetMC += f.Get("FR{}_{}_{}_fail_real_targetMC".format(p, c, m)).Integral()
            fake_targetMC += f.Get("FR{}_{}_{}_fail_fake_targetMC".format(p, c, m)).Integral()
        f.Close()

        f = TFile(os.path.join(dir, "RFactors_FR{}_{}.root".format(p, c)))
        m = tau_modes[0]
        real_allMC  = f.Get("FR{}_{}_{}_fail_real_allMCs".format(p, c, m)).Integral()
        for m in tau_modes[1:]:
            real_allMC += f.Get("FR{}_{}_{}_fail_real_allMCs".format(p, c, m)).Integral()
        f.Close()

        k[p] = (data - otherMC - real_targetMC) / fake_targetMC

        purity = (data - otherMC - real_targetMC) / (data - real_allMC)
        print "FR{}_{} purity: {:.2f}%".format(p, c, 100*purity)

    R = {}
    f = TFile(os.path.join(dir, "RFactors_FRQ_{}.root".format(c)))
    m = tau_modes[0]
    data  = f.Get("FRQ_{}_{}_fail_data".format(c, m)).Integral()
    real_allMC  = f.Get("FRQ_{}_{}_fail_real_allMCs".format(c, m)).Integral()
    R["W"]  = f.Get("FRQ_{}_{}_fail_fake_Wjets".format(c, m)).Integral()
    R["Zll"]  = f.Get("FRQ_{}_{}_fail_fake_Zll".format(c, m)).Integral()
    R["T"]  = f.Get("FRQ_{}_{}_fail_fake_top".format(c, m)).Integral()
    for m in tau_modes[1:]:
        data += f.Get("FRQ_{}_{}_fail_data".format(c, m)).Integral()
        real_allMC += f.Get("FRQ_{}_{}_fail_real_allMCs".format(c, m)).Integral()
        R["W"] += f.Get("FRQ_{}_{}_fail_fake_Wjets".format(c, m)).Integral()
        R["Zll"] += f.Get("FRQ_{}_{}_fail_fake_Zll".format(c, m)).Integral()
        R["T"] += f.Get("FRQ_{}_{}_fail_fake_top".format(c, m)).Integral()
    f.Close()

    for p in processes:
        R[p] *= k[p] / (data - real_allMC)

    purity = 1 - sum(R.values())
    print "FRQ_{} purity: {:.2f}%".format(c, 100*purity)

