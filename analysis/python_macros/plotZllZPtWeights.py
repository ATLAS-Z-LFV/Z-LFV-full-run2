#!/usr/bin/env python

import os, sys

from ROOT import *
gROOT.SetBatch()
gStyle.SetOptStat(0)

def divideByBinWidths(h):
    N = h.GetNbinsX()
    for i in xrange(1, N):
        val = h.GetBinContent(i)
        err = h.GetBinError(i)
        w = h.GetBinWidth(i)
        h.SetBinContent(i, val/w)
        h.SetBinError(i, err/w)
    val = h.GetBinContent(N)
    err = h.GetBinError(N)
    w = 2500. - h.GetBinLowEdge(N)
    h.SetBinContent(N, val/w)
    h.SetBinError(N, err/w)

c = TCanvas("c", "c", 800, 700)
c.SetLeftMargin(0.15)
c.SetBottomMargin(0.12)
c.SetRightMargin(0.05)
c.SetTopMargin(0.05)

l = TLine()
l.SetLineStyle(2)

pt = TPaveText(0.18, 0.82, 0.62, 0.95, "NDC")
pt.SetBorderSize(0)
pt.SetFillStyle(0)
pt.SetLineStyle(0)
atlas = pt.AddText("#font[72]{ATLAS} #font[42]{Internal}")
atlas.SetTextFont(72)
atlas.SetTextSize(0.05)
atlas.SetTextAlign(13)
lumi = pt.AddText(" ")
lumi.SetTextFont(42);
lumi.SetTextSize(0.04);
lumi.SetTextAlign(13);

#-------------------------------------------------

f = TFile("../ZWeights/fiducial_Z_pT_SFs.root")
h = f.Get("Zll_truth_Z_pT_SFs")

frame = h.Clone()
frame.SetLineWidth(0)
frame.SetTitle("")
frame.GetXaxis().SetTitle("truth #it{p}_{T}(#it{Z}) [GeV]")
frame.GetXaxis().SetTitleSize(0.050)
frame.GetXaxis().SetLabelSize(0.040)
frame.GetYaxis().SetTitle("#it{Z}#rightarrow#it{ll} samples reweighting factor")
frame.GetYaxis().SetTitleSize(0.050)
frame.GetYaxis().SetLabelSize(0.040)
height = frame.GetMaximum() - frame.GetMinimum()
frame.SetMinimum(frame.GetMinimum() - 0.1*height)
frame.SetMaximum(frame.GetMaximum() + 0.5*height)
frame.Draw()

l.DrawLine(0, 1, 125, 1)

h.SetLineColor(kBlue)
h.SetLineWidth(2)
h.Draw("E0 same")

pt.Draw()

c.SaveAs("Zll_truth_Z_pT_SFs.pdf")

for i in xrange(1, h.GetNbinsX()+1):
    val = h.GetBinContent(i)
    err = h.GetBinError(i)
    h.SetBinContent(i, 1./val)
    h.SetBinError(i, err/val/val)  # new val * rel. err = (1/val) * (err/val)
frame = h.Clone()
frame.SetLineWidth(0)
frame.SetTitle("")
frame.GetXaxis().SetTitle("truth #it{p}_{T}(#it{Z}) [GeV]")
frame.GetXaxis().SetTitleSize(0.050)
frame.GetXaxis().SetLabelSize(0.040)
frame.GetYaxis().SetTitle("#it{Z}#rightarrow#it{ll} samples MC / Data")
frame.GetYaxis().SetTitleSize(0.050)
frame.GetYaxis().SetLabelSize(0.040)
height = frame.GetMaximum() - frame.GetMinimum()
frame.SetMinimum(frame.GetMinimum() - 0.1*height)
frame.SetMaximum(frame.GetMaximum() + 0.5*height)
frame.Draw()

l.DrawLine(0, 1, 125, 1)

h.Draw("E0 same")

pt.Draw()

c.SaveAs("Zll_truth_Z_pT_MCtoData.pdf")

h1 = f.Get("measured_Z_pT_rebinned")
h2 = f.Get("Zll_truth_Z_pT")
divideByBinWidths(h1)
divideByBinWidths(h2)
frame = h1.Clone()
frame.SetLineWidth(0)
frame.SetTitle("")
frame.GetXaxis().SetTitle("#it{p}_{T}(#it{Z}) [GeV]")
frame.GetXaxis().SetTitleSize(0.050)
frame.GetXaxis().SetLabelSize(0.040)
frame.GetYaxis().SetTitle("Normalised number of events")
frame.GetYaxis().SetTitleSize(0.050)
frame.GetYaxis().SetLabelSize(0.040)
frame.SetMaximum(frame.GetMaximum() * 1.5)
frame.Draw()

h2.SetLineColor(TColor.GetColor(102, 153, 255))
h2.SetLineWidth(2)
h2.Draw("hist same")

h2_err = h2.Clone()
h2_err.SetMarkerColor(TColor.GetColor(102, 153, 255))
h2_err.SetLineColor(TColor.GetColor(102, 153, 255))
h2_err.SetFillStyle(3004)
h2_err.SetFillColor(kGray+1)
h2_err.Draw("E2 same")

h1.SetMarkerStyle(kFullCircle)
h1.SetMarkerColor(kBlack)
h1.SetLineColor(kBlack)
h1.Draw("E0 X0 same")

pt.Draw()

leg = TLegend(0.45, 0.74, 0.9, 0.84)
leg.SetMargin(0.12)
leg.SetLineWidth(0)
leg.SetTextSize(0.050)
leg.AddEntry(h1, "#font[42]{Fiducial measurement}", "P")
leg.AddEntry(h2, "#font[42]{#it{Z}#rightarrow#it{ll} MC truth}", "L")
leg.Draw()

c.SaveAs("Zll_truth_Z_pT_dists.pdf")

f.Close()
