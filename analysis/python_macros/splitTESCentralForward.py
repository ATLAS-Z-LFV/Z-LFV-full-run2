#!/usr/bin/env python

import os, sys
from ROOT import *
gROOT.SetBatch()
TTree.SetMaxTreeSize(long(4)*1024*1024*1024*1024)

files = sys.argv[1:]

def listOfNominalTrees(f):
    l = []
    keys = f.GetListOfKeys()
    for key in keys:
        if not key.GetClassName() == "TTree":
            continue
        if key.GetName().startswith("fakes") or key.GetName().startswith("data"):
            continue
        if key.GetName().endswith("NOMINAL"):
            l.append(key.GetName())
    return l

systs = ["TAUS_TRUEHADTAU_SME_TES_DETECTOR",
         "TAUS_TRUEHADTAU_SME_TES_INSITUFIT",
         "TAUS_TRUEHADTAU_SME_TES_INSITUEXP",
         "TAUS_TRUEHADTAU_SME_TES_MODEL"]

for file in files:
    print "Processing '{}'".format(file)
    f = TFile(file, "UPDATE")

    trees = listOfNominalTrees(f)
    for tree in trees:
        t_nom = f.Get(tree)

        has_taus = bool(t_nom.GetBranch("taus_eta"))
        if has_taus:
            t_nom_central = t_nom.CopyTree("fabs(taus_eta[0])<=1.37")
            t_nom_forward = t_nom.CopyTree("fabs(taus_eta[0])>1.37")
        else:
            t_nom_central = t_nom.CopyTree("")
            t_nom_forward = t_nom.CopyTree("")

        for syst in systs:
            for direction in ["up", "down"]:
                tree_syst = tree.replace("NOMINAL", syst) + "_1" + direction
                t_syst = f.Get(tree_syst)
                if not t_syst:
                    continue
                print "  -> " + tree_syst

                if has_taus:
                    t_syst_central = t_syst.CopyTree("fabs(taus_eta[0])<=1.37")
                    t_syst_forward = t_syst.CopyTree("fabs(taus_eta[0])>1.37")
                else:
                    t_syst_central = t_nom.CopyTree("", "", 0)
                    t_syst_forward = t_nom.CopyTree("", "", 0)

                # Central
                tree_list = TList()
                tree_list.Add(t_syst_central)
                tree_list.Add(t_nom_forward)
                new_t_syst = TTree.MergeTrees(tree_list)
                if new_t_syst:
                    new_t_syst.BuildIndex("runNumber", "eventNumber")
                else:
                    new_t_syst = t_nom.CopyTree("")  # Just take the nominal
                new_t_syst.SetName(tree_syst.replace(syst, syst+"_CENTRAL"))
                new_t_syst.SetTitle("{}_CENTRAL_1{}".format(syst, direction))
                new_t_syst.Write("", TObject.kOverwrite)

                # Forward
                tree_list = TList()
                tree_list.Add(t_syst_forward)
                tree_list.Add(t_nom_central)
                new_t_syst = TTree.MergeTrees(tree_list)
                if new_t_syst:
                    new_t_syst.BuildIndex("runNumber", "eventNumber")
                else:
                    new_t_syst = t_nom.CopyTree("")  # Just take the nominal
                new_t_syst.SetName(tree_syst.replace(syst, syst+"_FORWARD"))
                new_t_syst.SetTitle("{}_FORWARD_1{}".format(syst, direction))
                new_t_syst.Write("", TObject.kOverwrite)

    f.Close()
