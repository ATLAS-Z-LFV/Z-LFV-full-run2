#!/usr/bin/env python

import os, sys
import argparse
from copy import copy
from itertools import cycle
from multiprocessing import Process, Lock

from modules.Inputs import Inputs
from modules.Regions import Regions
from modules.Plots import Plots

from modules.general_utils import findDefaultConfigs, flattenCommaSeparatedLists
from modules.general_utils import expandPath, checkMakeDirs
from modules.general_utils import checkCpuCount, acquire
from modules.general_utils import rootVerbosityLevel
from modules.plot_utils import variablesFromCutString
from modules.fakes_utils import failRegion

from ROOT import gROOT, TFile, TTree


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser("./splitRegionTrees.py")

    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")
    parser.add_argument("--thinning-db", "-T", default="", type=str, help="Thinning config in YAML.")

    parser.add_argument("--output-dir", "-o", default="", type=str, help="Output directory for writing the split trees. Default = input directory appended with '_split'.")
    parser.add_argument("--input-file", "-i", dest="input_files", default=[], action='append', type=str, help="Input file(s). If this is not given, all files in inputs-db will be split (except alternative backgrounds).")
    parser.add_argument("--region", "-r", dest="regions", default=[], action='append', type=str, help="Regions that we want to split the trees into.")

    parser.add_argument("--no-syst", action='store_true', default=False, help="Do not split systematics.")

    parser.add_argument("--alias", "-a", dest="aliases", default=[], action='append', type=str, help="Branch alias. Assuming format 'name:=formula'. E.g. 'abseta:=fabs(eta)'.")

    parser.add_argument("--force-recreate", "-f", action='store_true', default=False, help="Force recreation of the output trees.")
    parser.add_argument("--force-recreate-files", "-F", action='store_true', default=False, help="Force recreation of the output FILES. (All existing trees in the files would be lost.)")
    parser.add_argument("--keep-old-cycles", action='store_true', default=False, help="Keep old cycles. This keeps tree backups, but might waste disk space.")

    parser.add_argument("--parallel", action='store_true', default=False, help="Use parallel multiprocessing.")
    parser.add_argument("--max-threads", default=16, type=int, help="Maximum number of threads to use for parallel multiprocessing.")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args)

        args.input_files = flattenCommaSeparatedLists(args.input_files)
        args.regions = flattenCommaSeparatedLists(args.regions)
        args.aliases = flattenCommaSeparatedLists(args.aliases)

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))
    if not os.path.exists(args.thinning_db):
        raise Exception("Cannot find thinning YAML file '{}'".format(args.thinning_db))

#-------------------------------------------------------------------------------
def splitRegionTrees(args, lock=None):

    gROOT.SetBatch(True)
    TTree.SetMaxTreeSize(long(2)*1024*1024*1024*1024)  # 2TB

    inputs = Inputs(args.inputs_db)
    regions = Regions(args.regions_db)
    thinnings = Plots(args.thinning_db)

    if not args.output_dir:
        dir = os.path.dirname(inputs.dir+'/')  # This strips away '/' at the end
        args.output_dir = dir + '_split'
    checkMakeDirs(args.output_dir)

    if args.input_files:
        input_files = args.input_files
        input_trees = {f:"" for f in input_files}
    else:
        input_files = []
        input_trees = {}
        file_iter = (f for i in inputs.values() for f in i.values())
        for f in file_iter:
            input_files.append(f.name)
            input_trees[f.name] = f.tree

    if args.parallel and len(input_files) > 1:
        n_threads = checkCpuCount(min(args.max_threads, len(input_files)))
        print "Will use multiprocessing with {} threads to process the files:".format(n_threads)
        print "  {}\n".format(input_files)
        lock = Lock()
        p = [None]*n_threads
        for f in input_files:
            for i,x in cycle(enumerate(p)):
                if x is None or not x.is_alive(): break
            _args = copy(args)
            _args.input_files = [f]
            p[i] = Process(target=splitRegionTrees, args=(_args, lock))
            p[i].start()
        return

    if not args.regions:
        # Sensible default regions
        args.regions = ["presel_mu",
                        "SR_mu",
                        "CRZtt_mu",
                        "SS_SR_mu",
                        "FRW_mu",
                        "FRT_mu",
                        "presel_el",
                        "SR_el",
                        "CRZtt_el",
                        "SS_SR_el",
                        "FRW_el",
                        "FRT_el",
                       ]

    for f in input_files:
        tree_prefix = input_trees[f]
        if not tree_prefix:
            basename = os.path.basename(f)
            if basename.startswith("data"):
                tree_prefix = "data"
            elif basename.startswith("HIGG4D1"):
                tree_prefix = basename.split(".")[0].split("_")[1]
            else:
                tree_prefix = basename.split(".")[0].split("_")[0]
            print "Guessing tree prefix = {}".format(tree_prefix)

        output_name = os.path.join(args.output_dir, os.path.basename(f))
        if expandPath(output_name) == expandPath(f) and args.force_recreate_files:
            #output_name = os.path.splitext(output_name)[0] + "_split.root"
            print "WARNING: the output file is the same as the input file!"
            print "         turning force-recreate-files off!"
            args.force_recreate_files = False

        with rootVerbosityLevel("Error"):
            in_file = TFile.Open(f)

        cycles = {}
        for t in in_file.GetListOfKeys():
            if not t.GetClassName() == "TTree":
                continue
            if not tree_prefix in t.GetName():
                continue
            tree_name = t.GetName()
            if tree_name in cycles:
                if t.GetCycle() > cycles[tree_name]:
                    # Normally, larger cycle numbers are encountered first in the loop
                    # So this shouldn't happen. But better be safe.
                    print "WARNING: Found tree '{}' with a larger cycle number! splitting again!".format(tree_name)
                else:
                    continue
            else:
                cycles[tree_name] = t.GetCycle()

            # Open the output file here so that we can use the disk as buffer
            if args.force_recreate_files:
                # Only recreate for the first time
                out_file = TFile.Open(output_name, "RECREATE")
                args.force_recreate_files = False
                existing_trees = []
            else:
                out_file = TFile.Open(output_name, "UPDATE")
                existing_trees = [key.GetName() for key in out_file.GetListOfKeys()]

            in_tree = t.ReadObj()

            for alias in args.aliases:
                name, formula = alias.split(":=")
                print in_tree.SetAlias(name, formula)

            for r in args.regions:
                region = regions[r]

                tree_suffix = tree_name.replace(tree_prefix, "", 1)
                if args.no_syst and not tree_suffix == "_NOMINAL":
                    continue
                out_tree_name = "{}_{}{}".format(tree_prefix, r, tree_suffix)
                if out_tree_name in existing_trees and not args.force_recreate:
                    with acquire(lock):
                        print "="*30
                        print "Tree '{}' already exists, skipping.\n".format(out_tree_name)
                    continue

                if tree_prefix == "fakes":
                    region = failRegion(region)
                cuts = region.temporary_selection
                if not (region.type == "FR" or tree_prefix in ["data", "Ztaumu", "Ztaue", "Ztaumugamma", "Ztauegamma", "fakes"]):
                    cuts &= region.truth_selection

                cut_variables = variablesFromCutString(cuts)

                weights = region.weights

                variables = list(thinnings[r])
                if region.type == "FR" or tree_prefix in ["data", "Ztaumu", "Ztaue", "Ztaumugamma", "Ztauegamma", "fakes"]:
                    variables += variablesFromCutString(region.truth_selection)
                for alias in args.aliases:
                    variables += variablesFromCutString(alias.split(":=")[1])

                with acquire(lock):
                    print "="*30
                    print "Splitting tree '{}' -> '{}'\n".format(tree_name, out_tree_name)
                    print "  cuts: {}\n".format(cuts)
                    print "  cut_variables: {}\n".format(sorted(cut_variables))
                    print "  weights: {}\n".format(weights)
                    print "  variables: {}\n".format(sorted(variables))

                # Set branch status
                in_tree.SetBranchStatus("*", 0)
                in_tree.SetBranchStatus("runNumber", 1)
                in_tree.SetBranchStatus("eventNumber", 1)
                in_tree.SetBranchStatus("lbNumber", 1)
                for v in set(cut_variables + weights + variables):
                    if not args.no_syst:
                        v = v.replace("_NOMINAL", "_*")
                    in_tree.SetBranchStatus(v, 1)

                # Copy the tree with the region cut, and write
                out_tree = in_tree.CopyTree(str(cuts))
                ###if not out_tree.GetEntries() == 0:
                ###    out_tree.BuildIndex("runNumber", "eventNumber")
                if not out_tree:
                    print "=================="
                    print "in_tree = {}".format(in_tree)
                    print "out_tree = {}".format(out_tree)
                    print "cut = {}".format(str(cuts))
                    raise Exception("in_tree.CopyTree() returned NULL!! Check the cut!")
                out_tree.SetName(out_tree_name)
                out_file.WriteTObject(out_tree, out_tree_name, "Overwrite")
                with acquire(lock):
                    if lock: print "="*30
                    print "{} written!\n".format(out_tree_name)

            pass  # regions

            out_file.Close()
            in_tree.Delete()
            del in_tree

        pass  # trees

        # At last, copy the weights tree
        if not tree_prefix == "data":
            out_file = TFile.Open(output_name, "UPDATE")
            in_tree = in_file.Get("weights")
            indices = in_tree.GetTreeIndex()
            out_tree = in_tree.CopyTree("")
            out_tree.BuildIndex(indices.GetMajorName(), indices.GetMinorName())
            out_file.WriteTObject(out_tree, "weights", "Overwrite")
            out_file.Close()

        in_file.Close()

    pass  # files

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    splitRegionTrees(args)
