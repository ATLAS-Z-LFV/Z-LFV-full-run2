#!/usr/bin/env python

import os, sys
import argparse
from copy import copy
from itertools import cycle
from multiprocessing import Process, Lock
from time import sleep

from modules.Inputs import Inputs
from modules.Regions import Regions
from modules.general_utils import findDefaultConfigs, checkMakeDirs, flattenCommaSeparatedLists
from modules.general_utils import checkCpuCount, acquire
from modules.general_utils import saveRootObjects
from modules.fakes_utils import *


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser("./computeRFactors.py")
    
    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")
    
    parser.add_argument("--output-dir", "-o", default="fakes", type=str, help="Output directory for writing saving the results. Default = './fakes'")
    parser.add_argument("--tag", "-t", default="foo", type=str, help="Output tag. Default = 'foo'")
    parser.add_argument("--force-recreate", "-f", action='store_true', default=False, help="Force recreation of the output files.")
    
    parser.add_argument("--region", "-r", dest="regions", action='append', default=[], type=str, help="The target region(s).")
    parser.add_argument("--fakes-bkg", "-F", dest="fakes_bkgs", action='append', default=[], type=str, help="The major fakes backgrounds. Do not include 'QCD', or anything that is not defined in inputs-db. Default = ['Wjets','Zll','top'].")
    parser.add_argument("--alt-bkgs", action="store_true", default=False, help="Use alternative background samples.")
    parser.add_argument("--temporary-cuts", default=False, action="store_true", help="Ignore cuts that have been marked 'ignore_temporarily' in the YAML file.")
    parser.add_argument("--BDT-ID", default=False, action="store_true", help="Using BDT tau jet-ID instead of the RNN one.")
    
    parser.add_argument("--split-mode", type=str, choices=["1p3p", "1p0nXn3p"], default="1p3p", help="How to split tau_had decay mode ('1p3p' or '1p0nXn3p').")
    
    parser.add_argument("--NN-tag", type=str, default=None, help="NN tag. Needed if NN outputs are used to define the region(s).")
    
    parser.add_argument("--parallel", action='store_true', default=False, help="Use parallel multiprocessing.")
    parser.add_argument("--max-threads", default=16, type=int, help="Maximum number of threads to use for parallel multiprocessing.")
    
    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args)
        
        regions = flattenCommaSeparatedLists(args.regions)
        args.regions = list(set(regions))
        
        if args.fakes_bkgs == []:
            args.fakes_bkgs = ['Wjets','Zll','top']
        else:
            fakes_bkgs = flattenCommaSeparatedLists(args.fakes_bkgs)
            args.fakes_bkgs = list(set(fakes_bkgs))
        
        checkArgs(args)
        
    except:
        parser.print_help()
        raise
    
    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))
    
    checkMakeDirs(os.path.join(args.output_dir, args.tag))
    
    if len(args.regions) == 0:
        raise Exception("You did not specify any target regions!")

#-------------------------------------------------------------------------------
def computeRFactors(args, lock=None):
    inputs = Inputs(args.inputs_db)
    regions = Regions(args.regions_db)
    
    if args.parallel and len(args.regions) > 1:
        n_threads = checkCpuCount(min(args.max_threads, len(args.regions)))
        print "Will use multiprocessing with {} threads to process the target regions:".format(n_threads)
        print "  {}\n".format(args.regions)
        lock = Lock()
        p = [None]*n_threads
        for r in args.regions:
            for i,x in cycle(enumerate(p)):
                if x is None or not x.is_alive(): break
            _args = copy(args)
            _args.regions = [r]
            p[i] = Process(target=computeRFactors, args=(_args, lock))
            p[i].start()
        while any(x is not None and x.is_alive() for x in p):
            # some threads are still alive, wait for 1 second and check again
            sleep(1)
        return
    
    inputs.useAlt(args.alt_bkgs)
    
    all_bkgs = inputs.bkgs.values()
    fakes_bkgs = []
    for b in args.fakes_bkgs:
        fakes_bkgs.append(inputs[b])
    data = inputs.data
    
    for r in args.regions:
        output_file_name = os.path.join(args.output_dir, args.tag, "RFactors_{}.root".format(r))
        if os.path.exists(output_file_name) and not args.force_recreate:
            print "File '{}' already exists. Skipping. (Use '--force-recreate' to force recreation of the file.)".format(output_file_name)
            continue
        
        region = failRegion(regions[r], args.BDT_ID)
        if args.NN_tag is None and "NN_Zll" in region.cuts:
            del region.cuts["NN_Zll"]
        if args.temporary_cuts:
            cuts_fail = region.temporary_selection
        else:
            cuts_fail = region.selection
        weights = region.weight_string(lumi=inputs.lumi*1000.)
        
        with acquire(lock):
            print "="*30
            print "Processing '{}'\n".format(r)
            print "  Fakes backgrounds: {}\n".format(args.fakes_bkgs)
            print "  selection (fail): {}\n".format(cuts_fail)
            print "  weights: {}\n".format(weights)
        
        if args.split_mode == "1p3p":
            tau_modes = ["1p", "3p"]
            extra_cuts = {"1p": "taus_n_tracks[0]==1",
                          "3p": "taus_n_tracks[0]==3"}
            branches = {"1p": "tau_0_lead_track_pt:taus_pt[0]",  # 'y:x' not 'x:y'
                        "3p": "taus_pt[0]"}
            binnings = {"1p": [[25, 30, 40, 55, 80],  # tau pt
                               [0, 10, 20, 25, 30, 80]],  # tau track pt
                        "3p": [[25, 30, 40, 55, 80]]}
        elif args.split_mode == "1p0nXn3p":
            tau_modes = ["1p0n", "1pXn", "3p"]
            extra_cuts = {"1p0n": "taus_n_tracks[0]==1&&taus_decay_mode[0]==0",
                          "1pXn": "taus_n_tracks[0]==1&&taus_decay_mode[0]!=0",
                          "3p": "taus_n_tracks[0]==3"}
            branches = {"1p0n": "tau_0_lead_track_pt:taus_pt[0]",  # 'y:x' not 'x:y'
                        "1pXn": "tau_0_lead_track_pt:taus_pt[0]",  # 'y:x' not 'x:y'
                        "3p": "taus_pt[0]"}
            binnings = {"1p0n": [[20, 30, 40, 55, 100],  # tau pt
                                 [0, 10, 20, 25, 30, 100]],  # tau track pt
                        "1pXn": [[20, 30, 40, 55, 100],  # tau pt
                                 [0, 10, 20, 25, 30, 100]],  # tau track pt
                        "3p": [[20, 30, 40, 55, 100]]}
        
        for tau_mode in tau_modes:
            hists = getRFactorComponents(region_name=r+"_"+tau_mode,
                                         extra_cut=extra_cuts[tau_mode],
                                         branch=branches[tau_mode],
                                         binnings=binnings[tau_mode],
                                         cuts_fail=cuts_fail,
                                         truth_sel=region.truth_selection,
                                         weights=weights,
                                         data=data,
                                         fakes_bkgs=fakes_bkgs,
                                         all_bkgs=all_bkgs,
                                         NN_tag=args.NN_tag,
                                         lock=lock,
                                        )
            
            for b in args.fakes_bkgs:
                getRFactors(hists, b, "R_{}_{}_{}".format(b, r, tau_mode))
            #keepRFactorsInRange(hists)  # Do this after multiplying k
            
            saveRootObjects(hists.values(), output_file_name, "UPDATE")
            
            with acquire(lock):
                if lock: print "="*30
                print "Completed processing '{}, {}'\n".format(r, tau_mode)
                if tau_mode == tau_modes[-1]:
                    print "'{}' Done!\n".format(r)
    
    print "="*30

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    computeRFactors(args)
