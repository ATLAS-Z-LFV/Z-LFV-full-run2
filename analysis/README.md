# Analysis

This directory contains all the codes needed to analyse the ntuples. This includes making cutflow tables, distribution plots, and fitting.

<BR>

## Setup

To set up for the first time, simply do:
```
source setup.sh
```

The setup should be done fully automatically if you are on Nikhef Stoomboot or using a machine with cvmfs (e.g. lxplus).

After the first-time setup, if the codes do not need updates, simply do:
```
source setup_fast.sh
```
each time you log in to the machine.

Blame Terry (ws.chan@cern.ch) if the setup scripts don't work.

<BR>

## Making cutflow tables

The script ``makeCutflowTable.py`` creates cutflow tables in tex format. It uses ``configs/inputs.yaml`` and ``configs/regions.yaml`` as input databases by default.

An example use of the script could be:
```
./makeCutflowTable.py -o results/cutflow -r loose_SR_mu -r SR_mu
```
, which will create a tex file ``cutflow__loose_SR_mu_SR_mu.tex`` together with a json cache file ``cutflow_cache__loose_SR_mu_SR_mu.json`` in the output directory ``results/cutflow/``.

Re-executing the command will recreate the tex file by reading the cache, and should cost nearly no time (useful for making minor/cosmetic changes). Use the ``--force-recreate`` command to force recreation of the cache.

The ``--unweighted`` option will tell the script to count in raw numbers of events, without applying any normalisation or scale factors.

<BR>
**Dev. note:** The script uses ``ROOT.TTree.GetEntries()`` and/or ``root_numpy.tree2array()`` to count events *for every cut*. It is therefore not very efficient in terms of execution time. But it has the advantage of being flexible, error-prone, and fully automatic. A possible improvement here is to use ``root_numpy.tree2array()`` to read multiple cut results at once to avoid having to read the same input tree multiple times. Although memory constrain might become an issue for very large trees (``root_numpy.tree2array()`` doesn't seem to utilise memory very well when reading a lot of complicated branches).

<BR>

## Splitting ntuples

Looping over large trees with many excessive branches (columns) and events (rows) is super inefficient. It practically takes forever to plot/fit with systematics if we always run on the full ntuples. The solution here, is to split the full trees into much smaller trees that contain only events that are in a given region (a.k.a. skimming). At the same time, we throw away unwanted branches (a.k.a. thinning). The script ``splitRegionTrees.py`` exists exactly for this purpose.

Example use of ``splitRegionTrees.py``:
```
./splitRegionTrees.py -i /path_to_ntuples/Ztaumu.root -r presel_mu,SR_mu
```
The above command will create/update the file ``/path_to_ntuples_split/Ztaumu.root``, in which you can find trees with names like ``Ztaumu_presel_mu_NOMINAL``, ``Ztaumu_SR_mu_SYST_UP``, etc.

You may also choose to split all files specified in the inputs config YAML file at once. Use the option ``--parallel`` to speed it up if your machine can affort it:
```
./splitRegionTrees.py -I configs/inputs.yaml -r presel_mu,SR_mu --parallel --max-threads 4
```

**Note:** If you encounter some ``TFile::ReadBuffer`` errors, check if you were trying to write to an immutable file system (yep, Nikhef's dCache...). If that's indeed the case, try changing the output directory to a disk with a mutable file system (on Stoomboot, ``/data/atlas`` or ``/project/atlas``). Manually move the files to your original destination afterwards.

<BR>

## Plotting 1D distributions

Making 1D plots is extremely simple. Just follow the steps:

1. Define the histograms that you want to plot in ``histograms.yaml``, if they weren't defined already.

1. Tell the plotter what to plot in ``plots.yaml``. (Alternatively, use the option ``--variable`` (or ``-v``) when you call plotter.)

1. Call the plotter ``plot1D.py``. A simplest call would be like:
<BR>```./plot1D.py --region presel_mu,SR_mu --tag myFirstPlots```<BR>
This creates a cache file ``data/myFirstPlots.root`` and some nice plots in ``results/plots_myFirstPlots/``. Explore more options by looking into the script.

**Note:** The capacity of the plotter hasn't been seriously tested yet. In case you are trying to make a lot of plots at once and experience bad performances, perhaps try to split the job first (about 10 plots per run with systematics).

<BR>

## Fake factor method

Here is the workflow for using fake factor method in the analysis:

1. Use ``createFakes.py`` to create the 'fakes ntuple', which is a combination of 'fail' (i.e. with tau-ID loose but not tight) data events and 'fail' truth-matched MC events.

1. Use ``computeFFactors.py`` to calculate the *F*- and *k*-factors for each fakes-enriched region (FR).

1. Use ``computeRFactors.py`` to calculate the *R*-factors for each target region that you want to analyse.

1. Use ``decorateFakes.py`` to decorate the final fake factors and their systematic variations to the 'fakes ntuple'.

The first 3 steps can actually be done in parallel, while the last step needs input from all the previous steps.

### createFakes.py
If your inputs config YAML file is well set, then you can probably rely on the default settings and just do:
```
./createFakes.py
``` 
This creates a ``fakes.root`` ntuple file in the directory where all your other ntuple files are in. Simple!

### computeFFactors.py
This script calculates the *F*- and *k*-factors from FRs and store the results as histograms in a ROOT file. To use the default FRs defined in ``configs/regions.yaml``, execute with ``-r ALL`` like:
```
./computeFFactors.py -r ALL --tag myAwesomeFFs --parallel
``` 
This creates the files ``fakes/myAwesomeFFs/FFactors_*.root``. Adjust multi-core CPU usage with the options ``--parallel`` and ``--max-threads``.

### computeRFactors.py
This script calculates the *R*-factors from the regions you want to analyse, and store the results as histograms in a ROOT file. To use the default FRs defined in ``configs/regions.yaml``, execute with ``-r ALL`` like:
```
./computeRFactors.py -r presel_mu,presel_el,SR_mu,SR_el --tag myAwesomeFFs --parallel
``` 
This creates the files ``fakes/myAwesomeFFs/FFactors_*.root``. Adjust multi-core CPU usage with the options ``--parallel`` and ``--max-threads``.

### decorateFakes.py
The final step is to decorate the fakes ntuples with the calculated fake factors. With default options, call:
```
./decorateFakes.py --tag myAwesomeFFs -r presel_mu,presel_el,SR_mu,SR_el --parallel
``` 
This creates friend trees and store them in an auxiliary ROOT file ``fakes.root.FF.friend``. Now we are done - fire the plotter and look at the awesome data-model agreement we achieved with the method!

<BR>

## Fitting

...
