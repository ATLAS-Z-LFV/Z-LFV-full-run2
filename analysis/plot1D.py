#!/usr/bin/env python

import os, sys, inspect
import argparse

from modules.general_utils import findDefaultConfigs, flattenCommaSeparatedLists

if __name__ == '__main__':
    import inspect
    from modules.general_utils import execute
    from modules.plot_utils import cleanUpPlotDirectory

else:
    from modules.Inputs import Inputs
    from modules.Regions import Regions, Cut
    from modules.Histograms import Histograms
    from modules.Systematics import Systematics
    from modules.Plots import Plots
    from modules.fakes_utils import failRegion
    from modules.HistFitter_utils import *


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser(prog='./plot1D.py')

    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")
    parser.add_argument("--histograms-db", "-H", default="", type=str, help="Histograms config in YAML.")
    parser.add_argument("--systematics-db", "-S", default="", type=str, help="Systematics config in YAML.")
    parser.add_argument("--plots-db", "-P", default="", type=str, help="Plots config in YAML.")

    parser.add_argument("--tag", "-t", default="foo", type=str, help="The input/output tag. HistFitter will read cached histograms from './data/<tag>.root' if already exists, and write results to the directory './results/plots_<tag>'.")
    parser.add_argument("--force-recreate", "-f", action="store_true", default=False, help="Force recreation of the histogram caches.")
    parser.add_argument("--use-existing-WS", action="store_true", default=False, help="Use existing workspace.")
    parser.add_argument("--backup-cache", default=None, type=str, help="Backup cache to fall back to when histograms are not found in the regular cache.")

    parser.add_argument("--region", "-r", dest="regions", default=[], type=str, action='append', help="Region to plot. Can have multiple regions.")
    parser.add_argument("--variable", "-v", dest="variables", default=[], type=str, action='append', help="Variable to plot. Can have multiple variables. Overrides --plots-db.")

    parser.add_argument("--force-unblind", action="store_true", default=False, help="Unblind all the regions, overriding the 'blind' property set by --regions-db. Use with caution!")
    parser.add_argument("--force-blind", action="store_true", default=False, help="Blind all the regions, overriding the 'blind' property set by --regions-db.")
    parser.add_argument("--plot-blind-sensitive", action="store_true", default=False, help="Blind sensitive regions in output plots (argument passed to replotFitOutputs.py).")
    parser.add_argument("--ignore-stat", action="store_true", default=False, help="Ignore statistical errors.")
    parser.add_argument("--syst", action="store_true", default=False, help="Show systematic errors. Otherwise only statistical errors will be shown.")
    parser.add_argument("--temporary-cuts", action="store_true", default=False, help="Ignore cuts that have been marked 'ignore_temporarily' in the YAML file.")
    parser.add_argument("--alt-bkgs", action="store_true", default=False, help="Use alternative background samples.")
    parser.add_argument("--use-MC-fakes", action="store_true", default=False, help="Use fakes estimated by MC, instead of the Fake Factor method.")
    parser.add_argument("--use-full-trees", action="store_true", default=False, help="Use full trees instead of region trees.")

    parser.add_argument("--NN-tag", default="", type=str, help="NN tag.")

    parser.add_argument("--no-split-prongs", dest="split_prongs", action="store_false", default=True, help="Do not plot events with 1-prong and 3-prong taus separately.")
    parser.add_argument("--split-mode", type=str, choices=["1p3p", "1p0nXn3p"], default="1p3p", help="How to split tau_had decay mode ('1p3p' or '1p0nXn3p').")
    parser.add_argument("--plot-fail-region", action="store_true", default=False, help="Plot 'fail region', i.e. region with loose-but-not-tight taus, instead.")
    parser.add_argument("--BDT-ID", action="store_true", default=False, help="Use BDT tau jet-ID instead of the RNN one.")

    parser.add_argument("--signal-BR", default=1e-3, type=float, help="The signal BR. Default is 1e-3 (100 * LEP tau-mu limit).")

    parser.add_argument("--v22", action="store_true", help="Backward compatibility with v22 n-tuples")
    parser.add_argument("--no-ZWeight", action="store_true", help="Do not apply Z pT reweighting.")
    parser.add_argument("--apply-ZTheory", action="store_true", help="Apply Ztautau theory weights.")

    parser.add_argument("--verbose", "-V", action="store_true", default=False, help="Run HistFitter in verbose mode.")
    parser.add_argument("--log-level", "-L", default="", type=str, help="Set log level of HistFitter, e.g. 'VERBOSE', 'DEBUG'")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args, default_split_inputs=(not args.use_full_trees))

        args.regions = flattenCommaSeparatedLists(args.regions)
        args.variables = flattenCommaSeparatedLists(args.variables)

        if args.verbose and not args.log_level:
            args.log_level = "VERBOSE"

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))
    if not os.path.exists(args.histograms_db):
        raise Exception("Cannot find histograms YAML file '{}'".format(args.histograms_db))
    if not os.path.exists(args.systematics_db):
        raise Exception("Cannot find systematics YAML file '{}'".format(args.systematics_db))
    if not args.variables and not os.path.exists(args.plots_db):
        raise Exception("Cannot find plots YAML file '{}'".format(args.plots_db))

    if not args.regions:
        raise Exception("No regions specified.")

    if args.force_blind and args.force_unblind:
        raise Exception("You told me to blind and unblind all regions at the same time! I am confused!")

#-------------------------------------------------------------------------------
# I am being directly called (by shell),
# I will first call HistFitter.py to use myself as a config file (see the non-__main__ part)
# Then I call replotFitOutputs.py to make pretty plots
if __name__ == '__main__':

    args = parseArgs(sys.argv[1:])
    this_file = inspect.getfile(inspect.currentframe())

    HistFitter = "HistFitter.py {} -f -w -D before".format(this_file)
    HistFitter += " -u='{}'".format(" ".join(sys.argv[1:]))
    if args.log_level:
        HistFitter += " -L {}".format(args.log_level)
    returncode = execute(HistFitter, log="results/plots_{0}/plots_{0}.log".format(args.tag))
    if not returncode == 0:
        print "Failed running HistFitter: '{}'".format(HistFitter)
        print "Return code non-zero: {}".format(returncode)
        sys.exit(returncode)

    # HistFitter's plots aren't pretty enough
    # Use replotFitOutputs.py to make them look better
    replotFitOutputs = "./" + this_file.replace("plot1D.py", "replotFitOutputs.py")
    replotFitOutputs += " -I " + args.inputs_db
    replotFitOutputs += " -R " + args.regions_db
    replotFitOutputs += " -H " + args.histograms_db
    replotFitOutputs += " --signal-BR {}".format(args.signal_BR)
    if args.force_blind:
        replotFitOutputs += " --force-blind"
    if args.force_unblind:
        replotFitOutputs += " --force-unblind"
    if args.plot_blind_sensitive:
        replotFitOutputs += " --blind-sensitive"
    if args.plot_fail_region:
        replotFitOutputs += " --is-fail-region"
    replotFitOutputs += " results/plots_{}".format(args.tag)
    # replotFitOutputs.py always exists with seg fault
    # But that's pretty harmless... so let's ignore it for now
    try:
        execute(replotFitOutputs)
    except Exception as exception:
        pass

    cleanUpPlotDirectory("results/plots_{}".format(args.tag))

    print "All done!!"

#-------------------------------------------------------------------------------
# I am being called by HistFitter.py, setup a dummy fit for plotting
else:
    args = parseArgs(configMgr.userArg.split())

    print "===================================================="
    print "Running HistPlotter with the following user configs:"
    print "===================================================="
    print "        inputs-db : %s" % args.inputs_db
    print "       regions-db : %s" % args.regions_db
    print "    histograms-db : %s" % args.histograms_db
    print "   systematics-db : %s" % args.systematics_db
    print "         plots-db : %s" % args.plots_db
    print "              tag : %s" % args.tag
    print "   force-recreate : %s" % args.force_recreate
    print "  use-existing-WS : %s" % args.use_existing_WS
    print "     backup-cache : %s" % args.backup_cache
    print "          regions : %s" % args.regions
    print "        variables : %s" % args.variables
    print "    force-unblind : %s" % args.force_unblind
    print "      force-blind : %s" % args.force_blind
    print "      ignore-stat : %s" % args.ignore_stat
    print "             syst : %s" % args.syst
    print "   temporary-cuts : %s" % args.temporary_cuts
    print "         alt-bkgs : %s" % args.alt_bkgs
    print "     use-MC-fakes : %s" % args.use_MC_fakes
    print "   use-full-trees : %s" % args.use_full_trees
    print "           NN-tag : %s" % args.NN_tag
    print "     split-prongs : %s" % args.split_prongs
    print "       split-mode : %s" % args.split_mode
    print " plot-fail-region : %s" % args.plot_fail_region
    print "           BDT-ID : %s" % args.BDT_ID
    print "        signal-BR : %s" % args.signal_BR
    print "       no-ZWeight : %s" % args.no_ZWeight
    print "    apply-ZTheory : %s" % args.apply_ZTheory
    print "===================================================="

    inputs = Inputs(args.inputs_db)
    regions = Regions(args.regions_db)
    histograms = Histograms(args.histograms_db)
    systematics = Systematics(args.systematics_db)

    if args.variables:
        variables = {r:args.variables for r in args.regions}
    else:
        plots = Plots(args.plots_db)
        variables = {r:plots[r] for r in plots if r in args.regions}

    if args.alt_bkgs:
        inputs.useAlt()

    configMgr.myFitType = FitType.Exclusion

    configMgr.analysisName = "plots_{}".format(args.tag)
    configMgr.outputFileName = "results/{0}/{0}_foo.root ".format(configMgr.analysisName)
    configMgr.histCacheFile = "data/{}.root".format(args.tag)
    configMgr.useHistBackupCacheFile = True
    configMgr.histBackupCacheFile = args.backup_cache if args.backup_cache else configMgr.histCacheFile
    configMgr.useCacheToTreeFallback = True
    configMgr.readFromTree = args.force_recreate
    configMgr.executeHistFactory = not args.use_existing_WS

    # All has to be 1 otherwise the fakes weights hack won't work!
    configMgr.outputLumi = 1.
    configMgr.inputLumi = 1.
    configMgr.lumiUnits = 1.
    configMgr.nomName = "_NOMINAL"

    configMgr.calculatorType = 2

    apply_ZWeight = not args.no_ZWeight
    apply_ZTheory = args.apply_ZTheory

    #-------------------------------------------------
    # Now set the inputs
    backgrounds = getBackgroundSampleList(inputs, args.NN_tag, apply_ZWeight)
    signals = getSignalSampleDict(inputs, args.NN_tag, apply_ZWeight, add_dummy_POI=True)
    data = getDataSample(inputs, args.NN_tag)

    if not args.use_MC_fakes:
        fakes = getFakesSample(inputs, args.NN_tag)
        backgrounds.append(fakes)

    #-------------------------------------------------
    # Fit config instances (one config for each signal)
    fitConfigs = {}
    for s in inputs.signals:
        new_fitConfig = configMgr.addFitConfig("DummyFit_{}".format(s))
        new_fitConfig.statErrThreshold = 100. if args.ignore_stat else 0.
        new_fitConfig.addSamples(backgrounds + [data])
        new_fitConfig.setSignalSample(signals[s])
        new_fitConfig.addMeasurement(name="NormalMeasurement", lumi=1.0, lumiErr=1e-5).addPOI("dummyPOI")
        fitConfigs[s] = new_fitConfig

    #-------------------------------------------------
    # Add channels
    configMgr.cutsDict = {}
    #tau_prongs = {"":"1"}  # do we need the inclusive channel anyway?
    tau_prongs = {}
    if args.split_prongs:
        if args.split_mode == "1p3p":
            tau_prongs.update({"_1P":"taus_n_tracks[0]==1", "_3P":"taus_n_tracks[0]==3"})
        elif args.split_mode == "1p0nXn3p":
            tau_prongs.update({"_1p0n":"taus_n_tracks[0]==1&&taus_decay_mode[0]==0",
                               "_1pXn":"taus_n_tracks[0]==1&&taus_decay_mode[0]!=0",
                               "_3p":"taus_n_tracks[0]==3"})
    else:
        if args.syst:
            print "WARNING: systematics does not work correctly in tau-prong-inclusive channel."
            # TODO: Fix this
        tau_prongs.update({"":"1"} )

    # NESTED NESTED NESTED loops over regions, prongs, variables, signals
    for r in args.regions:
        region = regions[r]
        blind = args.force_blind or (region.blind and not args.force_unblind)

        if args.plot_fail_region:
            region = failRegion(region, args.BDT_ID)

        for p in tau_prongs:
            region_name = r + p

            if args.use_full_trees:
                cut = region.temporary_selection if args.temporary_cuts else region.selection
            else:
                cut = Cut("1") if args.temporary_cuts else region.ignored_selection
            cut &= tau_prongs[p]
            # Truth selections will later be added per channel per background sample in loops
            configMgr.cutsDict[region_name] = str(cut)

            if not r in variables:
                raise Exception("Couldn't find region '{}' in plots-db '{}'".format(r, args.plots_db))

            for v in variables[r]:
                var = histograms[v]

                if var.is_2D: continue
                if var.is_NN_output and not args.NN_tag:
                    print "WARNING: '{}' is a NN output, but you did not tell me the NN version (use the '--NN-tag' argument). Skipping it.".format(var.name)
                    continue

                for s in region.signals:
                    fit = fitConfigs[s]

                    channel = addChannel(fit, var, region, inputs.lumi*1000, blind=blind, signal=signals[s], region_name=region_name, use_MC_fakes=args.use_MC_fakes, use_full_trees=args.use_full_trees, apply_ZWeight=apply_ZWeight, apply_ZTheory=apply_ZTheory, v22=args.v22)

                    fit.addValidationChannels(channel)

    if args.syst:
        addSystematics(configMgr.fitConfigs, systematics, args.verbose)

    # Remove fitConfig with no channels
    empty_fitConfig = [f for f in configMgr.fitConfigs if len(f.channels)==0]
    for fit in empty_fitConfig:
        configMgr.fitConfigs.remove(fit)
