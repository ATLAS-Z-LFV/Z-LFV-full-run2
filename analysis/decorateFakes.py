#!/usr/bin/env python

import os, sys
import argparse
from copy import copy
from array import array
from time import sleep
from tempfile import mkstemp
from multiprocessing import Process, Lock

from modules.Inputs import Inputs, InputFile
from modules.Regions import Regions
from modules.general_utils import findDefaultConfigs, checkMakeDirs, flattenCommaSeparatedLists
from modules.general_utils import checkCpuCount, acquire
from modules.general_utils import root_open, root_tree
from modules.general_utils import DefaultOrderedDict
from modules.plot_utils import variablesFromCutString, removeHistErrors, inheritOverflows
from modules.fakes_utils import *

from ROOT import TFile, TTree


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser("./decorateFakes.py")

    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")

    parser.add_argument("--input-file", default="", type=str, help="An option to override the fakes input file name specified in inputs-db.")
    parser.add_argument("--input-tree", default="", type=str, help="An option to override the fakes input tree name specified in inputs-db.")
    parser.add_argument("--input-friend-file", dest="input_friend_files", action='append', default=[], type=str, help="An option to override the fakes input friend file name(s) in specified inputs-db.")
    parser.add_argument("--input-friend-tree", dest="input_friend_trees", action='append', default=[], type=str, help="An option to override the fakes input friend tree name(s) in specified inputs-db.")

    parser.add_argument("--output-dir", "-o", default="", type=str, help="Output directory. Default = input directory.")
    parser.add_argument("--region", "-r", dest="regions", action='append', default=[], type=str, help="The target region(s). Will create one friend tree per region.")
    parser.add_argument("--decorate-all", default=False, action="store_true", help="Decorate all trees with all specified regions found in the input file. (This would be stupid when decorating split ntuples.)")

    parser.add_argument("--fakes-dir", "-d", default="fakes", type=str, help="Input directory in which the F-/k-/R-factors files are sitting. Default = './fakes'")
    parser.add_argument("--tag", "-t", default="foo", type=str, help="Input tag. Default = 'foo'")
    parser.add_argument("--fakes-region", "-F", dest="FRs", action='append', default=[], type=str, help="The Fakes-enriched region(s). Overrides the regions specified in regions-db. DO NOT mix between channels if you use this option!")
    parser.add_argument("--combine-emu-FRs", default=False, action='store_true', help="Combine FRs for electron and muon channels.")

    parser.add_argument("--split-mode", type=str, choices=["1p3p", "1p0nXn3p"], default="1p3p", help="How to split tau_had decay mode ('1p3p' or '1p0nXn3p').")

    #parser.add_argument("--temporary-cuts", default=False, action="store_true", help="Ignore cuts that have been marked 'ignore_temporarily' in the YAML file.")

    parser.add_argument("--no-index", action='store_true', default=False, help="Do not build tree indices. Useful if the (runNumber,eventNumber) indices turn out to be not unique for any reason.")

    parser.add_argument("--force-recreate", "-f", action='store_true', default=False, help="Force recreation of the output file.")
    parser.add_argument("--keep-old-cycles", action='store_true', default=False, help="Keep old cycles. This keeps tree backups, but might waste disk space.")

    parser.add_argument("--parallel", action='store_true', default=False, help="Use parallel multiprocessing.")
    parser.add_argument("--max-threads", default=16, type=int, help="Maximum number of threads to use for parallel multiprocessing.")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args, default_split_inputs=True)

        regions = flattenCommaSeparatedLists(args.regions)
        args.regions = list(set(regions))

        FRs = flattenCommaSeparatedLists(args.FRs)
        FRs = set(FRs)
        if 'ALL' in FRs:
            raise Exception("Please DO NOT mix regions for different channels!")
        if 'MU' in FRs:
            FRs.remove('MU')
            FRs = FRs.union(['FRW_mu','FRZll_mu','FRT_mu','FRQ_mu'])
        if 'EL' in FRs:
            FRs.remove('EL')
            FRs = FRs.union(['FRW_el','FRZll_el','FRT_el','FRQ_el'])
        args.FRs = list(FRs)

        args.input_friend_files = flattenCommaSeparatedLists(args.input_friend_files)
        args.input_friend_trees = flattenCommaSeparatedLists(args.input_friend_trees)

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))

    if (args.input_file=="") ^ (args.input_tree==""):
        raise Exception("Please use the options --input-file and --input-tree together.")

    if not len(args.input_friend_files) == len(args.input_friend_trees):
        raise Exception("--input-friend-file and --input-friend-tree should have a one-to-one correspondence.")

    if args.output_dir:
        checkMakeDirs(args.output_dir)

    if len(args.regions) == 0:
        raise Exception("You did not specify any target regions!")

#-------------------------------------------------------------------------------
def decorateFakes(args):
    inputs = Inputs(args.inputs_db)
    regions = Regions(args.regions_db)

    if args.input_file and args.input_tree:
        in_files = InputFile(args.input_file, args.input_tree, args.input_friend_files, args.input_friend_trees)
        in_files = [in_files]
    elif inputs.fakes is None:
        raise Exception("No input fakes file specified in inputs-db!")
    else:
        in_files = inputs.fakes.files.values()

    for in_file in in_files:
        if args.output_dir:
            basename = os.path.basename(in_file.name)
            out_file_name = os.path.join(args.output_dir, basename+".FF.friend")
        else:
            out_file_name = in_file.name + ".FF.friend"
        print "Decorating '{}' --> '{}'".format(in_file.name, out_file_name)

        if os.path.exists(out_file_name) and args.force_recreate:
            print "WARNING: Will force recreation of '{}'".format(out_file_name)
            sleep(3)  # panic now!
            os.remove(out_file_name)

        parallel = False
        if args.parallel and len(args.regions) > 1:
            parallel = True
            n_threads = checkCpuCount(min(args.max_threads, len(args.regions)))
            print "Will use multiprocessing with {} threads to process the regions:".format(n_threads)
            print "  {}\n".format(args.regions)
            lock = Lock()
            p = [None]*n_threads

        for r in args.regions:
            # Wrapped everything in a function so that we can multiprocess (also reset the indents)
            if not parallel:
                decorateFakesInRegion(args, inputs, regions, in_file, out_file_name, r)
            else:
                while all(x is not None and x.is_alive() for x in p):
                    # all threads are in use, wait for 1 second and check again
                    sleep(1)
                i = next(i for i in range(n_threads) if p[i] is None or not p[i].is_alive())
                p[i] = Process(target=decorateFakesInRegion, args=(args, inputs, regions, in_file, out_file_name, r, lock))
                p[i].start()

        if parallel:
            while any(x is not None and x.is_alive() for x in p):
                # some threads are still alive, wait for 1 second and check again
                sleep(1)

    print "Done!"

#-------------------------------------------------------------------------------
def decorateFakesInRegion(args, inputs, regions, in_file, out_file_name, r, lock=None):
    region = regions[r]
    region.deleteCut("tau_multiplicities")

    #if args.temporary_cuts:
    #    selection = region.temporary_selection
    #else:
    #    selection = region.selection

    if args.FRs:
        FRs = args.FRs
    else:
        FRs = region.fakes_regions

    bkgs = []
    with acquire(lock):
        print "="*30
        print "  Processing region '{}'".format(r)
        print "  Will use the following FRs:"
        for FR in FRs:
            if not FR in regions:
                raise Exception("'{}' is not defined in regions-db.".format(FR))
            print "    {}".format(FR)
            bkgs.append(regions[FR].background)
        print "  Reading F-/k-/R-factors from files, and calculating final FF..."
        print

    if args.split_mode == "1p3p":
        tau_modes = ["1p", "3p"]
    elif args.split_mode == "1p0nXn3p":
        tau_modes = ["1p0n", "1pXn", "3p"]

    F = DefaultOrderedDict(dict)
    k = DefaultOrderedDict(dict)
    R = DefaultOrderedDict(dict)

    for FR in FRs:
        bkg = regions[FR].background
        F_file = os.path.join(args.fakes_dir, args.tag, "FFactors_{0}.root")
        F_file = F_file.format(FR)
        k_file = F_file
        if args.combine_emu_FRs and not FR in ["FRQ_el", "FRQ_mu"]:
            F_file = F_file.replace("_mu.root", "_comb.root")\
                           .replace("_el.root", "_comb.root")
            k_file = k_file.replace("_mu.root", "_comb.root")\
                           .replace("_el.root", "_comb.root")
        if not os.path.exists(F_file):
            raise Exception("File '{}' does not exist.".format(F_file))
        if not os.path.exists(k_file):
            raise Exception("File '{}' does not exist.".format(k_file))
        with root_open(F_file) as f:
            for tau_mode in tau_modes:
                F[tau_mode][bkg] = f.Get("F_{}_{}".format(bkg, tau_mode))
                F[tau_mode][bkg].SetDirectory(0)
        if bkg=="QCD": continue  # QCD does not have k-factors
        with root_open(k_file) as f:
            for tau_mode in tau_modes:
                h = f.Get("k_fail_{}_{}".format(bkg, tau_mode))
                if not h:
                    h = f.Get("k_{}_{}".format(bkg, tau_mode))  # fall back to old version
                k[tau_mode][bkg] = h
                k[tau_mode][bkg].SetDirectory(0)

    R_file = os.path.join(args.fakes_dir, args.tag, "RFactors_{0}.root")
    R_file = R_file.format(r)
    with root_open(R_file) as f:
        for bkg in bkgs:
            if bkg=="QCD": continue
            for tau_mode in tau_modes:
                R[tau_mode][bkg] = f.Get("R_{}_{}_{}".format(bkg, r, tau_mode))
                R[tau_mode][bkg].SetDirectory(0)

    # First multiply R by k
    for bkg in bkgs:
        if bkg=="QCD": continue
        for tau_mode in tau_modes:
            R[tau_mode][bkg].Multiply(k[tau_mode][bkg])
    for tau_mode in tau_modes:
        keepRFactorsInRange(R[tau_mode].values())

    # Calculate R_QCD
    for tau_mode in tau_modes:
        R[tau_mode]["QCD"] = getRFactorsQCD(R[tau_mode].values())

    ## Don't need stat error on R
    #for tau_mode in tau_modes:
    #    removeHistErrors(R[tau_mode].values())

    FF = {x:{} for x in tau_modes}

    # Nominal FF
    for tau_mode in tau_modes:
        FF[tau_mode]["NOMINAL"] = getFinalFFactor(F[tau_mode], R[tau_mode])

    ## R_W variations
    #for syst_tau_mode in tau_modes:
    #    R_Wup = getRVariation(R[syst_tau_mode], 1.5, "Wjets")
    #    R_Wdown = getRVariation(R[syst_tau_mode], 0.5, "Wjets")
    #    for tau_mode in tau_modes:
    #        if tau_mode == syst_tau_mode:
    #            FF[tau_mode][syst_tau_mode.upper()+"_R_W_up"] \
    #              = getFinalFFactor(F[tau_mode], R_Wup)
    #            FF[tau_mode][syst_tau_mode.upper()+"_R_W_down"] \
    #              = getFinalFFactor(F[tau_mode], R_Wdown)
    #        else:
    #            FF[tau_mode][syst_tau_mode.upper()+"_R_W_up"] \
    #              = FF[tau_mode]["NOMINAL"]
    #            FF[tau_mode][syst_tau_mode.upper()+"_R_W_down"] \
    #              = FF[tau_mode]["NOMINAL"]

    # stat variations
    for syst_tau_mode in tau_modes:
        nom = FF[syst_tau_mode]["NOMINAL"]
        if syst_tau_mode in ["1p", "1p0n", "1pXn"]:
            # 2D binning
            N_pt = nom.GetNbinsX()
            N_track_pt = nom.GetNbinsY()
            for iPt in xrange(1, N_pt+1):
                for iTrkPt in xrange(1, N_track_pt+1):
                    idx = nom.GetBin(iPt, iTrkPt)
                    val = nom.GetBinContent(idx)
                    err = nom.GetBinError(idx)

                    syst = "{}_PTBIN{}_TRKPTBIN{}_STAT_up".format(syst_tau_mode.upper(), iPt, iTrkPt)
                    varied_FF = nom.Clone()
                    varied_FF.SetBinContent(idx, val+err)
                    for tau_mode in tau_modes:
                        if tau_mode == syst_tau_mode:
                            FF[tau_mode][syst] = varied_FF
                        else:
                            FF[tau_mode][syst] = FF[tau_mode]["NOMINAL"]

                    syst = "{}_PTBIN{}_TRKPTBIN{}_STAT_down".format(syst_tau_mode.upper(), iPt, iTrkPt)
                    varied_FF = nom.Clone()
                    varied_FF.SetBinContent(idx, val-err)
                    for tau_mode in tau_modes:
                        if tau_mode == syst_tau_mode:
                            FF[tau_mode][syst] = varied_FF
                        else:
                            FF[tau_mode][syst] = FF[tau_mode]["NOMINAL"]
        else:
            # 1D binning
            N_pt = nom.GetNbinsX()
            for idx in xrange(1, N_pt+1):
                val = nom.GetBinContent(idx)
                err = nom.GetBinError(idx)

                syst = "{}_PTBIN{}_STAT_up".format(syst_tau_mode.upper(), idx)
                varied_FF = nom.Clone()
                varied_FF.SetBinContent(idx, val+err)
                for tau_mode in tau_modes:
                    if tau_mode == syst_tau_mode:
                        FF[tau_mode][syst] = varied_FF
                    else:
                        FF[tau_mode][syst] = FF[tau_mode]["NOMINAL"]

                syst = "{}_PTBIN{}_STAT_down".format(syst_tau_mode.upper(), idx)
                varied_FF = nom.Clone()
                varied_FF.SetBinContent(idx, val-err)
                for tau_mode in tau_modes:
                    if tau_mode == syst_tau_mode:
                        FF[tau_mode][syst] = varied_FF
                    else:
                        FF[tau_mode][syst] = FF[tau_mode]["NOMINAL"]

    for tau_mode in tau_modes:
        inheritOverflows(FF[tau_mode])

    systs = FF[tau_modes[0]].keys()  # Names of all systs

    with acquire(lock):
        if lock: print "="*30
        print "  '{}': Calculated final FF and its variations. Decorate now...\n".format(r)

    f = TFile(in_file.name)

    # We need to create temporary buffers for multiprocessing
    # Acquire lock only when we are actually ready to write to the final output file
    _, buffer_name = mkstemp(dir=os.path.dirname(out_file_name), prefix=os.path.basename(out_file_name)+".tmp_")
    f_buffer = TFile(buffer_name, "RECREATE")

    try:
        found_tree = False
        for key in f.GetListOfKeys():
            if not key.GetClassName()=="TTree":
                continue
            if args.decorate_all and key.GetName()=="weights":
                continue
            if not args.decorate_all:
                try:
                    to_match = key.GetName().split("_NOMINAL")[0]
                    to_match = to_match.split(in_file.tree)[1].strip("_")
                    assert r == to_match
                except:
                    continue
            found_tree = True
            if args.decorate_all:
                out_tree_name = "FF_{}_{}".format(r, key.GetName())
            else:
                out_tree_name = "FF_{}".format(key.GetName())
            with acquire(lock):
                if lock: print "="*30
                print "  '{}': {} -> {} ...\n".format(r, key.GetName(), out_tree_name)

            t = f.Get(key.GetName())
            t.SetBranchStatus("*", 0)
            t.SetBranchStatus("runNumber", 1)
            t.SetBranchStatus("eventNumber", 1)
            t.SetBranchStatus("lbNumber", 1)
            t.SetBranchStatus("taus_n_tracks", 1)
            t.SetBranchStatus("taus_pt", 1)
            t.SetBranchStatus("tau_0_lead_track_pt", 1)
            if args.split_mode == "1p0nXn3p":
                t.SetBranchStatus("taus_decay_mode", 1)

            ### TODO: do selection?

            fd_t = root_tree(out_tree_name)
            branches = OrderedDict()
            branches["runNumber"] = 'I'
            branches["eventNumber"] = 'I'
            for syst in systs:
                branches["FF_"+syst] = 'D'
            fd_t.create_branches(branches)

            i = 0
            for event in t:
                i += 1
                if i%10000 == 0 and not lock:
                    print "    processed {} events...".format(i)

                fd_t.runNumber = event.runNumber
                fd_t.eventNumber = event.eventNumber

                pt = event.taus_pt[0]
                trk_pt = event.tau_0_lead_track_pt

                if args.split_mode == "1p3p":
                    n_trk = event.taus_n_tracks[0]
                    if n_trk == 1:
                        for s in systs:
                            fd_t.buffers["FF_"+s].set_value(findFF(FF["1p"][s], pt, trk_pt))
                    elif n_trk == 3:
                        for s in systs:
                            fd_t.buffers["FF_"+s].set_value(findFF(FF["3p"][s], pt))
                    else:
                        for s in systs:
                            fd_t.buffers["FF_"+s].set_value(0)
                elif args.split_mode == "1p0nXn3p":
                    n_trk = event.taus_n_tracks[0]
                    decay_mode = ord(event.taus_decay_mode[0])
                    if n_trk == 1 and decay_mode == 0:
                        for s in systs:
                            fd_t.buffers["FF_"+s].set_value(findFF(FF["1p0n"][s], pt, trk_pt))
                    elif n_trk == 1 and not decay_mode == 0:
                        for s in systs:
                            fd_t.buffers["FF_"+s].set_value(findFF(FF["1pXn"][s], pt))
                    elif n_trk == 3:
                        for s in systs:
                            fd_t.buffers["FF_"+s].set_value(findFF(FF["3p"][s], pt))
                    else:
                        for s in systs:
                            fd_t.buffers["FF_"+s].set_value(0)

                fd_t.fill()

            # Now copy the tree from the buffer to the final output file
            with acquire(lock):
                with root_open(out_file_name, "UPDATE") as f_out:
                    fd_t_copy = fd_t.CopyTree("")
                    # So far data and MCs have no overlapped runNumber (DSIDs)
                    # but will they in the future?
                    ###if not args.no_index:
                    ###    fd_t_copy.BuildIndex("runNumber", "eventNumber")
                    if args.keep_old_cycles:
                        f_out.WriteTObject(fd_t_copy)
                    else:
                        f_out.WriteTObject(fd_t_copy, "", "Overwrite")

        f_buffer.Close()
        f.Close()

    finally:
        os.remove(buffer_name)

    if not found_tree:
        if not args.decorate_all:
            raise Exception("Cannot find region-dedicated tree in '{}' for '{}'. If you are not running on a split ntuple with region-dedicated trees, use the option --decorate-full.".format(in_file.name, r))
        else:
            raise Exception("Cannot find trees to decorate in '{}' for '{}'.")

    with acquire(lock):
        print "="*30
        print "  Completed region '{}'".format(r)

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    decorateFakes(args)
