#!/usr/bin/env python

import os, sys
import argparse

from math import log10

from modules.Inputs import Inputs
from modules.Regions import Regions
from modules.Histograms import Histograms

from modules.general_utils import findDefaultConfigs
from modules.general_utils import root_open, rootVerbosityLevel
from modules.plot_utils import *

from ROOT import *


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser(prog='./replotFitOutputs.py')

    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")
    parser.add_argument("--histograms-db", "-H", default="", type=str, help="Histograms config in YAML.")

    parser.add_argument("dir", help="Directory of the plots.")

    parser.add_argument("--atlas-label", default="Internal", type=str, help="Text that follows the ATLAS label.")
    parser.add_argument("--signal-BR", default=1e-3, type=float, help="The BR to be normalised to when showing the overlaid signal distribution in the main panel.")
    parser.add_argument("--ratio-range", default=0.5, type=float, help="Range of the ratio panel. e.g. 0.5 = show ratios within the range [0.5, 1.5]")
    parser.add_argument("--show-chi2", action="store_true", default=False, help="Show p-value from chi2 test.")

    parser.add_argument("--force-unblind", action="store_true", default=False, help="Unblind all the regions, overriding the 'blind' property set by --regions-db.")
    parser.add_argument("--force-blind", action="store_true", default=False, help="Blind all the regions, overriding the 'blind' property set by --regions-db.")
    parser.add_argument("--blind-sensitive", action="store_true", default=False, help="Blind sensitive regions.")
    parser.add_argument("--is-fail-region", action="store_true", default=False, help="The plotted region is a fail region.")

    parser.add_argument("--output-format", dest="output_formats", default="eps,pdf,png", type=str, help="A comma-separated list of output formats, default=eps,pdf,png.")
    parser.add_argument("--no-rename-vars", action="store_true", default=False, help="Do not rename variable in the file names.")

    # Fiddle fiddle fiddle!
    parser.add_argument("--no-fit-label", action="store_true", default=False, help="Remove pre-fit/post-fit labels. Move region name from the top of the legend to the left.")
    parser.add_argument("--no-atlas-label", action="store_true", default=False, help="Remove the ATLAS label (for thesis).")
    parser.add_argument("--last-sample", default="", type=str, help="Merge all samples below the said sample into 'Others'.")
    parser.add_argument("--y-axis-scale", default=0.75, type=float, help="Adjust y-axis scale.")
    parser.add_argument("--postfit-BR", default=float('nan'), type=float, help="Specify post-fit BR. (Otherwise show 'best-fit' instead.)")
    parser.add_argument("--no-bestfit-sig-main", action="store_true", default=False, help="Remove best-fit signal in the main panel.")
    parser.add_argument("--no-bestfit-sig-ratio", action="store_true", default=False, help="Remove best-fit signal in the ratio panel.")
    parser.add_argument("--uncert-label", default="", type=str, help="Uncertainty label. e.g. 'Total' -> show 'Total uncertainty' or 'Stat' -> show 'Stat uncertainty'. No label if empty.")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args)

        if args.atlas_label.lower() == "none":
            args.atlas_label = ""

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))
    if not os.path.exists(args.histograms_db):
        raise Exception("Cannot find histograms YAML file '{}'".format(args.histograms_db))

    if not os.path.exists(args.dir) or not os.path.isdir(args.dir):
        raise Exception("Cannot access directory '{}'".format(args.dir))

    if args.force_blind and args.force_unblind:
        raise Exception("You told me to blind and unblind all regions at the same time! I am confused!")

    return

#-------------------------------------------------------------------------------
def replotFitOutputs(args):

    inputs = Inputs(args.inputs_db)
    regions = Regions(args.regions_db)
    histograms = Histograms(args.histograms_db)

    gROOT.SetBatch()
    gStyle.SetErrorX(1e-5)
    gStyle.SetEndErrorSize(0)

    for f in os.listdir(args.dir):
        if not f.endswith("Fit.root") or f.startswith("Fit_") or "model" in f:
            continue
        if f.startswith("c_corrMatrix"):
            continue
        f = os.path.splitext(f)[0]

        # Guess the region and variable from the file name (in a hard-coded way)
        # (This functionality has been moved to modules.plot_utils)
        r, v, p, prefit = guessRegionVariable(f)

        print "Replotting {}: region = {} ({}), variable = {}".format(f, r, p, v)
        region = regions[r]
        var = next((x for x in histograms.values() if x.match_name(v)), None)
        if var is None:
            raise Exception("Cannot find the variable '{}' in histograms-db.".format(v))
        remove_data = (region.blind or args.force_blind) and not args.force_unblind

        with root_open(os.path.join(args.dir, f) + ".root") as file:
            canvas = file.Get(f)
            pad_1 = canvas.GetPrimitive(f + "_pad1")
            pad_2 = canvas.GetPrimitive(f + "_pad2")

            # Identify objects in the main panel
            bkg_sig_name = ""
            bkg_name = ""
            lowest_bkg_name = ""
            for obj in pad_1.GetListOfPrimitives():
                name = obj.GetName()

                if name == "TFrame":
                    obj.SetLineWidth(0)

                elif obj.InheritsFrom("TH1D"):
                    frame_1 = obj

                elif obj.InheritsFrom("TLegend"):
                    leg = obj

                # Data is the only RooHist
                elif obj.InheritsFrom("RooHist"):
                    data = obj
                    if remove_data:
                        # It's not there if nobody can see it :)
                        data.SetMarkerSize(0)
                        data.SetLineWidth(0)
                        data.SetFillStyle(0)
                    elif args.blind_sensitive:
                        ### HARD-CODED VARIABLES AND RANGE!
                        if v == "NN_output_comb":
                            if p.startswith("1"):
                                NN_threshold = 0.70
                            else:
                                NN_threshold = 0.60
                            partialBlindData(data, (NN_threshold, 1.01))
                        elif "_mvis" in v:
                            partialBlindData(data, (75, 95))
                        elif "_mcoll" in v:
                            partialBlindData(data, (85, 110))
                        elif "_dAlpha" in v:
                            partialBlindData(data, (-0.5, 0.5))
                        elif "tautrack_" in v and "_invmass" in v:
                            partialBlindData(data, (70,90))
                    removeXErrors(data)
                    h_data = TGraphToTH1D(data, "_data", is_data=True)

                # The MC distributions are RooCurves
                elif obj.InheritsFrom("RooCurve"):

                    if name.endswith("_errorband"):
                        error_band = obj
                        error_band.SetFillStyle(3004)
                        error_band.SetLineColor(0)
                        continue

                    if name.startswith("model_") and not name.endswith("uncert]"):
                        # This is the 'total' histogram, 'remove' it from the plot
                        obj.SetLineWidth(0)

                    # Find the histograms that correspond to bkg and bkg+sig
                    # Logic:
                    #   bkg+sig pdf = pdf with the longest name with signal in it
                    #   bkg pdf = pdf with the longest name without signal in it
                    if "Ztaumu" in name or "Ztaue" in name:
                        if len(name) > len(bkg_sig_name):
                            bkg_sig_name = name
                    elif len(name) > len(bkg_name):
                        bkg_name = name

                    if args.last_sample and "_Comp" in name and '_'+args.last_sample+'_' not in name:
                        obj.SetFillColor(TColor.GetColor(255, 240, 170))
                        g_other = obj  # For legend

                    # Also find the background stacked at the bottom
                    # This helps us decide the y-axis range in log scale
                    if var.logscale and "_Comp" in name and (lowest_bkg_name == "" or len(name) < len(lowest_bkg_name)):
                        lowest_bkg_name = name

            if not bkg_sig_name:
                bkg_sig_name = bkg_name
            g_bkg_sig = pad_1.GetPrimitive(bkg_sig_name)
            h_bkg_sig = TGraphToTH1D(g_bkg_sig, "_bkg_sig", error_band)
            g_bkg = pad_1.GetPrimitive(bkg_name)
            h_bkg = TGraphToTH1D(g_bkg, "_bkg", error_band)
            if var.logscale:
                h_lowest_bkg = TGraphToTH1D(pad_1.GetPrimitive(lowest_bkg_name), "_lowestBkg")

            if args.no_bestfit_sig_main:
                g_bkg_sig.SetFillColor(kWhite)

            pad_1.SetTopMargin(0.053)
            pad_1.SetLeftMargin(0.18)
            pad_1.SetRightMargin(0.030)
            pad_1.SetBottomMargin(0.015)

            # Identify objects in the ratio panel
            for obj in pad_2.GetListOfPrimitives():
                name = obj.GetName()

                if name == "TFrame":
                    obj.SetLineWidth(0)

                elif obj.InheritsFrom("TH1D"):
                    frame_2 = obj

                elif obj.InheritsFrom("RooHist"):
                    # This is the data/bkg ratio
                    # 'Remove' it and later make our own
                    for i in xrange(obj.GetN()):
                        obj.SetPoint(i, -999, -999)

                elif obj.InheritsFrom("TLine") and obj.GetLineStyle() == 3:
                    obj.SetLineWidth(0)

            pad_2.SetTopMargin(0.042)
            pad_2.SetLeftMargin(0.18)
            pad_2.SetRightMargin(0.030)
            pad_2.SetBottomMargin(0.35)
            pad_2.SetTicks(1, 1)

            # Main panel
            pad_1.cd()

            # Set y-axis
            binSize = str(var.binSize)
            if "." in binSize:
                binSize = binSize.rstrip('0').rstrip('.')  # remove trailing zeros and decimal point
            if var.unit:
                title = "#font[42]{Events / %s %s}" % (binSize, var.unit)
            else:
                title = "#font[42]{Events / %s}" % binSize
            frame_1.GetYaxis().SetTitle(title)
            frame_1.GetYaxis().SetTitleSize(0.056)
            frame_1.GetYaxis().SetTitleOffset(1.60)
            frame_1.GetYaxis().SetLabelSize(0.048)
            frame_1.GetYaxis().SetNdivisions(510)

            # Legend
            leg.SetX1NDC(0.60)
            leg.SetX2NDC(0.95)
            if args.no_fit_label and args.last_sample == "Zll":
                leg.SetY1NDC(0.60)
            else:
                leg.SetY1NDC(0.52)
            leg.SetY2NDC(0.90)
            leg.SetMargin(0.20)
            leg.SetTextSize(0.040)

            region_title = region.root_title
            if not p == "inclusive":
                region_title = "{} {}".format(region_title, p)
            if args.is_fail_region:
                region_title = "{} (fail)".format(region_title)
            leg_header = "#scale[1.2]{%s}" % region_title
            if not args.no_fit_label:
                leg.SetHeader(leg_header)
            after_last_sample = False
            leg_entries = leg.GetListOfPrimitives()
            for entry in leg_entries:
                if not entry.InheritsFrom("TLegendEntry"):
                    continue
                entry_name = entry.GetLabel()
                if entry_name == leg_header:
                    continue
                if entry_name.startswith("L_x_"):
                    entry_name = entry_name.split('_')[2]
                if args.last_sample:
                    if after_last_sample:
                        leg.GetListOfPrimitives().Remove(entry)
                        continue
                    if args.last_sample == entry_name:
                        after_last_sample = True
                if entry_name == "Data":
                    ## Add 'Uncertainty' legend entry and put it after data
                    #if args.uncert_label:
                    #    uncert_legend = leg.AddEntry(error_band, "#font[42]{%s uncertainty}" % args.uncert_label, "F")
                    #    leg_entries.AddAfter(entry, uncert_legend)
                    #    leg_entries.RemoveLast()
                    if remove_data:
                        entry.SetLabel("#font[42]{Blinded}")
                        entry.SetObject(0)
                        # It's not there if nobody can see it :)
                        entry.SetMarkerSize(0)
                        entry.SetLineWidth(0)
                        entry.SetFillStyle(0)
                    else:
                        entry.SetLabel("#font[42]{%s}" % inputs.data.root_label)
                elif entry_name in ["Ztaumu", "Ztaue", "Ztaumugamma", "Ztauegamma"]:
                    # Remove first, add back later at the bottom
                    leg.GetListOfPrimitives().Remove(entry)
                    signal_name = entry_name
                else:
                    entry.SetLabel("#font[42]{%s}" % inputs[entry_name].root_label)

            if args.last_sample:
                leg.AddEntry(g_other, "#font[42]{Others}", "F")

            if not (args.signal_BR == 0 or args.no_bestfit_sig_main):
                #if args.postfit_BR==args.postfit_BR:
                #    coe, exp = "{:e}".format(args.postfit_BR).split('e')
                #    coe = str(float(coe)).strip(".0")
                #    exp = str(int(exp))
                #    if coe == "1":
                #        sig_BR = "10^{%s}" % exp
                #    elif coe == "-1":
                #        sig_BR = "-10^{%s}" % exp
                #    else:
                #        sig_BR = "%s#times10^{%s}" % (coe, exp)
                #    sig_BR = sig_BR.replace("-", "#minus")
                #    entry_title = "#font[42]{%s (#font[152]{B} = %s)}" % (inputs[signal_name].root_label, sig_BR)
                #elif prefit:
                if prefit:
                    entry_title = "#font[42]{%s (#font[152]{B} = 10^{#minus5})}" % inputs[signal_name].root_label
                else:
                    entry_title = "#font[42]{%s (best-fit)}" % inputs[signal_name].root_label
                leg.AddEntry(g_bkg_sig, entry_title, "F")

            # Add "uncertainty" legend before signals
            if args.uncert_label:
                uncert_legend = leg.AddEntry(error_band, "#font[42]{%s uncertainty}" % args.uncert_label, "F")

            # Clone the signal histogram and overlay it on top as a dashed line
            if prefit:
                h_sig = h_bkg_sig.Clone("{}_sigOnly".format(h_bkg_sig.GetName()))
                h_sig.Add(h_bkg, -1)
            else:
                # We don't know what the post-fit signal BR is
                # so instead we get the signal from the pre-fit plots
                prefit_plot = os.path.join(args.dir, f).replace("afterFit", "beforeFit")
                h_sig = getSignalHistogram(prefit_plot+".root")
                pad_1.cd()

            h_sig.Scale(args.signal_BR / 1e-5)
            coe, exp = "{:e}".format(args.signal_BR).split('e')
            coe = str(float(coe)).strip(".0")
            exp = str(int(exp))
            if coe == "1":
                sig_BR = "10^{%s}" % exp
            elif coe == "-1":
                sig_BR = "-10^{%s}" % exp
            else:
                sig_BR = "%s#times10^{%s}" % (coe, exp)
            sig_BR = sig_BR.replace("-", "#minus")

            if not args.signal_BR == 0:
                h_sig.SetLineColor(kRed)
                h_sig.SetLineWidth(4)
                h_sig.SetLineStyle(7)
                h_sig.Draw("HISTsame][")

                leg_label = inputs[region.signals[0]].root_label
                leg_label += " (#font[152]{B} = %s)" % sig_BR
                leg.AddEntry(h_sig, "#font[42]{%s}" % leg_label, "L")

            #sig_max = h_sig.GetMaximum() * 0.75
            #frame_max = max(frame_1.GetMaximum(), sig_max)
            frame_max = frame_1.GetMaximum()
            if var.logscale:
                pad_1.SetLogy()
                log_minimum = getNonZeroMinimum(h_lowest_bkg) / 2.
                height = log10(frame_max / log_minimum)
                buffer = pow(10, height * args.y_axis_scale)
                frame_1.GetYaxis().SetRangeUser(log_minimum, frame_1.GetMaximum()*buffer)
            else:
                height = frame_max
                buffer = height * args.y_axis_scale
                frame_1.GetYaxis().SetRangeUser(0.0, height+buffer)

            # Redraw the legend to make sure it is on top of the dashed signal line
            pad_1.cd()
            leg.Draw()


            # Add ATLAS label
            pt =  TPaveText(0.19, 0.81, 0.63, 0.94, "NDC")
            pt.SetBorderSize(0)
            pt.SetFillStyle(0)
            pt.SetLineStyle(0)

            if not args.no_atlas_label:
                atlas = pt.AddText("#font[72]{ATLAS} #font[42]{%s}" % args.atlas_label)
                atlas.SetTextFont(72)
                atlas.SetTextSize(0.056)
                atlas.SetTextAlign(13)

            if "run1" in r:
                lumi = pt.AddText("#font[42]{#sqrt{s} = 8 TeV, 20.3 fb^{-1}}")
            else:
                if inputs.lumi < 100:
                    lumi_str = "%.1f" % inputs.lumi  # 36.2 fb-1 / 79.8 fb-1
                else:
                    lumi_str = "%.0f" % inputs.lumi  # 139 fb-1
                lumi = pt.AddText("#font[42]{#sqrt{s} = 13 TeV, %s fb^{-1}}" % lumi_str)
            lumi.SetTextFont(42)
            if args.no_atlas_label:
                lumi.SetTextSize(0.044)
            else:
                lumi.SetTextSize(0.04)
            lumi.SetTextAlign(13)

            pt.Draw()

            # Add pre-/post-fit label
            pt_2 = TPaveText(0.25, 0.67, 0.63, 0.82, "NDC")
            pt_2.SetBorderSize(0)
            pt_2.SetFillStyle(0)
            pt_2.SetLineStyle(0)

            if args.no_fit_label:
                pre_or_post = pt_2.AddText("#font[42]{%s}" % region_title)
                pre_or_post.SetTextSize(0.045)
            elif prefit:
                pre_or_post = pt_2.AddText("#font[42]{Pre-fit}")
                pre_or_post.SetTextSize(0.055)
            else:
                pre_or_post = pt_2.AddText("#font[42]{Post-fit}")
                pre_or_post.SetTextSize(0.055)
            pre_or_post.SetTextFont(42)
            pre_or_post.SetTextAlign(13)
            pt_2.Draw()  # pre-/post-fit

            # Add chi2 p-value
            if args.show_chi2 and not prefit:
                pt_3 = TPaveText(0.21, 0.60, 0.59, 0.75, "NDC")
                pt_3.SetBorderSize(0)
                pt_3.SetFillStyle(0)
                pt_3.SetLineStyle(0)
                chi2 = pt_3.AddText("#font[42]{#it{#chi}^{2} test: #it{p}=%.3f}" % h_data.Chi2Test(h_bkg_sig, "UW"))
                chi2.SetTextFont(42)
                chi2.SetTextSize(0.050)
                chi2.SetTextAlign(13)
                pt_3.Draw()

            # Ratio panel
            pad_2.cd()
            pad_2.SetGridy(0)
            pad_2.SetAttLinePS(0, 0, 0);

            # Set x-axis
            if var.unit:
                title = "#font[42]{%s [%s]}" % (var.root_label, var.unit)
            else:
                title = "#font[42]{%s}" % var.root_label
            frame_2.GetXaxis().SetTitle(title)
            frame_2.GetXaxis().SetTitleSize(0.14)
            frame_2.GetXaxis().SetTitleOffset(1.10)
            frame_2.GetXaxis().SetLabelSize(0.12)

            if remove_data:
                frame_2.GetYaxis().SetTitle("(B+S) / B")
            else:
                frame_2.GetYaxis().SetTitle("Data / pred.")
            frame_2.GetYaxis().SetTitleSize(0.14)
            frame_2.GetYaxis().SetTitleOffset(0.62)
            frame_2.GetYaxis().SetLabelSize(0.12)
            frame_2.SetAxisRange(1-args.ratio_range, 1+args.ratio_range, "Y");
            frame_2.GetYaxis().SetNdivisions(-504);

            h_bkg_no_errors = h_bkg.Clone("h_bkg_no_errors")
            removeHistErrors(h_bkg_no_errors)

            if not (args.signal_BR == 0 or args.no_bestfit_sig_ratio):
                h_bkg_sig.Divide(h_bkg_no_errors)
                h_bkg_sig.SetLineColor(kRed)
                h_bkg_sig.SetLineWidth(2)
                h_bkg_sig.Draw("hist same ][")
                leg_bkg_sig = TLegend(0.2, 0.8, 0.5, 0.9)
                leg_bkg_sig.SetLineWidth(0)
                leg_bkg_sig.SetFillStyle(0)
                leg_bkg_sig.SetMargin(0.20)
                leg_bkg_sig.SetTextSize(0.080)
                if args.postfit_BR==args.postfit_BR:
                    coe, exp = "{:e}".format(args.postfit_BR).split('e')
                    coe = str(float(coe)).strip(".0")
                    exp = str(int(exp))
                    if coe == "1":
                        sig_BR = "10^{%s}" % exp
                    #elif coe == "-1":
                    #    sig_BR = "-10^{%s}" % exp
                    else:
                        sig_BR = "%s#times10^{%s}" % (coe, exp)
                    sig_BR = sig_BR.replace("-", "#minus")
                    leg_bkg_sig.AddEntry(h_bkg_sig, "Best-fit signal (#font[152]{B} = %s)" % sig_BR, "L")
                elif prefit:
                    leg_bkg_sig.AddEntry(h_bkg_sig, "Pre-fit signal (#font[152]{B} = 10^{#minus5})", "L")
                else:
                    leg_bkg_sig.AddEntry(h_bkg_sig, "Best-fit signal", "L")
                leg_bkg_sig.Draw()

            if not remove_data:
                h_data.Divide(h_bkg_no_errors)
                h_data.SetMarkerStyle(kFullCircle)
                h_data.SetMarkerColor(kBlack)
                h_data.SetLineColor(kBlack)
                h_data.Draw("E0same")
                #leg_data = TLegend(0.2, 0.8, 0.4, 0.9)
                #leg_data.SetLineWidth(0)
                #leg_data.SetFillStyle(0)
                #leg_data.SetMargin(0.20)
                #leg_data.SetTextSize(0.080)
                #leg_data.AddEntry(h_data, "Data / Bkg", "P")
                #leg_data.Draw()

                # Add arrows when out-of-range
                arrow = TArrow()
                arrow.SetArrowSize(0.015)
                arrow.SetFillColor(kBlack)
                arrow.SetLineColor(kBlack)
                for i in xrange(1, h_data.GetNbinsX()+1):
                    x = h_data.GetBinCenter(i)
                    y = h_data.GetBinContent(i)
                    if y > 1+args.ratio_range:
                        arrow.DrawArrow(x, 1+0.9*args.ratio_range, x, 1+args.ratio_range)
                    elif y < 1-args.ratio_range:
                        arrow.DrawArrow(x, 1-0.9*args.ratio_range, x, 1-args.ratio_range)

            canvas.Draw()
            canvas.SetCanvasSize(500,600)

            enum = enumeratePlots(r, var.name, p)
            if not args.no_rename_vars:
                f = f.replace(v, var.name)
            f = os.path.join(args.dir, "Plot_{}_{}".format(enum, f))
            if args.is_fail_region:
                f = (r+"_fail").join(f.rsplit(r, 1))
            for format in args.output_formats.split(","):
                canvas.SaveAs(f + "." + format)

            # Python's GC doesn't usually work well with ROOT's objects
            h_data.Delete()
            h_bkg_sig.Delete()
            h_bkg.Delete()
            h_sig.Delete()
            if var.logscale: h_lowest_bkg.Delete()

        pass  # root_open
    pass  # files
    ### TODO: Always seg fault when the script exits...

#-------------------------------------------------------------------------------
if __name__ == '__main__':
    args = parseArgs(sys.argv[1:])
    with rootVerbosityLevel("Warning"):
        replotFitOutputs(args)
