#!/usr/bin/env python

import os, sys, shutil, inspect
import argparse

from modules.general_utils import findDefaultConfigs, flattenCommaSeparatedLists
from modules.Inputs import Inputs
from modules.Regions import Regions
from modules.Histograms import Histograms

if __name__ == '__main__':
    import inspect
    from modules.general_utils import execute
    from modules.plot_utils import cleanUpPlotDirectory, friendlyBranchName

else:
    from modules.Systematics import Systematics
    from modules.Plots import Plots
    from modules.plot_utils import friendlyBranchName
    from modules.HistFitter_utils import *
    from numpy import random


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser(prog='./fitCombined.py')

    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")
    parser.add_argument("--histograms-db", "-H", default="", type=str, help="Histograms config in YAML.")
    parser.add_argument("--systematics-db", "-S", default="", type=str, help="Systematics config in YAML.")
    parser.add_argument("--plots-db", "-P", default="", type=str, help="Plots config in YAML.")

    parser.add_argument("--tag", "-t", default="foo", type=str, help="The input/output tag. HistFitter will find the combined workspace in './results/fit_<tag>'.")

    parser.add_argument("--fit-region", "-r", default="", type=str, help="Fit region.")
    parser.add_argument("--fit-variable", "-v", default="", type=str, help="Fit variable.")

    parser.add_argument("--con-region", dest="con_regions", default=[], type=str, action='append', help="Control region. Can have multiple regions. The fit region and control regions must NOT repeat.")
    parser.add_argument("--con-variable", dest="con_variables", default=[], type=str, action='append', help="Control region variable. Corresponds to the control region(s), one-to-one, in order.")

    parser.add_argument("--val-region", dest="val_regions", default=[], type=str, action='append', help="Validation region. Can have multiple regions. The fit region can also be a validation region (plotting other variables)")
    parser.add_argument("--val-variable", dest="val_variables", default=[], type=str, action='append', help="Validation variable. Can have multiple variables. Common in all regions. Overrides --plots-db.")

    parser.add_argument("--UL-scan-range", type=float, default=2.0, help="Upper bound of the upper limit scan range (the lower bound is always zero). Default to be 2.0")
    parser.add_argument("--UL-scan-points", type=int, default=50, help="Number of scan points. Default to be 51")

    parser.add_argument("--force-unblind", action="store_true", default=False, help="Unblind all the regions, overriding the 'blind' property set by --regions-db. Use with caution!")
    parser.add_argument("--force-blind", action="store_true", default=False, help="Blind all the regions, overriding the 'blind' property set by --regions-db.")
    parser.add_argument("--plot-blind-high-NN", action="store_true", default=False, help="Blind high NN region in output plots (argument passed to replotFitOutputs.py).")
    parser.add_argument("--randomise-BR", action="store_true", default=False, help="Randomise signal BR so that we can validate the fit without fully unblinding.")
    parser.add_argument("--ignore-stat", action="store_true", default=False, help="Ignore statistical errors.")

    parser.add_argument("--no-split-prongs", dest="split_prongs", action="store_false", default=True, help="Do not plot events with 1-prong and 3-prong taus separately.")
    parser.add_argument("--split-mode", type=str, choices=["1p3p", "1p0nXn3p"], default="1p3p", help="How to split tau_had decay mode ('1p3p' or '1p0nXn3p').")

    parser.add_argument("--signal-BR", default=1e-3, type=float, help="The signal BR. Used ONLY for plotting. Does NOT affect the actual fit. Default is 1e-3 (100 * LEP tau-mu limit).")

    parser.add_argument("--no-best-fit-plots", action="store_true", default=False, help="Do not make best-fit plots.")
    parser.add_argument("--no-limit-scan", action="store_true", default=False, help="Do not perform upper limit scan.")
    parser.add_argument("--no-discovery", action="store_true", default=False, help="Do not perform discovery hypothesis test.")
    parser.add_argument("--ranking", action="store_true", default=False, help="Create NP rankings.")
    parser.add_argument("--ranking-with-gamma", action="store_true", default=False, help="Create NP rankings with gamma's (MC statistical errors).")
    parser.add_argument("--plot-NLL", dest="plot_NLL_params", default=[], type=str, action='append', help="Parameters for which the projected NLL will be plotted.")

    parser.add_argument("--verbose", "-V", action="store_true", default=False, help="Run HistFitter in verbose mode.")
    parser.add_argument("--log-level", "-L", default="", type=str, help="Set log level of HistFitter, e.g. 'VERBOSE', 'DEBUG'")

    # Backward compatibility / ad-hoc settings / hacks
    parser.add_argument("--run-1", action="store_true", help="Run-1 workspace has been included in combination.")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args)

        args.con_regions = flattenCommaSeparatedLists(args.con_regions)
        args.con_variables = flattenCommaSeparatedLists(args.con_variables)

        args.val_regions = flattenCommaSeparatedLists(args.val_regions)
        args.val_variables = flattenCommaSeparatedLists(args.val_variables)

        args.plot_NLL_params = flattenCommaSeparatedLists(args.plot_NLL_params)

        if args.verbose and not args.log_level:
            args.log_level = "VERBOSE"

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))
    if not os.path.exists(args.histograms_db):
        raise Exception("Cannot find histograms YAML file '{}'".format(args.histograms_db))
    #if not os.path.exists(args.systematics_db):
    #    raise Exception("Cannot find systematics YAML file '{}'".format(args.systematics_db))
    if not args.val_variables and not os.path.exists(args.plots_db):
        raise Exception("Cannot find plots YAML file '{}'".format(args.plots_db))

    if not args.fit_region:
        raise Exception("No fit region specified.")

    if not args.fit_variable:
        raise Exception("No fit variable specified.")

    if not len(args.con_regions) == len(args.con_variables):
        raise Exception("Number of control regions doesn't match the number of control region variables. Each control region must have one and only one variable to fit!")

    if args.force_blind and args.force_unblind:
        raise Exception("You told me to blind and unblind all regions at the same time! I am confused!")

#-------------------------------------------------------------------------------
# I am being directly called (by shell),
# I will first call HistFitter.py to use myself as a config file to make best-fit plots
# Then call HistFitter.py once again to find upper limit
# At last I call various scripts to make pretty plots/tables
if __name__ == '__main__':

    args = parseArgs(sys.argv[1:])
    this_file = inspect.getfile(inspect.currentframe())
    inputs = Inputs(args.inputs_db)
    histograms = Histograms(args.histograms_db)
    regions = Regions(args.regions_db)
    signal = regions[args.fit_region].signals[0]

    #-------------------------------------------------
    # Run Best-fit
    HistFitter = "HistFitter.py {} -f -m ALL".format(this_file)
    if args.randomise_BR or args.no_best_fit_plots:
        HistFitter += " -D corrMatrix"
    else:
        HistFitter += " -D corrMatrix,before,after"
    HistFitter += " -u='{}'".format(" ".join(sys.argv[1:]).replace(" -f ", " --force-recreate "))
    if args.log_level:
        HistFitter += " -L {}".format(args.log_level)

    returncode = execute(HistFitter, log="results/fit_{0}/fit_{0}.log".format(args.tag))
    if not returncode == 0:
        print "Failed running HistFitter: '{}'".format(HistFitter)
        print "Return code non-zero: {}".format(returncode)
        sys.exit(returncode)

    #-------------------------------------------------
    # HistFitter's plots aren't pretty enough
    # Use replotFitOutputs.py to make them look better
    replotFitOutputs = "./" + this_file.replace("fitCombined.py", "replotFitOutputs.py")
    replotFitOutputs += " -I " + args.inputs_db
    replotFitOutputs += " -R " + args.regions_db
    replotFitOutputs += " -H " + args.histograms_db
    replotFitOutputs += " --signal-BR {}".format(args.signal_BR)
    if args.force_blind:
        replotFitOutputs += " --force-blind"
    if args.force_unblind:
        replotFitOutputs += " --force-unblind"
    if args.plot_blind_high_NN:
        replotFitOutputs += " --blind-high-NN"
    replotFitOutputs += " results/fit_{}".format(args.tag)
    if not args.no_best_fit_plots:
        # replotFitOutputs.py always exits with seg fault
        # But that's pretty harmless... so let's ignore it with a try-block for now
        try:
            execute(replotFitOutputs)
        except Exception as exception:
            pass

    cleanUpPlotDirectory("results/fit_{}".format(args.tag), remove_fit_parameters=args.randomise_BR)

    #-------------------------------------------------
    # PrintWS.C
    os.chdir("results/fit_{}".format(args.tag))

    fit_workspace = "Fit_{}_comb_combined_NormalMeasurement_model.root".format(signal)

    PrintWS = r'root -l -b -q ../../cpp_macros/PrintWS.C\(\"{}\"\)'.format(fit_workspace)
    PrintWS_succeed = True
    try:
        execute(PrintWS)
    except:
        print "ERROR: Failed running PrintWS: '{}'".format(PrintWS)
        PrintWS_succeed = False

    pdflatex = "pdflatex -interaction=batchmode PrintWS.tex"
    try:
        execute(pdflatex)
    except:
        print "ERROR: Failed running pdflatex: '{}'".format(pdflatex)
        PrintWS_succeed = False

    #mv = "mv -v PrintWS.* results/fit_{}/.".format(args.tag)
    #returncode = execute(mv, shell=True)
    #if not returncode == 0:
    #    print "Failed moving files: '{}'".format(mv)
    #    print "Return code non-zero: {}".format(returncode)
    #    sys.exit(returncode)

    #-------------------------------------------------
    # YieldsTable.py
    postfit_workspace = fit_workspace.replace(".root", "_afterFit.root")
    fit_variable = friendlyBranchName(histograms[args.fit_variable].branch)
    con_variables = [friendlyBranchName(histograms[v].branch) for v in args.con_variables]
    if args.split_prongs:
        suffix = "_ZLFV_tauhad"
        if args.split_mode == "1p3p":
            regions = "{reg}_1P_{var}{sfx},{reg}_3P_{var}{sfx}".format(reg=args.fit_region, var=fit_variable, sfx=suffix)
            for i,r in enumerate(args.con_regions):
                regions += ",{reg}_1P_{var}{sfx},{reg}_3P_{var}{sfx}".format(reg=r, var=con_variables[i], sfx=suffix)
        elif args.split_mode == "1p0nXn3p":
            regions = "{reg}_1p0n_{var}{sfx},{reg}_1pXn_{var}{sfx},{reg}_3p_{var}{sfx}".format(reg=args.fit_region, var=fit_variable, sfx=suffix)
            for i,r in enumerate(args.con_regions):
                regions += ",{reg}_1p0n_{var}{sfx},{reg}_1pXn_{var}{sfx},{reg}_3p_{var}{sfx}".format(reg=r, var=con_variables[i], sfx=suffix)
    else:
        suffix = "_ZLFV_taulep" if "taulep" in args.fit_region else "_ZLFV_taulep"
        regions = "{reg}_{var}{sfx}".format(reg=args.fit_region, var=fit_variable, sfx=suffix)
        for i,r in enumerate(args.con_regions):
            suffix = "_ZLFV_taulep" if "taulep" in r else "_ZLFV_taulep"
            regions += ",{reg}_{var}{sfx}".format(reg=r, var=con_variables[i], sfx=suffix)
    if args.run_1:
        # Hard-coded for Run-1 Ztaumu workspace
        regions += ",SR1_ZLFV_run1,SR2_ZLFV_run1,WCR_ZLFV_run1,TCR_ZLFV_run1"

    samples = ",".join(inputs.bkgs.keys()[::-1] + [signal])
    samples = "fakes," + samples

    YieldsTable_succeed = True
    if not args.randomise_BR:

        YieldsTable = r"YieldsTable.py -b -w {} -s {} -c {} -C 'Yields table.' -L 'tab:Yields_Table' -o 'YieldsTable.tex'".format(postfit_workspace, samples, regions)
        if args.force_blind:
            YieldsTable += " -B"
        try:
            execute(YieldsTable)
        except:
            print "ERROR: Failed running YieldsTable: '{}'".format(YieldsTable)
            YieldsTable_succeed = False


        pdflatex = "pdflatex -interaction=batchmode YieldsTable.tex"
        try:
            execute(pdflatex)
        except:
            print "ERROR: Failed running pdflatex: '{}'".format(pdflatex)
            YieldsTable_succeed = False

    os.chdir("../..")

    exit(1)

    ### TODO: SysTable.py

    #-------------------------------------------------
    # Plot NLLs
    if len(args.plot_NLL_params) > 0:
        HistFitter_NLL = HistFitter.replace(" -D corrMatrix", " -D likelihood")\
                                   .replace(",before,after", "")\
                                   .replace(" --force-recreate ", " --use-existing-WS ")
        for i,p in enumerate(args.plot_NLL_params):
            if not (p.startswith("mu_") or p.startswith("gamma_") or p.startswith("alpha_")):
                args.plot_NLL_params[i] = "alpha_" + p
        HistFitter_NLL = HistFitter_NLL.replace("-m ALL", "-m {}".format(",".join(args.plot_NLL_params)))
        returncode = execute(HistFitter_NLL)
        if not returncode == 0:
            print "Failed running HistFitter: '{}'".format(HistFitter_NLL)
            print "Return code non-zero: {}".format(returncode)
            sys.exit(returncode)

    #-------------------------------------------------
    # Run UL scan
    if not args.no_limit_scan:
        HistFitter_l = HistFitter.replace(" -f ", " -l ").replace(" --force-recreate ", " ")
        returncode = execute(HistFitter_l, log="results/fit_{0}/ULScan_{0}.log".format(args.tag))
        if not returncode == 0:
            print "Failed running HistFitter: '{}'".format(HistFitter_l)
            print "Return code non-zero: {}".format(returncode)
            sys.exit(returncode)

    #-------------------------------------------------
    # Run discovery hypotest
    if not args.no_discovery:
        HistFitter_z = HistFitter.replace(" -f ", " -z ").replace(" --force-recreate ", " ")
        returncode = execute(HistFitter_z, log="results/fit_{0}/DiscHypoTest_{0}.log".format(args.tag))
        if not returncode == 0:
            print "Failed running HistFitter: '{}'".format(HistFitter_z)
            print "Return code non-zero: {}".format(returncode)
            sys.exit(returncode)

    #-------------------------------------------------
    # NP rankings
    os.chdir("results/fit_{}".format(args.tag))

    if args.ranking or args.ranking_with_gamma:
        poi = "mu_BR_{}".format(signal.strip('Z'))

        if args.ranking:
            ranking = r'SystRankingPlot.py -w {} -f {} -p {} -n ranking --np mu_*,alpha_*'.format(postfit_workspace, regions, poi)
            returncode = execute(ranking)
            if not returncode == 0:
                print "Failed running SystRankingPlot: '{}'".format(ranking)
                print "Return code non-zero: {}".format(returncode)
                sys.exit(returncode)

        if args.ranking_with_gamma:
            ranking = r'SystRankingPlot.py -w {} -f {} -p {} -n ranking_with_gamma'.format(postfit_workspace, regions, poi)
            returncode = execute(ranking)
            if not returncode == 0:
                print "Failed running SystRankingPlot: '{}'".format(ranking)
                print "Return code non-zero: {}".format(returncode)
                sys.exit(returncode)

    os.chdir("../..")

    if not PrintWS_succeed:
        print "WARNING: Failed to create PrintWS pdf!!"
    if not YieldsTable_succeed:
        print "WARNING: Failed to create Yields Table pdf!!"

    print "All done!!"

#-------------------------------------------------------------------------------
# I am being called by HistFitter.py, setup the fit
else:
    args = parseArgs(configMgr.userArg.split())

    print "===================================================="
    print "Running HistPlotter with the following user configs:"
    print "===================================================="
    print "       inputs-db : %s" % args.inputs_db
    print "      regions-db : %s" % args.regions_db
    print "   histograms-db : %s" % args.histograms_db
    print "        plots-db : %s" % args.plots_db
    print "             tag : %s" % args.tag
    print "      fit-region : %s" % args.fit_region
    print "    fit-variable : %s" % args.fit_variable
    print "     con-regions : %s" % args.con_regions
    print "   con-variables : %s" % args.con_variables
    print "     val-regions : %s" % args.val_regions
    print "   val-variables : %s" % args.val_variables
    print "   UL-scan-range : %s" % args.UL_scan_range
    print "  UL-scan-points : %s" % args.UL_scan_points
    print "   force-unblind : %s" % args.force_unblind
    print "     force-blind : %s" % args.force_blind
    print "    split-prongs : %s" % args.split_prongs
    print "      split-mode : %s" % args.split_mode
    print "       signal-BR : %s" % args.signal_BR
    print "===================================================="

    inputs = Inputs(args.inputs_db)
    regions = Regions(args.regions_db)
    histograms = Histograms(args.histograms_db)
    #systematics = Systematics(args.systematics_db)

    fit_region = regions[args.fit_region]

    con_variables = {r:args.con_variables[i] for i,r in enumerate(args.con_regions)}

    if args.val_variables:
        val_variables = {r:args.val_variables for r in args.val_regions}
    else:
        plots = Plots(args.plots_db)
        val_variables = {r:plots[r] for r in plots if r in args.val_regions}

    #if args.alt_bkgs:
    #    inputs.useAlt()

    configMgr.myFitType = FitType.Discovery if doDiscoveryHypoTests else FitType.Exclusion

    configMgr.analysisName = "fit_{}".format(args.tag)
    configMgr.outputFileName = "results/{0}/{0}_foo.root ".format(configMgr.analysisName)
    configMgr.histCacheFile = ""
    configMgr.useHistBackupCacheFile = False
    configMgr.histBackupCacheFile = ""
    configMgr.useCacheToTreeFallback = False
    configMgr.readFromTree = False
    configMgr.executeHistFactory = False

    # All has to be 1 otherwise the fakes weights hack won't work!
    configMgr.outputLumi = 1.
    configMgr.inputLumi = 1.
    configMgr.lumiUnits = 1.
    configMgr.nomName = "_NOMINAL"

    configMgr.fixSigXSec = True
    configMgr.runOnlyNominalXSec = True

    configMgr.ReduceCorrMatrix = True

    configMgr.calculatorType = 2
    configMgr.testStatType = 3   # 3=one-sided profile likelihood test statistic (LHC default)
    configMgr.generateAsimovDataForObserved = False
    configMgr.scanRange = (args.UL_scan_range/args.UL_scan_points, args.UL_scan_range)
    configMgr.nPoints = args.UL_scan_points
    configMgr.disableULRangeExtension = True

    #-------------------------------------------------
    # Fit config instance and measurement instance
    signal = inputs[fit_region.signals[0]]
    fit = configMgr.addFitConfig("Fit_{}_comb".format(signal.name))
    fit.statErrThreshold = 100. if args.ignore_stat else 0.

    meas = fit.addMeasurement(name="NormalMeasurement", lumi=1.0, lumiErr=1.0)
    meas.addPOI("mu_BR_{}".format(signal.name.strip('Z')))
    #if args.randomise_BR:
    #    meas.addParamSetting("mu_SIG_randSF", True, randomScaleFactor)

    #-------------------------------------------------
    # Add samples
    bkg_samples = []
    for b in inputs.bkgs:
        bkg_samples.append(Sample(b, GetColor(*inputs[b].colour)))
    bkg_samples.append(Sample("fakes", GetColor(*inputs.fakes.colour)))
    bkg_samples.append(Sample("Di-Boson", GetColor(*inputs["diboson"].colour)))  # HACK

    sig_samples = []
    sig_samples.append(Sample(signal.name, GetColor(*signal.colour)))

    if args.run_1:
        # Hard-coded for Run-1 Ztaumu workspace
        bkg_samples.append(Sample("OtherBKG", GetColor(*inputs["diboson"].colour)))
        bkg_samples.append(Sample("Htt", GetColor(*inputs["Higgs"].colour)))
        bkg_samples.append(Sample("Wj", GetColor(*inputs["Wjets"].colour)))
        bkg_samples.append(Sample("Top", GetColor(*inputs["top"].colour)))
        bkg_samples.append(Sample("Ztt", GetColor(*inputs["Ztautau"].colour)))
        bkg_samples.append(Sample("SSData", GetColor(*inputs.fakes.colour)))

        sig_samples.append(Sample("Ztmu", ROOT.kRed))

    fit.addSamples(sig_samples + bkg_samples)
    for s in sig_samples:
        fit.setSignalSample(s)

    #-------------------------------------------------
    # Add channels
    all_regions = [args.fit_region] + args.con_regions
    all_variables = [args.fit_variable] + args.con_variables

    for r, v in zip(all_regions, all_variables):
        v = friendlyBranchName(histograms[v].branch)
        if "taulep" in r:
            c = "taulep"
            tau_prongs = [""]
        else:
            c = "tauhad"
            if args.split_prongs:
                if args.split_mode == "1p3p":
                    tau_prongs = ["_1P", "_3P"]
                elif args.split_mode == "1p0nXn3p":
                    tau_prongs = ["_1p0n", "_1pXn", "_3p"]
            else:
                tau_prongs = [""]
        for p in tau_prongs:
            channel = fit.addChannel("", ["{}{}_{}_ZLFV_{}".format(r, p, v, c)], 1, 0, 1)
            channel.channelName = channel.channelName[:-1]
            if args.force_blind:
                channel.blind = True
            fit.addSignalChannels(channel)

    if args.run_1:
        # Hard-coded for Run-1 Ztaumu workspace
        SRs = []
        SRs.append(fit.addChannel("", ["SR1_ZLFV_run1"], 1, 0, 1))
        SRs.append(fit.addChannel("", ["SR2_ZLFV_run1"], 1, 0, 1))
        for SR in SRs:
            SR.channelName = SR.channelName[:-1]
            fit.addSignalChannels(SR)

        CRs = []
        CRs.append(fit.addChannel("", ["TCR_ZLFV_run1"], 1, 0, 1))
        CRs.append(fit.addChannel("", ["WCR_ZLFV_run1"], 1, 0, 1))
        for CR in CRs:
            CR.channelName = CR.channelName[:-1]
            fit.addBkgConstrainChannels(CR)
