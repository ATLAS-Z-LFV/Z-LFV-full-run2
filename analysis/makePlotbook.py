#!/usr/bin/env python

import os, sys
import argparse
import fnmatch
from glob import glob

from modules.general_utils import checkMakeFile, flattenCommaSeparatedLists
from modules.Plotbook import Plotbook


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser("./makePlotbook.py")
    
    parser.add_argument("--output-file", "-o", default="plotbook.tex", type=str, help="Output tex file name (including path).")
    
    parser.add_argument("--input-dir", "-i", default="./results", type=str, help="Input directory.")
    parser.add_argument("--input-pattern", "-p", dest='input_patterns', default=[], action='append', type=str, help="Input file name pattern(s). Use shell-style wildcards. Feel free to include subdirectory names in the pattern.")
    parser.add_argument("--ignore-pattern", dest='ignore_patterns', default=[], action='append', type=str, help="Input file name patterns to be ignored. Use shell-style wildcards.")
    
    parser.add_argument("--N-columns", "-x", type=int, default = 4, help="Number of columns in each page." )
    parser.add_argument("--N-rows", "-y", type=int, default = 4, help="Number of rows in each page." )
    parser.add_argument("--show-file-names", action="store_true", default = False, help="Show file names in captions." )
    parser.add_argument("--column-major", action="store_true", default = False, help="Show plots in column-major order (within each page)." )
    
    parser.add_argument("--no-compile", default=False, action="store_true", help="Do not compile the tex file into PDF file.")
    
    try:
        args = parser.parse_args(argv)
        
        args.input_patterns = flattenCommaSeparatedLists(args.input_patterns)
        args.ignore_patterns = flattenCommaSeparatedLists(args.ignore_patterns)
        
        checkArgs(args)
        
    except:
        parser.print_help()
        raise
    
    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if len(args.input_patterns) == 0:
        raise Exception("You haven't specified any input file patterns.")
    checkMakeFile(args.output_file)

#-------------------------------------------------------------------------------
# Swap the order of "XXX_afterFit.pdf" and "XXX_beforeFit.pdf"
# so the pre-fit plot comes before the post-fit one
def switchPrePostFitOrder(plots):
    enum = range(len(plots)-1)
    for i, plot, next_plot in zip(enum, plots[:-1], plots[1:]):
        if plot.replace("afterFit", "") == next_plot.replace("beforeFit", ""):
            plots.remove(next_plot)
            plots.insert(i, next_plot)

#-------------------------------------------------------------------------------
def makePlotbook(args):
    plotbook = Plotbook(args.output_file, args.N_columns, args.N_rows, args.column_major, args.show_file_names)
    
    #-----------------------------
    # Get the list of plot file names
    input_files = []
    for pattern in args.input_patterns:
        files = glob(os.path.join(args.input_dir, pattern))
        files.sort()
        switchPrePostFitOrder(files)
        input_files += files
    
    ignored_files = set()
    for pattern in args.ignore_patterns:
        ignored_files |= set(fnmatch.filter(input_files, pattern))
    input_files = [f for f in input_files if not f in ignored_files]
    
    print "Will add the following plots into the plotbook:"
    for file in input_files:
        print "  " + file
    
    # Use relative path w.r.t. the tex file 
    tex_dir = os.path.dirname(args.output_file)
    tex_dir = tex_dir if tex_dir else "."
    input_files = [os.path.relpath(f, tex_dir) for f in input_files]
    
    plotbook.plots = input_files
    
    #-----------------------------
    # Write the tex file and compile
    plotbook.beginDocument()
    plotbook.addAllPlots()
    plotbook.endDocument()
    plotbook.writeTexFile()
    if not args.no_compile:
        plotbook.compilePdf()
    
    #-----------------------------
    print "Done!!"

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    makePlotbook(args)

