#!/usr/bin/env python

import os, sys
import argparse
import fnmatch
from glob import glob

import datetime
_date_str = datetime.datetime.now().strftime("%Y%m%d")
_datetime_str = datetime.datetime.now().strftime("%d/%m/%Y %H:%M")

import getpass, socket
_user_host = "{0}@{1}".format(getpass.getuser(), socket.getfqdn())

from modules.general_utils import checkMakeDirs, flattenCommaSeparatedLists
from modules.Gallery import Gallery


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser("./makeWebGallery.py")
    
    parser.add_argument("--output-dir", "-o", type=str, default="/user/wschan/public_html/{}_ZLFV_gallery".format(_date_str), help="Output directory name.")
    
    parser.add_argument("--input-dir", "-i", default="./results", type=str, help="Input directory.")
    parser.add_argument("--input-pattern", "-p", dest='input_patterns', default=[], action='append', type=str, help="Input file name pattern(s). Use shell-style wildcards. Feel free to include subdirectory names in the pattern.")
    parser.add_argument("--ignore-pattern", dest='ignore_patterns', default=[], action='append', type=str, help="Input file name patterns to be ignored. Use shell-style wildcards.")
    
    parser.add_argument("--ntup-ver", type=str, help="Ntuple version.")
    parser.add_argument("--plot-ver", default="unknown", type=str, help="Plot version.")
    parser.add_argument("--extra-comment", "-m", dest="extra_comments", action='append', default=[], type=str, help="Extra comments that will be added to the README.")
    
    try:
        args = parser.parse_args(argv)
        
        args.input_patterns = flattenCommaSeparatedLists(args.input_patterns)
        args.ignore_patterns = flattenCommaSeparatedLists(args.ignore_patterns)
        
        checkArgs(args)
        
    except:
        parser.print_help()
        raise
    
    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if len(args.input_patterns) == 0:
        raise Exception("You haven't specified any input file patterns.")
    
    if not args.ntup_ver:
        raise Exception("Please specify the ntuple persion by '--ntup-ver <version>'.")
    
    checkMakeDirs(args.output_dir)

#-------------------------------------------------------------------------------
def makeWebGallery(args):
    gallery = Gallery(args.output_dir)
    
    gallery.metadata["ntup_ver"] = args.ntup_ver
    gallery.metadata["plot_ver"] = args.plot_ver
    gallery.metadata["created_at"] = _datetime_str
    gallery.metadata["created_by"] = _user_host
    gallery.metadata["extra_comments"] = args.extra_comments
    
    #-----------------------------
    # Get the list of plot file names
    input_files = []
    for pattern in args.input_patterns:
        files = glob(os.path.join(args.input_dir, pattern))
        files.sort()
        input_files += files
    
    ignored_files = set()
    for pattern in args.ignore_patterns:
        ignored_files |= set(fnmatch.filter(input_files, pattern))
    input_files = [f for f in input_files if not f in ignored_files]
    
    # Only take the .png files
    input_files = [f for f in input_files if f[-4:].lower() == ".png"]
    
    if len(input_files) == 0:
        raise Exception("No file matched the required pattern. (Make sure your targets are .png files.)")
    
    print "Will add the following plots into the web gallery:"
    for file in input_files:
        print "  " + file
    
    #-----------------------------
    gallery.addPlots(input_files)
    gallery.writeReadme()
    gallery.writeIndex()
    
    #-----------------------------
    print "Done!!"

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    makeWebGallery(args)

