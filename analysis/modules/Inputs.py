import os
from collections import OrderedDict
import yaml
from yaml_utils import listToDict
from general_utils import find

#-------------------------------------------------------------------------------
# Class that loads inputs.yaml into a set of InputGroup's
class Inputs(object):

    def __init__(self, file_name=None):
        if file_name is None:
            file_name = find(os.environ, "LFV_INPUTS", "configs/inputs.yaml")

        with open(file_name, "r") as f:
            file = yaml.load(f)

        if "input_dir" not in file:
            raise Exception("Cannot find input_dir in your {}".format(file_name))
        self.dir = file["input_dir"]

        # Guessing version from directory name
        version = self.dir.strip('/').split('/')[-1]
        if version.startswith('v'):
            try:
                v = float(version.strip('v'))
                self.version = version
            except:
                self.version = 'Unknown'

        self.data = None
        if "data" in file:
            self.data = self.parseGroup(file["data"], name="Data", is_data=True)

        self.signals = OrderedDict()
        if "signals" in file:
            self.signals = self.parseGroups(file["signals"], is_signal=True)

        self.bkgs = OrderedDict()
        if "backgrounds" in file:
            self.bkgs = self.parseGroups(file["backgrounds"])

        self.fakes = None
        if "fakes" in file:
            self.fakes = self.parseGroup(file["fakes"], name="fakes")

        self.inputs = {"data":self.data, "fakes":self.fakes}
        self.inputs.update(self.signals)
        self.inputs.update(self.bkgs)
        self.inputs = Inputs.sortByOrder(self.inputs)

        # Alternative background samples:
        # Copy the baseline dictionaries and update with alternative inputs
        # Swap the dictionaries when using alternatives
        self.__use_alt = False  # Leading underscores to deter user from directly changing it

        self.__bkgs_swap = OrderedDict(self.bkgs)  # copy
        self.__inputs_swap = OrderedDict(self.inputs)  # copy

        if "backgrounds_alt" in file:
            alt_bkgs = self.parseGroups(file["backgrounds_alt"])
            self.__bkgs_swap.update(alt_bkgs)
            self.__inputs_swap.update(alt_bkgs)

        print "Initialised input database: found {:d} MC input groups.".format(len(self.signals)+len(self.bkgs))

    def useAlt(self, use_alt=True):
        if use_alt ^ self.__use_alt:
            self.__use_alt = use_alt
            self.bkgs, self.__bkgs_swap = self.__bkgs_swap, self.bkgs
            self.inputs, self.__inputs_swap = self.__inputs_swap, self.inputs

    def get(self, name, alt=None):
        # Historical
        if alt is not None:
            self.useAlt(alt)
        return self.inputs[name]

    def delete(self, input):
        input = str(input)  # overload: str(InputGroup) = InputGroup.name
        if input == "data":
            self.data = None
        elif input in self.signals:
            del self.signals[input]
        elif input in self.bkgs:
            del self.bkgs[input]
            del self.__bkgs_swap[input]
        del self.inputs[input]
        del self.__inputs_swap[input]

    @property
    def lumi(self):
        if self.data is None or len(self.data) == 0:
            raise Exception("Cannot tell the luminosity since there's no data files.")
        return self.data.lumi

    # Make the object act like a dictionary itself
    def __getitem__(self, key):
        if key in self.inputs:
            return self.inputs[key]
        else:
            for input in self.inputs.itervalues():
                if input.run1_name == key or input.label == key:
                    return input
        raise KeyError("InputGroup '{0}' not defined!".format(key))

    def __iter__(self):
        return self.inputs.__iter__()

    def __contains__(self, key):
        return key in self.inputs

    def __len__(self):
        return len(self.inputs)

    def keys(self):
        return self.inputs.keys()

    def values(self):
        return self.inputs.values()

    def __repr__(self):
        return "<{}.{} object at {}>: {:d} signal InputGroups and {:d} background InputGroups ({} ; {})".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        len(self.signals),
        len(self.bkgs),
        ", ".join(s for s in self.signals),
        ", ".join(b for b in self.bkgs) )

    #-------------------------------------------------
    def parseGroups(self, l, is_data=False, is_signal=False):
        input_groups = {}
        for name, input_group in listToDict(l).iteritems():
            input_groups[name] = self.parseGroup(input_group, name, is_data, is_signal)
        return Inputs.sortByOrder(input_groups)

    def parseGroup(self, d, name, is_data=False, is_signal=False):
        label = find(d, 'label', "")
        root_label = find(d, 'root_label', "")
        order = find(d, 'order', 999)
        colour = find(d, 'colour', None)
        run1_name = find(d, 'run1_name', None)
        input_files = self.parseFiles(d['files'], is_data, is_signal)
        return InputGroup(input_files=input_files,
                          name=name,
                          label=label,
                          root_label=root_label,
                          order=order,
                          colour=colour,
                          run1_name=run1_name,
                          is_data=is_data,
                          is_signal=is_signal)

    def parseFiles(self, l, is_data=False, is_signal=False):
        input_files = {}
        for name, input_file in listToDict(l).iteritems():
            input_files[name] = self.parseFile(input_file, is_data, is_signal)
        return input_files

    def parseFile(self, d, is_data=False, is_signal=False):
        if not 'file' in d:
            hint = "(Hint: tree name = {})".format(d['tree']) if 'tree' in d else ""
            raise Exception("InputFile doesn't have a file name. {}".format(hint))
        if not 'tree' in d:
            raise Exception("InputFile {} doesn't have a tree name.".format(d['tree']))
        name = self.dir + d['file']
        lumi = find(d, 'lumi', None)
        friend_files = find(d, 'friend_files', [])
        friend_trees = find(d, 'friend_trees', [])
        return InputFile(name=name,
                         tree=d['tree'],
                         friend_files=friend_files,
                         friend_trees=friend_trees,
                         is_data=is_data,
                         is_signal=is_signal,
                         lumi=lumi)

    #-------------------------------------------------
    @staticmethod
    def sortByOrder(d):
        key = lambda x: 999 if x[1] is None else x[1].order
        return OrderedDict(sorted(d.items(), key=key))

#-------------------------------------------------------------------------------
# Class of group of InputFile's
class InputGroup(dict):

    def __init__(self, input_files, name, label="", root_label="", order=999, colour=None, run1_name=None, is_data=False, is_signal=False):

        super(InputGroup, self).__init__(input_files)  # dict of InputFile's

        self.name = name
        self.label = label
        if label and not root_label:
            self.root_label = label
        else:
            self.root_label = root_label

        self.order = -999 if is_data else order
        self.colour = colour

        self.run1_name = run1_name

        if is_data and not all([f.is_data for f in self.values()]):
            raise Exception("{} is flagged as data, but not all of its files are flagged as data.".format(self.name))
        self.is_data = is_data

        if is_signal and not all([f.is_signal for f in self.values()]):
            raise Exception("{} is flagged as signal, but not all of its files are flagged as signal.".format(self.name))
        self.is_signal = is_signal

    @property
    def lumi(self):
        if not self.is_data:
            raise Exception("Asking for luminosity of MC inputs.")
        return sum(f.lumi for f in self.values())

    @property
    def files(self):
        # This is historical, point it to self
        return self

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<{}.{} object at {}>: '{}' with {:d} input files ({})".format(
                self.__class__.__module__,
                self.__class__.__name__,
                hex(id(self)),
                self.name,
                len(self),
                ", ".join(self.keys()) )

#-------------------------------------------------------------------------------
# Class of input file
class InputFile(object):

    def __init__(self, name, tree, friend_files=[], friend_trees=[], is_data=False, is_signal=False, lumi=None):

        self.name = name
        self.tree = tree
        self.friend_files = friend_files
        self.friend_trees = friend_trees

        self.is_data = is_data
        self.is_signal = is_signal

        self.lumi = lumi if is_data else None

    @property
    def tree_template(self):
        return self.tree + "_{}"

    @property
    def nominal_tree(self):
        return self.tree + "_NOMINAL"

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<{}.{} object at {}>: {}/{}".format(
                self.__class__.__module__,
                self.__class__.__name__,
                hex(id(self)),
                self.name,
                self.tree )
