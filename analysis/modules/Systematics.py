import os
from fnmatch import fnmatch
from collections import OrderedDict

from yaml_utils import listToDict, ordered_load
from general_utils import find


#-------------------------------------------------------------------------------
# Class that loads systematics.yaml into a set of Syst's
class Systematics(OrderedDict):

    def __init__(self, file_name=None):
        super(Systematics, self).__init__()

        if file_name is None:
            file_name = find(os.environ, "LFV_SYSTEMATICS", "configs/systematics.yaml")

        with open(file_name, "r") as f:
            file = ordered_load(f)

        for syst_name in file:
            properties = file[syst_name]

            title = find(properties, 'title', None)

            if "effect" not in properties:
                raise Exception("Systematic {0} does not have an effect".format(syst_name))
            effect = properties["effect"]

            regions = find(properties, 'regions', [])
            samples = find(properties, 'samples', [])

            if not effect in ["Global", "Regions", "Samples", "RegionSamples"]:
                raise Exception("Systematic {0} has unknown systematic effect '{1}'".format(syst_name, effect))

            if "Region" in effect and not regions:
                raise Exception("Systematic {0} affect '{1}' but does not have affected regions specified".format(syst_name, effect))
            if "Sample" in effect and not samples:
                raise Exception("Systematic {0} affect '{1}' but does not have affected samples specified".format(syst_name, effect))

            if "type" not in properties:
                raise Exception("Systematic {0} does not have a type".format(syst_name))
            type = properties["type"]

            weights = listToDict(find(properties, 'weights', []))
            treesuffixes = listToDict(find(properties, 'treesuffixes', []))
            errors = listToDict(find(properties, 'errors', []))

            if not type in ["weight", "tree", "flat"]:
                raise Exception("Systematic {0} has unknown systematic type '{1}'".format(syst_name, type))

            if type == "weight" and not weights:
                raise Exception("Systematic {0} has type '{1}' but does not have affected weights specified".format(syst_name, type))
            if type == "tree" and not treesuffixes:
                raise Exception("Systematic {0} has type '{1}' but does not have affected tree suffixes specified".format(syst_name, type))
            if type == "flat" and not errors:
                raise Exception("Systematic {0} has type '{1}' but does not have flat errors specified".format(syst_name, type))

            syst = Syst(name=syst_name,
                        title=title,
                        effect=effect,
                        regions=regions,
                        samples=samples,
                        type=type,
                        weights=weights,
                        treesuffixes=treesuffixes,
                        errors=errors,
                       )
            self[syst_name] = syst

        print "Initialised systematic database: found {:d} systematics.".format(len(self))

    def get(self, syst_name):
        # This is historical, point it to __getitem__
        return self[syst_name]

    def exists(self, syst_name):
        # This is historical, point it to __contains__
        return syst_name in self

    @property
    def systs(self):
        # This is historical, point it to self
        return self

    def delete(self, syst):
        syst = str(syst)  # overload: str(Region) = Region.name
        if syst in self:
            del self[syst]

    def getList(self, region=None, sample=None, type=None):
        l = [s for s in self.values() if s.affects(region, sample)]
        if type is not None:
            l = [s for s in l if s.type == type]
        return l

    def __repr__(self):
        return "<{}.{} object at {}>: {:d} systematics ({})".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        len(self),
        ", ".join(self.keys()) )

#-------------------------------------------------------------------------------
# Class of systematic
# avoiding the class name "Systematic", which has already been used by HistFitter
class Syst(object):

    def __init__(self, name, effect, type, title=None, regions=[], samples=[], weights={}, treesuffixes={}, errors={}):

        self.name = name
        self.title = title
        self.effect = effect
        self.type = type

        self.regions = regions
        self.samples = samples

        self.weights = weights
        self.treesuffixes = treesuffixes
        self._errors = errors

    def affects(self, region=None, sample=None, type=None):
        if region is None and sample is None:
            raise Exception("Syst.affects(): Call with arguments 'region' and/or 'sample'.")

        if type is not None and not self.type == type:
            return False

        # Data is never affected by systematics
        if sample in ["data", "Data", "data15_16", "data17", "data18"]:
            return False

        # Special treatment for fakes - Must be sample-specific
        if sample == "fakes" and not "Sample" in self.effect:
            return False

        if region is not None and "Region" in self.effect:
            if not any([fnmatch(region, r) for r in self.regions]):
                return False

        if sample is not None and "Sample" in self.effect:
            if not any([fnmatch(sample, s) for s in self.samples]):
                return False

        return True

    def errors(self, region=None, sample=None):
        if region is None and sample is None:
            raise Exception("Syst.flat_errors(): Call with arguments 'region' and/or 'sample'.")

        errors = [0,0]
        for r_s,e in self._errors.iteritems():
            r = r_s.split(';')[0]
            s = r_s.split(';')[1]
            if (region is None or fnmatch(region, r)) and (sample is None or fnmatch(sample, s)):
                errors = e
                break

        return errors

    def up_weights(self, nom_weights):
        non_affected_weights = [w for w in nom_weights if w not in self.weights['nominal']]
        varied_weights = [self.weights['up'][i] for i,w in enumerate(self.weights['nominal']) if w in nom_weights]

        if len(varied_weights) == 0:
            print "WARNING: weight-type systematic {} has no effect on the given sample, which only has weights {}".format(self.name, ",".join(nom_weights))

        return non_affected_weights + varied_weights

    def down_weights(self, nom_weights):
        non_affected_weights = [w for w in nom_weights if w not in self.weights['nominal']]
        varied_weights = [self.weights['down'][i] for i,w in enumerate(self.weights['nominal']) if w in nom_weights]

        if len(varied_weights) == 0:
            print "WARNING: weight-type systematic {} has no effect on the given sample, which only has weights {}".format(self.name, ",".join(nom_weights))

        return non_affected_weights + varied_weights

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<{}.{} object at {}>: Systematic '{}'".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        self.name )
