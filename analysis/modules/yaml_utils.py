import yaml
from collections import OrderedDict

#-------------------------------------------------------------------------------
def listToDict(l):
    if l is None: return {}
    return { k: v for d in l for k, v in d.items() if l is not None }

#-------------------------------------------------------------------------------
def ordered_load(file):
    class OrderedLoader(yaml.SafeLoader):
        pass
    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return OrderedDict(loader.construct_pairs(node))
    OrderedLoader.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, construct_mapping)
    return yaml.load(file, OrderedLoader)
