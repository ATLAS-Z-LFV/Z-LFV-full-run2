import os
from copy import copy
from collections import OrderedDict

from yaml_utils import listToDict, ordered_load
from general_utils import find


#-------------------------------------------------------------------------------
# Class that loads regions.yaml into a set of Region's
class Regions(OrderedDict):

    def __init__(self, file_name=None):
        super(Regions, self).__init__()

        if file_name is None:
            file_name = find(os.environ, "LFV_REGIONS", "configs/regions.yaml")

        with open(file_name, "r") as f:
            file = ordered_load(f)

        for region_name in file:
            properties = file[region_name]

            if "title" not in properties:
                raise Exception("Region {0} does not have a title".format(region_name))

            title = properties['title']
            root_title = find(properties, 'root_title', title)

            type = find(properties, 'type', None)
            blind = find(properties, 'blind', True)
            signals = find(properties, 'signals', [])
            weights = find(properties, 'weights', [])
            truth_selection = find(properties, 'truth_selection', "")
            fakes_regions = find(properties, 'fakes_regions', [])
            background = find(properties, 'background', None)
            split_tree_region = find(properties, 'split_tree_region', region_name)

            if "cuts" not in properties:
                raise Exception("Region {0} does not have any cuts".format(region_name))

            cuts = Regions.parseCuts(properties['cuts'])

            region = Region(name=region_name,
                            title=title,
                            root_title=root_title,
                            type=type,
                            blind=blind,
                            signals=signals,
                            weights=weights,
                            truth_selection=truth_selection,
                            fakes_regions=fakes_regions,
                            background=background,
                            cuts=cuts,
                            split_tree_region=split_tree_region,
                           )
            self[region_name] = region

        print "Initialised region database: found {:d} regions.".format(len(self))

    def get(self, region_name):
        # This is historical, point it to __getitem__
        return self[region_name]

    def exists(self, region_name):
        # This is historical, point it to __contains__
        return region_name in self

    @property
    def regions(self):
        # This is historical, point it to self
        return self

    def delete(self, region):
        region = str(region)  # overload: str(Region) = Region.name
        if region in self:
            del self[region]

    def __repr__(self):
        return "<{}.{} object at {}>: {:d} regions ({})".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        len(self),
        ", ".join(self.keys()) )

    #-------------------------------------------------
    @staticmethod
    def parseCuts(cut_dict):
        cuts = OrderedDict()

        for key in cut_dict:
            properties = listToDict(cut_dict[key])

            name = find(properties, 'name', key)
            label = find(properties, 'label', name)

            if 'cut' not in properties:
                raise Exception("Cut '{}' does not have a cut string.".format(name))
            cut_string = properties['cut']

            add_weights = find(properties, 'add_weights', [])

            ignore_temporarily = find(properties, 'ignore_temporarily', False)

            cut = Cut(cut=cut_string,
                      name=name,
                      label=label,
                      add_weights=add_weights,
                      ignore_temporarily=ignore_temporarily)
            cuts[key] = cut

        return cuts

#-------------------------------------------------------------------------------
# Class of region
class Region(object):

    def __init__(self, name, title, root_title=None, type=None, blind=True, signals=[], weights=[], truth_selection="", fakes_regions=[], background=None, cuts=OrderedDict(), split_tree_region=None):

        self.name = name
        self.title = title
        self.root_title = root_title

        self.type = type
        self.blind = blind
        self.signals = signals
        self.truth_selection = truth_selection
        self.fakes_regions = fakes_regions

        self.background = background

        if split_tree_region is None:
            self.split_tree_region = name
        else:
            self.split_tree_region = split_tree_region

        if len(cuts) == 0:
            raise Exception("Region '{}' has no cuts.".format(name))
        self.cuts = cuts

        self.basic_weights = weights

    @property
    def selection(self):
        cuts = self.cuts.values()
        _selection = Cut.all(cuts)
        _selection.name = "{} selection".format(self.name)
        return _selection

    @property
    def temporary_selection(self):
        cuts = [c for c in self.cuts.values() if not c.ignore_temporarily]
        _temporary_selection = Cut.all(cuts)
        _temporary_selection.name = "{} temporary selection".format(self.name)
        return _temporary_selection

    @property
    def ignored_selection(self):
        cuts = [c for c in self.cuts.values() if c.ignore_temporarily]
        _ignored_selection = Cut.all(cuts)
        _ignored_selection.name = "{} temporarily ignored selection".format(self.name)
        return _ignored_selection

    @property
    def added_weights(self):
        return [w for c in self.cuts.values() for w in c.add_weights]

    @property
    def add_weights(self):
        return self.added_weights

    @property
    def weights(self):
        return self.basic_weights + self.add_weights

    def weight_string(self, lumi=138972., lumi_in_ipb=False):
        if lumi < 1000. and not lumi_in_ipb:
            #print "WARNING: Assuming the given luminosity {} is in fb-1".format(lumi)
            #print "         If you meant pb-1, please specify lumi_in_ipb=True"
            return self.weight_string(1000.*lumi)
        return "{}*normWeight*{}".format(lumi, "*".join(self.weights))

    def draw_string(self, truth_selection=True, lumi=138972., lumi_in_ipb=False):
        # So that we can conveniently do TTree.Draw("variable", Region.draw_string())
        if truth_selection:
            return "(({0})&&({2})) * ({1})".format(self.selection, self.weight_string(lumi), self.truth_selection)
        else:
            return "({0}) * ({1})".format(self.selection, self.weight_string(lumi))

    def fakes_weight_string(self, lumi=138972., lumi_in_ipb=False):
        # Data (lbNumber>0) gets 1 and MCs (lbNumber==0) gets minus the MC weights
        # Then multiply by FF_NOMINAL
        return "((lbNumber>0) - (lbNumber==0)*{})*FF_NOMINAL".format(self.weight_string(lumi, lumi_in_ipb))

    def fakes_draw_string(self, truth_selection=True, lumi=138972., lumi_in_ipb=False):
        if truth_selection:
            return "(({0})&&({2})) * ({1})".format(self.selection, self.fakes_weight_string, self.truth_selection)
        else:
            return "({0}) * ({1})".format(self.selection, self.fakes_weight_string)

    def deleteCut(self, cut):
        cut = str(cut)  # overload: str(Cut) = Cut.string
        # We might be given a key or a cut string
        # Delete if matched in either case
        for key, val in self.cuts.items():
            if cut == key or cut == val:
                del self.cuts[key]

    def replaceCut(self, old_cut, new_cut):
        old_cut = str(old_cut)  # overload: str(Cut) = Cut.string
        # We might be given a key or a cut string
        # Replace if matched in either case
        for key, val in self.cuts.items():
            if old_cut == key or old_cut == val:
                self.cuts[key] = Cut(new_cut)

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<{}.{} object at {}>: Region '{}'".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        self.name )

#-------------------------------------------------------------------------------
# Class of cut
class Cut(object):

    def __init__(self, cut, name="new_instance", label="", add_weights=[], ignore_temporarily=False):
        if isinstance(cut, Cut):
            # 'copy constructor'
            self.__init__(cut.string, cut.name, cut.label, cut.add_weights, cut.ignore_temporarily)
            return
        self.string = str(cut)
        self.name=name
        self.label=label
        self.add_weights=add_weights
        self.ignore_temporarily=ignore_temporarily

        # Try to reduce the string
        if self.is_one:
            self.string = "1"
        elif self.is_zero:
            self.string = "0"

    def __str__(self):
        return self.string

    def __repr__(self):
        return "<{}.{} object at {}>: Cut '{}'".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        self.name )

    #-------------------------------------------------
    # Functions that help us smartly reduce the string
    @property
    def is_one(self):
        s = self.string
        if s.lower().islower():
            return False  # Contains alphabets, too many possibilities
        try:
            s = s.replace("&&", " and ").replace("||", " or ")
            return eval(s, {})==1
        except NameError:
            return False
        return False

    @property
    def is_zero(self):
        s = self.string
        if s.lower().islower():
            return False  # Contains alphabets, too many possibilities
        try:
            s = s.replace("&&", " and ").replace("||", " or ")
            return eval(s, {})==0
        except NameError:
            return False
        return False

    #-------------------------------------------------
    # Operators overrides
    def __and__(self, cut):
        if isinstance(cut, str): cut = Cut(cut)
        if self.is_zero or cut.is_zero:
            return Cut("0")
        elif self.is_one:
            return cut
        elif cut.is_one:
            return self
        return Cut("({})&&({})".format(self.string, str(cut)))

    def __or__(self, cut):
        if isinstance(cut, str): cut = Cut(cut)
        if self.is_one or cut.is_one:
            return Cut("1")
        elif self.is_zero:
            return cut
        elif cut.is_zero:
            return self
        return Cut("({})||({})".format(self.string, str(cut)))

    def __add__(self, cut):
        return self.__and__(cut)

    def __mul__(self, cut):
        if isinstance(cut, str): cut = Cut(cut)
        if self.is_zero or cut.is_zero:
            return Cut("0")
        elif self.is_one:
            return cut
        elif cut.is_one:
            return self
        return Cut("({})*({})".format(self.string, str(cut)))

    def __sub__(self, cut):
        ### (A - B) can have two sensible meanings here

        # # Case 1: A is a set of cuts and B is one of the cuts in A
        # # Assume the user wants to remove B from A
        # if str(cut) in self.string:
        #     return Cut(self.string.replace(str(cut), "1"))
        ### This is toooo dangerous! What was I thinking?!

        # Case 2: A and B are two distinct (sets of) cuts
        # Assume the user wants A but not B
        if isinstance(cut, str): cut = Cut(cut)
        return self.__and__(cut.negation())

    # Oops, there is no __not__() method for object instances in Python
    #def __not__(self):
    def negation(self):
        if self.is_zero:
            return Cut("1")
        if self.is_one:
            return Cut("0")
        return Cut("!({})".format(self.string))

    def __eq__(self, cut):
        if isinstance(cut, str): cut = Cut(cut)
        if self.is_zero and cut.is_zero:
            return True
        if self.is_one and cut.is_one:
            return True
        return self.string == cut.string  # simple string comparison (good enough?)

    #-------------------------------------------------
    # Static methods
    @staticmethod
    def all(cuts):
        if not hasattr(cuts, '__iter__'):
            return Cut(cuts)
        if len(cuts) == 0:
            return Cut("1")
        all_cuts = cuts[0]
        for cut in cuts[1:]:
            all_cuts &= cut
        return all_cuts

    @staticmethod
    def any(cuts):
        if not hasattr(cuts, '__iter__'):
            return Cut(cuts)
        if len(cuts) == 0:
            return Cut("1")
        any_cuts = cuts[0]
        for cut in cuts[1:]:
            any_cuts |= cut
        return any_cuts
