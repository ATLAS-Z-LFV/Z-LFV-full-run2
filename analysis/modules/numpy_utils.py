import numpy as np

#-------------------------------------------------------------------------------
# Function that changes objects in a (structured) array to another type
# Note: root_numpy.tree2array returns vector elements like 'muons_pt[0]' as objects
def objectAsType(arr, type='<f8'):
    if not arr.dtype.hasobject:
        return arr

    if arr.dtype.fields is None:
        # Not a structured array
        return arr.astype(type)

    # Is a structured array
    formats = []
    for dt in arr.dtype.fields.values():
        if dt[0].hasobject:
            formats.append(type)
        else:
            formats.append(str(dt[0]))
    dtype = np.dtype({'names': arr.dtype.names, 'formats': formats})

    return arr.astype(dtype)
