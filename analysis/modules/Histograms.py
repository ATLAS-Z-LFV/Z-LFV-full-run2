import os
import re
import yaml
from general_utils import find
from plot_utils import friendlyBranchName

#-------------------------------------------------------------------------------
# Class that loads histograms.yaml into a set of Variable's
class Histograms(dict):

    def __init__(self, file_name=None):
        if file_name is None:
            file_name = find(os.environ, "LFV_HISTOGRAMS", "configs/histograms.yaml")

        with open(file_name, "r") as f:
            file = yaml.load(f)

        for hist_name in file:
            properties = file[hist_name]

            if hist_name in self:
                raise Exception("Duplicated definition for '{0}'".format(hist_name))

            if not "branch" in properties:
                raise Exception("No branch specified for '{0}'".format(hist_name))

            # Split branch by ':'
            # but we have to avoid treating '::' as delimiter, therefore we use RE
            branches = re.split("(?<!:):(?!:)", properties["branch"])
            if len(branches) == 1:
                is_2D = False
            elif len(branches) == 2:
                is_2D = True
            else:
                raise Exception("The dimension of '{0}' is too high. I only know how to make 1D and 2D histograms!".format(hist_name))

            logscale = find(properties, 'logscale', False)

            NN = find(properties, 'NN', False)

            ad_hoc = find(properties, 'adhoc', False)

            if not is_2D:
                if not "min" in properties:
                    raise Exception("No minimum range specified for '{0}'".format(hist_name))
                if not "max" in properties:
                    raise Exception("No maximum range specified for '{0}'".format(hist_name))

                if "nBins" in properties and "binSize" in properties:
                    raise Exception("Specify either nBins or binSize, not both, for '{0}'".format(hist_name))
                if "nBins" in properties and "binSize" not in properties:
                    nBins = properties["nBins"]
                    binSize = None
                elif "nBins" not in properties and "binSize" in properties:
                    nBins = None
                    binSize = properties["binSize"]
                else:
                    raise Exception("No nBins or binSize specified for '{0}'".format(hist_name))

                label = find(properties, 'label', "")
                root_label = find(properties, 'root_label', "")
                unit = find(properties, 'unit', "")
                merge_overflow = find(properties, 'merge_overflow', True)
                merge_underflow = find(properties, 'merge_underflow', True)

                hist = Variable(hist_name,
                                properties["branch"],
                                labelX=label,
                                root_labelX=root_label,
                                minX=properties["min"],
                                maxX=properties["max"],
                                nBinsX=nBins,
                                binSizeX=binSize,
                                unitX=unit,
                                merge_overflowX=merge_overflow,
                                merge_underflowX=merge_underflow,
                                logscale=logscale,
                                NN=NN,
                                is_2D=False,
                                ad_hoc=ad_hoc,
                               )
                self[hist_name] = hist

            else:
                # x-axis
                if not "minX" in properties:
                    raise Exception("No minimum range (x-axis) specified for '{0}'".format(hist_name))
                if not "maxX" in properties:
                    raise Exception("No maximum range (x-axis) specified for '{0}'".format(hist_name))

                if "nBinsX" in properties and "binSizeX" in properties:
                    raise Exception("Specify either nBinsX or binSizeX, not both, for '{0}'".format(hist_name))
                if "nBinsX" in properties and "binSizeX" not in properties:
                    nBinsX = properties["nBinsX"]
                    binSizeX = None
                elif "nBinsX" not in properties and "binSizeX" in properties:
                    nBinsX = None
                    binSizeX = properties["binSizeX"]
                else:
                    raise Exception("No nBinsX or binSizeX specified for '{0}'".format(hist_name))

                labelX = find(properties, 'labelX', "")
                root_labelX = find(properties, 'root_labelX', "")
                unitX = find(properties, 'unitX', "")
                merge_overflowX = find(properties, 'merge_overflowX', False)
                merge_underflowX = find(properties, 'merge_underflowX', False)

                # y-axis
                if not "minY" in properties:
                    raise Exception("No minimum range (y-axis) specified for '{0}'".format(hist_name))
                if not "maxY" in properties:
                    raise Exception("No maximum range (y-axis) specified for '{0}'".format(hist_name))

                if "nBinsY" in properties and "binSizeY" in properties:
                    raise Exception("Specify either nBinsY or binSizeY, not both, for '{0}'".format(hist_name))
                if "nBinsY" in properties and "binSizeY" not in properties:
                    nBinsY = properties["nBinsY"]
                    binSizeY = None
                elif "nBinsY" not in properties and "binSizeY" in properties:
                    nBinsY = None
                    binSizeY = properties["binSizeY"]
                else:
                    raise Exception("No nBinsY or binSizeY specified for '{0}'".format(hist_name))

                labelY = find(properties, 'labelY', "")
                root_labelY = find(properties, 'root_labelY', "")
                unitY = find(properties, 'unitY', "")
                merge_overflowY = find(properties, 'merge_overflowY', False)
                merge_underflowY = find(properties, 'merge_underflowY', False)

                hist = Variable(hist_name,
                                properties["branch"],
                                labelX=labelX,
                                labelY=labelY,
                                root_labelX=root_labelX,
                                root_labelY=root_labelY,
                                minX=properties["minX"],
                                minY=properties["minY"],
                                maxX=properties["maxX"],
                                maxY=properties["maxY"],
                                nBinsX=nBinsX,
                                nBinsY=nBinsY,
                                binSizeX=binSizeX,
                                binSizeY=binSizeY,
                                unitX=unitX,
                                unitY=unitY,
                                merge_overflowX=merge_overflowX,
                                merge_underflowX=merge_underflowX,
                                merge_overflowY=merge_overflowY,
                                merge_underflowY=merge_underflowY,
                                logscale=logscale,
                                NN=NN,
                                is_2D=True,
                                ad_hoc=ad_hoc,
                               )
                self[hist_name] = hist

        print "Initialised histogram database: found {:d} histograms.".format(len(self))

    def get(self, hist_name):
        # This is historical, point it to __getitem__
        return self[hist_name]

    @property
    def histograms(self):
        # This is historical, point it to self
        return self

    def __repr__(self):
        return "<{}.{} object at {}>: {:d} histograms ({})".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        len(self),
        ", ".join(h.branch for h in self.values()) )

#-------------------------------------------------------------------------------
# Class of 1D/2D histogram(variable)
class Variable(object):

    def __init__(self, name, branch, labelX="", labelY="", root_labelX="", root_labelY="", minX=0, minY=1, maxX=0, maxY=1, nBinsX=None, nBinsY=None, binSizeX=None, binSizeY=None, unitX="", unitY="", merge_overflowX=True, merge_underflowX=True, merge_overflowY=True, merge_underflowY=True, logscale=False, NN=False, is_2D=False, ad_hoc=False):

        self.name = name
        self.branch = branch

        self.labelX = labelX
        if labelX and not root_labelX:
            self.root_labelX = labelX
        else:
            self.root_labelX = root_labelX

        if nBinsX is None and binSizeX is None:
            raise Exception("Specify one (and only one) of either nBinsX or binSizeX for {}!".format(name))
        self._nBinsX = nBinsX
        self._binSizeX = binSizeX
        self._edgesX = None

        self.minX = minX
        self.maxX = maxX

        self.unitX = unitX

        self.merge_overflowX = merge_overflowX
        self.merge_underflowX = merge_underflowX

        self.logscale = logscale

        self.is_NN_output = NN

        self.ad_hoc = ad_hoc

        self.is_2D = is_2D
        if is_2D:
            self.labelY = labelY
            if labelY and not root_labelY:
                self.root_labelY = labelY
            else:
                self.root_labelY = root_labelY

            if nBinsY is None and binSizeY is None:
                raise Exception("Specify one (and only one) of either nBinsY or binSizeY for {}!".format(name))
            self._nBinsY = nBinsY
            self._binSizeY = binSizeY
            self._edgesY = None

            self.minY = minY
            self.maxY = maxY

            self.unitY = unitY

            self.merge_overflowY = merge_overflowY
            self.merge_underflowY = merge_underflowY

    @property
    def nBinsX(self):
        if self._nBinsX is None:
            self._nBinsX = int(round((self.maxX-self.minX) / float(self._binSizeX)))
        return self._nBinsX

    @property
    def binSizeX(self):
        if self._binSizeX is None:
            self._binSizeX = (self.maxX-self.minX) / float(self._nBinsX)
        return self._binSizeX

    @property
    def edgesX(self):
        if self._edgesX is None:
            self._edgesX = np.linspace(self.minX, self.maxX, self.nBinsX+1)
        return self._edgesX

    @property
    def nBinsY(self):
        if not self.is_2D:
            raise Exception("Asking for y-axis binning info for a 1D histogram? Go home, you are drunk.")
        if self._nBinsY is None:
            self._nBinsY = int(round((self.maxY-self.minY) / float(self._binSizeY)))
        return self._nBinsY

    @property
    def binSizeY(self):
        if not self.is_2D:
            raise Exception("Asking for y-axis binning info for a 1D histogram? Go home, you are drunk.")
        if self._binSizeY is None:
            self._binSizeY = (self.maxY-self.minY) / float(self._nBinsY)
        return self._binSizeY

    @property
    def edgesY(self):
        if not self.is_2D:
            raise Exception("Asking for y-axis binning info for a 1D histogram? Go home, you are drunk.")
        if self._edgesY is None:
            self._edgesY = np.linspace(self.minY, self.maxY, self.nBinsY+1)
        return self._edgesY

    @property
    def label(self): return self.labelX

    @property
    def root_label(self): return self.root_labelX

    @property
    def nBins(self): return self.nBinsX

    @property
    def binSize(self): return self.binSizeX

    @property
    def edges(self): return self.edgesX

    @property
    def min(self): return self.minX

    @property
    def max(self): return self.maxX

    @property
    def unit(self): return self.unitX

    @property
    def merge_overflow(self): return self.merge_overflowX

    @property
    def merge_underflow(self): return self.merge_underflowX

    @property
    def friendlyName(self): return friendlyBranchName(self.branch)

    def match_name(self, name):
        if self.ad_hoc:
            return False
        return self.friendlyName == name

    def __str__(self):
        return self.branch

    def __repr__(self):
        return "<{}.{} object at {}>: Histogram '{}' = '{}'".format(
        self.__class__.__module__,
        self.__class__.__name__,
        hex(id(self)),
        self.name,
        self.branch )
