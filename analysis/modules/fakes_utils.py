from math import sqrt
from array import array
from copy import copy
from collections import OrderedDict

from Regions import Cut
from general_utils import root_open, rootVerbosityLevel
from general_utils import acquire
from plot_utils import getMaxBinIndex, forceHistNonNegative, mergeOverflows, findHistValue

from ROOT import gROOT
from ROOT import TH1D, TH2D


#-------------------------------------------------------------------------------
def failRegion(region, bdt_id=False):
    # expect region to be a modules.Regions object
    if "tau_multiplicities" not in region.cuts:
        raise Exception("There's no tau ID cut to invert in {}".format(region.name))

    fail_region = copy(region)
    cut = region.cuts["tau_multiplicities"]
    cut.name = "Loose-but-not-tight-ID Leading-pT tau"
    cut.label = 'Loose-but-not-tight-ID leading-$p_\mathrm{T}$ $\tau_\mathrm{had\mathchar"20 vis}$'
    if bdt_id:
        cut.string = "taus_signal[0] == 0 && taus_jet_bdt_loose[0] == 1"
    else:
        cut.string = "taus_signal[0] == 0 && taus_jet_rnn_loose[0] == 1"

    return fail_region

#-------------------------------------------------------------------------------
def initialiseHist(name, binnings):
    gROOT.cd()  # TTree.Project() finds histograms here

    obj = gROOT.FindObject(name)
    if obj:
        # Prevent memory leaks
        print "WARNING: Already exists an object with the name '{}'.".format(name)
        print "         Will delete this object before creating histogram of the same name."
        obj.Delete()

    # 1D
    if len(binnings) == 1 and hasattr(binnings[0], '__iter__'):
        h = TH1D(name, name, len(binnings[0])-1, array('d', binnings[0]))
    # 2D
    elif len(binnings) == 2:
        h = TH2D(name, name, len(binnings[0])-1, array('d', binnings[0]),
                             len(binnings[1])-1, array('d', binnings[1]))
    # Not implemented
    else:
        raise NotImplementedError("Cannot initiate histogram with binnings {}".format(binnings))

    h.Sumw2()
    return h

#-------------------------------------------------------------------------------
def keepSumInRange(hists, max=1):
    sum = hists[0].Clone()
    for h in hists[1:]:
        sum.Add(h)

    max_idx = getMaxBinIndex(sum)
    for i in xrange(max_idx+1):
        x = sum.GetBinContent(i)
        if x < max:
            continue
        scale = 1./x
        for h in hists:
            h.SetBinContent(i, h.GetBinContent(i) * scale)
            # Keep error size

    sum.Delete()

#-------------------------------------------------------------------------------
def getFFactorComponents(FR_name, extra_cut, branch, binnings, cuts_pass, cuts_fail, truth_sel, weights, data, target_bkgs, other_bkgs, NN_tag=None, lock=None):

    h = OrderedDict()  # dict to be returned

    if not isinstance(cuts_pass, Cut):
        cuts_pass = Cut(cuts_pass)
    if not isinstance(cuts_fail, Cut):
        cuts_fail = Cut(cuts_fail)

    # We need 8 histograms, all with the same binning
    # First 1-3,5,6 are for computing F-factors
    # Last 3,4,7,8 are for computing k-factors
    h["pass_data"]          = initialiseHist(FR_name+"_pass_data", binnings)
    h["pass_otherMC"]       = initialiseHist(FR_name+"_pass_otherMC", binnings)
    h["pass_real_targetMC"] = initialiseHist(FR_name+"_pass_real_targetMC", binnings)
    h["pass_fake_targetMC"] = initialiseHist(FR_name+"_pass_fake_targetMC", binnings)
    h["fail_data"]          = initialiseHist(FR_name+"_fail_data", binnings)
    h["fail_otherMC"]       = initialiseHist(FR_name+"_fail_otherMC", binnings)
    h["fail_real_targetMC"] = initialiseHist(FR_name+"_fail_real_targetMC", binnings)
    h["fail_fake_targetMC"] = initialiseHist(FR_name+"_fail_fake_targetMC", binnings)

    # Histograms for individual processes in 'otherMC'.
    it = (f for b in other_bkgs for f in b.files.values())
    for f in it:
        h["pass_real_"+f.tree] = initialiseHist(FR_name+"_pass_real_"+f.tree, binnings)
        h["pass_fake_"+f.tree] = initialiseHist(FR_name+"_pass_fake_"+f.tree, binnings)
        h["fail_real_"+f.tree] = initialiseHist(FR_name+"_fail_real_"+f.tree, binnings)
        h["fail_fake_"+f.tree] = initialiseHist(FR_name+"_fail_fake_"+f.tree, binnings)

    with acquire(lock):
        if lock: print "="*30
        print "  '{}': filling data\n".format(FR_name)
    for f in data.files.values():
        with rootVerbosityLevel("Error"):
            with root_open(f.name) as file:
                gROOT.cd()  # TTree.Project() finds histograms here
                tree = file.Get(f.tree + "_NOMINAL")
                if NN_tag:
                    NN_file = "{}.NN.{}.friend".format(f.name, NN_tag)
                    NN_tree = "NN_{}_NOMINAL".format(f.tree)
                    tree.AddFriend(NN_tree, NN_file)

                hist_name = "+{}_pass_data".format(FR_name)
                draw_string = str(cuts_pass&extra_cut)
                tree.Project(hist_name, branch, draw_string)

                hist_name = "+{}_fail_data".format(FR_name)
                draw_string = str(cuts_fail&extra_cut)
                tree.Project(hist_name, branch, draw_string)

    with acquire(lock):
        if lock: print "="*30
        print "  '{}': filling other MCs\n".format(FR_name)
    it = (f for b in other_bkgs for f in b.files.values())
    for f in it:
        with root_open(f.name) as file:
            gROOT.cd()  # TTree.Project() finds histograms here
            tree = file.Get(f.tree + "_NOMINAL")
            w = file.Get("weights")
            tree.AddFriend(w)
            if NN_tag:
                NN_file = "{}.NN.{}.friend".format(f.name, NN_tag)
                NN_tree = "NN_{}_NOMINAL".format(f.tree)
                tree.AddFriend(NN_tree, NN_file)

            #hist_name = "+{}_pass_otherMC".format(FR_name)
            #draw_string = "({}) * ({})".format(cuts_pass&extra_cut, weights)
            #tree.Project(hist_name, branch, draw_string)

            #hist_name = "+{}_fail_otherMC".format(FR_name)
            #draw_string = "({}) * ({})".format(cuts_fail&extra_cut, weights)
            #tree.Project(hist_name, branch, draw_string)

            hist_name = FR_name+"_pass_real_"+f.tree
            draw_string = "({}) * ({})".format(cuts_pass&extra_cut&truth_sel, weights)
            tree.Project(hist_name, branch, draw_string)
            h["pass_otherMC"].Add(h["pass_real_"+f.tree])

            hist_name = FR_name+"_pass_fake_"+f.tree
            draw_string = "({}) * ({})".format((cuts_pass&extra_cut)-truth_sel, weights)
            tree.Project(hist_name, branch, draw_string)
            h["pass_otherMC"].Add(h["pass_fake_"+f.tree])

            hist_name = FR_name+"_fail_real_"+f.tree
            draw_string = "({}) * ({})".format(cuts_fail&extra_cut&truth_sel, weights)
            tree.Project(hist_name, branch, draw_string)
            h["fail_otherMC"].Add(h["fail_real_"+f.tree])

            hist_name = FR_name+"_fail_fake_"+f.tree
            draw_string = "({}) * ({})".format((cuts_fail&extra_cut)-truth_sel, weights)
            tree.Project(hist_name, branch, draw_string)
            h["fail_otherMC"].Add(h["fail_fake_"+f.tree])

    if len(target_bkgs) == 0:
        # This must be the QCD background
        # leave the 'target MC' histograms empty
        pass
    else:
        with acquire(lock):
            if lock: print "="*30
            print "  '{}': filling target MC(s)\n".format(FR_name)
        it = (f for b in target_bkgs for f in b.files.values())
        for f in it:
            with root_open(f.name) as file:
                gROOT.cd()  # TTree.Project() finds histograms here
                tree = file.Get(f.tree + "_NOMINAL")
                w = file.Get("weights")
                tree.AddFriend(w)
                if NN_tag:
                    NN_file = "{}.NN.{}.friend".format(f.name, NN_tag)
                    NN_tree = "NN_{}_NOMINAL".format(f.tree)
                    tree.AddFriend(NN_tree, NN_file)

                hist_name = "+{}_pass_real_targetMC".format(FR_name)
                draw_string = "({}) * ({})".format(cuts_pass&extra_cut&truth_sel, weights)
                tree.Project(hist_name, branch, draw_string)

                hist_name = "+{}_pass_fake_targetMC".format(FR_name)
                draw_string = "({}) * ({})".format((cuts_pass&extra_cut)-truth_sel, weights)
                tree.Project(hist_name, branch, draw_string)

                hist_name = "+{}_fail_real_targetMC".format(FR_name)
                draw_string = "({}) * ({})".format(cuts_fail&extra_cut&truth_sel, weights)
                tree.Project(hist_name, branch, draw_string)

                hist_name = "+{}_fail_fake_targetMC".format(FR_name)
                draw_string = "({}) * ({})".format((cuts_fail&extra_cut)-truth_sel, weights)
                tree.Project(hist_name, branch, draw_string)

    mergeOverflows(h)

    with acquire(lock):
        if lock: print "="*30
        print "  '{}': F-/k-factors components:".format(FR_name)
        for k in h:
            print "      Total number of events in {:19s}: {}".format(k, h[k].Integral())
        if lock: print ""

    return h

#-------------------------------------------------------------------------------
def getFFactorComponentsFromCache(cache_file_name, FR, decay_mode):

    h = OrderedDict()  # ordered dict to be returned
    prefix = "{}_{}_".format(FR, decay_mode)

    with root_open(cache_file_name) as cache:
        keys = [key for key in cache.GetListOfKeys() if key.GetName().startswith(prefix)]
        for key in keys:
            hist = key.ReadObj()
            hist.SetDirectory(0)  # persists after closing the file
            h[key.GetName().replace(prefix, "", 1)] = hist

    return h

#-------------------------------------------------------------------------------
def getFFactors(hists, name=""):
    denominator = hists["fail_data"].Clone()
    denominator.Add(hists["fail_otherMC"], -1)
    denominator.Add(hists["fail_real_targetMC"], -1)
    forceHistNonNegative(denominator)

    F = hists["pass_data"].Clone(name)
    F.SetTitle(name)
    F.Add(hists["pass_otherMC"], -1)
    F.Add(hists["pass_real_targetMC"], -1)
    forceHistNonNegative(F)
    F.Divide(denominator)

    denominator.Delete()

    hists[name] = F
    return F

#-------------------------------------------------------------------------------
def getkFactors(hists, name="", category="fail"):
    k = hists["{}_data".format(category)].Clone(name)
    k.SetTitle(name)
    k.Add(hists["{}_otherMC".format(category)], -1)
    k.Add(hists["{}_real_targetMC".format(category)], -1)
    forceHistNonNegative(k)
    k.Divide(hists["{}_fake_targetMC".format(category)])

    hists[name] = k
    return k

#-------------------------------------------------------------------------------
def getRFactorComponents(region_name, extra_cut, branch, binnings, cuts_fail, truth_sel, weights, data, fakes_bkgs, all_bkgs, NN_tag=None, lock=None):

    h = OrderedDict()  # dict to be returned

    if not isinstance(cuts_fail, Cut):
        cuts_fail = Cut(cuts_fail)

    # We need 2+(nFakesBkgs) histograms, each with the same binning
    h["fail_data"]        = initialiseHist(region_name+"_fail_data", binnings)
    h["fail_real_allMCs"] = initialiseHist(region_name+"_fail_real_allMCs", binnings)
    for b in fakes_bkgs:
        key = "fail_fake_" + b.name
        h[key] = initialiseHist("{}_{}".format(region_name, key), binnings)

    with acquire(lock):
        if lock: print "="*30
        print "  '{}': filling data\n".format(region_name)
    for f in data.files.values():
        with rootVerbosityLevel("Error"):
            with root_open(f.name) as file:
                gROOT.cd()  # TTree.Project() finds histograms here
                tree = file.Get(f.tree + "_NOMINAL")
                if NN_tag:
                    NN_file = "{}.NN.{}.friend".format(f.name, NN_tag)
                    NN_tree = "NN_{}_NOMINAL".format(f.tree)
                    tree.AddFriend(NN_tree, NN_file)

                hist_name = "+{}_fail_data".format(region_name)
                draw_string = str(cuts_fail&extra_cut)
                tree.Project(hist_name, branch, draw_string)

    with acquire(lock):
        if lock: print "="*30
        print "  '{}': filling all MCs\n".format(region_name)
    it = (f for b in all_bkgs for f in b.files.values())
    for f in it:
        with root_open(f.name) as file:
            gROOT.cd()  # TTree.Project() finds histograms here
            tree = file.Get(f.tree + "_NOMINAL")
            w = file.Get("weights")
            tree.AddFriend(w)
            if NN_tag:
                NN_file = "{}.NN.{}.friend".format(f.name, NN_tag)
                NN_tree = "NN_{}_NOMINAL".format(f.tree)
                tree.AddFriend(NN_tree, NN_file)

            hist_name = "+{}_fail_real_allMCs".format(region_name)
            draw_string = "({}) * ({})".format(cuts_fail&extra_cut&truth_sel, weights)
            tree.Project(hist_name, branch, draw_string)

    for b in fakes_bkgs:
        with acquire(lock):
            if lock: print "="*30
            print "  '{}': filling target MC '{}'\n".format(region_name, b.name)
        for f in b.files.values():
            with root_open(f.name) as file:
                gROOT.cd()  # TTree.Project() finds histograms here
                tree = file.Get(f.tree + "_NOMINAL")
                w = file.Get("weights")
                tree.AddFriend(w)
                if NN_tag:
                    NN_file = "{}.NN.{}.friend".format(f.name, NN_tag)
                    NN_tree = "NN_{}_NOMINAL".format(f.tree)
                    tree.AddFriend(NN_tree, NN_file)

                hist_name = "+{}_fail_fake_{}".format(region_name, b.name)
                draw_string = "({}) * ({})".format((cuts_fail&extra_cut)-truth_sel, weights)
                tree.Project(hist_name, branch, draw_string)

    mergeOverflows(h)

    with acquire(lock):
        if lock: print "="*30
        print "  '{}': R-factor components:".format(region_name)
        for k in h:
            print "      Total number of events in {:17s}: {}".format(k, h[k].Integral())
        if lock: print ""

    return h

#-------------------------------------------------------------------------------
def getRFactors(hists, bkg, name=""):
    denominator = hists["fail_data"].Clone()
    denominator.Add(hists["fail_real_allMCs"], -1)
    forceHistNonNegative(denominator)

    R = hists["fail_fake_"+bkg].Clone(name)
    R.SetTitle(name)
    R.Divide(denominator)

    denominator.Delete()

    hists[name] = R
    return R

#-------------------------------------------------------------------------------
def getRFactorsQCD(hists, name=""):
    R_QCD = hists[0].Clone(name)
    R_QCD.SetTitle(name)

    max_idx = getMaxBinIndex(R_QCD)
    for i in xrange(max_idx+1):
        R_QCD.SetBinContent(i, 1)
        R_QCD.SetBinError(i, 0)

    for h in hists:
        R_QCD.Add(h, -1)

    return R_QCD

#-------------------------------------------------------------------------------
def keepRFactorsInRange(hists):
    if isinstance(hists, dict):
        R = [hists[k] for k in hists if k.startswith("R_")]
    else:
        R = hists
    forceHistNonNegative(R)
    keepSumInRange(R)

#-------------------------------------------------------------------------------
def getRVariation(hists, scale, background="Wjets"):
    varied = {}
    for key in hists:
        varied[key] = hists[key].Clone()

    bkg = varied[background]

    max_idx = getMaxBinIndex(bkg)
    # Scale bkg, and in/decrease others accordingly to keep sum = 1
    # But we cap the change such that others cannot be changed by
    # more than one half or double of the original value
    for i in xrange(max_idx+1):
        b = bkg.GetBinContent(i)
        c = 1 - b
        if b == 0 or c == 0:
            # It is probably a very stat-limited bin
            # doesn't really make sense to vary the R factor...
            continue
        c_scale = (1 - b*scale) / c
        if c_scale > 2:
            c_scale = 2
        elif c_scale < 0.5:
            c_scale = 0.5
        b = 1 - c*c_scale
        bkg.SetBinContent(i, b)
        others = (varied[key] for key in varied if key!=background)
        for h in others:
            h.SetBinContent(i, c_scale*h.GetBinContent(i))

    return varied

#-------------------------------------------------------------------------------
def getFinalFFactor(F, R):
    if not set(F.keys()) == set(R.keys()):
        raise Exception("F-factors and R-factors have different backgrounds.")
    FF = F.values()[0].Clone()
    FF.Reset()
    for key in F:
        temp = F[key].Clone()
        temp.Multiply(R[key])
        FF.Add(temp)
        temp.Delete()
    return FF

#-------------------------------------------------------------------------------
# Moved codes to plot_utils
def findFF(FFs, pt, trk_pt=0):
    return findHistValue(FFs, pt, trk_pt)
