import os

from general_utils import execute

#-------------------------------------------------------------------------------
class LatexFile(object):
    def __init__(self, file_name):
        if not os.path.splitext(file_name)[1] == ".tex":
            file_name += ".tex"
        
        self.file_name = file_name
        self.lines = []
        self.packages = []
        self.preambles = []
    
    #-----------------------------
    def addLine(self, line=""):
        self.lines.append(line)
    
    #-----------------------------
    def addPreamble(self, line=""):
        self.preambles.append(line)
    
    #-----------------------------
    def addPackage(self, package, option=""):
        self.packages.append((package, option))
    
    #-----------------------------
    def beginDocument(self):
        self.addLine(r"\documentclass[]{article}")
        for pkg in self.packages:
            if pkg[1]:
                self.addLine(r"\usepackage[%s]{%s}" % (pkg[1], pkg[0]))
            else:
                self.addLine(r"\usepackage{%s}" % pkg[0])
        self.addLine()
        for preamble in self.preambles:
            self.addLine(preamble)
        self.addLine()
        self.addLine(r"\begin{document}")
        self.addLine()
    
    #-----------------------------
    def endDocument(self):
        self.addLine()
        self.addLine(r"\end{document}")
    
    #-----------------------------
    def clearPage(self):
        if not self.lines[-1] == r"\clearpage":
            self.addLine(r"\clearpage")
    
    #-----------------------------
    def writeTexFile(self):
        with open(self.file_name, "w") as f:
            for l in self.lines:
                f.write(l + "\n")
    
    #-----------------------------
    def compilePdf(self):
        dir, tex = os.path.split(self.file_name)
        if dir == "":
            execute("pdflatex -interaction=batchmode {}".format(tex))
        else:
            current_dir = os.getcwd()
            os.chdir(dir)
            execute("pdflatex -interaction=batchmode {}".format(tex))
            os.chdir(current_dir)
