import numpy as np
from numpy import square, sqrt, reciprocal, sign
from numpy import sin, cos, sinh, cosh, arccos, arctan, arcsinh
from numpy import pi


#-------------------------------------------------------------------------------
def safeDivide(x, y, fail_return=None, sign=True):
    try:
        return x/y
    except:
        if fail_return is not None:
            if sign and not x == 0:
                return x/abs(x) * zero_division_return
            return zero_division_return
        else:  # Default
            if y == 0 and not x == 0:
                return x/abs(x) * float('inf')
            return float('nan')

#-------------------------------------------------------------------------------
# Mimic ROOT's TVector3 and TLorentzVector
# Things should work even if x,y,z,t are numpy arrays!
class Vector(object):
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z
    
    @property
    def pt(self):
        return sqrt(square(self.x) + square(self.y))
    
    @property
    def eta(self):
        if np.count_nonzero(self.pt) == np.size(self.pt):
            # No zero divisions
            return arcsinh(self.z / self.pt)
        # Special treatment for zero divisions
        return np.where(self.pt>0, arcsinh(self.z / self.pt), sign(self.z)*999)
    
    @property
    def phi(self):
        if np.count_nonzero(self.x) == np.size(self.x):
            # No zero divisions
            return arctan(self.y / self.x)
        # Special treatment for zero divisions
        return np.where(self.x!=0, arctan(self.y / self.x), sign(self.y)*pi/2)
    
    @property
    def magnitude2(self):
        return square(self.x) + square(self.y) + square(self.z)
    
    @property
    def magnitude(self):
        return sqrt(self.magnitude2)
    
    @property
    def unitVector(self):
        mag = self.magnitude
        return Vector(self.x/mag, self.y/mag, self.z/mag)
    
    def setPtEtaPhi(self, pt, eta, phi):
        self.x = pt*cos(phi)
        self.y = pt*sin(phi)
        self.z = pt*sinh(eta)
    
    def dot(self, v):
        return self.x*v.x + self.y*v.y + self.z*v.z
    
    def cross(self, v):
        return Vector(self.y*v.z-v.y*self.z, self.z*v.x-v.z*self.x, self.x*v.y-v.x*self.y)
    
    def angle(self, v):
        if isinstance(v, LorentzVector):
            v = v.vector3
        ptot2 = self.magnitude2 * v.magnitude2
        arg = self.dot(v) / sqrt(ptot2)
        return arccos(arg)
    
    def deltaPhi(self, v):
        dPhi = self.phi - v.phi
        return np.where(dPhi>0, dPhi%pi, 0) + np.where(dPhi<0, dPhi%(-pi), 0)
    
    def rotate(self, angle, axis):
        matrix = rotation_matrix(angle, axis)
        vector = np.asarray([self.x, self.y, self.z])
        
        is_array = (np.asarray(angle).shape != ())
        if is_array:
            N = np.asarray(angle).size
            if np.asarray(axis.x).shape == ():
                axis = Vector([axis.x]*N, [axis.y]*N, [axis.z]*N)
            elif np.asarray(axis.x).size != N:
                raise Exception("rotate angles and axes are not arrays of the same length.")
            matrix = np.transpose(matrix, (2,0,1))
            vector = np.transpose([vector], (2,1,0))
        
        rotated = np.matmul(matrix, vector)
        
        if is_array:
            rotated = np.transpose(rotated)[0]
        
        self.x = rotated[0]
        self.y = rotated[1]
        self.z = rotated[2]
    
    def __add__(self, v):
        return LorentzVector(self.x+v.x, self.y+v.y, self.z+v.z)
    
    def __sub__(self, v):
        return LorentzVector(self.x-v.x, self.y-v.y, self.z-v.z)
    
    def __mul__(self, s):
        return LorentzVector(self.x*s, self.y*s, self.z*s)
    
    def __truediv__(self, s):
        return LorentzVector(self.x/s, self.y/s, self.z/s)
    
    def __neg__(self):
        return LorentzVector(-self.x, -self.y, -self.z)

#-------------------------------------------------------------------------------
class LorentzVector(Vector):
    def __init__(self, x=0, y=0, z=0, t=0):
        self.x = x
        self.y = y
        self.z = z
        self.t = t
    
    @property
    def vector3(self):
        return Vector(self.x, self.y, self.z)
    
    @property
    def magnitude2(self):
        return square(self.t) - square(self.x) - square(self.y) - square(self.z)
    
    @property
    def magnitude(self):
        return sqrt(self.magnitude2)
    
    @property
    def m(self):
        return sqrt(self.magnitude2)
    
    def setPtEtaPhiE(self, pt, eta, phi, e):
        self.setPtEtaPhi(pt, eta, phi)
        self.t = e
    
    def setPtEtaPhiM(self, pt, eta, phi, m):
        self.setPtEtaPhiE(pt, eta, phi, sqrt(square(pt*cosh(eta)) + square(m)))
    
    def angle(self, v):
        return self.vector3.angle(v)
    
    def boostVector(self):
        return Vector(self.x/self.t, self.y/self.t, self.z/self.t)
    
    def boost(self, b):
        b2 = square(b.x) + square(b.y) + square(b.z)
        gamma = reciprocal(sqrt(1.0 - b2))
        bp = self.x*b.x + self.y*b.y + self.z*b.z
        gamma2 = (gamma - 1.0) / b2
        
        self.x += gamma2*bp*b.x + gamma*b.x*self.t
        self.y += gamma2*bp*b.y + gamma*b.y*self.t
        self.z += gamma2*bp*b.z + gamma*b.z*self.t
        self.t = gamma*(self.t + bp)
    
    def __add__(self, v):
        return LorentzVector(self.x+v.x, self.y+v.y, self.z+v.z, self.t+v.t)
    
    def __sub__(self, v):
        return LorentzVector(self.x-v.x, self.y-v.y, self.z-v.z, self.t-v.t)
    
    def __neg__(self):
        return LorentzVector(-self.x, -self.y, -self.z, -self.t)

#-------------------------------------------------------------------------------
def rotation_matrix(angle, axis):
    axis = axis.unitVector
    a = cos(angle/2.)
    foo = -axis * sin(angle/2.)
    b = foo.x
    c = foo.y
    d = foo.z
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])
