import os

import numpy as np

from ROOT import gROOT, gPad
from ROOT import Double
from ROOT import TH1D
from ROOT.TMath import Sqrt as sqrt
from ROOT.TMath import Hypot as hypot

from general_utils import root_open
from numpy_utils import objectAsType
from enum import EnumRegion, EnumVariable


#-------------------------------------------------------------------------------
# These formulae are too long!
friendly_branch_name_map = {}

friendly_branch_name_map["reco_Z_pT_mm"] = "sqrt((muons_pt[0]*cos(muons_phi[0]) + muons_pt[1]*cos(muons_phi[1]))**2 + (muons_pt[0]*sin(muons_phi[0]) + muons_pt[1]*sin(muons_phi[1]))**2)"
friendly_branch_name_map["reco_Z_pT_ee"] = "sqrt((electrons_pt[0]*cos(electrons_phi[0]) + electrons_pt[1]*cos(electrons_phi[1]))**2 + (electrons_pt[0]*sin(electrons_phi[0]) + electrons_pt[1]*sin(electrons_phi[1]))**2)"
friendly_branch_name_map["reco_Z_pT_mutauMET"] = "sqrt((muons_pt[0]*cos(muons_phi[0]) + taus_pt[0]*cos(taus_phi[0]) + met_et*cos(met_phi))**2 + (muons_pt[0]*sin(muons_phi[0]) + taus_pt[0]*sin(taus_phi[0]) + met_et*sin(met_phi))**2)"
friendly_branch_name_map["reco_Z_pT_etauMET"] = "sqrt((electrons_pt[0]*cos(electrons_phi[0]) + taus_pt[0]*cos(taus_phi[0]) + met_et*cos(met_phi))**2 + (electrons_pt[0]*sin(electrons_phi[0]) + taus_pt[0]*sin(taus_phi[0]) + met_et*sin(met_phi))**2)"

#-------------------------------------------------------------------------------
def friendlyBranchName(branch):
    for name,formula in friendly_branch_name_map.iteritems():
        if branch == formula:
            return name

    for symbol in [" ", "::", "TMath", "TVector2", "Phi_mpi_pi"]:
        branch = branch.replace(symbol, "")
    for symbol in "[]().":
        branch = branch.replace(symbol, "_")
    return branch.replace("**", "__pow__")\
                 .replace("+", "__plus__")\
                 .replace("-", "__minus__")\
                 .replace("*", "__times__")\
                 .replace("/", "__div__")\
                 .replace("__pow__2", "_sq")\
                 .replace(":", "__vs__")

#-------------------------------------------------------------------------------
def variablesFromCutString(cuts):
    cuts = str(cuts)
    for symbol in "()[]+-*/!=><&|. ":
        cuts = cuts.replace(symbol, ";")
    cuts = cuts.replace("TMath::IsNaN", ";")
    cuts = cuts.replace("TMath::Cos", ";")
    cuts = cuts.replace("TVector2::Phi_mpi_pi", ";")
    cuts = cuts.replace("fabs", ";")
    cuts = cuts.replace("sqrt", ";")
    vars = list(set(v for v in cuts.split(";") if not v.isdigit()))
    if "" in vars:
        vars.remove("")
    return vars

#-------------------------------------------------------------------------------
# Moved definition to fakes_utils
#def failRegion(region):

#-------------------------------------------------------------------------------
def removeXErrors(h):
    try:
        assert h.InheritsFrom("TGraphAsymmErrors")
    except:
        raise NotImplementedError("removeXErrors() only works for objects derived from TGraphAsymmErrors.")

    N = h.GetN()
    x, y = Double(), Double()
    h.GetPoint(0, x, y)
    x_min = x
    h.GetPoint(N-1, x, y)
    x_max = x

    bin_width = 1 if N is 1 else (x_max - x_min) / float(N-1)
    if bin_width == 0:
        bin_width = 1
    no_width = bin_width * 1e-5

    for i in xrange(N):
        h.SetPointEXhigh(i, no_width)
        h.SetPointEXlow(i, no_width)

#-------------------------------------------------------------------------------
def TGraphToTH1D(g, suffix="", error=None, is_data=False):
    x, y = Double(), Double()

    if is_data:
        N = g.GetN()
        if N == 1:
            x_min = 0
            x_max = 1
        else:
            g.GetPoint(0, x, y)
            x_min = float(x)
            g.GetPoint(N-1, x, y)
            x_max = float(x)
            bin_width = (x_max - x_min) / float(N-1)
            x_min -= bin_width/2.
            x_max += bin_width/2.
    else:
        N = (g.GetN() - 7) / 2
        if N == 1:
            x_min = 0
            x_max = 1
        else:
            g.GetPoint(2, x, y)
            x_min = float(x)
            g.GetPoint(2*N+4, x, y)
            x_max = float(x)

    name = g.GetName()+suffix
    old_h = gROOT.FindObject(name)
    if old_h: old_h.Delete()
    h = TH1D(name, g.GetTitle(), N, x_min, x_max)
    if not is_data:
        h.Sumw2()

    if is_data:
        for i in xrange(N):
            g.GetPoint(i, x, y)
            h.SetBinContent(i+1, y)
            h.SetBinError(i+1, sqrt(y) if y>0 else 0)
    else:
        for i in xrange(N):
            g.GetPoint(2*i+3, x, y)
            h.SetBinContent(i+1, y)
            if error is None:
                h.SetBinError(i+1, 0)
            else:
                error.GetPoint(2*i+3, x, y)
                h.SetBinError(i+1, y - h.GetBinContent(i+1))

    return h

#-------------------------------------------------------------------------------
def getSignalHistogram(file):
    current_pad = gPad  # cd back to this pad before returning

    bkg_sig_name = ""
    bkg_name = ""

    with root_open(file) as f:
        file = os.path.basename(file).replace(".root", "")
        can = f.Get(file)
        pad_1 = can.GetPrimitive(file + "_pad1")
        for obj in pad_1.GetListOfPrimitives():
            if not obj.InheritsFrom("RooCurve"):
                continue
            # Find the histograms that correspond to bkg and bkg+sig
            # Logic:
            #   bkg+sig pdf = pdf with the longest name with signal in it
            #   bkg pdf = pdf with the longest name without signal in it
            name = obj.GetName()
            if "Ztaumu" in name or "Ztaue" in name:
                if len(name) > len(bkg_sig_name):
                    bkg_sig_name = name
            elif len(name) > len(bkg_name):
                bkg_name = name
        h_bkg = TGraphToTH1D(pad_1.GetPrimitive(bkg_name), "_bkg_prefit")
        if bkg_sig_name == "":
            # There is no signal at all
            # bkg+sig is just bkg
            h_bkg_sig = TGraphToTH1D(pad_1.GetPrimitive(bkg_name), "_bkg_sig_prefit")
        else:
            h_bkg_sig = TGraphToTH1D(pad_1.GetPrimitive(bkg_sig_name), "_bkg_sig_prefit")

        h_sig = h_bkg_sig.Clone("{}_sigOnly".format(h_bkg_sig.GetName()))
        h_sig.Add(h_bkg, -1)
        h_sig.SetDirectory(0)
        del h_bkg_sig
        del h_bkg

    current_pad.cd()
    return h_sig

#-------------------------------------------------------------------------------
def getNonZeroMinimum(h):
    min = float('inf')
    for i in xrange(1, h.GetNbinsX()+1):
        x = h.GetBinContent(i)
        if x > 0 and x < min:
            min = x
    return 1. if min == float('inf') else min

#-------------------------------------------------------------------------------
def getMaxBinIndex(h):
    if h.InheritsFrom("TH3"):
        Nx = h.GetNbinsX()
        Ny = h.GetNbinsY()
        Nz = h.GetNbinsZ()
    elif h.InheritsFrom("TH2"):
        Nx = h.GetNbinsX()
        Ny = h.GetNbinsY()
        Nz = -1
    elif h.InheritsFrom("TH1"):
        Nx = h.GetNbinsX()
        Ny = -1
        Nz = -1
    else:
        raise Exception("'{}' is not a ROOT histogram object.".format(h))
    return h.GetBin(Nx+1, Ny+1, Nz+1)

#-------------------------------------------------------------------------------
def forceHistNonNegative(h):
    if isinstance(h, dict):
        return forceHistNonNegative(h.values())
    elif hasattr(h, '__iter__'):
        for x in h:
            forceHistNonNegative(x)
        return
    max_idx = getMaxBinIndex(h)
    it = (i for i in xrange(max_idx+1) if h.GetBinContent(i)<0)
    for i in it:
        h.SetBinContent(i, 0)

#-------------------------------------------------------------------------------
def removeHistErrors(h):
    if isinstance(h, dict):
        return removeHistErrors(h.values())
    elif hasattr(h, '__iter__'):
        for x in h:
            removeHistErrors(x)
        return
    max_idx = getMaxBinIndex(h)
    for i in xrange(max_idx+1):
        h.SetBinError(i, 0)

#-------------------------------------------------------------------------------
def partialBlindData(data, blind_range):
    # data should be a TGraph (or RooHist)
    # blind_range should be a tuple of two numbers
    for i in xrange(data.GetN()):
        if data.GetX()[i] > blind_range[0] and data.GetX()[i] < blind_range[1]:
            data.SetPoint(i, data.GetX()[i], -999)

#-------------------------------------------------------------------------------
def mergeOverflows(h):
    if isinstance(h, dict):
        return mergeOverflows(h.values())
    elif hasattr(h, '__iter__'):
        for x in h:
            mergeOverflows(x)
        return
    elif h.InheritsFrom("TH3"):
        raise NotImplementedError("Cannot merge overflows for 3-D histogram '{}'.".format(h))
    elif h.InheritsFrom("TH2"):
        return mergeOverflows_2D(h)
    elif not h.InheritsFrom("TH1"):
        raise Exception("'{}' is not a ROOT histogram object.".format(h))

    N = h.GetNbinsX()
    val = h.GetBinContent(N) + h.GetBinContent(N+1)
    err = hypot(h.GetBinError(N), h.GetBinError(N+1))
    h.SetBinContent(N, val)
    h.SetBinError(N, err)
    h.SetBinContent(N+1, 0)
    h.SetBinError(N+1, 0)

#-------------------------------------------------------------------------------
def mergeOverflows_2D(h):
    if isinstance(h, dict):
        return mergeOverflows(h.values())
    elif hasattr(h, '__iter__'):
        for x in h:
            mergeOverflows(x)
        return
    elif h.InheritsFrom("TH3"):
        raise NotImplementedError("Cannot merge overflows for 3-D histograms.")
    elif not h.InheritsFrom("TH2") and h.InheritsFrom("TH1"):
        mergeOverflows(h)
        return
    elif not h.InheritsFrom("TH1"):
        raise Exception("'{}' is not a ROOT histogram object.".format(h))

    Nx = h.GetNbinsX()
    Ny = h.GetNbinsY()

    # Merge one axis first
    for i in xrange(1, Nx+2):
        idx0 = h.GetBin(i, Ny)    # largest bin
        idx1 = h.GetBin(i, Ny+1)  # overflow bin
        val = h.GetBinContent(idx0) + h.GetBinContent(idx1)
        err = hypot(h.GetBinError(idx0), h.GetBinError(idx1+1))
        h.SetBinContent(idx0, val)
        h.SetBinError(idx0, err)
        h.SetBinContent(idx1, 0)
        h.SetBinError(idx1, 0)

    # Merge the remaining axis
    for i in xrange(1, Ny+2):
        idx0 = h.GetBin(Nx, i)    # largest bin
        idx1 = h.GetBin(Nx+1, i)  # overflow bin
        val = h.GetBinContent(idx0) + h.GetBinContent(idx1)
        err = hypot(h.GetBinError(idx0), h.GetBinError(idx1+1))
        h.SetBinContent(idx0, val)
        h.SetBinError(idx0, err)
        h.SetBinContent(idx1, 0)
        h.SetBinError(idx1, 0)

#-------------------------------------------------------------------------------
def inheritOverflows(h):
    if isinstance(h, dict):
        return inheritOverflows(h.values())
    elif hasattr(h, '__iter__'):
        for x in h:
            inheritOverflows(x)
        return
    elif h.InheritsFrom("TH3"):
        raise NotImplementedError("Cannot merge overflows for 3-D histogram '{}'.".format(h))
    elif h.InheritsFrom("TH2"):
        return inheritOverflows_2D(h)
    elif not h.InheritsFrom("TH1"):
        raise Exception("'{}' is not a ROOT histogram object.".format(h))

    N = h.GetNbinsX()
    h.SetBinContent(N+1, h.GetBinContent(N))
    h.SetBinError(N+1, h.GetBinError(N))

#-------------------------------------------------------------------------------
def inheritOverflows_2D(h):
    if isinstance(h, dict):
        return inheritOverflows(h.values())
    elif hasattr(h, '__iter__'):
        for x in h:
            inheritOverflows(x)
        return
    elif h.InheritsFrom("TH3"):
        raise NotImplementedError("Cannot merge overflows for 3-D histograms.")
    elif not h.InheritsFrom("TH2") and h.InheritsFrom("TH1"):
        inheritOverflows(h)
        return
    elif not h.InheritsFrom("TH1"):
        raise Exception("'{}' is not a ROOT histogram object.".format(h))

    Nx = h.GetNbinsX()
    Ny = h.GetNbinsY()

    # One axis first
    for i in xrange(1, Nx+2):
        idx0 = h.GetBin(i, Ny)    # largest bin
        idx1 = h.GetBin(i, Ny+1)  # overflow bin
        h.SetBinContent(idx1, h.GetBinContent(idx0))
        h.SetBinError(idx1, h.GetBinError(idx0))

    # The remaining axis
    for i in xrange(1, Ny+2):
        idx0 = h.GetBin(Nx, i)    # largest bin
        idx1 = h.GetBin(Nx+1, i)  # overflow bin
        h.SetBinContent(idx1, h.GetBinContent(idx0))
        h.SetBinError(idx1, h.GetBinError(idx0))

#-------------------------------------------------------------------------------
def findHistValue(h, x, y=0):
    return h.GetBinContent(h.FindBin(x, y))

#-------------------------------------------------------------------------------
# Numpy vectorized version of findHistValue()
def findHistValues(h, x_arr, y_arr=None):
    if h.InheritsFrom("TH2") == (y_arr is None):
        raise RuntimeError("findHistValues(): the histogram and the input array dimensions do not match.")

    if y_arr is None:
        f = lambda _x: findHistValue(h, _x)
        vectorized_findHistValue = np.vectorize(f)
        x_arr = objectAsType(x_arr, '<f8')
        return vectorized_findHistValue(x_arr)

    else:
        f = lambda _x, _y: findHistValue(h, _x, _y)
        vectorized_findHistValue = np.vectorize(f)
        x_arr = objectAsType(x_arr, '<f8')
        y_arr = objectAsType(y_arr, '<f8')
        return vectorized_findHistValue(x_arr, y_arr)

#-------------------------------------------------------------------------------
def guessRegionVariable(file_name):
    # Guess the region and variable from the file name (in a hard-coded way)
    f = os.path.splitext(os.path.basename(file_name))[0]
    f_split = f.split('_')
    if f_split[0] == "Plot" and f_split[1].isdigit():
        f = '_'.join(f_split[2:])

    prefit = "beforeFit" in f
    if "_1P_" in f:
        p = "1P"
        r = f.split("_1P_")[0]
        v = f.replace(r+"_1P_", "").replace("_beforeFit", "").replace("_afterFit", "")
    elif "_3P_" in f:
        p = "3P"
        r = f.split("_3P_")[0]
        v = f.replace(r+"_3P_", "").replace("_beforeFit", "").replace("_afterFit", "")
    elif "_1p0n_" in f:
        p = "1p0n"
        r = f.split("_1p0n_")[0]
        v = f.replace(r+"_1p0n_", "").replace("_beforeFit", "").replace("_afterFit", "")
    elif "_1pXn_" in f:
        p = "1pXn"
        r = f.split("_1pXn_")[0]
        v = f.replace(r+"_1pXn_", "").replace("_beforeFit", "").replace("_afterFit", "")
    elif "_3p_" in f:
        p = "3p"
        r = f.split("_3p_")[0]
        v = f.replace(r+"_3p_", "").replace("_beforeFit", "").replace("_afterFit", "")
    else:
        p = "inclusive"
        if "OS_bveto" in f:
            # Hack for v22 region definition
            r = f.split("OS_bveto")[0] + "OS_bveto"
        elif "CR_HDB_mue_1" in f:
            r = "CR_HDB_mue_1"
        elif "CR_HDB_mue_2" in f:
            r = "CR_HDB_mue_2"
        elif "CR_HDB_mue_3" in f:
            r = "CR_HDB_mue_3"
        elif "CR_HDB_mue_4" in f:
            r = "CR_HDB_mue_4"
        elif "CR_HDB_mue_5" in f:
            r = "CR_HDB_mue_5"
        elif "CR_HDB_mue_6" in f:
            r = "CR_HDB_mue_6"
        elif "CR_HDB_mue_7" in f:
            r = "CR_HDB_mue_7"
        elif "CR_HDB_mue_8" in f:
            r = "CR_HDB_mue_8"
        elif "CR_HDB_mue_9" in f:
            r = "CR_HDB_mue_9"
        elif "CR_HDB_emu_1" in f:
            r = "CR_HDB_emu_1"
        elif "CR_HDB_emu_2" in f:
            r = "CR_HDB_emu_2"
        elif "CR_HDB_emu_3" in f:
            r = "CR_HDB_emu_3"
        elif "CR_HDB_emu_4" in f:
            r = "CR_HDB_emu_4"
        elif "CR_HDB_emu_5" in f:
            r = "CR_HDB_emu_5"
        elif "CR_HDB_emu_6" in f:
            r = "CR_HDB_emu_6"
        elif "CR_HDB_emu_7" in f:
            r = "CR_HDB_emu_7"
        elif "CR_HDB_emu_8" in f:
            r = "CR_HDB_emu_8"
        elif "CR_HDB_emu_9" in f:
            r = "CR_HDB_emu_9"
        elif "FRW_mu" in f:
            r = "FRW_mu"
        elif "FRW_el" in f:
            r = "FRW_el"
        elif "CRDiBoson_mu" in f:
            r = "CRDiBoson_mu"
        elif "CRDiBoson_el" in f:
            r = "CRDiBoson_el"
        elif "CRDiBoson" in f:
            r = "CRDiBoson"
        elif "_invmT" in f:
            r = f.split("_invmT_")[0] + "_invmT"
        elif "_mutaulepe" in f:
            r = f.split("_mutaulepe_")[0] + "_mutaulepe"
        elif "_mutaulepmu" in f:
            r = f.split("_mutaulepmu_")[0] + "_mutaulepmu"
        elif "_eltaulepe" in f:
            r = f.split("_eltaulepe_")[0] + "_eltaulepe"
        elif "_eltaulepmu" in f:
            r = f.split("_eltaulepmu_")[0] + "_eltaulepmu"
        elif "_mumu_" in f:
            r = f.split("_mumu_")[0] + "_mumu"
        elif "_elel_" in f:
            r = f.split("_elel_")[0] + "_elel"
        elif "_muel_" in f:
            r = f.split("_muel_")[0] + "_muel"
        elif "_elmu_" in f:
            r = f.split("_elmu_")[0] + "_elmu"
        elif "_el_" in f:
            r = f.split("_el_")[0] + "_el"
        else:
            r = f.split("_mu_")[0] + "_mu"
        v = f.replace(r+"_", "").replace("_beforeFit", "").replace("_afterFit", "")

    ### HACK for combined workspace
    if v.endswith("_ZLFV_tauhad"):
        v = v.replace("_ZLFV_tauhad", "")
    elif v.endswith("_ZLFV_taulep"):
        v = v.replace("_ZLFV_taulep", "")

    ### HACK for m_ll
    if v == "m_ll":
        v = "el_el_invmass" if "_el_" in f else "mu_mu_invmass"

    return r, v, p, prefit

#-------------------------------------------------------------------------------
def enumeratePlots(region, variable, tau_prongs):
    # Enumerate the plots so that we can more easily find the plots we want.
    # The enumeration is an integer string with digits XXYYYZ

    # XX is the enumeration for the region
    enum = "{:02d}".format(EnumRegion[region])
    # YYY is the enumeration for the variable
    enum += "{:03d}".format(EnumVariable[variable])
    # Z is the enumeration for number of tau prongs
    if tau_prongs == "1P":
        enum += "1"
    elif tau_prongs == "3P":
        enum += "2"
    elif tau_prongs == "1p0n":
        enum += "1"
    elif tau_prongs == "1pXn":
        enum += "2"
    elif tau_prongs == "3p":
        enum += "3"
    else:
        enum += "0"

    return enum

#-------------------------------------------------------------------------------
def cleanUpPlotDirectory(dir, remove_fit_parameters=False):
    for f in os.listdir(dir):
        if os.path.isdir(os.path.join(dir, f)):
            continue
        if f.startswith("fit_parameters") and not remove_fit_parameters:
            continue
        if f.endswith("Fit.pdf"):
            continue
        if f.endswith("Fit.eps"):
            continue
        if f.endswith("Fit.png"):
            continue
        if f.endswith("Fit.root"):
            continue
        if "combined_NormalMeasurement" in f:
            continue
        if f.startswith("ULScan") or "Asym_CLs_grid" in f:
            continue
        if f.startswith("PrintWS"):
            continue
        if f.startswith("ranking"):
            continue
        if f.startswith("YieldsTable"):
            continue
        if f.startswith("can_NLL"):
            continue
        if f.endswith(".log"):
            continue
        if f.endswith(".xml"):
            continue
        if f.endswith(".tar") or f.endswith(".gz"):
            continue
        if f.startswith(".nfs"):
            continue
        #print "Cleaning up output directory: removing '{}'".format(f)
        os.remove(os.path.join(dir, f))
