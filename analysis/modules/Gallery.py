### This module is an adaptation of https://github.com/ajgilbert/gallery

import os, shutil
from collections import OrderedDict

from general_utils import checkMakeDirs
from plot_utils import guessRegionVariable

import inspect
_modules_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
_index = os.path.join(_modules_dir, "gallery_templates/index.html")
_download_all = os.path.join(_modules_dir, "gallery_templates/download_all.php")


#-------------------------------------------------------------------------------
# Some html snippets
_PLOT_ELE="""
  <div class="plot-element{tags}">
      <p class="file-title" style="font-size: 100%;">{file}</p>
      <a href="{file}" class="fancybox" data-fancybox-group="gallery" title="{file}" id="{file}">
      <img src="{file}" alt="image">
      </a>
  </div>\n"""

_OTHER_FORMATS="""
	<p>Other formats: {text}</p>"""

_SINGLE_FORMAT="""
	<a href="{file}">[{ext}]</a>"""

_SEARCH_BOX="""<p><input type="text" class="quicksearch" placeholder="Regex Search" /></p>"""

_BUTTON="""<button class="button" data-filter=".{FILTER}">{NAME}</button>"""

_BUTTON_GROUP="""\n
<div class="button-group js-radio-button-group" data-filter-group="{GROUP}">
  <button class="button is-checked" data-filter="">All {TITLE}</button>
  {BUTTONS}
</div>"""

_OTHER_LIST="""\n
<p id="otherfiles">Other files:</p>
<ul>
  {LISTITEMS}
</ul>
<p><a href="#top">Back to top</a></p>"""


#-------------------------------------------------------------------------------
# Markdown snippet
_METADATA="""
- Ntuple version: {ntup_ver}
- Plot version: {plot_ver}
- Created at: {created_at}
- Created by: {created_by}
"""

#-------------------------------------------------------------------------------
# Avoid using set() to keep the order
class orderedSet(list):
    def __init__(self, val=[]):
        super(orderedSet, self).__init__(val)
    def add(self, val):
        if val not in self:
            self.append(val)

#-------------------------------------------------------------------------------
class Gallery(object):

    def __init__(self, output_dir):
        self.output_dir = output_dir
        checkMakeDirs(output_dir)

        self.plots = OrderedDict()
        self.search_groups = OrderedDict([('regions', orderedSet()),
                                          ('tau decay modes', orderedSet()),
                                          ('variables', orderedSet()),
                                          ('pre-/post-fit', orderedSet()),
                                         ])

        self.metadata = {'ntup_ver':'unknown',
                         'plot_ver':'unknown',
                         'created_at':'unknown',
                         'created_by':'unknown',
                         'extra_comments':[],
                        }

    #-----------------------------
    def addPlots(self, plots):
        for plot in plots:
            self.addPlot(plot)

    #-----------------------------
    def addPlot(self, plot):
        file = os.path.basename(plot)

        if file.startswith("Plot_") and file.endswith("Fit.png"):
            # This was probably named by replotFitOutputs.py
            # Strip information from the file name
            r, v, p, prefit = guessRegionVariable(file)
            f = "prefit" if prefit else "postfit"
            file = "{}_{}_{}_{}.png".format(r, v, p, f)  # rename
            shutil.copy(plot, os.path.join(self.output_dir, file))
            tags = {'regions':r, 'variables':v, 'tau decay modes':p, 'pre-/post-fit':f}

        elif file.startswith("F_") or file.startswith("k_") or file.startswith("R_") \
          or file.startswith("SS_F_") or file.startswith("SS_k_") or file.startswith("SS_R_"):
            # This should be a FF plot
            if file.split("_")[-2] == "el":
                r = "e-tau"
                v = file.split("_el_")[0]
            elif file.split("_")[-2] == "mu":
                r = "mu-tau"
                v = file.split("_mu_")[0]
            else:
                r = "unknown region"
                v = "unknown variable"
            p = file.replace(".png", "").split("_")[-1].upper()
            shutil.copy(plot, self.output_dir)
            tags = {'regions':r, 'variables':v, 'tau decay modes':p, 'pre-/post-fit':'N/A'}

        else:
            shutil.copy(plot, self.output_dir)
            tags = {'regions':'unknown region',
                    'variables':'unknown variable',
                    'tau decay modes':'unknown tau decay mode',
                    'pre-/post-fit':'unknown fit stage',
                   }

        self.plots[file] = tags
        for key,val in tags.iteritems():
            self.search_groups[key].add(val)

    #-----------------------------
    def copyDownloadAllPhp(self):
        shutil.copy(_download_all, os.path.join(self.output_dir, "download_all.php"))

    #-----------------------------
    def writeReadme(self):
        with open(os.path.join(self.output_dir,'README.md'), "w") as f:
            metadata = _METADATA.format(ntup_ver=self.metadata["ntup_ver"],
                                        plot_ver=self.metadata["plot_ver"],
                                        created_at=self.metadata["created_at"],
                                        created_by=self.metadata["created_by"],
                                       )
            f.write(metadata)
            if self.metadata["extra_comments"]:
                f.write('- Notes / comments / issues:\n')
            for comment in self.metadata["extra_comments"]:
                f.write('  - {}\n'.format(comment))

    #-----------------------------
    def writeIndex(self):
        with open(_index) as template:
            index = template.read()

        title = os.path.basename(os.path.normpath(self.output_dir))
        index = index.replace('{TITLE}', title)

        elements = ""
        for f, tags in self.plots.iteritems():
            tags = " " + " ".join(tags.values())
            elements += _PLOT_ELE.format(file=f, tags=tags)
        index = index.replace('{ELEMENTS}', elements)

        button_groups = ""
        for grp,tags in self.search_groups.iteritems():
            buttons='\n'.join([_BUTTON.format(NAME=v, FILTER=v) for v in tags])
            button_groups += _BUTTON_GROUP.format(GROUP=grp, TITLE=grp, BUTTONS=buttons)
        index = index.replace('{BUTTONS}', button_groups)

        index = index.replace('{SEARCH}', _SEARCH_BOX)

        self.writeReadme()
        index = index.replace('{LINKS}', r'<p><a href="README.md">README</a></p>')

        self.copyDownloadAllPhp()

        # Unused options
        index = index.replace('{OTHERFILESJUMP}', '')
        index = index.replace('{OTHERFILES}', '')

        # Write the html to the output folder
        with open(os.path.join(self.output_dir,'index.html'), "w") as f:
            f.write(index)
