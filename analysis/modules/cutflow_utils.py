import json
from collections import OrderedDict
from pprint import pprint

from math import sqrt, log

#import numpy as np
#from root_numpy import tree2array
from ROOT import gROOT, TH1D

from Regions import Cut
from general_utils import root_open, rootVerbosityLevel
from general_utils import altReplace
from maths_utils import safeDivide
from latex_utils import LatexFile


#-------------------------------------------------------------------------------
def buildCutflow(inputs, regions, lumi, unweighted=False, count_data=False, aliases=[]):
    data = {}
    
    data["lumi"] = lumi
    data["unweighted"] = unweighted
    
    data["signals"] = OrderedDict()
    for s in inputs.signals.values():
        data["signals"][s.name] = {"name" : s.name, "label" : s.label}
    
    data["backgrounds"] = OrderedDict()
    for b in reversed(inputs.bkgs.values()):
        data["backgrounds"][b.name] = {"name" : b.name, "label" : b.label, "files" : [f.name for f in b.files.values()] }
    
    if count_data:
        data["Data"] = {"name" : "Data", "label" : "Data"}
    
    data["regions"] = []
    for r in regions.values():
        cuts = []
        for k,c in r.cuts.iteritems():
            cuts.append({"name": k, "label": c.label, "cut":c.string})
        region = {"name": r.name, "title": r.title, "cuts": cuts}
        data["regions"].append(region)
    
    fixed_cuts = ["Initial", "After obj. def."]
    if not unweighted:
        fixed_cuts.append("After obj. def. (weighted)")
    
    common_cuts = findCommonCuts(data["regions"])
    
    print "="*50
    print "Identified the following common cuts:"
    for i, c in enumerate(fixed_cuts + common_cuts):
        print "  {}: {}".format(i, c)
    
    for r in regions:
        if len(regions[r].cuts) == len(common_cuts):
            continue
        print "-"*50
        print "Identified the following extra cuts for '{}':".format(r)
        for i,c in enumerate(regions[r].cuts.values()):
            if i+1 > len(common_cuts):
                print "  {}: {}".format(i+len(fixed_cuts), c.string)
    print "="*50
    
    files = {}  # Keep files opened until we don't need them anymore
    trees = {}  # Also store the corresponding tree names for convenience
    with rootVerbosityLevel("Error"):  # Ignore SH::Metadata warnings
        for i in inputs:
            files[i] = [root_open(f.name) for f in inputs[i].files.values()]
            trees[i] = ["{}_NOMINAL".format(f.tree) for f in inputs[i].files.values()]
    
    data["cutflows"] = OrderedDict()
    cut_values = {}  # Store cut values to avoid processing the same cuts repeatedly
    for r in regions:
        print "Processing region '{}'\n".format(r)
        data["cutflows"][r] = OrderedDict()
        
        total_cut = Cut("1")
        total_weights = [str(lumi*1000), "normWeight"] + regions[r].basic_weights
        
        #-------------------------------------------------
        # Add "Initial"
        name = "Initial"
        if not name in cut_values:
            print "  Processing cut '{}'\n".format(name)
            values = {}
            for i in inputs:
                if i=="data": continue  # Data do not have "Initial" counts
                values[i] = sum(f.Get("metadata").GetBinContent(7) for f in files[i])
            cut_values[name] = values
        else:
            print "  Cut '{}' has previously been processed".format(name)
        print "\n  {}".format(cut_values[name])
        data["cutflows"][r][name] = cut_values[name]
        
        #-------------------------------------------------
        # Add "After obj. def."
        print "-"*50
        name = "After obj. def."
        if not name in cut_values:
            print "  Processing cut '{}'\n".format(name)
            cut = "1"
            weight = None
            values = {}
            for i in inputs:
                values[i] = sum(countEvents(f, t, cut, weight, aliases=aliases) for f, t in zip(files[i], trees[i]))
            cut_values[name] = values
        else:
            print "  Cut '{}' has previously been processed".format(name)
        print "\n  {}".format(cut_values[name])
        data["cutflows"][r][name] = cut_values[name]
        
        #-------------------------------------------------
        # Add "After obj. def. (weighted)"
        if not unweighted:
            print "-"*50
            name = "After obj. def. (weighted)"
            if not name in cut_values:
                print "  Processing cut '{}'\n".format(name)
                cut = str(total_cut)
                weight = "*".join(total_weights)
                print "    cut    = {}\n".format(cut)
                print "    weight = {}\n".format(weight)
                values = {}
                for i in inputs:
                    _weight = None if i=="data" else weight
                    values[i] = sum(countEvents(f, t, cut, _weight, aliases=aliases) for f, t in zip(files[i], trees[i]))
                cut_values[name] = values
            else:
                print "  Cut '{}' has previously been processed".format(name)
            print "\n  {}".format(cut_values[name])
            data["cutflows"][r][name] = cut_values[name] 
        
        #-------------------------------------------------
        # Now loop over the remaining cuts
        for c in regions[r].cuts.values():
            print "-"*50
            name = c.name
            total_cut &= c
            total_weights += c.add_weights
            if not str(total_cut) in cut_values:
                print "  Processing cut '{}'\n".format(name)
                cut = str(total_cut)
                weight = "*".join(total_weights) if not unweighted else None
                print "    cut    = {}\n".format(cut)
                print "    weight = {}\n".format(weight)
                values = {}
                for i in inputs:
                    _weight = None if i=="data" else weight
                    values[i] = sum(countEvents(f, t, cut, _weight, aliases=aliases) for f, t in zip(files[i], trees[i]))
                cut_values[str(total_cut)] = values
            else:
                print "  Cut '{}' has previously been processed".format(name)
            print "\n  {}".format(cut_values[str(total_cut)])
            data["cutflows"][r][name] = cut_values[str(total_cut)]
        
        #-------------------------------------------------
        print "="*50
    
    for input in files.values():
        for f in input:
            f.Close()
    
    return data

#-------------------------------------------------------------------------------
def writeCutflowTable(data, output_file, count_data=False):
    sigs = data['signals']
    bkgs = data['backgrounds']
    Data = data['Data'] if count_data else None
    regions = data['regions']
    cutflows = data['cutflows']
    
    lumi = data["lumi"]
    unweighted = data["unweighted"]
    
    formats = {}
    for process, initial in cutflows.values()[0].values()[0].iteritems():
        n_digits = len(str(int(initial)))
        formats[process] = "{}.2".format(n_digits)
    if count_data:
        # Data do not have "Initial" counts
        n_digits = len(str(int(cutflows.values()[0].values()[1]["data"])))
        formats["data"] = "{}.2".format(n_digits)
    
    print "Writing LaTeX table '{}'".format(output_file)
    latex = LatexFile(output_file)
    
    #-------------------------------------------------
    # Packages and preambles
    latex.addPackage("geometry", "a3paper, margin=1cm")
    latex.addPackage("amsmath")
    latex.addPackage("booktabs")
    latex.addPackage("rotating")
    latex.addPackage("tabularx")
    latex.addPackage("dcolumn")
    latex.addPackage("bm")
    latex.addPreamble(r"\newcolumntype{d}[1]{D{.}{.}{#1}}")
    latex.beginDocument()
    
    #-------------------------------------------------
    # Start sideswaytable
    latex.addLine(r"\begin{sidewaystable}")
    latex.addLine(r"\centering")
    latex.addLine(r"%\scriptsize")
    
    #-------------------------------------------------
    # The columns
    # Latex on Stoomboot is soooo old that it doesn't even support siunitx, oh dear...
    # Using dcolumn instead...
    write = latex.addLine  # I am lazy...
    write(r"\begin{tabularx}{\textwidth}{")
    write(r"    X % Cuts")
    if count_data:
        write(r"    d{%s}  %% %s" % (formats["data"], Data))
        write(r"    d{1.2}  % eff.")
        write(r"    @{\enskip(}d{1.2}@{)\qquad}  % Rel. Eff.")
    for s in sigs:
        write(r"    d{%s}  %% %s" % (formats[s], s))
        write(r"    d{1.2}  % eff.")
        if unweighted:
            write(r"    @{\enskip(}d{1.2}@{)\qquad}  % Rel. Eff.")
        else:
            write(r"    @{\enskip(}d{1.2}@{)\quad}  % Rel. Eff.")
            write(r"    d{2.2}  % Significance")
    for b in bkgs.keys()[:-1]:
        write(r"    d{%s}  %% %s" % (formats[b], b))
        write(r"    d{1.2}  % eff.")
        write(r"    @{\enskip(}d{1.2}@{)\qquad}  % Rel. Eff.")
    # The last one should have less space behind
    b = bkgs.keys()[-1]
    write(r"    d{%s}  %% %s" % (formats[b], b))
    write(r"    d{1.2}  % eff.")
    write(r"    @{\enskip(}d{1.2}@{)\quad}  % Rel. Eff.")
    write(r"}")
    N_columns = 1 + len(sigs)*(4-unweighted) + len(bkgs)*3
    
    #-------------------------------------------------
    # Table header
    write(r"\toprule")
    header = r"\multicolumn{1}{l}{\textbf{Selection}}"
    if count_data:
        header += r" & \multicolumn{1}{@{}r}{%s}" % bold("Data")
        header += r" & \multicolumn{1}{@{}r@{\enskip(}}{\bm{$\varepsilon_{\mathrm{tot}}$}}"
        header += r" & \multicolumn{1}{@{}r@{)\qquad}}{\bm{$\varepsilon_{\mathrm{rel}}$}}"
    for s in sigs.values():
        header += r" & \multicolumn{1}{@{}r}{%s}" % bold(s['label'])
        header += r" & \multicolumn{1}{@{}r@{\enskip(}}{\bm{$\varepsilon_{\mathrm{tot}}$}}"
        if unweighted:
            header += r" & \multicolumn{1}{@{}r@{)\qquad}}{\bm{$\varepsilon_{\mathrm{rel}}$}}"
        else:
            header += r" & \multicolumn{1}{@{}r@{)\quad}}{\bm{$\varepsilon_{\mathrm{rel}}$}}"
            header += r" & \multicolumn{1}{c}{\bm{$S$}}"
    for b in bkgs.values()[:-1]:
        header += r" & \multicolumn{1}{@{}r}{%s}" % bold(b['label'])
        header += r" & \multicolumn{1}{@{}r@{\enskip(}}{\bm{$\varepsilon_{\mathrm{tot}}$}}"
        header += r" & \multicolumn{1}{@{}r@{)\qquad}}{\bm{$\varepsilon_{\mathrm{rel}}$}}"
    # The last one should have less space behind
    b = bkgs.values()[-1]
    header += r" & \multicolumn{1}{@{}r}{%s}" % bold(b['label'])
    header += r" & \multicolumn{1}{@{}r@{\enskip(}}{\bm{$\varepsilon_{\mathrm{tot}}$}}"
    header += r" & \multicolumn{1}{@{}r@{)\quad}}{\bm{$\varepsilon_{\mathrm{rel}}$}} \\"
    write(header)
    
    #-------------------------------------------------
    # Now the fixed cuts
    write(r"\midrule")
    num_fixed_cuts = 3 - unweighted
    for i in xrange(num_fixed_cuts):
        name, values = cutflows.values()[0].items()[i]
        
        # significance (Only start calculating after obj. def. (weighted))
        if i == 2:
            total_bkg = sum(values[b] for b in bkgs)
            sig = {s:significance(values[s], total_bkg) for s in sigs}
        
        row = name
        if count_data:
            if i == 0:
                # Data do not have "Initial" counts
                row += r" & "
            else:
                row += r" & {}".format(fmt(values["data"]))
            if i == num_fixed_cuts - 1:
                row += r" & 1.00 & 1.00"  # eff
            else:
                row += r" & \multicolumn{2}{r}{}"  # eff
        for s in sigs:
            row += r" & {}".format(fmt(values[s]))
            if i == num_fixed_cuts - 1:
                row += r" & 1.00 & 1.00"  # eff
                if not unweighted:
                    row += r" & {}".format(fmt(sig[s]))  # sig
            else:
                row += r" & \multicolumn{2}{r}{}"  # eff
                if not unweighted:
                    row += r" &  "  # sig
        for b in bkgs:
            row += r" & {}".format(fmt(values[b]))
            if i == num_fixed_cuts - 1:
                row += r" & 1.00 & 1.00"  # eff
            else:
                row += r" & \multicolumn{2}{r}{}"  # eff
        row += r" \\"
        row = row.replace("inf", r"\rlap{$\infty$}").replace("nan", "0.00")
        write(row)
    
    init_values = values  # the "After obj. def." or "After obj. def. (weighted)" values
    prev_values = values
    
    #-------------------------------------------------
    # Now the common cuts (or cuts of the only region)
    write(r"\midrule")
    if len(regions) == 1:
        write(r"\multicolumn{%d}{c}{%s} \\" % (N_columns, bold(regions[0]['title'])))
    else:
        write(r"\multicolumn{%d}{c}{\textbf{Common selection}} \\" % N_columns)
    write(r"\midrule")
    num_common_cuts = len(findCommonCuts(regions))
    for i in xrange(num_fixed_cuts, num_fixed_cuts+num_common_cuts):
        prev_values = values
        name, values = cutflows.values()[0].items()[i]
        
        # efficiencies
        eff = {s:values[s]/float(init_values[s]) for s in sigs}
        eff.update({b:safeDivide(values[b],float(init_values[b])) for b in bkgs})
        eff_rel = {s:safeDivide(values[s],float(prev_values[s])) for s in sigs}
        eff_rel.update({b:safeDivide(values[b],float(prev_values[b])) for b in bkgs})
        if count_data:
            eff.update({"data":safeDivide(values["data"],float(init_values["data"]))})
            eff_rel.update({"data":safeDivide(values["data"],float(prev_values["data"]))})
        
        # significance
        if not unweighted:
            total_bkg = sum(values[b] for b in bkgs)
            sig = {s:significance(values[s], total_bkg) for s in sigs}
        
        row = regions[0]['cuts'][i - num_fixed_cuts]['label']
        if count_data:
            row += r" & {}".format(fmt(values["data"]))
            row += r" & {} & {}".format(fmt(eff["data"]), fmt(eff_rel["data"]))  # eff
        for s in sigs:
            row += r" & {}".format(fmt(values[s]))
            row += r" & {} & {}".format(fmt(eff[s]), fmt(eff_rel[s]))  # eff
            if not unweighted:
                row += r" & {}".format(fmt(sig[s]))  # sig
        for b in bkgs:
            row += r" & {}".format(fmt(values[b]))
            row += r" & {} & {}".format(fmt(eff[b]), fmt(eff_rel[b]))  # eff
        row += r" \\"
        row = row.replace("inf", r"\rlap{$\infty$}").replace("nan", "0.00")
        write(row)
    
    last_common_values = values
    
    #-------------------------------------------------
    # Now the extra cuts per region
    for r in regions:
        if len(r['cuts']) == num_common_cuts:
            continue  # No extra cuts other than the common ones
        write(r"\midrule")
        write(r"\multicolumn{%d}{c}{%s} \\" % (N_columns, bold(r['title'])))
        write(r"\midrule")
        values = last_common_values
        for i in xrange(num_common_cuts, len(r['cuts'])):
            i += num_fixed_cuts
            prev_values = values
            name, values = cutflows[r['name']].items()[i]
            
            # efficiencies
            eff = {s:values[s]/float(init_values[s]) for s in sigs}
            eff.update({b:safeDivide(values[b],float(init_values[b])) for b in bkgs})
            eff_rel = {s:safeDivide(values[s],float(prev_values[s])) for s in sigs}
            eff_rel.update({b:safeDivide(values[b],float(prev_values[b])) for b in bkgs})
            if count_data:
                eff.update({"data":safeDivide(values["data"],float(init_values["data"]))})
                eff_rel.update({"data":safeDivide(values["data"],float(prev_values["data"]))})
            
            # significance
            if not unweighted:
                total_bkg = sum(values[b] for b in bkgs)
                sig = {s:significance(values[s], total_bkg) for s in sigs}
            
            row = r['cuts'][i - num_fixed_cuts]['label']
            if count_data:
                row += r" & {}".format(fmt(values["data"]))
                row += r" & {} & {}".format(fmt(eff["data"]), fmt(eff_rel["data"]))  # eff
            for s in sigs:
                row += r" & {}".format(fmt(values[s]))
                row += r" & {} & {}".format(fmt(eff[s]), fmt(eff_rel[s]))  # eff
                if not unweighted:
                    row += r" & {}".format(fmt(sig[s]))  # sig
            for b in bkgs:
                row += r" & {}".format(fmt(values[b]))
                row += r" & {} & {}".format(fmt(eff[b]), fmt(eff_rel[b]))  # eff
            row += r" \\"
            row = row.replace("inf", r"\rlap{$\infty$}").replace("nan", "0.00")
            write(row)
    
    write(r"\bottomrule")
    write(r"\end{tabularx}")
    
    #-------------------------------------------------
    # Caption and label.
    titles = ['``{}"'.format(r['title']) for r in regions]
    if len(titles) == 1:
        write(r"\caption{Cutflow in %s}" % titles[0])
    else:
        write(r"\caption{Cutflow in %s and %s}" % (", ".join(titles[:-1]), titles[-1]))
    if unweighted:
        write(r'\label{tab:cutflow_unweighted_%s}' % "_".join(r['name'] for r in regions))
    else:
        write(r'\label{tab:cutflow_%s}' % "_".join(r['name'] for r in regions))
    
    #-------------------------------------------------
    # End
    write(r"\end{sidewaystable}")
    latex.endDocument()
    
    latex.writeTexFile()
    
    return latex

#-------------------------------------------------------------------------------
def findCommonCuts(regions):
    if len(regions) == 1:
        return [c['cut'] for c in regions[0]['cuts']]
    else:
        common_cuts = []
        all_cuts = [r['cuts'] for r in regions]
        for cuts in zip(*all_cuts):
            if not all(cut['cut'] == cuts[0]['cut'] for cut in cuts):
                break
            common_cuts.append(cuts[0]['cut'])
        return common_cuts

#-------------------------------------------------------------------------------
def countEvents(file, tree, cut, weight=None, is_data=False, aliases=[]):
    print "    Counting events in {}/{}".format(file.GetName(), tree)
    
    t = file.Get(tree)
    
    if weight is None:
        # Unweighted. We can simply use GetEntries()
        return float(t.GetEntries(cut))
    else:
        # Weighted. We use t.Project() to calculate the sum of weights
        if "normWeight" in weight and not t.GetFriend("weights"):
            t.AddFriend("weights")
        for alias in aliases:
            name, formula = alias.split(":=")
            t.SetAlias(name, formula)
        
        # OLD METHOD - tree2array uses too much memory and is not actually faster
        #arr = tree2array(t, branches=[weight], selection=cut)
        #N = arr[weight].sum()
        #while hasattr(N, '__len__'):
        #    N = N[0]
        #return N
        
        # Note: it is fastest to fill the underflow bin
        # So we create a histogram that starts from 1 and fill zeros
        h = gROOT.FindObject("h")
        if not h:
            h = TH1D("h", "h", 1, 1, 2)
        t.Project("h", "0", "({})*{}".format(cut, weight))
        return h.GetBinContent(0)

#-------------------------------------------------------------------------------
def significance(s, b):
    return sqrt(2*((s+b)*log(1.+safeDivide(s,float(b)))-s))

#-------------------------------------------------------------------------------
def bold(s):
    return r"\textbf{%s}" % altReplace(s, '$', (r'\bm{$', r'$}'))

#-------------------------------------------------------------------------------
def writeCacheFile(cache_name, data):
    with open(cache_name, 'w') as f:
        print "Writing data to cache file '{}'".format(cache_name)
        pprint(data)
        json.dump(data, f)

#-------------------------------------------------------------------------------
def readCacheFile(cache_name):
    with open(cache_name) as f:
        data = json.load(f, object_pairs_hook=OrderedDict) 
    return data

#-------------------------------------------------------------------------------
# Since we can't use siunitx on Stoomboot...
def fmt(number):
    if number < 10000:
        # No thousands separator for 4 digit numbers
        return "{:.2f}".format(number)
    else:
        # half-space as thousands separator
        return "{:,.2f}".format(number).replace(",", r"\,")
