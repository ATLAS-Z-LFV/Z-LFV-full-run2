import os
from latex_utils import LatexFile

#-------------------------------------------------------------------------------
class Plotbook(LatexFile):
    def __init__(self, tex_file_name, n_columns=4, n_rows=4, column_major=False, show_file_names=False):
        super(Plotbook, self).__init__(tex_file_name)
        
        self.n_columns = n_columns
        self.n_rows = n_rows
        self.show_file_names = show_file_names
        self.column_major = column_major
        
        self.plots = []
        
        self.addPackage("graphicx")
        self.addPackage("geometry", "margin=0.5in")
    
    #-----------------------------
    @property
    def float_width(self):
        # in terms of \textwidth
        return 1. / self.n_columns * 0.95
    
    #-----------------------------
    def addFigure(self, files):
        self.addLine(r"\begin{figure}[h!]")
        for file in files:
            self.addLine(r"\includegraphics[width=%f\textwidth]{%s}" % (self.float_width, file))
        if self.show_file_names: 
            for file in files:
                self.addLine(r"{\tiny \verb=%s=}" % file)
        self.addLine(r"\end{figure}")
    
    #-----------------------------
    def addAllPlots(self):
        # every N plots form a "page" (list of plots)
        N = self.n_columns * self.n_rows
        pages = [self.plots[i:i+N] for i in xrange(0, len(self.plots), N)]
        for page in pages:
            self.addPage(page)
    
    #-----------------------------
    def addPage(self, plots):
        rows = []
        if self.column_major:
            for i in xrange(self.n_rows):
                rows.append([plots[j] for j in xrange(i, len(plots), self.n_rows)])
        else:
            for i in xrange(self.n_rows):
                rows.append(plots[i*self.n_columns:(i+1)*self.n_columns])
        for row in rows:
            self.addFigure(row)
        self.clearPage()
