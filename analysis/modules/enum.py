### Enumeration for regions and variables, just so plots can be ordered more easily
#-------------------------------------------------------------------------------
class Enumeration(dict):
    def __getitem__(self, key):
        try:
            return super(Enumeration, self).__getitem__(key)
        except:
            print "WARNING: No enumeration defined for '{}', returning 0".format(key)
            return 0
    def __setitem__(self, key, value):
        if key in self:
            raise Exception('Enumeration for "{}" is already defined ({})'.format(key, self[key]))
        if isinstance(value, int):
            return super(Enumeration, self).__setitem__(key, value)
        else:
            raise Exception('Enumeration must be an integer - Was trying to set "{}":{} ({})'.format(key, value, value.__class__.__name__))

#-------------------------------------------------------------------------------
EnumRegion = Enumeration()

# @Ann-Kathrin, I don't know how I should organise this...
# Feel free to rearrange everything for yourself
#EnumRegion["CRDiBoson"]   = 20
#EnumRegion["FRZll_mumu"]  = 40
#EnumRegion["FRZll_elel"]  = 45

EnumRegion["presel_mu"]   = 0
EnumRegion["preselection_mu_OS_bveto"] = 0  # Hack for v22 region definition
EnumRegion["SR_mu"]       = 1
EnumRegion["loose_SR_mu"] = 2
EnumRegion["FRW_mu"]      = 3
EnumRegion["FRZll_mu"]    = 4
EnumRegion["FRT_mu"]      = 5
EnumRegion["FRQ_mu"]      = 6
EnumRegion["SS_SR_mu"]    = 7
EnumRegion["NRZll_mu"]    = 8
EnumRegion["VRZll_mu"]    = 8
EnumRegion["VRZtt_mu"]    = 9
EnumRegion["CRZtt_mu"]    = 9

EnumRegion["presel_mutaulepe"]   = 10
EnumRegion["SR_mutaulepe"]       = 11
EnumRegion["loose_SR_mutaulepe"] = 12
EnumRegion["FRW_mutaulepe"]      = 13
EnumRegion["FRZll_mutaulepe"]    = 14
EnumRegion["FRT_mutaulepe"]      = 15
EnumRegion["FRQ_mutaulepe"]      = 16
EnumRegion["SS_SR_mutaulepe"]    = 17
EnumRegion["NRZll_mutaulepe"]    = 18

EnumRegion["presel_mutaulepmu"]   = 20
EnumRegion["SR_mutaulepmu"]       = 21
EnumRegion["loose_SR_mutaulepmu"] = 22
EnumRegion["FRW_mutaulepmu"]      = 23
EnumRegion["FRZll_mutaulepmu"]    = 24
EnumRegion["FRT_mutaulepmu"]      = 25
EnumRegion["FRQ_mutaulepmu"]      = 26
EnumRegion["SS_SR_mutaulepmu"]    = 27
EnumRegion["NRZll_mutaulepmu"]    = 28

EnumRegion["presel_mugamma"]    = 30
EnumRegion["SR_mugamma"]        = 31
EnumRegion["loose_SR_mugamma"]  = 32
EnumRegion["CRZtt_mugamma"]     = 33
EnumRegion["CRW_mugamma"]       = 34
EnumRegion["FRW_mugamma"]       = 35
EnumRegion["FRZll_mugamma"]     = 36
EnumRegion["FRT_mugamma"]       = 37
EnumRegion["FRQ_mugamma"]       = 38
EnumRegion["SS_presel_mugamma"] = 39

EnumRegion["presel_el"]   = 50
EnumRegion["preselection_el_OS_bveto"] = 50  # Hack for v22 region definition
EnumRegion["SR_el"]       = 51
EnumRegion["loose_SR_el"] = 52
EnumRegion["FRW_el"]      = 53
EnumRegion["FRZll_el"]    = 54
EnumRegion["FRT_el"]      = 55
EnumRegion["FRQ_el"]      = 56
EnumRegion["SS_SR_el"]    = 57
EnumRegion["NRZll_el"]    = 58
EnumRegion["VRZll_el"]    = 58
EnumRegion["VRZtt_el"]    = 59
EnumRegion["CRZtt_el"]    = 59

EnumRegion["presel_eltaulepmu"]   = 60
EnumRegion["SR_eltaulepmu"]       = 61
EnumRegion["loose_SR_eltaulepmu"] = 62
EnumRegion["FRW_eltaulepmu"]      = 63
EnumRegion["FRZll_eltaulepmu"]    = 64
EnumRegion["FRT_eltaulepmu"]      = 65
EnumRegion["FRQ_eltaulepmu"]      = 66
EnumRegion["SS_SR_eltaulepmu"]    = 67
EnumRegion["NRZll_eltaulepmu"]    = 68

EnumRegion["presel_eltaulepe"]   = 70
EnumRegion["SR_eltaulepe"]       = 71
EnumRegion["loose_SR_eltaulepe"] = 72
EnumRegion["FRW_eltaulepe"]      = 73
EnumRegion["FRZll_eltaulepe"]    = 74
EnumRegion["FRT_eltaulepe"]      = 75
EnumRegion["FRQ_eltaulepe"]      = 76
EnumRegion["SS_SR_eltaulepe"]    = 77
EnumRegion["NRZll_eltaulepe"]    = 78

EnumRegion["presel_elgamma"]    = 80
EnumRegion["SR_elgamma"]        = 81
EnumRegion["loose_SR_elgamma"]  = 82
EnumRegion["CRZtt_elgamma"]     = 83
EnumRegion["CRW_elgamma"]       = 84
EnumRegion["FRW_elgamma"]       = 85
EnumRegion["FRZll_elgamma"]     = 86
EnumRegion["FRT_elgamma"]       = 87
EnumRegion["FRQ_elgamma"]       = 88
EnumRegion["SS_presel_elgamma"] = 89

EnumRegion["truth_muel"] = 90
#-------------------------------------------------------------------------------
EnumVariable = Enumeration()

EnumVariable["cut"] = 0
EnumVariable["NN_comb"]  = 1
EnumVariable["NN_comb_fit"]  = 1
EnumVariable["NN_Ztt"]   = 2
EnumVariable["NN_Wjets"] = 3
EnumVariable["NN_Zll"]   = 4
EnumVariable["NN_data"]  = 5
EnumVariable["NN_comb_coarse"]  = 1
EnumVariable["NN_Ztt_coarse"]   = 2
EnumVariable["NN_Wjets_coarse"] = 3
EnumVariable["NN_Zll_coarse"]   = 4
EnumVariable["NN_data_coarse"]  = 5
EnumVariable["tau_pt"]       = 10
EnumVariable["tau_eta"]      = 11
EnumVariable["tau_phi"]      = 12
EnumVariable["tau_mT"]       = 13
EnumVariable["tau_d0"]       = 14
EnumVariable["tau_a0xy"]     = 15
EnumVariable["tautrack_pt"]  = 20
EnumVariable["tautrack_eta"] = 21
EnumVariable["tautrack_phi"] = 22
EnumVariable["mu_pt"]        = 30
EnumVariable["mu_eta"]       = 31
EnumVariable["mu_phi"]       = 32
EnumVariable["mu_mT"]        = 33
EnumVariable["mu_d0"]        = 34
EnumVariable["mu_a0xy"]      = 35
EnumVariable["el_pt"]        = 40
EnumVariable["el_eta"]       = 41
EnumVariable["el_phi"]       = 42
EnumVariable["el_mT"]        = 43
EnumVariable["el_d0"]        = 44
EnumVariable["el_a0xy"]      = 45
EnumVariable["ph_et"]        = 50
EnumVariable["ph_eta"]       = 51
EnumVariable["ph_phi"]       = 52
EnumVariable["ph_mT"]        = 53
EnumVariable["MET"]          = 60
EnumVariable["MET_phi"]      = 61
EnumVariable["tau_mu_mvis"]         = 70
EnumVariable["tau_mu_mcoll"]        = 71
EnumVariable["tau_mu_MMC"]          = 72
EnumVariable["tau_mu_dAlpha"]       = 73
EnumVariable["tautrack_mu_invmass"] = 74
EnumVariable["tau_el_mvis"]         = 70
EnumVariable["tau_el_mcoll"]        = 71
EnumVariable["tau_el_MMC"]          = 72
EnumVariable["tau_el_dAlpha"]       = 73
EnumVariable["tautrack_el_invmass"] = 74
EnumVariable["mu_2_pt"]       = 80
EnumVariable["mu_2_eta"]      = 81
EnumVariable["mu_2_phi"]      = 82
EnumVariable["mu_mu_invmass"] = 83
EnumVariable["mu_mu_mvis"]    = 83  # The same as invmass?
EnumVariable["mu_mu_MMC"]     = 84
EnumVariable["mu_mu_mcoll"]   = 85
EnumVariable["mu_mu_dAlpha"]  = 86
EnumVariable["el_2_pt"]       = 90
EnumVariable["el_2_eta"]      = 91
EnumVariable["el_2_phi"]      = 92
EnumVariable["el_el_invmass"] = 93
EnumVariable["el_el_mvis"]    = 93  # The same as invmass?
EnumVariable["el_el_MMC"]     = 94
EnumVariable["el_el_mcoll"]   = 95
EnumVariable["el_el_dAlpha"]  = 96
EnumVariable["tau_mu_ph_mvis"]  = 100
EnumVariable["tau_mu_ph_mcoll"] = 101
EnumVariable["mu_ph_mT"]        = 102
EnumVariable["tau_el_ph_mvis"]  = 110
EnumVariable["tau_el_ph_mcoll"] = 111
EnumVariable["el_ph_mT"]        = 112
EnumVariable["el_tau_dEta"]  = 120
EnumVariable["el_ph_dEta"]   = 121
EnumVariable["mu_tau_dEta"]  = 122
EnumVariable["mu_ph_dEta"]   = 123
EnumVariable["tau_ph_dEta"]  = 124
EnumVariable["el_met_dEta"]  = 125
EnumVariable["mu_met_dEta"]  = 126
EnumVariable["tau_met_dEta"] = 127
EnumVariable["el_el_dEta"]   = 128
EnumVariable["mu_mu_dEta"]   = 129
EnumVariable["el_tau_dPhi"]  = 130
EnumVariable["el_ph_dPhi"]   = 131
EnumVariable["mu_tau_dPhi"]  = 132
EnumVariable["mu_ph_dPhi"]   = 133
EnumVariable["tau_ph_dPhi"]  = 134
EnumVariable["el_MET_dPhi"]  = 135
EnumVariable["mu_MET_dPhi"]  = 136
EnumVariable["tau_MET_dPhi"] = 137
EnumVariable["el_el_dPhi"]   = 138
EnumVariable["mu_mu_dPhi"]   = 139
EnumVariable["el_tau_dR"]    = 140
EnumVariable["el_ph_dR"]     = 141
EnumVariable["mu_tau_dR"]    = 142
EnumVariable["mu_ph_dR"]     = 143
EnumVariable["tau_ph_dR"]    = 144
EnumVariable["el_met_dR"]    = 145
EnumVariable["mu_met_dR"]    = 146
EnumVariable["tau_met_dR"]   = 147
EnumVariable["el_el_dR"]     = 148
EnumVariable["mu_mu_dR"]     = 149
EnumVariable["reco_Z_pT_mumu"]     = 150
EnumVariable["reco_Z_pT_mm"]     = 150
EnumVariable["reco_Z_pT_ee"]       = 151
EnumVariable["reco_Z_pT_mutauMET"] = 152
EnumVariable["reco_Z_pT_etauMET"]  = 153
EnumVariable["truth_Z_pT"]         = 159

EnumVariable["n_taus_baseline"]      = 901
EnumVariable["n_taus"]               = 902
EnumVariable["n_muons_baseline"]     = 903
EnumVariable["n_muons"]              = 904
EnumVariable["n_muons_non_iso"]      = 905
EnumVariable["n_electrons_baseline"] = 906
EnumVariable["n_electrons"]          = 907
EnumVariable["n_electrons_non_iso"]  = 908
EnumVariable["n_photons_baseline"]   = 909
EnumVariable["n_photons"]            = 910
EnumVariable["n_jets"]               = 911
EnumVariable["n_bjets"]              = 912
EnumVariable["avg_mu"]               = 913

