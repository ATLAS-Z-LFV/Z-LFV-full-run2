import os, subprocess, shlex, inspect
from itertools import cycle
from array import array
from collections import OrderedDict, Callable
from multiprocessing import cpu_count

from ROOT import gROOT, gErrorIgnoreLevel
from ROOT import TFile, TTree

#-------------------------------------------------------------------------------
### File utilities
#-------------------------------------------------------------------------------
class writeOnFile(object):
    def __init__(self, file_name, mode="w+", echo=False):
        self.file = open(file_name, mode)
        self.echo = echo
    def write(self, string):
        self.file.write("{}\n".format(string))
        if self.echo:
            print string
    def __enter__(self):
        return self.write
    def __exit__(self, type, value, traceback):
        self.file.close()

#-------------------------------------------------------------------------------
### ROOT/PyROOT utilities
#-------------------------------------------------------------------------------
### Terry: Somehow rootpy doesn't work happily in LCG gcc62 versions...(?)
### Perhaps there are ways, but these workarounds are also good enough:

#from rootpy.io import root_open
class root_open(TFile):
    def __enter__(self):
        return self
    def __exit__(self, type, value, traceback):
        if self and self.IsOpen():
            self.Close()

#-------------------------------------------------------------------------------
class buffer(object):
    # Convert 'TTree' type codes to 'array' type codes
    convert = {'F':'f', # float
               'D':'d', # double
               'I':'l', # 32-bit signed int
               'i':'L', # 32-bit unsigned int
              }
    def __init__(self, type_code):
        self.address = array(buffer.convert[type_code], [0])
    def set_value(self, value):
        self.address[0] = value

#-------------------------------------------------------------------------------
class root_tree(TTree):
    def __init__(self, name, title=""):
        super(root_tree, self).__init__(name, title)
        super(root_tree, self).__setattr__('buffers', {})
    def __setattr__(self, name, value):
        if name in self.buffers:
            self.buffers[name].set_value(value)
        else:
            super(root_tree, self).__setattr__(name, value)
    def fill(self):
        self.Fill()
    def write(self):
        self.Write()
    def create_branches(self, branches):
        for name in branches:
            type_code = branches[name]
            self.buffers[name] = buffer(type_code)
            self.Branch(name, self.buffers[name].address, "{}/{}".format(name, type_code))

#-------------------------------------------------------------------------------
def setRootVerbosityLevel(level="Error"):
    if isinstance(level, str):
        gROOT.ProcessLine("{gErrorIgnoreLevel = k%s;}" % level)
    elif isinstance(level, int):
        gROOT.ProcessLine("{gErrorIgnoreLevel = %d;}" % level)
    else:
        print "WARNING: Don't know how to set ROOT verbosity level to '{}'".format(level)

#-------------------------------------------------------------------------------
# It is sometimes convenient to use a context manager
class rootVerbosityLevel(object):
    def __init__(self, level):
        self.level = level
        x = gROOT.GetGlobal("gErrorIgnoreLevel")
        self.original_level = x.__get__(x)
    def __enter__(self):
        setRootVerbosityLevel(self.level)
    def __exit__(self, type, value, traceback):
        setRootVerbosityLevel(self.original_level)

#-------------------------------------------------------------------------------
def saveRootObjects(objects, file_name, file_mode="UPDATE", option=""):
    with root_open(file_name, file_mode) as f:
        for obj in objects:
            f.WriteTObject(obj, obj.GetName(), option)

#-------------------------------------------------------------------------------
### OS path utilities
#-------------------------------------------------------------------------------
def expandPath(path):
    return os.path.expanduser(os.path.expandvars(path))

#-------------------------------------------------------------------------------
def checkMakeDirs(dir):
    if dir == "": return
    dir = expandPath(dir)
    if not os.path.exists(dir):
        os.makedirs(dir)
    elif not os.path.isdir(dir):
        raise NotADirectoryError("Cannot create directory '{}' since the path exists and is not a directory!".format(dir))

#-------------------------------------------------------------------------------
def checkMakeFile(file):
    # Maybe you wanted to make a directory?
    if file.endswith('/'):
        checkMakeDirs(file)
        return
    # Create intermediate directories
    dir, file = os.path.split(file)
    try:
        checkMakeDirs(dir)
    except NotADirectoryError:
        raise Exception("Cannot create file '{}' since the path '{}' exists and is not a directory!".format(file, dir))

#-------------------------------------------------------------------------------
# I am tired of having to type things like "-I configs/_inputs_tmp.yaml" everytime
# Now we can just set environment variables like "LFV_INPUTS=configs/_inputs_tmp.yaml"
def findDefaultConfigs(args, default_split_inputs=False):
    if hasattr(args, "inputs_db") and not args.inputs_db:
        if default_split_inputs:
            args.inputs_db = find(os.environ, "LFV_INPUTS_SPLIT", "configs/inputs_split.yaml")
        else:
            args.inputs_db = find(os.environ, "LFV_INPUTS", "configs/inputs.yaml")
    if hasattr(args, "regions_db") and not args.regions_db:
        args.regions_db = find(os.environ, "LFV_REGIONS", "configs/regions.yaml")
    if hasattr(args, "histograms_db") and not args.histograms_db:
        args.histograms_db = find(os.environ, "LFV_HISTOGRAMS", "configs/histograms.yaml")
    if hasattr(args, "systematics_db") and not args.systematics_db:
        args.systematics_db = find(os.environ, "LFV_SYSTEMATICS", "configs/systematics.yaml")
    if hasattr(args, "plots_db") and not args.plots_db:
        args.plots_db = find(os.environ, "LFV_PLOTS", "configs/plots.yaml")
    if hasattr(args, "thinning_db") and not args.thinning_db:
        args.thinning_db = find(os.environ, "LFV_THINNING", "configs/thinning.yaml")

#-------------------------------------------------------------------------------
### Processes utilities
#-------------------------------------------------------------------------------
def execute(command, log=None, shell=False):
    if shell:
        args = command
    else:
        args = shlex.split(command)
    if log is None:
        return subprocess.check_call(args, stderr=subprocess.STDOUT, shell=shell)
    else:
        checkMakeFile(log)
        process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=shell)
        subprocess.check_call(["tee", log], stdin=process.stdout)
        process.wait()
        return process.returncode

#-------------------------------------------------------------------------------
def checkCpuCount(n_threads):
    if cpu_count() < n_threads:
        print "WARNING: You wanted to use {} threads for multiprocessing, but there are only {} on this machine!".format(n_threads, cpu_count())
        n_threads = cpu_count()
    return n_threads

#-------------------------------------------------------------------------------
# Yep... I love context manager
class acquire(object):
    def __init__(self, lock):
        self.lock = lock
    def __enter__(self):
        if self.lock is not None:
            self.lock.acquire()
    def __exit__(self, type, value, traceback):
        if self.lock is not None:
            self.lock.release()

#-------------------------------------------------------------------------------
### Misc. utilities
#-------------------------------------------------------------------------------
def flattenCommaSeparatedLists(l, delimiter=','):
    # A list of strings, where each string can be a comma-separated list itself
    l = [x for s in l for x in s.split(delimiter)]
    return l

#-------------------------------------------------------------------------------
def find(d, key, default=None):
    # This slightly improves readability when things have long names
    return d[key] if key in d else default

#-------------------------------------------------------------------------------
def altReplace(string, old_pattern, new_patterns):
    # Probably obvious, but this does NOT mutate the original string
    if not hasattr(new_patterns, '__iter__'):
        new_patterns = [new_patterns]
        # Strings don't have '__iter__', which is good!
        # Otherwise we will be looping over characters when new_patterns is just a string!
        # If you really want to loop over characters, use e.g. ['f','o','o'] instead of 'foo'
    l = string.split(old_pattern)
    new_pattern = cycle(new_patterns)
    new_string = l[0]
    for s in l[1:]:
        new_string += next(new_pattern) + s
    return new_string

#-------------------------------------------------------------------------------
class DefaultOrderedDict(OrderedDict):
    # Source: http://stackoverflow.com/a/6190500/562769
    def __init__(self, default_factory=None, *a, **kw):
        if (default_factory is not None and not isinstance(default_factory, Callable)):
            raise TypeError('first argument must be callable')
        OrderedDict.__init__(self, *a, **kw)
        self.default_factory = default_factory

    def __getitem__(self, key):
        try:
            return OrderedDict.__getitem__(self, key)
        except KeyError:
            return self.__missing__(key)

    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError(key)
        self[key] = value = self.default_factory()
        return value

    def __reduce__(self):
        if self.default_factory is None:
            args = tuple()
        else:
            args = self.default_factory,
        return type(self), args, None, None, self.items()

    def copy(self):
        return self.__copy__()

    def __copy__(self):
        return type(self)(self.default_factory, self)

    def __deepcopy__(self, memo):
        import copy
        return type(self)(self.default_factory,
                          copy.deepcopy(self.items()))

    def __repr__(self):
        return 'OrderedDefaultDict(%s, %s)' % (self.default_factory, OrderedDict.__repr__(self))
