from Regions import Region, Cut

from configWriter import Sample
from systematic import Systematic

from ROOT.TColor import GetColor

#-------------------------------------------------------------------------------
def getBackgroundSampleList(inputs, NN_tag=None, apply_ZWeight=True):
    backgrounds = []
    for b in inputs.bkgs:
        sample = Sample(b, GetColor(*inputs[b].colour))
        sample.setStatConfig(True)
        sample.friendTreeName = "weights"
        sample.normByTheory = True  # Needed for minos to work; lumi error should be set to extremely small (1e-5)
        for f in inputs[b].files.values():
            friends = []
            if b in ["Zll","Ztautau"] and apply_ZWeight:
                friends.append(("{}.ZWeights.friend".format(f.name), "ZWeights_{}".format(f.tree)))
            if NN_tag:
                friends.append(("{}.NN.{}.friend".format(f.name, NN_tag), "NN_{}".format(f.tree)))
            #friends.append(("{}.aux.friend".format(f.name), "aux_{}".format(f.tree)))
            sample.addInput(f.name, f.tree, friends=friends)

        ### ### HACK HACK HACK
        ### # Remove Wjets events with high weights
        ### if b == "Wjets":
        ###     sample.additionalCuts = "1.0944*(fabs(normWeight*mcWeight*pileupWeight_NOMINAL*sherpaWeight)<0.000468)"
        ### ###

        backgrounds.append(sample)

    return backgrounds

#-------------------------------------------------------------------------------
def getSignalSampleDict(inputs, NN_tag=None, apply_ZWeight=True, add_dummy_POI=False):
    signals = {}
    for s in inputs.signals:
        sample = Sample(s, GetColor(*inputs[s].colour))
        sample.setStatConfig(True)
        sample.friendTreeName = "weights"
        for f in inputs[s].files.values():
            friends = []
            if apply_ZWeight:
                friends.append(("{}.ZWeights.friend".format(f.name), "ZWeights_{}".format(f.tree)))
            if NN_tag:
                friends.append(("{}.NN.{}.friend".format(f.name, NN_tag), "NN_{}".format(f.tree)))
            #friends.append(("{}.aux.friend".format(f.name), "aux_{}".format(f.tree)))
            sample.addInput(f.name, f.tree, friends=friends)
        if add_dummy_POI:
            sample.setNormFactor("dummyPOI", 1, 1, 1)
        signals[s] = sample

    return signals

#-------------------------------------------------------------------------------
def getDataSample(inputs, NN_tag=None):
    data = Sample("data", 1)  # 1=Black
    data.setOverrideTreename("data_NOMINAL")
    data.setData()
    for f in inputs.data.files.values():
        friends = []
        if NN_tag:
            friends.append(("{}.NN.{}.friend".format(f.name, NN_tag), "NN_data_NOMINAL"))
        #friends.append(("{}.aux.friend".format(f.name), "aux_{}".format(f.tree)))
        data.addInput(f.name, "", friends=friends)

    return data

#-------------------------------------------------------------------------------
def getFakesSample(inputs, NN_tag=None):
    fakes = Sample("fakes", GetColor(*inputs.fakes.colour))
    fakes.setStatConfig(True)
    fakes.friendTreeName = "weights"
    fakes.isFakes = True
    for f in inputs.fakes.files.values():
        friends = []
        if NN_tag:
            friends.append(("{}.NN.{}.friend".format(f.name, NN_tag), "NN_{}".format(f.tree)))
        #friends.append(("{}.aux.friend".format(f.name), "aux_{}".format(f.tree)))
        friends.append(("{}.FF.friend".format(f.name), "FF_{}".format(f.tree)))
        fakes.addInput(f.name, f.tree, friends=friends)

    return fakes

#-------------------------------------------------------------------------------
def addTruthSelection(channel, truth_selection="tau_0_truth_isEle || tau_0_truth_isMuon || tau_0_truth_isTau"):
    if not isinstance(truth_selection, Cut):
        truth_selection = Cut(truth_selection)
    for s in channel.sampleList:
        if s.isData or s.isFakes:
            continue
        if s.additionalCuts == "":
            s.additionalCuts = str(truth_selection)
        else:
            s.additionalCuts = str(truth_selection*s.additionalCuts)
    return

#-------------------------------------------------------------------------------
def setFakesWeights(sample):
    # This is a hack to HistFitter
    # HistFitter simply combine the list of weights by "*".join(weights)
    # What we want is: 'w1 * w2 * ...' --> '((lbNumber>0) - (lbNumber==0) * (w1 * w2 * ...)) * FF_NOMINAL'
    # This string evaluates to FF for data and -FF*(w1 * w2 * w3) for MCs!
    # We give the entire first parenthesis as one single string (we won't vary those weights for fakes, so don't worry about replacing individual weights)

    weight = "((lbNumber>0)-(lbNumber==0)*{})".format("*".join(sample.weights))
    sample.weights = [weight, "FF_NOMINAL"]

    return


#-------------------------------------------------------------------------------
def useRegionTrees(channel, region):
    if isinstance(region, Region):
        tree_region = region.split_tree_region
    else:
        tree_region = region

    for s in channel.sampleList:
        new_input_files = set()
        for t in s.input_files:
            if "data" in t._treename:
                #print "Using region tree: {} --> {}".format(t._treename, "data_{}_NOMINAL".format(region))
                t._treename = "data_{}_NOMINAL".format(tree_region)
            else:
                #print "Using region tree: {} --> {}".format(t._treename, "{}_{}".format(t._treename, region))
                t._treename = "{}_{}".format(t._treename, tree_region)

            for fd in [x for x in t.friends]:
                if "data" in fd._treename:
                    # "data_NOMINAL" -> "data" + tree_region + "_NOMINAL"
                    #print "Using region tree: {} --> {}".format(fd._treename, fd._treename[:-7] + tree_region + fd._treename[-8:])
                    fd._treename = fd._treename[:-7] + tree_region + fd._treename[-8:]
                else:
                    #print "Using region tree: {} --> {}".format(fd._treename, "{}_{}".format(fd._treename, tree_region))
                    fd._treename = "{}_{}".format(fd._treename, tree_region)

            new_input_files.add(t)
        s.input_files = new_input_files
    return

#-------------------------------------------------------------------------------
def addChannel(fitConfig, var, region, lumi, blind=True, signal=None, region_name=None, use_MC_fakes=False, use_full_trees=False, apply_ZWeight=True, apply_ZTheory=True, v22=False):
    if region_name is None:
        region_name = region.name

    branch = var.branch
    nBins = var.nBins
    min = var.min
    max = var.max
    ### HACK HACK HACK
    if var.name in ["NN_comb_fit", "NN_comb_lowNNFit", "NN_comb_fit_coarse"]:
        if "_1P" in region_name:
            min = 0.10
        if "_3P" in region_name:
            min = 0.20
        nBins = int(round((max - min) / var.binSize))
    try:
        channel = fitConfig.addChannel(branch, [region_name], nBins, min, max)
    except RuntimeError as err:
        # Probably complaining about adding channels that already exist
        # This could happen when a fit channel is also a validation channel
        print "WARNING: {}".format(err)
        print "         Ignored channel. This is ok if you are adding a validation channel that is also a fit/control channel."
        return None
    channel.blind = blind
    channel.useOverflowBin = var.merge_overflow
    channel.useUnderflowBin = var.merge_underflow

    channel.addWeight(str(lumi))
    channel.addWeight("normWeight")
    for w in region.weights:
        channel.addWeight(w)

    if not use_MC_fakes:
        addTruthSelection(channel, region.truth_selection)  # before adding fakes and signal!
        setFakesWeights(channel.getSample("fakes"))  # overrides default weights

    if signal is not None:
        channel.addSample(signal)
        if apply_ZWeight:
            if v22:
                channel.getSample(signal.name).weights.append("ZWeight")
            else:
                channel.getSample(signal.name).weights.append("ZWeight_NOMINAL")

    if apply_ZWeight:
        if v22:
            channel.getSample("Zll").weights.append("ZWeight")
        else:
            channel.getSample("Zll").weights.append("ZWeight_NOMINAL")
            channel.getSample("Ztautau").weights.append("ZWeight_NOMINAL")
            if "_mu_1P" in region_name:
                channel.getSample("Zll").weights.append("muTauFakesSF_NOMINAL")
            if "_el_1P" in region_name:
                channel.getSample("Zll").weights.append("elTauFakesSF_NOMINAL")

    if apply_ZTheory:
        pass
        #channel.getSample("Ztautau").weights.append("theory_weights_PDF_central_value")
        #channel.getSample("Ztautau").weights.append("theory_weights_LHE3Weight_MUR1_MUF1_PDF261000")

    ### ### HACK HACK HACK
    ### if "_el" in region_name:
    ###     try:
    ###         channel.getSample("Zll").weights.remove("tau_0_SF_idSF_NOMINAL")
    ###     except:
    ###         pass

    if not use_full_trees:
        useRegionTrees(channel, region)

    return channel

#-------------------------------------------------------------------------------
def addSystematics(fitConfigs, systematics, verbose):
    # LOOP LOOP LOOP...
    for fit in fitConfigs:
        for c in fit.channels:
            region = c.regionString
            for s in c.sampleList:
                sample = s.name

                # Weight-type systematics
                for syst in systematics.getList(region, sample, "weight"):
                    if verbose:
                        print "Sample {} in channel {} is affected by 'weight' systematic {}".format(sample, region, syst.name)
                    HF_syst = Systematic(syst.name, s.weights, syst.up_weights(s.weights), syst.down_weights(s.weights), "weight", "overallHistoSys")
                    s.addSystematic(HF_syst)

                # Tree-type systematics
                for syst in systematics.getList(region, sample, "tree"):
                    if verbose:
                        print "Sample {} in channel {} is affected by 'tree' systematic {}".format(sample, region, syst.name)
                    HF_syst = Systematic(syst.name, "", syst.treesuffixes['up'], syst.treesuffixes['down'], "tree", "overallHistoSys")
                    s.addSystematic(HF_syst)

                # Custom-type systematics
                for syst in systematics.getList(region, sample, "flat"):
                    if verbose:
                        print "Sample {} in channel {} is affected by 'flat' systematic {}".format(sample, region, syst.name)
                    errors = syst.errors(region, sample)
                    s.addOverallSys(syst.name, 1.+errors[0], 1.-errors[1])

    return

#-------------------------------------------------------------------------------
def removeSystNorm(fitConfigs, verbose):
    # LOOP LOOP LOOP...
    for fit in fitConfigs:
        for c in fit.channels:
            region = c.regionString
            for s in c.sampleList:
                if len(s.normFactor) == 0:
                    continue
                to_be_removed = []
                for syst in s.systDict:
                    # keep NPs that affect 1P and 3P taus differently
                    if "mu_Z" in [nf[0] for nf in s.normFactor] and \
                       (syst.startswith("TAUS_") or "1P" in syst or "3P" in syst):
                        continue
                    if verbose:
                        print "Removing normalisation component of systematic '{}' on sample '{}' in channel '{}'".format(syst, s.name, c.name)
                    if s.systDict[syst].method == "overallHistoSys":
                        s.systDict[syst].method = "histoSys"
                    elif s.systDict[syst].method == "overallSys":
                        to_be_removed.append(syst)
                for syst in to_be_removed:
                    if verbose:
                        print"Removing systematic '{}' on sample '{}' in channel '{}'".format(syst, s.name, c.name)
                    s.removeSystematic(syst)
    return

### #-------------------------------------------------------------------------------
### # HACK: scale factors for mu->tau fakes
### def applyMuTauFakesSFs(channel):
###     channel.getSample("Zll").weights.append("((taus_pt[0]<40)*(1.15 + (fabs(taus_eta[0])<0.5)*0.51 + (fabs(taus_eta[0])>2.0)*0.16) + (taus_pt[0]>=40)*(0.94 + (fabs(taus_eta[0])<0.5)*(-0.07) + (fabs(taus_eta[0])>2.0)*0.38))")
