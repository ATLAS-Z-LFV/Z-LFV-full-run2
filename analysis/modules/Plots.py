import os
import yaml
from general_utils import find

#-------------------------------------------------------------------------------
# Class that loads plots.yaml into a dict of list of variables
class Plots(dict):
    def __init__(self, file_name=None):
        if file_name is None:
            file_name = find(os.environ, "LFV_PLOTS", "configs/plots.yaml")

        with open(file_name, "r") as f:
            file = yaml.load(f)
        for region in file:
            self[region] = list(set(x for l in file[region].values() for x in l))
