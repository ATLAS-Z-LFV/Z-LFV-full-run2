#!/usr/bin/env python

import os, sys
import argparse
from collections import OrderedDict

from ROOT import gROOT

from modules.Inputs import Inputs
from modules.Regions import Regions
from modules.general_utils import findDefaultConfigs, checkMakeDirs, flattenCommaSeparatedLists
from modules.cutflow_utils import *

#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser("./makeCutflowTable.py")
    user = os.environ['USER'] if 'USER' in os.environ else '.'

    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")

    parser.add_argument("--output-dir", "-o", default="/tmp/{}/cutflow".format(user), type=str, help="Output directory for writing the TeX file. Default = '/tmp/{}/cutflow'".format(user))
    parser.add_argument("--force-recreate", "-f", default=False, action="store_true", help="Force recreation of json cache files?")
    parser.add_argument("--cache-file", default=None, type=str, help="Non-default cache file name.")
    parser.add_argument("--no-compile", default=False, action="store_true", help="Do not compile the tex file into PDF file.")

    parser.add_argument("--region", "-r", dest='regions', default=[], action='append', type=str, help="Regions where we want to see the cutflows.")

    parser.add_argument("--unweighted", default=False, action="store_true", help="Use raw unweighted numbers?")
    parser.add_argument("--data", "-D", default=False, action="store_true", help="Make cutflow of data.")
    parser.add_argument("--lumi", default=None, type=float, help="Integrated lumi in fb-1")
    parser.add_argument("--temporary-cuts", default=False, action="store_true", help="Ignore cuts that have been marked 'ignore_temporarily' in the YAML file.")
    parser.add_argument("--alt-bkgs", default=False, action="store_true", help="Use alternative background samples.")

    parser.add_argument("--alias", "-a", dest="aliases", default=[], action='append', type=str, help="Branch alias. Assuming format 'name:=formula'. E.g. 'abseta:=fabs(eta)'.")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args)

        args.regions = flattenCommaSeparatedLists(args.regions)
        args.aliases = flattenCommaSeparatedLists(args.aliases)

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))

    if len(args.regions) == 0:
        raise Exception("You haven't specified any regions")

    checkMakeDirs(args.output_dir)

#-------------------------------------------------------------------------------
def makeCutflowTable(args):

    gROOT.SetBatch(True)

    inputs = Inputs(args.inputs_db)
    regions = Regions(args.regions_db)

    if not args.cache_file:
        args.cache_file = "cutflow_cache"
        if args.unweighted:
            args.cache_file += "_unweighted"
        args.cache_file += "__" + "_".join(args.regions)
        args.cache_file = os.path.join(args.output_dir, args.cache_file)
        args.cache_file += ".json"

    cached = False
    if not os.path.exists(args.cache_file):
        print "Will create cache file '{}'".format(args.cache_file)
    elif args.force_recreate:
        print "WARNING: Will recreate existing cache file '{}'".format(args.cache_file)
    else:
        print "Will read from cache file '{}'".format(args.cache_file)
        cached = True

    if args.lumi is not None:
        lumi = args.lumi  # TODO: needs to be stored in the json
    elif 'data' in inputs and len(inputs['data']) != 0:
        lumi = inputs.lumi
    else:
        raise Exception("No lumi specified and no data found in inputs-db!")

    # Delete unwanted regions, cuts and inputs before passing them to buildCutFlow()
    for r in args.regions:
        reg = regions[r]
        for c in reg.cuts.keys():
            if args.temporary_cuts and reg.cuts[c].ignore_temporarily:
                reg.deleteCut(c)
    for r in regions.keys():
        if r not in args.regions:
            regions.delete(r)

    inputs.useAlt(args.alt_bkgs)
    signals = set(s for r in regions.values() for s in r.signals)
    for s in inputs.signals.keys():
        if s not in signals:
            inputs.delete(s)
    if not args.data:
        inputs.delete("data")
    inputs.delete("fakes")

    if not cached:
        data = buildCutflow(inputs, regions, lumi, args.unweighted, args.data, args.aliases)
        writeCacheFile(args.cache_file, data)
    else:
        data = readCacheFile(args.cache_file)

    output_name = "cutflow"
    if args.unweighted:
        output_name += "_unweighted"
    output_name += "__" + "_".join(args.regions)
    output_name = os.path.join(args.output_dir, output_name)
    output_name += ".tex"

    latex = writeCutflowTable(data, output_name, args.data)
    if not args.no_compile:
        latex.compilePdf()

    print "Done!!"

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    makeCutflowTable(args)
