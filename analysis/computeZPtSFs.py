#!/usr/bin/env python

import os, sys
import argparse
from copy import copy
from array import array

from modules.Inputs import Inputs
from modules.Regions import Regions
from modules.general_utils import findDefaultConfigs, checkMakeFile
from modules.general_utils import setRootVerbosityLevel, root_open
from modules.plot_utils import mergeOverflows, removeHistErrors, inheritOverflows

from ROOT import *
TH1D.SetDefaultSumw2()
setRootVerbosityLevel("Error")

#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser("./computeZPtSFs.py")

    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")
    parser.add_argument("--output-file", "-o", default="ZWeights/fiducial_Z_pT_SFs.root", type=str, help="Output file name.")
    parser.add_argument("--Zll-file", default="", type=str, help="Name of the Zll ntuple file. Skip computing Zll SFs if empty.")
    parser.add_argument("--Ztt-file", default="", type=str, help="Name of the Ztautau ntuple file. Skip computing Ztautau SFs if empty.")
    parser.add_argument("--sig-file", default="", type=str, help="Name of the signal ntuple file. Skip computing signal SFs if empty.")

    parser.add_argument("--force-recreate", "-f", action='store_true', default=False, help="Force recreation of histograms.")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args)

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))

    checkMakeFile(args.output_file)

#-------------------------------------------------------------------------------
def computeZPtSFs(args):
    inputs = Inputs(args.inputs_db)
    regions = Regions(args.regions_db)

    #binning = array('d', [0., 4., 8., 12., 16., 20., 25., 30., 36., 42., 51., 65., 80., 105., 150.])
    binning = array('d', [0., 2., 4., 6., 8., 10., 12., 16., 20., 25., 30., 36., 42., 48., 57, 70., 85., 125.])

    f = TFile(args.output_file, "UPDATE")

    #-------------------------------------------------
    # Fiducial measurement, Ref. https://cds.cern.ch/record/2693665/files/STDM-2018-14-002.pdf
    bin_edges = array('d', [0., 2., 4., 6., 8., 10., 12., 14., 16., 18., 20., 22.5, 25., 27.5, 30., 33., 36., 39., 42., 45., 48., 51., 54., 57., 61., 65., 70., 75., 80., 85., 95., 105., 125., 150., 175., 200., 250., 300., 350., 400., 470., 550., 650., 900., 2500.])
    val = [0.048400, 0.102000, 0.106000, 0.094700, 0.081200, 0.068700, 0.058300, 0.049600, 0.042500, 0.036700, 0.039000, 0.033000, 0.028000, 0.023900, 0.024300, 0.020300, 0.017300, 0.014600, 0.012500, 0.010600, 0.008900, 0.007640, 0.006580, 0.007490, 0.006380, 0.006760, 0.005660, 0.004790, 0.004080, 0.006540, 0.004840, 0.006590, 0.004660, 0.002630, 0.001540, 0.001530, 0.000615, 0.000295, 0.000137, 0.000088, 0.000043, 0.000020, 0.000013, 0.000002]
    err = [0.240000, 0.100000, 0.090000, 0.100000, 0.120000, 0.140000, 0.160000, 0.180000, 0.200000, 0.210000, 0.180000, 0.200000, 0.230000, 0.250000, 0.230000, 0.270000, 0.300000, 0.320000, 0.330000, 0.360000, 0.400000, 0.440000, 0.490000, 0.410000, 0.450000, 0.420000, 0.460000, 0.530000, 0.600000, 0.350000, 0.450000, 0.300000, 0.390000, 0.600000, 0.900000, 0.770000, 1.600000, 2.600000, 4.200000, 5.300000, 9.000000, 15.000000, 19.000000, 77.000000]
    h_measured = TH1D("measured_Z_pT", "measured_Z_pT", len(val), bin_edges)
    for i in xrange(len(val)):
        h_measured.SetBinContent(i+1, val[i])
        h_measured.SetBinError(i+1, val[i]*err[i]*0.01)
    writeObjects(f, h_measured, args.force_recreate)

    h_measured_rebinned = h_measured.Rebin(len(binning)-1, "measured_Z_pT_rebinned", binning)
    h_measured_rebinned.SetTitle("measured_Z_pT_rebinned")
    mergeOverflows(h_measured_rebinned)
    writeObjects(f, h_measured_rebinned, args.force_recreate)

    #-------------------------------------------------
    # Zll
    if args.Zll_file:
        print "Getting expected truth pT(Z) distribution of Zll..."
        h_name = "Zll_truth_Z_pT"
        if h_name in f.GetListOfKeys() and not args.force_recreate:
            print "  Using existing histogram. (Use --force-recreate to fill from ntuples.)"
            h_Zll = f.Get(h_name)
        else:
            print "  Filling histogram from ntuples..."
            h_Zll = getTruthZPtHist(h_name, (len(binning)-1,binning), args.Zll_file, "Zll_NOMINAL")
            writeObjects(f, h_Zll, args.force_recreate)

        h_name += "_SFs"
        h_Zll_SFs = h_measured_rebinned.Clone(h_name)
        h_Zll_SFs.SetTitle(h_name)
        h_Zll_SFs.Divide(h_Zll)
        inheritOverflows(h_Zll_SFs)  # Overflow bin = last bin (they were merged)
        h_Zll_SFs.SetBinContent(0, 1.)  # Underflow = failed to obtain truth Z pT; set SF = 1.
        h_Zll_SFs.SetBinError(0, 0.)
        writeObjects(f, h_Zll_SFs, True)

    #-------------------------------------------------
    # Ztautau
    if args.Ztt_file:
        print "Getting expected truth pT(Z) distribution of Ztautau..."
        h_name = "Ztautau_truth_Z_pT"
        if h_name in f.GetListOfKeys() and not args.force_recreate:
            print "  Using existing histogram. (Use --force-recreate to fill from ntuples.)"
            h_Ztt = f.Get(h_name)
        else:
            print "  Filling histogram from ntuples..."
            h_Ztt = getTruthZPtHist(h_name, (len(binning)-1,binning), args.Ztt_file, "Ztautau_NOMINAL")
            writeObjects(f, h_Ztt, args.force_recreate)

        h_name += "_SFs"
        h_Ztt_SFs = h_measured_rebinned.Clone(h_name)
        h_Ztt_SFs.SetTitle(h_name)
        h_Ztt_SFs.Divide(h_Ztt)
        inheritOverflows(h_Ztt_SFs)  # Overflow bin = last bin (they were merged)
        h_Ztt_SFs.SetBinContent(0, 1.)  # Underflow = failed to obtain truth Z pT; set SF = 1.
        h_Ztt_SFs.SetBinError(0, 0.)
        writeObjects(f, h_Ztt_SFs, True)

    #-------------------------------------------------
    # Signals
    if args.sig_file:
        print "Getting expected truth pT(Z) distribution of signals..."
        h_name = "Ztaulep_truth_Z_pT"
        if h_name in f.GetListOfKeys() and not args.force_recreate:
            print "  Using existing histogram. (Use --force-recreate to fill from ntuples.)"
            h_sig = f.Get(h_name)
        else:
            print "  Filling histogram from ntuples..."
            h_sig = getTruthZPtHist(h_name, (len(binning)-1,binning), args.sig_file, "Ztaulep_NOMINAL")
            writeObjects(f, h_sig, args.force_recreate)

        h_name += "_SFs"
        h_sig_SFs = h_measured_rebinned.Clone(h_name)
        h_sig_SFs.SetTitle(h_name)
        h_sig_SFs.Divide(h_sig)
        inheritOverflows(h_sig_SFs)  # Overflow bin = last bin (they were merged)
        h_sig_SFs.SetBinContent(0, 1.)  # Underflow = failed to obtain truth Z pT; set SF = 1.
        h_sig_SFs.SetBinError(0, 0.)
        writeObjects(f, h_sig_SFs, True)

    #-------------------------------------------------
    print "Done!"
    print "Use 'decorateZPtSFs.py' to decorate the scale factors to n-tuples."

#-------------------------------------------------------------------------------
def getTruthZPtHist(h_name, binning, file, tree):
    gROOT.cd()  # needed for TTree.Project() to find the histogram
    h = TH1D(h_name, h_name, *binning)

    with root_open(file) as f:
        t = f.Get(tree)
        t.AddFriend("weights")
        gROOT.cd()  # needed for TTree.Project() to find the histogram
        t.Project("+"+h_name, "truth_Z_pT", "normWeight*mcWeight")

    mergeOverflows(h)
    h.Scale(1. / h.Integral(1, h.GetNbinsX()))

    return h

#-------------------------------------------------------------------------------
def writeObjects(file, objects, force_recreate=True):
    if not hasattr(objects, '__iter__'):
        objects = [objects]
    file.cd()
    for obj in objects:
        if obj.GetName() in file.GetListOfKeys() and not force_recreate:
            continue
        obj.SetDirectory(file)
        obj.Write("", TObject.kOverwrite)

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    computeZPtSFs(args)
