# ATLAS Z LFV Analysis (full Run-2)

This project contains everything for the ATLAS full Run-2 Z->τ+μ/e analysis.

<BR>

## Ntuple production

If you are on Stoomboot or a machine with cvmfs (e.g. lxplus), simply do:
```
cd ntuple_production
source setup.sh
```
to setup.

See ntuple_production/README.md for further details.

<BR>

## Analysis

If you are on Stoomboot or a machine with cvmfs (e.g. lxplus), simply do:
```
cd analysis
source setup.sh
```
to setup.

See analysis/README.md for further details.

<BR>

## Contact

Contact the analysis group with ```atlas-phys-exotics-lfv-z-with-tau-full-run2@cern.ch```.
