#!/usr/bin/env python

import sys, os, argparse
import json

import numpy as np
np.random.seed(42)

from modules.Regions import Regions
from modules.general_utils import findDefaultConfigs, checkMakeDirs, flattenCommaSeparatedLists
from NN_modules.NNModel import NNModel
from NN_modules.NN_utils import *

#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser('./trainNN_multi.py')
    
    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")
    
    parser.add_argument("--tag", "-t", default="foo", type=str, help="The version tag.")
    parser.add_argument("--input-dir", "-i", default="./inputs", type=str, help="The input directory where the transformed input numpy arrays have been stored.")
    parser.add_argument("--output-dir", "-o", default="./models", type=str, help="The output directory for saving the trained models and predicted results.")
    parser.add_argument("--transformation", "-T", default="", type=str, help="The input transformation scheme.")
    
    parser.add_argument("--samples", "-s", default="Ztautau,Wjets,Zll", type=str, help="A comma-separated list of input samples. Signal samples will be automatically added according to the '--region' option.")
    parser.add_argument("--sample-set-weights", "-w", default=None, type=str, help="A comma-separated list of numbers representing the relative weights of each sample set. Signal samples always have relative weight of one.")
    parser.add_argument("--region", "-r", default="", type=str, help="Region that defines the training sample selection.")
    
    parser.add_argument("--single-output",action='store_true', default=False, help="Single output mode (i.e. only signal vs multiple backgrounds, without background-vs-background outputs).")
    
    parser.add_argument("--nodes", "-N", default=16, type=int, help="The number of nodes per layer.")
    parser.add_argument("--layers", "-L", default=2, type=int, help="The number of hidden layers.")
    parser.add_argument("--epochs", "-E", default=200, type=int, help="The number training epochs.")
    parser.add_argument("--batch", "-B", default=256, type=int, help="The number of training samples per batch.")
    parser.add_argument("--learning-rate", "-lr", default=1e-4, type=float, help="The learning rate of the optimiser (adam).")
    parser.add_argument("--learning-rate-decay", "-d", default=0, type=float, help="The decay rate of the learning rate of the optimiser.")
    parser.add_argument("--drop-out", "-D", default=0, type=float, help="The drop-out probability for the neural network.")
    parser.add_argument("--loss", default="categorical_crossentropy", type=str, help="Loss/objective function used. Default being 'categorical_crossentropy' (or 'binary_crossentropy' in case '--single-output' is used).")
    
    parser.add_argument("--tau-prongs", default=1, choices=[1,3], type=int, help="Select number of tau prongs.")
    parser.add_argument("--drop-var", dest='drop_vars', default=[], action='append', type=str, help="Variable(s) to be dropped from inputs.")
    
    parser.add_argument("--no-history", action='store_true', default=False, help="Train without history.")
    parser.add_argument("--no-testing", action='store_true', default=False, help="Do not call testNN_multi.py at the end.")
    parser.add_argument("--resume-training", action='store_true', default=False, help="Resume training of a previously saved model. (NN architecture will be inherited (including drop-out), while optimiser properties can be changed.)")
    
    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args)
        
        args.samples = args.samples.split(",")
        args.input_dir = os.path.join(args.input_dir, args.tag)
        args.output_dir = os.path.join(args.output_dir, args.tag)
        
        args.drop_vars = flattenCommaSeparatedLists(args.drop_vars)
        
        if args.sample_set_weights is not None:
            args.sample_set_weights = [float(x) for x in args.sample_set_weights.split(',')]
        
        checkArgs(args)
        
    except:
        parser.print_help()
        raise
    
    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))
    
    if not os.path.isdir(args.input_dir):
        raise Exception("Input directory '{}' does not exists (or is not a directory).".format(args.input_dir))
    
    checkMakeDirs(args.output_dir)
    
    if not args.samples:
        raise Exception("No input samples specified.")
    
    if not args.region:
        raise Exception("No input region specified.")
    
    if not args.transformation:
        raise Exception("No input transformation scheme specified.")

#-------------------------------------------------------------------------------
def trainNN_multi(args):
    
    regions = Regions(args.regions_db)
    region = regions[args.region]
    
    backgrounds = args.samples
    signals = region.signals
    
    if args.single_output:
        model_name = "singleMulti_{}_{}P_{}".format(args.region, args.tau_prongs, args.transformation)
    else:
        model_name = "multi_{}_{}P_{}".format(args.region, args.tau_prongs, args.transformation)
    
    #-------------------------------------------------
    # Load
    input = os.path.join(args.input_dir, "{}_%s.%s.npy" % (args.region, args.transformation))
    sig_array = None
    for s in signals:
        print "Loading signal data '{}'...".format(input.format(s))
        if sig_array is None:
            sig_array = np_load(input.format(s))
        else:
            sig_array = np.concatenate([sig_array, np_load(input.format(s))])
    bkg_arrays = []
    for b in backgrounds:
        print "Loading background data '{}'...".format(input.format(b))
        bkg_arrays.append(np_load(input.format(b)))
    
    for bkg_array in bkg_arrays:
        if not bkg_array.dtype.names == sig_array.dtype.names:
            raise Exception("The input numpy arrays do not have the same field names.", bkg_array.dtype.names, sig_array.dtype.names)
    print "The loaded input arrays have the following fields:"
    for f in sig_array.dtype.names:
        print "  {}".format(f)
    
    #-------------------------------------------------
    # Normalise inputs and save the scaling information
    scale_file_name = "{}/{}_{}P_{}.scaling".format(args.output_dir, args.region, args.tau_prongs, args.transformation)
    if os.path.exists(scale_file_name):
        print "Found saved scaling information. Normalising inputs..."
        scale = json_load(scale_file_name)
    else:
        print "Normalising inputs and saving scaling information..."
        data_input = input.format("data")
        print "Loading data array '{}'...".format(data_input)
        if not os.path.exists(data_input):
            raise Exception("Data numpy array '{}' doesn't exist. Cannot perform input normalisation.".format(data_input))
        data_array = np_load(input.format("data"))
        scale = findInputScale([sig_array, data_array])
        json_save(scale_file_name, scale)
        data_array = None  # Make sure memory is freed
    normaliseInputs(bkg_arrays + [sig_array], scale=scale)
    
    #-------------------------------------------------
    # Split according to eventNumber
    print "Preparing samples (splitting according to eventNumber and tau-prongs)..."
    bkgs_odd = []
    bkgs_even = []
    bkgs_odd_odd = []
    bkgs_even_odd = []
    for bkg_array in bkg_arrays:
        odd  = (bkg_array["eventNumber"] & 1).astype(bool)
        even = np.logical_not(odd)
        four_multiples = bkg_array["eventNumber"] & 3
        odd_odd  = np.logical_not(four_multiples - 1)  # 4N+1
        even_odd = np.logical_not(four_multiples)      # 4N
        bkgs_odd.append(bkg_array[odd])
        bkgs_even.append(bkg_array[even])
        bkgs_odd_odd.append(bkg_array[odd_odd])
        bkgs_even_odd.append(bkg_array[even_odd])
    
    odd  = (sig_array["eventNumber"] & 1).astype(bool)
    even = np.logical_not(odd)
    four_multiples = sig_array["eventNumber"] & 3
    odd_odd  = np.logical_not(four_multiples - 1)  # 4N+1
    even_odd = np.logical_not(four_multiples)      # 4N
    sig_odd      = sig_array[odd]
    sig_even     = sig_array[even]
    sig_odd_odd  = sig_array[odd_odd]
    sig_even_odd = sig_array[even_odd]
    
    #-------------------------------------------------
    # Select events according to tau prongs
    for bkgs in [bkgs_odd, bkgs_even, bkgs_odd_odd, bkgs_even_odd]:
        for i,bkg in enumerate(bkgs):
            bkgs[i] = bkg[bkg["taus_n_tracks[0]"]==args.tau_prongs]
    sig_odd      = sig_odd[sig_odd["taus_n_tracks[0]"]==args.tau_prongs]
    sig_even     = sig_even[sig_even["taus_n_tracks[0]"]==args.tau_prongs]
    sig_odd_odd  = sig_odd_odd[sig_odd_odd["taus_n_tracks[0]"]==args.tau_prongs]
    sig_even_odd = sig_even_odd[sig_even_odd["taus_n_tracks[0]"]==args.tau_prongs]
    
    print "Will train on"
    print "  {} ({}) odd (even) {}P signal samples".format(len(sig_odd), len(sig_even), args.tau_prongs)
    for i,sam in enumerate(args.samples):
        print "  {} ({}) {} samples".format(len(bkgs_odd[i]), len(bkgs_even[i]), sam)
    print "and validate on"
    print "  {} ({}) odd (even) {}P signal samples".format(len(sig_even_odd), len(sig_odd_odd), args.tau_prongs)
    for i,sam in enumerate(args.samples):
        print "  {} ({}) {} samples".format(len(bkgs_even_odd[i]), len(bkgs_odd_odd[i]), sam)
    
    #-------------------------------------------------
    # Repeat samples so that all sample sets have roughly the same number of samples
    # Otherwise sample sets with smaller size won't be given enough "attention" by the training
    n_samples = []
    for i in range(len(bkgs_odd)):
        n_samples.append(len(bkgs_odd[i]) + len(bkgs_even[i]))
    n_samples.append(len(sig_odd) + len(sig_even))
    max_n_samples = max(n_samples)
    
    for i in range(len(bkgs_odd)):
        if n_samples[i] == max_n_samples:
            continue
        bkgs_odd[i] = np.repeat(bkgs_odd[i], max_n_samples/n_samples[i])
        bkgs_even[i] = np.repeat(bkgs_even[i], max_n_samples/n_samples[i])
        #bkgs_odd_odd[i] = np.repeat(bkgs_odd_odd[i], max_n_samples/n_samples[i])
        #bkgs_even_odd[i] = np.repeat(bkgs_even_odd[i], max_n_samples/n_samples[i])
    
    if not n_samples[-1] == max_n_samples:
        sig_odd = np.repeat(sig_odd, max_n_samples/n_samples[-1])
        sig_even = np.repeat(sig_even, max_n_samples/n_samples[-1])
        #sig_odd_odd = np.repeat(sig_odd_odd, max_n_samples/n_samples[-1])
        #sig_even_odd = np.repeat(sig_even_odd, max_n_samples/n_samples[-1])
    
    #-------------------------------------------------
    # Combine signal and background, and get labels and weights
    # Tuples of (samples, labels, weights)
    if args.single_output:
        f = getLabelsAndWeights
    else:
        f = getMultiLabelsAndWeights
    
    sam_odd = f(sig_odd, bkgs_odd, args.drop_vars, args.sample_set_weights)
    sam_even = f(sig_even, bkgs_even, args.drop_vars, args.sample_set_weights)
    sam_odd_odd = None
    sam_even_odd = None
    if not args.no_history:
        sam_odd_odd = f(sig_odd_odd, bkgs_odd_odd, args.drop_vars, args.sample_set_weights)
        sam_even_odd = f(sig_even_odd, bkgs_even_odd, args.drop_vars, args.sample_set_weights)
    
    #-------------------------------------------------
    print "Creating NN models..."
    if args.single_output:
        if args.loss == "categorical_crossentropy":
            args.loss = "binary_crossentropy"
        nOutputs = 1
        output_actv = 'sigmoid'
    else:
        nOutputs = len(args.samples)+1
        output_actv = 'softmax'
    
    model_odd = NNModel(sam_odd[0].shape[1], [args.nodes]*args.layers, nOutputs=nOutputs, output_actv=output_actv, loss=args.loss, epochs=args.epochs, batch_size=args.batch, learning_rate=args.learning_rate, lr_decay=args.learning_rate_decay, dropout=args.drop_out)
    if args.resume_training:
        print "Will resume training by loading previously saved model"
        model_odd.load("models/{}/{}.odd.h5".format(args.tag, model_name))
    model_odd.compile()
    
    model_even = model_odd.copy()
    if args.resume_training:
        # This hasn't been tested yet!
        model_even.load("models/{}/{}.even.h5".format(args.tag, model_name))
    model_even.compile()
    
    print "Successfully created NN models."
    
    #-------------------------------------------------
    print "Training NN model (odd)..."
    validation_data = None if args.no_history else sam_even_odd
    history_odd = model_odd.fit(sam_odd[0], sam_odd[1], sample_weight=sam_odd[2], validation_data=validation_data)
    print "Successfully trained NN model (odd). Saving..."
    model_odd.save("models/{}/{}.odd.h5".format(args.tag, model_name))
    
    print "Training NN model (even)..."
    validation_data = None if args.no_history else sam_odd_odd
    history_even = model_even.fit(sam_even[0], sam_even[1], sample_weight=sam_even[2], validation_data=validation_data)
    print "Successfully trained NN model (even). Saving..."
    model_even.save("models/{}/{}.even.h5".format(args.tag, model_name))
    
    #------------------------------
    if not args.no_history:
        print "Making validation plots..."
        
        import matplotlib
        matplotlib.use('Agg')  # No display for running in batch/screen
        import matplotlib.pyplot as plt
        
        # Plot loss
        plt.plot(history_odd.history['loss'])
        plt.plot(history_odd.history['val_loss'])
        plt.plot(history_even.history['loss'])
        plt.plot(history_even.history['val_loss'])
        axes = plt.gca()
        axes.set_ylim([0, axes.get_ylim()[1]])  # force y-range to start at zero
        plt.legend(['training 1', 'testing 1', 'training 2', 'testing 2'], loc=1)
        plt.xlabel('Epoch', horizontalalignment='right', x=1.0)
        plt.ylabel('Loss', horizontalalignment='right', y=1.0)
        #ax = plt.gca()
        #ax.set_yscale("log")
        
        checkMakeDirs("plots/{}".format(args.tag))
        plt.savefig("plots/{}/{}.loss.png".format(args.tag, model_name))
        plt.savefig("plots/{}/{}.loss.pdf".format(args.tag, model_name))
        plt.savefig("plots/{}/{}.loss.eps".format(args.tag, model_name))
        plt.close()
        
        # Plot accuracy
        plt.plot(history_odd.history['acc'])
        plt.plot(history_odd.history['val_acc'])
        plt.plot(history_even.history['acc'])
        plt.plot(history_even.history['val_acc'])
        axes = plt.gca()
        axes.set_ylim([0, 1])  # force y-range
        plt.legend(['training 1', 'testing 1', 'training 2', 'testing 2'], loc=4)
        plt.xlabel('Epoch', horizontalalignment='right', x=1.0)
        plt.ylabel('Accuracy', horizontalalignment='right', y=1.0)
        #ax = plt.gca()
        #ax.set_yscale("log")
        
        checkMakeDirs("plots/{}".format(args.tag))
        plt.savefig("plots/{}/{}.acc.png".format(args.tag, model_name))
        plt.savefig("plots/{}/{}.acc.pdf".format(args.tag, model_name))
        plt.savefig("plots/{}/{}.acc.eps".format(args.tag, model_name))
        plt.close()
        
        history_odd = np.c_[history_odd.history['loss'], history_odd.history['val_loss'], history_odd.history['acc'], history_odd.history['val_acc']]
        np_save("plots/{}/{}.odd.hist.npy".format(args.tag, model_name), history_odd)
        history_even = np.c_[history_even.history['loss'], history_even.history['val_loss'], history_even.history['acc'], history_even.history['val_acc']]
        np_save("plots/{}/{}.even.hist.npy".format(args.tag, model_name), history_even)
    
    #------------------------------
    print "Done!!"
    
    #------------------------------
    testNN_cmd = "./testNN_multi.py --tag {0} --input-dir {1} --model-dir {2} --transformation {3} --region {4} --signal {5} --background {6} --tau-prongs {7} --output-dir {8}".format(
        args.tag,
        os.path.join(os.path.split(args.input_dir)[0]),  # get rid of the tag
        os.path.join(os.path.split(args.output_dir)[0]),  # get rid of the tag
        args.transformation,
        args.region,
        ",".join(signals),
        ",".join(backgrounds),
        args.tau_prongs,
        "plots/{}".format(args.tag),
    )
    if args.drop_vars:
        testNN_cmd += " --drop-var {}".format(",".join(args.drop_vars))
    if args.single_output:
        testNN_cmd += " --single-output"
    
    if args.no_testing:
        print "Run '{}' to plot distributions and ROC curves.".format(testNN_cmd)
    else:
        print "Calling testNN_multi.py to plot distributions and ROC curves"
        execute(testNN_cmd)


#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    trainNN_multi(args)

#./trainNN_multi.py -t v22 -T R20_7_baseline -r loose_SR_el --tau-prongs 1 -s Ztautau --drop-var tau_el_mvis
#./trainNN_multi.py -t v22 -T R20_7_baseline -r loose_SR_el --tau-prongs 1 -s Wjets --drop-var tau_el_mvis
#./trainNN_multi.py -t v22 -T R20_7_baseline -r loose_SR_el --tau-prongs 1 -s Zll
#./trainNN_multi.py -t v22 -T R20_7_baseline -r loose_SR_el --tau-prongs 3 -s Ztautau --drop-var tau_el_mvis
#./trainNN_multi.py -t v22 -T R20_7_baseline -r loose_SR_el --tau-prongs 3 -s Wjets --drop-var tau_el_mvis
#
#./trainNN_multi.py -t v22 -T R20_7_baseline -r loose_SR_mu --tau-prongs 1 -s Ztautau --drop-var tau_mu_mvis
#./trainNN_multi.py -t v22 -T R20_7_baseline -r loose_SR_mu --tau-prongs 1 -s Wjets --drop-var tau_mu_mvis
#./trainNN_multi.py -t v22 -T R20_7_baseline -r loose_SR_mu --tau-prongs 1 -s Zll
#./trainNN_multi.py -t v22 -T R20_7_baseline -r loose_SR_mu --tau-prongs 3 -s Ztautau --drop-var tau_mu_mvis
#./trainNN_multi.py -t v22 -T R20_7_baseline -r loose_SR_mu --tau-prongs 3 -s Wjets --drop-var tau_mu_mvis
