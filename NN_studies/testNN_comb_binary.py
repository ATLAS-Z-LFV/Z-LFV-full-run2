#!/usr/bin/env python

import sys, os, argparse

import numpy as np

import matplotlib
matplotlib.use('Agg')  # No display for running in batch/screen
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 20})
plt.rcParams.update({'figure.subplot.left': 0.15})
plt.rcParams.update({'figure.subplot.right': 0.95})
plt.rcParams.update({'figure.subplot.bottom': 0.15})
plt.rcParams.update({'figure.subplot.top': 0.95})

from sklearn.metrics import roc_curve, roc_auc_score

from modules.general_utils import flattenCommaSeparatedLists, checkMakeDirs
from NN_modules.custom_objects import load_model
from NN_modules.NN_utils import *

# Why do we suddenly need this, but not before?!?!
# This is needed to avoid version conflict
import tensorflow as tf
tf.python.control_flow_ops = tf

#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser('./testNN_comb_binary.py')
    
    parser.add_argument("--tag", "-t", required=True, type=str, help="The version tag.")
    parser.add_argument("--input-dir", "-i", default="./inputs", type=str, help="The input directory where the transformed input numpy arrays have been stored.")
    parser.add_argument("--model-dir", "-m", default="./models", type=str, help="The model directory where the trained models have been saved.")
    parser.add_argument("--transformation", "-T", required=True, type=str, help="The input transformation scheme.")
    parser.add_argument("--region", "-r", required=True, type=str, help="Region that defines the training sample selection.")
    parser.add_argument("--signal", "-s", dest="signals", action='append', default=[], type=str, help="Signal samples.")
    parser.add_argument("--background", "-b", dest="backgrounds", action='append', default=[], type=str, help="Background samples.")
    parser.add_argument("--weights", "-w", default="", type=str, help="Relative weights for different backgrounds. Enter in format '<bkg1>:<wgt1>,<bkg2>:<wgt2>,...'.")
    
    parser.add_argument("--output-dir", "-o", required=True, type=str, help="Output directory for saving results.")
    parser.add_argument("--output-fmt", default="png,pdf,eps", type=str, help="Comma-separated list of output formats, default='png,pdf,eps'.")
    
    parser.add_argument("--tau-decay", "-p", default="1P", choices=["1P","3P","1p0n","1pXn","3p"], type=str, help="Select tau decay mode.")
    parser.add_argument("--drop-var", dest='drop_vars', default=[], action='append', type=str, help="Variable(s) to be dropped from inputs.")
    
    parser.add_argument("--final-validation", "-F", action='store_true', help="Final validation. i.e. test on second set of validation samples ('odd-even' and 'even-even').")
    
    try:
        args = parser.parse_args(argv)
        
        args.input_dir = os.path.join(args.input_dir, args.tag)
        args.model_dir = os.path.join(args.model_dir, args.tag)
        
        args.signals = flattenCommaSeparatedLists(args.signals)
        args.backgrounds = flattenCommaSeparatedLists(args.backgrounds)
        
        if args.weights:
            args.weights = dict(w.split(":") for w in args.weights.split(","))
            for b in args.weights:
                args.weights[b] = float(args.weights[b])
        else:
            args.weights = {}
        
        args.drop_vars = flattenCommaSeparatedLists(args.drop_vars)
        
        checkArgs(args)
        
    except:
        parser.print_help()
        raise
    
    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.isdir(args.input_dir):
        raise Exception("Input directory '{}' does not exists (or is not a directory).".format(args.input_dir))
    
    if not os.path.isdir(args.model_dir):
        raise Exception("Model directory '{}' does not exists (or is not a directory).".format(args.model_dir))
    
    checkMakeDirs(args.output_dir)
    
    if not args.signals:
        raise Exception("No signal samples provided.")
    if not args.backgrounds:
        raise Exception("No background samples provided.")

#-------------------------------------------------------------------------------
def testNN_comb_binary(args):
    
    #-------------------------------------------------
    # Load
    input = os.path.join(args.input_dir, "{}_%s.%s.npy" % (args.region, args.transformation))
    sig_array = None
    for s in args.signals:
        print "Loading signal data '{}'...".format(input.format(s))
        if sig_array is None:
            sig_array = np_load(input.format(s))
        else:
            sig_array = np.concatenate([sig_array, np_load(input.format(s))])
    bkg_arrays = []
    for b in args.backgrounds:
        print "Loading background data '{}'...".format(input.format(b))
        bkg_arrays.append(np_load(input.format(b)))
    
    for bkg_array in bkg_arrays:
        if not bkg_array.dtype.names == sig_array.dtype.names:
            raise Exception("The input numpy arrays do not have the same field names.", bkg_array.dtype.names, sig_array.dtype.names)
    print "The loaded input arrays have the following fields:"
    for f in sig_array.dtype.names:
        print "  {}".format(f)
    
    #-------------------------------------------------
    # Load the scaling information and normalise inputs
    scale_file_name = "{}/{}_{}_{}.scaling".format(args.model_dir, args.region, args.tau_decay, args.transformation)
    if not os.path.exists(scale_file_name):
        raise Exception("Couldn't find '{}'. Cannot perform input normalisation.".format(scale_file_name))
    print "Normalising inputs..."
    scale = json_load(scale_file_name)
    normaliseInputs(bkg_arrays + [sig_array], scale=scale)
    
    comb_model_name = "{}/comb_binary_{}_{}_{}".format(args.model_dir, args.region, args.tau_decay, args.transformation)
    
    #-------------------------------------------------
    # Split according to eventNumber
    print "Preparing samples (splitting according to eventNumber and tau-decay)..."
    
    bkgs_odd = []
    bkgs_even = []
    for bkg_array in bkg_arrays:
        four_multiples = bkg_array["eventNumber"] & 3
        if not args.final_validation:
            odd_even  = np.logical_not(four_multiples - 1)  # 4N+1
            even_even = np.logical_not(four_multiples)      # 4N
            bkgs_odd.append(bkg_array[odd_even])
            bkgs_even.append(bkg_array[even_even])
        else:
            odd_odd  = np.logical_not(four_multiples - 3)  # 4N+3
            even_odd = np.logical_not(four_multiples - 2)  # 4N+2
            bkgs_odd.append(bkg_array[odd_odd])
            bkgs_even.append(bkg_array[even_odd])
    
    four_multiples = sig_array["eventNumber"] & 3
    if not args.final_validation:
        odd_even  = np.logical_not(four_multiples - 1)  # 4N+1
        even_even = np.logical_not(four_multiples)      # 4N
        sig_odd  = sig_array[odd_even]
        sig_even = sig_array[even_even]
    else:
        odd_odd  = np.logical_not(four_multiples - 3)  # 4N+3
        even_odd = np.logical_not(four_multiples - 2)  # 4N+2
        sig_odd  = sig_array[odd_odd]
        sig_even = sig_array[even_odd]
    
    #-------------------------------------------------
    # Select events according to tau decay
    n_tracks = int(args.tau_decay[0])
    for samples in [bkgs_odd, bkgs_even]:
        for i,sam in enumerate(samples):
            samples[i] = sam[sam["taus_n_tracks[0]"]==n_tracks]
    sig_odd = sig_odd[sig_odd["taus_n_tracks[0]"]==n_tracks]
    sig_even = sig_even[sig_even["taus_n_tracks[0]"]==n_tracks]
    if args.tau_decay == "1p0n":
        for samples in [bkgs_odd, bkgs_even]:
            for i,sam in enumerate(samples):
                samples[i] = sam[sam["taus_decay_mode[0]"]==0]
        sig_odd = sig_odd[sig_odd["taus_decay_mode[0]"]==0]
        sig_even = sig_even[sig_even["taus_decay_mode[0]"]==0]
    elif args.tau_decay == "1pXn":
        for samples in [bkgs_odd, bkgs_even]:
            for i,sam in enumerate(samples):
                samples[i] = sam[sam["taus_decay_mode[0]"]!=0]
        sig_odd = sig_odd[sig_odd["taus_decay_mode[0]"]!=0]
        sig_even = sig_even[sig_even["taus_decay_mode[0]"]!=0]
    print "Will validate on"
    print "  {} ({}) odd (even) {} signal samples".format(len(sig_odd), len(sig_even), args.tau_decay)
    for i,sam in enumerate(args.backgrounds):
        print "  {} ({}) {} samples".format(len(bkgs_odd[i]), len(bkgs_even[i]), sam)
    
    # Do this later, after we make copies of the samples and scale them
    ##-------------------------------------------------
    ## Tuples of (samples, labels, weights)
    #sam_odd  = getLabelsAndWeights(sig_odd, bkg_odd, args.drop_vars)
    #sam_even = getLabelsAndWeights(sig_even, bkg_even, args.drop_vars)
    
    #-------------------------------------------------
    print "Loading NN models..."
    models_odd = []
    models_even = []
    for b in args.backgrounds:
        model_name = "{}/binary_{}_{}_{}_{}".format(args.model_dir, args.region, args.tau_decay, b, args.transformation)
        models_odd.append(load_model("{}.odd.h5".format(model_name)))
        models_even.append(load_model("{}.even.h5".format(model_name)))
    
    print "Successfully loaded NN models."
    
    #-------------------------------------------------
    print "Evaluating..."
    # It is faster to evaluate all the samples together as one tensor
    # We split results afterward
    pred_odd = np.zeros(len(sig_odd) + sum(len(b) for b in bkgs_odd))
    pred_even = np.zeros(len(sig_even) + sum(len(b) for b in bkgs_even))
    bkg_weight_sum = 0
    
    # Make copies of the samples and scale them differently for each model
    for i in range(len(args.backgrounds)):
        drop_vars = list(args.drop_vars)
        
        # Hard-coded version-specific variable drops
        ch = "mu" if args.signals[0]=="Ztaumu" else "el"
        drop_vars += droppedVariables(args.transformation, args.backgrounds[i], ch, args.tau_decay)
        drop_vars = list(set(drop_vars))
        
        _sig_odd = sig_odd.copy()
        _sig_even = sig_even.copy()
        _bkg_odd = np.concatenate([b.copy() for b in bkgs_odd])
        _bkg_even = np.concatenate([b.copy() for b in bkgs_even])
        sam_odd = getLabelsAndWeights(_sig_odd, _bkg_odd, drop_vars)
        sam_even = getLabelsAndWeights(_sig_even, _bkg_even, drop_vars)
        if not args.backgrounds[i] in args.weights:
            pred_odd += np.square(1 - models_even[i].predict(sam_odd[0])[:,0])
            pred_even += np.square(1 - models_odd[i].predict(sam_even[0])[:,0])
            bkg_weight_sum += 1
        else:
            w = args.weights[args.backgrounds[i]]
            pred_odd += w * np.square(1 - models_even[i].predict(sam_odd[0])[:,0])
            pred_even += w * np.square(1 - models_odd[i].predict(sam_even[0])[:,0])
            bkg_weight_sum += w
    
    pred_odd = 1 - np.sqrt(pred_odd/len(args.backgrounds))
    pred_even = 1 - np.sqrt(pred_even/len(args.backgrounds))
    
    # sam_odd/sam_even are from the last of the above loop
    # but it is OK since they are the same regardless of scaling
    true_odd = sam_odd[1]
    true_even = sam_even[1]
    weight_odd = sam_odd[2]
    weight_even = sam_even[2]
    
    # Now we split the results
    # Below are lists of indices for where each background starts in our arrays
    idx_odd = [len(sig_odd)]
    idx_even = [len(sig_even)]
    for i in range(len(args.backgrounds)):
        idx_odd.append(idx_odd[-1] + len(bkgs_odd[i]))
        idx_even.append(idx_even[-1] + len(bkgs_even[i]))
    
    sig_pred = np.concatenate([pred_odd[0:idx_odd[0]], pred_even[0:idx_even[0]]])
    sig_true = np.concatenate([true_odd[0:idx_odd[0]], true_even[0:idx_even[0]]])
    sig_weight = np.concatenate([weight_odd[0:idx_odd[0]], weight_even[0:idx_even[0]]])
    
    bkg_preds = []
    bkg_trues = []
    bkg_weights = []
    for i in range(len(args.backgrounds)):
        _odd = pred_odd[idx_odd[i]:idx_odd[i+1]]
        _even = pred_even[idx_even[i]:idx_even[i+1]]
        bkg_preds.append(np.concatenate([_odd, _even]))
        _odd = true_odd[idx_odd[i]:idx_odd[i+1]]
        _even = true_even[idx_even[i]:idx_even[i+1]]
        bkg_trues.append(np.concatenate([_odd, _even]))
        _odd = weight_odd[idx_odd[i]:idx_odd[i+1]]
        _even = weight_even[idx_even[i]:idx_even[i+1]]
        bkg_weights.append(np.concatenate([_odd, _even]))
    
    #-------------------------------------------------
    print "Plotting ROC curve..."
    for i,bkg in enumerate(args.backgrounds):
        t = np.concatenate([sig_true, bkg_trues[i]])
        p = np.concatenate([sig_pred, bkg_preds[i]])
        w = np.concatenate([sig_weight, bkg_weights[i]])
        fpr, tpr, th = roc_curve(t, p, sample_weight=w)
        auc = roc_auc_score(t, p, sample_weight=w)
        bkg = bkg.replace("Ztautau", r"$Z\rightarrow\tau\tau$")
        bkg = bkg.replace("Wjets", r"$W$+jets")
        bkg = bkg.replace("Zll", r"$Z\rightarrow\ell\ell$")
        plt.plot(fpr, tpr, label = '%s (AUC = %0.3f)' % (bkg, auc))
        
    leg = plt.legend(loc=4, fontsize="medium")
    leg.set_title("ROC curve for signal against:")
    plt.plot([0, 1], [0, 1],'k--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.xlabel('False positive rate', horizontalalignment='right', x=1.0)
    plt.ylabel('True positive rate', horizontalalignment='right', y=1.0)
    for fmt in args.output_fmt.split(','):
        plt.savefig("{}/{}.roc.{}".format(args.output_dir, os.path.basename(comb_model_name), fmt))
    plt.close()
    
    print "Saved '{}/{}.roc.pdf(/eps/png)'.".format(args.output_dir, os.path.basename(comb_model_name))
    
    #-------------------------------------------------
    print "Plotting NN output distributions..."
    nBins = 20
    log = False
    plt.hist(sig_pred, nBins, (0,1), weights=sig_weight/np.sum(sig_weight), label='Signal', color='r', linestyle='--', histtype='step', log=log)
    for i,bkg in enumerate(args.backgrounds):
        bkg = bkg.replace("Ztautau", r"$Z\rightarrow\tau\tau$")
        bkg = bkg.replace("Wjets", r"$W$+jets")
        bkg = bkg.replace("Zll", r"$Z\rightarrow\ell\ell$")
        plt.hist(bkg_preds[i], nBins, (0,1), weights=bkg_weights[i]/np.sum(bkg_weights[i]), label=bkg, histtype='step', log=log)
    axes = plt.gca()
    axes.set_ylim([0, axes.get_ylim()[1]])  # force y-range to start at zero
    if len(args.backgrounds) > 2:
        plt.legend(loc=(0.32,0.62), fontsize="medium")
    else:
        plt.legend(loc=9, fontsize="medium")
    plt.xlabel('Combined NN output', horizontalalignment='right', x=1.0)
    plt.ylabel('Normalised number of events', horizontalalignment='right', y=1.0)
    for fmt in args.output_fmt.split(','):
        plt.savefig("{}/{}.dist.{}".format(args.output_dir, os.path.basename(comb_model_name), fmt))
    plt.close()
    print "Saved '{}/{}.dist.pdf(/eps/png)'.".format(args.output_dir, os.path.basename(comb_model_name))
    
    #-------------------------------------------------
    print "Done!"


#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    testNN_comb_binary(args)
