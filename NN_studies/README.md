# NN_studies

This directory contains all the codes needed to train, test, and decorate neural networks for the analysis.

<BR>

## Setup

To set up for the first time, simply do:
```
source setup.sh
```

The setup should be done fully automatically if you are on Nikhef Stoomboot or using a machine with cvmfs (e.g. lxplus).

After the first-time setup, if the codes do not need updates, simply do:
```
source setup_fast.sh
```
each time you log in to the machine.

Blame Terry (ws.chan@cern.ch) if the setup scripts don't work.

<BR>

## Sample preparation

Before we can start training the NN, we need to first convert the training data into numpy arrays for Keras/TensorFlow to read. To do so, we use `rootToNumpy.py`, e.g.:
```
./rootToNumpy.py --mode had --tag v34 --transformation Lorentz_invar_a0xy --regions loose_SR_mu,loose_SR_el --samples Ztautau,Wjets,Zll,data
```

The `--transformation`/`-T` option selects the input transformation(s) (boosting, rotation, etc) to be applied. The transformations are defined in `NN_modules/transformation.py`.

<BR>

## Training

To train the NN, use `trainNN_binary.py` (or the outdated `trainNN_multi.py`), e.g.:
```
./trainNN_binary.py --mode had -t v34 -T Lorentz_invar_a0xy -r loose_SR_mu -s Ztautau --tau-prongs 1
```

To control the hyperparameters, use the options `--nodes`/`-N`, `--layers`/`-L`, `--epochs`/`-E`, `--batch`/`-B`, `--learning-rate`/`-lr`, `--drop-out`/`-D`, `--actv`.

Check the `plots/` directory for performance plots.

**NOTE:** (*To be improved*) Retraining with the same `--tag`/`-t`, `--transformation`/`-T`, `--region`/`-r` and `--sample`/`-s` will completely overwrite the previously trained model and validation plots. If you want to test different settings/hyperparameters without messing with the previously trained models, first create a symlink in `./inputs` (e.g. `ln -s v34 inputs/v34_new_test`) and then train with a new tag (e.g. `./trainNN_binary.py -t v34_new_test ...`).

<BR>

## Decorating

To decorate NN scores to existing ntuples, use `decorateNN_binary.py` (or the outdated `decorateNN_multi.py`), e.g.:
```
./decorateNN_binary.py --mode had -t v34 -T Lorentz_invar_a0xy --training-region loose_SR  --parallel --max-threads 8 --no-syst
```

<BR>
