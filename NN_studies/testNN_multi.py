#!/usr/bin/env python

import sys, os, argparse

import numpy as np

import matplotlib
matplotlib.use('Agg')  # No display for running in batch/screen
import matplotlib.pyplot as plt

from sklearn.metrics import roc_curve, roc_auc_score

from modules.general_utils import flattenCommaSeparatedLists, checkMakeDirs
from NN_modules.custom_objects import load_model
from NN_modules.NN_utils import *

# Why do we suddenly need this, but not before?!?!
# This is needed to avoid version conflict
import tensorflow as tf
tf.python.control_flow_ops = tf

#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser('./testNN_multi.py')
    
    parser.add_argument("--tag", "-t", default="foo", type=str, help="The version tag.")
    parser.add_argument("--input-dir", "-i", default="./inputs", type=str, help="The input directory where the transformed input numpy arrays have been stored.")
    parser.add_argument("--model-dir", "-m", default="./models", type=str, help="The model directory where the trained models have been saved.")
    parser.add_argument("--transformation", "-T", default="", type=str, help="The input transformation scheme.")
    parser.add_argument("--region", "-r", default="", type=str, help="Region that defines the training sample selection.")
    parser.add_argument("--signal", "-s", dest="signals", action='append', default=[], type=str, help="Signal samples.")
    parser.add_argument("--background", "-b", dest="backgrounds", action='append', default=[], type=str, help="Background samples.")
    
    parser.add_argument("--output-dir", "-o", default="", type=str, help="Output directory for saving results.")
    
    parser.add_argument("--single-output",action='store_true', default=False, help="Single output mode (i.e. only signal vs multiple backgrounds, without background-vs-background outputs).")
    
    parser.add_argument("--tau-prongs", "-p", default=1, choices=[1,3], type=int, help="Select number of tau prongs.")
    parser.add_argument("--drop-var", dest='drop_vars', default=[], action='append', type=str, help="Variable(s) to be dropped from inputs.")
    
    parser.add_argument("--final-validation", "-F", action='store_true', help="Final validation. i.e. test on second set of validation samples ('odd-even' and 'even-even').")
    
    try:
        args = parser.parse_args(argv)
        
        args.input_dir = os.path.join(args.input_dir, args.tag)
        args.model_dir = os.path.join(args.model_dir, args.tag)
        
        args.signals = flattenCommaSeparatedLists(args.signals)
        args.backgrounds = flattenCommaSeparatedLists(args.backgrounds)
        
        args.drop_vars = flattenCommaSeparatedLists(args.drop_vars)
        
        checkArgs(args)
        
    except:
        parser.print_help()
        raise
    
    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.isdir(args.input_dir):
        raise Exception("Input directory '{}' does not exists (or is not a directory).".format(args.input_dir))
    
    if not os.path.isdir(args.model_dir):
        raise Exception("Model directory '{}' does not exists (or is not a directory).".format(args.model_dir))
    
    checkMakeDirs(args.output_dir)
    
    if not args.signals:
        raise Exception("No signal samples provided.")
    if not args.backgrounds:
        raise Exception("No background samples provided.")

#-------------------------------------------------------------------------------
def testNN_multi(args):
    
    #-------------------------------------------------
    # Load
    input = os.path.join(args.input_dir, "{}_%s.%s.npy" % (args.region, args.transformation))
    sig_array = None
    for s in args.signals:
        print "Loading signal data '{}'...".format(input.format(s))
        if sig_array is None:
            sig_array = np_load(input.format(s))
        else:
            sig_array = np.concatenate([sig_array, np_load(input.format(s))])
    bkg_arrays = []
    for b in args.backgrounds:
        print "Loading background data '{}'...".format(input.format(b))
        bkg_arrays.append(np_load(input.format(b)))
    
    for bkg_array in bkg_arrays:
        if not bkg_array.dtype.names == sig_array.dtype.names:
            raise Exception("The input numpy arrays do not have the same field names.", bkg_array.dtype.names, sig_array.dtype.names)
    print "The loaded input arrays have the following fields:"
    for f in sig_array.dtype.names:
        print "  {}".format(f)
    
    #-------------------------------------------------
    # Load the scaling information and normalise inputs
    scale_file_name = "{}/{}_{}P_{}.scaling".format(args.model_dir, args.region, args.tau_prongs, args.transformation)
    if not os.path.exists(scale_file_name):
        raise Exception("Couldn't find '{}'. Cannot perform input normalisation.".format(scale_file_name))
    print "Normalising inputs..."
    scale = json_load(scale_file_name)
    normaliseInputs(bkg_arrays + [sig_array], scale=scale)
    
    #-------------------------------------------------
    # Split according to eventNumber
    print "Preparing samples (splitting according to eventNumber and tau-prongs)..."
    
    bkgs_odd = []
    bkgs_even = []
    for bkg_array in bkg_arrays:
        four_multiples = bkg_array["eventNumber"] & 3
        if not args.final_validation:
            odd_even  = np.logical_not(four_multiples - 1)  # 4N+1
            even_even = np.logical_not(four_multiples)      # 4N
            bkgs_odd.append(bkg_array[odd_even])
            bkgs_even.append(bkg_array[even_even])
        else:
            odd_odd  = np.logical_not(four_multiples - 3)  # 4N+3
            even_odd = np.logical_not(four_multiples - 2)  # 4N+2
            bkgs_odd.append(bkg_array[odd_odd])
            bkgs_even.append(bkg_array[even_odd])
    
    four_multiples = sig_array["eventNumber"] & 3
    if not args.final_validation:
        odd_even  = np.logical_not(four_multiples - 1)  # 4N+1
        even_even = np.logical_not(four_multiples)      # 4N
        sig_odd  = sig_array[odd_even]
        sig_even = sig_array[even_even]
    else:
        odd_odd  = np.logical_not(four_multiples - 3)  # 4N+3
        even_odd = np.logical_not(four_multiples - 2)  # 4N+2
        sig_odd  = sig_array[odd_odd]
        sig_even = sig_array[even_odd]
    
    #-------------------------------------------------
    # Select events according to tau prongs
    for bkgs in [bkgs_odd, bkgs_even]:
        for i,bkg in enumerate(bkgs):
            bkgs[i] = bkg[bkg["taus_n_tracks[0]"]==args.tau_prongs]
    sig_odd      = sig_odd[sig_odd["taus_n_tracks[0]"]==args.tau_prongs]
    sig_even     = sig_even[sig_even["taus_n_tracks[0]"]==args.tau_prongs]
    print "Will validate on"
    print "  {} ({}) odd (even) {}P signal samples".format(len(sig_odd), len(sig_even), args.tau_prongs)
    for i,sam in enumerate(args.backgrounds):
        print "  {} ({}) {} samples".format(len(bkgs_odd[i]), len(bkgs_even[i]), sam)
    
    #-------------------------------------------------
    # Tuples of (samples, labels, weights)
    if args.single_output:
        sam_odd  = getLabelsAndWeights(sig_odd, bkgs_odd, args.drop_vars)
        sam_even = getLabelsAndWeights(sig_even, bkgs_even, args.drop_vars)
    else:
        sam_odd  = getMultiLabelsAndWeights(sig_odd, bkgs_odd, args.drop_vars)
        sam_even = getMultiLabelsAndWeights(sig_even, bkgs_even, args.drop_vars)
    
    #-------------------------------------------------
    print "Loading NN models..."
    if args.single_output:
        model_name = "{}/singleMulti_{}_{}P_{}".format(args.model_dir, args.region, args.tau_prongs, args.transformation)
    else:
        model_name = "{}/multi_{}_{}P_{}".format(args.model_dir, args.region, args.tau_prongs, args.transformation)
    model_odd = load_model("{}.odd.h5".format(model_name))
    model_even = load_model("{}.even.h5".format(model_name))
    
    print "Successfully loaded NN models."
    
    #-------------------------------------------------
    print "Evaluating..."
    pred_odd = model_even.predict(sam_odd[0])
    pred_even = model_odd.predict(sam_even[0])
    
    if not args.single_output:
        pred = np.concatenate([pred_odd, pred_even])
        true = np.concatenate([sam_odd[1], sam_even[1]])
        weight = np.concatenate([sam_odd[2], sam_even[2]])
        
        mask = true[:,0]==1
        
        trues = []
        preds = []
        weights = []
        for i in range(len(args.backgrounds)+1):
            mask = true[:,i]==1
            trues.append(true[mask])
            preds.append(pred[mask])
            weights.append(weight[mask])
    
    else:
        # Now we split the results
        # Below are lists of indices for where each background starts in our arrays
        idx_odd = [0, len(sig_odd)]
        idx_even = [0, len(sig_even)]
        for i in range(len(args.backgrounds)):
            idx_odd.append(idx_odd[-1] + len(bkgs_odd[i]))
            idx_even.append(idx_even[-1] + len(bkgs_even[i]))
        
        preds = []
        trues = []
        weights = []
        for i in range(len(args.backgrounds)+1):
            _odd = pred_odd[idx_odd[i]:idx_odd[i+1]]
            _even = pred_even[idx_even[i]:idx_even[i+1]]
            preds.append(np.concatenate([_odd, _even]))
            _odd = sam_odd[1][idx_odd[i]:idx_odd[i+1]]
            _even = sam_even[1][idx_even[i]:idx_even[i+1]]
            trues.append(np.concatenate([_odd, _even]))
            _odd = sam_odd[2][idx_odd[i]:idx_odd[i+1]]
            _even = sam_even[2][idx_even[i]:idx_even[i+1]]
            weights.append(np.concatenate([_odd, _even]))
    
    #-------------------------------------------------
    print "Plotting ROC curve..."
    for i,bkg in enumerate(args.backgrounds):
        if args.single_output:
            t = np.concatenate([trues[0], trues[i+1]])
            p = np.concatenate([preds[0], preds[i+1]])
        else:
            t = np.concatenate([trues[0][:,0], trues[i+1][:,0]])
            p = np.concatenate([preds[0][:,0], preds[i+1][:,0]])
        w = np.concatenate([weights[0], weights[i+1]])
        fpr, tpr, th = roc_curve(t, p, sample_weight=w)
        auc = roc_auc_score(t, p, sample_weight=w)
        plt.plot(fpr, tpr, label = '%s (AUC = %0.3f)' % (bkg, auc))
        
    leg = plt.legend(loc=4)
    leg.set_title("ROC for signal against:", prop={'size':'large'})
    plt.plot([0, 1], [0, 1],'k--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.xlabel('False positive rate', horizontalalignment='right', x=1.0)
    plt.ylabel('True positive rate', horizontalalignment='right', y=1.0)
    plt.savefig("{}/{}.roc.png".format(args.output_dir, os.path.basename(model_name)))
    plt.savefig("{}/{}.roc.pdf".format(args.output_dir, os.path.basename(model_name)))
    plt.savefig("{}/{}.roc.eps".format(args.output_dir, os.path.basename(model_name)))
    plt.close()
    
    print "Saved '{}/{}.roc.pdf(/eps/png)'.".format(args.output_dir, os.path.basename(model_name))
    
    #-------------------------------------------------
    print "Plotting NN output distributions..."
    nBins = 20
    log = False
    for i,node in enumerate(["signal"]+args.backgrounds):
        if args.single_output and i > 0:
            continue
        plt.hist(preds[0][:,i], nBins, (0,1), weights=weights[0]/np.sum(weights[0]), label='signal', color='k', linestyle='--', histtype='step', log=log)
        for j,bkg in enumerate(args.backgrounds):
            j = j+1
            plt.hist(preds[j][:,i], nBins, (0,1), weights=weights[j]/np.sum(weights[j]), label=bkg, histtype='step', log=log)
        axes = plt.gca()
        axes.set_ylim([0, axes.get_ylim()[1]])  # force y-range to start at zero
        plt.legend(loc=1)
        plt.xlabel('NN output ({})'.format(node), horizontalalignment='right', x=1.0)
        plt.ylabel('Normalised number of events', horizontalalignment='right', y=1.0)
        plt.savefig("{}/{}.dist{}.png".format(args.output_dir, os.path.basename(model_name), i))
        plt.savefig("{}/{}.dist{}.pdf".format(args.output_dir, os.path.basename(model_name), i))
        plt.savefig("{}/{}.dist{}.eps".format(args.output_dir, os.path.basename(model_name), i))
        plt.close()
        print "Saved '{}/{}.dist{}.pdf(/eps/png)'.".format(args.output_dir, os.path.basename(model_name), i)
    
    #-------------------------------------------------
    print "Done!"


#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    testNN_multi(args)
