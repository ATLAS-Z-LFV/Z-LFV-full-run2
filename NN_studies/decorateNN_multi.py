#!/usr/bin/env python

import os, sys
import argparse
from collections import OrderedDict
from time import sleep
from tempfile import mkstemp
from multiprocessing import Process, Lock

import numpy as np
from numpy.lib.recfunctions import drop_fields, append_fields

from modules.Inputs import Inputs, InputFile
from modules.general_utils import findDefaultConfigs, checkMakeDirs, flattenCommaSeparatedLists
from modules.general_utils import checkCpuCount, acquire
from modules.general_utils import root_open, rootVerbosityLevel

from NN_modules.custom_objects import load_model
from NN_modules.transformation import transformInputs
from NN_modules.NN_utils import *

from ROOT import TFile, TTree, gROOT
from root_numpy import tree2array, array2tree



#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser("./decorateNN.py")
    
    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    
    parser.add_argument("--output-dir", "-o", default="", type=str, help="Output directory. Default = input directory.")
    parser.add_argument("--input-file", "-i", dest="input_files", default=[], action='append', type=str, help="Input file(s). If this is not given, all files in inputs-db will be decorated (except alternative backgrounds).")
    
    parser.add_argument("--model-dir", default="./models", type=str, help="The model directory where the trained models have been saved.")
    parser.add_argument("--tag", "-t", default="foo", type=str, help="Iteration tag. Default = 'foo'")
    parser.add_argument("--model-prefix", "-m", default="multi", type=str, help="Model prefix. Default = 'multi'")
    parser.add_argument("--training-region", "-r", default="loose_SR", type=str, help="Training region. Default = 'loose_SR'")
    parser.add_argument("--transformation", "-T", default="", type=str, help="The input transformation scheme.")
    
    parser.add_argument("--force-recreate", "-f", action='store_true', default=False, help="Force recreation of the output file.")
    parser.add_argument("--keep-old-cycles", action='store_true', default=False, help="Keep old cycles. This keeps tree backups, but might waste disk space.")
    
    parser.add_argument("--no-syst", action='store_true', default=False, help="Skip trees for systematic variations.")
    
    parser.add_argument("--no-index", action='store_true', default=False, help="Do not build tree indices. Useful if the (runNumber,eventNumber) indices turn out to be not unique for some reasons.")
    
    parser.add_argument("--parallel", action='store_true', default=False, help="Use parallel multiprocessing.")
    parser.add_argument("--max-threads", default=4, type=int, help="Maximum number of threads to use for parallel multiprocessing.")
    
    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args, default_split_inputs=True)
        
        args.input_files = flattenCommaSeparatedLists(args.input_files)
        
    except:
        parser.print_help()
        raise
    
    checkArgs(args)
    
    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    
    if args.output_dir:
        checkMakeDirs(args.output_dir)

#-------------------------------------------------------------------------------
def decorateNN(args):
    
    gROOT.SetBatch(True)
    TTree.SetMaxTreeSize(long(2)*1024*1024*1024*1024)  # 2TB
    
    inputs = Inputs(args.inputs_db)
    
    if not args.output_dir:
        args.output_dir = inputs.dir
    
    if args.input_files:
        input_files = args.input_files
    else:
        input_files = [f.name for i in inputs.values() if i is not None for f in i.values()]
    
    #-------------------------------------------------
    # Loop over every tree in every input file and decorate
    trees = []
    for file in input_files:
        seen_trees = []
        with root_open(file) as f:
            for t in f.GetListOfKeys():
                if not t.GetClassName() == "TTree":
                    continue
                tree = t.GetName()
                if tree == "weights" or tree.startswith("EventLoop"):
                    continue
                if args.no_syst and not tree.endswith("_NOMINAL"):
                    continue
                if tree in seen_trees:
                    continue
                seen_trees.append(tree)
                trees.append((file, tree))
    
    if len(trees) == 1:
        decorateNNSingleTree(args, trees[0][0], trees[0][1], models, scales)
        print "="*30
        print "Done!!"
        return
    
    if args.parallel:
        n_threads = checkCpuCount(min(args.max_threads, len(trees)))
        print "Will use multiprocessing with {} threads".format(n_threads)
        lock = Lock()
        p = [None]*n_threads
    
    for f,t in trees:
        if not args.parallel:
            decorateNNSingleTree(args, f, t)
        else:
            while all(x is not None and x.is_alive() for x in p):
                # all threads are in use, wait for 1 second and check again
                sleep(1)
            i = next(i for i in range(n_threads) if p[i] is None or not p[i].is_alive())
            p[i] = Process(target=decorateNNSingleTree, args=(args, f, t, lock))
            p[i].start()
    
    while any(x is not None and x.is_alive() for x in p):
        # some threads are still alive, wait for 1 second and check again
        sleep(1)
    
    print "="*30
    print "Done!!"

#-------------------------------------------------------------------------------
def decorateNNSingleTree(args, input_file, input_tree, lock=None):
    with acquire(lock):
        print "="*30
        print "  Processing tree '{}/{}'".format(input_file, input_tree)
    
    model_name_template = "models/%s/%s_{}_{}_%s.{}.h5" % (args.tag, args.training_region, args.transformation)
    scale_name_template = "models/%s/%s_{}_{}_%s.scaling" % (args.tag, args.training_region, args.transformation)
    
    outputs = OrderedDict()
    
    # We need a temporary index to remember the original order of the events
    # and then sort the outputs accordingly (before calling root_numpy.array2tree)
    # Otherwise plotting would be extremely slow!!
    outputs["temp_index"] = np.array([])
    
    outputs["runNumber"] = np.array([])
    outputs["eventNumber"] = np.array([])
    outputs["NN_output_comb"] = np.array([])
    
    for ch in ["el", "mu"]:
        if ch == "el":
            branches = ["runNumber", "eventNumber", "taus_n_tracks[0]",
                        "taus_pt[0]", "taus_eta[0]", "taus_phi[0]", "taus_m[0]",
                        "met_et", "met_phi",
                        "electrons_pt[0]", "electrons_eta[0]", "electrons_phi[0]", "electrons_e[0]",
                        "tau_el_mcoll", "tau_el_delta_alpha", "tau_el_mvis",
                        #"tautrack_el_invmass",  # only exists in v26
                       ]
            selection = "nTaus>0&&electrons_signal[0]==1"
        else:
            branches = ["runNumber", "eventNumber", "taus_n_tracks[0]",
                        "taus_pt[0]", "taus_eta[0]", "taus_phi[0]", "taus_m[0]",
                        "met_et", "met_phi",
                        "muons_pt[0]", "muons_eta[0]", "muons_phi[0]", "muons_e[0]",
                        "tau_mu_mcoll", "tau_mu_delta_alpha", "tau_mu_mvis",
                        #"tautrack_mu_invmass",  # only exists in v26
                       ]
            selection = "nTaus>0&&muons_signal[0]==1"
        branches += ["tau_0_lead_track_pt", "tau_0_lead_track_eta", "tau_0_lead_track_phi", "tau_0_lead_track_e"]  # needed to calculate tautrack_el/mu_invmass in v22
        
        # If we are trying to calculate 'el' NN output for a 'mu' region tree
        # we will not find the electron variables (of course), and vice versa
        # In that case, catch the ROOT exception silently, skip the tree, and print a warning
        try:
            with root_open(input_file) as f:
                t = f.Get(input_tree)
                with rootVerbosityLevel("Fatal"):
                    raw_input = tree2array(t, branches, selection)
        except:
            #print "Warning: Input variable(s) missing. Skipping channel '{}' for tree '{}'".format(ch, input_tree)
            continue
        
        # remember original order of events
        raw_input = append_fields(raw_input, "temp_index", np.arange(len(raw_input)), usemask=False)
        
        for pr in ["1P", "3P"]:
            if pr == "1P":
                arr_pr = raw_input[raw_input["taus_n_tracks[0]"]==1]
            else:
                arr_pr = raw_input[raw_input["taus_n_tracks[0]"]==3]
            
            scale = json_load(scale_name_template.format(ch, pr))
            
            odd = (arr_pr["eventNumber"] & 1).astype(bool)
            for par in ["odd", "even"]:
                if par == "odd":
                    arr_par = arr_pr[np.logical_not(odd)]  # even events (will be using odd model)
                else:
                    arr_par = arr_pr[odd]  # odd events (will be using even model)
                
                outputs["temp_index"] = np.concatenate([outputs["temp_index"].astype(int), arr_par["temp_index"].astype(int)])
                outputs["runNumber"] = np.concatenate([outputs["runNumber"].astype(int), arr_par["runNumber"].astype(int)])
                outputs["eventNumber"] = np.concatenate([outputs["eventNumber"].astype(int), arr_par["eventNumber"].astype(int)])
                
                arr_par = drop_fields(arr_par, ["runNumber", "eventNumber", "taus_n_tracks[0]", "temp_index"], usemask=False)
                
                if len(arr_par) == 0:
                    continue
                
                arr_par = transformInputs[args.transformation](arr_par)
                normaliseInputs(arr_par, scale=scale)
                
                if args.transformation == "R20_7_baseline" and not bkg == "Zll":  ### warning: hard-coding...
                    arr_par = drop_fields(arr_par, ["tau_{}_mvis".format(ch)], usemask=False)
                
                model = load_model(model_name_template.format(ch, pr, par))
                arr_par = arr_par.view((arr_par.dtype[0], len(arr_par.dtype.names)))
                nn_scores = model.predict(arr_par)
                outputs["NN_output_comb"] = np.concatenate([outputs["NN_output_comb"], nn_scores[:,0]])
    
    outputs = createStructuredArray(outputs.keys(), outputs.values())
    outputs.sort(order=["temp_index", "runNumber", "eventNumber"])
    outputs = drop_fields(outputs, "temp_index")
    
    with acquire(lock):
        print "="*30
        print "  Writing friend tree '{}'".format("NN_{}".format(input_tree))
        output_file = os.path.join(args.output_dir, os.path.basename(input_file))
        output_file += ".NN.{}_{}.friend".format(args.tag, args.transformation)
        with root_open(output_file, "UPDATE") as f_out:
            fd_t = array2tree(outputs, "NN_{}".format(input_tree))
            ###if not args.no_index:
            ###    fd_t.BuildIndex("runNumber", "eventNumber")
            if args.keep_old_cycles:
                f_out.WriteTObject(fd_t)
            else:
                f_out.WriteTObject(fd_t, "", "Overwrite")
            print "  Tree written, with {} entries".format(fd_t.GetEntries())
    
    return

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    decorateNN(args)
