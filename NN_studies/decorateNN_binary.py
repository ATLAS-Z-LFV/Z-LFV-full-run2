#!/usr/bin/env python

import os, sys
import argparse
from time import sleep
from tempfile import mkstemp
from multiprocessing import Process, Lock

import numpy as np
from numpy.lib.recfunctions import drop_fields, append_fields

from modules.Inputs import Inputs, InputFile
from modules.general_utils import findDefaultConfigs, checkMakeDirs, flattenCommaSeparatedLists
from modules.general_utils import checkCpuCount, acquire
from modules.general_utils import DefaultOrderedDict
from modules.general_utils import root_open, rootVerbosityLevel

from NN_modules.custom_objects import load_model
from NN_modules.transformation import transformInputs
from NN_modules.NN_utils import *

from ROOT import TFile, TTree, gROOT
from root_numpy import tree2array, array2tree



#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser("./decorateNN.py")

    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")

    parser.add_argument("--output-dir", "-o", default="", type=str, help="Output directory. Default = input directory.")
    parser.add_argument("--input-file", "-i", dest="input_files", default=[], action='append', type=str, help="Input file(s). If this is not given, all files in inputs-db will be decorated (except alternative backgrounds).")

    parser.add_argument("--model-dir", default="./models", type=str, help="The model directory where the trained models have been saved.")
    parser.add_argument("--tag", "-t", default="foo", type=str, help="Iteration tag. Default = 'foo'")
    parser.add_argument("--model-prefix", "-m", default="binary", type=str, help="Model prefix. Default = 'binary'")
    parser.add_argument("--training-region", default="loose_SR", type=str, help="Training region. Default = 'loose_SR'")
    parser.add_argument("--transformation", "-T", default="", type=str, help="The input transformation scheme.")
    parser.add_argument("--drop-var", dest='drop_vars', default=[], action='append', type=str, help="Variable(s) to be dropped from inputs.")

    parser.add_argument("--backgrounds-lep", default="Ztautau,Wjets,Zll", type=str, help="Comma-separated list of leptonic channel backgrounds. Default = 'Ztautau,Wjets,Zll'")
    parser.add_argument("--backgrounds-1P", default="Ztautau,Wjets,Zll", type=str, help="Comma-separated list of 1-prong backgrounds. Default = 'Ztautau,Wjets,Zll'")
    parser.add_argument("--backgrounds-3P", default="Ztautau,Wjets", type=str, help="Comma-separated list of 3-prong backgrounds. Default = 'Ztautau,Wjets'")
    parser.add_argument("--backgrounds-1p0n", default="Ztautau,Wjets,Zll", type=str, help="Comma-separated list of 1p0n backgrounds. Default = 'Ztautau,Wjets,Zll'")
    parser.add_argument("--backgrounds-1pXn-el", default="Ztautau,Wjets,Zll", type=str, help="Comma-separated list of 1pXn-el backgrounds. Default = 'Ztautau,Wjets,Zll'")
    parser.add_argument("--backgrounds-1pXn-mu", default="Ztautau,Wjets", type=str, help="Comma-separated list of 1pXn-mu backgrounds. Default = 'Ztautau,Wjets'")
    parser.add_argument("--backgrounds-3p", default="Ztautau,Wjets", type=str, help="Comma-separated list of 3p backgrounds. Default = 'Ztautau,Wjets'")

    parser.add_argument("--weights", "-w", default="", type=str, help="Relative weights for different backgrounds. Enter in format '<bkg1>:<wgt1>,<bkg2>:<wgt2>,...'.")

    parser.add_argument("--force-recreate", "-f", action='store_true', default=False, help="Force recreation of the output file.")
    parser.add_argument("--keep-old-cycles", action='store_true', default=False, help="Keep old cycles. This keeps tree backups, but might waste disk space.")

    parser.add_argument("--v22", action="store_true", help="Backward compatibility with v22 n-tuples")
    parser.add_argument("--gamma", "-G", action='store_true', default=False, help="Configurations for gamma channels.")
    parser.add_argument("--mode", choices=["had","lep"], default="had", type=str, help="Choose between leptonic['lep']/hadronic['had'] tau mode (default='had')")
    parser.add_argument("--split-mode", choices=["1p3p","1p0nXn3p"], default="1p3p", type=str, help="How to split tau_had decay mode ('1p3p' or '1p0nXn3p').")

    parser.add_argument("--region", "-r", dest="regions", action='append', default=[], type=str, help="Only decorate tree(s) of the specified region(s). Decorate all trees if not specified.")
    parser.add_argument("--channel", "-c", dest="channels", action='append', default=[], type=str, help="Only events in the specified channel(s).")
    parser.add_argument("--no-syst", action='store_true', default=False, help="Skip trees for systematic variations.")
    parser.add_argument("--store-input-var", action='store_true', default=False, help="Store low-level input variables (transformed) as well.")

    parser.add_argument("--build-index", action='store_true', default=False, help="Build tree indices using (runNumber,eventNumber) as indices.")

    parser.add_argument("--parallel", action='store_true', default=False, help="Use parallel multiprocessing.")
    parser.add_argument("--max-threads", default=4, type=int, help="Maximum number of threads to use for parallel multiprocessing.")

    parser.add_argument("--verbose", action='store_true', default=False, help="Verbose mode.")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args, default_split_inputs=True)

        args.input_files = flattenCommaSeparatedLists(args.input_files)

        args.backgrounds_lep = args.backgrounds_lep.split(",")

        args.backgrounds_1P = args.backgrounds_1P.split(",")
        args.backgrounds_3P = args.backgrounds_3P.split(",")
        args.backgrounds_1p0n = args.backgrounds_1p0n.split(",")
        args.backgrounds_1pXn_el = args.backgrounds_1pXn_el.split(",")
        args.backgrounds_1pXn_mu = args.backgrounds_1pXn_mu.split(",")
        args.backgrounds_3p = args.backgrounds_3p.split(",")

        if args.weights:
            args.weights = dict(w.split(":") for w in args.weights.split(","))
            for b in args.weights:
                args.weights[b] = float(args.weights[b])
        else:
            args.weights = {}

        args.drop_vars = flattenCommaSeparatedLists(args.drop_vars)

        args.regions = flattenCommaSeparatedLists(args.regions)
        args.channels = flattenCommaSeparatedLists(args.channels)

    except:
        parser.print_help()
        raise

    checkArgs(args)

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))

    if args.output_dir:
        checkMakeDirs(args.output_dir)

#-------------------------------------------------------------------------------
def decorateNN(args):

    gROOT.SetBatch(True)
    TTree.SetMaxTreeSize(long(2)*1024*1024*1024*1024)  # 2TB

    inputs = Inputs(args.inputs_db)

    if not args.output_dir:
        args.output_dir = inputs.dir

    if args.input_files:
        input_files = args.input_files
    else:
        input_files = [f.name for i in inputs.values() if i is not None for f in i.values()]

    #-------------------------------------------------
    # Loop over every tree in every input file and decorate
    trees = []
    for file in input_files:
        seen_trees = []
        with root_open(file) as f:
            for t in f.GetListOfKeys():
                if not t.GetClassName() == "TTree":
                    continue
                tree = t.GetName()
                if tree == "weights" or tree.startswith("EventLoop"):
                    continue
                if args.no_syst and not tree.endswith("_NOMINAL"):
                    continue
                if "NRZll" in tree:
                    continue
                if tree in seen_trees:
                    continue
                else:
                    seen_trees.append(tree)
                if args.regions:
                    match_region = False
                    for r in args.regions:
                        if r in tree and not (r.startswith("SR_") and "SS_SR" in tree):
                            match_region = True
                            break
                    if not match_region:
                        continue
                trees.append((file, tree))

    if len(trees) == 1:
        decorateNNSingleTree(args, trees[0][0], trees[0][1])
        print "="*30
        print "Done!!"
        return

    if args.parallel:
        n_threads = checkCpuCount(min(args.max_threads, len(trees)))
        print "Will use multiprocessing with {} threads".format(n_threads)
        lock = Lock()
        p = [None]*n_threads

    for f,t in trees:
        if not args.parallel:
            decorateNNSingleTree(args, f, t)
        else:
            while all(x is not None and x.is_alive() for x in p):
                # all threads are in use, wait for 1 second and check again
                sleep(1)
            i = next(i for i in range(n_threads) if p[i] is None or not p[i].is_alive())
            p[i] = Process(target=decorateNNSingleTree, args=(args, f, t, lock))
            p[i].start()

    if args.parallel:
        while any(x is not None and x.is_alive() for x in p):
            # some threads are still alive, wait for 1 second and check again
            sleep(1)

    print "="*30
    print "Done!!"

#-------------------------------------------------------------------------------
def decorateNNSingleTree(args, input_file, input_tree, lock=None):
    with acquire(lock):
        print "="*30
        print "  Processing tree '{}/{}'".format(input_file, input_tree)

    if args.mode == "had":
        model_name_template = "%s/%s/%s_%s_{}_{}_{}_%s.{}.h5" % (args.model_dir, args.tag, args.model_prefix, args.training_region, args.transformation)
        scale_name_template = "%s/%s/%s_{}_{}_%s.scaling" % (args.model_dir, args.tag, args.training_region, args.transformation)
    elif args.mode == "lep":
        model_name_template = "%s/%s/%s_%s_{}_%s.{}.h5" % (args.model_dir, args.tag, args.model_prefix, args.training_region, args.transformation)
        scale_name_template = "%s/%s/%s_%s.scaling" % (args.model_dir, args.tag, args.training_region, args.transformation)
#        scale_name_template = "%s/%s/%s_{}_%s.scaling" % (args.model_dir, args.tag, args.training_region, args.transformation)

    if args.gamma:
        model_name_template = model_name_template.replace("_{}_", "_{}ph_", 1)
        scale_name_template = scale_name_template.replace("_{}_", "_{}ph_", 1)

    empty_float_array = lambda: np.array([], float)
    outputs = DefaultOrderedDict(empty_float_array)

    # We need a temporary index to remember the original order of the events
    # and then sort the outputs accordingly (before calling root_numpy.array2tree)
    # Otherwise plotting would be extremely slow!!
    outputs["temp_index"] = np.array([], int)
    outputs["runNumber"] = np.array([], int)
    outputs["eventNumber"] = np.array([], int)

    if args.channels:
        channels = args.channels
    else:
        channels = ["el", "mu"] if args.mode == "had" else ["mutaulepe", "eltaulepmu"]
    for ch in channels:
        if args.mode == "had":
            if ch == "el":
                branches = ["runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]",
                            "taus_pt[0]", "taus_eta[0]", "taus_phi[0]", "taus_m[0]",
                            "met_et", "met_phi",
                            "electrons_pt[0]", "electrons_eta[0]", "electrons_phi[0]", "electrons_e[0]",
                            "tau_el_mcoll", "tau_el_delta_alpha", "tau_el_mvis",
                            "tautrack_el_invmass",
                           ]
                selection = "nTaus>0&&(electrons_signal[0]==1||electrons_non_isolated_signal[0]==1)"
            else:
                branches = ["runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]",
                            "taus_pt[0]", "taus_eta[0]", "taus_phi[0]", "taus_m[0]",
                            "met_et", "met_phi",
                            "muons_pt[0]", "muons_eta[0]", "muons_phi[0]", "muons_e[0]",
                            "tau_mu_mcoll", "tau_mu_delta_alpha", "tau_mu_mvis",
                            "tautrack_mu_invmass",
                           ]
                selection = "nTaus>0&&muons_signal[0]==1||muons_non_isolated_signal[0]==1"

        elif args.mode == "lep":
            ### to AK: Modify this block for your need
            ### the branches should be consistent with those in rootToNumpy.py
            aliases = {}
            selection = "1"
            branches = ["runNumber", "eventNumber",
                        "met_et", "met_phi",
                        "muons_pt[0]", "muons_eta[0]", "muons_phi[0]", "muons_e[0]",
                        "electrons_pt[0]", "electrons_eta[0]", "electrons_phi[0]", "electrons_e[0]",
                        #"mu_el_mcoll",
                       ]
            if ch == "mutaulepe":
                branches += ["mu_el_mcoll", "el_mu_mcoll", "mu_el_mvis", "mu_el_delta_alpha", "mu_el_invmass"] # "el_mu_delta_alpha",
            if ch == "eltaulepmu":
                branches += ["mu_el_mcoll", "el_mu_mcoll", "mu_el_mvis", "mu_el_delta_alpha", "mu_el_invmass"] # "el_mu_delta_alpha",

        if args.v22:
            branches += ["tau_0_lead_track_pt", "tau_0_lead_track_eta", "tau_0_lead_track_phi", "tau_0_lead_track_e"]
            if ch == "el":
                branches.remove("tautrack_el_invmass")
            else:
                branches.remove("tautrack_mu_invmass")

        if args.gamma:
            branches += ["ph_0_et", "ph_0_eta", "ph_0_phi"]
            if ch == "el":
                branches += ["tau_el_ph_mcoll", "tau_el_ph_mvis"]
            else:
                branches += ["tau_mu_ph_mcoll", "tau_mu_ph_mvis"]

        if args.mode == "had":
            # Add a0xy
            aliases = {}
            branches += ["tau_a0xy"]
            aliases["tau_a0xy"] = "fabs(taus_d0[0] - vertex_x*TMath::Sin(taus_phi[0]) + vertex_y*TMath::Cos(taus_phi[0]))"
            if ch == "el":
                branches += ["el_a0xy"]
                aliases["el_a0xy"] = "fabs(electrons_trk_d0[0] - vertex_x*TMath::Sin(electrons_phi[0]) + vertex_y*TMath::Cos(electrons_phi[0]))"
            else:
                branches += ["mu_a0xy"]
                aliases["mu_a0xy"] = "fabs(muons_trk_d0[0] - vertex_x*TMath::Sin(muons_phi[0]) + vertex_y*TMath::Cos(muons_phi[0]))"

        # If we are trying to calculate 'el' NN output for a 'mu' region tree
        # we will not find the electron variables (of course), and vice versa
        # Also similar for 'gamma' channels
        # In those cases, catch the ROOT exception silently, skip the tree, and print a warning
        try:
            with root_open(input_file) as f:
                t = f.Get(input_tree)
                for name,formula in aliases.iteritems():
                    t.SetAlias(name, formula)
                if args.verbose:
                    raw_input = tree2array(t, branches, selection)
                else:
                    with rootVerbosityLevel("Fatal"):
                        raw_input = tree2array(t, branches, selection)
        except:
            print "Warning: Input variable(s) missing. Skipping channel '{}' for tree '{}'".format(ch, input_tree)
            continue

        # remember original order of events
        if not args.build_index:
            raw_input = append_fields(raw_input, "temp_index", np.arange(len(raw_input)), usemask=False)

        if args.mode == "had":
            if args.split_mode == "1p3p":
                prongness = ["1P", "3P"]
            elif args.split_mode == "1p0nXn3p":
                prongness = ["1p0n", "1pXn", "3p"]
        else:
            prongness = [None]

        for pr in prongness:
            if pr is None:
                arr_pr = raw_input
                bkgs = args.backgrounds_lep
            elif pr == "1P":
                arr_pr = raw_input[raw_input["taus_n_tracks[0]"]==1]
                bkgs = args.backgrounds_1P
                if args.gamma and ch == "mu" and "Zll" in bkgs:
                    bkgs = [b for b in bkgs if not b == "Zll"]
            elif pr == "3P":
                arr_pr = raw_input[raw_input["taus_n_tracks[0]"]==3]
                bkgs = args.backgrounds_3P
            elif pr == "1p0n":
                arr_pr = raw_input[raw_input["taus_n_tracks[0]"]==1]
                arr_pr = arr_pr[arr_pr["taus_decay_mode[0]"]==0]
                bkgs = args.backgrounds_1p0n
            elif pr == "1pXn":
                arr_pr = raw_input[raw_input["taus_n_tracks[0]"]==1]
                arr_pr = arr_pr[arr_pr["taus_decay_mode[0]"]!=0]
                if ch == "el":
                    bkgs = args.backgrounds_1pXn_el
                elif ch == "mu":
                    bkgs = args.backgrounds_1pXn_mu
                else:
                    raise RuntimeError("Unknown 1pXn channel '{}'".format(ch))
            elif pr == "3p":
                arr_pr = raw_input[raw_input["taus_n_tracks[0]"]==3]
                bkgs = args.backgrounds_3p
            else:
                raise RuntimeError("Unknown prongness '{}'".format(pr))

            scale = json_load(scale_name_template.format(ch, pr))

            odd = (arr_pr["eventNumber"] & 1).astype(bool)
            for par in ["odd", "even"]:
                if par == "odd":
                    arr_par = arr_pr[np.logical_not(odd)]  # even events (will be using odd model)
                else:
                    arr_par = arr_pr[odd]  # odd events (will be using even model)

                if not args.build_index:
                    outputs["temp_index"] = np.concatenate([outputs["temp_index"].astype(int), arr_par["temp_index"].astype(int)])
                outputs["runNumber"] = np.concatenate([outputs["runNumber"].astype(int), arr_par["runNumber"].astype(int)])
                outputs["eventNumber"] = np.concatenate([outputs["eventNumber"].astype(int), arr_par["eventNumber"].astype(int)])

                arr_par = drop_fields(arr_par, ["runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]", "temp_index"], usemask=False)

                if len(arr_par) == 0:
                    continue

                transformed = transformInputs[args.transformation](arr_par)
                normaliseInputs(transformed, scale=scale)

                if args.drop_vars:
                    transformed = drop_fields(transformed, args.drop_vars, usemask=False)

                # Store the low-level input
                if args.store_input_var:
                    for v in transformed.dtype.names:
                        if v.endswith("_px") or v.endswith("_py") or v.endswith("_pz") or v.endswith("_e"):
                            outputs["NN_input_%s"%v] = np.concatenate([outputs["NN_input_%s"%v], transformed[v]])

                nn_scores = {}

                if args.mode == "had":
                    if args.split_mode == "1p3p":
                        all_bkgs = set(args.backgrounds_1P + args.backgrounds_3P)
                    elif args.split_mode == "1p0nXn3p" and ch == "el":
                        all_bkgs = set(args.backgrounds_1p0n + args.backgrounds_1pXn_el + args.backgrounds_3p)
                    elif args.split_mode == "1p0nXn3p" and ch == "mu":
                        all_bkgs = set(args.backgrounds_1p0n + args.backgrounds_1pXn_mu + args.backgrounds_3p)
                elif args.mode == "lep":
                    all_bkgs = set(args.backgrounds_lep)
                for bkg in all_bkgs:
                    if not bkg in bkgs:
                        nn_scores[bkg] = -np.ones(len(arr_par))

                    else:
                        # Hard-coded version-specific variable drops
                        arr_bkg = dropVariables(transformed.copy(), args.transformation, bkg, ch, pr)

                        with acquire(lock):
                            if pr is None:
                                if args.mode == "lep":
                                    model = load_model(model_name_template.format(bkg, par))
                                else:
                                    model = load_model(model_name_template.format(ch, bkg, par))
                            else:
                                model = load_model(model_name_template.format(ch, pr, bkg, par))
                        arr_bkg = arr_bkg.view((arr_bkg.dtype[0], len(arr_bkg.dtype.names)))
                        nn_scores[bkg] = model.predict(arr_bkg).flatten()

                comb_scores = np.zeros(len(arr_par))
                bkg_weight_sum = 0
                for bkg in bkgs:
                    if not bkg in args.weights:
                        comb_scores += np.square(1 - nn_scores[bkg])
                        bkg_weight_sum += 1
                    else:
                        w = args.weights[bkg]
                        comb_scores += w * np.square(1 - nn_scores[bkg])
                        bkg_weight_sum += w
                comb_scores = np.sqrt(comb_scores/bkg_weight_sum)
                nn_scores["comb"] = 1 - comb_scores

                ### #Test different relative weights
                ### bkg_weights = {}
                ### z_weights = [0.125, 0.143, 0.167, 0.20, 0.25, 0.33, 0.5, 0.67, 1.00, 1.50, 2.00]
                ### w_weights = [0.25, 0.33, 0.50, 0.67, 1.00, 1.50, 2.00, 3.00, 4.00]
                ### for w in w_weights:
                ###     for z in z_weights:
                ###         bkg_weights["comb_1_%.2f_%.2f"%(w,z)] = {"Ztautau":1, "Wjets":w, "Zll":z}
                ### for name in bkg_weights:
                ###     comb_scores = np.zeros(len(arr_par))
                ###     bkg_weight_sum = 0
                ###     for bkg in bkgs:
                ###         w = bkg_weights[name][bkg]
                ###         comb_scores += w * np.square(1 - nn_scores[bkg])
                ###         bkg_weight_sum += w
                ###     comb_scores = np.sqrt(comb_scores/bkg_weight_sum)
                ###     nn_scores[name] = 1 - comb_scores

                for nn in nn_scores:
                    outputs["NN_output_%s"%nn] = np.concatenate([outputs["NN_output_%s"%nn], nn_scores[nn]])

    outputs = createStructuredArray(outputs.keys(), outputs.values())
    if not args.build_index:
        outputs.sort(order=["temp_index", "runNumber", "eventNumber"])
        outputs = drop_fields(outputs, "temp_index")

    with acquire(lock):
        print "="*30
        print "  Writing friend tree '{}'".format("NN_{}".format(input_tree))
        output_file = os.path.join(args.output_dir, os.path.basename(input_file))
        output_file += ".NN.{}_{}.friend".format(args.tag, args.transformation)
        with root_open(output_file, "UPDATE") as f_out:
            fd_t = array2tree(outputs, "NN_{}".format(input_tree))
            if args.build_index:
                fd_t.BuildIndex("runNumber", "eventNumber")
            if args.keep_old_cycles:
                f_out.WriteTObject(fd_t)
            else:
                f_out.WriteTObject(fd_t, "", "Overwrite")
            print "  Tree written, with {} entries".format(fd_t.GetEntries())

    return

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    decorateNN(args)
