#!/usr/bin/env python

import sys

from numpy import count_nonzero as count
from NN_modules.NN_utils import np_load

print "Loading numpy array '{}'...".format(sys.argv[1])
arr = np_load(sys.argv[1])
print "The loaded array has\n  {} entries".format(len(arr))
if "taus_n_tracks[0]" in arr.dtype.names:
    print "  (1p: {} [1p0n: {}, 1pXn: {}], 3p: {})".format(
    count(arr["taus_n_tracks[0]"]==1),
    count((arr["taus_n_tracks[0]"]==1) & (arr["taus_decay_mode[0]"]==0)),
    count((arr["taus_n_tracks[0]"]==1) & (arr["taus_decay_mode[0]"]!=0)),
    count(arr["taus_n_tracks[0]"]==3))
print "and the following fields:"
for f in arr.dtype.names:
    print "  {}".format(f)
