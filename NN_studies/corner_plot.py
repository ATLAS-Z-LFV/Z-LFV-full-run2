#!/usr/bin/env python

import sys, os, argparse

from modules.Regions import Regions
from modules.general_utils import findDefaultConfigs, checkMakeDirs, flattenCommaSeparatedLists
from NN_modules.custom_objects import load_model
from NN_modules.NN_utils import *

import numpy as np
from numpy.lib.recfunctions import drop_fields

import pandas as pd

import matplotlib
matplotlib.use('Agg')  # No display for running in batch/screen
import matplotlib.pyplot as plt

import seaborn as sns
sns.set(style="ticks")
sns.set_context("paper", rc={"axes.labelsize":24})


#-------------------------------------------------------------------------------
variable_label = {}
variable_label["tau_el_mvis"] = r"$m_\mathrm{vis}(\ell,\tau)$"
variable_label["tau_mu_mvis"] = r"$m_\mathrm{vis}(\ell,\tau)$"
variable_label["tau_el_mcoll"] = r"$m_\mathrm{coll}(\ell,\tau)$"
variable_label["tau_mu_mcoll"] = r"$m_\mathrm{coll}(\ell,\tau)$"
variable_label["tautrack_el_invmass"] = r"$m(\tau\:\mathrm{track},\ell)$"
variable_label["tautrack_mu_invmass"] = r"$m(\tau\:\mathrm{track},\ell)$"
variable_label["tau_el_delta_alpha"] = r"$\Delta\alpha$"
variable_label["tau_mu_delta_alpha"] = r"$\Delta\alpha$"
variable_label["lep_pz"] = r"$\hat{p}_z(\ell)$"
variable_label["lep_e"] = r"$\hat{E}(\ell)$"
variable_label["tau_px"] = r"$\hat{p}_x(\tau)$"
variable_label["tau_pz"] = r"$\hat{p}_z(\tau)$"
variable_label["tau_e"] = r"$\hat{E}(\tau)$"
variable_label["met_e"] = r"$\hat{E}_\mathrm{T}^\mathrm{miss}$"

#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser('./corner_plot.py')

    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")

    parser.add_argument("--tag", "-t", default="foo", type=str, help="The version tag.")
    parser.add_argument("--input-dir", "-i", default="./inputs", type=str, help="The input directory where the transformed input numpy arrays have been stored.")
    parser.add_argument("--transformation", "-T", default="", type=str, help="The input transformation scheme.")

    parser.add_argument("--samples", "-s", default="", type=str, help="A comma-separated list of input samples. Signal samples or data will be automatically added.")
    parser.add_argument("--region", "-r", default="", type=str, help="Region that defines the training sample selection.")
    parser.add_argument("--data-mode", action='store_true', help="Mode for comparing models to data (instead of signal).")

    parser.add_argument("--tau-prongs", default=1, choices=[1,3], type=int, help="Select number of tau prongs.")
    parser.add_argument("--drop-var", dest='drop_vars', default=[], action='append', type=str, help="Variable(s) to be dropped from inputs.")

    parser.add_argument("--N-samples", "-N", dest='n_samples', default=1000, type=int, help="Number of samples used for plotting, each for signal and background.")
    parser.add_argument("--plot-output", action='store_true', help="Plot output.")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args)

        args.samples = args.samples.split(",")
        args.input_dir = os.path.join(args.input_dir, args.tag)

        #args.drop_vars = flattenCommaSeparatedLists(args.drop_vars)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))

    if not os.path.isdir(args.input_dir):
        raise Exception("Input directory '{}' does not exists (or is not a directory).".format(args.input_dir))

    checkMakeDirs(args.output_dir)

    if not args.samples:
        raise Exception("No input samples specified.")

    if not args.region:
        raise Exception("No input region specified.")

    if not args.transformation:
        raise Exception("No input transformation scheme specified.")

#-------------------------------------------------------------------------------
def corner_plot(args):

    regions = Regions(args.regions_db)
    region = regions[args.region]

    backgrounds = args.samples
    signals = ["data"] if args.data_mode else region.signals

    model_name = "binary_{}_{}P_{}_{}".format(args.region, args.tau_prongs, "_".join(signals + backgrounds), args.transformation)

    #-------------------------------------------------
    # Load
    input = os.path.join(args.input_dir, "{}_%s.%s.npy" % (args.region, args.transformation))
    sig_array = None
    for s in signals:
        print "Loading signal data '{}'...".format(input.format(s))
        if sig_array is None:
            sig_array = np_load(input.format(s))
        else:
            sig_array = np.concatenate([sig_array, np_load(input.format(s))])
    bkg_array = None
    for b in backgrounds:
        print "Loading background data '{}'...".format(input.format(b))
        if bkg_array is None:
            bkg_array = np_load(input.format(b))
        else:
            bkg_array = np.concatenate([bkg_array, np_load(input.format(b))])

    if not bkg_array.dtype.names == sig_array.dtype.names:
        raise Exception("The input numpy arrays do not have the same field names.", bkg_array.dtype.names, sig_array.dtype.names)
    print "The loaded input arrays have the following fields:"
    for f in bkg_array.dtype.names:
        print "  {}".format(f)

    fields = [str(f) for f in bkg_array.dtype.names]
    drop_vars = droppedVariables(args.transformation, backgrounds[0],
                                 "mu" if region.signals[0] == "Ztaumu" else "el",
                                 "{:d}P".format(args.tau_prongs))
    drop_vars += args.drop_vars
    for x in drop_vars + ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]:
        if x in fields:
            fields.remove(x)

    #-------------------------------------------------
    # Load the scaling information and normalise inputs
    print "Loading scaling of the model and normalising inputs..."
    scale = json_load("models/{}/{}_{:d}P_{}.scaling".format(args.tag, args.region, args.tau_prongs, args.transformation))
    normaliseInputs([sig_array, bkg_array], scale=scale)

    #-------------------------------------------------
    # Select events according to tau prongs
    print "Preparing samples (splitting according to eventNumber and tau-prongs)..."
    bkg_array = bkg_array[bkg_array["taus_n_tracks[0]"]==args.tau_prongs]
    sig_array = sig_array[sig_array["taus_n_tracks[0]"]==args.tau_prongs]

    if args.plot_output:
        # Use only odd events and evaluate output with even model
        odd  = (sig_array["eventNumber"] & 1).astype(bool)
        sig_array = sig_array[odd]
        odd  = (bkg_array["eventNumber"] & 1).astype(bool)
        bkg_array = bkg_array[odd]

    # Pick same number of samples from bkg and sig to make fair scattering plots
    np.random.seed(42)

    weights = np.clip(bkg_array["weight"].astype(float), 0, None)
    probability = weights / np.sum(weights)
    bkg_array = np.random.choice(bkg_array, size=args.n_samples, p=probability)

    weights = np.clip(sig_array["weight"].astype(float), 0, None)
    probability = weights / np.sum(weights)
    sig_array = np.random.choice(sig_array, size=args.n_samples, p=probability)

    print "Randomly selected {} samples each from background and signal for plotting.".format(args.n_samples)

    #-------------------------------------------------
    # Make corner plot
    print "Making corner plot..."

    data, labels, _ = getLabelsAndWeights(sig_array, bkg_array, drop_vars)

    if args.plot_output:
        # Append NN output scores
        print "Calculating NN outputs using model '{}/{}'...".format(args.tag, model_name)
        model = load_model("models/{}/{}.even.h5".format(args.tag, model_name))
        nn_output = model.predict(data)
        data = np.append(data, nn_output.reshape((-1,1)), axis=1)
        fields.append("NN_output")

    # Append labels
    data = np.append(data, labels.reshape((-1,1)), axis=1)
    fields.append("labels")

    df = pd.DataFrame(data)

    renamed_fields = []
    for field in fields:
        try:
            renamed_fields.append(variable_label[field])
        except KeyError:
            renamed_fields.append(field)
    df.columns = renamed_fields

    # Shuffle so that both background and signal markers can be seen on scatter plots
    df = df.sample(frac=1).reset_index(drop=True)

    if args.data_mode:
        df['labels'] = df['labels'].map({0: 'background model', 1: 'data'})
    else:
        df['labels'] = df['labels'].map({0: 'background', 1: 'signal'})

    g = sns.PairGrid(df, hue="labels")
    g = g.map_diag(plt.hist, histtype="step", linewidth=3)
    g = g.map_offdiag(sns.regplot)

    handles = g._legend_data.values()
    labels = g._legend_data.keys()
    g.fig.legend(handles=handles, labels=labels, loc='upper center', ncol=2, fontsize=24)
    g.fig.subplots_adjust(top=0.95)

    #-------------------------------------------------
    # Save
    print "Saving the plots..."
    checkMakeDirs("plots/{}".format(args.tag))
    g.savefig("plots/{}/{}.cnr.png".format(args.tag, model_name))
    g.savefig("plots/{}/{}.cnr.pdf".format(args.tag, model_name))
    #g.savefig("plots/{}/{}.cnr.eps".format(args.tag, model_name))

    print "Saved 'plots/{}/{}.cnr.pdf(/eps/png)'.".format(args.tag, model_name)

    #-------------------------------------------------
    print "Done!"


#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    corner_plot(args)
