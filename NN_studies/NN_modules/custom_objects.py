#!/usr/bin/env python

#######################################################################
### Define custom activation/loss functions,                        ###
### and hack the Keras activations.get and objectives.get functions ###
### such that it can correctly load models with custom objects      ###
#######################################################################

import numpy as np

from keras import backend as K
from keras.models import load_model as keras_load_model
from keras.activations import get as get_default_activations
from keras.objectives import get as get_default_objectives

import tensorflow as tf
tf.python.control_flow_ops = tf

from mock import patch


#-------------------------------------------------------------------------------
# Custom activation functions
class CustomActivations(dict):
    def __init__(self):
        self['leaky_relu'] = self.leaky_relu
        self['leakier_relu'] = self.leakier_relu
        self['leaky_tanh'] = self.leaky_tanh
        self['leaky_tanh_plus_1'] = self.leaky_tanh_plus_1
        self['leaky_sigmoid'] = self.leaky_sigmoid
    
    @staticmethod
    def leaky_relu(x):
        return K.relu(x, alpha=0.1, max_value=0.0)

    @staticmethod
    def leakier_relu(x):
        return K.relu(x, alpha=0.3, max_value=0.0)

    @staticmethod
    def leaky_tanh(x):
        return K.tanh(x) + x*0.01

    @staticmethod
    def leaky_tanh_plus_1(x):
        return (K.tanh(x) + x*0.01) + 1

    @staticmethod
    def leaky_sigmoid(x):
        return K.sigmoid(x) + x*0.01

custom_activations = CustomActivations()

#-------------------------------------------------------------------------------
# Custom loss functions
class CustomObjectives(dict):
    def __init__(self):
        self['mean_squared_relative_error'] = self.mean_squared_relative_error
        self['msre'] = self.mean_squared_relative_error
        self['MSRE'] = self.mean_squared_relative_error
        self['binary_crossentropy'] = self.binary_crossentropy
        self['categorical_hinge'] = self.categorical_hinge
    
    @staticmethod
    def mean_squared_relative_error(y_true, y_pred):
        diff = K.square((y_true - y_pred) / K.clip(K.abs(y_true), K.epsilon(), np.inf))
        return K.mean(diff, axis=-1)
    
    @staticmethod
    # Conflict between Keras and TensorFlow with the versions used (LCG_91)
    # Patch with our own backend function
    def binary_crossentropy(y_true, y_pred):
        # transform back to logits
        epsilon = tf.convert_to_tensor(K.epsilon())
        if epsilon.dtype != y_pred.dtype.base_dtype:
            epsilon = tf.cast(epsilon, y_pred.dtype.base_dtype)
        y_pred = tf.clip_by_value(y_pred, epsilon, 1 - epsilon)
        y_pred = tf.log(y_pred / (1 - y_pred))
        return tf.nn.sigmoid_cross_entropy_with_logits(logits=y_pred, labels=y_true)
    
    @staticmethod
    # Keras version is too old in LCG_91 to have categorical_hinge defined...
    def categorical_hinge(y_true, y_pred):
        pos = K.sum(y_true * y_pred, axis=-1)
        neg = K.max((1. - y_true) * y_pred, axis=-1)
        return K.maximum(0., neg - pos + 1.)

custom_objectives = CustomObjectives()

#-------------------------------------------------------------------------------
# Combine custom objects
class CustomObjects(CustomActivations, CustomObjectives):
    pass

custom_objects = CustomObjects()

#-------------------------------------------------------------------------------
# Patch load model function
def get_activations(x):
    if x in custom_activations:
        return custom_activations[x]
    else:
        return get_default_activations(x)

def get_objectives(x):
    if x in custom_objectives:
        return custom_objectives[x]
    else:
        return get_default_objectives(x)

@patch('keras.activations.get', get_activations)
@patch('keras.objectives.get', get_objectives)
def load_model(file):
    return keras_load_model(file, custom_objects=custom_objects)

