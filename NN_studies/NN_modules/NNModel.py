#!/usr/bin/env python

import os
import numpy as np

from tensorflow.python.framework.ops import Tensor

#from keras.models import Sequential
from keras.models import Model
from keras.layers import Input, Dense, Dropout
from keras.optimizers import Adam, RMSprop
from keras.utils.visualize_util import plot as plot_model

from NN_utils import model_save
from custom_objects import get_activations, get_objectives, load_model

# Why do we suddenly need this, but not before?!?!
# This is needed to avoid version conflict
import tensorflow as tf
tf.python.control_flow_ops = tf



#-------------------------------------------------------------------------------
class NNModel(object):
    def __init__(self, input=None, nodes=[16,16], nOutputs=1, actv='relu', output_actv='sigmoid', loss='binary_crossentropy', epochs=200, batch_size=512, optimizer='adam', learning_rate=0.001, lr_decay=0, dropout=0):
        self.input = input
        self.nodes = nodes
        self.layers = len(nodes)
        self.nOutputs = nOutputs
        
        self.epochs = epochs
        self.batch_size = batch_size
        self.optimizer = optimizer
        self.learning_rate = learning_rate
        self.lr_decay = lr_decay
        self.dropout = dropout
        
        self.input_layer = input if isinstance(input, Tensor) else None
        self.hidden_layers = None
        self.output_layer = None
        self.model = None
        
        self.history = None
        
        try:
            self.actv = get_activations(actv)
            self.output_actv = get_activations(output_actv)
        except:
            raise Exception("NNModel.__init__(): activation function not defined in keras.activations or modules.custom_objects.")
        
        try:
            self.loss = get_objectives(loss)
        except:
            raise Exception("NNModel.__init__(): loss function not defined in keras.objectives or modules.custom_objects.")
    
    def build(self):
        if self.input_layer is None:
            if isinstance(self.input, int):
                self.input_layer = Input(shape = (self.input,))
            elif isinstance(self.input, Tensor):
                self.input_layer = self.input
            else:
                raise ValueError("NNModel.build(): Unknown input layer.")
        
        next_layer = Dense(self.nodes[0], activation=self.actv)(self.input_layer)
        self.hidden_layers = [next_layer]
        if self.dropout:
            self.hidden_layers.append(Dropout(self.dropout)(self.hidden_layers[-1]))
        for i,n in enumerate(self.nodes[1:]):
            next_layer = Dense(n, activation=self.actv)(self.hidden_layers[-1])
            self.hidden_layers.append(next_layer)
            if self.dropout:
                self.hidden_layers.append(Dropout(self.dropout)(self.hidden_layers[-1]))
        
        self.output_layer = Dense(self.nOutputs, activation=self.output_actv)(self.hidden_layers[-1])
        
        model = Model(input=self.input_layer, output=self.output_layer)
        self.model = model
    
    def compile(self):
        if self.model is None:
            self.build()
        
        if self.optimizer.lower() == 'adam':
            adam = Adam(lr=self.learning_rate, decay=self.lr_decay)
            self.model.compile(loss=self.loss, optimizer=adam, metrics=['accuracy'])
        elif self.optimizer.lower() == 'rmsprop':
            rmsprop = RMSprop(lr=self.learning_rate, decay=self.lr_decay)
            self.model.compile(loss=self.loss, optimizer=rmsprop, metrics=['accuracy'])
        else:
            self.model.compile(loss=self.loss, optimizer=self.optimizer, metrics=['accuracy'])
        
        print self.model.summary()
    
    def fit(self, input, target, sample_weight=None, validation_data=None):
        if self.model is None:
            raise ValueError("NNModel.fit(): Model was not compiled. Call NNModel.compile() before training.")
        
        self.history = self.model.fit(input, target, sample_weight=sample_weight, validation_data=validation_data, nb_epoch=self.epochs, batch_size=self.batch_size, verbose=2)
        
        return self.history
    
    def predict(self, input):
        return self.model.predict(input)
    
    def save(self, name):
        model_save(name, self.model)
        plot = os.path.splitext(name)[0]+"_visualized.png"
        if os.path.exists(plot):
            print "WARNING: Replacing existing file '{}'".format(plot)
            os.remove(plot)
        plot_model(self.model, to_file=plot, show_shapes=True)
    
    def load(self, name):
        self.model = load_model(name)
    
    def copy(self):
        return NNModel(input=self.input, nodes=self.nodes, nOutputs=self.nOutputs, actv=self.actv, output_actv=self.output_actv, loss=self.loss, epochs=self.epochs, batch_size=self.batch_size, optimizer=self.optimizer, learning_rate=self.learning_rate, lr_decay=self.lr_decay, dropout=self.dropout)


##-------------------------------------------------------------------------------
## The Keras wrappers require a function as an argument
## Create a class that initialises with NN model parameters,
## and return a compiled model when called
#class NNModel(object):
#    def __init__(self, nInputs, nodes=[10,10], actv='relu', output_actv='sigmoid', loss='MSE'):
#        self.nInputs = nInputs
#        self.nodes = nodes
#        self.layers = len(nodes)
#        
#        try:
#            self.actv = get_activations(actv)
#            self.output_actv = get_activations(output_actv)
#        except:
#            raise ValueError("NNModel.__init__(): activation function not defined in keras.activations or modules.custom_objects.")
#        
#        try:
#            self.loss = get_objectives(loss)
#        except:
#            raise ValueError("NNModel.__init__(): loss function not defined in keras.objectives or modules.custom_objects.")
#    
#    def __call__(self):
#        model = Sequential()
#        
#        # first hidden layer
#        model.add(Dense(self.nodes[0], input_shape=(self.nInputs,), activation=self.actv))
#        # other hidden layer(s)
#        for n in self.nodes[1:]:
#            model.add(Dense(n, activation=self.actv))
#        # output layer
#        model.add(Dense(1, activation=self.output_actv))
#        
#        model.compile(loss=self.loss, optimizer='adam')
#        print model.summary()
#        
#        return model

