import os, sys
import json

import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields

from ROOT import TChain
from root_numpy import tree2array

from modules.general_utils import rootVerbosityLevel
from modules.general_utils import checkMakeFile


#-------------------------------------------------------------------------------
# Force np.save() to remove existing file
# Create intermediate directories if not already exist
def np_save(path, array):
    if os.path.exists(path):
        print "WARNING: Replacing existing file '{}'".format(path)
        os.remove(path)
    checkMakeFile(path)
    np.save(path, array)

#-------------------------------------------------------------------------------
def np_load(path):
    with open(path) as f:
        arr = np.load(f)
    return arr

#-------------------------------------------------------------------------------
def json_save(path, data):
    if os.path.exists(path):
        print "WARNING: Replacing existing file '{}'".format(path)
        os.remove(path)
    with open(path, 'w') as f:
        json.dump(data, f)

#-------------------------------------------------------------------------------
def json_load(path):
    with open(path) as f:
        data = json.load(f)
    return data

#-------------------------------------------------------------------------------
# Force KerasRegressor.model.save() to remove existing file
# Create intermediate directories if not already exist
def model_save(path, model):
    if os.path.exists(path):
        print "WARNING: Replacing existing file '{}'".format(path)
        os.remove(path)
    checkMakeFile(path)
    model.save(path)

#-------------------------------------------------------------------------------
def setTensorFlowThreads(n_threads):
    from keras import backend as K
    import tensorflow as tf

    config = tf.ConfigProto(intra_op_parallelism_threads=n_threads,
                            inter_op_parallelism_threads=n_threads,
                            allow_soft_placement=True,
                            device_count = {'CPU': n_threads})
    session = tf.Session(config=config)
    K.set_session(session)

#-------------------------------------------------------------------------------
def createStructuredArray(field_names, field_values):
    arr = np.array(field_values[0], dtype=[(field_names[0], field_values[0].dtype.str)])
    if len(field_names) > 1:
        arr = append_fields(arr, field_names[1:], field_values[1:], usemask=False)
    return arr

#-------------------------------------------------------------------------------
def structuredArrayHeader(arr, drop=[]):
    header = arr[0:0]
    header = drop_fields(arr, drop)
    return header

#-------------------------------------------------------------------------------
def flattenStructuredArray(arr, reorder=False):
    field_names = list(arr.dtype.names)
    if reorder:
        field_names.sort()

    field_values = []
    for field in field_names:
        if arr[field].dtype == 'O':
            #field_values.append(np.hstack(arr[field]))  # slower but safer(?)
            field_values.append(arr[field].astype(arr[field][0][0]))
        else:
            field_values.append(arr[field])

    if not all(len(field_values[0])==len(x) for x in field_values[1:]):
        raise RuntimeError("Cannot flatten array - fields have non-uniform lengths!")

    return createStructuredArray(field_names, field_values)

#-------------------------------------------------------------------------------
def concatenateStructuredArray(arrays, no_warnings=False):
    if all(arr.dtype==arrays[0].dtype for arr in arrays[1:]):
        return np.concatenate(arrays)

    if not all(len(arr.dtype)==len(arrays[0].dtype) for arr in arrays[1:]):
        print arrays[0].dtype
        print arrays[1].dtype
        raise TypeError("Cannot concatenate structured arrays with different number of fields!")

    # Check the names of the fields, and print warnings when different
    if not no_warnings:
        for i in xrange(len(arrays[0].dtype)):
            s = set(arr.dtype.names[i] for arr in arrays)
            if len(s) > 1:
                print "  !! assuming equivalence: {}".format(" <--> ".join(s))

    field_names = list(arrays[0].dtype.names)

    field_values = []
    for i in xrange(len(arrays[0].dtype)):
        column = np.concatenate([arr[arr.dtype.names[i]] for arr in arrays])
        field_values.append(column)

    if not all(len(field_values[0])==len(x) for x in field_values[1:]):
        raise RuntimeError("Cannot concatenate structured arrays - fields have non-uniform lengths!")

    return createStructuredArray(field_names, field_values)

#-------------------------------------------------------------------------------
# root_numpy.tree2array, but read in events in smaller slices
# This is to avoid memory allocation error (even though we actually have enough memory)
def tree2array_inSlices(tree, branches, selection=None, stop=None, slices_size=2500000):
    if stop is None:
        stop = tree.GetEntries()
    else:
        stop = min(tree.GetEntries(), stop)

    slices = range(0, stop, slices_size) + [stop]

    print "  tree2array: Reading events #0-{}".format(slices[1]-1)
    arr = tree2array(tree, branches, selection, start=slices[0], stop=slices[1])

    if len(slices) > 2:
        for begin,end in zip(slices[1:-1], slices[2:]):
            print "  tree2array: Reading events #{}-{}".format(begin, end-1)
            arr = np.concatenate([arr, tree2array(tree, branches, selection, start=begin, stop=end)])

    return arr

#-------------------------------------------------------------------------------
def getRawInputArray(input, branches, cut, weight, aliases={}, max_samples=None):
    chain = TChain(input.name)
    chain.BuildIndex("runNumber", "eventNumber")

    chain.SetAlias("weight", weight)
    for name,formula in aliases.iteritems():
        chain.SetAlias(name, formula)

    with rootVerbosityLevel("Error"):
        for f in input.files.values():
            chain.Add("{}/{}_NOMINAL".format(f.name, f.tree))
            chain.AddFriend("weights", f.name)

    raw_input = tree2array_inSlices(chain, branches, cut.string, stop=max_samples)

    if max_samples is None:
        n_processed = chain.GetEntries()
    else:
        n_processed = min(chain.GetEntries(), max_samples)

    return raw_input, n_processed

#-------------------------------------------------------------------------------
def findInputScale(arrays, weighted=True):
    if not isinstance(arrays, list):
        arrays = [arrays]

    scale = {}
    for field in arrays[0].dtype.names:
        if field in ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]:
            continue
        sum = 0
        sum_sq = 0
        sum_weight = 0
        for arr in arrays:
            if weighted:
                sum += np.sum(arr[field] * arr["weight"])
                sum_sq += np.sum(np.square(arr[field]) * arr["weight"])
                sum_weight += np.sum(arr["weight"])
            else:
                sum += np.sum(arr[field])
                sum_sq += np.sum(np.square(arr[field]))
                sum_weight += len(arr[field])
        mean = sum / sum_weight
        std = np.sqrt(sum_sq / sum_weight - mean * mean)
        scale[field] = {'mean':float(mean), 'std':float(std)}

    return scale

#-------------------------------------------------------------------------------
def normaliseInputs(arrays, weighted=True, scale=None):
    if not isinstance(arrays, list):
        arrays = [arrays]

    if scale is None:
        scale = findInputScale(arrays, weighted)

    for field in arrays[0].dtype.names:
        if field in ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]:
            continue
        for arr in arrays:
            arr[field] = (arr[field] - scale[field]['mean']) / scale[field]['std']

    return scale

#-------------------------------------------------------------------------------
def getLabelsAndWeights(sig=None, bkg=None, drop=[], ratios=None):
    sam = []
    bkgs = bkg if isinstance(bkg, list) else None

    # Normalise weights for sig/bkg separately
    if sig is not None:
        sig["weight"] = sig["weight"] / np.sum(sig["weight"]) * len(sig["weight"])
        sam.append(sig)
    if bkgs is not None:
        for i,b in enumerate(bkgs):
            if b is None:
                continue
            norm = len(b["weight"]) / float(len(bkgs))
            if ratios is not None:
                norm *= ratios[i]
            b["weight"] = b["weight"] / np.sum(b["weight"]) * norm
        bkg = np.concatenate(bkgs)
        sam.append(bkg)
    elif bkg is not None:
        bkg["weight"] = bkg["weight"] / np.sum(bkg["weight"]) * len(bkg["weight"])
        sam.append(bkg)

    # Combine sig+bkg into one sample set, label sig=1 and bkg=0
    samples = np.concatenate(sam)
    labels = np.concatenate([np.ones(0 if sig is None else len(sig)),
                             np.zeros(0 if bkg is None else len(bkg))])
    weights = samples["weight"]

    # Drop unused fields and turn into numpy array (unstructured)
    _drop = drop + ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]
    samples = drop_fields(samples, _drop, usemask=False)
    for i in xrange(len(samples.dtype.names)):
        if not samples.dtype[i] == samples.dtype[0]:
            raise Exception("Input array does not have a uniform dtype.")
    samples = samples.view((samples.dtype[0], len(samples.dtype.names)))

    return samples, labels, weights

#-------------------------------------------------------------------------------
def getMultiLabelsAndWeights(sig=None, bkgs=[None, None, None], drop=[], ratios=None):
    sam = []

    # Normalise weights for sig/bkg separately
    if sig is not None:
        sam.append(sig)
        sig["weight"] = sig["weight"] / np.sum(sig["weight"]) * len(sig["weight"])
    for i,bkg in enumerate(bkgs):
        if bkg is None:
            continue
        sam.append(bkg)
        N = len(bkg["weight"])
        if ratios is not None:
            N *= ratios[i]
        bkg["weight"] = bkg["weight"] / np.sum(bkg["weight"]) * N

    # Combine sig+bkg into one sample set
    samples = np.concatenate(sam)
    weights = samples["weight"]

    # Set labels
    label = [1] + [0]*len(bkgs)
    labels = np.repeat([label], 0 if sig is None else len(sig), axis=0)
    for i,bkg in enumerate(bkgs):
        if bkg is None:
            continue
        label = [0] * (len(bkgs) + 1)
        label[i+1] = 1
        l = np.repeat([label], len(bkg), axis=0)
        labels = np.concatenate([labels, l])

    # Drop unused fields and turn into numpy array (unstructured)
    _drop = drop + ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]
    samples = drop_fields(samples, _drop, usemask=False)
    for i in xrange(len(samples.dtype.names)):
        if not samples.dtype[i] == samples.dtype[0]:
            raise Exception("Input array does not have a uniform dtype.")
    samples = samples.view((samples.dtype[0], len(samples.dtype.names)))

    return samples, labels, weights

#-------------------------------------------------------------------------------
def droppedVariables(transformation, process, channel, tau_mode):
    drop_vars = []

    # All the hard-coded variable drops
    if transformation == "R20_7_baseline" and not process == "Zll":  ###
        drop_vars.append("tau_{}_mvis".format(channel))

    if transformation == "Lorentz_invar_v2":
        if not process == "Zll":
            drop_vars.append("tautrack_{}_invmass".format(channel))

    if transformation == "Lorentz_invar_a0xy_v2":
        if not process == "Ztautau":
            drop_vars.append("lep_a0xy")
        if not tau_mode in ["1P", "1p0n"]:
            drop_vars.append("tautrack_{}_invmass".format(channel))

    if transformation in ["Lorentz_invar_a0xy_v3", "no_trans"]:
        if not process == "Ztautau":
            drop_vars.append("lep_a0xy")
        if not process == "Zll":
            drop_vars.append("tautrack_{}_invmass".format(channel))

    return drop_vars

#-------------------------------------------------------------------------------
def dropVariables(arr, transformation, process, channel, tau_mode):
    drop_vars = droppedVariables(transformation, process, channel, tau_mode)
    if drop_vars == []:
        return arr
    return drop_fields(arr, drop_vars, usemask=False)
