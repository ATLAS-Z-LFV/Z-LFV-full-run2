from collections import OrderedDict

import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields

from modules.maths_utils import Vector, LorentzVector


#-------------------------------------------------------------------------------
class Transform(dict):
    def __init__(self):
        ### to AK: Add the transformation you need
        self["example_transformation"] = self.example_transformation

        self["no_trans"] = self.no_trans
        self["R20_7_baseline"] = self.R20_7_baseline
        self["Lorentz_invar"] = self.Lorentz_invar
        self["Lorentz_invar_v2"] = self.Lorentz_invar_v2
        self["Lorentz_invar_tau_track"] = self.Lorentz_invar_tau_track
        self["Lorentz_invar_d0sig"] = self.Lorentz_invar_d0sig
        self["Lorentz_invar_a0xy"] = self.Lorentz_invar_a0xy
        self["Lorentz_invar_a0xy_v2"] = self.Lorentz_invar_a0xy_v2
        self["Lorentz_invar_a0xy_v3"] = self.Lorentz_invar_a0xy_v3
        self["Lorentz_invar_a0xy_gamma"] = self.Lorentz_invar_a0xy_gamma
        self["Lorentz_invar_a0xy_gamma_dPhi"] = self.Lorentz_invar_a0xy_gamma_dPhi

    #-------------------------------------------------
    def no_trans(self, arr):
        fields = arr.dtype.names
        new_fields = OrderedDict()

        mu_channel = "muons_pt[0]" in fields
        el_channel = "electrons_pt[0]" in fields

        tau_pt  = arr["taus_pt[0]"].astype(float)
        tau_eta = arr["taus_eta[0]"].astype(float)
        tau_phi = arr["taus_phi[0]"].astype(float)
        tau_m   = arr["taus_m[0]"].astype(float)
        tau_p4 = LorentzVector()
        tau_p4.setPtEtaPhiM(tau_pt, tau_eta, tau_phi, tau_m)

        met_et  = arr["met_et"].astype(float)
        met_eta = np.zeros(len(met_et))
        met_phi = arr["met_phi"].astype(float)
        met_m   = np.zeros(len(met_et))
        met_p4 = LorentzVector()
        met_p4.setPtEtaPhiM(met_et, met_eta, met_phi, met_m)

        if mu_channel:
            lep_pt  = arr["muons_pt[0]"].astype(float)
            lep_eta = arr["muons_eta[0]"].astype(float)
            lep_phi = arr["muons_phi[0]"].astype(float)
            lep_e   = arr["muons_e[0]"].astype(float)
            lep_a0xy = arr["mu_a0xy"].astype(float)
        elif el_channel:
            lep_pt  = arr["electrons_pt[0]"].astype(float)
            lep_eta = arr["electrons_eta[0]"].astype(float)
            lep_phi = arr["electrons_phi[0]"].astype(float)
            lep_e   = arr["electrons_e[0]"].astype(float)
            lep_a0xy = arr["el_a0xy"].astype(float)
        else:
            raise Exception("Cannot find muon or electron kinematics. What channel is this?")
        lep_p4 = LorentzVector()
        lep_p4.setPtEtaPhiE(lep_pt, lep_eta, lep_phi, lep_e)

        if "tau_0_lead_track_pt" in fields and not ("tautrack_mu_invmass" in fields or "tautrack_el_invmass" in fields):
            trk_pt  = arr["tau_0_lead_track_pt"].astype(float)
            trk_eta = arr["tau_0_lead_track_eta"].astype(float)
            trk_phi = arr["tau_0_lead_track_phi"].astype(float)
            trk_e   = arr["tau_0_lead_track_e"].astype(float)
            trk_p4 = LorentzVector()
            trk_p4.setPtEtaPhiE(trk_pt, trk_eta, trk_phi, trk_e)
            if mu_channel:
                new_fields["tautrack_mu_invmass"] = (trk_p4 + lep_p4).m
            elif el_channel:
                new_fields["tautrack_el_invmass"] = (trk_p4 + lep_p4).m

        # Only add the non-vanishing, independent components
        new_fields["lep_pz"] = lep_p4.z
        new_fields["lep_e"]  = lep_p4.t
        new_fields["tau_px"] = tau_p4.x
        new_fields["tau_pz"] = tau_p4.z
        new_fields["tau_e"]  = tau_p4.t
        new_fields["met_e"]  = met_p4.t

        new_fields["lep_a0xy"] = lep_a0xy

        # Old fields to drop
        keep = ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]
        if mu_channel:
            keep += ["tau_mu_mcoll", "tau_mu_delta_alpha", "tau_mu_mvis", "tautrack_mu_invmass"]
        elif el_channel:
            keep += ["tau_el_mcoll", "tau_el_delta_alpha", "tau_el_mvis", "tautrack_el_invmass"]
        old_fields = [f for f in fields if not f in keep]

        # Do not use mask, otherwise dumped MaskedArray has problems unpickling
        arr = drop_fields(arr, old_fields, usemask=False)
        arr = append_fields(arr, new_fields.keys(), new_fields.values(), usemask=False)

        return arr

    #-------------------------------------------------
    ### to AK: Modify this function for your need
    def example_transformation(self, arr):
        fields = arr.dtype.names
        new_fields = OrderedDict()

        elel_channel = "electrons_pt[0]" in fields and "electrons_pt[1]" in fields
        mumu_channel = "muons_pt[0]" in fields and "muons_pt[1]" in fields
        if "muons_pt[0]" in fields and "electrons_pt[0]" in fields:
            if arr["muons_pt[0]"][0] > arr["electrons_pt[0]"][0]:
                elmu_channel = False
                muel_channel = True
            else:
                elmu_channel = True
                muel_channel = False

        met_et  = arr["met_et"].astype(float)
        met_eta = np.zeros(len(met_et))
        met_phi = arr["met_phi"].astype(float)
        met_m   = np.zeros(len(met_et))
        met_p4 = LorentzVector()
        met_p4.setPtEtaPhiM(met_et, met_eta, met_phi, met_m)

        if elmu_channel:
            lep1_pt  = arr["electrons_pt[0]"].astype(float)
            lep1_eta = arr["electrons_eta[0]"].astype(float)
            lep1_phi = arr["electrons_phi[0]"].astype(float)
            lep1_e   = arr["electrons_e[0]"].astype(float)
            lep2_pt  = arr["muons_pt[0]"].astype(float)
            lep2_eta = arr["muons_eta[0]"].astype(float)
            lep2_phi = arr["muons_phi[0]"].astype(float)
            lep2_e   = arr["muons_e[0]"].astype(float)
        elif muel_channel:
            lep1_pt  = arr["muons_pt[0]"].astype(float)
            lep1_eta = arr["muons_eta[0]"].astype(float)
            lep1_phi = arr["muons_phi[0]"].astype(float)
            lep1_e   = arr["muons_e[0]"].astype(float)
            lep2_pt  = arr["electrons_pt[0]"].astype(float)
            lep2_eta = arr["electrons_eta[0]"].astype(float)
            lep2_phi = arr["electrons_phi[0]"].astype(float)
            lep2_e   = arr["electrons_e[0]"].astype(float)
        elif mumu_channel:
            lep1_pt  = arr["muons_pt[0]"].astype(float)
            lep1_eta = arr["muons_eta[0]"].astype(float)
            lep1_phi = arr["muons_phi[0]"].astype(float)
            lep1_e   = arr["muons_e[0]"].astype(float)
            lep2_pt  = arr["muons_pt[1]"].astype(float)
            lep2_eta = arr["muons_eta[1]"].astype(float)
            lep2_phi = arr["muons_phi[1]"].astype(float)
            lep2_e   = arr["muons_e[1]"].astype(float)
        elif elel_channel:
            lep1_pt  = arr["electrons_pt[0]"].astype(float)
            lep1_eta = arr["electrons_eta[0]"].astype(float)
            lep1_phi = arr["electrons_phi[0]"].astype(float)
            lep1_e   = arr["electrons_e[0]"].astype(float)
            lep2_pt  = arr["electrons_pt[1]"].astype(float)
            lep2_eta = arr["electrons_eta[1]"].astype(float)
            lep2_phi = arr["electrons_phi[1]"].astype(float)
            lep2_e   = arr["electrons_e[1]"].astype(float)
        else:
            pass
        lep1_p4 = LorentzVector()
        lep1_p4.setPtEtaPhiE(lep1_pt, lep1_eta, lep1_phi, lep1_e)
        lep2_p4 = LorentzVector()
        lep2_p4.setPtEtaPhiE(lep2_pt, lep2_eta, lep2_phi, lep2_e)

        # Boost to rest frame in x-y plane, do not touch z-direction
        tot_p4 = lep1_p4 + lep2_p4 + met_p4
        boost = -(tot_p4.boostVector())
        boost.z = np.zeros(len(boost.z))
        lep1_p4.boost(boost)
        lep2_p4.boost(boost)
        met_p4.boost(boost)

        # Rotate about z-axis to align MET with the x-axis
        x_axis = Vector(1,0,0)
        angle = met_p4.angle(x_axis)
        axis = met_p4.cross(x_axis)  # should be either +z or -z axis

        lep1_p4.rotate(angle, axis)
        lep2_p4.rotate(angle, axis)
        #met_p4.rotate(angle, axis)  # only need met_e as final component

        # Rotate about (new) x-axis by 180 degrees if lep2-pz is negative
        sign = np.sign(lep2_p4.z)
        lep2_p4.y = sign*lep2_p4.y
        lep2_p4.z = sign*lep2_p4.z
        #lep1_p4.y = sign*lep1_p4.y  # not final component
        lep1_p4.z = sign*lep1_p4.z

        # Only add the non-vanishing, independent components
        new_fields["lep1_pz"] = lep1_p4.z
        new_fields["lep1_e"]  = lep1_p4.t
        new_fields["lep2_px"] = lep2_p4.x
        new_fields["lep2_py"] = lep2_p4.y
        new_fields["lep2_pz"] = lep2_p4.z
        new_fields["lep2_e"]  = lep2_p4.t
        new_fields["met_e"]  = met_p4.t

        # Old fields to drop
        keep = ["weight", "runNumber", "eventNumber"]
        if elmu_channel:
            keep += ["mu_el_mcoll", "mu_el_mvis", "mu_el_delta_alpha", "el_mu_dEta"]
        elif muel_channel:
            keep += ["el_mu_mcoll", "mu_el_mvis", "mu_el_delta_alpha", "el_mu_dEta"] # "el_mu_delta_alpha",
        # elif mumu_channel:
        #     keep += ["mu_mu_mcoll", "mu_mu_mvis", "mu_mu_delta_alpha", "mu_mu_dEta"]
        # elif elel_channel:
        #     keep += ["el_el_mcoll", "el_el_mvis", "el_el_delta_alpha", "el_el_dEta"]
        old_fields = [f for f in fields if not f in keep]

        # Do not use mask, otherwise dumped MaskedArray has problems unpickling
        arr = drop_fields(arr, old_fields, usemask=False)
        arr = append_fields(arr, new_fields.keys(), new_fields.values(), usemask=False)

        return arr

    #-------------------------------------------------
    def R20_7_baseline(self, arr):
        fields = arr.dtype.names
        new_fields = OrderedDict()

        mu_channel = "muons_pt[0]" in fields
        el_channel = "electrons_pt[0]" in fields

        tau_pt  = arr["taus_pt[0]"].astype(float)
        tau_eta = arr["taus_eta[0]"].astype(float)
        tau_phi = arr["taus_phi[0]"].astype(float)
        tau_m   = arr["taus_m[0]"].astype(float)
        tau_p4 = LorentzVector()
        tau_p4.setPtEtaPhiM(tau_pt, tau_eta, tau_phi, tau_m)

        met_et  = arr["met_et"].astype(float)
        met_eta = np.zeros(len(met_et))
        met_phi = arr["met_phi"].astype(float)
        met_m   = np.zeros(len(met_et))
        met_p4 = LorentzVector()
        met_p4.setPtEtaPhiM(met_et, met_eta, met_phi, met_m)

        if mu_channel:
            lep_pt  = arr["muons_pt[0]"].astype(float)
            lep_eta = arr["muons_eta[0]"].astype(float)
            lep_phi = arr["muons_phi[0]"].astype(float)
            lep_e   = arr["muons_e[0]"].astype(float)
        elif el_channel:
            lep_pt  = arr["electrons_pt[0]"].astype(float)
            lep_eta = arr["electrons_eta[0]"].astype(float)
            lep_phi = arr["electrons_phi[0]"].astype(float)
            lep_e   = arr["electrons_e[0]"].astype(float)
        else:
            raise Exception("Cannot find muon or electron kinematics. What channel is this?")
        lep_p4 = LorentzVector()
        lep_p4.setPtEtaPhiE(lep_pt, lep_eta, lep_phi, lep_e)

        # Boost to rest frame
        tot_p4 = tau_p4 + lep_p4 + met_p4
        boost = -(tot_p4.boostVector())
        tau_p4.boost(boost)
        lep_p4.boost(boost)
        met_p4.boost(boost)

        # Rotate to align lep momentum with the z-axis
        z_axis = Vector(0,0,1)
        angle = lep_p4.angle(z_axis)
        axis = lep_p4.cross(z_axis)

        tau_p4.rotate(angle, axis)
        lep_p4.rotate(angle, axis)
        met_p4.rotate(angle, axis)

        # Rotate about (new) z-axis to put tau momentum in (new) x-z plane
        angle = -np.arctan(tau_p4.y / tau_p4.x)

        tau_p4.rotate(angle, z_axis)
        lep_p4.rotate(angle, z_axis)
        met_p4.rotate(angle, z_axis)

        # Only add the non-vanishing, independent components
        new_fields["lep_e"]  = lep_p4.t
        new_fields["tau_px"] = tau_p4.x
        new_fields["tau_pz"] = tau_p4.z
        new_fields["tau_e"]  = tau_p4.t
        new_fields["met_pz"] = met_p4.z
        new_fields["met_e"]  = met_p4.t
        new_fields["tot_pt"] = tot_p4.pt

        # Old fields to drop
        keep = ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]"]
        if mu_channel:
            keep += ["tau_mu_mcoll", "tau_mu_delta_alpha", "tau_mu_mvis"]
        elif el_channel:
            keep += ["tau_el_mcoll", "tau_el_delta_alpha", "tau_el_mvis"]
        old_fields = [f for f in fields if not f in keep]

        # Do not use mask, otherwise dumped MaskedArray has problems unpickling
        arr = drop_fields(arr, old_fields, usemask=False)
        arr = append_fields(arr, new_fields.keys(), new_fields.values(), usemask=False)

        return arr

    #-------------------------------------------------
    def Lorentz_invar(self, arr):
        fields = arr.dtype.names
        new_fields = OrderedDict()

        mu_channel = "muons_pt[0]" in fields
        el_channel = "electrons_pt[0]" in fields

        tau_pt  = arr["taus_pt[0]"].astype(float)
        tau_eta = arr["taus_eta[0]"].astype(float)
        tau_phi = arr["taus_phi[0]"].astype(float)
        tau_m   = arr["taus_m[0]"].astype(float)
        tau_p4 = LorentzVector()
        tau_p4.setPtEtaPhiM(tau_pt, tau_eta, tau_phi, tau_m)

        met_et  = arr["met_et"].astype(float)
        met_eta = np.zeros(len(met_et))
        met_phi = arr["met_phi"].astype(float)
        met_m   = np.zeros(len(met_et))
        met_p4 = LorentzVector()
        met_p4.setPtEtaPhiM(met_et, met_eta, met_phi, met_m)

        if mu_channel:
            lep_pt  = arr["muons_pt[0]"].astype(float)
            lep_eta = arr["muons_eta[0]"].astype(float)
            lep_phi = arr["muons_phi[0]"].astype(float)
            lep_e   = arr["muons_e[0]"].astype(float)
        elif el_channel:
            lep_pt  = arr["electrons_pt[0]"].astype(float)
            lep_eta = arr["electrons_eta[0]"].astype(float)
            lep_phi = arr["electrons_phi[0]"].astype(float)
            lep_e   = arr["electrons_e[0]"].astype(float)
        else:
            raise Exception("Cannot find muon or electron kinematics. What channel is this?")
        lep_p4 = LorentzVector()
        lep_p4.setPtEtaPhiE(lep_pt, lep_eta, lep_phi, lep_e)

        if "tau_0_lead_track_pt" in fields and not ("tautrack_mu_invmass" in fields or "tautrack_el_invmass" in fields):
            trk_pt  = arr["tau_0_lead_track_pt"].astype(float)
            trk_eta = arr["tau_0_lead_track_eta"].astype(float)
            trk_phi = arr["tau_0_lead_track_phi"].astype(float)
            trk_e   = arr["tau_0_lead_track_e"].astype(float)
            trk_p4 = LorentzVector()
            trk_p4.setPtEtaPhiE(trk_pt, trk_eta, trk_phi, trk_e)
            if mu_channel:
                new_fields["tautrack_mu_invmass"] = (trk_p4 + lep_p4).m
            elif el_channel:
                new_fields["tautrack_el_invmass"] = (trk_p4 + lep_p4).m

        # Boost to rest frame in x-y plane, do not touch z-direction
        tot_p4 = tau_p4 + lep_p4 + met_p4
        boost = -(tot_p4.boostVector())
        boost.z = np.zeros(len(boost.z))
        tau_p4.boost(boost)
        lep_p4.boost(boost)
        met_p4.boost(boost)

        # Rotate about z-axis to align MET with the x-axis
        x_axis = Vector(1,0,0)
        angle = met_p4.angle(x_axis)
        axis = met_p4.cross(x_axis)  # should be either +z or -z axis

        tau_p4.rotate(angle, axis)
        lep_p4.rotate(angle, axis)
        #met_p4.rotate(angle, axis)  # only need met_e as final component

        # Rotate about (new) x-axis by 180 degrees if tau-pz is negative
        sign = np.sign(tau_p4.z)
        tau_p4.y = sign*tau_p4.y
        tau_p4.z = sign*tau_p4.z
        #lep_p4.y = sign*lep_p4.y  # not final component
        lep_p4.z = sign*lep_p4.z

        # Only add the non-vanishing, independent components
        new_fields["lep_pz"] = lep_p4.z
        new_fields["lep_e"]  = lep_p4.t
        new_fields["tau_px"] = tau_p4.x
        new_fields["tau_py"] = tau_p4.y
        new_fields["tau_pz"] = tau_p4.z
        new_fields["tau_e"]  = tau_p4.t
        new_fields["met_e"]  = met_p4.t

        # Old fields to drop
        keep = ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]
        if mu_channel:
            keep += ["tau_mu_mcoll", "tau_mu_delta_alpha", "tau_mu_mvis", "tautrack_mu_invmass"]
        elif el_channel:
            keep += ["tau_el_mcoll", "tau_el_delta_alpha", "tau_el_mvis", "tautrack_el_invmass"]
        old_fields = [f for f in fields if not f in keep]

        # Do not use mask, otherwise dumped MaskedArray has problems unpickling
        arr = drop_fields(arr, old_fields, usemask=False)
        arr = append_fields(arr, new_fields.keys(), new_fields.values(), usemask=False)

        return arr

    #-------------------------------------------------
    def Lorentz_invar_v2(self, arr):
        # Also drop m(track,lep) for non-Zll
        # (hard-coded in NN_modules/NN_utils/droppedVariables.py)
        return self.Lorentz_invar(arr)

    #-------------------------------------------------
    def Lorentz_invar_tau_track(self, arr):
        fields = arr.dtype.names
        new_fields = OrderedDict()

        mu_channel = "muons_pt[0]" in fields
        el_channel = "electrons_pt[0]" in fields

        #tau_pt  = arr["taus_pt[0]"].astype(float)
        #tau_eta = arr["taus_eta[0]"].astype(float)
        #tau_phi = arr["taus_phi[0]"].astype(float)
        #tau_m   = arr["taus_m[0]"].astype(float)
        #tau_p4 = LorentzVector()
        #tau_p4.setPtEtaPhiM(tau_pt, tau_eta, tau_phi, tau_m)

        met_et  = arr["met_et"].astype(float)
        met_eta = np.zeros(len(met_et))
        met_phi = arr["met_phi"].astype(float)
        met_m   = np.zeros(len(met_et))
        met_p4 = LorentzVector()
        met_p4.setPtEtaPhiM(met_et, met_eta, met_phi, met_m)

        if mu_channel:
            lep_pt  = arr["muons_pt[0]"].astype(float)
            lep_eta = arr["muons_eta[0]"].astype(float)
            lep_phi = arr["muons_phi[0]"].astype(float)
            lep_e   = arr["muons_e[0]"].astype(float)
        elif el_channel:
            lep_pt  = arr["electrons_pt[0]"].astype(float)
            lep_eta = arr["electrons_eta[0]"].astype(float)
            lep_phi = arr["electrons_phi[0]"].astype(float)
            lep_e   = arr["electrons_e[0]"].astype(float)
        else:
            raise Exception("Cannot find muon or electron kinematics. What channel is this?")
        lep_p4 = LorentzVector()
        lep_p4.setPtEtaPhiE(lep_pt, lep_eta, lep_phi, lep_e)

        trk_pt  = arr["tau_0_lead_track_pt"].astype(float)
        trk_eta = arr["tau_0_lead_track_eta"].astype(float)
        trk_phi = arr["tau_0_lead_track_phi"].astype(float)
        trk_e   = arr["tau_0_lead_track_e"].astype(float)
        trk_p4 = LorentzVector()
        trk_p4.setPtEtaPhiE(trk_pt, trk_eta, trk_phi, trk_e)
        if mu_channel:
            new_fields["tautrack_mu_invmass"] = (trk_p4 + lep_p4).m
        elif el_channel:
            new_fields["tautrack_el_invmass"] = (trk_p4 + lep_p4).m
        tau_p4 = trk_p4

        # Boost to rest frame in x-y plane, do not touch z-direction
        tot_p4 = tau_p4 + lep_p4 + met_p4
        boost = -(tot_p4.boostVector())
        boost.z = np.zeros(len(boost.z))
        tau_p4.boost(boost)
        lep_p4.boost(boost)
        met_p4.boost(boost)

        # Rotate about z-axis to align MET with the x-axis
        x_axis = Vector(1,0,0)
        angle = met_p4.angle(x_axis)
        axis = met_p4.cross(x_axis)  # should be either +z or -z axis

        tau_p4.rotate(angle, axis)
        lep_p4.rotate(angle, axis)
        #met_p4.rotate(angle, axis)  # only need met_e as final component

        # Rotate about (new) x-axis by 180 degrees if tau-pz is negative
        sign = np.sign(tau_p4.z)
        tau_p4.y = sign*tau_p4.y
        tau_p4.z = sign*tau_p4.z
        #lep_p4.y = sign*lep_p4.y  # not final component
        lep_p4.z = sign*lep_p4.z

        # Only add the non-vanishing, independent components
        new_fields["lep_pz"] = lep_p4.z
        new_fields["lep_e"]  = lep_p4.t
        new_fields["tau_px"] = tau_p4.x
        new_fields["tau_py"] = tau_p4.y
        new_fields["tau_pz"] = tau_p4.z
        new_fields["tau_e"]  = tau_p4.t
        new_fields["met_e"]  = met_p4.t

        # Old fields to drop
        keep = ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]
        if mu_channel:
            keep += ["tau_mu_mcoll", "tau_mu_delta_alpha", "tau_mu_mvis"]
        elif el_channel:
            keep += ["tau_el_mcoll", "tau_el_delta_alpha", "tau_el_mvis"]
        old_fields = [f for f in fields if not f in keep]

        # Do not use mask, otherwise dumped MaskedArray has problems unpickling
        arr = drop_fields(arr, old_fields, usemask=False)
        arr = append_fields(arr, new_fields.keys(), new_fields.values(), usemask=False)

        return arr

    #-------------------------------------------------
    def Lorentz_invar_d0sig(self, arr):
        fields = arr.dtype.names
        new_fields = OrderedDict()

        mu_channel = "muons_pt[0]" in fields
        el_channel = "electrons_pt[0]" in fields

        tau_pt  = arr["taus_pt[0]"].astype(float)
        tau_eta = arr["taus_eta[0]"].astype(float)
        tau_phi = arr["taus_phi[0]"].astype(float)
        tau_m   = arr["taus_m[0]"].astype(float)
        tau_d0sig = np.clip(arr["taus_d0_sig[0]"], -1000., 1000.).astype(float)
        tau_p4 = LorentzVector()
        tau_p4.setPtEtaPhiM(tau_pt, tau_eta, tau_phi, tau_m)

        met_et  = arr["met_et"].astype(float)
        met_eta = np.zeros(len(met_et))
        met_phi = arr["met_phi"].astype(float)
        met_m   = np.zeros(len(met_et))
        met_p4 = LorentzVector()
        met_p4.setPtEtaPhiM(met_et, met_eta, met_phi, met_m)

        if mu_channel:
            lep_pt  = arr["muons_pt[0]"].astype(float)
            lep_eta = arr["muons_eta[0]"].astype(float)
            lep_phi = arr["muons_phi[0]"].astype(float)
            lep_e   = arr["muons_e[0]"].astype(float)
            lep_d0sig = arr["muons_trk_d0_sig[0]"].astype(float)
        elif el_channel:
            lep_pt  = arr["electrons_pt[0]"].astype(float)
            lep_eta = arr["electrons_eta[0]"].astype(float)
            lep_phi = arr["electrons_phi[0]"].astype(float)
            lep_e   = arr["electrons_e[0]"].astype(float)
            lep_d0sig = arr["electrons_trk_d0_sig[0]"].astype(float)
        else:
            raise Exception("Cannot find muon or electron kinematics. What channel is this?")
        lep_p4 = LorentzVector()
        lep_p4.setPtEtaPhiE(lep_pt, lep_eta, lep_phi, lep_e)

        if "tau_0_lead_track_pt" in fields and not ("tautrack_mu_invmass" in fields or "tautrack_el_invmass" in fields):
            trk_pt  = arr["tau_0_lead_track_pt"].astype(float)
            trk_eta = arr["tau_0_lead_track_eta"].astype(float)
            trk_phi = arr["tau_0_lead_track_phi"].astype(float)
            trk_e   = arr["tau_0_lead_track_e"].astype(float)
            old_fields += ["tau_0_lead_track_pt", "tau_0_lead_track_eta", "tau_0_lead_track_phi", "tau_0_lead_track_e"]
            trk_p4 = LorentzVector()
            trk_p4.setPtEtaPhiE(trk_pt, trk_eta, trk_phi, trk_e)
            if mu_channel:
                new_fields["tautrack_mu_invmass"] = (trk_p4 + lep_p4).m
            elif el_channel:
                new_fields["tautrack_el_invmass"] = (trk_p4 + lep_p4).m

        # Boost to rest frame in x-y plane, do not touch z-direction
        tot_p4 = tau_p4 + lep_p4 + met_p4
        boost = -(tot_p4.boostVector())
        boost.z = np.zeros(len(boost.z))
        tau_p4.boost(boost)
        lep_p4.boost(boost)
        met_p4.boost(boost)

        # Rotate about z-axis to align MET with the x-axis
        x_axis = Vector(1,0,0)
        angle = met_p4.angle(x_axis)
        axis = met_p4.cross(x_axis)  # should be either +z or -z axis

        tau_p4.rotate(angle, axis)
        lep_p4.rotate(angle, axis)
        #met_p4.rotate(angle, axis)  # only need met_e as final component

        # Rotate about (new) x-axis by 180 degrees if tau-pz is negative
        sign = np.sign(tau_p4.z)
        tau_p4.y = sign*tau_p4.y
        tau_p4.z = sign*tau_p4.z
        #lep_p4.y = sign*lep_p4.y  # not final component
        lep_p4.z = sign*lep_p4.z

        # Only add the non-vanishing, independent components
        new_fields["lep_pz"] = lep_p4.z
        new_fields["lep_e"]  = lep_p4.t
        new_fields["tau_px"] = tau_p4.x
        new_fields["tau_py"] = tau_p4.y
        new_fields["tau_pz"] = tau_p4.z
        new_fields["tau_e"]  = tau_p4.t
        new_fields["met_e"]  = met_p4.t

        new_fields["lep_d0sig"] = lep_d0sig
        new_fields["tau_d0sig"] = tau_d0sig

        # Old fields to drop
        keep = ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]
        if mu_channel:
            keep += ["tau_mu_mcoll", "tau_mu_delta_alpha", "tau_mu_mvis", "tautrack_mu_invmass"]
        elif el_channel:
            keep += ["tau_el_mcoll", "tau_el_delta_alpha", "tau_el_mvis", "tautrack_el_invmass"]
        old_fields = [f for f in fields if not f in keep]

        # Do not use mask, otherwise dumped MaskedArray has problems unpickling
        arr = drop_fields(arr, old_fields, usemask=False)
        arr = append_fields(arr, new_fields.keys(), new_fields.values(), usemask=False)

        return arr

    #-------------------------------------------------
    def Lorentz_invar_a0xy(self, arr):
        fields = arr.dtype.names
        new_fields = OrderedDict()

        mu_channel = "muons_pt[0]" in fields
        el_channel = "electrons_pt[0]" in fields

        tau_pt  = arr["taus_pt[0]"].astype(float)
        tau_eta = arr["taus_eta[0]"].astype(float)
        tau_phi = arr["taus_phi[0]"].astype(float)
        tau_m   = arr["taus_m[0]"].astype(float)
        tau_p4 = LorentzVector()
        tau_p4.setPtEtaPhiM(tau_pt, tau_eta, tau_phi, tau_m)

        met_et  = arr["met_et"].astype(float)
        met_eta = np.zeros(len(met_et))
        met_phi = arr["met_phi"].astype(float)
        met_m   = np.zeros(len(met_et))
        met_p4 = LorentzVector()
        met_p4.setPtEtaPhiM(met_et, met_eta, met_phi, met_m)

        if mu_channel:
            lep_pt  = arr["muons_pt[0]"].astype(float)
            lep_eta = arr["muons_eta[0]"].astype(float)
            lep_phi = arr["muons_phi[0]"].astype(float)
            lep_e   = arr["muons_e[0]"].astype(float)
            lep_a0xy = arr["mu_a0xy"].astype(float)
        elif el_channel:
            lep_pt  = arr["electrons_pt[0]"].astype(float)
            lep_eta = arr["electrons_eta[0]"].astype(float)
            lep_phi = arr["electrons_phi[0]"].astype(float)
            lep_e   = arr["electrons_e[0]"].astype(float)
            lep_a0xy = arr["el_a0xy"].astype(float)
        else:
            raise Exception("Cannot find muon or electron kinematics. What channel is this?")
        lep_p4 = LorentzVector()
        lep_p4.setPtEtaPhiE(lep_pt, lep_eta, lep_phi, lep_e)

        if "tau_0_lead_track_pt" in fields and not ("tautrack_mu_invmass" in fields or "tautrack_el_invmass" in fields):
            trk_pt  = arr["tau_0_lead_track_pt"].astype(float)
            trk_eta = arr["tau_0_lead_track_eta"].astype(float)
            trk_phi = arr["tau_0_lead_track_phi"].astype(float)
            trk_e   = arr["tau_0_lead_track_e"].astype(float)
            trk_p4 = LorentzVector()
            trk_p4.setPtEtaPhiE(trk_pt, trk_eta, trk_phi, trk_e)
            if mu_channel:
                new_fields["tautrack_mu_invmass"] = (trk_p4 + lep_p4).m
            elif el_channel:
                new_fields["tautrack_el_invmass"] = (trk_p4 + lep_p4).m

        # Boost to rest frame in x-y plane, do not touch z-direction
        tot_p4 = tau_p4 + lep_p4 + met_p4
        boost = -(tot_p4.boostVector())
        boost.z = np.zeros(len(boost.z))
        tau_p4.boost(boost)
        lep_p4.boost(boost)
        met_p4.boost(boost)

        # Rotate about z-axis to align MET with the x-axis
        x_axis = Vector(1,0,0)
        angle = met_p4.angle(x_axis)
        axis = met_p4.cross(x_axis)  # should be either +z or -z axis

        tau_p4.rotate(angle, axis)
        lep_p4.rotate(angle, axis)
        #met_p4.rotate(angle, axis)  # only need met_e as final component

        # Rotate about (new) x-axis by 180 degrees if tau-pz is negative
        sign = np.sign(tau_p4.z)
        tau_p4.y = sign*tau_p4.y
        tau_p4.z = sign*tau_p4.z
        #lep_p4.y = sign*lep_p4.y  # not final component
        lep_p4.z = sign*lep_p4.z

        # Only add the non-vanishing, independent components
        new_fields["lep_pz"] = lep_p4.z
        new_fields["lep_e"]  = lep_p4.t
        new_fields["tau_px"] = tau_p4.x
        new_fields["tau_py"] = tau_p4.y
        new_fields["tau_pz"] = tau_p4.z
        new_fields["tau_e"]  = tau_p4.t
        new_fields["met_e"]  = met_p4.t

        new_fields["lep_a0xy"] = lep_a0xy

        # Old fields to drop
        keep = ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]
        if mu_channel:
            keep += ["tau_mu_mcoll", "tau_mu_delta_alpha", "tau_mu_mvis", "tautrack_mu_invmass"]
        elif el_channel:
            keep += ["tau_el_mcoll", "tau_el_delta_alpha", "tau_el_mvis", "tautrack_el_invmass"]
        old_fields = [f for f in fields if not f in keep]

        # Do not use mask, otherwise dumped MaskedArray has problems unpickling
        arr = drop_fields(arr, old_fields, usemask=False)
        arr = append_fields(arr, new_fields.keys(), new_fields.values(), usemask=False)

        return arr

    #-------------------------------------------------
    def Lorentz_invar_a0xy_v2(self, arr):
        # Also drop a0xy for non-Ztautau and m(track,lep) for 3P
        # (hard-coded in NN_modules/NN_utils/droppedVariables.py)
        return drop_fields(self.Lorentz_invar_a0xy(arr), ["tau_py"], usemask=False)

    #-------------------------------------------------
    def Lorentz_invar_a0xy_v3(self, arr):
        # Also drop a0xy for non-Ztautau and m(track,lep) for non-Zll
        # (hard-coded in NN_modules/NN_utils/droppedVariables.py)
        return self.Lorentz_invar_a0xy_v2(arr)

    #-------------------------------------------------
    def Lorentz_invar_a0xy_gamma(self, arr):
        fields = arr.dtype.names
        new_fields = OrderedDict()

        mu_channel = "muons_pt[0]" in fields
        el_channel = "electrons_pt[0]" in fields

        tau_pt  = arr["taus_pt[0]"].astype(float)
        tau_eta = arr["taus_eta[0]"].astype(float)
        tau_phi = arr["taus_phi[0]"].astype(float)
        tau_m   = arr["taus_m[0]"].astype(float)
        tau_p4 = LorentzVector()
        tau_p4.setPtEtaPhiM(tau_pt, tau_eta, tau_phi, tau_m)

        met_et  = arr["met_et"].astype(float)
        met_eta = np.zeros(len(met_et))
        met_phi = arr["met_phi"].astype(float)
        met_m   = np.zeros(len(met_et))
        met_p4 = LorentzVector()
        met_p4.setPtEtaPhiM(met_et, met_eta, met_phi, met_m)

        if mu_channel:
            lep_pt  = arr["muons_pt[0]"].astype(float)
            lep_eta = arr["muons_eta[0]"].astype(float)
            lep_phi = arr["muons_phi[0]"].astype(float)
            lep_e   = arr["muons_e[0]"].astype(float)
            lep_a0xy = arr["mu_a0xy"].astype(float)
        elif el_channel:
            lep_pt  = arr["electrons_pt[0]"].astype(float)
            lep_eta = arr["electrons_eta[0]"].astype(float)
            lep_phi = arr["electrons_phi[0]"].astype(float)
            lep_e   = arr["electrons_e[0]"].astype(float)
            lep_a0xy = arr["el_a0xy"].astype(float)
        else:
            raise Exception("Cannot find muon or electron kinematics. What channel is this?")
        lep_p4 = LorentzVector()
        lep_p4.setPtEtaPhiE(lep_pt, lep_eta, lep_phi, lep_e)

        if "tau_0_lead_track_pt" in fields and not ("tautrack_mu_invmass" in fields or "tautrack_el_invmass" in fields):
            trk_pt  = arr["tau_0_lead_track_pt"].astype(float)
            trk_eta = arr["tau_0_lead_track_eta"].astype(float)
            trk_phi = arr["tau_0_lead_track_phi"].astype(float)
            trk_e   = arr["tau_0_lead_track_e"].astype(float)
            trk_p4 = LorentzVector()
            trk_p4.setPtEtaPhiE(trk_pt, trk_eta, trk_phi, trk_e)
            if mu_channel:
                new_fields["tautrack_mu_invmass"] = (trk_p4 + lep_p4).m
            elif el_channel:
                new_fields["tautrack_el_invmass"] = (trk_p4 + lep_p4).m

        ph_et  = arr["ph_0_et"].astype(float)
        ph_eta = arr["ph_0_eta"].astype(float)
        ph_phi = arr["ph_0_phi"].astype(float)
        ph_m   = np.zeros(len(ph_et))
        ph_p4 = LorentzVector()
        ph_p4.setPtEtaPhiM(ph_et, ph_eta, ph_phi, ph_m)

        # Boost to rest frame in x-y plane, do not touch z-direction
        tot_p4 = tau_p4 + lep_p4 + met_p4 + ph_p4
        boost = -(tot_p4.boostVector())
        boost.z = np.zeros(len(boost.z))
        tau_p4.boost(boost)
        lep_p4.boost(boost)
        met_p4.boost(boost)
        ph_p4.boost(boost)

        # Rotate about z-axis to align MET with the x-axis
        x_axis = Vector(1,0,0)
        angle = met_p4.angle(x_axis)
        axis = met_p4.cross(x_axis)  # should be either +z or -z axis

        tau_p4.rotate(angle, axis)
        lep_p4.rotate(angle, axis)
        #met_p4.rotate(angle, axis)  # only need met_e as final component
        ph_p4.rotate(angle, axis)

        # Rotate about (new) x-axis by 180 degrees if tau-pz is negative
        sign = np.sign(tau_p4.z)
        tau_p4.y = sign*tau_p4.y
        tau_p4.z = sign*tau_p4.z
        #lep_p4.y = sign*lep_p4.y  # not final component
        lep_p4.z = sign*lep_p4.z
        ph_p4.y = sign*ph_p4.y
        ph_p4.z = sign*ph_p4.z

        # Only add the non-vanishing, independent components
        new_fields["lep_pz"] = lep_p4.z
        new_fields["lep_e"]  = lep_p4.t
        new_fields["tau_px"] = tau_p4.x
        new_fields["tau_py"] = tau_p4.y
        new_fields["tau_pz"] = tau_p4.z
        new_fields["tau_e"]  = tau_p4.t
        new_fields["met_e"]  = met_p4.t
        new_fields["ph_px"]  = ph_p4.x
        #new_fields["ph_py"]  = ph_p4.y  # redundant (photon mass = 0)
        new_fields["ph_pz"]  = ph_p4.z
        new_fields["ph_e"]   = ph_p4.t

        new_fields["lep_a0xy"] = lep_a0xy

        # Old fields to drop
        keep = ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]
        if mu_channel:
            keep += ["tau_mu_delta_alpha", "tau_mu_mvis", "tautrack_mu_invmass", "tau_mu_ph_mcoll"]
        elif el_channel:
            keep += ["tau_el_delta_alpha", "tau_el_mvis", "tautrack_el_invmass", "tau_el_ph_mcoll"]
        old_fields = [f for f in fields if not f in keep]

        # Do not use mask, otherwise dumped MaskedArray has problems unpickling
        arr = drop_fields(arr, old_fields, usemask=False)
        arr = append_fields(arr, new_fields.keys(), new_fields.values(), usemask=False)

        return arr

    #-------------------------------------------------
    def Lorentz_invar_a0xy_gamma_dPhi(self, arr):
        fields = arr.dtype.names
        new_fields = OrderedDict()

        mu_channel = "muons_pt[0]" in fields
        el_channel = "electrons_pt[0]" in fields

        tau_pt  = arr["taus_pt[0]"].astype(float)
        tau_eta = arr["taus_eta[0]"].astype(float)
        tau_phi = arr["taus_phi[0]"].astype(float)
        tau_m   = arr["taus_m[0]"].astype(float)
        tau_p4 = LorentzVector()
        tau_p4.setPtEtaPhiM(tau_pt, tau_eta, tau_phi, tau_m)

        met_et  = arr["met_et"].astype(float)
        met_eta = np.zeros(len(met_et))
        met_phi = arr["met_phi"].astype(float)
        met_m   = np.zeros(len(met_et))
        met_p4 = LorentzVector()
        met_p4.setPtEtaPhiM(met_et, met_eta, met_phi, met_m)

        if mu_channel:
            lep_pt  = arr["muons_pt[0]"].astype(float)
            lep_eta = arr["muons_eta[0]"].astype(float)
            lep_phi = arr["muons_phi[0]"].astype(float)
            lep_e   = arr["muons_e[0]"].astype(float)
            lep_a0xy = arr["mu_a0xy"].astype(float)
        elif el_channel:
            lep_pt  = arr["electrons_pt[0]"].astype(float)
            lep_eta = arr["electrons_eta[0]"].astype(float)
            lep_phi = arr["electrons_phi[0]"].astype(float)
            lep_e   = arr["electrons_e[0]"].astype(float)
            lep_a0xy = arr["el_a0xy"].astype(float)
        else:
            raise Exception("Cannot find muon or electron kinematics. What channel is this?")
        lep_p4 = LorentzVector()
        lep_p4.setPtEtaPhiE(lep_pt, lep_eta, lep_phi, lep_e)

        if "tau_0_lead_track_pt" in fields and not ("tautrack_mu_invmass" in fields or "tautrack_el_invmass" in fields):
            trk_pt  = arr["tau_0_lead_track_pt"].astype(float)
            trk_eta = arr["tau_0_lead_track_eta"].astype(float)
            trk_phi = arr["tau_0_lead_track_phi"].astype(float)
            trk_e   = arr["tau_0_lead_track_e"].astype(float)
            trk_p4 = LorentzVector()
            trk_p4.setPtEtaPhiE(trk_pt, trk_eta, trk_phi, trk_e)
            if mu_channel:
                new_fields["tautrack_mu_invmass"] = (trk_p4 + lep_p4).m
            elif el_channel:
                new_fields["tautrack_el_invmass"] = (trk_p4 + lep_p4).m

        ph_et  = arr["ph_0_et"].astype(float)
        ph_eta = arr["ph_0_eta"].astype(float)
        ph_phi = arr["ph_0_phi"].astype(float)
        ph_m   = np.zeros(len(ph_et))
        ph_p4 = LorentzVector()
        ph_p4.setPtEtaPhiM(ph_et, ph_eta, ph_phi, ph_m)

        # Boost to rest frame in x-y plane, do not touch z-direction
        tot_p4 = tau_p4 + lep_p4 + met_p4 + ph_p4
        boost = -(tot_p4.boostVector())
        boost.z = np.zeros(len(boost.z))
        tau_p4.boost(boost)
        lep_p4.boost(boost)
        met_p4.boost(boost)
        ph_p4.boost(boost)

        # Only add pt and (some) delta phi
        new_fields["lep_pt"] = tau_p4.pt
        new_fields["tau_pt"] = lep_p4.pt
        new_fields["ph_et"] = ph_p4.pt
        new_fields["met_et"] = met_p4.pt
        new_fields["lep_tau_dPhi"] = lep_p4.deltaPhi(tau_p4)
        new_fields["lep_ph_dPhi"] = lep_p4.deltaPhi(ph_p4)
        new_fields["lep_met_dPhi"] = lep_p4.deltaPhi(met_p4)

        # a0xy
        new_fields["lep_a0xy"] = lep_a0xy

        # Old fields to drop
        keep = ["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]
        if mu_channel:
            keep += ["tau_mu_delta_alpha", "tau_mu_mvis", "tautrack_mu_invmass", "tau_mu_ph_mcoll"]
        elif el_channel:
            keep += ["tau_el_delta_alpha", "tau_el_mvis", "tautrack_el_invmass", "tau_el_ph_mcoll"]
        old_fields = [f for f in fields if not f in keep]

        # Do not use mask, otherwise dumped MaskedArray has problems unpickling
        arr = drop_fields(arr, old_fields, usemask=False)
        arr = append_fields(arr, new_fields.keys(), new_fields.values(), usemask=False)

        return arr


#-------------------------------------------------------------------------------
transformInputs = Transform()
