LCGENV_VER="LCG_91"
lsb_release -a | grep 'CentOS' | grep 'release 7' > /dev/null \
&& LCGENV_ARCH="x86_64-centos7-gcc62-opt" \
|| LCGENV_ARCH="x86_64-slc6-gcc62-opt"

[[ -z ${QUICK_SETUP+x} ]] && QUICK_SETUP=false

#--------------------------------------------------------------------------------
# setupATLAS

( hash asetup &> /dev/null )
if [[ $? -ne 0 ]] ; then
  # asetup not hashed, for sure you haven't setupATLAS! Try to do so now
  
  if [[ -z ${ATLAS_LOCAL_ROOT_BASE+x} ]] ; then
    # ATLAS_LOCAL_ROOT_BASE is not set, try to set it now
    echo "Setting ATLAS_LOCAL_ROOT_BASE..."
    
    if [[ `hostname -f` = stbc*.nikhef.nl ]] ; then
      # Good! We are on Nikhef's Stoomboot, we know how to set up :)
      source /project/atlas/nikhef/cvmfs/setup.sh
      
    elif [[ -d /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase ]] ; then
      # We have cvmfs, try setting ATLAS_LOCAL_ROOT_BASE there
      export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
      
    else
      echo "ERROR: ATLAS_LOCAL_ROOT_BASE is not set. Please set it first."
      return 1
      
    fi
  fi
  
  echo "Setting up ATLAS..."
  source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh > /dev/null
fi

( hash asetup &> /dev/null )
if [[ $? -ne 0 ]] ; then
  # asetup still not hashed, giving up...
  echo "ERROR: Failed to setup ATLAS."
  return 2
fi

#--------------------------------------------------------------------------------
# lcgenv

if [[ -f /cvmfs/sft.cern.ch/lcg/views/setupViews.sh ]] ; then
  source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh $LCGENV_VER $LCGENV_ARCH
else
  [[ $PYTHONPATH != *root_numpy* ]] && lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH root_numpy"
  [[ $PYTHONPATH != *tensorflow* ]] && lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH tensorflow"
  [[ $PYTHONPATH != *keras* ]] && lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH keras"
  [[ $PYTHONPATH != *scikitlearn* ]] && lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH scikitlearn"
  [[ $PYTHONPATH != *seaborn* ]] && lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH seaborn"
  [[ $PYTHONPATH != *h5py* ]] && lsetup "lcgenv -p $LCGENV_VER $LCGENV_ARCH h5py"
fi

# Remove duplicates in PATH/PYTHONPATH
PATH="$(perl -e 'print join(":", grep { not $seen{$_}++ } split(/:/, $ENV{PATH}))')"
PYTHONPATH="$(perl -e 'print join(":", grep { not $seen{$_}++ } split(/:/, $ENV{PYTHONPATH}))')"

##--------------------------------------------------------------------------------
## Python packages not available in lcgenv
#( pip show eli5 &> /dev/null )
#if [[ $? -ne 0 ]] ; then
#  # eli5 not installed, install now
#  echo 'Trying to install eli5...'
#  pip install --user eli5
#  if [[ $? -ne 0 ]] ; then
#    echo 'WARNING: Failed to install eli5.'
#  else
#    echo 'Installed eli5 (locally)'
#  fi
#fi

#--------------------------------------------------------------------------------
# Default config yaml files
[[ -z ${LFV_INPUTS+x} ]] && export LFV_INPUTS='configs/inputs.yaml'
[[ -z ${LFV_INPUTS_SPLIT+x} ]] && export LFV_INPUTS_SPLIT='configs/inputs_split.yaml'
[[ -z ${LFV_REGIONS+x} ]] && export LFV_REGIONS='configs/regions.yaml'
[[ -z ${LFV_HISTOGRAMS+x} ]] && export LFV_HISTOGRAMS='configs/histograms.yaml'
[[ -z ${LFV_SYSTEMATICS+x} ]] && export LFV_SYSTEMATICS='configs/systematics.yaml'
[[ -z ${LFV_PLOTS+x} ]] && export LFV_PLOTS='configs/plots.yaml'
[[ -z ${LFV_THINNING+x} ]] && export LFV_THINNING='configs/thinning.yaml'

#--------------------------------------------------------------------------------
# set python path

[[ $PYTHONPATH != *$PWD:* ]] && export PYTHONPATH=$PWD:$PYTHONPATH

return 0
