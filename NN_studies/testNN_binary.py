#!/usr/bin/env python

import sys, os, argparse

import numpy as np
np.random.seed(42)

import matplotlib
matplotlib.use('Agg')  # No display for running in batch/screen
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 20})
plt.rcParams.update({'figure.subplot.left': 0.15})
plt.rcParams.update({'figure.subplot.right': 0.95})
plt.rcParams.update({'figure.subplot.bottom': 0.15})
plt.rcParams.update({'figure.subplot.top': 0.95})

from sklearn.metrics import roc_curve, roc_auc_score

from modules.general_utils import flattenCommaSeparatedLists, checkMakeDirs
from modules.general_utils import writeOnFile
from NN_modules.custom_objects import load_model
from NN_modules.NN_utils import *

#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser('./testNN_binary.py')

    parser.add_argument("--tag", "-t", default="foo", type=str, help="The version tag.")
    parser.add_argument("--input-dir", "-i", default="./inputs", type=str, help="The input directory where the transformed input numpy arrays have been stored.")
    parser.add_argument("--model-dir", "-m", default="./models", type=str, help="The model directory where the trained models have been saved.")
    parser.add_argument("--region", "-r", default="", type=str, help="Region that defines the training sample selection.")
    parser.add_argument("--transformation", "-T", default="", type=str, help="The input transformation scheme.")
    parser.add_argument("--mode", choices=["had","lep"], default="had", type=str, help="Choose between leptonic['lep']/hadronic['had'] tau mode (default='had')")

    parser.add_argument("--signal", "-s", dest="signals", action='append', default=[], type=str, help="Signal samples.")
    parser.add_argument("--background", "-b", dest="backgrounds", action='append', default=[], type=str, help="Background samples.")

    parser.add_argument("--tau-decay", "-p", default="1P", choices=["1P","3P","1p0n","1pXn","3p"], type=str, help="Select tau decay mode.")
    parser.add_argument("--drop-var", dest='drop_vars', default=[], action='append', type=str, help="Variable(s) to be dropped from inputs.")

    parser.add_argument("--output-dir", "-o", required=True, default="", type=str, help="Output directory for saving plots and tables.")
    parser.add_argument("--output-fmt", default="png,pdf,eps", type=str, help="Comma-separated list of output formats, default='png,pdf,eps'.")

    parser.add_argument("--final-validation", "-F", action='store_true', help="Final validation. i.e. test on second set of validation samples ('odd-even' and 'even-even').")

    parser.add_argument("--skip-ranking", action='store_true', help="Skip evaluating input variable ranking.")

    try:
        args = parser.parse_args(argv)

        args.input_dir = os.path.join(args.input_dir, args.tag)
        args.model_dir = os.path.join(args.model_dir, args.tag)

        args.signals = flattenCommaSeparatedLists(args.signals)
        args.backgrounds = flattenCommaSeparatedLists(args.backgrounds)

        args.drop_vars = flattenCommaSeparatedLists(args.drop_vars)

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.isdir(args.input_dir):
        raise Exception("Input directory '{}' does not exists (or is not a directory).".format(args.input_dir))

    if not os.path.isdir(args.model_dir):
        raise Exception("Model directory '{}' does not exists (or is not a directory).".format(args.model_dir))

    checkMakeDirs(args.output_dir)

    if not args.signals:
        raise Exception("No signal samples provided.")
    if not args.backgrounds:
        raise Exception("No background samples provided.")

#-------------------------------------------------------------------------------
def testNN_binary(args):

    #-------------------------------------------------
    # Load
    input = os.path.join(args.input_dir, "{}_%s.%s.npy" % (args.region, args.transformation))
    sig_array = None
    for s in args.signals:
        print "Loading signal data '{}'...".format(input.format(s))
        if sig_array is None:
            sig_array = np_load(input.format(s))
        else:
            sig_array = np.concatenate([sig_array, np_load(input.format(s))])
    bkg_array = None
    for b in args.backgrounds:
        print "Loading background data '{}'...".format(input.format(b))
        if bkg_array is None:
            bkg_array = np_load(input.format(b))
        else:
            bkg_array = np.concatenate([bkg_array, np_load(input.format(b))])

    if not bkg_array.dtype.names == sig_array.dtype.names:
        raise Exception("The input numpy arrays do not have the same field names.", bkg_array.dtype.names, sig_array.dtype.names)
    print "The loaded input arrays have the following fields:"
    for f in bkg_array.dtype.names:
        print "  {}".format(f)

    #-------------------------------------------------
    # Load the scalings and normalise inputs
    if args.mode == "had":
        scale_file_name = "{}/{}_{}_{}.scaling".format(args.model_dir, args.region, args.tau_decay, args.transformation)
    elif args.mode == "lep":
        scale_file_name = "{}/{}_{}.scaling".format(args.model_dir, args.region, args.transformation)

    if not os.path.exists(scale_file_name):
        raise Exception("Couldn't find '{}'. Cannot perform input normalisation.".format(scale_file_name))
    print "Normalising inputs..."
    scale = json_load(scale_file_name)
    normaliseInputs([sig_array, bkg_array], scale=scale)

    #-------------------------------------------------
    # Split according to eventNumber
    print "Preparing samples (splitting according to eventNumber and tau-decay)..."
    four_multiples = bkg_array["eventNumber"] & 3
    if not args.final_validation:
        odd_even  = np.logical_not(four_multiples - 1)  # 4N+1
        even_even = np.logical_not(four_multiples)      # 4N
        bkg_odd  = bkg_array[odd_even]
        bkg_even = bkg_array[even_even]
    else:
        odd_odd  = np.logical_not(four_multiples - 3)  # 4N+3
        even_odd = np.logical_not(four_multiples - 2)  # 4N+2
        bkg_odd  = bkg_array[odd_odd]
        bkg_even = bkg_array[even_odd]

    four_multiples = sig_array["eventNumber"] & 3
    if not args.final_validation:
        odd_even  = np.logical_not(four_multiples - 1)  # 4N+1
        even_even = np.logical_not(four_multiples)      # 4N
        sig_odd  = sig_array[odd_even]
        sig_even = sig_array[even_even]
    else:
        odd_odd  = np.logical_not(four_multiples - 3)  # 4N+3
        even_odd = np.logical_not(four_multiples - 2)  # 4N+2
        sig_odd  = sig_array[odd_odd]
        sig_even = sig_array[even_odd]

    #-------------------------------------------------
    # Select events according to tau decay
    if args.mode == "had":
        n_tracks = int(args.tau_decay[0])
        bkg_odd      = bkg_odd[bkg_odd["taus_n_tracks[0]"]==n_tracks]
        bkg_even     = bkg_even[bkg_even["taus_n_tracks[0]"]==n_tracks]
        sig_odd      = sig_odd[sig_odd["taus_n_tracks[0]"]==n_tracks]
        sig_even     = sig_even[sig_even["taus_n_tracks[0]"]==n_tracks]
        if args.tau_decay == "1p0n":
            bkg_odd      = bkg_odd[bkg_odd["taus_decay_mode[0]"]==0]
            bkg_even     = bkg_even[bkg_even["taus_decay_mode[0]"]==0]
            sig_odd      = sig_odd[sig_odd["taus_decay_mode[0]"]==0]
            sig_even     = sig_even[sig_even["taus_decay_mode[0]"]==0]
        elif args.tau_decay == "1pXn":
            bkg_odd      = bkg_odd[bkg_odd["taus_decay_mode[0]"]!=0]
            bkg_even     = bkg_even[bkg_even["taus_decay_mode[0]"]!=0]
            sig_odd      = sig_odd[sig_odd["taus_decay_mode[0]"]!=0]
            sig_even     = sig_even[sig_even["taus_decay_mode[0]"]!=0]
        print "Will validate on {1} {0} signal samples and {2} {0} background samples.".format(
            args.tau_decay,
            len(sig_odd)+len(sig_even),
            len(bkg_odd)+len(bkg_even)
        )
    else:
        # bkg_odd      = bkg_odd[bkg_odd["taus_n_tracks[0]"]==args.tau_prongs]
        # bkg_even     = bkg_even[bkg_even["taus_n_tracks[0]"]==args.tau_prongs]
        # sig_odd      = sig_odd[sig_odd["taus_n_tracks[0]"]==args.tau_prongs]
        # sig_even     = sig_even[sig_even["taus_n_tracks[0]"]==args.tau_prongs]
        print "Will validate on {} signal samples and {} background samples.".format(
            len(sig_odd)+len(sig_even),
            len(bkg_odd)+len(bkg_even)
        )

    #-------------------------------------------------
    # Tuples of (samples, labels, weights)
    sam_odd  = getLabelsAndWeights(sig_odd, bkg_odd, args.drop_vars)
    sam_even = getLabelsAndWeights(sig_even, bkg_even, args.drop_vars)

    true = np.concatenate([sam_odd[1], sam_even[1]])
    weight = np.concatenate([sam_odd[2], sam_even[2]])

    #-------------------------------------------------
    print "Loading NN models..."
    if args.mode == "had":
        model_name = "{}/binary_{}_{}_{}_{}".format(args.model_dir, args.region, args.tau_decay, "_".join(args.backgrounds), args.transformation)
    else:
        model_name = "{}/binary_{}_{}_{}".format(args.model_dir, args.region, "_".join(args.backgrounds), args.transformation)
    model_odd = load_model("{}.odd.h5".format(model_name))
    model_even = load_model("{}.even.h5".format(model_name))

    print "Successfully loaded NN models."

    #-------------------------------------------------
    print "Evaluating..."
    pred_odd = model_even.predict(sam_odd[0])
    pred_even = model_odd.predict(sam_even[0])
    pred = np.concatenate([pred_odd, pred_even])

    #-------------------------------------------------
    print "Plotting ROC curve..."
    fpr, tpr, th = roc_curve(true, pred, sample_weight=weight)
    original_auc = roc_auc_score(true, pred, sample_weight=weight)

    plt.plot(fpr, tpr, 'b', label = 'AUC = %0.3f' % original_auc)

    sig_label = ",".join(args.signals)
    sig_label = sig_label.replace("Ztaue", r"$Z\rightarrow e\tau$")
    sig_label = sig_label.replace("Ztaumu", r"$Z\rightarrow\mu\tau$")
    bkg_label = ",".join(args.backgrounds)
    bkg_label = bkg_label.replace("Ztautau", r"$Z\rightarrow\tau\tau$")
    bkg_label = bkg_label.replace("Wjets", r"$W$+jets")
    bkg_label = bkg_label.replace("Zll", r"$Z\rightarrow\ell\ell$")
    legend_title = "{} vs {}".format(sig_label, bkg_label)
    if args.mode == "had":
        legend_title += ", {}".format(args.tau_decay)
    plt.legend(loc=4, title=legend_title, fontsize="medium")

    plt.plot([0, 1], [0, 1],'k--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.xlabel('False positive rate', horizontalalignment='right', x=1.0)
    plt.ylabel('True positive rate', horizontalalignment='right', y=1.0)
    for fmt in args.output_fmt.split(','):
        plt.savefig("{}/{}.roc.{}".format(args.output_dir, os.path.basename(model_name), fmt))
    plt.close()

    print "  Saved '{}/{}.roc.pdf(/eps/png)'.".format(args.output_dir, os.path.basename(model_name))

    #-------------------------------------------------
    print "Plotting NN output distributions..."
    nBins = 20
    log = False

    if args.mode == "had":
        sig_label += ", {}".format(args.tau_decay)
    plt.hist(pred[true==1], nBins, (0,1), weights=weight[true==1]/np.sum(weight[true==1]), label=sig_label, color='r', histtype='step', log=log)

    if args.mode == "had":
        bkg_label += ", {}".format(args.tau_decay)
    plt.hist(pred[true==0], nBins, (0,1), weights=weight[true==0]/np.sum(weight[true==0]), label=bkg_label, color='b', histtype='step', log=log)

    axes = plt.gca()
    axes.set_ylim([0,  # force y-range to start at zero
                   max(0.3, axes.get_ylim()[1]) ])

    plt.legend(loc=1, fontsize="medium")
    plt.xlabel('NN output', horizontalalignment='right', x=1.0)
    plt.ylabel('Normalised number of events', horizontalalignment='right', y=1.0)
    for fmt in args.output_fmt.split(','):
        plt.savefig("{}/{}.dist.{}".format(args.output_dir, os.path.basename(model_name), fmt))
    plt.close()

    print "  Saved '{}/{}.dist.pdf(/eps/png)'.".format(args.output_dir, os.path.basename(model_name))

    #-------------------------------------------------
    if args.skip_ranking:
        # We are done
        print "Done!"
        return
    else:
        print "Ranking variables according to permutation importance..."

    auc_scores = {}
    header = structuredArrayHeader(bkg_odd, drop=["weight", "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]"]+args.drop_vars)

    for i,v in enumerate(header.dtype.names):
        shuffled_input_odd = sam_odd[0].copy()
        np.random.shuffle(shuffled_input_odd[:,i])
        shuffled_input_even = sam_even[0].copy()
        np.random.shuffle(shuffled_input_even[:,i])

        #print "  Evaluating ROC AUC score with shuffled '{}'...".format(v)
        pred_odd = model_even.predict(shuffled_input_odd)
        pred_even = model_odd.predict(shuffled_input_even)
        pred = np.concatenate([pred_odd, pred_even])

        auc_scores[v] = roc_auc_score(true, pred, sample_weight=weight)
        #print "    AUC score = {}".format(auc_scores[v])

    out_file = "{}/{}.ranking.txt".format(args.output_dir, os.path.basename(model_name))
    with writeOnFile(out_file, echo=True) as f_write:
        var_name_len = max(8, max(len(v) for v in auc_scores))
        f_write("=" * (23+var_name_len))
        f_write((" Ranking  {:^%d}  Importance " % var_name_len).format("Variable"))
        f_write("=" * (23+var_name_len))

        rank = 0
        template = " {:7d}  {:%ds}  {:10f}" % var_name_len
        for v,s in sorted(auc_scores.items(), key=lambda x: x[1]):
            rank += 1
            line = template.format(rank, v, original_auc-s)
            f_write(line)
        f_write("=" * (23+var_name_len))

    print "  Saved '{}/{}.ranking.txt'.".format(args.output_dir, os.path.basename(model_name))

    #-------------------------------------------------
    print "Done!"


#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    testNN_binary(args)
