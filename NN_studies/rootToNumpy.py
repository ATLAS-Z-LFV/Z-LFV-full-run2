#!/usr/bin/env python

import os, sys, argparse
import gc
import numpy as np

from modules.Inputs import Inputs
from modules.Regions import Regions
from modules.general_utils import findDefaultConfigs, flattenCommaSeparatedLists, checkMakeDirs
from NN_modules.transformation import transformInputs
from NN_modules.NN_utils import *


#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser('./rootToNumpy.py')

    parser.add_argument("--inputs-db", "-I", default="", type=str, help="Inputs config in YAML.")
    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")

    parser.add_argument("--output-dir", "-o", default="./inputs", type=str, help="The output directory.")
    parser.add_argument("--tag", "-t", default="foo", type=str, help="The version tag.")
    parser.add_argument("--force-recreate", "-f", default=False, action="store_true", help="Force recreation of the input numpy files?")
    parser.add_argument("--force-recreate-raw", "-F", default=False, action="store_true", help="Force recreation of the raw input numpy files?")

    parser.add_argument("--samples", "-s", dest='samples', default="Ztautau,Wjets,Zll,data", type=str, help="A comma-separated list of input samples. Signal samples will be automatically added.")
    parser.add_argument("--regions", "-r", dest='regions', default="loose_SR_mu,loose_SR_el", type=str, help="Regions that define the training sample selections.")
    parser.add_argument("--additional-cut", "-c", dest='additional_cuts', default=["Zll:(tau_0_truth_isEle||tau_0_truth_isMuon)&&taus_signal[0]", "Wjets:taus_jet_rnn_medium[0]", "data:taus_signal[0]"], action='append', type=str, help="Additional sample-specific cut(s). Recognise string(s) in the format '<sample>:<cut>'.")
    parser.add_argument("--additional-cut-lep", dest='additional_cuts_lep', default=[""], action='append', type=str, help="Additional sample-specific cut(s) for the leptonic channel. Recognise string(s) in the format '<sample>:<cut>'.")
    parser.add_argument("--alt-bkgs", default=False, action="store_true", help="Use alternative background samples.")
    parser.add_argument("--temporary-cuts", default=False, action="store_true", help="Ignore cuts that have been marked 'ignore_temporarily' in the YAML file.")

    parser.add_argument("--transformation", "-T", dest='transformations', default=[], action='append', type=str, help="The input transformation scheme.")

    parser.add_argument("--max-samples", "-m", default=None, type=int, help="Maximum number of samples to be processed for each set of samples.")

    parser.add_argument("--v22", action="store_true", help="Backward compatibility with v22 n-tuples")
    parser.add_argument("--mode", choices=["had","lep"], default="had", type=str, help="Choose between leptonic['lep']/hadronic['had'] tau mode (default='had')")

    parser.add_argument("--no-combine-signal", default=False, action="store_true", help="Do not attempt to combine signals in el and mu channels.")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args)

        args.samples = args.samples.split(",")
        args.regions = args.regions.split(",")
        cuts = {}
        if args.mode=="lep":
            for c in flattenCommaSeparatedLists(args.additional_cuts_lep):
                l = c.split(":")
                cuts[l[0]] = ":".join(l[1:])
        else:
            for c in flattenCommaSeparatedLists(args.additional_cuts):
                l = c.split(":")
                cuts[l[0]] = ":".join(l[1:])
        args.additional_cuts = cuts
        args.transformations = flattenCommaSeparatedLists(args.transformations)
        if "ALL" in args.transformations:
            args.transformations = transformInputs.keys()

        if args.force_recreate_raw:
            args.force_recreate = True

        args.output_dir = os.path.join(args.output_dir, args.tag)

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.inputs_db):
        raise Exception("Cannot find inputs YAML file '{}'".format(args.inputs_db))
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))

    if len(args.samples) == 0:
        raise Exception("You haven't specified any samples")
    if len(args.regions) == 0:
        raise Exception("You haven't specified any regions")

    checkMakeDirs(args.output_dir)

#-------------------------------------------------------------------------------
def rootToNumpy(args):

    inputs = Inputs(args.inputs_db)
    regions = Regions(args.regions_db)

    inputs.useAlt(args.alt_bkgs)

    # Loop over input trees and set branches addresses
    for r in args.regions:
        region = regions[r]

        aliases = {}

        # Leptonic tau mode
        if args.mode == "lep":
            ### to AK: Modify this block for your need
            ### add all input variables to 'branches'

            has_muon = "mu_0_SF_recoSF_NOMINAL" in region.weights
            has_electron = "el_0_SF_recoSF_NOMINAL" in region.weights
            has_muon_2 = "mu_1_SF_recoSF_NOMINAL" in region.weights
            has_electron_2 = "el_1_SF_recoSF_NOMINAL" in region.weights

            # Common variables
            branches = ["weight",  # This will be aliased
                        "runNumber", "eventNumber",
                        "met_et", "met_phi",
                       ]

            # Channel specific variables
            if has_muon:
                branches += ["muons_pt[0]", "muons_eta[0]", "muons_phi[0]", "muons_e[0]"] #muons_d0[0]
            if has_electron:
                branches += ["electrons_pt[0]", "electrons_eta[0]", "electrons_phi[0]", "electrons_e[0]"]#electrons_d0[0]
            if has_muon_2:
                pass
            if has_electron_2:
                pass

            if has_muon and has_electron:
                branches += ["mu_el_mcoll", "el_mu_mcoll", "mu_el_mvis", "mu_el_delta_alpha", "mu_el_invmass"]#, "el_mu_delta_alpha", "mu_el_invmass"] #invmass
                # branches += ["el_mu_dEta"]
                # aliases["el_mu_dEta"] = "electrons_eta[0]-muons_eta[0]"

            ### Add formulae to 'aliases' if you need to use some derived variables
            # branches += ["derived_variable"]
            # aliases["derived_variable"] = "(electrons_eta[0]-muons_eta[0])"

            # if has_muon and has_electron:
            #     branches += ["el_mu_dEta"]
            #     aliases["el_mu_dEta"] = "(electrons_eta[0]-muons_eta[0])"
            ## TODO: how about a0xy or d0?


        # Hadronic tau mode
        elif args.mode == "had":
            has_muon = "mu_0_SF_recoSF_NOMINAL" in region.weights
            has_electron = "el_0_SF_recoSF_NOMINAL" in region.weights
            has_photon = "ph_0_SF_recoSF_NOMINAL" in region.weights

            branches = ["weight",  # This will be aliased
                        "runNumber", "eventNumber", "taus_n_tracks[0]", "taus_decay_mode[0]",
                        "taus_pt[0]", "taus_eta[0]", "taus_phi[0]", "taus_m[0]",
                        "met_et", "met_phi",
                       ]
            if has_muon:
                branches += ["muons_pt[0]", "muons_eta[0]", "muons_phi[0]", "muons_e[0]",
                             "tau_mu_mcoll", "tau_mu_delta_alpha", "tau_mu_mvis",
                             "tautrack_mu_invmass",
                            ]
            if has_electron:
                branches += ["electrons_pt[0]", "electrons_eta[0]", "electrons_phi[0]", "electrons_e[0]",
                             "tau_el_mcoll", "tau_el_delta_alpha", "tau_el_mvis",
                             "tautrack_el_invmass",
                            ]
            if args.v22:
                branches += ["tau_0_lead_track_pt", "tau_0_lead_track_eta", "tau_0_lead_track_phi", "tau_0_lead_track_e"]
                if has_muon:
                    branches.remove("tautrack_mu_invmass")
                if has_electron:
                    branches.remove("tautrack_el_invmass")

            if has_photon:
                branches += ["ph_0_et", "ph_0_eta", "ph_0_phi"]
                if has_muon:
                    branches += ["tau_mu_ph_mcoll", "tau_mu_ph_mvis"]
                if has_electron:
                    branches += ["tau_el_ph_mcoll", "tau_el_ph_mvis"]

            # Add a0xy
            branches += ["tau_a0xy"]
            aliases["tau_a0xy"] = "fabs(taus_d0[0] - vertex_x*TMath::Sin(taus_phi[0]) + vertex_y*TMath::Cos(taus_phi[0]))"
            if has_muon:
                branches += ["mu_a0xy"]
                aliases["mu_a0xy"] = "fabs(muons_trk_d0[0] - vertex_x*TMath::Sin(muons_phi[0]) + vertex_y*TMath::Cos(muons_phi[0]))"
            if has_electron:
                branches += ["el_a0xy"]
                aliases["el_a0xy"] = "fabs(electrons_trk_d0[0] - vertex_x*TMath::Sin(electrons_phi[0]) + vertex_y*TMath::Cos(electrons_phi[0]))"

            # In case we get a mix of v22- and v27+ ntuples
            aliases["taus_ele_bdt_score_run2"] = "taus_ele_bdt_score_trans"

        # Start converting root to npy
        for s in args.samples + region.signals:
            input = inputs[s]

            if args.temporary_cuts:
                cut = region.temporary_selection
            else:
                cut = region.selection
            if s in args.additional_cuts:
                cut &= args.additional_cuts[s]

            if input.is_data:
                # We just wanted weight=1
                # but we must do stupid things like this to match the data type...
                if has_muon:
                    weight = "muons_pt[0] - muons_pt[0] + 1"
                elif has_electron:
                    weight = "electrons_pt[0] - electrons_pt[0] + 1"
            else:
                weight = "normWeight*" + "*".join(region.weights)

            npy_file_name = "{}_{}.raw.npy".format(os.path.join(args.output_dir, s), r)
            if not os.path.exists(npy_file_name) or args.force_recreate_raw:
                print "Creating raw input '{}'".format(npy_file_name)
                raw_input, n_processed = getRawInputArray(input, branches, cut, weight, aliases, args.max_samples)
                print "{} - {}: Selected {} samples out of {} processed samples.".format(s, r, len(raw_input), n_processed)
                raw_input = flattenStructuredArray(raw_input)
                np_save(npy_file_name, raw_input)
                gc.collect()
            else:
                print "Reading raw input '{}'".format(npy_file_name)
                raw_input = np_load(npy_file_name)

            for t in args.transformations:
                npy_file_name = "{}_{}.{}.npy".format(os.path.join(args.output_dir, s), r, t)
                if os.path.exists(npy_file_name) and not args.force_recreate:
                    print "Found transformed input '{}'. Skipping...".format(npy_file_name)
                    continue
                print "Processing transformation '{}'".format(t)

                try:
                    transformed = transformInputs[t](raw_input)
                except KeyError:
                    print "WARNING: Transformation not found, skipping..."
                    continue

                np_save(npy_file_name, transformed)
                gc.collect()

    if not args.no_combine_signal:
        gc.collect()
        combinedElMuSignal(args)

    print "Done."


#-------------------------------------------------------------------------------
def combinedElMuSignal(args):
    regions = Regions(args.regions_db)
    for r in args.regions:
        if "_mu" in r:
            mu_region = regions[r]
        else:
            continue

        ### HARD-CODING!!
        if "_mutaulepe" in r:
            r_counterpart = r.replace("_mutaulepe", "_eltaulepmu")
        else:
            r_counterpart = r.replace("_mu", "_el")

        if r_counterpart in args.regions:
            el_region = regions[r_counterpart]
        else:
            continue

        for t in args.transformations:
            el_file = "{}_{}.{}.npy".format(el_region.signals[0], el_region.name, t)
            el_file = os.path.join(args.output_dir, el_file)
            el_file_backup = el_file + ".backup"

            mu_file = "{}_{}.{}.npy".format(mu_region.signals[0], mu_region.name, t)
            mu_file = os.path.join(args.output_dir, mu_file)
            mu_file_backup = mu_file + ".backup"

            if not os.path.exists(el_file):
                print "[WARNING] combinedElMuSignal(): Could not find '{}'".format(el_file)
                continue
            elif not os.path.exists(mu_file):
                print "[WARNING] combinedElMuSignal(): Could not find '{}'".format(mu_file)
                continue
            elif (os.path.exists(el_file_backup) or os.path.exists(mu_file_backup)) \
                and not args.force_recreate:
                continue

            el_arr = np_load(el_file)
            mu_arr = np_load(mu_file)

            print "Adding '{}' to '{}'".format(mu_file, el_file)
            comb_arr = concatenateStructuredArray([el_arr, mu_arr])
            if os.path.exists(el_file_backup):
                print "WARNING: Replacing existing file '{}'".format(el_file_backup)
            os.rename(el_file, el_file_backup)
            np_save(el_file, comb_arr)

            print "Adding '{}' to '{}'".format(el_file, mu_file)
            comb_arr = concatenateStructuredArray([mu_arr, el_arr])
            if os.path.exists(el_file_backup):
                print "WARNING: Replacing existing file '{}'".format(mu_file_backup)
            os.rename(mu_file, mu_file_backup)
            np_save(mu_file, comb_arr)


#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    rootToNumpy(args)
