#!/usr/bin/env python

import sys, os, argparse
import json

import numpy as np
np.random.seed(42)
import tensorflow as tf
tf.set_random_seed(42)

from modules.Regions import Regions
from modules.general_utils import findDefaultConfigs, checkMakeDirs, flattenCommaSeparatedLists
from modules.general_utils import execute
from NN_modules.NNModel import NNModel
from NN_modules.NN_utils import *

#-------------------------------------------------------------------------------
def parseArgs(argv):
    parser = argparse.ArgumentParser('./trainNN_binary.py')

    parser.add_argument("--regions-db", "-R", default="", type=str, help="Regions config in YAML.")

    parser.add_argument("--tag", "-t", default="foo", type=str, help="The version tag.")
    parser.add_argument("--input-dir", "-i", default="./inputs", type=str, help="The input directory where the transformed input numpy arrays have been stored.")
    parser.add_argument("--model-dir", "-m", default="./models", type=str, help="The output directory for saving the trained models.")
    parser.add_argument("--plot-dir", "-o", default="./plots", type=str, help="The output directory for saving the plots and tables.")
    parser.add_argument("--transformation", "-T", default="", type=str, help="The input transformation scheme.")
    parser.add_argument("--mode", choices=["had","lep"], default="had", type=str, help="Choose between leptonic['lep']/hadronic['had'] tau mode (default='had')")

    parser.add_argument("--samples", "-s", default="", type=str, help="A comma-separated list of input samples. Signal samples will be automatically added according to the '--region' option..")
    parser.add_argument("--region", "-r", default="", type=str, help="Region that defines the training sample selection.")

    parser.add_argument("--nodes", "-N", default=20, type=int, help="The number of nodes per layer.")
    parser.add_argument("--layers", "-L", default=2, type=int, help="The number of hidden layers.")
    parser.add_argument("--actv", default='relu', type=str, help="Hidden layer activation.")
    parser.add_argument("--epochs", "-E", default=200, type=int, help="The number training epochs.")
    parser.add_argument("--batch", "-B", default=256, type=int, help="The number of training samples per batch.")
    parser.add_argument("--learning-rate", "-lr", default=1e-4, type=float, help="The learning rate of the optimiser (adam).")
    parser.add_argument("--drop-out", "-D", default=0, type=float, help="The drop-out probability for the neural network.")

    parser.add_argument("--tau-decay", "-p", default="1P", choices=["1P","3P","1p0n","1pXn","3p"], type=str, help="Select tau decay mode.")
    parser.add_argument("--drop-var", dest='drop_vars', default=[], action='append', type=str, help="Variable(s) to be dropped from inputs.")

    parser.add_argument("--no-history", action='store_true', default=False, help="Train without history.")
    parser.add_argument("--no-testing", action='store_true', default=False, help="Do not call testNN_binary.py at the end.")
    parser.add_argument("--resume-training", action='store_true', default=False, help="Resume training of a previously saved model. (NN architecture will be inherited (including drop-out), while optimiser properties can be changed.)")

    try:
        args = parser.parse_args(argv)
        findDefaultConfigs(args)

        args.samples = args.samples.split(",")
        args.input_dir = os.path.join(args.input_dir, args.tag)
        args.model_dir = os.path.join(args.model_dir, args.tag)
        args.plot_dir = os.path.join(args.plot_dir, args.tag)

        args.drop_vars = flattenCommaSeparatedLists(args.drop_vars)

        checkArgs(args)

    except:
        parser.print_help()
        raise

    return args

#-------------------------------------------------------------------------------
def checkArgs(args):
    if not os.path.exists(args.regions_db):
        raise Exception("Cannot find regions YAML file '{}'".format(args.regions_db))

    if not os.path.isdir(args.input_dir):
        raise Exception("Input directory '{}' does not exists (or is not a directory).".format(args.input_dir))

    checkMakeDirs(args.model_dir)
    checkMakeDirs(args.plot_dir)

    if not args.samples:
        raise Exception("No input samples specified.")

    if not args.region:
        raise Exception("No input region specified.")

    if not args.transformation:
        raise Exception("No input transformation scheme specified.")

#-------------------------------------------------------------------------------
def trainNN_binary(args):

    setTensorFlowThreads(8)  # 'qsub -l nodes=1:ppn=8'

    regions = Regions(args.regions_db)
    region = regions[args.region]

    backgrounds = args.samples
    signals = region.signals

    if args.mode == "had":
        model_name = "binary_{}_{}_{}_{}".format(args.region, args.tau_decay, "_".join(args.samples), args.transformation)
    elif args.mode == "lep":
        model_name = "binary_{}_{}_{}".format(args.region, "_".join(args.samples), args.transformation)

    #-------------------------------------------------
    # Load
    input = os.path.join(args.input_dir, "{}_%s.%s.npy" % (args.region, args.transformation))
    sig_array = None
    for s in signals:
        print "Loading signal array '{}'...".format(input.format(s))
        if sig_array is None:
            sig_array = np_load(input.format(s))
        else:
            sig_array = np.concatenate([sig_array, np_load(input.format(s))])
    bkg_array = None
    for b in backgrounds:
        print "Loading background array '{}'...".format(input.format(b))
        if bkg_array is None:
            bkg_array = np_load(input.format(b))
        else:
            bkg_array = np.concatenate([bkg_array, np_load(input.format(b))])

    if not bkg_array.dtype.names == sig_array.dtype.names:
        raise Exception("The input numpy arrays do not have the same field names.", bkg_array.dtype.names, sig_array.dtype.names)
    print "The loaded input arrays have the following fields:"
    for f in bkg_array.dtype.names:
        print "  {}".format(f)

    #-------------------------------------------------
    # Normalise inputs and save the scaling (mean and variance of data)
    if args.mode == "had":
        scale_file_name = "{}/{}_{}_{}.scaling".format(args.model_dir, args.region, args.tau_decay, args.transformation)
    elif args.mode == "lep":
        scale_file_name = "{}/{}_{}.scaling".format(args.model_dir, args.region, args.transformation)

    if os.path.exists(scale_file_name):
        print "Found saved scaling information. Normalising inputs..."
        scale = json_load(scale_file_name)
    else:
        print "Normalising inputs and saving scaling information..."
        data_input = input.format("data")
        print "Loading data array '{}'...".format(data_input)
        if not os.path.exists(data_input):
            raise Exception("Data numpy array '{}' doesn't exist. Cannot perform input normalisation.".format(data_input))
        data_array = np_load(input.format("data"))
        scale = findInputScale([sig_array, data_array])
        json_save(scale_file_name, scale)
        data_array = None  # Make sure memory is freed

    normaliseInputs([sig_array, bkg_array], scale=scale)

    #-------------------------------------------------
    # Split according to eventNumber
    print "Preparing samples (splitting according to eventNumber and tau-prongs)..."
    odd  = (bkg_array["eventNumber"] & 1).astype(bool)
    even = np.logical_not(odd)
    four_multiples = bkg_array["eventNumber"] & 3
    odd_odd  = np.logical_not(four_multiples - 1)  # 4N+1
    even_odd = np.logical_not(four_multiples)      # 4N
    bkg_odd      = bkg_array[odd]
    bkg_even     = bkg_array[even]
    bkg_odd_odd  = bkg_array[odd_odd]
    bkg_even_odd = bkg_array[even_odd]

    odd  = (sig_array["eventNumber"] & 1).astype(bool)
    even = np.logical_not(odd)
    four_multiples = sig_array["eventNumber"] & 3
    odd_odd  = np.logical_not(four_multiples - 1)  # 4N+1
    even_odd = np.logical_not(four_multiples)      # 4N
    sig_odd      = sig_array[odd]
    sig_even     = sig_array[even]
    sig_odd_odd  = sig_array[odd_odd]
    sig_even_odd = sig_array[even_odd]

    #-------------------------------------------------
    # Select events according to tau decay mode
    # Do not distinguish difference decay modes for signal
    if args.mode == "had":
        n_tracks = int(args.tau_decay[0])
        bkg_odd      = bkg_odd[bkg_odd["taus_n_tracks[0]"]==n_tracks]
        bkg_even     = bkg_even[bkg_even["taus_n_tracks[0]"]==n_tracks]
        bkg_odd_odd  = bkg_odd_odd[bkg_odd_odd["taus_n_tracks[0]"]==n_tracks]
        bkg_even_odd = bkg_even_odd[bkg_even_odd["taus_n_tracks[0]"]==n_tracks]
        sig_odd      = sig_odd[sig_odd["taus_n_tracks[0]"]==n_tracks]
        sig_even     = sig_even[sig_even["taus_n_tracks[0]"]==n_tracks]
        sig_odd_odd  = sig_odd_odd[sig_odd_odd["taus_n_tracks[0]"]==n_tracks]
        sig_even_odd = sig_even_odd[sig_even_odd["taus_n_tracks[0]"]==n_tracks]
        if args.tau_decay == "1p0n":
            bkg_odd      = bkg_odd[bkg_odd["taus_decay_mode[0]"]==0]
            bkg_even     = bkg_even[bkg_even["taus_decay_mode[0]"]==0]
            bkg_odd_odd  = bkg_odd_odd[bkg_odd_odd["taus_decay_mode[0]"]==0]
            bkg_even_odd = bkg_even_odd[bkg_even_odd["taus_decay_mode[0]"]==0]
            sig_odd      = sig_odd[sig_odd["taus_decay_mode[0]"]==0]
            sig_even     = sig_even[sig_even["taus_decay_mode[0]"]==0]
            sig_odd_odd  = sig_odd_odd[sig_odd_odd["taus_decay_mode[0]"]==0]
            sig_even_odd = sig_even_odd[sig_even_odd["taus_decay_mode[0]"]==0]
        elif args.tau_decay == "1pXn":
            bkg_odd      = bkg_odd[bkg_odd["taus_decay_mode[0]"]!=0]
            bkg_even     = bkg_even[bkg_even["taus_decay_mode[0]"]!=0]
            bkg_odd_odd  = bkg_odd_odd[bkg_odd_odd["taus_decay_mode[0]"]!=0]
            bkg_even_odd = bkg_even_odd[bkg_even_odd["taus_decay_mode[0]"]!=0]
            sig_odd      = sig_odd[sig_odd["taus_decay_mode[0]"]!=0]
            sig_even     = sig_even[sig_even["taus_decay_mode[0]"]!=0]
            sig_odd_odd  = sig_odd_odd[sig_odd_odd["taus_decay_mode[0]"]!=0]
            sig_even_odd = sig_even_odd[sig_even_odd["taus_decay_mode[0]"]!=0]
        print "Will train on {1} ({2}) odd (even) {0} signal samples and {3} ({4}) {0} background samples, and validate on {5} ({6}) {0} signal samples and {7} ({8}) {0} background samples.".format(
            args.tau_decay,
            len(sig_odd), len(sig_even),
            len(bkg_odd), len(bkg_even),
            len(sig_even_odd), len(sig_odd_odd),
            len(bkg_even_odd), len(bkg_odd_odd),
        )
    elif args.mode == "lep":
        print "Will train on {} ({}) odd (even) signal samples and {} ({}) background samples, and validate on {} ({}) signal samples and {} ({}) background samples.".format(
            len(sig_odd), len(sig_even),
            len(bkg_odd), len(bkg_even),
            len(sig_even_odd), len(sig_odd_odd),
            len(bkg_even_odd), len(bkg_odd_odd),
        )

    #-------------------------------------------------
    # Repeat samples so that all sample sets have roughly the same number of samples
    # Otherwise sample sets with smaller size won't be sampled enough
    if not args.transformation == "R20_7_baseline":
        n_sig = len(sig_odd) + len(sig_even)
        n_bkg = len(bkg_odd) + len(bkg_even)

        if n_sig < n_bkg:
            sig_odd = np.repeat(sig_odd, n_bkg/n_sig)
            sig_even = np.repeat(sig_even, n_bkg/n_sig)
            #sig_odd_odd = np.repeat(sig_odd_odd, n_bkg/n_sig)
            #sig_even_odd = np.repeat(sig_even_odd, n_bkg/n_sig)
        else:
            bkg_odd = np.repeat(bkg_odd, n_sig/n_bkg)
            bkg_even = np.repeat(bkg_even, n_sig/n_bkg)
            #bkg_odd_odd = np.repeat(bkg_odd_odd, n_sig/n_bkg)
            #bkg_even_odd = np.repeat(bkg_even_odd, n_sig/n_bkg)

    #-------------------------------------------------
    # Combine signal and background, and get labels and weights
    # Tuples of (samples, labels, weights)
    sam_odd = getLabelsAndWeights(sig_odd, bkg_odd, args.drop_vars)
    sam_even = getLabelsAndWeights(sig_even, bkg_even, args.drop_vars)
    if args.no_history:
        sam_odd_odd = None
        sam_even_odd = None
    else:
        sam_odd_odd = getLabelsAndWeights(sig_odd_odd, bkg_odd_odd, args.drop_vars)
        sam_even_odd = getLabelsAndWeights(sig_even_odd, bkg_even_odd, args.drop_vars)

    #-------------------------------------------------
    print "Creating NN models..."
    model_odd = NNModel(sam_odd[0].shape[1], [args.nodes]*args.layers, actv=args.actv, epochs=args.epochs, batch_size=args.batch, learning_rate=args.learning_rate, dropout=args.drop_out)
    if args.resume_training:
        print "Will resume training by loading previously saved model"
        model_odd.load("models/{}/{}.odd.h5".format(args.tag, model_name))
    model_odd.compile()

    model_even = model_odd.copy()
    if args.resume_training:
        # This hasn't been tested yet!
        model_even.load("models/{}/{}.even.h5".format(args.tag, model_name))
    model_even.compile()

    print "Successfully created NN models."

    #-------------------------------------------------
    print "Training NN model (odd)..."
    validation_data = None if args.no_history else sam_even_odd
    history_odd = model_odd.fit(sam_odd[0], sam_odd[1], sample_weight=sam_odd[2], validation_data=validation_data)
    print "Successfully trained NN model (odd). Saving..."
    model_odd.save("{}/{}.odd.h5".format(args.model_dir, model_name))

    print "Training NN model (even)..."
    validation_data = None if args.no_history else sam_odd_odd
    history_even = model_even.fit(sam_even[0], sam_even[1], sample_weight=sam_even[2], validation_data=validation_data)
    print "Successfully trained NN model (even). Saving..."
    model_even.save("{}/{}.even.h5".format(args.model_dir, model_name))

    #------------------------------
    if not args.no_history:
        print "Making validation plots..."

        import matplotlib
        matplotlib.use('Agg')  # No display for running in batch/screen
        import matplotlib.pyplot as plt

        # Plot loss
        plt.plot(history_odd.history['loss'])
        plt.plot(history_odd.history['val_loss'])
        plt.plot(history_even.history['loss'])
        plt.plot(history_even.history['val_loss'])
        axes = plt.gca()
        axes.set_ylim([0, axes.get_ylim()[1]])  # force y-range to start at zero
        plt.legend(['training 1', 'testing 1', 'training 2', 'testing 2'], loc=1)
        plt.xlabel('Epoch', horizontalalignment='right', x=1.0)
        plt.ylabel('Loss', horizontalalignment='right', y=1.0)
        #ax = plt.gca()
        #ax.set_yscale("log")

        plt.savefig("{}/{}.loss.png".format(args.plot_dir, model_name))
        plt.savefig("{}/{}.loss.pdf".format(args.plot_dir, model_name))
        plt.savefig("{}/{}.loss.eps".format(args.plot_dir, model_name))
        plt.close()

        # Plot accuracy
        plt.plot(history_odd.history['acc'])
        plt.plot(history_odd.history['val_acc'])
        plt.plot(history_even.history['acc'])
        plt.plot(history_even.history['val_acc'])
        axes = plt.gca()
        axes.set_ylim([0, 1])  # force y-range to end at one
        plt.legend(['training 1', 'testing 1', 'training 2', 'testing 2'], loc=4)
        plt.xlabel('Epoch', horizontalalignment='right', x=1.0)
        plt.ylabel('Accuracy', horizontalalignment='right', y=1.0)
        #ax = plt.gca()
        #ax.set_yscale("log")

        plt.savefig("{}/{}.acc.png".format(args.plot_dir, model_name))
        plt.savefig("{}/{}.acc.pdf".format(args.plot_dir, model_name))
        plt.savefig("{}/{}.acc.eps".format(args.plot_dir, model_name))
        plt.close()

        history_odd = np.c_[history_odd.history['loss'], history_odd.history['val_loss'], history_odd.history['acc'], history_odd.history['val_acc']]
        np_save("plots/{}/{}.odd.hist.npy".format(args.tag, model_name), history_odd)
        history_even = np.c_[history_even.history['loss'], history_even.history['val_loss'], history_even.history['acc'], history_even.history['val_acc']]
        np_save("plots/{}/{}.even.hist.npy".format(args.tag, model_name), history_even)

    #------------------------------
    print "Done!!"

    #------------------------------
    testNN_cmd = "./testNN_binary.py --tag {t} --input-dir {i} --model-dir {m} --transformation {T} --region {r} --signal {s} --background {b} --mode {M} --tau-decay {p} --output-dir {o}".format(
        t=args.tag,
        i=os.path.join(os.path.split(args.input_dir)[0]),  # get rid of the tag
        m=os.path.join(os.path.split(args.model_dir)[0]),  # get rid of the tag
        T=args.transformation,
        r=args.region,
        s=",".join(signals),
        b=",".join(backgrounds),
        M=args.mode,
        p=args.tau_decay,
        o=args.plot_dir,
    )
    if args.drop_vars:
        testNN_cmd += " --drop-var {}".format(",".join(args.drop_vars))

    if args.no_testing:
        print "Run '{}' to plot distributions and ROC curves.".format(testNN_cmd)
    else:
        print "Calling testNN_binary.py to plot distributions and ROC curves"
        execute(testNN_cmd)


#-------------------------------------------------------------------------------
if __name__ == "__main__":
    args = parseArgs(sys.argv[1:])
    trainNN_binary(args)

